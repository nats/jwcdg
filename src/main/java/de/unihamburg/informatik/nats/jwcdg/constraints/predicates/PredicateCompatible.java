package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Hierarchy;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * Does #2 subsume #3, or vice versa, in #1?
 * 
 * equivalent to `( subsumes(a, b, c) | subsumes(a, c, b)
 */
public class PredicateCompatible extends Predicate {

	public PredicateCompatible(Grammar g) {
		super("compatible", g, false, 3, false);
	}

	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {
		AVM val1 = args.get(0).eval(lg, context, binding);
		AVM val2 = args.get(1).eval(lg, context, binding);
		AVM val3 = args.get(2).eval(lg, context, binding);
		
		AVMString sval1;
		AVMString sval2;
		AVMString sval3;

		if (val1 instanceof AVMString) {
			sval1 = (AVMString) val1;
		} else {
			throw new PredicateEvaluationException("invalid 1st arg of predicate 'compatible'");
		}
		
		if (val2 instanceof AVMString) {
			sval2 = (AVMString) val2;
		} else {
			throw new PredicateEvaluationException("invalid 2nd arg of predicate 'compatible'");
		}
		
		if (val3 instanceof AVMString) {
			sval3 = (AVMString) val3;
		} else {
			throw new PredicateEvaluationException("invalid 3rd arg of predicate 'compatible'");
		}
		
		Hierarchy h = grammar.findHierarchy(sval1.getString());
		
		if(h == null) {
			throw new PredicateEvaluationException("Hierarchy "+ sval1.getString()+ 
					" not found in predicate 'compatible'");
		}
		
		/* find both sorts */
		// TODO #EXCEPTION_HANDLING not found warnings ?!
//		"WARNING: type `%s' not found in hierarchy `%s' in constraint `%s'\n",
//		"WARNING: type `%s' not found in hierarchy `%s' in constraint `%s'\n",

		/*
		 * if ( a == b || (int)arrayElement( h->distance, a, b ) > 0 ) {
		 * cdgPrintf(CDG_WARNING, "WARNING: predicate `subsumes' true for %s and
		 * %s\n", tva->string, tvb->string); } */

		/* return canned answer */
		return h.subsumes(sval2.getString(), sval3.getString()) || h.subsumes(sval3.getString(), sval2.getString());
	}

}
