package de.unihamburg.informatik.nats.jwcdg.predictors;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.GraphemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.helpers.DaemonThreadFactory;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.lattice.Arc;

public class Tagger {
	public static final String TAGGER_COMMAND           = "taggerCommand";
	public static final String TAGGER_USE_SH           = "taggerUseShell";
	public static final String TAGGER_POS_PATH          = "taggerPosPath";
	public static final String TAGGER_IGNORE_IMPOSSIBLE = "taggerIgnoreImpossible";
	public static final String TAGGER_NORMALIZE_TO_ONE  = "taggerNOrmalizetoOne";
	
	private static final String RESULT_SPLIT_PATTERN = "[\t ]+";
	private static final String NAME = "POS";
	
	private static Map<String, TaggerProducer> activeTaggers = new HashMap<>();
	
	private static ExecutorService executorService = Executors.newCachedThreadPool(new DaemonThreadFactory());
	
	private static class TaggerProducer implements Runnable {
		Configuration config;
		LinkedBlockingQueue<Tagger> sink;
		public TaggerProducer(Configuration config) {
			this.config = config;
			this.sink = new LinkedBlockingQueue<>(4);
		}
		@Override
		public void run() {
			//noinspection InfiniteLoopStatement
			while(true)
				try {
					sink.put(new Tagger(config));
				} catch (InterruptedException e) {
				}
		}
		public Tagger get() {
			try {
				return sink.take();
			} catch (InterruptedException e) {
				throw new RuntimeException("Interrupted");
			}
		}
	}
	
	public static Tagger getTagger(Configuration config) {
		String command = config.getString(TAGGER_COMMAND); 
		if(!activeTaggers.containsKey(command)) {
			TaggerProducer prod = new TaggerProducer(config);
			executorService.submit(prod);
			activeTaggers.put(command, prod);
		}
		return activeTaggers.get(command).get();
	}
	
	/////////////////////////////////////////////////////////////
	
	private Configuration config;
	private String taggerCommand;
	private Process tproc;
	private File resfile;
	private static Logger logger = LogManager.getLogger(Tagger.class);
	
	public Tagger(Configuration config) {
		this.config = config;
		this.taggerCommand = config.getString(TAGGER_COMMAND);
		
		if(taggerCommand.equals("")) {
			logger.warn("No taggerCommand defined! Not using a tagger will have a significant impact on parsing performance and accuracy!");
			return;
		}
		
		try {
			resfile = File.createTempFile("jwcdg_taggerout",".txt");
			resfile.deleteOnExit();
		} catch(IOException e) {
			throw new RuntimeException("An error occured when calling the tagger: "+ e.getMessage());
		}
		
		logger.info("running tagger: "+taggerCommand+" >"+resfile.getAbsolutePath());
		try {
		if (config.getFlag(TAGGER_USE_SH)) {
			String[] cmd = new String[3];
			cmd[0] = "sh";
			cmd[1] = "-c";
			cmd[2] = taggerCommand+" >"+resfile.getAbsolutePath();
			tproc =Runtime.getRuntime().exec(cmd);
		} else {
			tproc =Runtime.getRuntime().exec(taggerCommand+" >"+resfile.getAbsolutePath());
		}
		} catch (IOException e) {
			throw new RuntimeException("An error occured when calling the tagger: "+ e.getMessage());
		}
	}
	
	/**
	 Decorate a lexeme graph with part-of-speech scores.
	 
	 Calls the specified external tagger and assigns predictions to the
	 underlying grapheme node.
	 */
	public void predict(LexemeGraph lg) {
		if (taggerCommand == null || taggerCommand.equals(""))
			return;
		int gi;
		List<Arc> l;

		/* check if wordgraph is linear, only then tagging can be done */
		if (lg.getLattice().branches()) {
			logger.warn("can't tag branching lattices!");
			return;
		}
		

		/* check for valid category index */
		if (config.getString(TAGGER_POS_PATH).equals("")) {
			logger.warn("no taggerPosPath defined!");
			return;
		}
		
		Path tagCat = new Path(config.getString(TAGGER_POS_PATH), null);

		//if(lg->lattice->lookahead) {
		//	l = listAppendList(listClone(lg->lattice->arcs), lg->lattice->lookahead);
		//} else {
		l = lg.getLattice().getArcs();
		//}

		try{
			OutputStreamWriter writer = new OutputStreamWriter(tproc.getOutputStream());
			/* iterate through words of the lattice
			 and send them to the tagger*/
			for (Arc a : l) {
				/* Words with embedded spaces confuse most taggers.
				   Temporarily replace ' ' with an unprintable control character. */
				String b = a.word.replace(' ', '\255');
				writer.write(String.format("%s\n", b));
			}
			
			writer.write("\n");
			writer.flush();
			writer.close();
			try {
				tproc.waitFor();
			} catch (InterruptedException e) {
				throw new RuntimeException("Tagger was interrupted : "+ e.getMessage());
			}
			BufferedReader reader = new BufferedReader( new FileReader(resfile));
			/* go through tagger response and set tag score accordingly */
			gi = 0;
			String line;
			while (!"".equals(line = reader.readLine()) && !(line==null) ) {
				GraphemeNode gn;
				String w;
				double best = 0.0;
	
				/* Misbehaving taggers might print more lines of output than input,
				 guard against this */
				if (gi >= lg.getLastGN().getArc().to) { 
					//if(lg->lattice->lookahead) {
					//	// a longer output is to be expected, as we gave more words, just stop the loop
					//	break;
					//} else {
					logger.error("Too many lines of tagger output! Tagging stopped for this sentence!");
					return;
					//}
				}
	
				gn = lg.getGNs().get(gi);
				String[] res = line.split(RESULT_SPLIT_PATTERN);
				if(res.length < 2) {
					logger.warn("tagger returned a missformed line:\n" + line);
					continue;
				}
				
				w = res[0];
	
				/* Undo space escaping that we might have done */
				String b= w.replace('\255', ' ');
	
				if (!b.equals(gn.getArc().word)) {
					logger.error(String.format("word mismatch in tagger output: %s != %s", 
							b, gn.getArc().word));
					break;
				}
	
				// determine and write tag scores
				Set<String> lexCats = gn.collectPathValues(tagCat);
				Map<String, AVM> tagCats= new HashMap<>();
	
				/*
				 * We want to remember the best tag regardless of its
				 * (non-)appearance in the lexicon so we can pass it over to
				 * the malt predictor if none of the tags matches the one in
				 * our lexicon. Else malt wouldn't have a tag for this word
				 * and couldn't predict.
				 */
				double bestScoreOverall=0.0;
				String bestTagOverall = "NN";
				/* loop through all tokens in a response line */
				for (int i=1; i<res.length; i+=2) {
					String cat = res[i];
					double score = Double.parseDouble(res[i+1]);
					if (score > bestScoreOverall) {
						bestScoreOverall = score;
						bestTagOverall = cat;
					}
					if (!lexCats.contains(cat)) {
						if (config.getFlag(TAGGER_IGNORE_IMPOSSIBLE)) {
							logger.debug(String.format(
									"category `%s' not in lexicon", cat));
							continue;
						}
					}
					best = Math.max(best, score);
					tagCats.put(cat, new AVMNumber(score));
				}
	
				/* Normalize away the probabilities spent on impossible categories */
	//			if (config.getFlag(TAGGER_IGNORE_IMPOSSIBLE)) {
	//				normalizeTagScore(tagCats, total);
	//				best /= total;
	//			}
	
				/* Convert probabilities to scores */
				//if (config.getFlag(TAGGER_NORMALIZE_TO_ONE)) {
				normalizeTagScore(tagCats, best);
				//}
	
				/* if any of the predictions match any of the possibilities in our
				 lexicon, write those scores and set the others to 0 */
				if (!tagCats.isEmpty()) {
					for (String cat : lexCats) {
						if(!tagCats.containsKey(cat))
							tagCats.put(cat, new AVMNumber(0));
					}
					
				} else {
					/* Otherwise just leave the predictions of 1 everywhere. */
					logger.warn(String.format(
							"none of the suggested tags for `%s' is in your lexicon.", 
							b));
					/* and put the best tag into tagCats for the malt predictor */
					tagCats.put(bestTagOverall, new AVMNumber(1.0));
				}
				gn.setPredictionFor(NAME, tagCats);
	
				gi++;
			}
		} catch (IOException e) {
			logger.error("An error occured when calling the tagger: "+ e.getMessage());
			throw new RuntimeException("An error occured when calling the tagger: "+ e.getMessage());
		}
	}

	private void normalizeTagScore(Map<String, AVM> tagCats, double best) {
		List<String> keys= new ArrayList<>(tagCats.keySet());
		
		for(String key : keys) {
			double newScore = ((AVMNumber)tagCats.get(key)).getNumber()/best;
			tagCats.put(key, new AVMNumber(newScore));
		}
	}
}
