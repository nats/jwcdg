package de.unihamburg.informatik.nats.jwcdg.transform;

import java.util.ArrayList;
import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.evaluation.HasCache;
import de.unihamburg.informatik.nats.jwcdg.lattice.Arc;

/**
 * state of an analysis that cannot, in contrast to an analysis, be changed
 * used for evaluating context sensitive constraints 
 * 
 * @author the CDG-Team
 */
public class Context {

	private List<LevelValue> slots;
	
	private HasCache hasCache;
	
	
	/**
	 * no cache is initialized here!
	 * 
	 * @param slots
	 */
	public Context(List<LevelValue> slots) {
		this.slots= new ArrayList<>(slots);
		hasCache = null;
	}
	
	public boolean isCacheInitialized() {
		return hasCache != null;
	}
	
	/**
	 * will overwrite existing cache,
	 * check via isCacheInitialized beforehand
	 */
	public void initializeCache(int lgLength, int noHas) {
		this.hasCache = new HasCache(lgLength, noHas);
	}

	public int getNoOfLVs() {
		return slots.size();
	}

	public LevelValue getLV(int index) {
		return slots.get(index);
	}

	/**
	 * will do nothing if no cache is initialized
	 */
	public void cacheResult(int formulaID, Arc arc, boolean b) {
		if(hasCache == null)
			return;
		
		hasCache.put(formulaID, arc.from, b);
	}

	public boolean hasCachedResultFor(int formulaID, Arc arc) {
		if(hasCache == null)
			return false;
		
		return hasCache.lookup(formulaID, arc.from) != HasCache.UNKNOWN;
	}

	public boolean lookupCachedResult(int formulaID, Arc arc) {
		int res = hasCache.lookup(formulaID, arc.from); 
		if(res == HasCache.UNKNOWN) {
			throw new IllegalArgumentException("No value cached for ther given arguments, check with hasCachedResultFor() beforehand");
		}
		
		return res == HasCache.TRUE;
	}
}
