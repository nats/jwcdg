package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Hierarchy;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * Does #2 subsume #3 in hierarchy #1?
 */
public class PredicateSubsumes extends Predicate {

	private static Logger logger = LogManager.getLogger(PredicateSubsumes.class);
	
	public PredicateSubsumes(Grammar g) {
		super("subsumes", g, false, 3, false);
	}

	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {

		AVM val1 = args.get(0).eval(lg, context, binding);
		AVM val2 = args.get(1).eval(lg, context, binding);
		AVM val3 = args.get(2).eval(lg, context, binding);
		
		AVMString sval1;
		AVMString sval2;
		AVMString sval3;

		if (val1 instanceof AVMString) {
			sval1 = (AVMString) val1;
		} else {
			if(val1 instanceof AVMError) {
				logger.warn("AVMError encountered: " +
						((AVMError)val1).getMessage());
			}
			throw new PredicateEvaluationException("invalid 1st arg of predicate 'subsumes'");
		}
		
		if (val2 instanceof AVMString) {
			sval2 = (AVMString) val2;
		} else {
			logger.warn("AVMError encountered: " +
					((AVMError)val2).getMessage());
			throw new PredicateEvaluationException("invalid 2nd arg of predicate 'subsumes'");
		}
		
		if (val3 instanceof AVMString) {
			sval3 = (AVMString) val3;
		} else {
			logger.warn("AVMError encountered: " +
					((AVMError)val3).getMessage());
			args.get(2).eval(lg, context, binding);
			throw new PredicateEvaluationException("invalid 3rd arg of predicate 'subsumes'");
		}
		
		Hierarchy h = grammar.findHierarchy(sval1.getString());
		
		if(h == null) {
			throw new PredicateEvaluationException("Hierarchy "+ sval1.getString()+ 
					" not found in predicate 'subsumes'");
		}

		/* find both sorts */
		// TODO #EXCEPTION_HANDLING not found warnings ?!
//		"WARNING: type `%s' not found in hierarchy `%s' in constraint `%s'\n",
//		"WARNING: type `%s' not found in hierarchy `%s' in constraint `%s'\n",

		/* return canned answer */
		return h.subsumes(sval2.getString(), sval3.getString());
	}

}
