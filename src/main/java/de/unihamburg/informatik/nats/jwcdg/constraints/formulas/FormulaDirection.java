package de.unihamburg.informatik.nats.jwcdg.constraints.formulas;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.AccessContext;
import de.unihamburg.informatik.nats.jwcdg.constraints.Direction;
import de.unihamburg.informatik.nats.jwcdg.constraints.VarInfo;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

public class FormulaDirection extends Formula {
	
	private Direction dir;
	private VarInfo   vi;
	private int vindex;
	
	public FormulaDirection(Direction dir, VarInfo vi) {
		this.dir= dir;
		this.vi = vi;
		this.vindex = vi.index;
	}
	
	@Override
	public boolean evaluate(LevelValue[] binding,
			Context context, LexemeGraph lg) {
		LevelValue lv = binding[vindex];
		
		return dir.subsumes(lv.getDirection());
	}
	
	@Override
	public void analyze(AccessContext context, Grammar g) {
		context.restrictDir(vindex, dir);
	}
	
	@Override
	public String toString() {
		return String.format("%s%s", vi.varname, dir.form);
	}

	@Override
	public void collectFeatures(List<Path> sofar) {
    }
	
	@Override
	public boolean check() {
		return true;
	}
	
	@Override
	public void collectHasInvocations(List<Formula> akku) {
    }
}
