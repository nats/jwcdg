package de.unihamburg.informatik.nats.jwcdg.constraints.functions;

import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.lattice.Arc;


/**
 * How many levels of sentence quotation does the word occupy?
 */
public class FunctionParens extends FunctionNesting {

	public FunctionParens(Grammar g) {
		super("parens", g);
	}


	@Override
	protected int nesting(Arc a) {
		return a.getParens();
	}
}
