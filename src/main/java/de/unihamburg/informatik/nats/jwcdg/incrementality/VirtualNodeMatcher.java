package de.unihamburg.informatik.nats.jwcdg.incrementality;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNet;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNetState;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.GraphemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.exceptions.TimeUpException;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.transform.Analysis;

public class VirtualNodeMatcher {

	public static final String VIRT_FLEXIBLE_LABEL_MATCHING = "virtFlexibleLabelMatching";
	public static final String INITIATION_TIMELIMIT = "initiationTimelimit";

	private static Logger logger = LogManager.getLogger(VirtualNodeMatcher.class);

	private GrammarEvaluator evaluator;
	private Configuration config;
	private volatile boolean interrupted;

	public VirtualNodeMatcher (GrammarEvaluator evaluator, Configuration config) {
		this.config = config;
		this.evaluator = evaluator;
	}
	
	public void interrupt() {
		interrupted = true;
	}
	
	public Analysis bestMatchingReplacement(ConstraintNetState state, Analysis a,
	                                        GraphemeNode newNode, double threshold) {
	
		double bestMatch = 0.0;
		Analysis result = null;
		long endTime;
		if (config.getInt(INITIATION_TIMELIMIT)>0)
			endTime = System.currentTimeMillis() + config.getInt(INITIATION_TIMELIMIT);
		else
			endTime = Long.MAX_VALUE;
		List<GraphemeNode> gns = state.getNet().getLexemeGraph().getGNs();
		
		int i =0;
		while(i< gns.size() && !gns.get(i).isVirtual()) {
			// skip all nonvirtual nodes
			i++;
		}
		
		for(int j= i; j< gns.size(); j++) {
			GraphemeNode virtNode = gns.get(j);
			try {
				Analysis current = unifyGNs(a, state, newNode, virtNode, threshold, endTime);
				if(current!= null && current.getBadness().hard ==0 && current.getBadness().getScore()> bestMatch) {
					result = current;
					bestMatch= result.getBadness().getScore();
				}
			} catch (TimeUpException e) {
				logger.info("Timeout reached, took " + Long.toString(System.currentTimeMillis()- endTime) + " ms too long");
				break; // no time left
			}
			if (System.currentTimeMillis()> endTime)
				break; // no time left
		}
		if(bestMatch > threshold) {
			logger.info("virtual node matching: found matching node!\n");
			return result;
		} else if(bestMatch > 0.0) {
			logger.info("virtual node matching:");
			logger.info(" best match < threshold");
			logger.info(String.format(" %1.2f < %1.2f", bestMatch, threshold));
			return null;
		} else {
			logger.info("no matching virtual node found\n");
			return null;
		}
	}
	
	/**
	 * returns the best of the Analyses where the virtual node is replaced by the new node.
	 * All edges where the virtual node is regent or dependent have to be replaced.
	 * There might be more than one way to replace them due to lexem ambiguity,
	 * but only the best one is returned.
	 * The virtual node itself is changed to unused status (under NIL with empty label)
	 * @throws TimeUpException 
	 */
	private Analysis unifyGNs(Analysis a, ConstraintNetState state, 
			GraphemeNode newNode, GraphemeNode virtNode, double threshold, long endTime) throws TimeUpException {

		if(		a.isUnusedVirtualNode(virtNode) ||
				! newNode.haveCompatibleLexemes(virtNode)) {
			return null;
		}

		logger.info("Trying to replace "+ virtNode.getArc().word);
		
		ConstraintNet net = state.getNet();

		List<Analysis> current  = new ArrayList<>();
		List<Analysis> previous = new ArrayList<>();
		previous.add(a);

		// for all slots (first levels first, as the main level probably prunes the most paths)
		ArrayList<Integer> visitedSlots = new ArrayList<>();
		for(int i= 0; i < net.getNoOfLevels(); i++) {
			for(int j=0; j< net.getNoOfTimepoints(); j++) {
				int slot = j*net.getNoOfLevels() + i;
				LevelValue lv = a.getSlots().get(slot);

				if(lv == null) {
					continue;
				}

				if(lv.getDependentGN().equals(newNode)) {
					// if the regent of the new Node is by chance the virtual node,
					// the new node would point to itself when replacing this lv
					// so skip it, the new node will get it's regent soon enough
					continue;
				}

				current= new ArrayList<>();

				for(Analysis curA : previous) {
					current.addAll(unifyGNsIter(curA, state, newNode, virtNode, lv, slot, threshold, visitedSlots, endTime));
				}

				previous = current;

				logger.info(String.format("Number of alternatives after %s (%s): %d",
						lv.getDependentGN().toString(), lv.getLevel().getId(), current.size()));

				if(current.isEmpty()) {
					// not replaceable
					return null;
				}
				visitedSlots.add(slot);
			}
		}

		Analysis result= null;
		for(Analysis curA : current) {
			if (System.currentTimeMillis()>endTime || interrupted)
				throw new TimeUpException();
			//curA.judge(evaluator);
			assert curA.check(evaluator, 3, new ArrayList<>());
			if(result == null || curA.getBadness().compare(result.getBadness())) {
				result = curA;
			}
		}

		return result;
	}
	
	/**
	 * Subroutine to find all analyses where a certain edge from the analysis is replaced.
	 * If the node to be replaced is neither regent nor dependent of the edge,
	 * replace nothing and return a list containing just the old analysis
	 */
	private List<Analysis> unifyGNsIter(Analysis a, ConstraintNetState state, 
			GraphemeNode newNode, GraphemeNode virtNode,
			LevelValue lv, int slot, double threshold, List<Integer> visitedSlots,
			long endTime)
			throws TimeUpException
			{
		
		List<Analysis> result= new ArrayList<>();

		GraphemeNode dep = lv.getDependentGN();
		GraphemeNode reg = lv.getRegentGN();
		GraphemeNode dep2, reg2;
		
		List<LexemeNode> availableLNs = new ArrayList<>(state.getNet().getLexemeGraph().getNodes());
		
		// we only have to replace lv if either end is the virtual node to be replaced 
		if(dep.equals(virtNode)) {
			// hit, the VN to be replaced is the dependent of lv
			
			// first we check which lexemenodes are available given the requirements of the other levelvalues
			// we only account for llvs before this slot, as the others ( especially this slot, that WILL)
			//  might still change
			for(int i : visitedSlots) {
				LevelValue lv2 = a.getSlots().get(i);
				
				// only the regent graphemNode of lv and the newNode are relevant currently
				// are the ends of lv2 one of them?
				dep2 = lv2.getDependentGN();
				reg2 = lv2.getRegentGN();

				if(dep2.equals(newNode)) {//|| (reg && dep2 == reg)) {
					for(LexemeNode ln : dep2.getLexemes()) {
						if(availableLNs.contains(ln) && !lv2.getDependents().contains(ln)) {
							availableLNs.remove(ln);
						}
					}
				}

				//if( (reg && reg2 == reg) || reg2 == newNode ) {
				if( reg2.equals(newNode)) {
					for(LexemeNode ln : reg2.getLexemes()) {
						if(availableLNs.contains(ln) && !lv2.getRegents().contains(ln)) {
							availableLNs.remove(ln);
						}
					}
				}
				if (endTime < System.currentTimeMillis() || interrupted)
					throw new TimeUpException();
			}

			List<LevelValue> candidates= findSubstitutes(lv, newNode, 
					state.getDomain(state.getNet().getIndexOf(newNode, lv.getLevel())), 
					availableLNs, false);
			for(LevelValue substitute : candidates ) {
				
				if(threshold > state.getLvLimit(substitute)) {
					continue; // levelvalue is too bad, abort here
				}
				if (endTime < System.currentTimeMillis() || interrupted)
					throw new TimeUpException(); // time is up, result will be discarded by caller
				Analysis a2= new Analysis(a);
				a2.insertLv(substitute, state, evaluator);
				// the replaced node needs a new edge
				LevelValue unused= state.findUnusedLv(slot, null);
				a2.insertLv(unused, state, evaluator);

				result.add(a2);
			}
		} else if(reg.equals(virtNode)) {
			// hit, the VN to be replaced is the regent of lv
			
			// first we check which lexemenodes are available given the requirements of the other levelvalues
			// we only account for llvs before this slot, as the others ( especially this slot, that WILL)
			//  might still change
			for(int i : visitedSlots) {
				LevelValue lv2 = a.getSlots().get(i);
				
				// only the dependent graphemNode of lv and the newNode are relevant currently
				// are the ends of lv2 one of them?
				dep2 = lv2.getDependentGN();
				reg2 = lv2.getRegentGN();

				//if(dep2 == dep || dep2 == newNode){
				if(dep2 == newNode){
					for(LexemeNode ln : dep2.getLexemes()) {
						if(! lv2.getDependents().contains(ln)) {
							availableLNs.remove(ln);
						}
					}
				}

				//if(reg2 == dep || reg2 == newNode) {
				if( reg2 == newNode) {
					for(LexemeNode ln : reg2.getLexemes()) {
						if(! lv2.getRegents().contains(ln)) {
							availableLNs.remove(ln);
						}
					}
				}
				if (endTime < System.currentTimeMillis() || interrupted)
					throw new TimeUpException(); // time is up, result will be discarded by caller
			}

			List<LevelValue> candidates= findSubstitutes(lv, newNode, 
					state.getDomain(lv.getIndex()), availableLNs, true);
			for(LevelValue substitute : candidates) {

				if(threshold > state.getLvLimit(substitute)) {
					continue; // levelvalue is too bad, abort here
				}
				if (endTime < System.currentTimeMillis() || interrupted)
					throw new TimeUpException(); // time is up, result will be discarded by caller
				Analysis a2= new Analysis(a);
				//vectorSetElement(a2.slots,substitute,slot);
				// todo is the lv compatible with current lexem choices?
				a2.insertLv(substitute, state, evaluator);

				result.add(a2);
			}
		} else {
			// nothing to substitute
			result.add(a);
		}
		return result;
	}
	
	/**
	 * returns all levelValues with same regent and label as lv,
	 * but with newNode as dependent
	 * 
	 * is regent is true it instead 
	 * returns all levelvalues with same dependent and label as lv,
	 * but with newNode as regent
	 * 
	 * domain has to be a list of levelvalues with the intended dependent
	 */
	private List<LevelValue> findSubstitutes(LevelValue lv, GraphemeNode newNode, 
			List<LevelValue> domain, List<LexemeNode> availableLNs,
			boolean regent) {
		List<LevelValue> candidates= new ArrayList<>();
		boolean available;

		for(LevelValue candidate : domain) {
			if(!candidate.getRegentGN().equals(regent?newNode:lv.getRegentGN())) {
				continue;
			}

			if(lv.getLevel().getNo() == 0 && config.getFlag(VIRT_FLEXIBLE_LABEL_MATCHING)) {
				if(!evaluator.getGrammar().isCompatibleLabel(lv.getLabel(), candidate.getLabel(), config))
					continue;
			} else {
				if(!lv.getLabel().equals(candidate.getLabel()))
					continue;
			}

			available = false;
			for(LexemeNode ln : candidate.getDependents()) {
				if(availableLNs.contains(ln)) {
					available = true;
					break;
				}
			}
			if (!available) {
				// none of the dependent lexemes is available
				continue;
			}

			if(candidate.getRegentGN().spec()) { // only check regent lexemes if the regent is specified
				available = false;
				for(LexemeNode ln : candidate.getRegents()) {
					if(availableLNs.contains(ln)) {
						available = true;
						break;
					}
				}
				if (!available) {
					// none of the regent lexemes is available
					continue;
				}
			}

			candidates.add(candidate);
		}
		
		return candidates;
	}
}
