package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;


/**
 * Is the current input sequence marked as finished or is it partial?
 */
public class PredicatePartial extends Predicate {

	public static final String USE_PREDICATE_PARTIAL = "usePredicatePartial";
	
	public PredicatePartial(Grammar g) {
		super("partial", g, false, 0, false);
	}

	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {	
		if(!lg.getConfig().getFlag(USE_PREDICATE_PARTIAL)) {
			// this predicate is disabled, default to false
			return false;
		}
		
		return !lg.isComplete();
	}

}
