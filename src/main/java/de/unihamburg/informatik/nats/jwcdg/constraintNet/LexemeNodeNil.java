package de.unihamburg.informatik.nats.jwcdg.constraintNet;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.avms.Path.PseudoFeature;
import de.unihamburg.informatik.nats.jwcdg.input.LexiconItem;
import de.unihamburg.informatik.nats.jwcdg.lattice.Arc;

public class LexemeNodeNil extends LexemeNode {

	public LexemeNodeNil(GraphemeNode gn) {
		super(-1, gn, null);
	}

	@Override
	public boolean isNil() {
		return true;
	}
	
	@Override
	public AVM getFeature(Path path) {
		if(path != null && path.isPseudoFeature()) {
			PseudoFeature pf = path.toPseudoFeature();
			if(pf.equals(PseudoFeature.id))
				return new AVMLexemNode(this);
		}
		return new AVMError(String.format("Im nil, the feature %s is not specified!",
		                    path == null ? "[null]" : path.toString()));
	}
	
	@Override
	public boolean overlaps(LexemeNode lnb) {
		return false;
	}
	
	@Override
	public Arc getArc() {
		throw new RuntimeException("Don't ask me, I'm not specified!");
	}
	
	@Override
	public LexiconItem getLexeme() {
		throw new RuntimeException("Don't ask me, I'm not specified!");
	}
	
	@Override
	public String toString() {
		return "nil"; 
	}
	

}
