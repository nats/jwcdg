package de.unihamburg.informatik.nats.jwcdg.gui;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.EventObject;

import javax.swing.JTree;
import javax.swing.event.EventListenerList;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;
import de.unihamburg.informatik.nats.jwcdg.parse.Word;

/**
 * The subcomponent of the DepTreeDisplayInteractor that handles the
 * interactions with the navigation tree on the left side of the Display. The
 * concrete classes are either the DepTreeNavigator or the AnnoNavigator.
 * 
 * @author Sven Zimmer and Arne Köhn
 * 
 */

abstract class AbstractNavigator {
	protected NavNode _root = new NavNode("root", true);
	protected DefaultTreeModel _treeModel = new DefaultTreeModel(_root);
	protected JTree _tree = new JTree(_treeModel);
	protected NavNode _currentNode = null;
	private ArrayList<NavNode> _allNodes = new ArrayList<>();
	protected EventListenerList _listeners = new EventListenerList();

	public AbstractNavigator() {
		init();
	}

	protected void init() {
		_tree.setToggleClickCount(1);
		_tree.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				NavNode navNode =
							(NavNode) _tree.getLastSelectedPathComponent();
				if (navNode != null) {
					if (! navNode._isDir) {
						_currentNode = navNode;
						notifySelection(new NavSelectEvent(this, navNode));
					}
				}
			}
		});
	}

	protected class NavNode extends DefaultMutableTreeNode {
		private static final long serialVersionUID = 1L;
		public boolean _isDir = false;

		protected NavNode(String name, Boolean isDir) {
			super(name);
			_isDir = isDir;
		}
	}

	public class NavSelectEvent extends EventObject {
		private static final long serialVersionUID = 1L;
		NavNode _navNode;

		public NavSelectEvent(Object source, NavNode navNode) {
			super(source);
			_navNode = navNode;
		}
	}

	interface NavSelectListener extends EventListener {
		void navNodeSelected(NavSelectEvent e);
	}

	public void addNavSelectListener(NavSelectListener listener) {
		_listeners.add(NavSelectListener.class, listener);
	}

	protected synchronized void notifySelection(NavSelectEvent event) {
		for (NavSelectListener l : _listeners.getListeners(NavSelectListener.class))
			l.navNodeSelected(event);
	}

	public static String getSentence(DecoratedParse parse) {
		StringBuilder result = new StringBuilder();
		if (parse != null) {
			for (Word word : parse.getWords()) {
				result.append(word.word).append(" ");
			}
		}
		return result.toString();
	}

	public boolean hasDir(String name) {
		boolean result = false;
		NavNode node = getDir(name);
		result = (node != null);
		return result;
	}

	public NavNode getDir(String name) {
		NavNode result = null;
		if (_root.getChildCount() > 0) {
			NavNode node = (NavNode) _root.getFirstChild();
			while (node != null) {
				if (node._isDir && node.toString().equals(name)) {
					result = node;
				}
				node = (NavNode) node.getNextSibling();
			}
		}
		else {
		}
		return result;
	}

	protected void insertNode(NavNode node, NavNode dirNode) {
		_treeModel.insertNodeInto(node, dirNode, dirNode.getChildCount());
		_tree.scrollPathToVisible(new TreePath(node.getPath()));
		_currentNode = node;
		_allNodes.add(node);
	}

	public void makeDir(String name) {
		if (! hasDir(name)) {
			NavNode node = new NavNode(name, true);
			_treeModel.insertNodeInto(node, _root, _root.getChildCount());
			try {
				_tree.scrollPathToVisible(new TreePath(node.getPath()));
			}
			catch (NullPointerException e) {
				e.printStackTrace();
			}
		}
	}

	public int getDirSize(String name) {
		int result = 0;
		if (hasDir(name)) {
			result = getDir(name).getChildCount();
		}
		return result;
	}

	public int getRootSize() {
		return _root.getChildCount();
	}

	public void clearTree() {
		_root.removeAllChildren();
		_treeModel.reload();
		_allNodes.clear();
	}

	public void removeDir(String directory) {
		if (hasDir(directory)) {
			NavNode dirNode = getDir(directory);
			_treeModel.removeNodeFromParent(dirNode);
			if (_currentNode != null) {
				if (_currentNode.getParent().equals(dirNode)) {
					_currentNode = null;
				}
			}
		}
	}

	public void selectCurrent() {
		if (_currentNode != null) {
			TreePath currentPath = new TreePath(_currentNode.getPath());
			_tree.scrollPathToVisible(currentPath);
		}
	}

	public void setTree(JTree tree) {
		_tree = tree;
	}

	public JTree getTree() {
		return _tree;
	}

	public NavNode getCurrentNode() {
		return _currentNode;
	}

	public void setCurrentNode(NavNode currentNode) {
		_currentNode = currentNode;
	}

	public ArrayList<NavNode> getAllNodes() {
		return _allNodes;
	}

	public void setAllNodes(ArrayList<NavNode> allNodes) {
		_allNodes = allNodes;
	}
}
