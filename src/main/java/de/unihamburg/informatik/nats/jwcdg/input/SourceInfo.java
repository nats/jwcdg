package de.unihamburg.informatik.nats.jwcdg.input;


public class SourceInfo implements java.io.Serializable {

	/**
	 * SourceInfo is not used at the moment but nearly everything is in place
	 * to use it.
	 */
	private static final long serialVersionUID = 7492394670307667998L;
	private String filename;		   /* source file */
	private int lineNo;			       /* line number in source file */
	
	public SourceInfo() {
		filename = "*USER*";
		lineNo = 0;
	}
	
	public SourceInfo(String filename, int lineNo) {
		this.filename = filename;
		this.lineNo = lineNo;
	}

	public String getFilename() {
		return filename;
	}


	public int getLineNo() {
		return lineNo;
	}
	
	@Override
	public String toString() {
		return filename + ":" + lineNo;
	}

}
