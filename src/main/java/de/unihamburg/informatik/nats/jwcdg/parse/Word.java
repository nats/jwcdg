package de.unihamburg.informatik.nats.jwcdg.parse;

import java.io.Serializable;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMConj;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMFlat;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.incrementality.virtualNodes.VirtualNodeSpec;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Hierarchy;
import de.unihamburg.informatik.nats.jwcdg.input.LexiconItem;
import de.unihamburg.informatik.nats.jwcdg.input.SourceInfo;
import io.gitlab.nats.deptreeviz.WordInterface;

/**
 * representation of a word in a parse
 * 
 * @author the CDG-Team
 *
 */
public class Word implements WordInterface, Comparable<Word>, Serializable {
	
    private static final long serialVersionUID = 1L;
    public final int from;           /* Interval in the                       */
	public final int to;             /* lattice graph                         */

	public final String  word;           /* Which word does this node represent?  */
	public final String  description;    /* == description of the currently corresponding LexiconItem */
	
	private Map<String, String> features;
	
	public Word(int from, int to, String word, String description, Map<String, String> features) {
		this.from = from;
		this.to   = to;
		this.word = word;
		this.description = description;
		this.features= new HashMap<>(features);
	}
	
	public Word(LexemeNode ln, List<String> annoFeats, int offset) {
		this.from = ln.getArc().from + offset;
		this.to   = ln.getArc().to   + offset;
		this.word = ln.getArc().word;
		this.description = ln.getLexeme().getDescription();
		this.features = new HashMap<>(
				ln.getLexeme().getDistinctiveFeatures().size());
		for(String feat: annoFeats) {
			Path p = new Path(feat, null);
			AVM val = ln.getFeature(p);
			if(!(val instanceof AVMError))
				features.put(feat, val.toString());
		}
		for(Path p: ln.getLexeme().getDistinctiveFeatures()) {
			if(annoFeats.contains(p.toString()))
				continue;
			features.put(p.toString(), ln.getFeature(p).toString());
		}
		if(ln.isVirtual()) {
			features.put("virtual", "true");
		}
	}

	@Override
	public int compareTo(Word o) {
		if(this.from < o.from) 
			return -1;
		if(this.from > o.from) 
			return  1;
		return 0;
	}

	public String getWord() {return word;}

	public Set<String> getDefinedFeatureNames() {
		return features.keySet();
	}
	
	public String getFeature(String name) {
		return features.get(name);
	}
	
	@Override
	public String toString() {
		return String.format("(%2d-%2d) %s", from, to, word);
	}
	

	/**
	 * How well does this match to an lexicon item, featurewise?
	 * returns the number of differences
	 */
	public int differencesToLI(LexiconItem li, Hierarchy h) {
		int result = 0;
	
		for(String feat : features.keySet()) {
			if(feat.equals("virtual") || feat.equals("oldpos"))
				continue;
			
			String val = features.get(feat);
			
			AVM v = li.getFeature(new Path(feat, null));
			if (v instanceof AVMError) {
				result++;
			} else {
				int i = compareFeatures(val, v.toString(), h);
				result += i;
			}
		}
	
		/* penalize variants that differ in case */
		if (!word.equals(li.getWord())) {
			result++;
		}
	
		return result;
	}
	
	/**
	   Are these two values similar enough to count as a `hit' when
	   verifying parse results?

	   Returns 0 if they values are identical, 1 if they are different
	   but compatible according to H, and 2 if they are neither.
	*/
	private int compareFeatures(String a, String b, Hierarchy h) {
		String bottom = "bot";

		/* `bot' matches everything, no need to even look at h */
		if(a.equals(bottom) && b.equals(bottom)) {
			return 0;
		}
		if(a.equals(bottom) || b.equals(bottom)) {
			return 1;
		}

		/* `nom' matches `nom' */
		if(a.equals(b)) {
			return 0;
		}

		/* `nom_dat_acc' should match `nom',
	     but that only works if the user named a hierarchy to use for lookup */
		if(h!= null) {
			if(h.contains(a) && h.contains(b)) {
				if ( h.subsumes(a, b) || h.subsumes(b, a)) {
					return 1;
				}
			}
		}

		/* nope, too different */
		return 2;
	}
	
	/**
	 * generates a virtualNodeSpec that has a lexeme corresponding to this word description
	 */
	public VirtualNodeSpec toVirtualNodeSpec() {
		if( !features.containsKey("virtual") || !features.get("virtual").equals("true")) {
			return null;
		}
		
		List<AVM> avms = new ArrayList<>();
		
		for(String feat : features.keySet()) {
			if(feat.equals("virtual"))
				continue;
			
			String val = features.get(feat);
			
			AVMNode node = new AVMNode(feat, new AVMString(val));
			avms.add(node);
		}
		
		AVMConj feats   = new AVMConj(avms);
		avms = new ArrayList<>(1);
		avms.add(feats);
		VirtualNodeSpec spec = new VirtualNodeSpec(word, avms, 1, new SourceInfo());
		return spec;
	}

	public LexiconItem toLexiconItem(Configuration config) {
		Map<Path, AVM> map = new HashMap<>();
		for(String key : features.keySet()) {
			
			// this is not part of the lexicon entry
			if(key.equals("oldpos"))
				continue;
			
			String val= features.get(key);
			try {
				NumberFormat numFormat = NumberFormat.getInstance(
						new Locale(config.getString(AVMNumber.NUMBER_FORMAT_LOCALE)));
				double d = numFormat.parse(val).doubleValue();
				map.put(new Path(key, null), new AVMNumber(d));
			} catch (ParseException e) {
				map.put(new Path(key, null), new AVMString(val));
			}
		}
		AVM feats = new AVMFlat(map);
		return new LexiconItem(word, feats, new SourceInfo());
	}
}
