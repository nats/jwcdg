package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * is #1 a start node?
 */
public class PredicateStart extends Predicate {

	public PredicateStart(Grammar g) {
		super("start", g, false, 1, false);
	}

	@Override
	public boolean eval(List<Term> args, LevelValue[] binding, 
			LexemeGraph lg, Context context, int formulaID) {
		AVM val = args.get(0).eval(lg, context, binding);
		AVMLexemNode lnval;
		if (val instanceof AVMLexemNode) {
			lnval = (AVMLexemNode) val;
		} else {
			throw new PredicateEvaluationException("ERROR: argument of predicate `start' not a lexeme node");
		}
		LexemeNode ln = lnval.getLexemeNode();

		/* virtual nodes are not in the lattice, so we cannot ask it about them */
		if(ln.isVirtual()) {
			return false;
		}
		
		/* detect word that is truly the start */
		if (ln.getArc().from == lg.getMin()) {
			return true;
		}

		/* detect word that is the start of an embedded clause */
		if (lg.getLattice().prevArc(ln.getArc()).isSentenceBoundary()) {
			return true;
		}

		return false;
	}
}
