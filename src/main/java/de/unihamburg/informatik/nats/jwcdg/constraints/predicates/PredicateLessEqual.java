package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import de.unihamburg.informatik.nats.jwcdg.input.Grammar;

public class PredicateLessEqual extends PredicateNumericalComparison {

	public PredicateLessEqual(Grammar g) {
		super("<=");
	}
	@Override
	protected boolean compareNumbers(double a, double b) {
		return a <= b;
	}

}
