package de.unihamburg.informatik.nats.jwcdg.constraints.functions;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * Returns the normalized acoustic score.
 */
public class FunctionAcoustics extends Function {

	public FunctionAcoustics(Grammar g) {
		super("acoustic", g, 1);
	}

	@Override
	public AVM eval(List<Term> args, Context context, LexemeGraph lg,
			LevelValue[] binding) {

		AVM val = args.get(0).eval(lg, context, binding);

		if (val instanceof AVMLexemNode) {
			LexemeNode ln = ((AVMLexemNode) val).getLexemeNode(); 

			if (ln == null || ln.getArc() == null) {
				return new AVMError("WARNING: argument of function `acoustics' not complete");
			}

			return new AVMNumber(ln.getArc().getPenalty());
			
		} else {
			return new AVMError("WARNING: argument of function `acoustics' not an id");
		}
	}
}
