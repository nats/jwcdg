package de.unihamburg.informatik.nats.jwcdg.constraintNet;

import java.util.ArrayList;
import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.lattice.Arc;

public class GraphemeNodeNonspec extends GraphemeNode {

	List<LexemeNode> nslns;

	public GraphemeNodeNonspec(LexemeGraph lg) {
		super(null);
		nslns = new ArrayList<>();
		nslns.add(new LexemeNodeNonspec(this, lg));
	}

	@Override
	public boolean isSpecified() {
		return false;
	}

	@Override
	public boolean isNonspec() {
		return true;
	}

	@Override
	public void addLexeme(LexemeNode ln) {
		throw new RuntimeException("I'm nonspec, you can't add a lexeme node to me!");
	}

	@Override
	public Arc getArc() {
		throw new RuntimeException("Don't ask me, I'm not specified!");
	}

	@Override
	public List<LexemeNode> getLexemes() {
		return nslns;
	}

	@Override
	public int getNo() {
		return -2;
	}

	@Override
	public void setNo(int no) {
		throw new RuntimeException("I'm nonspec, don't change my number!");
	}

	@Override
	public String toString() {
		return "nonspec";
	}
}
