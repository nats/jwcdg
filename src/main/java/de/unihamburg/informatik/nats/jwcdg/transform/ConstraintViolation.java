package de.unihamburg.informatik.nats.jwcdg.transform;

import java.io.Serializable;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraints.Constraint;

public class ConstraintViolation implements Comparable<ConstraintViolation>, Serializable {

	private static final long serialVersionUID = 1L;
    /** violated constraint */
	private Constraint constraint;   
	/** holds the penalty of this particular
     * instance of \b constraint (recall that a
     * constraint declaration specifies an entire
     * set of constraints, possibly with variable
     * penalties). 
     */
	private double penalty;          
	private int nodeBindingIndex1;   /* holds the position of the LV causing
	                                    the conflict, as calculated by
	                                    lvIndex(). */
	/** first node binding */
	private LevelValue lv1;          
	private int nodeBindingIndex2;   /* holds the position of the LV causing
	                                    the conflict, as calculated by
	                                    lvIndex(). */
	/** second node binding, maybe empty */
	private LevelValue lv2;
	
//	private int nodeBindingIndex3;   /* holds the position of the LV causing
//	                                    the conflict, as calculated by
//	                                    lvIndex(). */
	/** third node binding, maybe empty */
	private LevelValue lv3;          
	
	public ConstraintViolation(Constraint c, LevelValue lva, LevelValue lvb, LevelValue lvc, double penalty) {
		this.constraint = c;
		this.lv1 = lva;
		this.lv2 = lvb;
		// this.lv3 = lvc; 
		this.penalty = penalty;
		
		if(lv1.getIndexWRTNet() == -1) {
			lv1 = new LevelValue(lv1);
		}
		if(lv2 != null && lv2.getIndexWRTNet() == -1) {
			lv2 = new LevelValue(lv2);
		}
		
		// Maybe someday someone wants to use ternary constreints. If so, use this code.
		//if(lv3 != null && lv3.getIndexWRTNet() == -1) {
		//	lv3 = new LevelValue(lv3);
		//}
		
		nodeBindingIndex1 = lv1.getIndex();
		nodeBindingIndex2 = lv2 !=null ? lv2.getIndex() : -1;
		//nodeBindingIndex2 = lv3 !=null ? lv3.getIndex() : -1;
		/* Note that the default initialization to -1 is meaningful. At several
		   points in the program, cv structures are compared by comparing their
		   fields nodeBindingIndex1 and nodeBindingIndex2. If nodeBindingIndex2
		   were left undefined for unary constraints, two identical unary
		   violations might get classified as different.  */
	}
	
	/**
	 * copy constructor
	 */
	public ConstraintViolation(ConstraintViolation orig) {
		this(orig.constraint, orig.lv1, orig.lv2, orig.lv3, orig.penalty);
	}

	public double getPenalty() {
		return penalty;
	}

	/**
	   Constraint violations should be sorted first by penalty, then by arity,
	   finally by natural order.

	   This function compares two conflicts. It returns \b true
	    - if \b a has the lower penalty
	    - otherwise, if \b a is unary and \b b is binary
	    - otherwise, if the first LV in \b a precedes the first lv in
	     \b b according to natural order.
	 */
	@Override
	public int compareTo(ConstraintViolation other) {

		int indexa, indexb;

		/* compare by penalty */
		if (this.penalty < other.penalty)
			return -1;
		if (this.penalty > other.penalty)
			return 1;

		/* compare by arity */
		if (this.lv2 == null && other.lv2 != null)
			return -1;
		if (this.lv2 != null && other.lv2 == null)
			return 1;

		if (this.lv3 == null && other.lv3 != null)
			return -1;
		if (this.lv3 != null && other.lv3 == null)
			return 1;

		/* compare names */
		indexa = this.constraint.getId().compareTo(other.constraint.getId());
		if (indexa < 0)
			return 1;
		if (indexa > 0)
			return -1;

		/* compare by natural order */
		indexa = this.lv1.getIndex();
		indexb = other.lv1.getIndex();
		if (indexa < indexb)
			return -1;
		else if (indexa > indexb)
			return 1;

		return 0;
	}

	public Constraint getConstraint() {
		return constraint; 		
	}

	public LevelValue getLV1() {
		return lv1;
	}
	
	public LevelValue getLV2() {
		return lv2;
	}
	
	public LevelValue getLV3() {
		return lv3;
	}

	public int getNodeBindingIndex1() {
		return nodeBindingIndex1;
	}

	public int getNodeBindingIndex2() {
		return nodeBindingIndex2;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this==obj)
			return true;
		
		if (obj instanceof ConstraintViolation) {
			ConstraintViolation other = (ConstraintViolation) obj;
			if(!this.constraint.equals(other.constraint))
				return false;
			if(!this.lv1.equals(other.lv1))
				return false;
			
			if(this.lv2== null || other.lv2==null) {
				if(this.lv2 != other.lv2)
					return false;
			} else {
				if(!this.lv2.equals(other.lv2))
					return false;
				if(this.lv3== null || other.lv3==null) {
					if(this.lv3 != other.lv3)
						return false;
				} else {
					if(!this.lv3.equals(other.lv3))
						return false;
				}
			}
			return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return String.format("Violation: %s - %1.3f", constraint.getId(), penalty);
	}

	/**
	 * more verbose version of toString 
	 * @return
	 */
	public String print() {
		if (nodeBindingIndex2 >= 0) {
			return String.format("%03d : %03d : %4.3e : %s",
					nodeBindingIndex1, nodeBindingIndex2,
					penalty, constraint.getId());
		} else {
			return String.format("      %03d : %4.3e : %s",
					nodeBindingIndex1, penalty, constraint.getId());
		}
	}
		
	@Override
	public int hashCode() {
		// we have no clue here about how many constraints or levelvalues there are
		// but as hash colisions are not very critical here, 
		// we simply take some arbitrary prime numbers as multipliers 
		return constraint.getNo() +
				lv1.hashCode() * 23 +
				(lv2!=null?lv2.hashCode()*47:0)+
				(lv3!=null?lv3.hashCode()*103:0);
	}
}
