package de.unihamburg.informatik.nats.jwcdg.evaluation;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;

/**
 * A cache for the evaluation of all non context sesitive unary and binary constraints
 * cached by participating levelvalues  
 * 
 * @author the CDG-Team
 *
 */
public class ScoreCache implements Serializable{

	private static final long serialVersionUID = 1L;

    public static final String USE_SCORECACHE = "useScorecache";

	private Map<LevelValue, GrammarEvaluationResult>                  cacheUna;
	private Map<LevelValue, Map<LevelValue, GrammarEvaluationResult>> cacheBin;
	
	public ScoreCache() {
		cacheUna = new ConcurrentHashMap<>();
		cacheBin = new ConcurrentHashMap<>();
	}

	/**
	 * retain a score previously cached
	 * better check whether the score was actually cached before via hasEntryFor() 
	 */
	public double getScoreFor(LevelValue lv1) {
		return cacheUna.get(lv1).getScore();
	}
	
	/**
	 * retain a score previously cached
	 * better check whether the score was actually cached before via hasEntryFor() 
	 */
	public double getScoreFor(LevelValue lv1, LevelValue lv2) {
		return cacheBin.get(lv1).get(lv2).getScore();
	}
	
	/**
	 * cache a result for a given set of levelvalues
	 * 
	 * @param lv1
	 * @param lv2
	 * @param result
	 */
	public void setResultFor(LevelValue lv1, LevelValue lv2, GrammarEvaluationResult ger) {
		if(!cacheBin.containsKey(lv1)) {
			cacheBin.put(lv1, new ConcurrentHashMap<>());
		}
		
		cacheBin.get(lv1).put(lv2, ger);
	}
	
	/**
	 * cache a result for a givenlevelvalue
	 * 
	 * @param lv1
	 * @param result
	 */
	public void setResultFor(LevelValue lv1, GrammarEvaluationResult ger) {		
		cacheUna.put(lv1, ger);
	}
	
	public GrammarEvaluationResult getResultFor(LevelValue lv1) {
		return cacheUna.get(lv1);
	}
	
	public GrammarEvaluationResult getResultFor(LevelValue lv1, LevelValue lv2) {
		return cacheBin.get(lv1).get(lv2);
	}
	
	/**
	 * whether the cache contains a score for the given lvs
	 */
	public boolean hasEntryFor(LevelValue lv1) {
		return cacheUna.containsKey(lv1);
	}
	
	/**
	 * whether the cache contains a score for the given lvs
	 * 
	 * @param lv1
	 * @param lv2
	 * @return
	 */
	public boolean hasEntryFor(LevelValue lv1, LevelValue lv2) {
		if(! cacheBin.containsKey(lv1)) {
			return false;
		}
		if(! cacheBin.get(lv1).containsKey(lv2)) {
			return false;
		}
		return true;
	}
	    
	/**
	 * delete all entries
	 */
	public void clear() {
		cacheUna.clear();
		cacheBin.clear();
	}
	
}
