package de.unihamburg.informatik.nats.jwcdg.gui;

import de.unihamburg.informatik.nats.jwcdg.Parser;
import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.evaluation.CachedGrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.gui.AbstractDisplay.PreferencesDisplay;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;
import de.unihamburg.informatik.nats.jwcdg.input.LexiconItem;
import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;
import de.unihamburg.informatik.nats.jwcdg.transform.ConstraintViolation;
import io.gitlab.nats.deptreeviz.DepTree;
import io.gitlab.nats.deptreeviz.DepTreeNode;
import io.gitlab.nats.deptreeviz.PopupEvent;
import org.w3c.dom.Element;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.AbstractTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * The component of the AbstractViewer tool that handles interactions with the
 * GUI has the subcomponents DepTreeInteractor for the interactions with the
 * DepTree itself and AbstractNaviagator for the interactions with the tree that
 * helps to navigate between different parses. The concrete classes are either
 * the DepTreeDisplayInteractor or the AnnoDisplayInteractor.
 * 
 * @author Sven Zimmer and Arne Köhn
 * 
 */

abstract class AbstractDisplayInteractor {

	// true if the constraints model is initialized so the resulting value
	// changes can be ignored by handleValueChanged
	private boolean _isSettingConstraintsModel = false;
	protected boolean _hideLastRow = false;
	private int _selectedRow = -1;
//	private Integer _queueId = 0;
	protected ArrayList<Integer> _queue = new ArrayList<>();
	protected boolean _keepSelectedRow = false;
	protected String _helpText = null;

	// adds the category of a word to its description if it is missing
	// this happens because only features that are different are listed
	protected String amendDescription(String description, LexiconItem li) {
		String result = description;
		String amendment =
					"_" + li.getFeature(Path.fromSequence("cat")).toString();
		if (! result.contains(amendment)) {
			result = result + amendment;
		}
		return result;
	}

	protected void drawParse(DepTreeInteractor dtI,
				DecoratedParse navParse,
				Map<Integer, ArrayList<String>> markedNodes) {
		dtI.setDecParse(navParse);
		dtI._selection.setHasTarget(false);
		dtI._dt.setMarkedNodes(markedNodes);
		dtI.draw(false);
	}

	// abstract DecoratedParse decorate(Parse parse);

	public DecoratedParse decorate(Parse parse, Parser parser) {
		DecoratedParse result = null;
		if (parse != null) {
//			System.out.println(parse.toString());
			Lexicon lexicon = null;
			if (! parser.getConfig().getFlag("DisplayNoLexicon")) {
				System.out.println("using Lexicon for DisplayInteractor");
				lexicon = parser.getLexicon();
			}
			result =
				parse.decorate(new CachedGrammarEvaluator(parser.getGrammar(), parser.getConfig()),
							   lexicon,
							   parser.getConfig());
		}
		return result;
	}

	/**
	 * This listener is used for the items of a menu that pops up when certain
	 * words in a DepTree are clicked, such as labels of the SYN level
	 * 
	 * @author 3zimmer
	 * 
	 */
	protected abstract class TreeMenuListener implements ActionListener {
		public DepTreeNode _node;
		public String _level;
		public ArrayList<Element> _targets;
		private Map<String, LexemeNode> _lexiconItems;

		public void setFields(DepTreeNode node,
					String level,
					ArrayList<Element> targets) {
			_node = node;
			_level = level;
			_targets = targets;
		}

		void setLexiconItems(Map<String, LexemeNode> _lexiconItems) {
			this._lexiconItems = _lexiconItems;
		}

		Map<String, LexemeNode> getLexiconItems() {
			return _lexiconItems;
		}
	}

	/**
	 * Reacts to the selection of a row in the constraints table by marking the
	 * nodes and levels in the DepTree that the constraint refers to
	 * 
	 */
	protected void handleValueChanged(ListSelectionEvent e,
				DepTreeInteractor dtI,
				AbstractDisplay gui) {
		// if the values of the table are changing or it is being initialized,
		// then the marked levels don't need to be changed
		if (e.getValueIsAdjusting() || _isSettingConstraintsModel) {
			return;
		}
		JTable conTable = gui.getConstraintsTable();
		ConstraintsModel conModel = (ConstraintsModel) conTable.getModel();
		// set all nodes at all levels as unmarked
		for (ArrayList<String> markedLevels : dtI._dt.getMarkedNodes().values()) {
			markedLevels.clear();
		}
		for (int row : conTable.getSelectedRows()) {
			DepTreeNode node1 = conModel.getNode1(row);
			String level1 = conModel.getLevel1(row);
			DepTreeNode node2 = conModel.getNode2(row);
			String level2 = conModel.getLevel2(row);
			markNode(dtI, node1, level1);
			if (level2 != null) {
				markNode(dtI, node2, level2);
			}
		}
		dtI._dt.highlightNodes();
		dtI._dt.refreshDisplay();
		setSelectedRow(conTable.getSelectedRow(), gui);
	}

	/**
	 * Mark a node at a level
	 *
	 */
	private void
				markNode(DepTreeInteractor dtI, DepTreeNode node, String level) {
		ArrayList<String> markedLevels =
					dtI._dt.getMarkedNodes().get(node.getIndex());
		if (markedLevels == null) {
			markedLevels = new ArrayList<>();
			dtI._dt.getMarkedNodes().put(node.getIndex(), markedLevels);
			node.setMarkedLevels(markedLevels);
		}
		markedLevels.add(level);
	}

	/**
	 * Generates the menu that pops up when certain words in a DepTree are
	 * clicked, such as labels of the SYN level
	 */
	protected void makeDepTreePopup(PopupEvent e,
				final DepTreeInteractor dtI,
				final AbstractDisplay gui) {
		gui._popup.removeAll();
		ArrayList<JMenuItem> items = new ArrayList<>();
		String currentItem = null;
		final String level = dtI._dt.getCurrentLevel();
		TreeMenuListener l = new TreeMenuListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				String text = evt.getActionCommand();
				String synText = "[]";
				if (! "".equals(evt.getActionCommand())) {
					synText = text;
				}
				if (_level.equals(level)) {
					for (Element elem : _targets) {
						elem.getLastChild().setTextContent(synText);
					}
					dtI.getDecParse().selectLabelAt(_node.getIndex(),
								_level,
								text);
					dtI.refreshDisplay(true);
				}
				else if (_level.equals("DEPTREEWORD")) {
					dtI.getDecParse()
								.selectLexeme(getLexiconItems().get(evt.getActionCommand()));
					int sizeBefore = dtI.getDecParse().getWords().size();
					if (sizeBefore == dtI.getDecParse().getWords().size()) {
						dtI.redraw(_node, _level, true);
					}
					else {
						draw();
//						updateTable();
					}
				}
				handleFocus(dtI);
			}
		};

		l.setFields(e.getNode(),
					e.getLevel(),
					dtI._dt.getElements(e.getType(), e.getNode(), e.getLevel(), true));
		if (e.getLevel().equals(level)) {
			for (String label : dtI.getParser().getGrammar()
						.findLevel(level)
						.getLabels()) {
				items.add(makeMenuItem(label));
			}
		}
		else if (e.getLevel().equals("DEPTREEWORD")) {
			int nodeIndex = e.getNode().getIndex();
			l.setLexiconItems(new HashMap<>());
			for (LexemeNode ln : dtI.getDecParse().getLexemeVariants(nodeIndex)) {
				LexiconItem lexiconItem = ln.getLexeme();
				String lexName = lexiconItem.getDescription();
				String lexDescription = amendDescription(lexName, lexiconItem);
				l.getLexiconItems().put(lexDescription, ln);
				items.add(makeMenuItem(lexDescription));
				if (lexName.equals(dtI.getDecParse().getWords().get(nodeIndex).description)) {
					currentItem = lexDescription;
				}
			}
		}
		for (JMenuItem item : items) {
			item.addActionListener(l);
			if (item.getActionCommand().equals(currentItem)) {
				gui._popup.addSeparator();
				gui._popup.add(item);
				gui._popup.addSeparator();
			}
			else {
				gui._popup.add(item);
			}
		}
		gui._popup.pack();
		gui._popup.show(gui,
					e.getDevt().getScreenX() - gui.getX(),
					e.getDevt().getScreenY() - gui.getY());
	}

	protected JMenuItem makeMenuItem(String label) {
		JMenuItem result = new JMenuItem(label);
		result.setName(label + AbstractDisplay.MENU_ITEM);
//		System.out.println(result.getName());
		return result;
	}

	abstract void draw();

	abstract void handleFocus(DepTreeInteractor dtI);

	abstract void set_GUI(AbstractDisplay gui);

	/**
	 * Initializes the GUI after its creation by setting its listeners
	 *
	 */
	protected void init_GUI(final AbstractDisplay gui) {
		gui.getPrefItem().addActionListener(arg0 -> {
            DepTree dt = getDTwithOptions();
            refreshPreferences(gui, dt);
            gui.showPreferences();
        });

		gui.getQuitItem().addActionListener(arg0 -> System.exit(0));

		gui.getHelpItem().addActionListener(arg0 -> gui._HelpTextFrame.setVisible(true));

		gui.getButton(AbstractDisplay.CANCEL_PREFS_BUTTON)
					.addActionListener(arg0 -> gui.hidePreferences());

		gui.getButton(AbstractDisplay.SAVE_PREFS_BUTTON)
					.addActionListener(arg0 -> {
                        // System.out.print("save ");
                        gui.hidePreferences();
                        setOptions();
                        draw();
                    });

		gui.getButton(AbstractDisplay.APPLY_PREFS_BUTTON)
					.addActionListener(arg0 -> {
                        setOptions();
                        draw();
                    });
		gui.getConstraintsTable()
					.addMouseListener( new MouseAdapter() {
						@Override
						public void mousePressed(MouseEvent e ){
							if ( SwingUtilities.isRightMouseButton( e )){
								_hideLastRow = ! _hideLastRow;
								_keepSelectedRow = true;
								updateTable();
								_keepSelectedRow = false;
							}
						}
					});
	}

	abstract DepTree getDTwithOptions();

	abstract void setOptions();

	/**
	 * Stores the values shown in the preferences window in the DepTree after
	 * the
	 * appropriate button like the save button has been pressed
	 *
	 */
	protected void setOptions(PreferencesDisplay prefD, DepTree dt) {
		dt.setShowingCat(prefD.getBox(AbstractDisplay.TAGS_BOX).isSelected());
		dt.setShowingReferent(prefD.getBox(AbstractDisplay.REF_BOX).isSelected());
		dt.setDrawingRoots(prefD.getBox(AbstractDisplay.ROOT_BOX).isSelected());
		dt.setDrawingBoxes(prefD.getBox(AbstractDisplay.TRANSPARENT_BOX).isSelected());
		dt.setDroppingLeaves(prefD.getBox(AbstractDisplay.DROPLEAVES_BOX).isSelected());
		if (prefD.getBox(AbstractDisplay.PARSE_BOX) != null) {
			dt.setParsingIncrementally(prefD.getBox(AbstractDisplay.PARSE_BOX)
						.isSelected());
		}

		// dt.setMinY(Double.parseDouble(prefD._MinYField.getText()));
		dt.setCurrentYLength(Double.parseDouble(prefD.getField(AbstractDisplay.Y_CURRENT_FIELD)
					.getText()));
		dt.setREFYLength(Double.parseDouble(prefD.getField(AbstractDisplay.Y_NONCURRENT_FIELD)
					.getText()));
		dt.setFont(dt.getFont().deriveFont(dt.getFont().getStyle(),
					Float.parseFloat(prefD.getField(AbstractDisplay.FONT_FIELD)
								.getText())));
	}


	/**
	 * Sets the options shown in the preferences window to the values stored in
	 * the DepTree when opening it
	 *
	 */
	protected void refreshPreferences(AbstractDisplay gui, DepTree dt) {
		gui.getBox(AbstractDisplay.TAGS_BOX).setSelected(dt.isShowingCat());
		gui.getBox(AbstractDisplay.REF_BOX).setSelected(dt.isShowingReferent());
		gui._prefD.getBox(AbstractDisplay.ROOT_BOX).setSelected(dt.isDrawingRoots());
		gui._prefD.getBox(AbstractDisplay.TRANSPARENT_BOX)
					.setSelected(dt.getTransparentColor()
								.equals(DepTree.transWhite));
		gui._prefD.getBox(AbstractDisplay.DROPLEAVES_BOX)
					.setSelected(dt.isDroppingLeaves());
		if (gui._prefD.getBox(AbstractDisplay.PARSE_BOX) != null) {
			gui._prefD.getBox(AbstractDisplay.PARSE_BOX)
						.setSelected(dt.isParsingIncrementally());
		}

		// gui._prefD._MinYField.setText(String.valueOf(dt.getMinY()));
		gui.getField(AbstractDisplay.Y_CURRENT_FIELD)
					.setText(String.valueOf(dt.getCurrentYLength()));
		gui._prefD.getField(AbstractDisplay.Y_NONCURRENT_FIELD)
					.setText(String.valueOf(dt.getREFYLength()));
		gui._prefD.getField(AbstractDisplay.FONT_FIELD)
					.setText(String.valueOf(dt.getFont().getSize()));
	}

	/**
	 * Gets the x size of a string shown in the constraints table
	 *
	 * @param table
	 *            the constraints table
	 */
	protected double stringLengthInPixels(String string, JTable table) {
		return table.getFont()
					.getStringBounds(string,
								table.getFontMetrics(table.getFont())
											.getFontRenderContext())
					.getMaxX();
	}

	/**
	 * Updates the constraints table
	 *
	 */
	protected void updateTable(DepTreeInteractor dtI, AbstractDisplay gui) {
//		System.out.println("update " + dtI._dt.toString());
		JTable table = gui.getConstraintsTable();
		ConstraintsModel model = new ConstraintsModel();
		model.init();
		_isSettingConstraintsModel = true;
		table.setModel(model);
		_isSettingConstraintsModel = false;
		if (dtI.getDecParse() != null) {
			gui.getScoreValueLabel().setText(String.valueOf(dtI.getDecParse()
						.getBadness()
						.getScore()));
			for (ConstraintViolation cv : dtI.getDecParse().getViolations()) {
				addConstraint(model, cv, dtI);
			}
			for (int col = 0; col < model.getColumnCount(); col++) {
				double maxSize =
							stringLengthInPixels(model.getColumnName(col),
										table);
				// this makes it worse
				// try {
				// waitForRendering(dtI._dt);
				// } catch (InterruptedException e) {
				// e.printStackTrace();
				// }
//				System.out.println("constraints");
				for (int row = 0; row < model.getRowCount(); row++) {
					// TODO remove bug: sometimes indexOutofBounds exception at
					// getValueAt, probably because of lack of thread safety.
					// Does not only happen when entering text into parsefield.
					if (row < model.getRowCount() && col < model.getColumnCount()){
						maxSize =
									Math.max(maxSize,
												stringLengthInPixels(model.getValueAt(row,
															col)
															.toString(),
															table));
					}
				}
				table.getColumnModel()
							.getColumn(col)
							.setMinWidth((int) maxSize + 2
										* table.getFont().getSize());
			}
		}
		if (_keepSelectedRow){
			table.changeSelection(getSelectedRow(), 0, false, false);
		}
		else {
			setSelectedRow(-1, gui);
		}
		if (_hideLastRow){
			gui._constraintsSplitPane.setDividerLocation(gui.getConstraintsTable().getWidth());
		}
		else{
			gui._constraintsSplitPane.setDividerLocation(1.0);
		}
	}

	protected abstract void updateTable();

	/**
	 * When this works it is supposed to prevent conflicts between threads
	 * 
	 * @param dt
	 *            contains the isBusy field that shows that the drawing of the
	 *            tree is in progress
	 * @throws InterruptedException hopefully never
	 */
	protected void waitForRendering(DepTree dt) throws InterruptedException {
		while (dt.isBusy()) {
			System.out.print("waiting rendering ");
			Thread.sleep(10);
		}
		System.out.println();
	}

	protected void addConstraint(ConstraintsModel model,
				ConstraintViolation cv,
				DepTreeInteractor dtI) {
		int wordIndex1 = dtI.getDecParse().calcIndex1(cv);
		int wordIndex2 = dtI.getDecParse().calcIndex2(cv);
		String lvlName1 =
					dtI._dt.getDecParse().getCvLevelName(cv.getNodeBindingIndex1());
		String lvlName2 = null;
		DepTreeNode node2 = null;
		if (wordIndex2 >= 0) {
			node2 = dtI._dt.getNode(wordIndex2);
			lvlName2 = dtI._dt.getDecParse().getCvLevelName(
						 cv.getNodeBindingIndex2());
		}
		model.add(cv.getConstraint().getId(),
					cv.getPenalty(),
					cv.getConstraint().toConstraintString(),
					dtI._dt.getNode(wordIndex1),
					lvlName1,
					node2,
					lvlName2);
	}

	/**
	 * Writes the CDA data into a file
	 *
	 */
	protected void writeToFile(DecoratedParse decParse, File file) {
		FileService.saveToFile(decParse.toAnnotation(true), file);
	}

	/**
	 * Writes the CDA data into a file
	 *
	 */
	protected void writeToFile(DecoratedParse decParse, String filename) {
		File file = new File(filename);
		writeToFile(decParse, file);
	}

	private int getSelectedRow() {
		return _selectedRow;
	}

	private void setSelectedRow(int selectedRow, AbstractDisplay gui) {
		_selectedRow = selectedRow;
		String cvID;
		if (selectedRow == -1){
			cvID = "";
		}
		else {
			cvID = ": '" + gui._constraintsTable.getModel().getValueAt(selectedRow, 1).toString();
		}
		JTextArea area = gui._constraintsArea;
		area.setCaretPosition(area.getText().length() - 1);
//		System.out.println(area.getText().indexOf(cvID));
		if (!area.getText().contains(cvID)){
			cvID = ": " + gui._constraintsTable.getModel().getValueAt(selectedRow, 1).toString();
		}
		area.setCaretPosition(area.getText().indexOf(cvID));
//		Point pt2 = area.getCaret().getMagicCaretPosition();
//		if (pt2 != null){
//			Point pt1 = area.getCaret().getMagicCaretPosition();
//			Rectangle rect = new Rectangle(pt1, new Dimension(1, 10));
//			area.scrollRectToVisible(rect);
////			rect = new Rectangle(pt2, new Dimension(1, 10));
////			area.scrollRectToVisible(rect);
//		}
	}
	
	protected void setHelpText(File helpFile, AbstractDisplay gui){
		_helpText = FileService.loadText(helpFile);
		gui._helpArea.setText(_helpText);
		gui._HelpTextFrame.setSize(gui.getSize());
	}


	/**
	 * Contains the data for the table with the constraints violations
	 * 
	 * @author Sven Zimmer
	 * 
	 */
	protected class ConstraintsModel extends AbstractTableModel {
		private static final long serialVersionUID = 1L;
		private ArrayList<String> _columnNames;
		private ArrayList<ArrayList<Object>> _rows;
		private ArrayList<DepTreeNode> _nodes1;
		private ArrayList<DepTreeNode> _nodes2;
		private ArrayList<String> _levels1;
		private ArrayList<String> _levels2;

		@Override
		public int getColumnCount() {
			int result = _columnNames.size();
			if (_hideLastRow ){
				result = result -1;
			}
			return  result;
		}

		@Override
		public int getRowCount() {
			return _rows.size();
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			return _rows.get(rowIndex).get(columnIndex);
		}

		@Override
		public String getColumnName(int col) {
			return _columnNames.get(col);
		}

		public void init() {
			_columnNames = new ArrayList<>();
			_rows = new ArrayList<>();
			_nodes1 = new ArrayList<>();
			_nodes2 = new ArrayList<>();
			_levels1 = new ArrayList<>();
			_levels2 = new ArrayList<>();
			_columnNames.add("Penality");
			_columnNames.add("Violation");
			_columnNames.add("Rule");
		}

		public void add(String violation,
					double penality,
					String rule,
					DepTreeNode node1,
					String level1,
					DepTreeNode node2,
					String level2) {
			ArrayList<Object> row = new ArrayList<>();
			row.add(penality);
			row.add(violation);
			row.add(rule);
			_rows.add(row);
			_nodes1.add(node1);
			_levels1.add(level1);
			_nodes2.add(node2);
			_levels2.add(level2);
		}
		

		public DepTreeNode getNode1(int row) {
			return _nodes1.get(row);
		}

		public DepTreeNode getNode2(int row) {
			return _nodes2.get(row);
		}

		public String getLevel1(int row) {
			return _levels1.get(row);
		}

		public String getLevel2(int row) {
			return _levels2.get(row);
		}
	}

}
