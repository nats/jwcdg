package de.unihamburg.informatik.nats.jwcdg.frobbing;

import java.util.EnumSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNetState;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.InvalidNetStateException;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.exceptions.TimeUpException;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;
import de.unihamburg.informatik.nats.jwcdg.parse.ParseInfo;
import de.unihamburg.informatik.nats.jwcdg.transform.Analysis;
import de.unihamburg.informatik.nats.jwcdg.transform.Badness;
import de.unihamburg.informatik.nats.jwcdg.transform.ConstraintViolation;
import de.unihamburg.informatik.nats.jwcdg.transform.Patch;

/**
 * Repair enhanced with pruning: like errorDriven, but restricted to
 * Patches with an optimal limit.
*/
public class CombinedFrobber extends AbstractFrobber {

	private static Logger logger = LogManager.getLogger(CombinedFrobber.class);
	
	public CombinedFrobber(ConstraintNetState state, Configuration config,
			GrammarEvaluator evaluator, double pressure) {
		super(state, config, evaluator, pressure);
		initializeProblem();
	}

	public CombinedFrobber(ConstraintNetState state, Analysis a,
			Configuration config, GrammarEvaluator evaluator, double pressure) {
		super(state, a, config, evaluator, pressure);
		initializeProblem();
	}
	
	public CombinedFrobber(ConstraintNetState state, Configuration config,
			GrammarEvaluator evaluator, double pressure, Parse p) {
		super(state, config, evaluator, pressure, p);
		initializeProblem();
	}

	private void initializeProblem() {
		while(true) { // as pressure cannot be reduced indefinetly, we will inevitably run into the break or throw statements
			try {
				newMaximum(current);
			} catch (InvalidNetStateException e) {
				if(!reducePressure()) {
					throw new RuntimeException("No valid net for this input sentence and grammar! " +
							"Please contact the grammar author. " +
							"If you are the grammar author, try reducing the number of hard contraints in the grammar.");
				}
				continue;
			}
			break;
		}
	}

	/**
	 * The old combined method
	 */
	public boolean solve() {
		Patch p;
		ConstraintViolation cv;
		ConstraintViolation primaryConflict = null;
		RepairAttempt currentAttempt = null;

		logger.info("Initial badness:" +best.getBadness());
		logger.info(String.format("Ambiguity: %.1f", netState.getAmbiguity()));
		registerParse(best.toParse(config, evaluator));
//		/* report on distance to the annotation, if any */
//		if(nsAutoCompare && P.annotation) {
//		  aReport(P.current, P.annotation);
//		}
		
		logger.info("Starting to frob!\n");

		/* iterate */
		while (best.getBadness().hard >0 ||
			   Badness.significantlyGreater(globalMaximum, best.getBadness().soft)) {

			/* check for internal errors */
			assert current.check(evaluator, 3, best.getConflicts());

			// check if a score goal is still reachable, otherwise stop frobbing
			if(!goalReachable()) {
				if(reducePressure()) {
					// as long as the pressure can still be reduced, we should not give up
					primaryConflict = null;
					continue;
				}
				
				logger.info("Frobbing goal not reachable, aborting frobbing!");
				
				finish();
				return false;
			} 

			/* step */
			cv = selectConflict();

			/* no progress possible? */
			if (cv == null) {
				traceBack();
				
				// if the solution is worse than the pressure, we have to go on 
				// and reduce the pressure, if possible
				if(Badness.significantlyGreater(globalMinimum, best.getBadness().getScore()) 
						&& reducePressure()) {
					primaryConflict = null;
					continue;
				}
				
				logger.info("No removable conflicts remain");
				break;
			}

			/* no serious penalties remaining? */
			if(cv.getPenalty() >= config.getDouble(IGNORETHRESHOLD)) {
				traceBack();
				
				// if the solution is worse than the pressure, we have to go on 
				// and reduce the pressure, if possible
				if(Badness.significantlyGreater(globalMinimum, best.getBadness().getScore()) 
						&& reducePressure()) {
					primaryConflict = null;
					continue;
				}
				
				logger.info("No repairable serious conflicts remain");
				break;
			}

			if (primaryConflict == null) {
				stats.incPatchAttempts();
				primaryConflict = new ConstraintViolation(cv);
				currentAttempt = new RepairAttempt(primaryConflict);
			} else {
				currentAttempt.addConstraint(cv.getConstraint());
			}

			logger.debug(String.format( "attacking %s", cv.getConstraint().getId() ));

			EnumSet<FrobbingFlag> flags = EnumSet.of(
					FrobbingFlag.OD_LIMIT,
					FrobbingFlag.OD_NARROW,
					FrobbingFlag.OD_PRUNE);
			if(config.getFlag(GREEDY)) {
				flags.add(FrobbingFlag.OD_IMMEDIATE);
			}
			if(config.getFlag(BLOCK_NEW_LVS)) {
				flags.add(FrobbingFlag.OD_NONEWLVS);
			}
			
			boolean pressureReduced = false;
			try {
				
				//  |     most of the work is done here!
				// \|/
				//  V
				p = attackConflict(current, cv, flags);
				//     A      A
				//    /|\    /|\
				//     |      |
				
			} catch (InvalidNetStateException e1) {
				if(!reducePressure()) {
					finish();
					return false;
				}
				pressureReduced = true;
				p = null; // make the compiler happy
			} catch (TimeUpException e2) {
				break;
			}
			
			/* user break or other interruption? */
			if (interrupted) {
				logger.info("interrupted!");
				stats.registerRepairAttempt(currentAttempt, false);
				//traceBack();
				break;
			}

			/* time limit? */
			if(timerExpired()) {
				logger.info("Timer expired!");

				stats.registerRepairAttempt(currentAttempt, false);
				killedByTimelimit = true;
				
				finish();
				return false;
			}

			/* pressure has been reduced, try again*/
			if (pressureReduced) {
				primaryConflict = null;
				continue;
			}

			/* analysis impossible? */
			if (globalMaximum == 0.0) {
				stats.registerRepairAttempt(currentAttempt, false);
				if(!reducePressure()) {
					finish();
					return false;
				}
				primaryConflict = null;
				continue;
			}

			/* step limit reached? */
			if(config.getInt(MAXSTEPS) >0 && removedConflicts.size() > config.getInt(MAXSTEPS)) {

				logger.info(String.format("Giving up on conflict '%s', step limit of %d exceeded.", 
						primaryConflict.getConstraint().getId(), config.getInt(MAXSTEPS)));
				stats.registerRepairAttempt(currentAttempt, false);

				unremovable.add(primaryConflict);
				primaryConflict = null;
				traceBack();
				continue;
			}

			/* correction possible? */
			if (p != null) {

				current.apply(p, evaluator);
				assert current.check(evaluator, 3, best.getConflicts());

				/* new maximum? */
				if( current.getBadness().compare( best.getBadness()) ) {
					int steps = currentAttempt.getCs().size() + 1;
					logger.info(String.format("Removed conflict '%s' in %d step%s",
							primaryConflict.getConstraint().getId(),
							steps, (steps > 1)?"s":""));
					
//					if(nsAutoCompare && P.annotation) {
//						aReport(P.current, P.annotation);
//					}
					
					try {
						newMaximum(current);
					} catch (InvalidNetStateException e) {
						if(!reducePressure()) {
							finish();
							return false;
						}
					}
					
					primaryConflict = null;

					// finalize repair attempt
					stats.registerRepairAttempt(currentAttempt, true);
					
					continue;
				}
			} else { //p==null

				/* skip the conflict whose removal led us here... */
				logger.info(String.format("Giving up on conflict '%s' after %d step%s", 
						primaryConflict.getConstraint().getId(),
						removedConflicts.size(),
						removedConflicts.size()>1?"s":""));
				
				// finalize repair attempt
				stats.registerRepairAttempt(currentAttempt, false);

				/* Do not accept that a hard conflict cannot be removed.
			     Reduce pressure instead. */
				if(0.0 == primaryConflict.getConstraint().getPenalty()) {
					if(!reducePressure()) {
						finish();
						return false;
					}
					primaryConflict = null;
					continue;
				}

				unremovable.add(primaryConflict);
				primaryConflict = null;

				/* ...and return to the best Analysis */
				traceBack();
			}

			// if the pressure is too high, no solution might be possible,
			// we can catch this here and reduce the pressure,
			// otherwise the loop condition might end the search prematurely
			if( Badness.significantlyGreater(globalMinimum, globalMaximum) ) {
				if(!reducePressure()) {
					finish();
					return false;
				} 
				primaryConflict = null;
			}
			
		} // end while (P.best.b.hard || significantlyGreater(P.globalMaximum, P.best.b.soft))

		finish();		
		return true;
	}

	/**
	 * the stuff that has to be done at every exit point of solve()
	 */
	private void finish() {
		traceBack();
		solution = best.toParse(config, evaluator);
		ParseInfo info = solution.getInfo();
		info.setEvaluationTime(System.currentTimeMillis() - stats.getStartTime());
		info.setSearchStrategy("Frobbing");
		info.setEntry("Frobbing Method", this.methodName());
		logger.info("\n"+ solution.print(true, config.getDouble(AbstractFrobber.IGNORETHRESHOLD)));
		logger.info(String.format("Frobbing took %.3f seconds", (System.currentTimeMillis()-stats.getStartTime())/1000d ));
	}
	
	@Override
	public String methodName() {
		return CombinedFactory.NAME;
	}
}
