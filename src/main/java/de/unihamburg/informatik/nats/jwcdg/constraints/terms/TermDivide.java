package de.unihamburg.informatik.nats.jwcdg.constraints.terms;


import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

public class TermDivide extends TermArithmethic {

	public TermDivide(Term a, Term b) {
		super(a, b);
	}
	
	@Override
	public AVM eval(LexemeGraph lg, Context context,
			LevelValue[] binding) {
		AVM va = a.eval(lg, context, binding);
		AVM vb = b.eval(lg, context, binding);
		
		if( (va instanceof AVMNumber) && (vb instanceof AVMNumber)) {
			double d1 = ((AVMNumber)va).getNumber();
			double d2 = ((AVMNumber)vb).getNumber();
			if(d2 == 0.0) {
				return new AVMError("Division by zero");
			}
			return new AVMNumber(calculate(d1, d2));
		} else {
			return new AVMError("Cannot calculate with non-numerical values");
		}
	}
	
	
	@Override
	protected double calculate(double d1, double d2) {
		return d1 / d2;
	}
	
	@Override
	protected String sign() {
		return "/";
	}
}
