package de.unihamburg.informatik.nats.jwcdg.constraints.formulas;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.AccessContext;
import de.unihamburg.informatik.nats.jwcdg.constraints.Connection;
import de.unihamburg.informatik.nats.jwcdg.constraints.VarInfo;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

public class FormulaConnection extends Formula {
	
	private VarInfo v1, v2;
	private int v1index, v2index;
	private Connection conn;
	
	public FormulaConnection(VarInfo v1, Connection conn, VarInfo v2) {
		this.v1   = v1;
		this.conn = conn;
		this.v2   = v2;
		this.v1index = v1.index;
		this.v2index = v2.index;
	}

	@Override
	public boolean evaluate(LevelValue[] binding,
			Context context, LexemeGraph lg) {
		LevelValue lv1 = binding[v1index];
		LevelValue lv2 = binding[v2index];
		
		return conn.subsumes(lv1.getCon(lv2));
	}
	
	@Override
	public String toString() {
		return String.format("%s%s%s", v1.varname, conn.form, v2.varname);
	}

	@Override
	public void analyze(AccessContext context, Grammar g) {
	}

	@Override
	public void collectFeatures(List<Path> sofar) {
	}

	@Override
	public boolean check() {
		return true;
	}
	
	@Override
	public void collectHasInvocations(List<Formula> akku) {
	}
}
