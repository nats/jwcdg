package de.unihamburg.informatik.nats.jwcdg.constraintNet;

import static de.unihamburg.informatik.nats.jwcdg.constraints.Direction.Left;
import static de.unihamburg.informatik.nats.jwcdg.constraints.Direction.Nil;
import static de.unihamburg.informatik.nats.jwcdg.constraints.Direction.NonNil;
import static de.unihamburg.informatik.nats.jwcdg.constraints.Direction.Right;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraints.Direction;
import de.unihamburg.informatik.nats.jwcdg.constraints.Level;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluationResult;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.transform.ConstraintViolation;

/**
 * Models a node in a constraint net
 */
public class ConstraintNode {
	private ConstraintNet net;		   /* corresponding constraint net */
	private Level level;		       /* points to the level for which
	                                      a constraint node was built. */
	private GraphemeNode gn;		   /* corresponding grapheme node */
	private List<LevelValue> values;   /* holds shallow copies of all LVs that 
	                                      may be used to bind this constraint node. */
	//private List<ProtoEdge> frozenProtoedges; // edges that where not built due to freezing but might need to be later on
	
	public ConstraintNode(ConstraintNet net, Level level, GraphemeNode gn) {
		this.net = net;
		this.gn = gn;
		this.level = level;
		values = new ArrayList<>();
		// frozenProtoedges = new ArrayList<ProtoEdge>();
	}
	
	
	public List<LevelValue> getValues() {
		return values;
	}

	/**
	   Builds all level values for LEVEL, MODIFIER and MODIFIEE.

	   This function builds all LVs that represent tuples composed from the
	   parameters it receives. This function is used as the innermost loop by
	   \b cnBuildIter().
	 */
	public void buildLevelValues(Level lvl,
			GraphemeNode modifier, GraphemeNode modifiee,
			GrammarEvaluator evaluator) {
		buildLevelValues(lvl, modifier, modifiee, false, false, null, null, evaluator);
	}

	/**
	   Builds all level values for LEVEL, MODIFIER and MODIFIEE.

	   This function builds all LVs that represent tuples composed from the
	   parameters it receives. This function is used as the innermost loop by
	   \b cnBuildIter().

	   returns the number of LVs built
	 */
	public int buildLevelValues(Level level, 
			GraphemeNode modifier, GraphemeNode modifiee,
			boolean frozen, // domain is frozen
			boolean skipFrozen, // don't even build LVs that would be frozen
			List<String> labelExceptions, // LVs with one of these labels will not be frozen!
			List<String> ownLabels, // use these instead of level->labels
			GrammarEvaluator evaluator
			)
	{
		int ret =0;
		
		Direction dir;

		/* guard against self-modification */
		if(!net.getLexemeGraph().mayModify(modifier, modifiee)) {
			return 0;
		}

//		ProtoEdge pe =null;
//		if(frozen && skipFrozen) {
//			pe = new ProtoEdge(level, modifier, modifiee);
//
//			if(labelExceptions == null) {
//				// skip all, there will be nothing to do!
//				// but save the parameters as protoedge, it might need to be build later on
//				frozenProtoedges.add(pe);
//				return 0;
//			} else {
//				// some edges will be build, some need to be preserved for later building
//				pe.setUseOwnLabels(true);
//				frozenProtoedges.add(pe);
//			}
//		}

		/* compute edge direction */
		if (modifiee.isNonspec()) {
			dir = Right;
		} else if(modifiee.isNil()) {
			dir = Nil;
		} else if (modifiee.isVirtual() && modifier.isVirtual()) {
			dir = NonNil; // TODO #POSSIBLE_POINT_OF_FAILURE is Nonnil a valid value here (it being underspecified for left or right) 
		} else if (modifiee.isVirtual()) {
			dir = Right; // virtual nodes are always right of nonvirtual ones
		} else if (modifier.isVirtual()){
			dir = Left; // virtual nodes are always right of nonvirtual ones
		} else if(modifier.getArc().from < modifiee.getArc().from) {
			dir = Right;
		} else {
			dir = Left;
		}

		/* iterate over labels */
		List<String> l;
		if(ownLabels != null)
			l= ownLabels;
		else
			l= level.getLabels();

		for(String label: l) {
			/* compute edge type */
//			boolean fr= frozen;
//			if(frozen) { // if this domain is frozen...
//				if(labelExceptions != null && labelExceptions.contains(label)) {
//					// ... this label could be an exception ...
//					fr= false;
//				} else if(skipFrozen) {
//					// ... or we just want to skip the whole thing, next label
//					pe.addLabel(label); // save label for later
//					continue;
//				}
//			}

			int et = net.getGrammar().etEncode(level, label, dir);
			Set<Path> downRelevant = net.getGrammar().getDownFeatures(et);
			Set<Path>   upRelevant = net.getGrammar().getUpFeatures(et);

			List<List<LexemeNode>> downClasses = net.getLexemeGraph().partitions(modifier, downRelevant);
			List<List<LexemeNode>> upClasses   = net.getLexemeGraph().partitions(modifiee, upRelevant);

			for (List<LexemeNode> downClass : downClasses) {
				for (List<LexemeNode> upClass : upClasses) {
					if(buildLv(downClass, modifier, level, label, upClass, modifiee, true, evaluator))
						ret++;
				}
			}
		}
		return ret;
	}
	
	/**
	   Build a new levelvalue.

	   This function creates exactly one LV with the specified fields using
	   \b lvNew() and stores it in \b node, incrementing all relevant
	   counters properly.
	 */
	private boolean buildLv(List<LexemeNode> dependents, GraphemeNode dep, Level level, 
			String label, List<LexemeNode> regents, GraphemeNode reg, boolean useReactivation,
			GrammarEvaluator evaluator) {

		LevelValue newLv = new LevelValue(level, net.getNoOfLevels(), 
				dependents, dep, label, regents, reg);

		GrammarEvaluationResult ger
			= evaluator.evalUnary(newLv, net.getLexemeGraph(), null, false, false);
		newLv.setScore(ger.getScore());
		for(ConstraintViolation cv : ger.getCVs()) {
			newLv.addViolatedConstraint(cv.getConstraint());
		}
		
		newLv.setNonFluctuatingScore(ger.getNonFluctBadness().getScore());
		
		if (newLv.getScore() == 0.0) {
			if( !useReactivation || newLv.getNonflucScore()==0.0 ) {
				// only delete LV if it does not result from fluctuating constraints penalty.
				// those are not included in nonFluctuatingScore
				net.noteLVDeletedUponbuilding();
			} else {
				// safe LV for later reactivation
				net.postponeLV(newLv);
			}
			return false;
		} else {
			net.addLv(newLv);
			return true;
		}
	}

	public GraphemeNode getGN() {
		return gn;
	}

	public Level getLevel() {
		return level;
	}
	
	/**
	 * The index all the values of this node share,
	 * gn.no * nooflevels + lvlno
	 */
	public int getIndex() {
		return gn.getNo() * net.getNoOfLevels() + level.getNo();
	}

	/**
	 * sort the levelvlaues by the given comparator
	 * 
	 * @param comparator
	 */
	public void sortLVs(Comparator<LevelValue> comparator) {
		Collections.sort(values, comparator);
	}
	
	@Override
	public String toString() {
		return String.format("CNode: %s, %s, %d", gn, level, values.size());
	}
}
