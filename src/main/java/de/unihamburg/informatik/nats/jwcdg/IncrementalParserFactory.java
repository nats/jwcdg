package de.unihamburg.informatik.nats.jwcdg;

import de.unihamburg.informatik.nats.jwcdg.frobbing.CombinedFactory;
import de.unihamburg.informatik.nats.jwcdg.incrementality.IncrementalFrobber;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;
import de.unihamburg.informatik.nats.jwcdg.transform.SolverFactory;

public class IncrementalParserFactory implements ParserFactory<IncrementalFrobber> {

	@Override
	public IncrementalFrobber makeParser(Grammar grammar, Lexicon lexicon,
			SolverFactory factory, Configuration config) {
		return new IncrementalFrobber(grammar, lexicon, factory, config);
	}
	
	public IncrementalFrobber makeParser(Grammar grammar, Lexicon lexicon,
			Configuration config) {
		return new IncrementalFrobber(grammar, lexicon, new CombinedFactory(grammar, lexicon, config), config);
	}
}
