package de.unihamburg.informatik.nats.jwcdg.incrementality;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.util.concurrent.AbstractFuture;

import de.unihamburg.informatik.nats.jwcdg.ParseObserver;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNet;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNetState;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.GraphemeNode;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;
import de.unihamburg.informatik.nats.jwcdg.parse.ParseInfo;
import de.unihamburg.informatik.nats.jwcdg.transform.Analysis;
import de.unihamburg.informatik.nats.jwcdg.transform.ProblemSolver;
import de.unihamburg.informatik.nats.jwcdg.transform.SolverFactory;


/**
 * This future computes an incremental step
 * @author Niels Beuck, Arne Köhn
 *
 */
public class IncrementalStepFuture extends AbstractFuture<IncrementalStep> implements Runnable{
	
	private static Logger logger = LogManager.getLogger(IncrementalStepFuture.class);
	
	private volatile SolverFactory solverFactory;

	// These variables are confined to the thread running
	private IncrementalStep previousStep;
	private ChunkIncrement nextChunk;
	private boolean isFinalStep;
	// private List<String> lookahead;
	private Configuration config;
	private GrammarEvaluator evaluator;
	
	// references needed to interrupt the computation
	private volatile VirtualNodeMatcher vnMatcher;
	private volatile ProblemSolver solver;
	private volatile boolean interrupted;
	private volatile List<ParseObserver> observers;
	
	public IncrementalStepFuture (IncrementalStep previousStep,
	                          ChunkIncrement nextChunk,
	                          boolean isFinalStep,
	                          List<String> lookahead,
	                          Configuration config,
	                          GrammarEvaluator evaluator,
	                          SolverFactory solverFactory) {
		this.previousStep = previousStep;
		this.nextChunk = nextChunk;
		this.isFinalStep = isFinalStep;
		// this.lookahead = lookahead;
		this.config = config;
		this.evaluator = evaluator;
		this.vnMatcher = new VirtualNodeMatcher(evaluator, config);
		this.solverFactory = solverFactory;
	}
	
	/**
	 * set the observers that should be handed down to the solver
	 * note: Use this method *before* executing this object! Else there is no
	 * guarantee that the observers will be notified.
	 * @param observers
	 */
	public void setObservers(List<ParseObserver> observers) {
		this.observers = observers;
	}

	public void run() {
		logger.info("Increment: " + nextChunk.getString());
		
		long startTime = System.currentTimeMillis();
		IncrementalStep step = new IncrementalStep(previousStep, nextChunk.getString());
		
		ConstraintNet net = previousStep.getNet();
		
		// TODO #lookahead
//		if(lookahead) {
//			step.problem.lg.lattice.lookahead = lookahead;
//		}
		
		ConstraintNetState state = null;
		Analysis a = previousStep.getAnalysis();
		for(String word: nextChunk.getWords()) {
			net.extend(word, isFinalStep);
			state = new ConstraintNetState(net, config);
			a = a.extendAnalysis(state);
		}
		a.judge(evaluator);
		assert a.check(evaluator, 3, new ArrayList<>());
		long initTime = System.currentTimeMillis() - startTime;
		// Virtual node matching
		long matchTime = 0;
		boolean triedMatching = false;
		boolean foundMatching = false;
		if( config.getFlag(ConstraintNet.USE_VIRTUAL_NODES)
		 && config.getFlag(IncrementalFrobber.MATCH_VIRTUAL_NODES)
		 && previousStep.getAnalysis().isValid()
		 && ! interrupted ) {
			/* matching makes only sense if the old analysis is valid i.e. doesn't
			 * violate hard constraints. Else we wouldn't be able to compute
			 * which matching is the best and spend precious seconds here.
			 * So we give up right away instead.
			 */
			// evaluate if replacing one of the new nodes for a virtual one results in a better analysis
			matchTime = System.currentTimeMillis();
			triedMatching = true;
			GraphemeNode newNode = state.getNet().getLexemeGraph().getLastGN();
			Analysis newA = vnMatcher.bestMatchingReplacement(state, a, newNode, 
			     previousStep.getScore() * config.getDouble(IncrementalFrobber.VIRT_MATCHING_FACTOR));
			if(newA!= null) {
				step.setVirtualNodeMatch(true);
				a = newA;
				foundMatching = true;
			}
			matchTime = System.currentTimeMillis() - matchTime;
			logger.info(String.format("matching took %d ms", matchTime));
		}
		long frobTime = System.currentTimeMillis();
		solver = solverFactory.generateSolver(state, a);
		for (ParseObserver po : observers) {
			solver.registerObserver(po);
		}
		// our caller is responsible for interrupting;
		// we run as long as we can
		solver.setTimeLimit(0);
		logger.info(String.format("Frobbing iteration #%d", step.getIteration()));
		if (!interrupted)
			solver.solve();
		Analysis resA = solver.getBest();
		Parse resP = solver.getSolution();
		if (resP == null)
			resP = resA.toParse(config, evaluator);
		resP.getInfo().setSearchStrategy("Incremental Frobbing");
		frobTime = System.currentTimeMillis() - frobTime;
		long timeneeded =  System.currentTimeMillis() - startTime;
		long timetotal = previousStep.getTimeTotal() + timeneeded;
		ParseInfo info = resP.getInfo();
		info.setEvaluationTime(timeneeded);
		info.setEntry("Overall Time",
		                        Long.toString(timetotal));
		info.setEntry("Incremental Init Time",
				Long.toString(initTime));
		info.setEntry("Incremental Matching Time",
                Long.toString(matchTime));
		info.setEntry("Incremental Frob Time",
                Long.toString(frobTime));
		info.setEntry("Incremental Matching Tried",
                Boolean.toString(triedMatching));
		info.setEntry("Incremental Matching Found",
                Boolean.toString(foundMatching));
		logger.info("iteration done");
		
		step.registerSolution(resA, resP, timetotal);

		set(step);
	}
	
	@Override
	protected void interruptTask() {
		/* the solver might not yet be initialized, so we have to check for that.
		 * A race condition is impossible since run() checks interrupted between
		 * the initialization and the call to solve() of the solver.
		 */
		interrupted = true;
		if (solver != null)
			solver.interrupt();
		vnMatcher.interrupt();
	}
	
	/**
	 * kindly ask the solver to stop as soon as possible so we can get
	 * the result. The solver should not need more than a few milliseconds
	 * for this.
	 * 
	 * Use this instead of cancel(true) iff you are still interested in
	 * the result
	 */
	public void pleaseFinish() {
		interruptTask();
	}
}
