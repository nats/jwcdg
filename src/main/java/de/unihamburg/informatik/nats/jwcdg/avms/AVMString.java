package de.unihamburg.informatik.nats.jwcdg.avms;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.regex.Matcher;


import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import de.unihamburg.informatik.nats.jwcdg.input.Configuration;

public class AVMString extends AVM {
	private static final long serialVersionUID = 8240190601388622172L;
	private static Logger logger = LogManager.getLogger(AVMString.class);
	
	private String string;
	
	public AVMString(String string) {
		this.string = string;
	}

	public String getString() {
		return string;
	}
	
	@Override
	public String toString() {
		return string;
	}
	
	@Override
	public AVM substituteRefs(Matcher matcher, Configuration config) {
		String s = string;
		if(s.length() == 2 &&
				s.charAt(0) == '\\' &&
				"0123456789".lastIndexOf(s.charAt(1)) > -1) {
			int i = Integer.parseInt(string.substring(1));
			
			String match = matcher.group(i); // if this fails the lexicon writer did something wrong	
			if(match == null) {
				logger.warn(String.format("backreference %d not matched by '%s' in pattern '%s'!", 
						i, matcher.group(), matcher.pattern().toString()));
				match = "";
			}
			
			/* maybe cast the result back to number */
			try {
				NumberFormat numFormat = NumberFormat.getInstance(new Locale(config.getString(AVMNumber.NUMBER_FORMAT_LOCALE)));
				double d = numFormat.parse(match).doubleValue();
				return new AVMNumber(d); 
			} catch (ParseException e) {
				return new AVMString(match);
			}
		}
		return this;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((string == null) ? 0 : string.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AVMString))
			return false;
		AVMString other = (AVMString) obj;
		if (string == null) {
			if (other.string != null)
				return false;
		} else if (!string.equals(other.string))
			return false;
		return true;
	}
}
