package de.unihamburg.informatik.nats.jwcdg.constraints.formulas;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.AccessContext;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

public class FormulaBiImpl extends Formula {
	private Formula f1, f2;

	public FormulaBiImpl(Formula a, Formula b) {
		this.isContextSensitive = a.isContextSensitive || b.isContextSensitive;
		f1 = a;
		f2 = b;
	}

	@Override
	public boolean evaluate(LevelValue[] binding, Context context, LexemeGraph lg) {
		return f1.evaluate(binding, context, lg) == f2.evaluate(binding, context, lg);
	}
	
	
	@Override
	public void collectFeatures(List<Path> sofar) {
		f1.collectFeatures(sofar);
		f2.collectFeatures(sofar);
	}
	
	@Override
	public String toString() {
		return String.format("%s<->%s", f1.toString(), f2.toString());
	}
	
	@Override
	public boolean check() {
		if(!f1.check() || !f2.check())
			return false;
		return true;
	}

	@Override
	/** Any restriction imposed by f1 have no impact on f2 and vice versa
	 * as both have to be evaluated no matter what the result of the other is.
	 */
	public void analyze(AccessContext context, Grammar g) {
		AccessContext throwaway = new AccessContext(context);
		f1.analyze(throwaway, g);
		throwaway = new AccessContext(context);
		f2.analyze(throwaway, g);
	}
	
	@Override
	public void collectHasInvocations(List<Formula> akku) {
		f1.collectHasInvocations(akku);
		f2.collectHasInvocations(akku);
	}
	
}
