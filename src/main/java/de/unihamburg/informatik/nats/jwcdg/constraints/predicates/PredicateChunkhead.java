package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * returns TRUE if lattice is a chunk head
 * 
 * @author the CDG-Team
 *
 */
public class PredicateChunkhead extends Predicate {

	public PredicateChunkhead(Grammar g) {
		super("chunkhead", g, false, 1, false);
	}

	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {
		// TODO #PREDICTOR_CHUNKING Auto-generated method stub
		return false;
	}

}
