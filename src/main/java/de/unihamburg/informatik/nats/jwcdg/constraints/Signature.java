package de.unihamburg.informatik.nats.jwcdg.constraints;

import java.io.Serializable;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;

public class Signature implements Serializable {
	
    private static final long serialVersionUID = 1L;
    public final int arity;                   /* number of edges */
	public final Connection conn;             /* configuration of edges */
	public final Level l1;                    /* level of edge 1 */
	public final Level l2;                    /* level of edge 2 */
	public final Direction d1;                /* direction of edge 1 */
	public final Direction d2;                /* direction of edge 2 */

	public Signature(Level level, Direction dir) {
		this.arity = 1;
		this.l1    = level;
		this.d1    = dir;
		this.l2    = null;
		this.d2    = null;
		this.conn  = null;
	}

	public Signature(Level level1, Direction d1, Level level2,
			Direction d2, Connection c){
		this.arity = 2;
		this.l1    = level1;
		this.l2    = level2;
		this.d1    = d1;
		this.d2    = d2;
		this.conn  = c;
	}

//	public void addSecondArg(Direction d, Level level, Connection c) {
//		if (arity != 1) {
//			throw new RuntimeException("This signature is already binary!");
//		}
//		this.d2 = d;
//		this.l2 = level;
//		this.conn = c;
//	}

	public boolean match(LevelValue lv) {
		return d1.subsumes(lv.getDirection());
	}

	public boolean match(LevelValue lv1, LevelValue lv2) {
		return 
		d1.subsumes(lv1.getDirection()) &&
		d2.subsumes(lv2.getDirection()) &&
		conn.subsumes(lv1.getCon(lv2));
	}
	
	
	@Override
	public String toString() {
		if(arity == 1) {
			return String.format("X%s%s", d1.form, l1.getId());	
		} else {
			return String.format("X%s%s%sY%s%s", d1.form, l1.getId(), conn.form, d2.form, l2.getId());
		}
	}

}
