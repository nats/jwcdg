package de.unihamburg.informatik.nats.jwcdg.incrementality.virtualNodes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.GraphemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.input.LexiconItem;
import de.unihamburg.informatik.nats.jwcdg.input.SourceInfo;

/**
 * specification of a virtual node as defined in the grammar
 * 
 * @author Niels Beuck
 *
 */
public class VirtualNodeSpec implements Serializable{
	
    private static final long serialVersionUID = 1L;

    public static final String INIT_VNS_UNUSED = "initVNsUnused";

	/** name that will appear as lexical form */
	private String name;				
	
	/** list of lexeme definitions */
	private List<AVM> values;
	
	/** how many copies should be generated in advance **/
	private int copies;
	
	private SourceInfo info;
	
	
	public VirtualNodeSpec(String name, List<AVM> values ,int copies, SourceInfo info) {
		this.name   = name;
		this.values = values;
		this.copies = copies;
		this.info   = info;
	}
	
	public String getName() {
		return name;
	}
	
	public int getNoOfCopies() {
		return copies;
	}
	
	public List<AVM> getValues() {
		return values;
	}
	
	public SourceInfo getInfo() {
		return info;
	}
	
	/**
	 * Builds Lexemes for a virtual Lexeme Node given a specifiaction
	 * tp= timpoint
	 * n= number of instance of this spec, just for naming purposes
	 */
	public List<LexemeNode> makeLexemeNodes(int tp, int n, GraphemeNode gn, List<String> annoFeats) {
		List<LexemeNode> ret = new ArrayList<>(values.size());
		List<LexiconItem> lis = makeVirtualLexiconItems(annoFeats);

		for(LexiconItem li : lis) {
			LexemeNode ln= new LexemeNode(-3, gn, li);
			ret.add(ln);
		}
		return ret;
	}
	
	/**
	 * build a lexicon item corresponding to the virtual node spec
	 */
	public List<LexiconItem> makeVirtualLexiconItems(List<String> annoPaths) {
		List<LexiconItem> ret = new ArrayList<>(values.size());

		for(AVM v : values) {
			LexiconItem li = new LexiconItem(name, v, info);
			
			// v = new AVMFlat(v, allAttributes);

			StringBuilder desc= new StringBuilder(name);
			
			for(String p : annoPaths) {
				AVM feat = v.followPath(new Path(p, null));
				if(feat instanceof AVMString) {
					String s = ((AVMString)feat).getString();
					if(!s.equals("bot")) {
						desc.append("_");
						desc.append(s);
					}
				}
			}
			
			li.setDescription(desc.toString());
			ret.add(li);
		}

		return ret;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		
		if (obj instanceof VirtualNodeSpec) {
			VirtualNodeSpec other = (VirtualNodeSpec) obj;
			return this.name.equals(other.name);
		} else {
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
	
	@Override
	public String toString() {
		return name + values.toString();
	}
}
