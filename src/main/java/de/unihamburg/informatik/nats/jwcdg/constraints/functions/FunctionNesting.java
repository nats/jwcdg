package de.unihamburg.informatik.nats.jwcdg.constraints.functions;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.lattice.Arc;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

public abstract class FunctionNesting extends Function {

	public FunctionNesting(String name, Grammar g) {
		super(name, g, 1);
	}

	@Override
	public AVM eval(List<Term> args, Context context, LexemeGraph lg,
			LevelValue[] binding) {
	
		LexemeNode ln;
		AVM val = args.get(0).eval(lg, context, binding);
		
		
		if (val instanceof AVMLexemNode) {
			ln = ((AVMLexemNode) val).getLexemeNode();
		} else { 
			return new AVMError(String.format(
					"WARNING: argument of function `%s' not a lexeme node", name));
		}
		
		if (!ln.spec()) {
			return new AVMError(String.format(
					"WARNING: argument of function `%s' not a lexeme node", name));
		}
		
		int n = nesting(ln.getArc());
		if(n < 0 || n > 64) {
			return new AVMError(String.format(
					"WARNING: unreasonable %s count of %d for arc `%s'(%d-%d)",
					name, n, ln.getArc().word, ln.getArc().from, ln.getArc().to));
		}
		
		return new AVMNumber(n);
	}
	
	protected abstract int nesting(Arc a);

}
