package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;

/**
 * abstract superclass for all predicates comparing two numbers
 * 
 * does not subsume equals, as equals can handle non-numerical terms
 * 
 * @author the CDG-Team
 *
 */
public abstract class PredicateNumericalComparison extends PredicateComparison {

	public PredicateNumericalComparison(String name) {
		super(name);
	}

	@Override
	public boolean compare(AVM val1, AVM val2) {
		double a, b;
		
		if (val1 instanceof AVMNumber) {
			a = ((AVMNumber)val1).getNumber();
		} else {
			throw new PredicateEvaluationException("WARNING: 1st arg of comparison not a number");
		}
		
		if (val2 instanceof AVMNumber) {
			b = ((AVMNumber)val2).getNumber();
		} else {
			throw new PredicateEvaluationException("WARNING: 2nd arg of comparison not a number");
		}
		
		return compareNumbers(a, b);
	}
	
	protected abstract boolean compareNumbers(double a, double b);
	
}
