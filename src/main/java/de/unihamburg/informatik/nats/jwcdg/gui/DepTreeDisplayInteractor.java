package de.unihamburg.informatik.nats.jwcdg.gui;

import de.unihamburg.informatik.nats.jwcdg.Parser;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarLexer;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarParser;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.GraphemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.Constraint;
import de.unihamburg.informatik.nats.jwcdg.constraints.formulas.FormulaFalse;
import de.unihamburg.informatik.nats.jwcdg.constraints.formulas.FormulaImpl;
import de.unihamburg.informatik.nats.jwcdg.gui.DepTreeNavigator.DepTreeNavNode;
import de.unihamburg.informatik.nats.jwcdg.incrementality.ChunkIncrement;
import de.unihamburg.informatik.nats.jwcdg.incrementality.IncrementalFrobber;
import de.unihamburg.informatik.nats.jwcdg.incrementality.IncrementalStep;
import de.unihamburg.informatik.nats.jwcdg.incrementality.IncrementalStepFuture;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;
import de.unihamburg.informatik.nats.jwcdg.parse.Word;
import de.unihamburg.informatik.nats.jwcdg.predictors.TurboWrapper;
import de.unihamburg.informatik.nats.jwcdg.transform.ConstraintViolation;
import io.gitlab.nats.deptreeviz.DepTree;
import io.gitlab.nats.deptreeviz.PopupEvent;
import io.gitlab.nats.deptreeviz.PopupListener;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.apache.batik.dom.util.DOMUtilities;
import org.w3c.dom.svg.SVGDocument;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The component of the DepTreeViewer tool that handles interactions with the GUI has the
 * subcomponents DepTreeInteractor for the interactions with the DepTree itself and DepTreeNavigator
 * for the interactions with the tree that helps to navigate between different parses
 *
 * @author Sven Zimmer, Arne Köhn and Christine Köhn
 */
public class DepTreeDisplayInteractor extends AbstractDisplayInteractor
{
    public DepTreeDisplay _GUI;
    private DepTreeInteractor _dtI;
    private DepTreeNavigator _nav = new DepTreeNavigator();
    private IncrementalStepFuture _currentStep;
    private String _lastInput = "";
    private boolean _wasComplete = false;
    private boolean _turbo = false;
    private TurboWrapper _tw = null;
    private ArrayList<String> _lastParsedWords = new ArrayList<>();
    //	private File _workingDir = FileService.currentDir();
    private String _currentFilename = "";
    private static ExecutorService _executorServiceDrawing = Executors
        .newSingleThreadScheduledExecutor();
    private static ExecutorService _executorServiceParsing = Executors
        .newSingleThreadScheduledExecutor();
    //	private int _currentStepID = 0;

    public DepTreeDisplayInteractor()
    {
        _dtI = new DepTreeInteractor();
        _dtI.addPopupListener(new PopupListener()
        {
            @Override
            public void makePopup(PopupEvent e)
            {
                makeDepTreePopup(e);
            }

            @Override
            public void setIndex(int index)
            {
            }
        });

        _nav.addNavSelectListener(e -> {
            DepTreeNavNode depTreeNavNode = (DepTreeNavNode) e._navNode;
            _currentFilename = depTreeNavNode._content._fileName;
            String dirname = depTreeNavNode._content._directory;
            if (dirname != null)
            {
                FileService.setWorkingDir(new File(dirname));
            }
            else
            {
                // _workingDir = new File(".");
                FileService.setWorkingDir(FileService.currentDir());
            }
            DecoratedParse navParse = depTreeNavNode._content._parse;
            Map<Integer, ArrayList<String>> markedNodes = depTreeNavNode._content._markedNodes;
            if (_dtI.getDecParse() != navParse)
            {
                drawParse(_dtI, navParse, markedNodes);
            }
        });
    }

    //	/**
    //	 * Sets _workingDir by getting it from wDir.bin in LocalData.
    //	 * If a directory with this name doesn't exist create one.
    //	 * Used during initialisation.
    //	 *
    //	 */
    //	private void setupWorkingDirs() {
    //		_workingDir = FileService.getWorkingDir();
    //	}

    /**
     * Used during initialisation.
     *
     * @param dt
     * @param parser
     * @param parse
     */
    public void setFields(DepTree<DecoratedParse, Word> dt,
            IncrementalFrobber parser, Parse parse)
    {
        _dtI.setParser(parser);
        _dtI.addDTRefreshListener(isChanged -> {
            if (isChanged)
            {
                _nav.getCurrentNode()._content._isChanged = true;
            }
            if (_GUI != null)
            {
                _GUI.setNodesScrollPane(_dtI._dt);
                // _GUI.getSaveCDAButton().setEnabled(isChanged);
                _GUI.getSaveItem()
                    .setEnabled(isChanged);
                updateTable();
            }
        });

        DecoratedParse dParse = null;
        if (parse != null)
        {
            dParse = decorate(parse);
        }
        _dtI.setFields(dt, parser, dParse);
    }

    /**
     * Used during initialisation.
     *
     * @param e
     */
    private void makeDepTreePopup(PopupEvent e)
    {
        makeDepTreePopup(e, _dtI, _GUI);
    }

    @Override
    protected void handleFocus(DepTreeInteractor dtI)
    {
    }

    /**
     * Used during initialisation.
     */
    @Override
    public void set_GUI(AbstractDisplay gui)
    {
        init_GUI(gui);
        _GUI = (DepTreeDisplay) gui;
        //		System.out.println(FileService.currentDir().getAbsolutePath());
        setHelpText(new File(FileService.currentDir()
            .getAbsolutePath() + File.separator + "resources" + File.separator
                + "help dtv.txt"), _GUI);
        _GUI.setNavTree(_nav.getTree());

        _GUI.getSaveItem()
            .addActionListener(arg0 -> {
                if (_dtI._dt.getNodesCanvas() != null)
                {
                    saveCDA();
                }
            });

        _GUI.getSaveAsItem()
            .addActionListener(arg0 -> {
                if (_dtI._dt.getNodesCanvas() != null)
                {
                    saveAsCDA();
                }
            });

        _GUI.getOpenItem()
            .addActionListener(arg0 -> {
                try
                {
                    openCDA();
                }
                catch (RecognitionException e)
                {
                    _GUI.showErrorMessage(String
                        .format("Could not load file because it is ill-formed"
                                + ". Make sure the file is a valid CDA file.\n%s",
                                e.toString()));
                }
            });

        _GUI.getExportItem()
            .addActionListener(arg0 -> {
                if (_dtI._dt.getNodesCanvas() != null)
                {
                    saveSVG();
                }
            });

        _GUI.getButton(AbstractDisplay.PRINT_BUTTON)
            .addActionListener(arg0 -> {
                if (_dtI._dt.getNodesCanvas() != null)
                {
                    System.out.println(_dtI.getDecParse()
                        .print());
                }
            });

        _GUI.getButton(AbstractDisplay.LEVEL_BUTTON)
            .setText(_dtI._dt.getCurrentLevel());
        _GUI.getButton(AbstractDisplay.LEVEL_BUTTON)
            .addActionListener(arg0 -> {
                if (_dtI._dt.getNodesCanvas() != null)
                {
                    String level = _dtI._dt.getCurrentLevel();
                    int index = _dtI.getDecParse()
                        .getLevels()
                        .indexOf(level);
                    if (index == _dtI.getDecParse()
                        .getLevels()
                        .size() - 1)
                    {
                        index = 0;
                    }
                    else
                    {
                        index++;
                    }
                    _dtI._dt.setCurrentLevel(_dtI.getDecParse()
                        .getLevels()
                        .get(index));
                    _GUI.getButton(AbstractDisplay.LEVEL_BUTTON)
                        .setText(_dtI._dt.getCurrentLevel());
                    _dtI.redraw(null, null, false);
                }
            });

        _GUI.getButton(AbstractDisplay.CLEAR_TEXTFIELD_BUTTON)
            .addActionListener(arg0 -> {
                resetParsing();
                _GUI.getParseField()
                    .setText(null);
                _GUI.enableSaveItems(false);
            });

        _GUI.getButton(AbstractDisplay.PARSE_BUTTON)
                .addActionListener(e -> parse(_GUI.getButton(AbstractDisplay.PARSE_BUTTON)));

        _GUI.getParseField()
            .getDocument()
            .addDocumentListener(new DocumentListener()
            {

                @Override
                public void insertUpdate(DocumentEvent e)
                {
                    changedUpdate(e);
                }

                @Override
                public void removeUpdate(DocumentEvent e)
                {
                    changedUpdate(e);
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    if (_dtI._dt.isParsingIncrementally()) {
                        parse(_GUI.getParseField());
                    }
                }
            });

        _GUI.getParseField().addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
                if (keyEvent.getKeyCode() == keyEvent.VK_ENTER) {
                    parse(_GUI.getParseField());
                }
            }
        });

        _GUI.getConstraintsTable()
                .getSelectionModel()
                .addListSelectionListener(e -> handleValueChanged(e, _dtI, _GUI));

        _dtI.getParser()
                .registerObserver(parse -> _executorServiceDrawing
                        .execute(new DepTreeDisplayDrawParseRun(parse)));

        _GUI.getConstraintButton()
            .addActionListener(e -> searchConstraint());
    }

    /**
     * Activates or deactivates use of the turbo wrapper for parsing when entering a sentence into
     * the parse field. First activation takes some time.
     */

    protected void activateTurboMaybe(Configuration config)
    {
        String libTurboPath = config.getString("libturbo_path");
        if (libTurboPath.length() == 0) return;
        _turbo = true;
        if (_tw == null)
        {
            System.load(libTurboPath);
            _tw = new TurboWrapper(config);
        }
    }

    protected void draw(DecoratedParse parse)
    {
        _dtI.setDecParse(parse);
        _dtI._selection.setHasTarget(false);
        _dtI._dt.setMarkedNodes(null);
        _dtI.draw(false);
        _GUI.setNodesScrollPane(_dtI._dt);
        String sentence = DepTreeNavigator.getSentence(parse);
        NavNodeContent content = new NavNodeContent(parse, false);
        _nav.add(content, String.valueOf(_nav.getDirSize(sentence)), sentence);
        updateTable();
    }

    /**
     * Updates the constraints table.
     */
    @Override
    protected void updateTable()
    {
        updateTable(_dtI, _GUI);
    }

    /**
     * @param parse
     * @param directory the name of the directory in the NavTree where the parse is inserted.
     */
    public void add(Parse parse, String directory)
    {
        NavNodeContent content = new NavNodeContent(decorate(parse), false);
        _nav.add(content, directory);
        _GUI.enableSaveItems(true);
    }

    /**
     * @param parse
     * @param directory   the name of the directory in the NavTree where the parse is inserted.
     * @param markedNodes
     */
    public void add(Parse parse, String directory,
            Map<Integer, ArrayList<String>> markedNodes)
    {
        NavNodeContent content = new NavNodeContent(decorate(parse), false,
                markedNodes);
        _nav.add(content, directory);
        _GUI.enableSaveItems(true);
    }

    public DecoratedParse decorate(Parse parse)
    {
        Parser parser = _dtI.getParser();
        return decorate(parse, parser);
    }

    /**
     * Exports the parse of the currently displayed DepTree as a SVG file. Its name is chosen by the
     * user in a window.
     */
    public void saveSVG()
    {
        // bug: formating
        File file = selectFile("svg", true);
        if (file != null)
        {
            try
            {
                Writer w = new OutputStreamWriter(new FileOutputStream(file),
                        "UTF-8");
                PrintWriter writer = new PrintWriter(w);
                // writer.write(
                // "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
                // writer.write ("<!DOCTYPE svg PUBLIC '");
                // writer.write (SVGConstants.SVG_PUBLIC_ID);
                // writer.write ("' '");
                // writer.write (SVGConstants.SVG_SYSTEM_ID);
                // writer.write ("'>\n\n");
                // graph.stream(graph.getRoot(),writer);
                // } catch (SVGGraphics2DIOException e) {
                // e.printStackTrace();
                SVGDocument sd = _dtI._dt.getNodesCanvas()
                    .getSVGDocument();
                DOMUtilities.writeDocument(sd, writer);
                writer.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * Saves the parse of the currently displayed DepTree into a CDA file of the actual name.
     */
    protected void saveCDA()
    {
        if (_currentFilename.equals(""))
        {
            saveAsCDA();
        }
        else
        {
            File file = new File(FileService.getWorkingDir()
                .getAbsolutePath() + File.separator + _currentFilename
                    + ".cda");
            writeToFile(_dtI.getDecParse(), file);
            _nav.getCurrentNode()._content._isChanged = false;
            _GUI.getSaveItem()
                .setEnabled(false);
        }
    }

    /**
     * Saves the parse of the currently displayed DepTree into a selected CDA file.
     */
    protected void saveAsCDA()
    {
        File file = selectFile("cda", true);
        if (file != null)
        {
            writeToFile(_dtI.getDecParse(), file);
            _nav.getCurrentNode()._content._isChanged = false;
            _GUI.getSaveItem()
                .setEnabled(false);
        }
    }

    /**
     * Generates a user dialog to select a file either for saving or loading.
     *
     * @param extension the extension for the filename
     * @param save      true if saving a file, false if loading
     * @return the selected filename or null in case of a canceling
     */
    private File selectFile(String extension, boolean save)
    {
        JFileChooser chooser = new JFileChooser();
        FileFilter filter = new FileNameExtensionFilter(extension + " files",
                extension);
        chooser.addChoosableFileFilter(filter);
        chooser.setFileFilter(filter);
        chooser.setCurrentDirectory(FileService.getWorkingDir());
        chooser.setSelectedFile(new File(_currentFilename + "." + extension));
        String filename;
        boolean finished;
        File result;
        do
        {
            filename = null;
            finished = true;
            result = null;
            int choise;
            if (save)
            {
                choise = chooser.showSaveDialog(_GUI);
            }
            else
            {
                choise = chooser.showOpenDialog(_GUI);
            }
            if (choise == JFileChooser.APPROVE_OPTION)
            {
                File selected = chooser.getSelectedFile();
                filename = selected.getPath();
                result = new File(filename);
                // System.out.println(chooser.getDescription(result));
                if (save)
                {
                    // problem: you don't want to add an extension if the user
                    // chooses a file with a different one with the mouse
                    // if (! result.toLowerCase().endsWith("." + extension)){
                    // result = result + "." + extension;
                    // }
                    if (result.exists())
                    {
                        // TODO JOptionPane to DepTreeDisplay
                        int answer = JOptionPane.showConfirmDialog(_GUI,
                                "File " + selected.getName()
                                        + " already exists. Overwrite?",
                                "Warning", JOptionPane.YES_NO_OPTION,
                                JOptionPane.WARNING_MESSAGE);
                        finished = answer == JOptionPane.YES_OPTION;
                    }
                }
                File parentDir = selected.getParentFile();
                try
                {
                    if (finished && !FileService.getWorkingDir()
                        .getCanonicalPath()
                        .equals(parentDir.getCanonicalPath())
                            && extension.equals("cda"))
                    {
                        FileService.setWorkingDir(parentDir);
                        //						updateWorkDirs();
                    }
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
        while (!finished);
        if (result != null && extension.equals("cda"))
        {
            _currentFilename = FileService.removeExtension(result.getName());
            if (save)
            {
                _nav.getCurrentNode()._content._directory = result.getParent();
                _nav.getCurrentNode()._content._fileName = _currentFilename;
            }
        }
        return result;
    }

    //	/**
    //	 * Puts _workingDir into the file wDir.bin in the directory LocalData for
    //	 * remembering it after closing the DepTreeViewer.
    //	 *
    //	 */
    //	private void updateWorkDirs() {
    //		FileService.setWorkingDir(_workingDir);
    //	}
    //

    /**
     * Generates a decorated parse from a CDA file.
     */
    protected void openCDA() throws RecognitionException
    {
        File file = selectFile("cda", false);
        loadCDAfromFile(file);
    }

    /**
     * @param file
     * @throws RecognitionException file could not be parsed
     */
    protected void loadCDAfromFile(File file) throws RecognitionException
    {
        if (file != null)
        {
            String fileContent = FileService.loadText(file);
            _dtI.setDecParse(decorate(Parse.fromCda(fileContent)));
            _dtI._selection.setHasTarget(false);
            _dtI._dt.resetMarkedNodes();
            _dtI.draw(false);
            _GUI.setNodesScrollPane(_dtI._dt);
            _GUI.repaint();
            _nav.clearTree();
            //				System.out.println("before: " + FileService.getWorkingDir());
            FileService.setWorkingDir(file.getParentFile());
            //				System.out.println("Loading Dir: " + FileService.getWorkingDir() + " - file: " + file.getAbsolutePath());
            _currentFilename = FileService.removeExtension(file.getName());
            NavNodeContent content = new NavNodeContent(_dtI.getDecParse(),
                    file.getParent(), _currentFilename, false);
            _nav.add(content, file.getName());
            _GUI.getParseField()
                .setText(null);
            _GUI.enableSaveItems(true);
            _nav.getCurrentNode()._content._isChanged = false;
        }
    }

    /**
     * Parses the input in the parse field then draws the result.
     *
     * @param source GUI object triggering the parse action
     */
    private void parse(JComponent source) {
        boolean sourceIsButtonOrEnter = _GUI.getButton(AbstractDisplay.PARSE_BUTTON).equals(source)
                || _GUI.getParseField().equals(source);
        String inputText = _GUI.getParseField()
                .getText();
        boolean isComplete = !_GUI.getBox(AbstractDisplay.PARTIAL_CHECKBOX)
                .isSelected();
        boolean inputChanged = !_lastInput.equals(inputText);
        // Is changing from partial to complete (or vice versa) the only change?
        boolean buttonOrEnterCompletenessChange = !inputChanged && sourceIsButtonOrEnter
                && (isComplete != _wasComplete);
        if (!_lastInput.equals(inputText) || buttonOrEnterCompletenessChange) {
            _lastInput = inputText;
            ArrayList<String> inputWords = makeWordsList(inputText);
            if (!inputWords.equals(_lastParsedWords)
                    || buttonOrEnterCompletenessChange && inputWords.size() > 0) {
                // re-parse when
                // - extending a previously marked completed parse
                // - a button press or pressing return triggered re-parsing and changing
                // complete/partial is only change
                if (_wasComplete || buttonOrEnterCompletenessChange) {
                    // pretend that previous input was empty;
                    // parse whole sentence again to reach the same state as if the last token
                    // completed the sentence or as if the sentence had never been marked completed
                    _lastParsedWords = new ArrayList<>();
                    _wasComplete = isComplete;
                }
                _executorServiceParsing.execute(new DepTreeDisplayParsingRun(
                        new ArrayList<>(inputWords),
                        new ArrayList<>(_lastParsedWords), isComplete));
                _lastParsedWords = inputWords;
            }
        }
    }

    /**
     * Turns a sentence into a list of it's words.
     *
     * @param inputText
     * @return
     */
    private ArrayList<String> makeWordsList(String inputText)
    {
        ArrayList<String> result = new ArrayList<>(
                Arrays.asList(inputText.split("[ ]+")));
        // if the user hasn't finished writing the last word by typing a space,
        // then it doesn't count
        if (!result.isEmpty() && !inputText.endsWith(" "))
        {
            result.remove(result.size() - 1);
        }
        return result;
    }

    // TODO improve commentary

    /**
     * Checks if the first word in restWords is equal to the one in lastInputWords. If it is, it
     * then puts it into equalWords else incrementally removes the directories in the navigation
     * tree that end with the words in lastInputWords and incrementally parses the words from
     * restWords.
     *
     * @param equalWords
     * @param restWords
     * @param lastInputRestWords
     * @param isStillEqual
     * @param isComplete
     */
    protected void parse(ArrayList<String> equalWords,
            ArrayList<String> restWords, ArrayList<String> lastInputRestWords,
            boolean isStillEqual, boolean isComplete)
    {
        if (isStillEqual)
        {
            // if a word has been deleted
            if (restWords.isEmpty())
            {
                parse(equalWords, restWords, lastInputRestWords, false,
                        isComplete);
            }
            else
            {
                String word = restWords.get(0);
                restWords.remove(0);
                // if a new word has been entered
                if (lastInputRestWords.isEmpty())
                {
                    restWords.add(0, word);
                    parse(equalWords, restWords, false, isComplete);
                }
                else
                {
                    String lastWord = lastInputRestWords.get(0);
                    // if the first word in both lists are still equal
                    if (word.equals(lastWord))
                    {
                        equalWords.add(word);
                        lastInputRestWords.remove(0);
                        parse(equalWords, restWords, lastInputRestWords, true,
                                isComplete);
                    }
                    else
                    {
                        restWords.add(0, word);
                        parse(equalWords, restWords, lastInputRestWords, false,
                                isComplete);
                    }
                }
            }
        }
        else
        {
            // How many parses are now obsolete?
            int size = lastInputRestWords.size();
            // Unfortuantely, we do not have a history with parsing steps, so we
            // have to create a new parsing step, even if the only deleted words
            // are at the end. Therefore, we have to have the last word in restWords.
            if (!equalWords.isEmpty() && restWords.isEmpty())
            {
                // mark the current parse as obsolete as well because it will  be parsed again.
                size++;
                restWords.add(0, equalWords.get(equalWords.size() - 1));
                equalWords.remove(equalWords.size() - 1);
            }
            removeDirs(size);
            parse(equalWords, restWords, true, isComplete);
        }
    }

    /**
     * Removes directories from the NavTree.
     *
     * @param size the minimum size of the directories that are not removed
     */
    private void removeDirs(int size)
    {
        for (int i = 0; i < size; i++)
        {
            removeDir(_lastParsedWords.subList(0, _lastParsedWords.size() - i));
        }
    }

    private void parse(ArrayList<String> parsedWords,
            ArrayList<String> restWords, boolean parsingIsReset,
            boolean isComplete)
    {
        if ((parsedWords.isEmpty() && !_lastParsedWords.isEmpty()))
        {
            resetParsing();
        }
        if (!restWords.isEmpty())
        {
            String word = restWords.get(0);
            restWords.remove(0);
            if (_turbo)
            {
                parsedWords.add(word);
                makeParseStepTurbo(parsedWords);
            }
            else
            {
                boolean isFinalStep = isComplete && (restWords.size() == 0);
                if (parsingIsReset)
                {
                    _currentStep = null;
                    makeParseStep(new ChunkIncrement(word, parsedWords),
                            isFinalStep);
                }
                else
                {
                    makeParseStep(new ChunkIncrement(word), isFinalStep);
                }
                parsedWords.add(word);
            }
            parse(parsedWords, restWords, false, isComplete);
        }
    }

    private void makeParseStep(ChunkIncrement nextChunk, boolean isFinalStep)
    {
        IncrementalStep result;
        // check if we are parsing at the moment
        // try {
        // waitForRendering();
        // } catch (InterruptedException e) {
        // e.printStackTrace();
        // }
        if (_currentStep == null)
        {
            // no -> create initial step
            result = _dtI.getParser()
                .makeInitialStep();
        }
        else
        {
            // yes -> stop it, save the result in savedStates
            _currentStep.pleaseFinish();
            try
            {
                result = _currentStep.get();
            }
            catch (InterruptedException | ExecutionException e1)
            {
                result = _dtI.getParser()
                    .makeInitialStep();
                e1.printStackTrace();
            }
        }
        _currentStep = _dtI.getParser()
            .continueParsingAsync(result, nextChunk, isFinalStep, null);
    }

    private void makeParseStepTurbo(ArrayList<String> words)
    {
        StringBuilder sb = new StringBuilder("");
        for (String word : words)
        {
            sb.append(word);
            sb.append("\n");
        }
        // if normal parser is still working, then wait until it is finished,
        // throw the result away
        if (_currentStep != null)
        {
            _currentStep.pleaseFinish();
            _currentStep = null;
        }
        String result;
        // result = _tw.tagAndParse(sb.toString());
        result = _tw.parseIncrementally(sb.toString());
        Parse p = Parse.fromConll(result);
        DecoratedParse dp = decorate(p);
        draw(dp);
    }

    /**
     * Removes a directory in the NavTree by it's name.
     *
     * @param list list of the words in the name of the directory.
     */
    private void removeDir(List<String> list)
    {
        _currentStep = null;
        StringBuilder result = new StringBuilder();
        for (String word : list)
        {
            result.append(word)
                .append(" ");
        }
        _nav.removeDir(result.toString());
    }

    /**
     * Used when the parse field has been cleared.
     */
    private void resetParsing()
    {
        _currentStep = null;
        _nav.clearTree();
    }

    @Override
    public void draw()
    {
        // if (_dtI._dt.getNodesCanvas() != null){
        _dtI.draw(false);
        // }
    }

    private void searchConstraint()
    {
        String c = _GUI.getConstraintInput()
            .getText();
        // Check Constraint
        try
        {
            Constraint constraint = checkConstraint(c);
            // Find constraint in parse
            List<ConstraintViolation> violations = _dtI.getDecParse()
                .evalConstraint(constraint);
            for (ConstraintViolation v : violations)
            {
                if (v != null)
                {
                    // create deptree or change deptree
                    Map<Integer, ArrayList<String>> nodes = new TreeMap<>();
                    ArrayList<String> markedLevels = new ArrayList<>();
                    markedLevels.add("SYN");

                    // Edge 1
                    GraphemeNode dependent = v.getLV1()
                        .getDependentGN();
                    GraphemeNode regent = v.getLV1()
                        .getRegentGN();
                    if (!regent.isNil())
                    {
                        nodes.put(dependent.getNo(), markedLevels);
                    }
                    // Edge 2
                    if (v.getLV2() != null)
                    {
                        dependent = v.getLV2()
                            .getDependentGN();
                        regent = v.getLV2()
                            .getRegentGN();
                        if (!regent.isNil())
                        {
                            nodes.put(dependent.getNo(), markedLevels);
                        }
                    }
                    _dtI._dt.resetMarkedNodes();
                    _dtI._dt.setMarkedNodes(nodes);
                    _dtI.draw(true);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private Constraint checkConstraint(String constraint) throws Exception
    {
        // Check if the constraint has a signature
        if (!constraint.contains(":"))
        {
            // checkFormula(constraint);

            // complete the constraint by adding a signature to the
            // given formula
            StringBuilder sb = new StringBuilder();
            if (constraint.contains("Y") && constraint.contains("X"))
            {
                sb.append("{X:SYN,Y:SYN} :");
                sb.append("'")
                    .append(System.currentTimeMillis())
                    .append("' : ");
                sb.append("proj : 1.0 : ");
            }
            else if (constraint.contains("X"))
            {
                sb.append("{X:SYN} :");
                sb.append("'")
                    .append(System.currentTimeMillis())
                    .append("' : ");
                sb.append("proj : 1.0 : ");
            }

            constraint = sb.toString() + constraint;
        }

        if (!constraint.endsWith(";"))
        {
            constraint += ";";
        }

        jwcdggrammarParser cParser = new jwcdggrammarParser(
                new CommonTokenStream(new jwcdggrammarLexer(
                        new ANTLRStringStream(constraint))));

        Constraint con;
        try
        {
            con = cParser.constraint().c;
            con.formula = new FormulaImpl(con.formula, new FormulaFalse());

        }
        catch (RecognitionException re)
        {
            throw new Exception("Constraint not recognized.\n"
                    + cParser.getErrorHeader(re) + " "
                    + cParser.getErrorMessage(re, jwcdggrammarParser.tokenNames)
                    + "-" + re.token.getText());
        }
        return con;
    }

    /**
     * @return the DepTree that contains the  options that are used. There is only one in the
     * DepTreeViewer anyways.
     */
    @Override
    protected DepTree getDTwithOptions()
    {
        return _dtI._dt;
    }

    @Override
    public void setOptions()
    {
        setOptions(_GUI._prefD, _dtI._dt);
    }

    /**
     * Runnable for parsing the input
     *
     * @author Christine Köhn
     */
    public class DepTreeDisplayParsingRun implements Runnable
    {

        private ArrayList<String> _input;
        private ArrayList<String> _lastInput;
        private boolean _isComplete;

        public DepTreeDisplayParsingRun(ArrayList<String> input,
                ArrayList<String> lastInput, boolean isComplete)
        {
            _input = input;
            _lastInput = lastInput;
            _isComplete = isComplete;
        }

        @Override
        public void run()
        {
            parse(new ArrayList<>(), _input, _lastInput, true, _isComplete);
        }
    }

    /**
     * Runnable for drawing a DecoratedParse
     *
     * @author Christine Köhn
     */
    public class DepTreeDisplayDrawParseRun implements Runnable
    {

        private Parse _parse;

        public DepTreeDisplayDrawParseRun(Parse parse)
        {
            _parse = parse;
        }

        @Override
        public void run()
        {
            DecoratedParse decoratedParse = (DecoratedParse) _parse;
            //			int id = getStepID();
            //			System.out.println("before");
            while (!_currentStep.isDone())
            {
                try
                {
                    Thread.sleep(10);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
            //			System.out.println("after");
            //			if (_dtDI.getDTwithOptions().getDecParse() != null && _parse != null){
            //				System.out.println(id + " " + _currentStepID + " " + _dtDI.getDTwithOptions().getDecParse().toString() + " " + (decoratedParse.toString()));
            //			}
            draw(decoratedParse);
            _GUI.enableSaveItems(true);
            _GUI.getSaveItem()
                .setEnabled(true);
        }

    }

    //	private int getStepID(){
    //		_currentStepID++;
    //		return _currentStepID;
    //	}

}
