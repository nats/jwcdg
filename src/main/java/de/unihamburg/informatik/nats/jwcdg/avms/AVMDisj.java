package de.unihamburg.informatik.nats.jwcdg.avms;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import de.unihamburg.informatik.nats.jwcdg.input.Configuration;

public class AVMDisj extends AVM {
	private static final long serialVersionUID = -60491128712243505L;

	private List<AVM> list;
	private int coordinationIndex=0;

	public List<AVM> getList() {
		return list;
	}
	
	public AVMDisj(List<AVM> list) {
		this.list = list;
	}
	
	/**
	 * gets Index for Coordination; 0->not coordinated
	 * @return
	 */
	public int getCoordinationIndex() {
		return coordinationIndex;
	}

	public void setCoordinationIndex(int coordinationIndex) {
		this.coordinationIndex = coordinationIndex;
	}

	@Override
	public AVM substituteRefs(Matcher matcher, Configuration config) {
		List<AVM> l2 = new ArrayList<>(list.size());
		for(AVM val : list) {
			l2.add(val.substituteRefs(matcher, config));
		}
		
		return new AVMDisj(l2);
	}
	
	@Override
	public String toString() {
		return "Disj: "+ list.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + coordinationIndex;
		result = prime * result + ((list == null) ? 0 : list.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AVMDisj))
			return false;
		AVMDisj other = (AVMDisj) obj;
		if (coordinationIndex != other.coordinationIndex)
			return false;
		if (list == null) {
			if (other.list != null)
				return false;
		} else if (!list.equals(other.list))
			return false;
		return true;
	}
	
}
