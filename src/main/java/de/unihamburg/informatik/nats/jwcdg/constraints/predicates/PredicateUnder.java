package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.Level;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.TermPeek;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 *  Is A indirectly a modifier of B?
 */
public class PredicateUnder extends Predicate {

	public PredicateUnder(Grammar g) {	
		super("subsumes", g, true, 2, 3, false);
	}

	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {

		LexemeNode ln1, ln2;
		Level level; 

		Term t = args.get(0);
		AVM val = t.eval(lg, context, binding);

		if (    ! (t   instanceof TermPeek) ||
				! (val instanceof AVMLexemNode)) {
			throw new PredicateEvaluationException("type mismatch (1st arg) for `under'");
		}

		ln1 = ((AVMLexemNode)val).getLexemeNode();

		if (!ln1.spec()) {
			return false;
		}

		AVM val2 = args.get(1).eval(lg, context, binding);
		if (!(val2 instanceof AVMLexemNode)) {
			throw new PredicateEvaluationException("mismatch (2nd arg) for `under'");
		}

		ln2 = ((AVMLexemNode)val2).getLexemeNode();
		if (!ln2.spec()) {
			return false;
		}

		if(args.size() == 3) {
			AVM val3 = args.get(2).eval(lg, context, binding);
			if (!(val3 instanceof AVMString)) {
				throw new PredicateEvaluationException("type mismatch (3rd arg) for `under'");
			}

			/* find level */
			String levelName = ((AVMString)val3).getString();
			level = grammar.findLevel(levelName);
			if (level == null) {
				throw new PredicateEvaluationException(String.format(
						"no level `%s'", levelName));
			}
		} else {
			level = binding[((TermPeek)args.get(0)).getVarIndex()].getLevel();
		}

		return predUnderImpl(ln1, ln2, lg, context, level);
	}

	private boolean predUnderImpl(LexemeNode ln1, LexemeNode ln2,
			LexemeGraph lg, Context context, Level level) {
		int a = ln1.getArc().from;
		int b = ln2.getArc().from;
		int j = 0;
		int noOflevels = grammar.getNoOfLevels();
		LevelValue lv;

		//int levelno = ((VarInfo)evalCurrentConstraint->vars->item)->level->no;

		int lgMax = lg.getMax();
		do {
			lv = context.getLV(noOflevels * a + level.getNo());
			if(lv == null || !lv.getRegentGN().spec() ) {
				/* failure */
				return false;
			}
			if(lv.getRegentGN().getArc().from == b) {
				/* success */
				return true;
			}

			if(j > lgMax) {
				/* cycle */
				return false;
			}
			a = lv.getRegentGN().getArc().from;
			j++;
		} while(true);
	}

}
