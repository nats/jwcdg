package de.unihamburg.informatik.nats.jwcdg.gui;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JSlider;

import de.unihamburg.informatik.nats.jwcdg.Parser;
import de.unihamburg.informatik.nats.jwcdg.gui.AnnoNavigator.AnnoNavNode;
import de.unihamburg.informatik.nats.jwcdg.incrementality.IncrementalFrobber;
import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;
import de.unihamburg.informatik.nats.jwcdg.parse.Word;
import io.gitlab.nats.deptreeviz.DepTree;
import io.gitlab.nats.deptreeviz.PopupEvent;
import io.gitlab.nats.deptreeviz.PopupListener;

/**
 * The component of the AnnoViewer tool that handles interactions with the GUI
 * has the subcomponents DepTreeInteractor for the interactions with the DepTree
 * itself and AnnoNaviagator for the interactions with the tree that helps to
 * navigate between different parses
 * 
 * @author Sven Zimmer and Arne Köhn
 * 
 */
public class AnnoDisplayInteractor extends AbstractDisplayInteractor {
	public AnnoDisplay _GUI;
	private ArrayList<DepTreeInteractor> _dtIs =
			new ArrayList<>();
	private AnnoNavigator _nav = new AnnoNavigator();
	// the files are saved in the same directory for each DepTreeInteractor
	public List<String> _dirs = new ArrayList<>();
	// the name they are saved with are the same for each DepTreeInteractor, and
	// different for each navigator selection
	public String _fileName = "";
	private int _dirCount;
	// contains for each directory the list of the filenames of the deptrees
	// that
	// have been marked as done
	public ArrayList<ArrayList<String>> _doneLists =
			new ArrayList<>();
	public HashMap<String, Boolean> _savedFiles =
			new HashMap<>();

	public AnnoDisplayInteractor(int dirCount) {
		_dirCount = dirCount;
		for (int i = 0; i < _dirCount; i++) {
			DepTreeInteractor dtI = new DepTreeInteractor();
			_dtIs.add(dtI);
			PopupListener listener = new PopupListener() {
				int _index;

				@Override
				public void makePopup(PopupEvent e) {
					makeDepTreePopup(e, _index);
				}

				@Override
				public void setIndex(int index) {
					_index = index;
				}
			};
			listener.setIndex(_dtIs.indexOf(dtI));
			dtI.addPopupListener(listener);
		}
		_nav.addNavSelectListener(e -> {
            for (DepTreeInteractor dtI : _dtIs) {
                AnnoNavNode annoNavNode = (AnnoNavNode) e._navNode;
                int index = _dtIs.indexOf(dtI);
                NavNodeContent content =
                            annoNavNode._contentList.get(index);
                DecoratedParse navParse = content._parse;
                Map<Integer, ArrayList<String>> markedNodes =
                            content._markedNodes;
                _fileName = content._fileName;
                if (dtI.getDecParse() != navParse) {
                    drawParse(dtI, navParse, markedNodes);
                    // always focus on the navtree so the navkeys always
                    // work
                    dtI._dt.getNodesCanvas()
                                .addFocusListener(new FocusListener() {
                                    @Override
                                    public void
                                                focusGained(FocusEvent arg0) {
                                        _GUI.getNavTree()
                                                    .requestFocusInWindow();
                                    }

                                    @Override
                                    public void focusLost(FocusEvent arg0) {
                                    }
                                });
                }
            }
        });

	}

	/**
	 * Used during initialization.
	 * 
	 * @param dts
	 * @param parser
	 * @param dirs
	 */
	public void setFields(List<DepTree<DecoratedParse, Word>> dts,
				IncrementalFrobber parser,
				List<String> dirs) {
		for (final DepTreeInteractor dtI : _dtIs) {
			dtI.setParser(parser);
			int index = _dtIs.indexOf(dtI);
			dtI.addDTRefreshListener(isChanged -> {
                int index1 = _dtIs.indexOf(dtI);
                if (isChanged) {
                    _nav.getCurrentNode()._contentList.get(index1)._isChanged =
                                true;
                }
                if (_GUI != null) {
                    _GUI.setNodesScrollPane(index1,
                                dts,
                                getDoneList(),
                                makeChangeList());
                    updateTable();
                }
            });
			dtI.setFields(dts.get(index), parser, null);
		}
		_dirs = dirs;
	}

	private ArrayList<Boolean> makeChangeList() {
		ArrayList<Boolean> isChangedList = new ArrayList<>();
		for (NavNodeContent content : _nav.getCurrentNode()._contentList) {
			isChangedList.add(content._isChanged);
		}
		return isChangedList;
	}

	private ArrayList<Boolean> getDoneList() {
		ArrayList<Boolean> isDoneList = new ArrayList<>();
		int index = 0;
		for (@SuppressWarnings("unused")
		String i : _dirs) {
			boolean isDone = _doneLists.get(index).contains(_fileName);
			isDoneList.add(isDone);
			index++;
		}
		return isDoneList;
	}

	private void makeDepTreePopup(PopupEvent e, final int dTIIndex) {
		makeDepTreePopup(e, _dtIs.get(dTIIndex), _GUI);
	}

	protected void handleFocus(DepTreeInteractor dtI) {
		// always focus on the NavTree so the NavKeys always work
		dtI._dt.getNodesCanvas().addFocusListener(new FocusListener() {

			@Override
			public void focusGained(FocusEvent arg0) {
				_GUI.getNavTree().requestFocusInWindow();
			}

			@Override
			public void focusLost(FocusEvent arg0) {
			}
		});
		_GUI.getNavTree().requestFocusInWindow();

	}

	public void set_GUI(AbstractDisplay gui) {
		init_GUI(gui);
		_GUI = (AnnoDisplay) gui;
		setHelpText(new File(FileService.currentDir().getAbsolutePath() + File.separator + "resources"+ File.separator + "help av.txt"), _GUI);
		_GUI.setNavTree(_nav.getTree());

		_GUI.getSaveAllItem().addActionListener(arg0 -> {
            boolean savingIsPossible = true;
            for (JButton notDoneButton : _GUI.getNotDoneButtons()){
                int index = _GUI.getNotDoneButtons().indexOf(notDoneButton);
                if (notDoneButton.isEnabled() && makeChangeList().get(index)){
                    savingIsPossible = false;
                }
            }
            if (! savingIsPossible) {
            JOptionPane.showMessageDialog(_GUI,
                        "You must mark the current trees with changes as \"not done\" first.");
        }
            else {
                for (DepTreeInteractor dtI : _dtIs) {
                    if (dtI._dt.getNodesCanvas() != null) {
                        saveCDA(dtI);
                    }
                }
                _GUI.getNavTree().requestFocusInWindow();
            }
        });

		for (JButton saveButton : _GUI.getSaveButtons()) {
			final int index = _GUI.getSaveButtons().indexOf(saveButton);
			saveButton.addActionListener(arg0 -> {
                DepTreeInteractor dtI = _dtIs.get(index);
                boolean savingIsPossible = true;
                JButton notDoneButton = _GUI.getNotDoneButtons().get(index);
                if (notDoneButton.isEnabled() && makeChangeList().get(index)){
                    savingIsPossible = false;
                }
                if (dtI._dt.getNodesCanvas() != null && savingIsPossible) {
                    saveCDA(dtI);
                }
                else {
                    JOptionPane.showMessageDialog(_GUI,
                                "You must mark the tree with as \"not done\" first.");
                }
                _GUI.getNavTree().requestFocusInWindow();
            });
		}

		// _GUI.getCurrentButton().addActionListener(new ActionListener(){
		// @Override
		// public void actionPerformed(ActionEvent arg0){
		// _nav.selectCurrent();
		// }
		// });

		for (final JButton button : _GUI.getSelectTreeButtons()) {
			button.addActionListener(arg0 -> {
                int index = _GUI.getSelectTreeButtons().indexOf(button);
                List<DepTree<DecoratedParse, Word>> dts = new ArrayList<>();
                for (DepTreeInteractor dtI : _dtIs) {
                    dts.add(dtI._dt);
                }
                _GUI.showNodesScrollPanes(index,
                            dts,
                            getDoneList(),
                            makeChangeList());
                _GUI.disableSelectTreeButton();
                _GUI.getNavTree().requestFocusInWindow();
                updateTable();
            });
		}

		for (final JButton button : _GUI.getDoneButtons()) {
			button.addActionListener(e -> {
                if (makeChangeList().contains(true)) {
                    JOptionPane.showMessageDialog(_GUI,
                                "You must save the current tree as CDA first.");
                }
                else {
                    int index = _GUI.getDoneButtons().indexOf(button);
                    _doneLists.get(index).add(_fileName);
                    saveDoneList(index);
                    _dtIs.get(index).refreshDisplay(false);
                }
            });
		}

		for (final JButton button : _GUI.getNotDoneButtons()) {
			button.addActionListener(e -> {
//					if (makeChangeList().contains(true)) {
//						JOptionPane.showMessageDialog(_GUI,
//									"You must save the current trees as CDAs first.");
//					}
//					else {
                    int index = _GUI.getNotDoneButtons().indexOf(button);
                    _doneLists.get(index).remove(_fileName);
                    saveDoneList(index);
                    _dtIs.get(index).refreshDisplay(false);
//					}
            });
		}
		int i = 0;
		for (final JSlider zoomSlider : _GUI.getZoomSliders()) {
			i++;
			zoomSlider.setName("slider " + i);
			zoomSlider.addChangeListener(e -> {
                _GUI.rememberDividerLocation();
                DepTreeInteractor dtI =
                            _dtIs.get(_GUI.getZoomSliders()
                                        .indexOf(zoomSlider));
                dtI._dt.setZoomFactor(Math.pow(2.0,
                            (((double) zoomSlider.getValue()) / 4.0)));
                dtI.redraw(null, null, false);
                // _GUI.resetDividerLocation();
            });
		}

		_GUI.getButton(AbstractDisplay.LEVEL_BUTTON).setText(_dtIs.get(0)._dt.getCurrentLevel());
		_GUI.getButton(AbstractDisplay.LEVEL_BUTTON).addActionListener(arg0 -> {
            for (DepTreeInteractor dtI : _dtIs) {
                if (dtI._dt.getNodesCanvas() != null) {
                    String level = dtI._dt.getCurrentLevel();
                    int index =
                                dtI.getDecParse()
                                            .getLevels()
                                            .indexOf(level);
                    if (index == dtI.getDecParse().getLevels().size() - 1) {
                        index = 0;
                    }
                    else {
                        index++;
                    }
                    dtI._dt.setCurrentLevel(dtI.getDecParse()
                                .getLevels()
                                .get(index));
                    dtI.redraw(null, null, false);
                }
            }
            _GUI.getButton(AbstractDisplay.LEVEL_BUTTON)
                        .setText(_dtIs.get(0)._dt.getCurrentLevel());
        });

		_GUI.getConstraintsTable()
					.getSelectionModel()
					.addListSelectionListener(e -> handleValueChanged(e,
                                _dtIs.get(_GUI._visiblePane - 1),
                                _GUI));

	}

	/**
	 * Saves the strings in the done list into separate lines of a text file.
	 * Creates the file if it doesn't exist. 
	 * 
	 * @param index
	 */
	private void saveDoneList(int index) {
		String dir = _dirs.get(index);
		if (! dir.endsWith("/")) {
			dir = dir + "/";
		}
		File doneFile = new File(dir + "done.txt");
		FileService.listToFile(_doneLists.get(index), doneFile);
	}

	public void add(List<Parse> parses, String navDirectory, String fileName) {
		ArrayList<NavNodeContent> contentList = new ArrayList<>();
		for (Parse parse : parses) {
			contentList.add(new NavNodeContent(decorate(parse),
						navDirectory,
						fileName,
						false));
		}
		_nav.add(contentList, navDirectory);
		_savedFiles.put(fileName, true);
	}

	// public ArrayList <DecoratedParse> decorate(List <Parse> parses){
	// ArrayList <DecoratedParse> result = new ArrayList <DecoratedParse>();
	// for (Parse parse : parses){
	// result.add(decorate(parse));
	// }
	// return result;
	// }

	public DecoratedParse decorate(Parse parse) {
		// assumption: all dtIs have the same parser
		Parser parser = _dtIs.get(0).getParser();
		return decorate(parse, parser);
	}

	protected void saveCDA(DepTreeInteractor dtI) {
		int index = _dtIs.indexOf(dtI);
		String dir = _dirs.get(index);
		DecoratedParse decParse = dtI.getDecParse();
		if (decParse != null) {
			if (! dir.endsWith("/")) {
				dir = dir + "/";
			}
			writeToFile(decParse, dir + _fileName);
			_savedFiles.put(_fileName, true);
			_nav.getCurrentNode()._contentList.get(index)._isChanged = false;
			_dtIs.get(index).refreshDisplay(false);
		}
	}

	public void draw() {
		for (DepTreeInteractor dtI : _dtIs) {
			dtI.draw(false);
		}
		updateTable();
	}

	protected void activateTurbo(boolean turbo) {
	}

	protected void updateTable() {
		if (_GUI._visiblePane != 0) {
			DepTreeInteractor dtI = _dtIs.get(_GUI._visiblePane - 1);
			updateTable(dtI, _GUI);
		}
	}

	public DepTree getDTwithOptions() {
		return _dtIs.get(0)._dt;
	}

	public void setOptions() {
		for (DepTreeInteractor dtI : _dtIs) {
			setOptions(_GUI._prefD, dtI._dt);
		}
	}

	public int getDirCount() {
		return _dirCount;
	}

	public void setDirCount(int dirCount) {
		_dirCount = dirCount;
	}

}
