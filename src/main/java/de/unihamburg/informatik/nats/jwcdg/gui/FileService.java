package de.unihamburg.informatik.nats.jwcdg.gui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Deals with all file operations.
 * 
 * @author 3zimmer
 *
 */
public class FileService {

	/**
	 * Loads the text content from a File
	 * 
	 * @param file
	 * @param encoding
	 * @return
	 */
	public static String loadText(File file, String encoding) {
		StringBuilder fileContent = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding));
			String line;
			while ((line = reader.readLine()) != null) {
				fileContent.append(line);
				fileContent.append(System.getProperty("line.separator"));
			}
			reader.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	return fileContent.toString();
	}

	/**
	 * Loads the text content from a File into a String using standard encoding
	 * 
	 * @param file
	 * @return
	 */
	public static String loadText(File file) {
		return loadText(file, "UTF-8");
	}
	
	/**
	 * Removes the extension from a filename.  
	 * 
	 * @param filename
	 * @return
	 */
	public static String removeExtension(String filename) {
		return filename.replaceFirst("[.][^.]+$", "");
	}

	public static void setWorkingDir(File workingDir){
//		System.out.println("setting: " + workingDir.getAbsolutePath());
//		String test = null;
//		if (! workingDir.getAbsolutePath().endsWith("temp")){
//		if ("/informatik2/students/home/3zimmer".equals(workingDir.getAbsolutePath())){
//			test.charAt(2);
//		}
		File dir = Paths.get(".").toFile();
		File data = new File(dir.getAbsolutePath() + File.separator
				+ "LocalData");
		if (data.exists() && data.isDirectory()) {
			try {
				File workDirs = new File(data.getAbsolutePath()
						+ File.separator + "wDir.bin");
				FileOutputStream fos = new FileOutputStream(workDirs);
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(workingDir.getCanonicalPath());
				oos.close();
			}
			catch (Exception e) {
			}
		}
	}

	public static File getWorkingDir(){
		File result = null;
		try {
			File dir = Paths.get(".").toFile();
			File data =
						new File(dir.getCanonicalPath() + File.separator
									+ "LocalData");
			if (data.exists() && data.isDirectory()) {
	
				File workDirs =
							new File(data.getCanonicalPath() + File.separator
										+ "wDir.bin");
				FileInputStream fis = new FileInputStream(workDirs);
				ObjectInputStream ois = new ObjectInputStream(fis);
				result = new File((String) ois.readObject());
				ois.close();
			}
			else {
				data.mkdir();
			}
		}
		catch (Exception e) {
		}
//		System.out.println("getting: " + result.getAbsolutePath());
		return result;
	}

	public  static File currentDir() {
		return new File(Paths.get("").toString());
	}

	/**
	 * Adds the lines of a text file separately into a list 
	 * of strings, if the file exists.
	 * 
	 * @param file
	 * @param list
	 */
	public static void fileToList(File file, ArrayList<String> list) {
		if (file.exists()) {
			BufferedReader reader;
			try {
				reader = new BufferedReader(new FileReader(file));
				String line;
				while ((line = reader.readLine()) != null) {
					list.add(line);
				}
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void listToFile(ArrayList<String> list, File file) {
		try {
			OutputStreamWriter w =
						new OutputStreamWriter(new FileOutputStream(file),
									"UTF-8");
			PrintWriter writer = new PrintWriter(w);
			StringBuilder fileContent = new StringBuilder();
			for (String line : list) {
				fileContent.append(line);
				fileContent.append(System.getProperty("line.separator"));
			}
			writer.print(fileContent);
			writer.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Converts the content of a CDA file into a text that can be inserted into 
	 * TestParseFactory, like e.g. PARSE_ERRORC1
	 * 
	 * @param name the filename
	 */
	public static void convert(String name) {
		try {
			File file = new File(Paths.get("../..").toAbsolutePath().toString() + File.separator
						+ name);
			StringBuilder fileContent = new StringBuilder();
			BufferedReader reader;
				reader = new BufferedReader(new FileReader(file));
			String line;
			while ((line = reader.readLine()) != null) {
				fileContent.append("			\"");
				fileContent.append(line);
				fileContent.append("\\n\" +");
				fileContent.append(System.getProperty("line.separator"));
			}
			reader.close();
			try {
				Writer w =
							new OutputStreamWriter(new FileOutputStream(file),
										"UTF-8");
				PrintWriter writer = new PrintWriter(w);
				writer.print(fileContent);
				writer.close();
			}
			catch (UnsupportedEncodingException | FileNotFoundException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Writes a String into a text file, assuming that the file exists.
	 * 
	 * @param text
	 * @param file
	 */
	public static void saveToFile(String text, File file) {
//		System.out.println("Saving: " + text + "\n to:_" + file.getAbsolutePath());
		try {
			Writer w =
						new OutputStreamWriter(new FileOutputStream(file),
									"UTF-8");
			PrintWriter writer = new PrintWriter(w);
			writer.print(text);
			writer.close();
		}
		catch (UnsupportedEncodingException | FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void saveToFile(String text, String dirname, String filename) {
		File dir = new File(dirname);
		File file = new File(dirname + File.separator + filename);
//		System.out.println("Generated: " + file.getAbsolutePath());
		if (! dir.exists()){
			dir.mkdirs();
		}
		if (! file.exists()){
			try {
				file.createNewFile();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		saveToFile(text, file);
	}
}
