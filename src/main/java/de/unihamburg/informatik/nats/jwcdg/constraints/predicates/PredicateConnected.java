package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * Are A and B part of the same tree?
 * true if A and B have the same root
 */
public class PredicateConnected extends Predicate {

	public PredicateConnected(Grammar g) {
		super("connected", g, true, 2, false);
	}

	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {

		int a, b, roota = -1, rootb = -1;
		LexemeNode lna, lnb;
		LevelValue lv;
		int noOflevels = grammar.getNoOfLevels();
		int max = lg.getMax() - lg.getMin();
		int count;
		int levelNo = binding[0].getLevel().getNo();

		AVM val1 = args.get(0).eval(lg, context, binding);
		if (!(val1 instanceof AVMLexemNode)) {
			throw new PredicateEvaluationException("type mismatch (1st arg) for `connected'");
		}
		lna = ((AVMLexemNode) val1).getLexemeNode();
		if (!lna.spec())
			return false;
		a = lna.getArc().from;
		
		AVM val2 = args.get(1).eval(lg, context, binding);
		if (!(val2 instanceof AVMLexemNode)) {
			throw new PredicateEvaluationException("type mismatch (2nd arg) for `connected'");
		}
		lnb = ((AVMLexemNode) val1).getLexemeNode();
		if (!lnb.spec())
			return false;
		b = lnb.getArc().from;
		
		count = 0;
		lv = context.getLV( noOflevels * a + levelNo);
		while (lv != null) {
			if (!lv.getRegentGN().spec()) {
				roota = lv.getDependentGN().getArc().from;
				break;
			} else if (count++ > max) {
				roota = a;
				break;
			} else {
				lv = context.getLV(noOflevels * lv.getRegentGN().getArc().from + levelNo);
			}
		}

		count = 0;
		lv = context.getLV( noOflevels * b + levelNo);
		while (lv != null) {
			if (!lv.getRegentGN().spec()) {
				rootb = lv.getDependentGN().getArc().from;
				break;
			} else if (count++ > max) {
				rootb = b;
				break;
			} else {
				lv = context.getLV(noOflevels * lv.getRegentGN().getArc().from + levelNo);
			}
		}

		return roota == rootb;
	}
}
