package de.unihamburg.informatik.nats.jwcdg.constraints.formulas;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.AccessContext;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

public class FormulaTrue extends Formula {

	public FormulaTrue() {
		this.isContextSensitive = false;
	}

	@Override
	public boolean evaluate(LevelValue[] binding, Context context, LexemeGraph lg) {
		return true;
	}
	
	@Override
	public String toString() {
		return "true";
	}

	@Override
	public void analyze(AccessContext context, Grammar g) {
	}

	@Override
	public void collectFeatures(List<Path> sofar) {
	}

	@Override
	public boolean check() {
		return true;
	}
	
	@Override
	public void collectHasInvocations(List<Formula> akku) {
	}
}
