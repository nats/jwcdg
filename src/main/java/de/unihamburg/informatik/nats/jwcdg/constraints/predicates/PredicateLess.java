package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import de.unihamburg.informatik.nats.jwcdg.input.Grammar;

public class PredicateLess extends PredicateNumericalComparison {

	public PredicateLess(Grammar g) {
		super("<");
	}
	@Override
	protected boolean compareNumbers(double a, double b) {
		return a < b;
	}

}
