package de.unihamburg.informatik.nats.jwcdg.gui;

import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import io.gitlab.nats.deptreeviz.DepTree;

import javax.swing.*;
import java.awt.*;

/**
 * The UI component of the DepTreeViewer tool. Doesn't include the interactions.
 *
 * @author Sven Zimmer and Arne Köhn
 *
 */

class DepTreeDisplay extends AbstractDisplay {
	private static final long serialVersionUID = 1L;
	private JScrollPane _nodesScrollPane;

	DepTreeDisplay(DepTree dt, boolean hasInteractions, Configuration config) {
		super(config);

		_prefD = new DepTreePreferencesDisplay();

		initButtons();
		initElements(hasInteractions);

		_depTreePanel.setLayout(new GridLayout(0, 1));

		if (hasInteractions) {
			JPanel parsePanel = new JPanel();
			getContentPane().add(parsePanel, BorderLayout.NORTH);

			_buttonPanel.add(getButton(PRINT_BUTTON));
			_buttonPanel.add(getButton(LEVEL_BUTTON));

			getFileMenu().add(getOpenItem());
			getFileMenu().insertSeparator(1);
			getFileMenu().add(getSaveItem());
			getFileMenu().add(getSaveAsItem());
			getFileMenu().insertSeparator(5);
			getFileMenu().add(getExportItem());
			getFileMenu().insertSeparator(7);
			getFileMenu().add(getQuitItem());

			_vertSplitPane.setRightComponent(_constraintsPanel);
			_vertSplitPane.setDividerLocation(0.5);

			int fontSize = dt.getFont().getSize();
			_parseField.setPreferredSize(new Dimension(fontSize * 30,
						fontSize + 10));
			_parseField.setFont(dt.getFont());
			_parseField.setEditable(true);

			_constraintSearchInput.setPreferredSize(new Dimension(fontSize * 30,
					fontSize + 10));
			_constraintSearchInput.setFont(dt.getFont());
			_constraintSearchInput.setEditable(true);

			parsePanel.add(_parseField);
			parsePanel.add(getButton(PARSE_BUTTON));
			parsePanel.add(getButton(CLEAR_TEXTFIELD_BUTTON));
			parsePanel.add(getBox(PARTIAL_CHECKBOX));
		}

		// TODO fix bug: only scroll bars on resize, perhaps a problem with
		// batik

		// scrollCanvas.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		// frame.firePropertyChange("WIDTH",frame.WIDTH,frame.WIDTH);
		// scrollCanvas.getVerticalScrollBar().setValueIsAdjusting(true);
		// System.out.println(scrollCanvas.getVerticalScrollBar().getValue());
		// canvas.setEnableZoomInteractor(true);
		// DOMTreeManager tm = g.getDOMTreeManager();

		setNodesScrollPane(dt);
		setSize(dt.getWindowSize());

	}

	protected class DepTreePreferencesDisplay extends PreferencesDisplay {
		private static final long serialVersionUID = 1L;

		@Override
		protected void addSpecialPrefs() {
			makeBox(PARSE_BOX, "In parsefield, parse after every word");

			// addComp (_parseIncBox, _parseIncLabel);
		}
	}

	public void setNodesScrollPane(DepTree dt) {
		_depTreePanel.removeAll();
		_nodesScrollPane = new JScrollPane(dt.getNodesCanvas());
		int fontSize = dt.getFont().getSize();
		_nodesScrollPane.getHorizontalScrollBar()
				.setUnitIncrement(fontSize * 3);
		_nodesScrollPane.getVerticalScrollBar().setUnitIncrement(fontSize * 3);
		_nodesScrollPane.setSize(dt.getCanvasSize());
		_depTreePanel.add(_nodesScrollPane);
		validate();
		repaint();
		_vertSplitPane.setDividerLocation(dt.getCanvasSizeY());
	}

	public JScrollPane getNodesScrollPane() {
		return _nodesScrollPane;
	}

	@Override
	public JTable getConstraintsTable() {
		return _constraintsTable;
	}

	@Override
	public void setConstraintsTable(JTable constraintsTable) {
		_constraintsTable = constraintsTable;
	}

}
