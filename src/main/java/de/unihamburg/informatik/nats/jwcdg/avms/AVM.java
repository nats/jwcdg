package de.unihamburg.informatik.nats.jwcdg.avms;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import de.unihamburg.informatik.nats.jwcdg.input.Configuration;

/**
 * Attribute value matrix
 * 
 * @author the CDG-Team
 *
 */
public abstract class AVM implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	
	public AVM followPath(Path path) {
		if(path == null) {
			return this;
		} else {
			return followPathImpl(path);
		}
	}
	
	protected AVM followPathImpl(Path path) {
		return new AVMError("ERROR: a non empty path cannot be followed in an AVM of type " + this.getClass().toString() + "!");
	}
	
	
	public List<Path> possiblePaths() {
		// default: the empty path
		List<Path> ret = new ArrayList<>();
		ret.add(null);
		return ret;
	}
	
	public abstract AVM substituteRefs(Matcher matcher, Configuration config);
	// Some code need sane equals() handling so we force an implementation
	// by the subclasses.
	public abstract int hashCode();
	public abstract boolean equals(Object other);
}
