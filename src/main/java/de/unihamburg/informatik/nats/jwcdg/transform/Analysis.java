package de.unihamburg.informatik.nats.jwcdg.transform;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.ListIterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNet;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNetState;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.GraphemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.NoPathPossibleException;
import de.unihamburg.informatik.nats.jwcdg.constraints.Level;
import de.unihamburg.informatik.nats.jwcdg.evaluation.ConstraintEvaluationResult;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluationResult;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.frobbing.FrobbingFlag;
import de.unihamburg.informatik.nats.jwcdg.incrementality.virtualNodes.VirtualLexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.incrementality.virtualNodes.VirtualNodeSpec;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;

/**
 * A single consistent, but not necessarily grammatical, parse.
 * 
 * The main data structure manipulated by this module is the Analysis. An
 * Analysis normally corresponds to a complete parse of the underlying
 * utterance, that is, a binding of all time points of the underlying lattice on
 * all levels of analysis. (Compare the fields substart and substop, however.)
 * The parse is not guaranteed to be grammatical. In fact, an Analysis may
 * violate any number of defined constraints, even those with score 0.0. This is
 * to ensure that an Analysis can be built quickly for any lattice, so that we
 * have something to repair.
 * 
 * The central part of an Analysis is the Vector LVs. This vector contains all
 * LVs that together form the dependency trees that make up the parse. Each LV
 * only occupies one slot, whose number is calculated using the function
 * lvIndex(). If an utterance contains lexeme hypotheses that extend over
 * several timepoints, then some of the slots will be empty. This is to ensure
 * that if we want to test whether a certain LV is in an Analysis, we only have
 * to search one slot rather than several. A simple example illustrates the
 * layout of several LVs within the Vector: assume that we have the levels SYN
 * and SEM, and the lattice
 * 
 * -- der -- ------ Mann --------- -- schläft -- / \ / \ / \ 0 1 2 3 4 \ / \ / \
 * / -------- das ------- -- Lamm -- --- steht ---
 * 
 * 
 * then the Vector could look like this:
 * 
 * LVs[0]: SYN:der(0,1)---1--.Mann(1,3) LVs[1]: SEM:der(0,1)--DET-.Mann(1,3)
 * LVs[2]: SYN:Mann(1,3)--1-.schläft(3,4) LVs[3]: SEM:Mann(1,3)--AG.schläft(3,4)
 * LVs[4]: NULL LVs[5]: NULL LVs[6]: SYN:schläft(3,4)--MOD-.NIL LVs[7]:
 * SEM:schläft(3,4)--PROP.NIL
 * 
 * The LVs stored in this vector must make up a complete set of dependency trees
 * at any time.
 * 
 * The Vector isUnselectedLexeme notes which lexemes from the underlying lexeme
 * graph are selected for analysis, much like the field isDeletedLexemNode of a
 * NetSearchState. `isUnselectedLexeme' is admittedly an inelegant way of
 * putting it, but we need an array of negative information (TRUE means not
 * used) so we can pass it as an argument to lvPrint, and we don't want to call
 * it `isDeletedLexeme' to avoid confusion with lexemes that are actually
 * deleted in the graph. The Vector must select a set of lexemes that form a
 * complete path through the graph at any time. All LVs in the Analysis must be
 * valid under this path.
 * 
 * The List conflicts notes all ConstraintViolations that the Analysis causes.
 * 
 * If the fields subStart and subStop differ from the extreme time points of the
 * enclosing Problem, then all functions changing and evaluating Analyses
 * pretend that the world ends at these boundaries rather than the real ones.
 * 
 * An Analysis owns only its Badness and the ConstraintViolation structures in
 * conflicts.
 */
public class Analysis {
	private static Logger logger = LogManager.getLogger(Analysis.class);
	private final static boolean ignoreUnconnectedVirtualNodes = true;
	
	/** the constraint problem this is a possible solution for */
	private ConstraintNet net;
	/** set of constituting LVs, indices corresponding to unused levels point to NULL */
	private List<LevelValue> slots;
	/** info about currently selected lexemes, this list is NOT sorted by timepoints */
	private List<LexemeNode> selectedLexemes;
	/** list of ConstraintViolations */
	private List<ConstraintViolation> conflicts;
	/** generalized score */
	private Badness b;
	/** start tp of current active subrange */
	private int subStart;
	/** end tp of current active subrange */
	private int subStop;

	/**
	 * Allocate an Analysis.
	 * 
	 * This function allocates a new Analysis for the Problem P. It does not
	 * allocate all fields of the structure, since the first Analysis in a
	 * Problem will be completed by net2problem(), and further structures are
	 * produced by aClone().
	 */
	public Analysis(ConstraintNet net) {
		this.net = net;
		this.slots = new ArrayList<>(net.getNoOfSlots());
		for (int i = 0; i < net.getNoOfSlots(); i++) {
			// initializing with null so we have a list of the desired size
			slots.add(null);
		}

		this.selectedLexemes = new ArrayList<>();
		this.b = Badness.bestBadness();
		this.conflicts = new ArrayList<>();
		this.subStart  = net.getMinTimepoint();
		this.subStop   = net.getMaxTimepoint();
	}

	/**
	 * Copy another Analysis.
	 * 
	 * This function creates a deep copy of A, except for the constituting LVs,
	 * which are usually simply copied rather than cloned. LVs are only cloned
	 * if they are not owned by the underlying constraint net (i.e., have
	 * indexWRTNet == -1).
	 */
	public Analysis(Analysis orig) {
		this(orig.net);

		// shallow copy of LNs
		selectedLexemes = new ArrayList<>(orig.selectedLexemes);

		// mostly shallow copy of LVs ...
		slots = new ArrayList<>(orig.slots.size());
		for (int i = 0; i < orig.slots.size(); i++) {
			LevelValue lv = orig.slots.get(i);
			if (lv == null) {
				slots.add(null);
				continue;
			}
			if (lv.getIndexWRTNet() == -1) {
				// ... unless this lv is not owned by the net
				lv = new LevelValue(lv);
			}
			slots.add(lv);
		}

		/* deep copy of the rest */
		for (ConstraintViolation cv : orig.conflicts) {
			conflicts.add(new ConstraintViolation(cv));
		}
		b = orig.b;
		subStart = orig.subStart;
		subStop  = orig.subStop;
	}

	/**
	 * Return TRUE if A has no hard violations.
	 */
	public boolean isValid() {
		return (b.hard == 0);
	}
	
	/**
	 * will select a attachment to nil via the empty lable in every domain
	 * 
	 * will not calcualte a badness, call judge afterwards
	 */
	public void setToMaxFragmentation() {
		try {
			selectPath(
					net.getLexemeGraph().makePathThrough(null, new ArrayList<>()),
					true);
		} catch (NoPathPossibleException e) {
			return;
		}
		
		for(int i = 0; i < net.getNoOfSlots(); i++) {
			LevelValue selected = null; 
			for(LevelValue lv : net.getCNode(i).getValues()) {
				if(lv.getRegentGN().isNil() && lv.getLabel().isEmpty()) {
					selected = lv;
					break;
				}
			}
			
			if(selected==null)  
				throw new RuntimeException("trivial levelvalues missing, invalid net!");
			
			insertLv(selected);
		}
	}

	/**
	 * selects levelvalues according to the given specification, if possible
	 * 
	 * all given lists need to be of the same length
	 * 
	 * @param names list of VN names, if one of the names was not found, we fail
	 * @param labels list of labels with whom the specified VNs will be attached
	 * @param heads may only contain indices of the first list or -1 for the root (or -2 for nonspec) 
	 * @param level lvs on the given level will be selected
	 * @return true, if selection was possible
	 */
	public boolean selectVNStructure(List<String> names, List<String> labels, List<Integer> heads, Level level) {
		
		// first, make sure all lists are of the same size
		if(names.size() != labels.size() || labels.size() != heads.size())
			return false;
		
		// now evaluate the names and try to find according virtual GNs
		List<GraphemeNode> vns = new ArrayList<>(names.size());
		for(String name : names) {
			boolean found = false;
			for(GraphemeNode gn : net.getLexemeGraph().getGNs()) {
				if(gn.isVirtual() && gn.getArc().word.startsWith(name) && !vns.contains(gn)) {
					vns.add(gn);
					found = true;
					break;
				}
			}
			
			if(!found)
				return false;
		}
		
		// for find levelvalues according to the other specifications
		for(int i= 0; i < vns.size(); i++) {
			GraphemeNode dep =     vns.get(i);
			String label     =  labels.get(i);
			int regNo        =   heads.get(i);
			GraphemeNode reg;
			if(regNo== -1) {
				reg = net.getLexemeGraph().getNil();
			} else if(regNo== -2) {
				reg = net.getLexemeGraph().getNonspec();
			} else {
				reg = vns.get(regNo); 
			}
			
			List<LevelValue> candidates = net.findExistingLVs(level, dep, label, reg);
			if(candidates.isEmpty())
				return false;
			
			selectLV(candidates.get(0));
		}
		
		// success
		return true;
	}
	
	/**
	 * Re-calculate the soft score of an Analysis by multiplying the scores of
	 * all soft conflicts.
	 * 
	 * This is to prevent rounding errors after repeated division and
	 * multiplication during the repair process.
	 */
	public void calibrate() {

		double score = 1;

		for (ConstraintViolation cv : conflicts) {
			if (cv.getPenalty() > 0.0)
				score *= cv.getPenalty();
		}

		if (score != b.soft) {
			b = new Badness(b.no, b.hard, score);
		}
	}

	/**
	 * Calculate all conflicts in an analysis.
	 * 
	 * Sets the fields .b and .conflicts. If these fields were non-empty, the
	 * contents are deallocated.
	 * 
	 * if the score falls below the given threshold, cancel evaluation and
	 * return false else true is returned
	 */
	public boolean judgeWithThreshold(GrammarEvaluator evaluator,
			double threshold) {
		LevelValue lva, lvb;

		/* remove old conflict set */
		if (!conflicts.isEmpty()) {
			conflicts.clear();
		}

		/* start out with the perfect badness */
		b = Badness.bestBadness();

		Context context = new Context(slots);

		/* seek unary conflicts */
		for (int i = 0; i < slots.size(); i++) {
			if (!slotActive(i)) {
				continue;
			}

			lva = slots.get(i);
			if (lva == null)
				continue;
			GrammarEvaluationResult ger = evaluator.evalUnary(lva,
					net.getLexemeGraph(), context, false, true);
			conflicts.addAll(ger.getCVs());
			b = b.addBadness(ger.getBadness());

			if (threshold > 0.0
					&& (b.hard > 0 || Badness.significantlyGreater(threshold,
							b.soft))) {
				return false;
			}

			/* seek binary conflicts */
			for (int j = i + 1; j < slots.size(); j++) {
				if (!slotActive(j))
					continue;
				lvb = slots.get(j);
				if (lvb == null)
					continue;

				GrammarEvaluationResult ger2 = evaluator.evalBinary(lva, lvb,
						net.getLexemeGraph(), context, false, true);
				b = b.addBadness(ger2.getBadness());
				conflicts.addAll(ger2.getCVs());
				// #ifdef TERNARY
				// if(inputCurrentGrammar.no_ternary) {
				// /* seek ternary conflicts */
				// int k;
				// for (k = j + 1; k < vectorSize(slots); k++) {
				// if (!slotActive(a, k)) {
				// continue;
				// }
				// LevelValue lvc = (LevelValue) vectorElement(slots, k);
				// if (!lvc) {
				// continue;
				// }
				//
				// (void) evalTernary(lva, lvb, lvc, P.net, slots, FALSE,
				// b, &conflicts);
				// }
				// }
				// #endif
				if (threshold > 0.0
						&& (b.hard > 0 || Badness.significantlyGreater(
								threshold, b.soft))) {
					return false;
				}
			}
		}
		
		/* sort conflicts */
		if (conflicts.size() > 0) {
			Collections.sort(conflicts);
		}

		return true;
	}

	/**
	 * Should we bind slot x? Returns FALSE if its level is inactive, or the
	 * timepoint is not used by the lexeme graph, or if we are currently solving
	 * only a subproblem.
	 */
	public boolean slotActive(int x) {

		if (x / net.getNoOfLevels() < subStart) {
			return false;
		}
		if (x / net.getNoOfLevels() >= subStop) {
			return false;
		}
		if (!net.getLevelActive((x % net.getNoOfLevels()))) {
			return false;
		}
		return true;
	}

	/**
	 * Calculate all conflicts in an analysis. Sets the fields .b and .conflicts. 
	 */
	public void judge(GrammarEvaluator evaluator) {
		judgeWithThreshold(evaluator, 0.0);
	}

	/**
	 * Re-compute conflicts after applying a patch.
	 * 
	 * After applying a Patch to an Analysis, it is faster (~20% less time
	 * needed) not to evaluate the grammar on the entire new Analysis, but
	 * simply to re-evaluate it merely on the changed bits. This is slightly
	 * complicated by the fact that the LVs listed in the Patch are not the only
	 * ones that might have changed. If the Patch also changed the path though
	 * the lexeme graph, other LVs might have changed to follow suit.
	 * 
	 * Basically this procedure first inspects every LV in a to see if it has
	 * changed. Then all conflicts caused by the changed LVs are erased, because
	 * those are the ones that may have been resolved. Finally the grammar is
	 * evaluated on the changed LVs.
	 */
	public void rejudge(Patch p, GrammarEvaluator evaluator) {
		List<Integer> changedLV = new ArrayList<>(net.getNoOfSlots());
		List<Integer> changedInterval = new ArrayList<>(
                net.getMaxTimepoint());

		List<LevelValue> lvList;

		List<ConstraintViolation> oldCVs = new ArrayList<>();
		List<ConstraintViolation> newCVs = new ArrayList<>();

		List<LevelValue> partialSlots = new ArrayList<>(slots);

		GrammarEvaluationResult ger;

		/* 1. Note down all LVs that were changed by the patch. */
		/* explicitly changed LVs */
		for (LevelValue lv : p.getSelection()) {
			if (lv == null)
				continue;
			changedLV.add(lv.getIndex());
		}
		/*
		 * implicitly changed LVs: to find those, we note down all changed time
		 * intervals
		 */
		if (p.getNewLexemes().size() > 0) {

			/* make vector of changed time intervals */
			for (LexemeNode ln : p.getNewLexemes()) {
				for (int i = ln.getArc().from; i <= ln.getArc().to; i++) {
					changedInterval.add(i);
				}
			}

			/* Mark those LVs that modify one of the new lexemes... */
			for (int i = 0; i < slots.size(); i++) {
				LevelValue lv = slots.get(i);
				if (lv == null)
					continue;
				if (!lv.getRegentGN().spec())
					continue;
				for (int j = lv.getRegentGN().getArc().from; j < lv
						.getRegentGN().getArc().to; j++) {
					if (changedInterval.contains(j)) {
						changedLV.add(i);
						break;
					}
				}
			}

			/* ...or are modified by one of them */
			for (int i = 0; i < slots.size(); i++) {
				LevelValue lv = slots.get(i);
				if (lv == null)
					continue;
				for (int j = lv.getDependentGN().getArc().from; j < lv
						.getDependentGN().getArc().to; j++) {
					if (changedInterval.contains(j)) {
						changedLV.add(i);
						break;
					}
				}
			}
		}

		/* 2. Remove conflicts that may have disappeared. */
		for (ConstraintViolation cv : conflicts) {
			/* caused by a changed lv? */
			if (changedLV.contains(cv.getLV1().getIndex())
			    || cv.getLV2() != null
			       && changedLV.contains(cv.getLV2().getIndex())
			    || cv.getLV3() != null
			       && changedLV.contains(cv.getLV3().getIndex())
			    /*
			     * context-sensitive conflict? This can disappear whenever
			     * anything change at all.
			     */
			    || cv.getConstraint().isContextSensitive()
			    /*
			     * caused by an lv that is unchanged but has disappeared?
			     * (This can only happen during initialization.)
			     */
			    || slots.get(cv.getLV1().getIndex()) == null
			    || cv.getLV2() != null
			       && slots.get(cv.getLV2().getIndex()) == null
			    || cv.getLV3() != null
			       && slots.get(cv.getLV3().getIndex()) == null) {

				/*
				 * Since we are going to remove this conflict, we must remove
				 * its penalty as well.
				 */
				b = b.subtract(cv.getPenalty());
			} else {
				/* retain */
				oldCVs.add(cv);
			}
		}

		/* 3. Calculate newly introduced conflicts. */

		/* make list of new LVs */
		lvList = new ArrayList<>();
		for (int i = 0; i < slots.size(); i++) {
			if (!slotActive(i)) {
				partialSlots.set(i, null);
				continue;
			}
			LevelValue lv = slots.get(i);
			if (lv == null)
				continue;
			if (changedLV.contains(i)) {
				lvList.add(lv);
			}
		}

		Context context= new Context(slots);

		/* seek new conflicts */
		for (LevelValue lv : lvList) {

			/* seek new unary conflicts */
			ger = evaluator.evalUnary(lv, net.getLexemeGraph(), null, false,
					true);
			newCVs.addAll(ger.getCVs());
			b = b.addBadness(ger.getBadness());

			/* seek new binary conflicts between changed & unchanged LVs */
			for (int i = 0; i < slots.size(); i++) {
				if (!slotActive(i))
					continue;
				LevelValue lvb = slots.get(i);
				if (lvb == null)
					continue;

				/* don't evaluate against another member of lvList */
				if (changedLV.contains(i))
					continue;
				ger = evaluator.evalBinary(lv, lvb, net.getLexemeGraph(), null,
						false, true);
				newCVs.addAll(ger.getCVs());
				b = b.addBadness(ger.getBadness());
			}

			/* seek new binary conflicts between changed LVs */
			ListIterator<LevelValue> lit = lvList.listIterator(lvList
					.indexOf(lv) + 1);
			while (lit.hasNext()) {
				LevelValue lvb = lit.next();
				ger = evaluator.evalBinary(lv, lvb, net.getLexemeGraph(), null,
						false, true);
				newCVs.addAll(ger.getCVs());
				b = b.addBadness(ger.getBadness());
			}
		}

		/* find context-sensitive conflicts */
		ger = evaluator.evalInContext(partialSlots, context, net.getLexemeGraph());
		newCVs.addAll(ger.getCVs());
		b = b.addBadness(ger.getBadness());

		/* sort new conflicts */
		if (!newCVs.isEmpty()) {
			Collections.sort(newCVs);
		}

		/* ensure that new conflicts precede old conflicts */
		conflicts = newCVs;
		conflicts.addAll(oldCVs);
	}

	public Badness getBadness() {
		return b;
	}

	/**
	 * Check Analysis for internal consistency, and abort with warning if an
	 * error is found.
	 * 
	 * Different levels of checking are necessary so that aCheck() can be used
	 * during the construction of Analyses, when some of the structures checked
	 * here are incomplete because they haven't been built yet.
	 */
	public boolean check(GrammarEvaluator evaluator, int level,
			List<ConstraintViolation> oldCVs) {
		int i, j, k;
		LevelValue lv;
		Analysis a2;

		List<Integer> boundTPs = new ArrayList<>(net.getMaxTimepoint());
		List<Integer> boundSlots = new ArrayList<>(net.getNoOfSlots());

		/* invalid LV in the Analysis? */
		for (i = 0; i < slots.size(); i++) {
			lv = slots.get(i);
			if (lv == null)
				continue;

			if(!checkLvValidity(lv))
				return false;
				
			if (!slotActive(i)) {
				continue;
			}

			/* zoning violation? */
			// if(spec(lv.regents) &&
			// (((LexemNode)lv.regents.item).arc.from < a.P.subStart ||
			// ((LexemNode)lv.regents.item).arc.from >= a.P.subStop)) {
			// cdgPrintf(CDG_INFO,
			// "PANIC: illegal LV @%d/#%d points to %d rather than [%d..%d]\n",
			// i,
			// lv.indexWRTNet,
			// ((LexemNode)lv.regents.item).arc.from,
			// a.P.subStart, a.P.subStop);
			// abort();
			// }

			/* LV residing in wrong domain? */
			if ((j = lv.getIndex()) != i) {
				logger.error(String.format(
						"PANIC: LV at @%d has lvIndex() of %d:", i, j));
				logger.error(lv.toString());
				return false;
			}

			for (j = lv.getDependentGN().getArc().from; j < lv.getDependentGN()
					.getArc().to; j++) {
				k = net.getNoOfLevels() * j + lv.getLevel().getNo();
				boundSlots.add(k);
			}
		}

		/* path criterion */
		for (LexemeNode lna : selectedLexemes) {

			for (j = lna.getArc().from; j < lna.getArc().to; j++) {
				boundTPs.add(j);
			}

			// have we passed lna? used to prevent comparing the same lns twice
			boolean postLna = false; 

			for (LexemeNode lnb : selectedLexemes) {

				if (lna == lnb) {
					postLna = true;
					continue;
				} else if (!postLna) {
					continue;
				}

				if (lna.overlaps(lnb)) {
					logger.error(String.format("PANIC: %s and %s  both selected at time point %d"
					             ,lna, lnb, lna.getArc().from)
							);
					return false;
				}
			}
		}

		/* skip Badness check */
		if (level >= 2) {

			/* wrong Badness calculated? */
			a2 = new Analysis(this);
			a2.judge(evaluator);
			if (this.b.compare(a2.b) || a2.b.compare(this.b)) {
				logger.error("PANIC: misevaluated Analysis!");
				logger.error(b);
				logger.error("computed conflicts=" + b);
				this.sortConflicts(oldCVs);
				logger.error(this.printConflicts());
				logger.error("actual conflicts= " + a2.b);
				logger.error(a2.printConflicts());
				return false;
			}
			a2 = null;

			/* skip completeness check */
			if (level >= 3) {

				/* path criterion */
				for (i = subStart; i < subStop; i++) {
					if (!boundTPs.contains(i)) {
						logger.error(String.format(
								"PANIC: time point %d not bound", i));
						return false;
					}
				}
				/* All domains bound? */
				for (i = 0; i < net.getNoOfSlots(); i++) {
					if (!slotActive(i)) {
						continue;
					}
					if (!boundSlots.contains(i)) {
						logger.error(String.format(
								"PANIC: domain @%d not bound", i));
						logger.error(this);
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * List all conflicts in an Analysis.
	 * 
	 * cvPrint() is used to display conflicts.
	 */
	private String printConflicts() {
		StringBuilder ret = new StringBuilder();
		int counter = 0;

		/* show all conflicts */
		for (ConstraintViolation cv : conflicts) {
			ret.append(String.format("%3d. ", counter++));
			ret.append(cv.toString());
		}
		return ret.toString();
	}

	/**
	 * Check that an LV is consistent internally _and_ with the containing
	 * Analysis.
	 */
	private boolean checkLvValidity(LevelValue lv) {
		if (isValidLv(lv, true)) {
			return true;
		}

		if (lv.getRegentGN().spec() && selectedLexeme(lv.getRegents()) == null) {
			logger.error(String.format("PANIC: regents of (%s) are all unselected!", lv));
			return false;
		}

		if (selectedLexeme(lv.getDependents()) == null) {
			logger.error(String.format("PANIC: dependents of (%s) are all unselected!", lv));
			return false;
		}
		return true;
	}

	public List<ConstraintViolation> getConflicts() {
		return conflicts;
	}

	/**
	 * implementation for apply and applyIfPossible
	 */
	private boolean applyImpl(Patch p, boolean forced,
			GrammarEvaluator evaluator) {
		/* swap in all new lexemes */
		if (!selectPath(p.getNewLexemes(), forced)) {
			if (forced) {
				logger.error("ERROR: select path failed even though forced was set!");
			}
			return false;
		}

		/* insert all new LVs */
		for (LevelValue source : p.getSelection()) {
			if (source == null) {
				continue;
			}
			selectLV(source);
		}

		/* rejudge in all the necessary places */
		if(evaluator != null) {
			rejudge(p, evaluator);
			calibrate();
		}

		return true;
	}

	/**
	 * Apply a Patch to an Analysis. P must already lexicalized.
	 * 
	 * After apply(), - A contains all Lvs from P - all LVs from P are valid in
	 * A - A is path consistent
	 * 
	 * Note that this deletes the Patch when done, so a Patch can only be
	 * applied once.
	 * 
	 * It is not meaningful to apply a Patch to any other Analysis than the one
	 * it was built for. A different but equivalent Analysis is fine, though --
	 * that is in fact what pJudge() does.
	 */
	public void apply(Patch p, GrammarEvaluator evaluator) {
		applyImpl(p, true, evaluator);
	}

	/**
	 * like apply, but can fail if forced = false
	 */
	public boolean applyIfPossible(Patch p, GrammarEvaluator evaluator) {
		return applyImpl(p, false, evaluator);
	}

	public ConstraintNet getCNet() {
		return net;
	}

	public int getSubStart() {
		return subStart;
	}

	public int getSubStop() {
		return subStop;
	}

	/**
	 * Selects list of lexemes and switches all LVs correspondingly.
	 * 
	 * Substitutes all lexeme nodes in WHICH into A. All previously selected LVs
	 * are changed just enough so that they comply with the new path selected.
	 * Naturally, this may make part of the structure and the labels of the
	 * Analysis totally meaningless if the new path differs substantially from
	 * the old one.
	 * 
	 * If forced is set, new LevelValues might be created if no existing one
	 * fits the new path.
	 * 
	 * Returns false, if path selection fails due to missing LV and forced =
	 * false. In this case the resulting analysis might be corrupt! In every
	 * other case true is returned
	 */
	public boolean selectPath(List<LexemeNode> which, boolean forced) {

		/* It is admissible to call selectPath() vacuously. */
		if (which.isEmpty()) {
			return true;
		}

		List<Boolean> changed = new ArrayList<>(net.getMaxTimepoint());
		for (int i = 0; i < net.getMaxTimepoint(); i++) {
			changed.add(false);
		}

		for (LexemeNode lnNew : which) {
			if (selectedLexemes.contains(lnNew)) {
				continue;
			}
			for (int i = lnNew.getArc().from; i < lnNew.getArc().to; i++) {
				changed.set(i, true);
			}
		}

		/* unselect all lexemes that cover changed timespans */
		for (ListIterator<LexemeNode> li = selectedLexemes.listIterator(); li
				.hasNext();) {
			LexemeNode lnOld = li.next();
			for (int j = lnOld.getArc().from; j < lnOld.getArc().to; j++) {
				if (changed.get(j)) {
					li.remove(); // yes, this works, see documentation of
									// listIterator
					break;
				}
			}
		}

		/* record the selection */
		for (LexemeNode lnNew : which) {
			selectedLexemes.add(lnNew);
		}

		/*
		 * exchange all LVs that bind unselected lexemes. These are:
		 * 
		 * [A] LVs that use one of them as a modifiee. They are simply switched
		 * to the simultaneously selected lexeme. If that results in an LV that
		 * already exists, no additional LV must be allocated.
		 * 
		 * To find them, we must iterate over all LVs.
		 */
		for (int index = 0; index < slots.size(); index++) {
			LevelValue oldLv = slots.get(index);

			/* hit? */
			if (oldLv == null || !oldLv.getRegentGN().isSpecified()
					|| selectedLexeme(oldLv.getRegents()) != null) {
				continue;
			}
			
			LexemeNode depLex = selectedLnAt(oldLv.getDependentGN().getArc().from);
			LexemeNode regLex = selectedLnAt(oldLv.getRegentGN().getArc().from);

			// see if we can find a lv for the changed regent lexeme specification
			LevelValue newLV = net.findExistingLV(oldLv.getLevel(), 
					oldLv.getDependentGN(), depLex, 
					oldLv.getLabel(),
					oldLv.getRegentGN(), regLex);

			// If the swap produced an LV that already exists, take that one
			// otherwise we need a cloned one
			if (newLV == null) {
				if (forced) {
					newLV = oldLv.makeVariant(depLex, regLex);
					/* A manipulated LV may even be self-modifying. */
					// TODO #ASR can only happen in branching lattices...
					newLV.breakCircle(net.getLexemeGraph().getNil()); 
				} else {
					// there is probably a good reason no such LV exists, fail
					return false;
				}
			}

			/* Finally, substitute the new LV into the Analysis. */
			slots.set(index, newLV);
			// This overwrites the old content of the slot, so there is no need to remove the old LV.
		}

		/*
		 * [B] Those LVs that use an unselected lexeme as a modifier. Note that
		 * an LV may fall under both category [A] and [B].
		 */
		for (int index = 0; index < slots.size(); index++) {
			LevelValue oldLv = slots.get(index);

			/* hit? */
			if (oldLv == null || selectedLexeme(oldLv.getDependents()) != null) {
				continue;
			}

			LexemeNode depLex = selectedLnAt(oldLv.getDependentGN().getArc().from);
			LexemeNode regLex;
			if(oldLv.getRegentGN().spec()) {
				regLex = selectedLnAt(oldLv.getRegentGN().getArc().from);
			} else {
				regLex = oldLv.getRegentGN().getLexemes().get(0);
			}
			
			// let's first see if the replacement was benign, 
			// i.e., by a lexeme node spanning the same timepoints
			// if not branching-lattice-hell breaks loose
			if(     oldLv.getDependentGN().getArc().from == depLex.getArc().from &&
					oldLv.getDependentGN().getArc().to   == depLex.getArc().to) {
				// all is well, proceed analog to case [A]
				// see if we can find a lv for the changed regent lexeme specification
				
				LevelValue newLV = net.findExistingLV(oldLv.getLevel(), 
						oldLv.getDependentGN(), depLex, 
						oldLv.getLabel(),
						oldLv.getRegentGN(), regLex);

				// If the swap produced an LV that already exists, take that one
				// otherwise we need a cloned one
				if (newLV == null) {
					if (forced) {
						newLV = oldLv.makeVariant(depLex, regLex); 
					} else {
						// there is probably a good reason no such LV exists, fail
						return false;
					}
				}
				/* Finally, substitute the new LV into the Analysis. */
				slots.set(index, newLV);
				// This overwrites the old content of the slot, so there is no need to remove the old LV.
			} else {
				throw new RuntimeException("Arcs spanning multiple timepoints not supported yet (or something went really wrong)!");
//				/*
//				 * since case [B] may replace an LV from domain @X with an lV that
//				 * goes into domain @Y, we must take care to remove the old LV from
//				 * its slot.
//				 */
//				int index = oldLv.getIndex();
//				slots.set(index, null);
//	
//				/*
//				 * Beware of holes in the Analysis!
//				 * 
//				 * If we replace the lexeme
//				 * 
//				 * DATUM(0-3)
//				 * 
//				 * by the lexemes
//				 * 
//				 * der(0-1) erste(1-2) April(2-3),
//				 * 
//				 * then the old LV bound three domains. We cannot simply replace
//				 * this LV with one that binds der(0-1) because that would leave two
//				 * domains vacant. Instead we inspect all of these domains and make
//				 * sure that they all are bound.
//				 */
//				LexemeNode ln;
//				int i = oldLv.getDependentGN().getArc().from;
//				while (i < oldLv.getDependentGN().getArc().to) {
//	
//					/* clone the LV (perhaps only temporarily) */
//					LexemeNode depLex = selectedLnAt(oldLv.getDependentGN().getArc().from);
//					LexemeNode regLex;
//					if(oldLv.getRegentGN().spec()) {
//						regLex = selectedLnAt(oldLv.getRegentGN().getArc().from);
//					} else {
//						regLex = oldLv.getRegentGN().getLexemes().get(0);
//					}
//					ln = selectedLnAt(i);
//					
//					LevelValue newLV = net.findExistingLV(oldLv.getLevel(), 
//							oldLv.getDependentGN(), depLex, 
//							oldLv.getLabel(),
//							oldLv.getRegentGN(), regLex);
//	
//					/*
//					 * If the swap produced an LV that already exists, take this one
//					 * and delete the clone.
//					 */
//					LevelValue original;
//					if ((original = findExistingLV(newLv)) != null) {
//						newLv = original;
//					} else {
//						if (forced) {
//							/* A manipulated LV may even be self-modifying. */
//							newLv.breakCircle(net.getLexemeGraph().getNil());
//						} else {
//							// there is probably a good reason no such LV exists,
//							// fail
//							return false;
//						}
//					}
//	
//					/* Finally, substitute the LV into the Analysis. */
//					slots.set(newLv.getIndex(), newLv);
//					/* step over this interval */
//					i = ln.getArc().to;
//				} // end while (i < oldLv.getDependentGN().getArc().to)
			} // end if
		}
		return true;
	}

	/**
	 * Return the lexeme node that is selected in a at time point WHERE.
	 * 
	 * The return value is never NULL. If an existing time point is unbound in
	 * am Analysis, that is an internal error.
	 */
	public LexemeNode selectedLnAt(int where) {
		/*
		 * Column #n of the vector lexemes contains a full listing of all
		 * lexemes that span timepoint #n. Therefore we need only look at that
		 * row.
		 */
		for (LexemeNode ln : net.getLexemesAt(where)) {
			if (selectedLexemes.contains(ln))
				return ln;
		}

		/* This can't happen */
		logger.error(String.format("PANIC: unbound time interval %d", where));
		throw new RuntimeException(String.format(
				"PANIC: unbound time interval %d", where));
	}

	/**
	 * Return the lexeme node from the list that is selected in a, or NULL.
	 */
	private LexemeNode selectedLexeme(List<LexemeNode> lexemes) {
		for (LexemeNode ln : lexemes) {
			if (selectedLexemes.contains(ln)) {
				return ln;
			}
		}
		return null;
	}

	/**
	 * Insert SOURCE int A.
	 * 
	 * Does not respect the path criterion; if isValid(SOURCE, A) does not hold,
	 * A may become path inconsistent afterwards. You must use pLexicalize()
	 * before this function.
	 */
	public void selectLV(LevelValue lv) {
		int where = lv.getIndex();
		LevelValue target = slots.get(where);

		/* self-replacement? */
		if (lv == target) {
			return;
		}

		/* insert */
		slots.set(where, lv);
	}

	/**
	 * Return the LV that binds slot WHERE in A.
	 * 
	 * Note that the slot WHERE itself may be empty; in that case an LV in an
	 * earlier slot must exist that binds WHERE.
	 */
	public LevelValue slotFiller(int where) {
		LevelValue result = null;
		int i = where;

		while (i >= 0 && result == null) {
			result = slots.get(i);
			i -= net.getNoOfLevels();
		}

		/*
		 * if the Analysis is incomplete (under construction), then the latest
		 * previous LV may not actually bind that slot. In that case no LV binds
		 * it.
		 */
		if (result != null && !net.lvBindsSlot(result, where)) {
			result = null;
		}

		return result;
	}

	public List<LevelValue> getSlots() {
		return slots;
	}

	/**
	 * Do lva and lvb cause the binary conflict cv?
	 * 
	 * This function checks whether the two LVs violate the constraint recorded
	 * in CV. This checks both instantiation variants, so the order of the LVs
	 * is irrelevant. If the constraint has a variable penalty, TRUE is returned
	 * only if lva and lvb make the constraint fail with the same penalty as
	 * recorded in the conflict.
	 */
	public boolean lvsCauseConflict(LevelValue lv1, LevelValue lv2,
			ConstraintViolation cv) {

		if (cv.getConstraint().getSignature().match(lv1, lv2)) {
			ConstraintEvaluationResult cer = cv.getConstraint().eval(
					net.getLexemeGraph(), new Context(slots), lv1, lv2);
			if (cer.isViolated() && cv.getPenalty() == cer.getScore()) {
				return true;
			}

		} else if (cv.getConstraint().getSignature().match(lv2, lv1)) {
			ConstraintEvaluationResult cer = cv.getConstraint().eval(
					net.getLexemeGraph(), new Context(slots), lv2, lv1);
			if (cer.isViolated() && cv.getPenalty() == cer.getScore()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Sort all conflicts in A by recentness and score.
	 * 
	 * This is to ensure that conflicts introduced by a repair step will end up
	 * in the front of the list, and will thus be attacked before other
	 * conflicts.
	 */
	public void sortConflicts(List<ConstraintViolation> oldCVs) {
		List<ConstraintViolation> newCVs       = new ArrayList<>();
		List<ConstraintViolation> recurringCVs = new ArrayList<>();

		for (ConstraintViolation cv : conflicts) {
			if (!oldCVs.contains(cv)) {
				newCVs.add(cv);
			} else {
				recurringCVs.add(cv);
			}
		}
		
		Collections.sort(recurringCVs);
		Collections.sort(newCVs);
		newCVs.addAll(recurringCVs);
		conflicts = newCVs;
	}

	/**
	 * Is this LV valid in a?
	 * 
	 * Returns TRUE if LV binds a set of lexemes that are valid in the current
	 * lexical path, i.e. at least one modifier and at least one modifiee (if
	 * there are any) must currently be selected.
	 * 
	 * If STRICT is FALSE, then lexeme nodes outside the current subrange of the
	 * Analysis are always considered valid no matter what the path selection
	 * is. Usually this should be TRUE. It only makes sense to say FALSE if you
	 * are sure you will take care of the path criterion yourself before
	 * expanding the subrange of the Analysis again.
	 */
	public boolean isValidLv(LevelValue lv, boolean strict) {
		if (lv.getRegentGN().spec()
				&& (strict || (lv.getRegentGN().getArc().from >= subStart && lv
						.getRegentGN().getArc().from < subStop))
				&& !lv.getRegents().contains(
						selectedLnAt(lv.getRegentGN().getArc().from))) {
			return false;
		}

		if (strict
				|| (lv.getDependentGN().getArc().from >= subStart && lv
						.getDependentGN().getArc().from < subStop)
				&& !lv.getDependents().contains(
						selectedLnAt(lv.getDependentGN().getArc().from))) {
			return false;
		}
		return true;
	}

	/**
	 * Transform Analysis to Parse.
	 * 
	 * This function returns a new decorated Parse built from A. It calls
	 * parseComputeStructure() to do the real work.
	 */
	public DecoratedParse toParse(Configuration config, GrammarEvaluator evaluator) {

		List<LevelValue> lvs = new ArrayList<>();
		for (LevelValue lv : slots) {
			if (lv == null) {
				continue;
			}
			lvs.add(lv);
		}

//		info.setScore(b.getScore());
//		info.setOptimal(!Badness.significantlyGreater(optimum, b.getScore()));

		ArrayList<Integer> exceptions = new ArrayList<>();
		if(ignoreUnconnectedVirtualNodes) {
			ArrayList<GraphemeNode> vns = new ArrayList<>();
			for(GraphemeNode gn : net.getLexemeGraph().getGNs()) {
				if(gn.isVirtual()) {
					vns.add(gn);
				}
			}
			
			for(LevelValue lv : lvs) {
				GraphemeNode reg = lv.getRegentGN();
				if(reg.isVirtual() && vns.contains(reg)) {
					vns.remove(reg);
				}
				GraphemeNode dep = lv.getDependentGN();
				if(dep.isVirtual() && (!reg.isNil() || !lv.getLabel().equals("")) && vns.contains(dep)) {
					vns.remove(dep);
				}
			}
			
			for(GraphemeNode gn : vns) {
				exceptions.add(gn.getNo());
			}
			Collections.sort(exceptions);
		}
		
		DecoratedParse p = new DecoratedParse(net.getLexemeGraph(), lvs,
				selectedLexemes, config.getList(Parse.ANNO_FEATS), evaluator, exceptions);

		return p;
	}

	/**
	 * Let A have an arbitrary, but consistent and complete lexeme selection.
	 */
	public void buildPath(ConstraintNetState state)
			throws NoPathPossibleException {

		List<LexemeNode> l = net.getLexemeGraph().makePathThrough(
				state, new ArrayList<>());

		selectPath(l, true);
	}

	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder(String.format("Analysis (%s)", b.toString()));
		for(LevelValue lv : slots) {
			ret.append("\n");
			ret.append(lv.toString());
		}
		ret.append("\n");
		
		for(ConstraintViolation cv : conflicts) {
			ret.append("\n");
			ret.append(cv.toString());
		}
		
		return ret.toString();
	}
	
	/**
	 * binds another slot in the given analysis
	 * on every level a levelvalue is selected so that it binds a lexeme as regent that is
	 * already part of the selected path through the wordgraph in the analysis
	 * if there is no such lexem NIL (and NONSPEC, if used) will always be available,
	 * so it will always succeed
	 *
	 * after the first level there will be an lexeme selected as dependent as well,
	 * so the values on the other levels have to respect this
	 * this will be no problem in the standard setting where SYN is the first level
	 * and the others are unproblematic.
	 *
	 * In other settings an error might occur!
	 *
	 * due to virtual nodes not yet pushed back, newSlots may not be read!
	 */
	public void bindAnotherSlot(ConstraintNetState state, int timepoint) {
		List<Level> levels= state.getNet().getGrammar().getLevels();
		int lastpos=timepoint * levels.size();
		
		LexemeNode selectedLN = null;
		
		for(Level lvl : levels) { // for all Levels...
			
			if(!lvl.isUsed()) {
				// skip unused levels
				slots.add(lastpos, null);
				lastpos++;
				continue;
			}

			List<LevelValue> lvs = state.getDomain(timepoint*levels.size()+lvl.getNo());
			boolean acceptable = false;

			LevelValue selectedLV = null; 
			if(selectedLN == null) {
				// first level, no LN is selected yet
				for(LevelValue lv : lvs) { // for all lvs of the current domain
					// ...und schaue fuer die regenten, ob ein entsprechndes lexem verfuegbar ist
					// in diesem fall ist der levelvalue ein zulaessiger  (wenn auch nicht unbedingt linguistisch sinnvoller) wert
					if(lv.getRegentGN().isNil() || lv.getRegentGN().isNonspec()) { // no need to check if regent LNs are selected, as there are none
						if(lv.getScore() > 0) {
							acceptable = true;
						} else { // better not take a lv with score 0.0
							continue;
						}
					} else {
						for(LexemeNode ln : lv.getRegents()) {
							if(selectedLexemes.contains(ln)) {
								acceptable = true;
								break;
							}
						}
					}
					if(acceptable) { // akzeptabler Startwert fuer die neue domaene gefunden
						selectedLV = lv;
						break;
					} 
				} // end while(lvs)

				if(!acceptable) {
					// wenn keiner passt, nimm den ersten
					selectedLV = lvs.get(0);
				}
				lastpos= insertSlot(selectedLV);
				selectedLN= selectedLV.getDependents().get(0); // lexem fuer spaeter merken
				selectedLexemes.add(selectedLN);

			} else { // ein lexem wurde bereits in einem anderen level ausgewaehlt
				// durchsuche alle levelvalues nach Kandidaten
				for(LevelValue lv : lvs) { // for all lvs of the current domain
					if(lv.getDependents().contains(selectedLN)) {
						if(     lv.getRegentGN().isNil() || 
								lv.getRegentGN().isNonspec() ||
								lv.getRegents().contains(selectedLnAt(timepoint))) {
							selectedLV = lv;
							acceptable=true;
							break;
						}
					}
				}
				if(acceptable) {
					lastpos= insertSlot(selectedLV);
					// isUnselected schon gesetzt von anderem level
				} else {
					// the above code expects there to be at least a trivial lv to always be valid,
					// no matter what lexme we selected when processign the first level
					// if this expectation is not met, we might end up here
					throw new RuntimeException("ERROR: expected level to be unproblematic regarding lexemes, but it was not \n");
				}
			} // end if(selectedLexem)
		} // end for all Levels
		
		this.subStop++;
	}
	
	/**
	 * insert slot,
	 * take care that all virtual nodes stay in the back
	 */
	private int insertSlot(LevelValue lv) {
		if( !(net.getLexemeGraph()  instanceof VirtualLexemeGraph ) || lv.getDependentGN().isVirtual()) {
			// just add it at the end
			slots.add(lv);
			return slots.size()-1; 
		} else {
			int n;
			for(n=0; n<slots.size(); n++) {
				LevelValue lv2= slots.get(n);
				if(lv2== null) {
					if(!net.getLevelActive(n%net.getNoOfLevels()))
						continue;
					break;
				}
				if(lv2.getDependentGN().isVirtual()) {
					break; // first virtual node found, insert here
				}
			}
			slots.add(n, lv);
			return n;
		}
	}

	/**
	 * a virtual graphemeNode (one that has no corresponding token in the input)
	 * is unused if
	 * - has NIL as regent on all levels
	 * - on all levels it's levelvalue has no label
	 * - is no regent for any other gn on any level
	 */
	public boolean isUnusedVirtualNode(GraphemeNode gn) {
		if(!gn.isVirtual())
			return false;

		for(LevelValue lv : slots) {
			if(lv == null)
				continue;

			if(lv.getRegentGN() == gn) {
				 // gn acts as a regent, so is not unused
				return false;
			}
			
			if(lv.getDependentGN() == gn && !lv.getLabel().equals("")) {
				return false;
			}
		}
		// no reason found not to return true;
		return true;
	}
	
	/**
	 * Change this Analysis so that it contains LV.
	 *
	 * This function is essentially a wrapper for pNew()+apply(). This means
	 * that it takes care of the path criterion itself; you can insert any LV
	 * into any Analysis.
	 * 
	 * If null is given for evaluator, no reevalution is done. Choose this, if the analysis is not 
	 * complete, yet. 
	 */
	public void insertLv(LevelValue lv, ConstraintNetState state, GrammarEvaluator evaluator) {
		Patch p = new Patch(this, 1, EnumSet.of(FrobbingFlag.OD_DELETED), 
				state.getLnDeletion());
		Patch q = p.next(lv, state, null, 0.0, 
				EnumSet.of(
						FrobbingFlag.OD_DELETED, 
						FrobbingFlag.OD_ORIENT, 
						FrobbingFlag.OD_LEXICAL, 
						FrobbingFlag.OD_LABEL));
		q.lexicalize(state, EnumSet.of(FrobbingFlag.OD_DELETED));
		this.apply(q, evaluator);
	}
	
	/**
	 * like above, but 
	 * does not consider limits (as you gave no state)
	 * and does not recalculate the badness 
	 */
	public void insertLv(LevelValue lv) {
		Patch p = new Patch(this, 1, EnumSet.of(FrobbingFlag.OD_DELETED), 
				null);
		Patch q = p.next(lv, null, null, 0.0, 
				EnumSet.of(
						FrobbingFlag.OD_DELETED, 
						FrobbingFlag.OD_ORIENT, 
						FrobbingFlag.OD_LEXICAL, 
						FrobbingFlag.OD_LABEL));
		q.lexicalize(null, EnumSet.of(FrobbingFlag.OD_DELETED));
		this.apply(q, null);
	}

	/**
	 * Modify Analysis a so that the LV at slot #where points to the
	 * lexeme at time point direction. If no such LV exists, a temporary
	 * LV is allocated for the purpose.
	 */
	public void redirect(int where, int direction) {
		LevelValue lv = slots.get(where);
		LevelValue tmpLV;
		while (lv == null) {
			where--;
			lv = slots.get(where);
		}
		
		GraphemeNode regent;
		LexemeNode regentLN;
		/* re-orientate */
		if (direction < 0) {
			regent   = net.getLexemeGraph().getNil();
			regentLN = regent.getLexemes().get(0);
		} else if (direction >= net.getLexemeGraph().getMax()) {
			regent   = net.getLexemeGraph().getNonspec();
			regentLN = regent.getLexemes().get(0);
		} else {
			regent   = net.getLexemeGraph().getGNs().get(direction);
			regentLN = selectedLnAt(direction);
		}
		
		LevelValue lv2 = net.findExistingLV(lv.getLevel(), 
				lv.getDependentGN(), getSelectedLNforGN(lv.getDependentGN()), 
				lv.getLabel(), regent, regentLN);
		if (lv2 != null) {
			tmpLV = lv2;
		} else {
			List<LexemeNode> dependents = new ArrayList<>(1);
			dependents.add(getSelectedLNforGN(lv.getDependentGN()));
			List<LexemeNode> regents = new ArrayList<>(1);
			regents.add(regentLN);
			tmpLV = new LevelValue(lv.getLevel(), net.getNoOfLevels(),
					dependents, lv.getDependentGN(), 
					lv.getLabel(), 
					regents, regent);
		}

		/* Finally, substitute the LV into the Analysis. */
		slots.set(where, tmpLV);
	}
	
	/**
	 * Return a Vector of Badness by domain of A.
	 */
	public List<Badness> analyzeSlots() {

		List<Badness> bySlot = new ArrayList<>(slots.size());
		Badness b;

		for (int i = 0; i < slots.size(); i++) {
			bySlot.add(Badness.bestBadness());
		}

		/* count all conflicts */
		for (ConstraintViolation cv : conflicts) {

			b = bySlot.get(cv.getNodeBindingIndex1());
			bySlot.set(cv.getNodeBindingIndex1(), b.add(cv.getPenalty()));

			if (cv.getLV2() != null) {
				b = bySlot.get(cv.getNodeBindingIndex2());
				bySlot.set(cv.getNodeBindingIndex2(), b.add(cv.getPenalty()));
			}
		}
		
		return bySlot;
	}
	
	/**
	 * Mark an Analysis as covering only part of an utterance.
	 */
	public void restrict(int from, int to) {
		subStart = Math.max(from, net.getMinTimepoint());
		subStop  = Math.min(to  , net.getMaxTimepoint());
	}
	
	/**
	 * Mark an Analysis as covering the entire utterance.
	 */
	public void unrestrict() {
		subStart = net.getMinTimepoint();
		subStop  = net.getMaxTimepoint();
	}
	
	/**
	 *  Simulate sentence fragments by changing all LVs 
	 *  that cross one of the boundaries given in CLAUSES.
	 *  The changed LVs will point to NIL
	 */
	public void separateRanges(List<Integer> clauses) {
		for(int i = net.getNoOfLevels() * net.getLexemeGraph().getMin();
				i < net.getNoOfLevels() * net.getLexemeGraph().getMax();
				i++) {
			LevelValue lv = slotFiller(i);
			if(lv!= null && lv.getRegentGN().spec()) {
				int down = lv.getDependentGN().getArc().from;
				int up   = lv.getRegentGN().getArc().from;
				for(int boundary : clauses) {
					if((down < boundary && up >= boundary) ||
							(down >= boundary && up < boundary)) {
						redirect(i, -1);
						break;
					}
				}
			}
		}
	}
	
	/**
	 * returns the one lexeme node of the given GN's lexemes, 
	 * that is currently selected in this analysis
	 * 
	 * there can never be more than one LN fulfilling this criterion
	 * and it can only be less than one in branching lattices
	 * 
	 * @param gn
	 * @return
	 */
	public LexemeNode getSelectedLNforGN(GraphemeNode gn) {
		for(LexemeNode ln : selectedLexemes) {
			if(gn.getLexemes().contains(ln))
				return ln;
		}
		
		return null;
	}
	
	/**
	 * Looks for occurrences of the vnSpec that are unused in this analysis
	 * returns the first one, if any
	 */
	public GraphemeNode getUnusedVNSpecInstance(VirtualNodeSpec vns) {
		LexemeGraph lg = net.getLexemeGraph();
		if(!(lg instanceof VirtualLexemeGraph))
			return null;
		
		VirtualLexemeGraph vlg = (VirtualLexemeGraph) lg;
		List<GraphemeNode> ocs = vlg.vnsOccurences(vns);
		for(GraphemeNode gn :  ocs) {
			if(isUnusedVirtualNode(gn)) {
				return gn;
			}
		}
		return null;
	}

	public List<LexemeNode> getSelectedLexemes() {
		return selectedLexemes;
	}
	
	/**
	 * Extends this analysis so that it binds another domain of an extended netstate
	 * state must contain one more token than this analysis
	 */
	public Analysis extendAnalysis(ConstraintNetState state) {
		// the state has already been extended, the analysis has not
		// This means that the ConstraintNetState has one additional slot for every level.
		assert state.getNet().getNoOfSlots() - slots.size() == state.getNet().getNoOfLevels();  
		
		Analysis ret = new Analysis(this);
		int newestTP = state.getNet().getLexemeGraph().getLastGN().getArc().from;
		ret.bindAnotherSlot(state, newestTP);

//		if(useVirtualNodes) {
//			// TODO #VN_VARIABLE_NR new virtual nodes might need binding, too
//			for(i=vectorSize(newSlots)/P.nrLevels;i<P.maxTimepoint;i++) {
//				bindAnotherSlot(A, newIsUnselected, newSlots,i);
//			}
//		}
		
		return ret;
	}
}
