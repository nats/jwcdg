package de.unihamburg.informatik.nats.jwcdg;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;
import de.unihamburg.informatik.nats.jwcdg.lattice.Lattice;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;
import de.unihamburg.informatik.nats.jwcdg.transform.ProblemSolver;
import de.unihamburg.informatik.nats.jwcdg.transform.SolverFactory;

/**
 * This class is a high level representation of a parser.
 * It is initialised with 
 * - a grammar, 
 * - a lexicon 
 * - a configuration
 * - as well as a way to generate a solver
 * 
 * it can then be used to parse sentences (or word graphs) 
 * 
 * @author the CDG-Team
 */
public class Parser {
	//private static Logger logger = LogManager.getLogger(Parser.class);
	
	protected Grammar grammar;
	protected Lexicon lexicon;
	protected Configuration config;
	protected SolverFactory factory;
	
	protected List<ParseObserver> observers;
	
	/**
	 * Splits an input sentence such as
	 * Viele 'Michael Jackson-Fans' weinten .
	 * @param sentence
	 * @return list of words
	 */
	public static List<String> splitSentence(String sentence) {
		sentence = sentence.trim();
		Matcher m = Pattern.compile("('(?:[^'\\\\]|\\\\')+'|(?:[^'\\\\ ]|\\\\')+)+").matcher(sentence);
		List<String> tokens = new ArrayList<>();
		String token;
		while (m.find()) {
			token = m.group();
			if (token.startsWith("'"))
				token = token.substring(1, token.length()-1);
			tokens.add(token); // or whatever you want to do
		}
		return tokens;
	}
	
	/**
	 * @param grammar
	 * @param lexicon
	 * @param config is cloned, so later changes have no effect on the parser
	 * @param factory
	 */
	public Parser(Grammar grammar, Lexicon lexicon, Configuration config,
			SolverFactory factory) {
		this.grammar = grammar;
		this.lexicon = lexicon;
		this.config = new Configuration(config);
		this.factory = factory;
		observers = new ArrayList<>();
	} 
	
	/**
	 * parse the given sentence 
	 * a minimalistic tokenizer is called, only splitting at spaces
	 * if you want to tokenize yourself, use parse(List<String> tokens)
	 * 
	 * @param sentence
	 * @return
	 */
	public Parse parse(String sentence) {
		List<String> tokens = splitSentence(sentence);
		if (tokens.size() == 0)
			return new Parse("emptyParse");
		return parse(tokens);
	}
	
	/**
	 * Parse the given pre-tokenized sentence 
	 */
	public Parse parse(List<String> tokens) {
		Lattice lat= new Lattice(tokens);
		lat.scan(true);
		return parse(lat);
	}
	
	/**
	 * Parse the given word lattice.
	 * Branching lattices are not supported, yet. 
	 */
	public Parse parse(Lattice lat) {
		ProblemSolver solver = factory.generateSolver(lat);
		for(ParseObserver obs : observers) {
			solver.registerObserver(obs);
		}
		solver.solve();
		return solver.getSolution();
	}
	
	public void registerObserver(ParseObserver observer) {
		observers.add(observer);
	}
	
	protected void registerParse(Parse p) {
		for(ParseObserver obs : observers) {
			obs.register(p);
		}
	}
	
	public Grammar getGrammar() {
		return grammar;
	}
	
	public Lexicon getLexicon() {
		return lexicon;
	}
	
	public Configuration getConfig() {
		return config;
	}
}