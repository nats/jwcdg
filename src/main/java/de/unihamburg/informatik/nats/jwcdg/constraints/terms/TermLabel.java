package de.unihamburg.informatik.nats.jwcdg.constraints.terms;


import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.VarInfo;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * term evaluating to the label of a levelvalue 
 * 
 * @author the CDG-Team
 *
 */
public class TermLabel extends Term {
	private final VarInfo vi; 
	private final int vindex;

	public TermLabel(VarInfo vi) {
		this.vi = vi;
		this.vindex = vi.index;
	}
	
	@Override
	public AVM eval(LexemeGraph lg, Context context,
			LevelValue[] binding) {
		return binding[vindex].getLabelVal();
	}

	@Override
	public String toString() {
		return String.format("%s.label", vi.varname);
	}
	
	public VarInfo getVi() {
		return vi;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + vi.hashCode();
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof TermLabel))
			return false;
		TermLabel other = (TermLabel) obj;
		if (!vi.equals(other.vi))
			return false;
		return true;
	}
}
