package de.unihamburg.informatik.nats.jwcdg.evaluation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.transform.Badness;
import de.unihamburg.informatik.nats.jwcdg.transform.ConstraintViolation;

/**
 * the result of applying a set of constraints to a set of levelvalues
 * 
 * @author the CDG-Team
 * 
 */
public class GrammarEvaluationResult implements Serializable{

	private static final long serialVersionUID = 1L;
    private List<ConstraintViolation> cvs;
	private double score;
	private Badness b; // optional
	private Badness nonFluctB; // optional

	public GrammarEvaluationResult(List<ConstraintViolation> cvs, double score,
			Badness b, Badness nonFluctB) {
		assert !cvs.contains(null);
		this.score = score;
		this.cvs = cvs;
		this.b = b;
		this.nonFluctB = nonFluctB;
	}

	public List<ConstraintViolation> getCVs() {
		return cvs;
	}

	public double getScore() {
		return score;
	}

	public Badness getBadness() {
		return b;
	}
	
	public Badness getNonFluctBadness() {
		return nonFluctB;
	}
	
	/**
	 * Adds resp. merges all fields of this and the given GER
	 * does not check for intersections in the conflicts, 
	 * so make sure you only call this for GERs of distinctive cosntraitn sets, 
	 * e.g. unary and binary or context sensitive and context free constraints.
	 * 
	 * mergeWith() Does not change this GrammarEvaluationResult.
	 */
	public GrammarEvaluationResult mergeWith(GrammarEvaluationResult other) {
		List<ConstraintViolation> cvs = new ArrayList<>(this.cvs);
		cvs.addAll(other.cvs);
		double score     = this.score * other.score;
		Badness b        = this.b.addBadness(other.b);
		
		Badness nonFlucB;
		if(this.nonFluctB !=null) {
			if(other.nonFluctB !=null) {
				nonFlucB = this.nonFluctB.addBadness(other.nonFluctB);	
			} else { 
				nonFlucB = this.nonFluctB;
			}
		} else {
			if(other.nonFluctB !=null) {
				nonFlucB = other.nonFluctB;	
			} else { 
				nonFlucB = null;
			}
		}
		
		return new GrammarEvaluationResult(cvs, score, b, nonFlucB);
	}
	
	@Override
	public String toString() {
		return b.toString();
	}
}
