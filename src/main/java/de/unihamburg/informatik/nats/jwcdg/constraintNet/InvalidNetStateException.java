package de.unihamburg.informatik.nats.jwcdg.constraintNet;

/**
 * 
 * This exception indicates that in the current state of a ConstraintNet there 
 * are no valid LevelValues for one of the slots 
 * or no valid LexemeNodes for one of the time points.
 * 
 * @author Niels Beuck
 */
public class InvalidNetStateException extends Exception {
	public InvalidNetStateException(String messsage) {
		super(messsage);
	}

	private static final long serialVersionUID = 1180678210574513474L;
}
