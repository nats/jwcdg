package de.unihamburg.informatik.nats.jwcdg.incrementality;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.unihamburg.informatik.nats.jwcdg.Parser;
import de.unihamburg.informatik.nats.jwcdg.evaluation.CachedGrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.frobbing.AbstractFrobber;
import de.unihamburg.informatik.nats.jwcdg.helpers.DaemonThreadFactory;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;
import de.unihamburg.informatik.nats.jwcdg.lattice.Lattice;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;
import de.unihamburg.informatik.nats.jwcdg.transform.SolverFactory;

public class IncrementalFrobber extends Parser implements IncrementalParser<IncrementalStep> {

	public static final String FLUCTUATING_SCORE            = "scoreFluctuating";
	public static final String VIRT_FLEXIBLE_LABEL_MATCHING = "virtFlexibleLabelMatching";
	public static final String MATCH_VIRTUAL_NODES          = "matchVirtualNodes";
	public static final String VIRT_MATCHING_FACTOR         = "virtMatchingFactor";

	private static Logger logger = LogManager.getLogger(IncrementalFrobber.class);
	
	private GrammarEvaluator evaluator;
	
	private ExecutorService executor;
	
	/**
	 * constructor
	 * 
	 * @param evaluator
	 * @param lexicon
	 * @param factory
	 * @param config
	 */
	public IncrementalFrobber(Grammar grammar, Lexicon lexicon,
			SolverFactory factory, Configuration config) {
		super(grammar, lexicon, config, factory);
		this.evaluator = new CachedGrammarEvaluator(grammar, config); // TODO #EVALUATOR allow other kinds of evaluators
		this.executor = Executors.newSingleThreadExecutor(new DaemonThreadFactory());
	}
	
	@Override
	public Parse parse(Lattice lat) {
		throw new RuntimeException("not implemented");
	}
	
	@Override
	public Parse parse(java.util.List<String> tokens) {
		assert !tokens.isEmpty();
		
		IncrementalStep step = IncrementalStep.makeInitialStep(evaluator, lexicon, config);
		
		for(int i = 0; i < tokens.size(); i++) {
			logger.info("====================================================");
			ChunkIncrement chunk = new ChunkIncrement(tokens.get(i));
			
			step= continueParsing(step, chunk, i==tokens.size()-1, null);
			
			//logger.info("\n" + step.getParse().print());
		}
		
		return step.getParse();
	}
	
	/**
	 * create an initial step to start from
	 */
	public IncrementalStep makeInitialStep() {
		return IncrementalStep.makeInitialStep(evaluator, lexicon, config);
	}
	
	/**
	 * given a previous step and a new word,
	 * this function extends the problem from the previous step with the given word
	 * and frobbs it
	 *
	 * final says, if this iteration should be treated as the final iteration over the sentence
	 *
	 * result is the next incremental step
	 *
	 */
	public IncrementalStepFuture continueParsingAsync(IncrementalStep previousStep, ChunkIncrement nextChunk, 
			boolean isFinalStep, List<String> lookahead) {
		IncrementalStepFuture result = new IncrementalStepFuture (previousStep, nextChunk,
		                                    isFinalStep, lookahead, config,
		                                    evaluator, factory);
		result.setObservers(observers);
		// start computation in a new step
		executor.execute(result);
		return result;
	}
	
	/**
	 * the synchronized version of continueParsingAsync, using the timelimit
	 * from the config
	 *
	 */
	public IncrementalStep continueParsing(IncrementalStep previousStep, ChunkIncrement nextChunk, 
			boolean isFinalStep, List<String> lookahead) {
		IncrementalStepFuture f = continueParsingAsync(previousStep, nextChunk, isFinalStep, lookahead);
		IncrementalStep step;
		try {
			try {
				step = f.get(config.getInt(AbstractFrobber.TIMELIMIT), TimeUnit.MILLISECONDS);
			} catch (TimeoutException e) {
				f.pleaseFinish();
				step = f.get();
			}
		} catch (InterruptedException e) {
			throw new RuntimeException("Interruption caught!");
		} catch (ExecutionException e) {
			throw new RuntimeException("ExecutionExeption caught!");
		}
		return step;
	}

	@Override
	public List<IncrementalStep> parseIncrementally(IncrementalStep init, List<ChunkIncrement> chunks) {
		List<IncrementalStep> steps = new ArrayList<>(chunks.size());
		
		IncrementalStep step= init;
		for(int i=0; i < chunks.size(); i++) {
			step = continueParsing(step, chunks.get(i), i== chunks.size()-1, null);
			steps.add(step);
		}
		
		return steps;
	}
	
	@Override
	public List<IncrementalStep> parseIncrementally(List<String> tokens) {
		List<ChunkIncrement> chunks = new ArrayList<>(tokens.size());
		for(String token : tokens) {
			chunks.add(new ChunkIncrement(token));
		}
		
		return parseIncrementally(IncrementalStep.makeInitialStep(evaluator, lexicon, config), chunks);
	}

	@Override
	public List<IncrementalStep> parseIncrementally(String sentence) {
		StringTokenizer tokenizer = new StringTokenizer(sentence);
		List<String> tokens = new ArrayList<>();
		while(tokenizer.hasMoreTokens()) {
			tokens.add(tokenizer.nextToken());
		}
		return parseIncrementally(tokens);
	}
	
	
}
