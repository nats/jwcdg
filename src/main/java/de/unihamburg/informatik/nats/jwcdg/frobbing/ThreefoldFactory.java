package de.unihamburg.informatik.nats.jwcdg.frobbing;

import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;

public class ThreefoldFactory extends FrobbingFactory {
	public static final String NAME = "threefold";

	public ThreefoldFactory(Grammar grammar, Lexicon lex, Configuration config) {
		super(grammar, lex, config, FrobbingMethod.Threefold);
	}
}
