package de.unihamburg.informatik.nats.jwcdg.incrementality;

import java.util.List;

public interface IncrementalParser <T extends IncrementalParsingState> {

	T continueParsing(
            T previousStep,
            ChunkIncrement nextChunk,
            boolean isFinalStep,
            List<String> lookahead);
	
	List<T> parseIncrementally(T initialStep, List<ChunkIncrement> chunks);
	List<T> parseIncrementally(List<String> tokens);
	List<T> parseIncrementally(String sentence);
}
