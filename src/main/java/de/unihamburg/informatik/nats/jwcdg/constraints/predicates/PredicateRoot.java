package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * is #1 the root node?
 */
public class PredicateRoot extends Predicate {

	public PredicateRoot(Grammar g) {
		super("root", g, false, 1, false);
	}

	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {

		AVM val = args.get(0).eval(lg, context, binding);
		AVMLexemNode lnval;
		if (val instanceof AVMLexemNode) {
			lnval = (AVMLexemNode) val;
		} else {
			throw new PredicateEvaluationException("WARNING: argument of predicate `root' not a lexeme node");
		}
		
		return (lnval.getLexemeNode().isNil());
	}

}
