package de.unihamburg.informatik.nats.jwcdg.avms;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import de.unihamburg.informatik.nats.jwcdg.input.Configuration;

public class AVMConj extends AVM {
	private static final long serialVersionUID = -1332321925268210409L;
	
	private List<AVM> list;

	public AVMConj(List<AVM> list) {
		this.list = list;
	}
	public List<AVM> getList() {
		return list;
	}

	@Override
	public AVM followPathImpl(Path path) {
		String key = path.getHead();
		for(AVM avm: list) {
			if (avm instanceof AVMNode) {
				AVMNode node = (AVMNode) avm;
				if(node.getAttribute().equals(key)) {
					return node.getValue().followPath(path.getRest());
				}
			}
		}
		return new AVMError(String.format("Error: This attribute is not available! Path: %s",
				path.toString()));
	}
	
	
	@Override
	public List<Path> possiblePaths() {
		// collect all subexpressions
		List<Path> ret = new ArrayList<>();
		for(AVM avm : list) {
			ret.addAll(avm.possiblePaths());
		}
		return ret;
	}
	
	@Override
	public AVM substituteRefs(Matcher matcher, Configuration config) {
		List<AVM> l2 = new ArrayList<>(list.size());
		for(AVM val : list) {
			l2.add(val.substituteRefs(matcher, config));
		}
		
		return new AVMConj(l2);
	}
	
	@Override
	public String toString() {
		return "Conj: "+ list.toString();
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((list == null) ? 0 : list.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AVMConj))
			return false;
		AVMConj other = (AVMConj) obj;
		if (list == null) {
			if (other.list != null)
				return false;
		} else if (!list.equals(other.list))
			return false;
		return true;
	}
}
