package de.unihamburg.informatik.nats.jwcdg.parse;

import com.google.common.collect.Lists;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.GraphemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.Constraint;
import de.unihamburg.informatik.nats.jwcdg.constraints.Level;
import de.unihamburg.informatik.nats.jwcdg.evaluation.ConstraintEvaluationResult;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluationResult;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;
import de.unihamburg.informatik.nats.jwcdg.transform.Badness;
import de.unihamburg.informatik.nats.jwcdg.transform.ConstraintViolation;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;
import io.gitlab.nats.deptreeviz.ParseInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/** 
 * A lexicalized version of a parse,
 * i.e. a parse decorated with aspects of an analysis.
 * It is quite a redundant structure, therefore.
 * 
 * It can be evaluated, modified and partly optimized, 
 * but cannot directly used for constraint driven parsing, 
 * as it is not directly associated to a constraint net.
 *
 * In contrast to an abstract parse, it includes 
 * lexeme selection and concrete levelvalues.
 *
 * @author the CDG-Team
 *
 */
public class DecoratedParse extends Parse implements ParseInterface<Word> {
	private static final long serialVersionUID = 7360457384158175086L;
	private static Logger logger = LogManager.getLogger(DecoratedParse.class);
	/**
	 * the concrete levelvalues
	 */
	private List<LevelValue> lvs;

	private Badness b;

	private List<ConstraintViolation> violations;

	private LexemeGraph lg;

	private GrammarEvaluator evaluator;

	private List<LexemeNode> lexicalization;
	
	private List<String> annoFeats;
	
	
	public DecoratedParse(LexemeGraph lg, List<LevelValue> lvs,
			List<LexemeNode> lns, List<String> annoFeats,
			GrammarEvaluator evaluator,
			List<Integer> exceptions) {
		super("");
		this.b = null;
		this.lg = lg;
		this.evaluator = evaluator;
		this.lexicalization = new ArrayList<>(lns);
		this.annoFeats = new ArrayList<>(annoFeats);
		
		computeStructure(lvs, this.lexicalization, this.annoFeats, exceptions);
		
		this.lvs = new ArrayList<>(lvs.size());
		for(LevelValue lv: lvs) {
			this.lvs.add(new LevelValue(lv));
		}
		
		this.eval(evaluator, lg);
	}

	/**
	 * Compute the score and the conflicts of the Parse.
	 */
	private void eval(GrammarEvaluator evaluator, LexemeGraph lg) {
		//double score = 1.0;
		b= Badness.bestBadness();
		GrammarEvaluationResult ger;

		violations = new LinkedList<>();
		List<LevelValue> lvs = sortLVsAccordingToGrammar(evaluator.getGrammar()); 

		for (LevelValue lva : lvs) {
			if (lva != null) {
				/* seek unary violations */
				ger= evaluator.evalUnary(lva, lg, new Context(lvs), false, true);
				b= b.addBadness(ger.getBadness());
				violations.addAll(ger.getCVs());

				/* seek binary violations */
				for (LevelValue lvb : lvs) {
					if(lvb == null || lvs.indexOf(lva) >= lvs.indexOf(lvb))
						continue;
					ger = evaluator.evalBinary(lva, lvb, lg, new Context(lvs), false, true);
					b= b.addBadness(ger.getBadness());
					violations.addAll(ger.getCVs());
				}
			}
		}

		/* sort the list of violations */
		Collections.sort(this.violations);

		/* compute constraint list */
		info.setEntry("score",String.format("%.4f", b.getScore()));
	}

	@Override
	public DecoratedParse decorate(GrammarEvaluator evaluator, Lexicon lex,
			Configuration config) {
		return this;
	}

	@Override
	public String print() {
		return print(false, 1.0);
	}
	
	@Override
	public String print(boolean skipUninteresting) {
		return print(skipUninteresting, 1.0);
	}
	
	/**
	 * 
	 * returns a multi line string of the following form:
	 * 
	 "\n"
	 "   parse id    : %s\n"
	 "   lattice id  : %s\n"
	 "   username    : %s\n"
	 "   date        : %s"
	 "   strategy    : %s\n"
	 "   comment     : %s\n"
	 "   score       : %4.3e\n"
	 "   optimal     : %s\n\n   bindings:\n",
	 parse->id,
	 parse->latticeId,
	 parse->userName,
	 ctime(&parse->dateOfCreation),
	 parse->searchStrategy,
	 parse->comment ? parse->comment : "<undefined>",
	 parse->score,
	 parse->optimal ? "yes" : "no");
	 */
	public String print(boolean skipUninteresting, double threshold) {
		String label;
		Word word;
		int noOfLevels = levels.size();
		
		StringBuilder ret = new StringBuilder("\n"); // TODO is a leading \n neccessary?

		for (int i = 0; i < words.size(); i++) {
			for(String levelId : levels) {
				label = verticesLabels.get(levelId).get(i);
				if (skipUninteresting && label.equals("") && 
						verticesStructure.get(levelId).get(i) == -1)
					continue;
	
				word = words.get(i);
	
				ret.append(String.format("   %03d %10s: %25s(%2d-%2d) --%4s--> ", 
						word.from * noOfLevels + levels.indexOf(levelId), levelId,
						word.description, word.from, word.to, label));
	
				int reg = verticesStructure.get(levelId).get(i);
				if (reg == -1) {
					ret.append("NIL\n");
				} else if (reg == -2) {
					ret.append("NONSPEC\n");
				} else {
					word = words.get(reg);
					ret.append(String.format("%s(%2d-%2d)\n", word.description,
							word.from, word.to));
				}
			}
		}
		
		ret.append("\n   Score: ").append(b.toString()).append("\n");
		
		if (violations != null) {
			ret.append("\n   Violations:\n");
	
			Collections.sort(violations);
			for (ConstraintViolation cv : violations) {
				if (cv.getPenalty() > threshold)
					continue;
				
				ret.append(cv.print());
				ret.append("\n");
			}
		}

		ret.append("\n");
		return ret.toString();
	}
	
	public Badness getBadness() {
		return b;
	}
	
	public List<ConstraintViolation> getViolations() {
		return violations;
	}
	
	/**
	 * evaluates a given constraint on the whole structure
	 * 
	 * @param c
	 * @return all ConstraintViolations
	 */
	public List<ConstraintViolation> evalConstraint(Constraint c) {		
		//List<LevelValue> lvs = sortLVsAccordingToGrammar(g);
		List<ConstraintViolation> cvs = new ArrayList<>();
		ConstraintEvaluationResult cer;
		
		for(LevelValue lv : lvs) {
			switch (c.getSignature().arity) {
			case 1:
				cer = c.eval(lg, new Context(lvs), lv);
				cvs.add(cer.toCV());
				break;
			case 2:
				for(LevelValue lv2 : lvs) {
					cer = c.eval(lg, new Context(lvs), lv, lv2);
					cvs.add(cer.toCV());
				}
				break;
			default:
				throw new RuntimeException("Unsupported constraint arity "+ c.getSignature().arity); 
			}	
		}
		
		return cvs;
	}
	
	public ConstraintEvaluationResult evalConstraintByName(String cName, String lvl, int tp, Grammar g) {
		Constraint c = g.findConstraint(cName);
		if(c==null)
			return null;
		
		List<LevelValue> lvs = sortLVsAccordingToGrammar(g);
		
		LevelValue lv = null;
		for(LevelValue lv2 : lvs) {
			if(lv2.getLevel().getId().equals(lvl) && lv2.getDependentGN().getArc().from == tp) {
				lv = lv2;
				break;
			}
		}
		if(lv== null)
			return null;
		return c.eval(lg, new Context(lvs), lv);
	}
	
	private List<LevelValue> sortLVsAccordingToGrammar(Grammar g) {
		List<LevelValue> ret= new ArrayList<>(lvs.size());
		for(int tp=0; tp < lg.getMax(); tp++) {
			for(Level lvl : g.getLevels()) {
				for(LevelValue lv2 : lvs) {
					if(lv2.getLevel().getId().equals(lvl.getId()) 
							&& lv2.getDependentGN().getArc().from == tp) {
						ret.add(lv2);
						break;
					}
				}
			}
		}
		return ret;
	}

	public ConstraintEvaluationResult evalConstraintByName(String cName, String lvl1, int tp1, String lvl2, int tp2, Grammar g) {
		Constraint c = g.findConstraint(cName);
		if(c==null)
			return null;
		
		List<LevelValue> lvs = sortLVsAccordingToGrammar(g);
		
		LevelValue lv1 = null;
		for(LevelValue lv1a : lvs) {
			if(lv1a.getLevel().getId().equals(lvl1) && lv1a.getDependentGN().getArc().from == tp1) {
				lv1 = lv1a;
				break;
			}
		}
		if(lv1== null)
			return null;
		LevelValue lv2 = null;
		for(LevelValue lv2a : lvs) {
			if(lv2a.getLevel().getId().equals(lvl2) && lv2a.getDependentGN().getArc().from == tp2) {
				lv2 = lv2a;
				break;
			}
		}
		if(lv2== null)
			return null;
		return c.eval(lg, new Context(lvs), lv1, lv2);
	}
	
	/**
	 * @return the possible lexical readings at the given position
	 */
	public List<LexemeNode> getLexemeVariants(int pos) {
		return lg.getGNs().get(pos).getLexemes();
	}
	
	/**
	 * Change the lexical reading of a word in the parse,
	 * the word is denoted by the lexeme node. 
	 * 
	 * The lexeme node must be from the lexeme graph of this parse.
	 * 
	 * changes word and levelvalues.
	 * 
	 * recalculates the score of the parse.
	 * 
	 * @param ln
	 */
	public void selectLexeme(LexemeNode ln) {
		// four kinds of information have to be updated:
		// 1. word
		int pos = ln.getArc().from;
//		int offset=0; // TODO
//		if(!exceptions.isEmpty()) {
//			offset= adjustWordIndex(pos, exceptions) - pos;
//		}
		
		Word w = new Word(ln, annoFeats, 0);
		words.set(pos, w);
		
		// 2. lexeme
	    GraphemeNode gn = ln.getGN();
		for (int i = 0; i < lexicalization.size(); i++) {
		    if (lexicalization.get(i).getGN().equals(gn)) {
		        lexicalization.set(i, ln);
		    }
		}
		
		// 3. LVs
		for(int i=0; i < lvs.size(); i++) {
			LevelValue lv = lvs.get(i);
			if(lv.getDependentGN().equals(gn) && !lv.getDependents().contains(ln)) {
				LevelValue newLV = new LevelValue(
						lv.getLevel(), 
						evaluator.getGrammar().getNoOfLevels(), 
						Lists.newArrayList(ln), 
						gn, 
						lv.getLabel(), 
						lv.getRegents(), 
						lv.getRegentGN());
				lvs.set(i, newLV);
			}
			if(lv.getRegentGN().equals(gn) && !lv.getRegents().contains(ln)) {
				LevelValue newLV = new LevelValue(
						lv.getLevel(), 
						evaluator.getGrammar().getNoOfLevels(), 
						lv.getDependents(), 
						lv.getDependentGN(), 
						lv.getLabel(), 
						Lists.newArrayList(ln), 
						gn);
				lvs.set(i, newLV);
			}
		}
		
		// 4. score
		eval(evaluator, lg);
	}
	
	/**
	 * Change the label of an edge.
	 * 
	 * recalculates the score of the parse.
	 * 
	 * @param pos
	 * @param level
	 * @param label
	 */
	public void selectLabelAt(int pos, String level, String label) {
		verticesLabels.get(level).set(pos, label);
		
		for(int i = 0; i < lvs.size(); i++) {
			LevelValue lv = lvs.get(i);
			if(lv.getLevel().getId().equals(level) 
					&& lv.getDependentGN().getNo() == pos 
					&& !lv.getLabel().equals(label)) {
				LevelValue newLV = new LevelValue(
						lv.getLevel(), 
						evaluator.getGrammar().getNoOfLevels(), 
						lv.getDependents(), 
						lv.getDependentGN(), 
						label, 
						lv.getRegents(), 
						lv.getRegentGN());
				lvs.set(i, newLV);
			}
		}
		
		eval(evaluator, lg);
	}
	
	/**
	 * Change the head node of a dependency efge (but not the label)
	 * 
	 * recalculates the score of the parse.
	 * 
	 * @param pos
	 * @param level
	 * @param to
	 */
	public void redirectEdge(int pos, String level, int to) {
		if( pos < 0 || pos >= lg.getMax()) {
			throw new IllegalArgumentException("Illegal position " + pos);
		}
		
		if( to < -2 || to >= lg.getMax()) {
			throw new IllegalArgumentException("Illegal destination " + to);
		}
		
		if( to == pos) {
			throw new IllegalArgumentException("Position and destination must not be equal, as a word cannot be attached to itself");
		}
		
		verticesStructure.get(level).set(pos, to);

		for(int i = 0; i < lvs.size(); i++) {
			LevelValue lv = lvs.get(i);
			if(lv.getLevel().getId().equals(level) 
					&& lv.getDependentGN().getNo() == pos 
					&& lv.getRegentGN().getNo() != to) {
				
				GraphemeNode gn;
				List<LexemeNode> lns= null;
				if(to == -1) {
					gn= lg.getNil();
					lns = gn.getLexemes();
				} else if(to == -2) {
					gn= lg.getNonspec();
					lns = gn.getLexemes();
				} else {
					gn = lg.getGNs().get(to);
					for(LexemeNode ln : lexicalization) {
						if(ln.getGN().equals(gn)) {
							lns = Lists.newArrayList(ln);
							break;
						}
					}
				}
				
				if(lns == null) {
					throw new RuntimeException("No lexeme found at position "+ to);
				}
				
				LevelValue newLV = new LevelValue(
						lv.getLevel(), 
						evaluator.getGrammar().getNoOfLevels(), 
						lv.getDependents(), 
						lv.getDependentGN(), 
						lv.getLabel(), 
						lns,  
						gn);
				lvs.set(i, newLV);
			}
		}
		
		eval(evaluator, lg);
	}
	
	/**
	 * changes the parse to hold the best label in the specified domain (position+level),
	 * everything else staying the same,
	 * according to the grammar evaluator (given at construction time).
	 * 
	 * @param pos
	 * @param levelname
	 */
	public void optimizeLabel(int pos, String levelname) {
		Level level = evaluator.getGrammar().findLevel(levelname);
		
		String current = verticesLabels.get(levelname).get(pos);
		Badness currentB = b;
		String result  = "";
		Badness best = Badness.worstBadness();
		
		for (String label : level.getLabels()) {
			selectLabelAt(pos, levelname, label);
			if (b.compare(best)) {
				result = label;
				best = b;
			}
		}

		if (!best.compare(currentB)) {
			result = current;
		}

		selectLabelAt(pos, levelname, result);
	}
	
	/**
	 * changes the parse to hold the best head in the specified domain (position+level),
	 * everything else staying the same,
	 * according to the grammar evaluator (given at construction time).
	 * 
	 * @param pos
	 * @param levelname
	 */
	public void optimizeStructure(int pos, String levelname) {
		int current = verticesStructure.get(levelname).get(pos);
		Badness currentB = b;
		int result  = 0;
		Badness best = Badness.worstBadness();
		
		for (int i = lg.getMin(); i <= lg.getMax(); i++) {
			if (i != pos) {
				int j = i;
				/* try NIL last */
				if (j == lg.getMax()) {
					j = -1;
				}

				redirectEdge(pos, levelname, j);
				if (b.compare(best)) {
					result = j;
					best = b;
				}
			}
		}

		if (!best.compare(currentB)) {
			result = current;
		}

		redirectEdge(pos, levelname, result);
	}
	
	/**
	 * changes the parse to hold the best word at the specified position,
	 * everything else staying the same,
	 * according to the grammar evaluator (given at construction time).
	 * 
	 * @param pos
	 * @param levelname
	 */
	public void optimizeWord(int pos) {
		LexemeNode current = getSelectedLnAt(pos);
		Badness currentB = b;
		LexemeNode result = null;
		Badness best = Badness.worstBadness();
		
		for (LexemeNode ln : lg.getGNs().get(pos).getLexemes()) {
			selectLexeme(ln);
			if (b.compare(best)) {
				result = ln;
				best = b;
			}
		}
		
		if (!best.compare(currentB)) {
			result = current;
		}
		
		selectLexeme(result);
	}
	
	/**
	 * can handle -1 and -2 postions,
	 * returns null if no node can be found
	 * 
	 * @param pos
	 * @return
	 */
	private LexemeNode getSelectedLnAt(int pos) {
		if(pos == -1) {
			return lg.getNil().getLexemes().get(0);
		} else if(pos == -2) {
			return lg.getNonspec().getLexemes().get(0);
		} else {
			GraphemeNode gn = lg.getGNs().get(pos);
			for(LexemeNode ln : lexicalization) {
				if(ln.getGN().equals(gn)) {
					return ln;
				}
			}
		}
		return null;
	}
	
}
