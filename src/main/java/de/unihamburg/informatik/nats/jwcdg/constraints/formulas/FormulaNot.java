package de.unihamburg.informatik.nats.jwcdg.constraints.formulas;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.AccessContext;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

public class FormulaNot extends Formula {
	
	private Formula subform;
	
	public FormulaNot(Formula f) {
		subform = f;
		this.isContextSensitive = f.isContextSensitive;
	}
	@Override
	public boolean evaluate(LevelValue[] binding, Context context, LexemeGraph lg) {
		return ! subform.evaluate(binding, context, lg);
	}

	@Override
	public void collectFeatures(List<Path> sofar) {
		subform.collectFeatures(sofar);
	}

	@Override
	public boolean check() {
		if(!subform.check())
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return String.format("~%s", subform.toString());
	}

	@Override
	/** The information inside a negation could also be used to affect the
	    current context, but that would require at least one more Hashtable
	    to distinguish what edge types were already forbidden and which were
	    forbidden by the negated formula. At the moment we simply pretend
	    that negated formulas do not impose any edge type restrictions.
	    This loses some optimization but makes no errors. */
	public void analyze(AccessContext context, Grammar g) {
		AccessContext throwaway = new AccessContext(context);
		subform.analyze(throwaway, g);
	}
	
	@Override
	public void collectHasInvocations(List<Formula> akku) {
		subform.collectHasInvocations(akku);
	}
}
