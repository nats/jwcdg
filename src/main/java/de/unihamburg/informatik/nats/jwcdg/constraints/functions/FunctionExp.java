package de.unihamburg.informatik.nats.jwcdg.constraints.functions;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * returns the value of e raised to the power of x
 */
public class FunctionExp extends Function {

	public FunctionExp(Grammar g) {
		super("exp", g, 1);
	}

	@Override
	public AVM eval(List<Term> args, Context context, LexemeGraph lg,
			LevelValue[] binding) {
	
		AVM val = args.get(0).eval(lg, context, binding);
		if (val instanceof AVMNumber) {
			AVMNumber numVal = (AVMNumber) val;
			return new AVMNumber(Math.exp(numVal.getNumber()));
		} else {
			return new AVMError("WARNING: argument of function `exp' not a number");
		}
	}	

}
