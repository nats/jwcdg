package de.unihamburg.informatik.nats.jwcdg.avms;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import de.unihamburg.informatik.nats.jwcdg.input.Configuration;

public class AVMNode extends AVM {
	private static final long serialVersionUID = 1061838730778084287L;

	private String attribute;
	private AVM    value;

	public AVMNode(String attribute, AVM value) {
		this.attribute = attribute;
		this.value = value;
	}
	public String getAttribute() {
		return attribute;
	}
	
	public AVM getValue() {
		return value;
	}
	
	@Override
	protected AVM followPathImpl(Path path) {
		if(path.getHead().equals(attribute)) {
			return value.followPath(path.getRest());
		}
		return new AVMError("not my path");
	}
	
	@Override
	public List<Path> possiblePaths() {
		List<Path> ret = new ArrayList<>();
		List<Path> sub = value.possiblePaths();
		for(Path p : sub) {
			ret.add(new Path(attribute, p));
		}
		
		return ret;
	}

	@Override
	public String toString() {
		return String.format("[%s: %s]", attribute, value.toString());
	}
	
	@Override
	public AVM substituteRefs(Matcher matcher, Configuration config) {
		return new AVMNode(attribute, value.substituteRefs(matcher, config));
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((attribute == null) ? 0 : attribute.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AVMNode))
			return false;
		AVMNode other = (AVMNode) obj;
		if (attribute == null) {
			if (other.attribute != null)
				return false;
		} else if (!attribute.equals(other.attribute))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
}
