package de.unihamburg.informatik.nats.jwcdg.gui;

import de.unihamburg.informatik.nats.jwcdg.IncrementalParserFactory;
import de.unihamburg.informatik.nats.jwcdg.JWCDG;
import de.unihamburg.informatik.nats.jwcdg.incrementality.IncrementalFrobber;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;
import io.gitlab.nats.deptreeviz.DepTree;
import org.antlr.runtime.RecognitionException;
import org.apache.batik.swing.JSVGCanvas;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.w3c.dom.svg.SVGDocument;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

/**
 * Displays the words in a parse as a tree in a window, with interactions for changing the
 * connections and labels If you just want to have a SVG Document without opening a window, then use
 * the class DepTreeGenerator
 *
 * @author Sven Zimmer, Arne Köhn and Christine Köhn
 */
public class DepTreeViewer extends AbstractViewer {
	public static final String PARSE_AFTER_EVERY_WORD = "parseAfterEveryWord";

	private DepTree _dt = null;
	private DepTreeDisplayInteractor _dtDI;

	public static void main(String[] args) throws IOException {
		Options options = initOptions();
		CommandLine line = initCommandLine(options, args);
		String[] argsString = line.getArgs();

		if (argsString.length > 1) {
			System.err.println("You have provided more than one argument. Only one argument " +
					"(path to a cda file) is supported.");
			printHelp(options);
			System.exit(1);
		}

		if (line.hasOption("help")) {
			printHelp(options);
			System.exit(0);
		}

		File cda = null;
		if (argsString.length == 1) {
			cda = new File(argsString[0]);
			boolean error = false;
			if (!cda.exists()) {
				System.err.println(String.format("File does not exist: %s",
						cda.getAbsolutePath()));
				error = true;
			} else if (cda.isDirectory()) {
				System.err.println(String.format("File is not a cda file but a directory: %s",
						cda.getAbsolutePath()));
				error = true;
			} else if (!cda.canRead()) {
				System.err.println(String.format("Cannot open file, check permissions: %s",
						cda.getAbsolutePath()));
				error = true;
			}
			if (error) {
				printHelp(options);
				System.exit(1);
			}
		}

		Configuration config = readConfig(line, options);
		new DepTreeViewer(config, cda);
	}

	private static void printHelp(Options options) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("DepTreeViewer [-c CONFIG_FILE] [CDA_FILE]", "", options,
				"For more information, please refer to the README file.");
	}


	/**
	 * @param config !=null
	 * @throws IOException
	 */
	public DepTreeViewer(Configuration config) throws IOException {
		new DepTreeViewer(config, null);
	}

	/**
	 * @param config !=null
	 * @throws IOException
	 */
	private DepTreeViewer(Configuration config, File cda) throws IOException {
		IncrementalFrobber parser = JWCDG.makeParser(config,
				new IncrementalParserFactory());
		initGrammarString(config);

		_dt = new DepTree();
		_dt.setParsingIncrementally(config.getFlag(PARSE_AFTER_EVERY_WORD));
		_dtDI = new DepTreeDisplayInteractor();
		_dtDI.activateTurboMaybe(parser.getConfig());
		_dtDI.setFields(_dt, parser, null);
		createDisplay(_dtDI, new DepTreeDisplay(_dt, true, config));
		if (cda != null) {
			try {
				_dtDI.loadCDAfromFile(cda);
			} catch (RecognitionException e) {
				System.err.println(String.format("Could not parse file. Is it a cda file?: %s",
						cda.getAbsolutePath()));
				e.printStackTrace();
				System.exit(1);
			}
		}
	}

	public void add(Parse parse, String directory) {
		_dtDI.add(parse, directory);
	}

	public void add(Parse parse,
					String directory,
					Map<Integer, ArrayList<String>> markedNodes) {
		_dtDI.add(parse, directory, markedNodes);
	}

	public JSVGCanvas getSVGCanvas() {
		return _dt.getNodesCanvas();
	}

	public SVGDocument getSVG() {
		return _dt.getDoc();
	}

	public DepTreeDisplay getGUI() {
		return _dtDI._GUI;
	}

	public void setFont(Font font) {
		_dt.setFont(font);
	}

	public Font getFont() {
		return _dt.getFont();
	}

	public DepTree getDepTree() {
		return _dt;
	}
}
