package de.unihamburg.informatik.nats.jwcdg.constraints.functions;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

public abstract class Function {
	public final int arity;
	public final int maxArity;
	public final boolean iscontextSensitive;
	protected Grammar grammar;
	public final String name;
	
	
	public Function(String name, Grammar grammar, int arity) {
		this.arity = arity;
		this.maxArity = arity;
		this.iscontextSensitive = false;
		this.name= name;
		this.grammar = grammar;
	}
	
	public Function(String name, Grammar grammar, int minArity, int maxArity, boolean iscontextSensitive) {
		this.arity = minArity;
		this.maxArity = maxArity;
		this.iscontextSensitive = iscontextSensitive;
		this.name = name;
		this.grammar = grammar;
	}

	public boolean isContextSensitive() {
		return iscontextSensitive;
	}

	public abstract AVM eval(List<Term> args, Context context, LexemeGraph lg, LevelValue[] binding );
	
	@Override
	public String toString() {
		return name;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Function))
			return false;
		Function other = (Function) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
