package de.unihamburg.informatik.nats.jwcdg.input;

import java.io.Reader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.runtime.CharStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.unihamburg.informatik.nats.jwcdg.ForgettingANTLRInputStream;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarLexer;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarParser;
import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraints.AccessContext;
import de.unihamburg.informatik.nats.jwcdg.constraints.Connection;
import de.unihamburg.informatik.nats.jwcdg.constraints.Constraint;
import de.unihamburg.informatik.nats.jwcdg.constraints.Direction;
import de.unihamburg.informatik.nats.jwcdg.constraints.Level;
import de.unihamburg.informatik.nats.jwcdg.constraints.formulas.Formula;
import de.unihamburg.informatik.nats.jwcdg.incrementality.virtualNodes.VirtualNodeSpec;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;

public class Grammar implements Serializable{
	
    private static final long serialVersionUID = 1L;
    /* ***********
	 * Constants *
	 *********** */	
	public static final String DEACTIVATED_CONSTRAINT_CATEGORIES = "deactivatedConstraintCategories";
	public static final String LABEL_GROUPS      = "labelGroups";
	public static final String FEATURE_HIERARCHY = "featureHierarchy";
	
	/* ***********
	 * Fields *
	 *********** */
	private Map<String, Constraint> constraints;     /* loaded constraints */
	private List<Level> levels;                      /* level declarations */
	private Map<String, Hierarchy> hierarchies;      /* loaded hierarchies */
	private Map<String, Map<String, AVM>> maps;      /* loaded data maps */
	
	private int noOfLevels;                 /* number of loaded levels */
	private int maxLabelLength;             /* length of longest CDG label */
	private int maxNrLabels;                /* maximal number of labels in one level */
	
	private Map<Integer, List<Constraint>> hashedUCs;          /* unary constraints by signature */
	private Map<Integer, List<Constraint>> hashedFlUCs;        /* unary constraints requested by increment to be hashed */
	private Map<Integer, List<Constraint>> hashedFlUCsNonspec; /* same but only for nonspec edges */
	private Map<Integer, List<Constraint>> hashedBCs;          /* binary constraints by signature */
	
	//Vector levelMatrixCounter;      /* counts # of constraints between levels */
	private List<Path> allAttributes; /* Used to flatten LexiconItems and Constraints which are trees */
	
    /** list of VirtualNodeSpecs, used for virtual nodes in anticiaptory incremental parsing */
	private List<VirtualNodeSpec> vnspecs;
	// TODO #BEAMSEARCH
//	private List<TopdownPredictor> topDownPredictors;         /* list of constraint based top down prediction opportunities used in incremental beamsearch */
	
	/* which edge types look at which features  */
	private Map<Integer, Set<Path>> upFeatures;              
	private Map<Integer, Set<Path>> downFeatures; 
	
	private boolean isContextSensitive;   /* does it contain c.s. constraints? */
	private int noHas;                     /* counts instances of has() */
	// private int no_ternary;                 /* counts all ternary constraints */
	
	private int noOfDirections;
	private int noOfConnections;
	
	private List<String>     deactivatedSections;
	private List<Constraint> deactivatedConstraints;
	
	private static Logger logger = LogManager.getLogger(Grammar.class);
	
	private int constraintCounter;

	private boolean cachingValid;
	
	public static Grammar fromFile(Reader is, Configuration config) throws RecognitionException {
		CharStream cs = new ForgettingANTLRInputStream(is);
		jwcdggrammarLexer lexer = new jwcdggrammarLexer(cs);
		CommonTokenStream ts = new CommonTokenStream(lexer);
		jwcdggrammarParser pGram = new jwcdggrammarParser(ts);
		
		Grammar g = pGram.cdggrammar();
		g.cache(config);
		return g;
	}
	
	/* **************
	 * Constructors *
	 ************** */
	/**
	 * creates an empty grammar
	 * 
	 * fill it with constraints and call 
	 */
	public Grammar() {
		this.levels      = new ArrayList<>();
		this.hierarchies = new HashMap<>();
		this.maps        = new HashMap<>();
		this.constraints = new HashMap<>();
		this.constraintCounter = 0;
		
		this.deactivatedConstraints = new ArrayList<>();
		this.deactivatedSections    = new ArrayList<>();
		
		this.allAttributes = new ArrayList<>();
		this.downFeatures  = null;
		this.upFeatures    = null;
		
		this.hashedBCs = null; 
		this.hashedUCs = null;
		this.hashedFlUCs = null;
		this.hashedFlUCsNonspec = null;
		
		this.maxLabelLength = 0;
		this.noHas = 0;
		this.noOfLevels = 0;
		this.noOfConnections = Connection.values().length;
		this.noOfDirections  = Direction.values().length;
		
		this.vnspecs    = new ArrayList<>();
		
		this.isContextSensitive = false;
		
		this.cachingValid = false;
	}
	
	
	/* *********
	 * Methods *
	 ********* */
	public void addLevel(Level lvl) {
		lvl.setNo(noOfLevels);
		levels.add(lvl);
		noOfLevels ++;
		cachingValid = false;
	}
	
	public void addHierarchy(Hierarchy h) {
		hierarchies.put(h.getId(), h);
	}
	
	public void addMap(String name, Map<String, AVM> m) {
		maps.put(name, m);
	}
	
	public boolean isContextSensitive() {
		return isContextSensitive;
	}


	public boolean isActiveSection(String section) {
		return !deactivatedSections.contains(section);
	}
	
	/**
	 * Checks whether the given constraitn is active, i.e. should be used in evaluation.
	 * Cosntraints can be deactivated individually or section wise.
	 */
	public boolean isActiveConstraint(Constraint c) {
		return !deactivatedConstraints.contains(c) && !deactivatedSections.contains(c.getSection());
	}
	
	/**
	 * set whether the given section of constraints should be used in evaluation
	 * re-caches all constraints
	 * 
	 * note, that the constraints of an active section might still be deactivated individually
	 */
	public void setSectionActive(String section, boolean active) {
		if(active)
			deactivatedSections.remove(section);
		else 
			deactivatedSections.add(section);
		
		cacheConstraints();
	}

	/**
	 * set whether the given constraint should be used in evaluation
	 * re-caches all constraints
	 * 
	 * note, that even if activated here, the constraint might still be deactivated via its section
	 * 
	 * @param c
	 * @param active
	 */
	public void setConstraintActive(Constraint c, boolean active) {
		if(active)
			deactivatedConstraints.remove(c);
		else
			deactivatedConstraints.add(c);
				
		cacheConstraints();
	}
	
	public int getNoOfLevels() {
		return noOfLevels;
	}

	/**
	 * calculate index for use with input->hashedBCs
	 */
	private int bcIndex(Level lvlA, Level lvlB, Direction dirA, Connection conn, Direction dirB)
	{
		return (dirB.no +
				conn.no      * noOfDirections +
				dirA.no      * noOfDirections * noOfConnections +
				lvlB.getNo() * noOfDirections * noOfConnections * noOfDirections +
				lvlA.getNo() * noOfDirections * noOfConnections * noOfDirections * noOfLevels);
	}
	
	/**
	 * calculate index for use with input->hashedUCs
	 */
	private int ucIndex(Level level, Direction dir)
	{
		return (dir.no + level.getNo() * noOfDirections);
	}

	public List<Level> getLevels() {
		return levels;
	}

	public List<Path> getAllAttributes() {
		return allAttributes;
	}	
	
	/* **********************************
	 *         Edgetype encoding        * 
	 ********************************** */ 
	/**
	 *  Compute index of edge type
	 */
	public int etEncode(Level lvl, String label, Direction dir) {
		int labelno = lvl.getLabels().indexOf(label);

		if (labelno == -1) {
			return -1;
		}
	
		return  dir.no +
				labelno * noOfDirections +
				lvl.getNo() * noOfDirections * maxNrLabels;
	}
	
	public Direction etDecodeDirection(int et) {
		return Direction.values()[et % Direction.values().length];
	}

	
	public String etDecodeLabel(int et) {
		Level level = etDecodeLevel(et);
		int labelno = (et / noOfDirections) % maxNrLabels;
		if(level.getLabels().size() > labelno)
			return level.getLabels().get(labelno);
		else 
			return null;
	}

	public Level etDecodeLevel(int et) {
	  return levels.get(et / noOfDirections / maxNrLabels);
	}
	
	public int etMax() {
		return  (noOfDirections-1) +
				(maxNrLabels-1) * noOfDirections +
				(noOfLevels-1) * noOfDirections * maxNrLabels;
	}
	/* **********************************************************/
	
	
	public Set<Path> getDownFeatures(int et) {
		return downFeatures.get(et);
	}
	
	public Set<Path> getUpFeatures(int et) {
		return upFeatures.get(et);
	}



	public Level findLevel(String levelName) {
		for (Level level : levels) {
			if(levelName.equals(level.getId())) {
				return level;
			}
		}
		return null;
	}
	
	public List<Constraint> getHashedBCs(Level level, Level level2, Direction dir1,
			Connection connection, Direction dir2) {
		int index = bcIndex(level, level2, dir1, connection, dir2);
		return hashedBCs.get(index);
	}
	
	public List<Constraint> getHashedUCs(Level level, Direction dir) {
		int index = ucIndex(level, dir);
		return hashedUCs.get(index);
	}

	public List<Constraint> getHashedFlUCs(Level level, Direction dir) {
		int index = ucIndex(level, dir);
		return hashedFlUCs.get(index);
	}
	
	public List<Constraint> getHashedFlUCsNonspec(Level level, Direction dir) {
		int index = ucIndex(level, dir);
		return hashedFlUCsNonspec.get(index);
	}

	public int getNoOfHas() {
		return noHas;
	}
	
	/**
	 * add a single constraint to the grammar
	 * 
	 * it will be added to the appropriate caches
	 * 
	 * @param c
	 */
	private void cacheConstraint(Constraint c) {
		c.setNo(constraintCounter++);
		
		if(c.isContextSensitive()) {
			isContextSensitive = true;
		}
		
		if(c.getAttributes().contains(Constraint.ATTR_TEMP_ALLOWED) &&
				c.getAttributes().contains(Constraint.ATTR_TEMP_PENALIZED)) {
			logger.warn(String.format("Constraint %s marked as both temporarily allowed and penalized", c.getId()));
		}

		if (c.getSignature().arity == 1) {
			int levelno1 = c.getSignature().l1.getNo();
			
			for (Direction d : Direction.values()) {
				
				if (!c.getSignature().d1.subsumes(d))
					continue;
				
				for(int i = 0; i < noOfLevels; i++) {
					/* if level specified, skip all others */
					if(levelno1 != -1 && levelno1 != i) {
						continue;
					}
					int index = ucIndex(c.getSignature().l1, d);
					if (c.getPenalty() == 0)
						// hard constraints go first
						hashedUCs.get(index).add(0, c);
					else
						hashedUCs.get(index).add(c);

					if(c.getAttributes().contains(Constraint.ATTR_FLUCTUATION)) {
						hashedFlUCs.get(index).add(c);
						//cdgPrintf(CDG_INFO, "constraint '%s' marked as fluctuating\n", c.id);
					}

					if(c.getAttributes().contains(Constraint.ATTR_NONSPEC_FLUCTUATION)) {
						hashedFlUCsNonspec.get(index).add(c);
						//cdgPrintf(CDG_INFO, "constraint '%s' marked as fluctuating for nonspec edges\n", c.id);
					}
				}
			}
			
		} else if(c.getSignature().arity == 2) {
			/* binary constraints are trickier because they may fall into
			   many categories simultaneously: a {X/SYN/\Y/SYN} constraint
			   goes into only one cell, but a {X:SYN,Y:SYN} goes into 112
			   cells, and a {X,Y} constraint goes into 112n� cells. */
			
			int levelno1 = c.getSignature().l1.getNo();
			int levelno2 = c.getSignature().l2.getNo();
			for (int i = 0; i < noOfLevels; i++) {
				if(levelno1 != -1 && levelno1 != i) continue;

				for (int j = 0; j < noOfLevels; j++) {
					if(levelno2 != -1 && levelno2 != j) continue;

					int index = i * noOfLevels + j;

//					/* count the constraint */
//					vectorSetElement(levelMatrixCounter,
//							((int)vectorElement(levelMatrixCounter, index) + 1),
//							index);

					/* classify the constraint */
					for (Direction d1 : Direction.values()) {
						if (!c.getSignature().d1.subsumes(d1))
							continue;
						for (Connection conn : Connection.values()) {
							if (!c.getSignature().conn.subsumes(conn))
								continue;
							for (Direction d2 : Direction.values()) {
								if (!c.getSignature().d2.subsumes(d2))
									continue;

								/* place hard constraints at start of list for early failing */
								index = bcIndex(c.getSignature().l1, c.getSignature().l2, 
										d1, conn, d2);
								if (c.getPenalty() == 0)
									// hard constraints go first
									hashedBCs.get(index).add(0, c);
								else
									hashedBCs.get(index).add(c);
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 *  Calculate levelMatrixCounter, hashedUCs, hashedBCs etc.
	 */
	private void cacheConstraints() {
		isContextSensitive = false;

		/* check function calls for correct arity */
		checkArities();

		/* calculate hashedUCs */
		hashedUCs   = new HashMap<>();
		hashedFlUCs = new HashMap<>();
		hashedFlUCsNonspec = new HashMap<>();
		for (int i = 0; i < noOfLevels * noOfDirections; i++) {
			hashedUCs.put(i, new ArrayList<>());
			hashedFlUCs.put(i, new ArrayList<>());
			hashedFlUCsNonspec.put(i, new ArrayList<>());
		}

		int index = noOfLevels * noOfLevels * noOfLevels;
		//
		//	  levelMatrixCounter = NULL;
		//	  levelMatrixCounter = vectorNew(index);
		//	  vectorSetAllElements(levelMatrixCounter, (void *)0);
		/* calculate hashedBCs */
		hashedBCs = new HashMap<>();
		for (int i = 0; i < index * noOfDirections * noOfConnections * noOfDirections; i++) {
			hashedBCs.put(i, new ArrayList<>() );
		}

		/* count and classify constraints */
		for(Constraint c : constraints.values()) {
			if(isActiveConstraint(c)) {
				cacheConstraint(c);
			}
		}
		
		/* Scan constraint formulas for common expression optimization */
		optimizeGrammar();
	}
	
	
	/**
	 *  Looks for common subformulas in a grammar and assigns them the same
	 *  index. At the moment this only detects identical applications of has().
	 */
	public void optimizeGrammar() {
		List<Formula> hasFormulas = new ArrayList<>();
		int counter = 0;
		
		/* find all has() applications */
		for(Constraint c: constraints.values()) {
			c.getFormula().collectHasInvocations(hasFormulas);
		}
		
		/* co-index equal formulas */
		for(int i=0; i < hasFormulas.size(); i++) {
			Formula f = hasFormulas.get(i);
			boolean found = false;
			for(int j = 0; j < i; j++) {
				Formula f2 = hasFormulas.get(j);
				if(f.areSameHasInvocation(f2)) {
					f.setIndex(f2.getIndex());
					found= true;
					break;
				}
			}
			if(!found) {
				f.setIndex(counter++);
			}
		}
		
		noHas = counter;
	}
	
	
	/**
	   Checks the number of arguments of function and predicate invocations.

	   If a constraint contains a detectably wrong call, it is removed from
	   the grammar.
	*/
	private void checkArities() {
		List<Constraint> badConstraints = new ArrayList<>();
		
		/* scan all constraint formulas */
		for(Constraint c : constraints.values()) {
			if(!c.getFormula().check()) {
				badConstraints.add(c);
			}
		}

		for(Constraint c : badConstraints) {
			logger.warn(String.format("WARNING: bad arities in constraint %s, it will be ignored..", c.getId()));
			constraints.remove(c.getId());
		}
	}

	/**
	 * To be called after all levels, constraint and hierarchies have been added.
	 * caches constraints and collects features
	 * 
	 */
	public void cache(Configuration config) {
		
		for(String cat: config.getList(DEACTIVATED_CONSTRAINT_CATEGORIES)) {
			setSectionActive(cat, false);
		}
		
		// Label information has to be collected first since cacheConstraints
		// depends on the computed values
		collectLabelInfo();
		cacheConstraints();
		
		collectAllAttributes(config);
		collectRelevantFeatures();
		
		cachingValid = true;
	}

	/**
	 * collect all path to a feature used in any constraint
	 */
	private void collectAllAttributes(Configuration config) {
		allAttributes = new ArrayList<>();
		
		/* The user's favorite attributes,
		     as declared in anno-categories,
		     get the best seats. */
		for(String feat : config.getList(Parse.ANNO_FEATS)) {
			Path path = new Path(feat, null);
			if(!path.isPseudoFeature() && !allAttributes.contains(path)) 
				allAttributes.add(path);
		}

		/* (b) find features in constraints */
		for(Constraint c: constraints.values()) {
			c.getFormula().collectFeatures(allAttributes);
		}		
	}

	/**
	 * updates maxNrLabels and maxLabelLength from the levels
	 */
	public void collectLabelInfo() {
		for (Level level : levels) {
			this.maxNrLabels = Math.max(this.maxNrLabels,level.getLabels().size());
			for (String label : level.getLabels()) {
				this.maxLabelLength = Math.max(this.maxLabelLength, label.length());
			}
		}
	}

	public void addConstraint(Constraint c) {
		constraints.put(c.getId(), c);
		cachingValid = false;
	}
	
	/**
	   Compute upFeatures and downFeatures.

	   This function scans all constraints in INPUT and determines what types
	   of LV access which lexical feature. To keep track of what edge types are
	   affected at any moment, it maintains data structures called Context
	   (really just Hashtables of BitStrings).

	   A Context stores a Bitstring under the name of each constraint variable.
	   Each bit indicates whether the corresponding edge type can occur as an
	   instantiation of that variable if evaluation reaches this point.

	   The function then follows the control flow in the constraint and
	   effectively qualifies each feature access with the edge types that
	   actually perform it. The result is that when building LVs for a
	   constraint net, more of them can be proved to be exactly equivalent and
	   combined into one disjunctive LV.

	 */
	private void collectRelevantFeatures() {

		int noOfEdgeTypes = etMax();
		
		/* allocate Vectors of BitStrings */
		upFeatures   = new HashMap<>();
		downFeatures = new HashMap<>();
		
		for(int i = 0; i < etMax(); i++) {
			upFeatures.put(  i, new HashSet<>());
			downFeatures.put(i, new HashSet<>());
		}

		/* analyze constraints to fill the maps */
		for(Constraint c: constraints.values()) {
			AccessContext context = new AccessContext(this);
			
			/* first of all, find out what variables this constraints talks about */
			for(int vi = 0; vi<c.getVars().size(); vi++) {
				ArrayList<Boolean> edgeTypesAllowed = new ArrayList<>();
				for(int i = 0; i < noOfEdgeTypes; i++) {
					edgeTypesAllowed.add(true);
				}
				
				context.addVariable( edgeTypesAllowed);

				/* exploit information in the signature */
				context.restrictLevel(vi, c.getVars().get(vi).level);
				context.restrictDir  (vi, c.getVars().get(vi).dir  );
			}

			/* now scan the entire formula */
			c.getFormula().analyze(context, this);
		}
	}

	public Hierarchy findHierarchy(String hname) {
		return hierarchies.get(hname);
	}

	public Map<String, AVM> findMap(String mname) {
		return maps.get(mname);
	}

	/**
	 * Returns the constraint of the given name.
	 * If no such constraint exists, null is returned.
	 * This method does not regard whether the constraint is active or not,
	 * Make sure to check the activation status before using the returned 
	 * constriant for evaluation. 
	 */
	public Constraint findConstraint(String name) {
		return constraints.get(name);
	}
	
	public List<VirtualNodeSpec> getVNSpecs() {
		return vnspecs;
	}
	
	public void addVNSpec(VirtualNodeSpec vns) {
		vnspecs.add(vns);
	}
	
	/*
	 * returns whether the given labels are the same or at least similar
	 * similarity is defined by the label hierarchy and the list of label groups
	 */
	public boolean isCompatibleLabel(String l1, String l2, Configuration config) {
		if(l1.equals(l2))
			return true;

		Hierarchy h = findHierarchy("Label");
		if(h== null) {
			logger.warn("WARNING: Type hierarchy 'Label' not found, can't check label compatibility!");
			return false;
		}

		if(!h.contains(l1)) {
			logger.warn(String.format("Label %s not found in hierarchy 'label', can't check label compatibility", l1));
			return false;
		}
		if(!h.contains(l2)) {
			logger.warn(String.format("Label %s not found in hierarchy 'label', can't check label compatibility", l2));
			return false;
		}

		for(String group : config.getList(LABEL_GROUPS)) {
			if(!h.contains(group)) {
				logger.warn(String.format("Label group %s not found in hierarchy 'label', skipping...", group));
					continue;
			}

			if(h.subsumes(group, l1) && h.subsumes(group, l2)) {
				return true;
			}
		}
		
		return false;
	}
	
	public boolean isCachingValid() {
		return cachingValid;
	}
	
	@Override
	public String toString() {
		return String.format("Grammar: %d levels, %d constraints, %d hierarchies", noOfLevels, constraintCounter, hierarchies.size());		
	}

	/**
	 * simply the first level is returned
	 * 
	 * TODO main level flag?
	 */
	public Level getMainLevel() {
		return levels.get(0);
	}
}
