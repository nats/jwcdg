package de.unihamburg.informatik.nats.jwcdg.constraints.functions;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
Returns user-defined data.

The first argument must be the name of a defined map. The second
argument is a key into this map, and the corresponding Value is
returned.
 */
public class FunctionLookup extends Function {

	public FunctionLookup(Grammar g) {
		super("lookup", g, 1, -1, false);
	}

	@Override
	public AVM eval(List<Term> args, Context context, LexemeGraph lg,
			LevelValue[] binding) {

		AVM result;
		Map<String, AVM> map;
		int counter;
		String sep = ":";

		AVM val1 = args.get(0).eval(lg, context, binding);
		if (val1 instanceof AVMString) {
			String m = ((AVMString)val1).getString();
			map = grammar.findMap(m);
			if(map == null)  {
				return new AVMError(String.format("WARNING: map `%s' not found", m));
			}
		} else {
			return new AVMError("WARNING: type mismatch (1st arg) for `lookup'");
		}

		/* construct key */
		String key = "";
		counter = 1;
		for (Term t : args.subList(1, args.size())) {
			AVM v = t.eval(lg, context, binding);
			counter++;
			String s;
			if (v instanceof AVMString) {
				s= ((AVMString)v).getString();
			} else if(v instanceof AVMNumber) {
				s = String.format("%.0f", ((AVMNumber)v).getNumber());
			} else {
				return new AVMError("WARNING: type mismatch (arg "+counter+") for `lookup'");
			}

			if(key.length() == 0) {
				key = s;
			} else {
				key += sep + s;
			}
		}

		result = map.get(key);
		if (result != null) {
			return result;
		} 

		/* Try to be smart about whether to return
			 0 or "" upon lookup failure */
		Iterator<AVM> it = map.values().iterator();
		if (it.hasNext() && (it.next() instanceof AVMNumber)) {
			// numeric 
			return new AVMNumber(0);
		}
		
		return new AVMString("");
	}
}
