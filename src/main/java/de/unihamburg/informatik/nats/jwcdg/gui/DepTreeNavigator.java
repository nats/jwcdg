package de.unihamburg.informatik.nats.jwcdg.gui;

import javax.swing.tree.TreePath;

/**
 * The subcomponent of the DepTreeDisplayInteractor that handles the
 * interactions with the navigation tree on the left side of the Display.
 * 
 * @author Sven Zimmer and Arne Köhn
 * 
 */
public class DepTreeNavigator extends AbstractNavigator {
	// new nodes must be inserted into the model to ensure their visibility

	public class DepTreeNavNode extends NavNode {
		private static final long serialVersionUID = 1L;
		public NavNodeContent _content = new NavNodeContent();

		public DepTreeNavNode(NavNodeContent content, String name) {
			super(name, false);
			_content = content;
		}

		public DepTreeNavNode(NavNodeContent content) {
			this(content, getSentence(content._parse));
		}

		public DepTreeNavNode(String directoryName) {
			super(directoryName, true);
		}
	}

	public void add(NavNodeContent content, String directory) {
		DepTreeNavNode node = new DepTreeNavNode(content);
		addNode(node, directory);
	}

	public void add(NavNodeContent content, String name, String directory) {
		DepTreeNavNode node = new DepTreeNavNode(content, name);
		addNode(node, directory);
	}

	private void addNode(DepTreeNavNode node, String directory) {
		makeDir(directory);
		NavNode dirNode = getDir(directory);
		insertNode(node, dirNode);
		_tree.setSelectionRow(_tree.getRowForPath(new TreePath(node.getPath())));
	}

	public DepTreeNavNode getCurrentNode() {
		return (DepTreeNavNode) _currentNode;
	}

}
