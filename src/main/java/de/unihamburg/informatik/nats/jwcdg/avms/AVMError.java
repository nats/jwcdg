package de.unihamburg.informatik.nats.jwcdg.avms;

import java.util.regex.Matcher;

import de.unihamburg.informatik.nats.jwcdg.input.Configuration;

public class AVMError extends AVM {
	private static final long serialVersionUID = 1009569196150768337L;
	
	private String message;
	
	public AVMError(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
	
	@Override
	public AVM followPathImpl(Path path) {
		return this;
	}
	
	@Override
	public String toString() {
		return String.format("%s", message);
	}

	@Override
	public AVM substituteRefs(Matcher matcher, Configuration config) {
		return this;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AVMError))
			return false;
		AVMError other = (AVMError) obj;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		return true;
	}
}
