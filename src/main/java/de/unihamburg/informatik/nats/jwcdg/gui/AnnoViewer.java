package de.unihamburg.informatik.nats.jwcdg.gui;

import de.unihamburg.informatik.nats.jwcdg.IncrementalParserFactory;
import de.unihamburg.informatik.nats.jwcdg.JWCDG;
import de.unihamburg.informatik.nats.jwcdg.incrementality.IncrementalFrobber;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;
import de.unihamburg.informatik.nats.jwcdg.parse.Word;
import io.gitlab.nats.deptreeviz.DepTree;
import org.antlr.runtime.RecognitionException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Displays the words in a parses in different directories as a trees in a
 * window, with interactions for changing the connections and labels. Trees for
 * Parses with the same filename in different directories are displayed
 * together. If you just want to have a SVG Document without opening a window,
 * then use the class DepTreeGenerator.
 *
 * @author Sven Zimmer and Christine Köhn and Arne Köhn
 *
 */
public class AnnoViewer extends AbstractViewer {

	public List<DepTree<DecoratedParse, Word>> _dts = new ArrayList<>();
	private AnnoDisplayInteractor _aDI;
	private Logger logger = LogManager.getLogger(AnnoViewer.class);

	public static void main(String[] args) throws IOException{

		Options options = initOptions();
		CommandLine line = initCommandLine(options, args);

		String[] dirArgs = line.getArgs();
		boolean sufficientArguments;

		if (args.length >= 1) {
			sufficientArguments = true;
		} else {
			System.err
			.println("You have to specify one or more directories as arguments");
			sufficientArguments = false;
		}

		if (line.hasOption("help") || ! sufficientArguments) {
			printHelpAndExit(!sufficientArguments, options);
		}

		ArrayList<String> dirs = new ArrayList<>();

		int dirCount = 0;
		for (String dirArg : dirArgs) {
			dirCount++;
			File dir = new File(dirArg);
			if (dir.isDirectory()) {
				dirs.add(dir.getAbsolutePath());
			} else {
				System.err.println(java.lang.String.format(
						"Argument %d '%s' is not a valid directory", dirCount,
						dirArg));
				printHelpAndExit(true, options);
			}
		}

		Configuration config = readConfig(line, options);
		if (config == null) {
			System.exit(1);
		}

		new AnnoViewer(config, dirs);
	}

	/**
	 *
	 * @param config !=null
	 * @param dirNames
	 *            list of the directories with the ".cda" files containing the
	 *            parses
	 * @throws IOException
	 */
	public AnnoViewer(Configuration config, List<String> dirNames) throws IOException{
		IncrementalFrobber parser = JWCDG.makeParser(config, new IncrementalParserFactory());
		initGrammarString(config);
		int dirCount = 0;
		for (String dir : dirNames) {
			DepTree<DecoratedParse, Word> dt = new DepTree<>();
			dt.setWindowSize(new Dimension(1800, 1150));
			dt.setShowingCat(true);
			_dts.add(dt);
			if (!dir.endsWith("/")) {
				dirNames.set(dirCount, dir + "/");
			}
			dirCount++;
		}

		_aDI = new AnnoDisplayInteractor(dirCount);
		_aDI.setFields(_dts, parser, dirNames);
		_aDI._GUI = new AnnoDisplay(_dts, config);
		createDisplay(_aDI, _aDI._GUI);

		// // each list contains the filenames of one directory as a list
		ArrayList<String> allFilenames = new ArrayList<>();
		// maps a filename to a list of parses. Each position in the list is for
		// one file
		HashMap<String, ArrayList<Parse>> allParses =
				new HashMap<>();
		int dirPos = 0;

		for (String dirName : dirNames) {
			File dir = new File(dirName);
			if (dir.exists() && dir.isDirectory()) {
				String[] fileNames = dir.list();
				Arrays.sort(fileNames);
				for (String fileName : fileNames) {
					File file = new File(dirName, fileName);
					if (file.exists() && ! file.isDirectory()
							&& file.getName().endsWith(".cda")) {
						if (! allFilenames.contains(fileName)) {
							allFilenames.add(fileName);
						}
						setParse(file, dirPos, allParses);
					}
				}
				File doneFile = new File(dirName + "done.txt");
				ArrayList<String> doneList = new ArrayList<>();
				_aDI._doneLists.add(doneList);
				FileService.fileToList(doneFile, doneList);
				dirPos++;
			}
			else {
				System.err.println("Directory not found: " + dir.getAbsolutePath());
			}
		}

		for (String fileName : allFilenames) {
			ArrayList<Parse> currentParses = allParses.get(fileName);
			_aDI.add(currentParses,
					fileName.substring(0, fileName.length() - 4),
					fileName);
			int parsePos = 0;
			for (Parse parse : currentParses) {
				if (parse == null) {
					System.err.println("missing file " + fileName
							+ " in directory " + dirNames.get(parsePos));
				}
				parsePos++;
			}
		}

		if (_aDI._GUI.getNavTree().getRowCount() >= 1) {
			_aDI._GUI.getNavTree().setSelectionRow(1);
		}
	}

	private static void printHelpAndExit(boolean error, Options options){
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("AnnoViewer [-c CONFIG_FILE] DIRECTORIES...", "",
				options,
				"For more information, please refer to the README file.");
		if (error) {
			System.exit(1);
		} else {
			System.exit(0);
		}
	}

	/**
	 * generate a parse from a filename and put it into the map allParses at the
	 * appropriate position
	 * @param file
	 * @param dirPos
	 * @param allParses
	 */
	private void setParse(File file,
			int dirPos,
			HashMap<String, ArrayList<Parse>> allParses) {
		// if allParses doesn't already contain a list of parses with one parse
		// for each directory for this filename, then create an empty one
		ArrayList<Parse> currentParses = allParses.get(file.getName());
		StringBuilder fileContent = new StringBuilder();
		if (currentParses == null) {
			currentParses = new ArrayList<>();
			for (int i = 0; i < _aDI.getDirCount(); i++) {
				currentParses.add(null);
			}
			allParses.put(file.getName(), currentParses);
		}
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(file));
			String line;
			while ((line = reader.readLine()) != null) {
				fileContent.append(line);
				fileContent.append(System.getProperty("line.separator"));
			}
			reader.close();
			currentParses.set(dirPos,
					generateParseObject(fileContent.toString()));
		} catch (RecognitionException | IOException e) {
			e.printStackTrace();
		}
	}

	public Frame getFrame() {
		return _aDI._GUI;
	}

	public AnnoDisplay getGUI() {
		return _aDI._GUI;
	}
}
