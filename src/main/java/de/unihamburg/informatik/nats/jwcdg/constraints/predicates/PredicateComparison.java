package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

public abstract class PredicateComparison extends Predicate {

	public PredicateComparison(String name) {
		//comparisons don't need a grammar; set it to null.
		super(name, null, false, 2, true);
	}
	
	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {
		AVM val1 = args.get(0).eval(lg, context, binding);
		
		AVM val2 = args.get(1).eval(lg, context, binding);
		
		return compare(val1, val2);
	}
	
	protected abstract boolean compare(AVM val1, AVM val2);
}
