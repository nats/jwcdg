package de.unihamburg.informatik.nats.jwcdg.predictors;

import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.predictors.malt.IncrementalMaltParser;

public class TurboWrapper {
	public static final String LIB_TURBO_PATH = "libturbo_path";
	public static final String LIB_TURBO_PMODEL = "libturbo_parser_model";
	public static final String LIB_TURBO_TMODEL = "libturbo_tagger_model";
	public static final String LIB_TURBO_LANGUAGE = "libturbo_language";
	
	private long parserhandle;
	private long taggerhandle;
	private String lang;
	
	/**
	 * Initializes a wrapper according to the configuration
	 * Note: libturbo.so needs to be loaded already!
	 * @param conf
	 */
	public TurboWrapper(Configuration conf) {
		// System.load(LIB_TURBO_PATH);
		parserhandle = initialize(conf.getString(LIB_TURBO_PMODEL));
		taggerhandle = initializeTagger(conf.getString(LIB_TURBO_TMODEL));
		lang = conf.getString(LIB_TURBO_LANGUAGE);
	}

	public String tag(String sentence) {
		return tag(taggerhandle, sentence);
	}
	public String tagAndParse(String sentence) {
		return parse(tag(sentence));
	}	
	public String parse(String sentence) {
		return parse(parserhandle, sentence);
	}

	public String parseIncrementally(String sentence) {
		int numVN = 2;
		if (lang.equals("de")) {
			sentence += "[virtual]\n[virtual]\n[virtual]\n[unused]\n";
			numVN = 3;
			System.err.println("LANG EQUALS DE!");
		} else {
		 	sentence += "[virtual]\n[virtual]\n[unused]\n";
		}
		System.out.println(sentence);
		sentence = tag(sentence);
		String[] lines = sentence.split("\r?\n");

		// set CPOStag
		StringBuilder tagged = new StringBuilder();
		for (int i = 0; i < lines.length - (numVN+1); i++) {
			String[] line = lines[i].split("\t");
			line[3] = IncrementalMaltParser.getCoarseTag(line[4]);
			for (int col = 0; col < 7; col++) {
				tagged.append(line[col]).append("\t");
			}
			tagged.append(line[7]);
			tagged.append("\n");
		}

		// add Virtual Nodes
		int numlines = lines.length - (numVN+1);
		tagged.append(Integer.toString(numlines  ));
		tagged.append("\t[virtual]\t_\tNVIRT\tNVIRT\tvirtual\t0\tROOT\t_\t_\n");
		tagged.append(Integer.toString(numlines+1));
		if (numVN > 2) {
			tagged.append("\t[virtual]\t_\tNVIRT\tNVIRT\tvirtual\t0\tROOT\t_\t_\n");
			tagged.append(Integer.toString(numlines+2));
		}
		tagged.append("\t[virtual]\t_\tVVIRT\tVVIRT\tvirtual\t0\tROOT\t_\t_\n");
		tagged.append(Integer.toString(numlines+numVN));
		tagged.append("\t[unused]\t_\tZZZNOWORD\tZZZNOWORD\t_\t0\tUNUSED\t_\t_\n");
		System.out.println("finalsentence");
		System.out.println(tagged);
		String parsed =  parse(tagged.toString());

		//
		/*
		StringBuilder result = new StringBuilder();
		String[] parsedLines = parsed.split("\r?\n");
		for (int i = 0; i < parsedLines.length - 4; i++) {
			result.append(parsedLines[i]).append("\n");
		}
		for (int i = parsedLines.length - 4; i < parsedLines.length - 1; i++) {
			String[] cols = parsedLines[i].split("\t");
			if (Integer.parseInt(cols[6]) ==  parsedLines.length - 1) {
				for (int j = 0; j < 6; j++)
					result.append(cols[j]).append("\t");
				result.append("0\t_");
			} else {
				result.append(parsedLines[i]).append("\n");
			}
		}
		*/
		String result = parsed;
		System.out.println(result);
		return result;
	}	

	
	public native long initialize(String modelFilePath);
	public native long initializeTagger(String modelFilePath);
	private native String parse(long pipe, String sentence);
	private native String tag(long pipe, String sentence);
}
