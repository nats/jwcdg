package de.unihamburg.informatik.nats.jwcdg.transform;

import static de.unihamburg.informatik.nats.jwcdg.frobbing.FrobbingFlag.OD_DELETED;
import static de.unihamburg.informatik.nats.jwcdg.frobbing.FrobbingFlag.OD_LABEL;
import static de.unihamburg.informatik.nats.jwcdg.frobbing.FrobbingFlag.OD_LEXICAL;
import static de.unihamburg.informatik.nats.jwcdg.frobbing.FrobbingFlag.OD_ORIENT;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNetState;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluationResult;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.frobbing.FrobbingFlag;

/** Collected info about a possible repair step.

The canonical method to transform an Analysis is to construct a Patch
for it, then apply that Patch. A Patch is essentially a list of LVs to
be inserted into the Analysis. These LVs are held in the Vector
selection.

The List newLexemes holds a set of lexemes needed to make all these LVs
valid. The Vector forbiddenLexemes marks all those lexemes that
contradict one of the LVs chosen so far. No incompatible LVs are allowed
in the same Patch: any LV that relies on a set of `forbidden'
lexemes cannot be added by pNext().

The field newLexemes is rather expensive to calculate, since
changing one lexeme in an Analysis may require more swaps on both
sides. Therefore it is not calculated as a Patch is constructed.
To actually decide which new set of lexemes is to be used for the
Patch, the function pLexicalize() must be called.

The two Badnesses local and predicted give estimates about how
good (by the generalized measure) the partial Analysis represented
by the Patch and the resulting complete Analysis will be. The
Number limit gives the lowest boundary that can be calculated for
the score of an Analysis that results from this Patch. Again,
different repair strategies can use this information in different
ways.

The field noOfNewLVs counts the number of LVs that are not
already present in the current Analysis (in principle, it is
legal to substitute an LV for itself, though care must be taken to
ensure termination).

The Vector bound holds the indices of all domains bound by the
LVs in the Patch. Note that since an LV may bind several
domains, this Vector may contain more elements than the selection.

The field local holds the Badness of the partial
Analysis constituted by the LVs.

The field predicted holds an estimate of the Badness of
the Analysis that will result from applying the Patch.

The field limit holds an upper bound to the classical score of
the Analysis that will result from applying the Patch.

Patches are allocated by the function pNew(). The canonical method
to construct a Patch that binds the LVs A and B is to call the
functions pNext(B, pNext(A, pNew())) (modulo some more
parameters). A Patch p is then applied to the Analysis a by
calling the functions pLexicalize(a, p) and apply(a, p).

Neither LevelValue nor Analysis structures are owned by a
Patch, only its own Vectors and Lists.

*/
public class Patch implements Comparable<Patch> {
	private Analysis a;                     /* Analysis that the patch applies to */
	private List<LevelValue> selection;     /* LVs to swap in */

	private List<LexemeNode> newLexemes;    /* lexemes to be swapped in */
	private int noOfNewLVs;                 /* # of non-vacuous switches */
	private List<LexemeNode> forbiddenLexemes; /* mark incompatible lexemes */
	private List<Boolean> bound;            /* mark domains bound by a Patch */

	private Badness local;                  /* mutual score of LVs in a Patch */
	private Badness predicted;              /* Badness of resulting Analysis */
	private double limit;                   /* maximally attainable score */

	private List<ConstraintViolation> conflicts; /* holds all conflicts between LVs of the patch */
	
	private static Logger logger = LogManager.getLogger(Patch.class);
	
	/**
	 * copy constructor
	 */
	public Patch(Patch other) {
		this.a          = other.a;
		this.local      = other.local;
		this.limit      = other.limit;
		this.noOfNewLVs   = other.noOfNewLVs;
		this.predicted  = other.predicted;
		
		this.selection  = new ArrayList<>(other.selection);
		this.newLexemes = new ArrayList<>(other.newLexemes);
		this.conflicts  = new ArrayList<>(other.conflicts);
		this.bound      = new ArrayList<>(other.bound);
		this.forbiddenLexemes = new ArrayList<>(other.forbiddenLexemes);
	}
	

	public Patch(Analysis a, int width, EnumSet<FrobbingFlag> flags, Set<LexemeNode> deleted) {
		this.a = a;
		this.local = Badness.bestBadness();
		this.limit = 1.0;
		this.selection = new ArrayList<>();
		this.noOfNewLVs = 0;
		this.predicted = null;
		this.newLexemes = new ArrayList<>();
		this.conflicts = new ArrayList<>();
		this.bound = new ArrayList<>(a.getCNet().getNoOfSlots());
		for(int i=0; i < a.getCNet().getNoOfSlots(); i++) {
			bound.add(false);
		}

		/* normally, disallow deleted lexeme nodes implicitly */
		this.forbiddenLexemes = new ArrayList<>();

		// if deleted lexemes are not allowed, forbid them all
		if (!flags.contains(FrobbingFlag.OD_DELETED)) {
			forbiddenLexemes.addAll(deleted);
		}

	}

	public List<LevelValue> getSelection() {
		return selection;
	}

	public List<LexemeNode> getNewLexemes() {
		return newLexemes;
	}

	public double getLimit() {
		return limit;
	}

	public int getNoOfNewLVs() {
		return noOfNewLVs; 
	}

	/**
	 Compute the set of lexemes that have to be substituted into A so
	 that P may be applied.

	 Basically, this function computes p.newLexemes from
	 p.forbiddenLexemes.

	 Rationale: adding an LV to a Patch usually decreases the number of
	 lexemes that can be used in the new path. But there is no guarantee that
	 all LVs requested together *uniquely* define a set of lexemes that has
	 to be swapped in. Thus the space of possible lexemes is narrowed down in
	 pNext(), and the positive list of lexemes to be used is built here.

	 Returns FALSE if no path can be built, TRUE otherwise.

	 Usually, this function does not select deleted lexeme nodes. Therefore,
	 if a Patch was built with OD_DELETED, the flag must also be given to
	 pLexicalize(). This is the only flag that pLexicalize() reads.

	 If P was not built with OD_DELETED, this function can only fail because
	 the lexeme graph has become altogether disconnected, e.g. there is no
	 path at all that spans the entire graph.

	 It is a critical error if the lexicalisation fails (this should be
	 changed to something more sensible). As it is, the caller is guaranteed
	 to get a valid path if one exists, or an assertion otherwise. No
	 provision is made to create a path that is as `nearly' valid as
	 possible.

	 */
	public boolean lexicalize(ConstraintNetState state, EnumSet<FrobbingFlag> flags) {
		LexemeNode orig, found;
		boolean exactfit;
		LexemeGraph lg = a.getCNet().getLexemeGraph();
		int tp;

		/* stores the hypothetically selected lexeme nodes per time point */
		List<LexemeNode> bound = new ArrayList<>(lg.getMax());
		for(int i=0; i< lg.getMax(); i++) {
			bound.add(null);
		}

		/* As of now, all time points are bound. */
		int coverage = a.getCNet().getNoOfTimepoints();

		/* if the lexeme graph has become disconnected,
		 pLexicalize() cannot possibly succeed. */
		if (state != null && state.getNoOfPaths() == 0) {
			return false;
		}

		for(int i= lg.getMin(); i < lg.getMax(); i++) {
			LexemeNode ln = a.selectedLnAt(i);
			for(int j= ln.getArc().from; j < ln.getArc().to; j++) {
				bound.set(j, ln);
			}
		}

		/* Look for points that must be unbound. */
		tp = lg.getMin();
		while(tp < lg.getMax()) {
			LexemeNode ln = bound.get(tp);

			if (forbiddenLexemes.contains(ln)) {
				coverage -= (ln.getArc().to - ln.getArc().from);
				for (int j = ln.getArc().from; j < ln.getArc().to; j++) {
					bound.set(tp, null);
				}
			}

			/* step to next (possibly) unbound time point */
			tp = ln.getArc().to;
		}

		while (coverage < a.getCNet().getNoOfTimepoints()) {
			/* Find replacements. */
			tp = lg.getMin();
			while (tp < lg.getMax()) {
				if (bound.get(tp) != null) {
					tp = bound.get(tp).getArc().to;
					continue;
				}
				/* Find a new lexeme binding point #i. */
				orig = a.selectedLnAt(tp);
				found = null;
				exactfit = false;
				for (LexemeNode ln : a.getCNet().getLexemesAt(tp)) {
					
					if (ln == null)
						continue;
					
					if (!flags.contains(FrobbingFlag.OD_DELETED) && 
							state.isDeletedLn(ln)) {
						continue;
					}
					if (forbiddenLexemes.contains(ln)) {
						continue;
					}
					/* Try very hard to make a good choice. We want to prefer nodes
					 that bind exactly the same time slot as before, because then
					 we don't have to make subsidiary swaps. Secondarily, we want
					 nodes with high limits because those lead to better solutions.
					 */
					if (exactfit && (orig.getArc().from != ln.getArc().from
							|| orig.getArc().from != ln.getArc().from)) {
						continue;
					}
					if (found != null && state != null && state.getLnLimit(found) > state.getLnLimit(ln)) {
						continue;
					}
					found = ln;
					exactfit = (orig.getArc().from == ln.getArc().from && 
							orig.getArc().from == ln.getArc().from);
				}

				if (found == null) {
					//logger.debug("PANIC: couldn't lexicalize Patch!");
					throw new RuntimeException("\"PANIC: couldn't lexicalize Patch!\"");
				}

				/* OK, we'll take it. */
				requireLexeme(found);

				/* clear all lexemes that overlap it... */
				int tp2 = found.getArc().from;
				while (tp2 < found.getArc().to) {
					if (bound.get(tp2)!= null) {
						LexemeNode ln= bound.get(tp2);
						coverage -= (ln.getArc().to - ln.getArc().from);
						for (int k = ln.getArc().from; k < ln.getArc().to; k++) {
							bound.set(k, null);
						}
						tp2 = ln.getArc().to;
					} else {
						tp2++;
					}
				}

				/* ...and insert it. */
				for (int j = found.getArc().from; j < found.getArc().to; j++) {
					bound.set(j, found);
					coverage++;
				}
				tp = found.getArc().to;
			}
		}

		/* translate array into lexeme List */
		tp = lg.getMin();
		while (tp < lg.getMax()) {
			LexemeNode ln = bound.get(tp);
			if (ln != a.selectedLnAt(tp)) {
				newLexemes.add(ln);
			}
			if (ln == null) {
				throw new RuntimeException(
						"PANIC: pLexicalize failed to bind" + tp + "!");
			}
			tp = ln.getArc().to;
		}

		return true;
	}

	/**
	 Predict accurately the Badness that applying P would yield.

	 To this end, the Patch is temporarily applied to its Analysis and the
	 resulting Badness is noted.

	 if forced is set, levelValues not available in the net might be used,
	 if not set, the score would be set to worst instead
	 */
	public void judge(GrammarEvaluator evaluator, boolean forced) {

		Analysis newA = new Analysis(a);

		boolean applyable;

		if(forced) {
			applyable = true;
			newA.apply(new Patch(this), evaluator);
		} else {
			applyable = newA.applyIfPossible(new Patch(this), evaluator);
		}

		if(applyable) {
			predicted = newA.getBadness();
		} else {
			// we have to expect the worst
			predicted = Badness.worstBadness();
		}
	}	

	public Badness getPredicted() {
		return predicted; 
	}
	/**
	 * 
	 * @return the number of selected lvs
	 */
	public int getIndex() {
		return selection.size();
	}

	public boolean getIsBound(int i) {
		return bound.get(i);
	}

	public void select(LevelValue lv) {
		selection.add(lv);
	}

	public Badness getLocal() {
		return local;
	}
	
	public void setLocal(Badness b) {
		local = b;
	}

	public void setLimit(double score) {
		limit = score;
	}

	/**
	 Compare Patches, first by limit, then by local Badness.
	 */
	public boolean compare(Patch other) {
		return compareTo(other) < 0 ;
	}
	
	/**
	 Compare Patches, first by limit, then by local Badness.
	 */
	@Override
	public int compareTo(Patch other) {
		if (this.limit > other.limit)
			return -1;
		if (this.limit < other.limit)
			return 1;
		return (this.local.compareTo(other.local));
	}

	public void incNrNewLVs() {
		noOfNewLVs ++;
	}
	
	/**
	 Build a Patch that binds one more LV than the original one.

	 Usually, Patches that cannot help in solving the problem are not built,
	 and NULL is returned instead. A Patch is considered unhelpful if it
	 contains a deleted LV, or has a lower limit than an existing solution,
	 or has score 0.

	 This is normally a Good Thing, but in some places we expressly want to
	 build a Patch even though we know it won't help us, so the pruning can
	 be suppressed by setting OD_DELETED.

	 In no case will pNext() ever construct a Patch with physically
	 incompatible LVs.
	 */
	public Patch next(LevelValue lv, ConstraintNetState state, 
			GrammarEvaluator evaluator, double globalMinimum,
			EnumSet<FrobbingFlag> flags) {
		LevelValue current;
		boolean prune = !flags.contains(OD_DELETED);
		boolean debug = logger.isDebugEnabled();
		Patch newP;
		List<ConstraintViolation> localConflicts = new ArrayList<>();

		/* reject deleted LVs */
		if (prune && state.isDeletedLv(lv)) {
			if (debug) {
				logger.debug(String.format("#%d is deleted.", lv.getIndexWRTNet()));
			}
			return null;
		}

		/* new LV must satisfy the search flags */
		current = a.slotFiller(lv.getIndex());
		if (!flags.contains(OD_ORIENT)) {
			if (current != null) {
				int newT = -1;
				int oldT = -1;
				if (current.getRegentGN().spec()) {
					oldT = current.getRegentGN().getArc().from;
				}
				if (lv.getRegentGN().spec()) {
					newT = lv.getRegentGN().getArc().from;
				}
				
				if (oldT != newT) {
					if (debug) {
						logger.debug(String.format("#%d would change the structure", lv.getIndexWRTNet()));
					}
					return null;
				}
			}
		}
		if (!flags.contains(OD_LABEL)) {
			if (current != null) {
				if (! current.getLabel().equals(lv.getLabel())) {
					logger.debug(String.format("#%d would change the label",
					                           lv.getIndexWRTNet()));
					return null;
				}
			}
		}
		if (!flags.contains(OD_LEXICAL)) {
			if (!a.isValidLv(lv, true)) {
				logger.debug(String.format("#%d would change the lexemes",
				                           lv.getIndexWRTNet()));
				return null;
			}
		}

		/* ...and the path criterion */
		if (!lv.isLexemeCompatible(forbiddenLexemes)) {
			logger.debug(String.format("#%d is lexically incompatible.",
			                           lv.getIndexWRTNet()));
			return null;
		}

		/* ...and the grammaticality constraints */
		Badness b = Badness.bestBadness();
		for (LevelValue oldLv : selection) {
			if (oldLv == null)
				continue;
			
			GrammarEvaluationResult ger= evaluator.evalBinary(lv, oldLv, a.getCNet().getLexemeGraph(), null, false, true);
			b= b.addBadness(ger.getBadness());
			localConflicts.addAll(ger.getCVs());
			if(prune && b.hard >0) {
				logger.debug("pruning Patch with score 0.0");
				return null;
			}
		}

		/* build new Patch */
		newP = new Patch(this);
		
		newP.conflicts.addAll(localConflicts);

		/* extend by one LV */
		newP.selection.add(lv);

		/* note down all domains bound by this LV */
		for (int i = lv.getDependentGN().getArc().from; 
		     i < lv.getDependentGN().getArc().to; i++) {
			int j = a.getCNet().getNoOfLevels() * i + lv.getLevel().getNo();
			newP.bound.set(j, true);
		}

		/* update Badness */
		newP.local = newP.local.addBadness(b);
		if (lv.getScore() != 1.0) {
			/* This is incorrect. A score of f < 1.0 for the new LV may have
			 been caused by several unary faults, so it might have to be
			 represented by (2/0/f) rather than (1/0/f). But we ignore this
			 for the time being. */
			newP.local = newP.local.add(lv.getScore());
		}

		/* update limits */
		if (newP.local.soft < newP.limit) {
			newP.limit = newP.local.soft;
		}
		if (state != null && state.getLvLimit(lv) < newP.limit) {
			newP.limit = state.getLvLimit(lv);
		}

		/* reject Patches with limit that is too low */
		if (prune) {
			if (Badness.significantlyGreater( globalMinimum, newP.limit)) {
				logger.debug(String.format("limit %4.3e too low", newP.limit));
				return null;
			}
		}

		/* update compatibility Vector */
		if(lv.getRegentGN().spec()) {
			newP.requireLexemes(lv.getRegents());
		}
		newP.requireLexemes(lv.getDependents());

		return newP;
	}
	
	/**
	 Takes a Vector of Boolean, and sets all cells that correspond to the numbers
	 of nodes incompatible with ln. This function can be used in combination with
	 lvVectorCompatible() to decide whether an LV is compatible with a set of
	 other LVs.
	 */
	private void requireLexeme(LexemeNode ln) {
		if (!ln.spec())
			return;

		for (LexemeNode ln2 : a.getCNet().getLexemeGraph().getNodes()) {
			if (ln2 != ln && ln.overlaps(ln2) && !forbiddenLexemes.contains(ln2)) {
				forbiddenLexemes.add(ln2);
			}
		}
	}
	
	/**
	 This function is similar to requireLexeme(), but takes a List of lexeme
	 nodes. It marks all those lexeme nodes that are incompatible with all
	 lexemnodes of @a which.
	 */
	private void requireLexemes(List<LexemeNode> which) {
		
		/* we only need the first ln here, 
		 * as an ln2 that is compatible with one of which is compatible with all of them.
		 * we will prevent the other lns in which from being forbidden by explicitely checking
		 */
		LexemeNode ln1 = which.get(0);
		
		for (LexemeNode ln2 : a.getCNet().getLexemeGraph().getNodes()) {
			/* TODO #POSSIBLE_POINT_OF_FAILURE is this compatibility? what about paths?*/
			if (!which.contains(ln2) && ln1.overlaps(ln2) && !forbiddenLexemes.contains(ln2) ) {
				forbiddenLexemes.add(ln2);
			}
		}
	}
	
	@Override
	public String toString() {
		if(predicted != null)
			return String.format("(%1.2f, %1.2f, %1.2f) %s", local.getScore(), predicted.getScore(), limit, selection.toString());
		else 
			return String.format("(%1.2f, -, %1.2f) %s", local.getScore(), limit, selection.toString());
	}
	
}
