package de.unihamburg.informatik.nats.jwcdg.incrementality.virtualNodes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.GraphemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.input.LexiconItem;
import de.unihamburg.informatik.nats.jwcdg.lattice.Arc;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;

/**
 * LexemeGraph with virtual nodes
 * 
 * @author Niels Beuck
 */
public class VirtualLexemeGraph extends LexemeGraph {
	
	private Map<VirtualNodeSpec, List<GraphemeNode>> virtualNodes;
	private int vStartId;
	
	public VirtualLexemeGraph(LexemeGraph lg) {
		super(lg);
		vStartId=max;
		virtualNodes = new HashMap<>();
	}
	
	@Override
	public void addGraphemeNode(GraphemeNode gn) {
		if(gn.isVirtual()) {
			// virtual nodes go to the end of the list, as before
			super.addGraphemeNode(gn);
		} else {
			// in this case add it before the virtual nodes
			int pos = vStartId;
			graphemeNodes.add(pos, gn);
			
			gn.setNo(pos);
			for(GraphemeNode gn2 : graphemeNodes){	
				if(gn2.isVirtual()) {
					gn2.setNo(gn2.getNo()+1);
					gn2.pushArc();
					max = Math.max(max, gn2.getArc().to);
				}
			}
			vStartId++;
		
//			lg.ve.blocked = bvNew(vectorSize(lg.graphemnodes));
//			bvSetAllElements(lg.ve.blocked, FALSE);
		}
	}

	@Override
	public GraphemeNode getLastGN() {
		GraphemeNode ret = null;
		for(GraphemeNode gn : getGNs()) {
			if(gn.isVirtual())
				return ret;
			ret= gn;
		}
		return ret;
	}
	
	/**
	 * add a new instance of the virtualNodeSpec to the lexemgraph
	 * returns the newly generated graphemenode
	 */
	public GraphemeNode addVirtualNode(VirtualNodeSpec spec, int n) {
		if( !virtualNodes.containsKey(spec) ) {
			virtualNodes.put(spec, new ArrayList<>());
		}
		
		max++;

		GraphemeNode gn = new GraphemeNode(spec, getGNs().size(), n);
		addGraphemeNode(gn);
		
		List<LexiconItem> lis= spec.makeVirtualLexiconItems(config.getList(Parse.ANNO_FEATS));
		for(LexiconItem li : lis) {
			addLexemeNode(gn, li);
		}
		
		computeDistances();
		virtualNodes.get(spec).add(gn);

		return gn;
	}

	public VirtualNodeSpec getSpecOf(LexemeNode ln) {
		GraphemeNode gn = ln.getGN();
		for(VirtualNodeSpec vns : virtualNodes.keySet()) {
			if(virtualNodes.get(vns).contains(gn))
				return vns;
		}
		
		return null;
	}

	public int getVStartId() {
		return vStartId;
	}
	
	/**
	 * Extends the given lexemgraph with the given arc
	 */
	public void extend(String word, boolean complete) {
		setComplete(complete);
		
		Arc arc = new Arc(vStartId, vStartId+1, vStartId, word, 1.0);
		
		// extending lattice
		lattice.addArc(arc);
		lattice.scan(complete);

		// extending the lg, stuff like the max field will be taken care of here, too
		lgNewIter(arc);
		
		computeDistances();
	}
	
	/**
	 * returns all GraphemeNodes that are virtual nodes for the given spec
	 */
	public List<GraphemeNode> vnsOccurences(VirtualNodeSpec vns) {
		return virtualNodes.get(vns);
	}

}
