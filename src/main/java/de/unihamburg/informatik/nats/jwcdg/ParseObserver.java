package de.unihamburg.informatik.nats.jwcdg;

import de.unihamburg.informatik.nats.jwcdg.parse.Parse;

/**
 * Interface for every object that might want to be notified of new parses.
 * Check this box to subscribe 
 * 
 * Yes, this is the observer design pattern
 * 
 * @author the CDG-Team
 *
 */
public interface ParseObserver {
	
	void register(Parse parse);
	
}
