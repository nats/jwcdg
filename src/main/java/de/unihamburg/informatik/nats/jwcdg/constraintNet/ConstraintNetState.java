package de.unihamburg.informatik.nats.jwcdg.constraintNet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.unihamburg.informatik.nats.jwcdg.constraints.Level;
import de.unihamburg.informatik.nats.jwcdg.incrementality.virtualNodes.VirtualLexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;
import de.unihamburg.informatik.nats.jwcdg.transform.Analysis;
import de.unihamburg.informatik.nats.jwcdg.transform.Badness;


/**
 * State of a constraint net
 * contains information gathered while solving a constraint optimization problem
 * especially deletion of values and limit information (how good can it still get?)
 * This information was distibuted among levelValues, the ConstraiNet and lexemgraphs 
 * in the C implementation
 * 
 * @author the CDG-Team
 *
 */
public class ConstraintNetState implements Comparator<LevelValue> {
	private static Logger logger = LogManager.getLogger(ConstraintNetState.class);
	
	private ConstraintNet net;
	private LexemeGraph   lg;
	
	private Set<LevelValue> deletedLVs;
	private Map<LevelValue, Double > lvLimits;
	private Set<LexemeNode> deletedLNs;
	private Map<LexemeNode, Double > lnLimits;
	
	private Map<GraphemeNode, Boolean> liveGNs;
	
	/** total number of paths through graph */
	private int noOfPaths;           
	
	/** matrix of distances */
	private List<List<Integer>> distance;  
	
	/** vector of numbers of paths */
	private List<Long> noOfPathsFromStart;
	
	/** vector of numbers of paths */
	private List<Long> noOfPathsToEnd;
	
	/** Domains are not a data type in their own. Each domain in a problem
	 is simply a List of all LVs that may bind a specific level/time
	 point pair. This means that a domain for time point 0 may contain
	 LVs with the modifier wer(0,1) and also werden(0,2). Conversely,
	 shallow copies of the same LV appear in several domains if its
	 modifier spans more than one time interval. */
	private List<List<LevelValue>> domains;
	
	/** of LVs not deleted */
	private int noOfUndeletedLVs;  
	/** number of not yet deleted LVs per constraintNode */
	private Map<ConstraintNode, Integer> noValidValues;         
	
	private Configuration config;
	
	/* ****************/
	/* constructors   */
	/* ****************/
	
//	/**
//	 * Build an initial state where nothing is deleted,
//	 * the limits for the lexemenodes are all 1.0
//	 * and the limits of the lvs are set to the given initial values
//	 * 
//	 * @param initialLimits
//	 * @param lns
//	 */
//	public ConstraintNetState(Map<LevelValue, Double> initialLimits, List<LexemeNode> lns) {
//		deletedLVs = new HashMap<LevelValue, Boolean>();
//		deletedLNs = new HashMap<LexemeNode, Boolean>();
//		lvLimits   = new HashMap<LevelValue, Double>();
//		lnLimits   = new HashMap<LexemeNode, Double>();
//		
//		for (LevelValue lv : initialLimits.keySet()) {
//			deletedLVs.put(lv, false);
//			lvLimits.put(lv, initialLimits.get(lv));
//		}
//		
//		for (LexemeNode ln : lns) {
//			deletedLNs.put(ln, false);
//			lnLimits.put(ln, 1.0);
//		}
//	}
	
	
	/**
	 * build a constraint net state for a given constraint net,
	 * i.e. initialize all dynamic information and 
	 * optimize these (like limits and deletion status)
	 */
	public ConstraintNetState(ConstraintNet net, Configuration config) {
		this.net = net;
		this.lg  = net.getLexemeGraph();
		this.config = config;
		
		deletedLVs = new HashSet<>();
		lvLimits   = new HashMap<>();
		for(LevelValue lv : net.getValues()) {
			lvLimits.put(lv, lv.getScore());
		}
		
		deletedLNs = new HashSet<>();
		lnLimits   = new HashMap<>();
		for(LexemeNode ln : lg.getNodes()) {
			lnLimits.put(ln, 1.0);
		}
		
		liveGNs = new HashMap<>();
		for(GraphemeNode gn : lg.getGNs()) {
			liveGNs.put(gn, true);
		}
		
		noOfUndeletedLVs = 0; // will be set in addLV
		noValidValues  = new HashMap<>();
		for(ConstraintNode cnode : net.getNodes()) {
			noValidValues.put(cnode, 0); // will be set in addLV
		}

		/* build domains */
		logger.info("Building domains...");
		domains = new ArrayList<>(net.getNoOfSlots());
		for (int i = 0; i < net.getNoOfSlots(); i++) {
			domains.add(new ArrayList<>());
		}
		for (ConstraintNode cnode : net.getNodes()) {
			if (!cnode.getLevel().isUsed()) {
				continue;
			}
			for (LevelValue lv : cnode.getValues()) {
				if (lv.getIndexWRTNet() < 0)
					continue;
				addLV(lv);
			}
		}
		assert checkAmbiguity();
		
		/* count the number of possible paths and values */
		lgBuildFinal();
		cnBuildFinal();
		assert checkAmbiguity();
		
		/* If a domain remains empty, no solution is possible. This is important
		 because transformation methods may assume that each domain has at
		 least one value. */
		for (int i = 0; i < net.getNoOfSlots(); i++) {
			int tp = i / net.getNoOfLevels();
			if (tp > lg.getMax() || tp < lg.getMin()) {
				continue;
			}
			if ( ! net.getGrammar().getLevels().get(i % net.getNoOfLevels()).isUsed() ) {
				continue;
			}
			
			// TODO should we apply pressure here?
			checkDomain(i, 0.0);
			
			if (domains.get(i).size() == 0) {
				Level level = net.getGrammar().getLevels().get(i % net.getNoOfLevels());
				LexemeNode ln = net.getLexemesAt(i / net.getNoOfLevels()).get(0);
				throw new RuntimeException(String.format("Empty domain `%s'/%s, problem has no solution.", 
						ln.getArc().word, level.getId()));
			}
		}
		logger.info(" done.\n");

		/* Since the calls to dCheck() in the previous step may have deleted
		 lexemes, we have to re-optimize the net so that lgMakePath will
		 not select dangling lexemes. */
		if (optimizeNet() < 0) {
			throw new RuntimeException("Invalid net!");
		}
	}
	
	/**
	 * copy constructor
	 */
	public ConstraintNetState(ConstraintNetState orig) {
		this.net    = orig.net;
		this.lg     = orig.lg;
		this.config = orig.config;
		
		this.deletedLNs = new HashSet<>(orig.deletedLNs);
		this.deletedLVs = new HashSet<>(orig.deletedLVs);
		this.lnLimits   = new HashMap<>(orig.lnLimits);
		this.lvLimits   = new HashMap<>(orig.lvLimits);
		this.liveGNs    = new HashMap<>(orig.liveGNs);
		
		this.distance   = new ArrayList<>(orig.distance.size());
		for(List<Integer> l : orig.distance) {
			this.distance.add(new ArrayList<>(l));
		}
		this.domains    = new ArrayList<>(orig.domains.size());
		for(List<LevelValue> l : orig.domains) {
			this.domains.add(new ArrayList<>(l));
		}
		
		this.noOfPaths          = orig.noOfPaths;
		this.noOfPathsFromStart = new ArrayList<>(orig.noOfPathsFromStart);
		this.noOfPathsToEnd     = new ArrayList<>(orig.noOfPathsToEnd);
		
		this.noValidValues      = new HashMap<>(orig.noValidValues);
		this.noOfUndeletedLVs   = orig.noOfUndeletedLVs;
	}
	
	/**
	   Final improvements: optimize, sort, unary pruning, score cache and edges

	   This function performs various clean-up operations on a net for which
	   all constraint nodes have been built:
	   -  It removes unnecessary structures from the net using
	     \b cnOptimizeNet().
	   -  If \b cnSortNodes is set, it sorts the constraint nodes
	     using \b cnSortNodes().
	   -  It applies \b cnUnaryPruning() to each constraint node.
	   -  If \b scUseCache is set, it initializes \b net->cache
	     to a new cache returned by \b scNew().
	   -  If \b cnEdgesFlag is not \b cnEdgesOff, it applies
	     \b cnBuildEdges() to build the edges of the constraint net.

	   The function returns \b FALSE iff any of the subsidiary functions
	   returned \b FALSE.
	 */
	private void cnBuildFinal() { //FIXME is this method needed?

		/* checks net for deletable values and lexemes */
		if (optimizeNet() < 0) {
			throw new RuntimeException("ERROR: Building of net state failed");
		}

		/* sorts nodes of constraintnet */
		// TODO enable sorting of nodes or make sure this is not needed
		// it is disabled by default in ccdg
//		if (cnSortNodesMethod != 0) {
//			cnSortNodes(net);
//		}
	}
	
	/**
	 * Put an LV into all appropriate domains, and count it.
	 *
	 * This function registers lv in all domains where it is appropriate. 
	 * It also updates noOfUndeletedLVs.
	 */
	private void addLV(LevelValue lv) {
		/* put this LV into whatever domains need it */
		for (int k = lv.getDependentGN().getArc().from; 
				k < lv.getDependentGN().getArc().to; k++) {
			int index = net.getNoOfLevels() * k + lv.getLevel().getNo();
			domains.get(index).add(lv); // unsorted insertion, but that will be done by checkdomain
		
			if (!deletedLVs.contains(lv)) {
				noOfUndeletedLVs += 1;
				ConstraintNode cnode = net.getCNode(lv.getLevel(), k);
				noValidValues.put(cnode, noValidValues.get(cnode)+1);
			}
		}
	}
	
	/* ***********/
	/* methods   */
	/* ***********/

	/**
	 @brief does the final computations for the lexemgraph

	 This function sets those fields of @a lg that can only be computed after all
	 lexeme nodes are present:
	 - It initializes LexemGraph::isDeletedNode to all FALSE.
	 - It uses lgComputeDistances() to compute the distance
	 between any to nodes in the graph.
	 - It applies lgComputeNoOfPaths() to compute the number of paths to and from
	 all lexeme nodes in the graph. If no complete path exists, @a lg is
	 deallocated with a warning.

	 The function can fail returning FALSE if there is no valid path through the
	 lexeme graph.

	 does NOT call the predictors

	 */
	private boolean lgBuildFinal() { // TODO #NAMING this name?

		/* compute noOfPathsFromStart and noOfPathsToEnd */
		computeNoOfPaths(true);

		/* no path? */
		if (noOfPaths == 0) {
			logger.warn("WARNING: no path possible, invalid graph\n");
			return false;
		}
		
		deletedLNs.removeAll(lg.getNodes());
		
		computeDistances();

		return true;
	}
	
	/**
	 @brief (re-)computes the distance matrix LexemGraph::distance

	 This function computes the distance between any two lexeme nodes in @a lg and
	 stores the result in @a lg.distance.
	 */
	private void computeDistances() {
		GraphemeNode gn, gnNext;
		int noOfNodes = lg.getGNs().size();
		int i, j, k, ji, ik, jk, ij;

		/* arrayNew initializes the cells with NULL */
		if (distance == null || distance.size() < noOfNodes) {
			distance = new ArrayList<>(noOfNodes);
			for(i = 0; i< noOfNodes; i++) {
				ArrayList<Integer> l = new ArrayList<>(noOfNodes);
				for(j=0; j<noOfNodes; j++) {
					l.add(0);
				}
				distance.add(l);
			}
		} else {
			for(i = 0; i< noOfNodes; i++) {
				for(j=0; j<noOfNodes; j++) {
					distance.get(i).set(j, 0);
				}
			}
		}

		for (i = 0; i < noOfNodes; i++) {
			gn = lg.getGNs().get(i);

			/* If this gn has been deleted from the problem, leave its distances all
			 * at 0. This is necessary because the distance info does double duty as
			 * compatibility info. */
			if (areDeletedNodes(gn.getLexemes())) {
				continue;
			}
			for (j = 0; j < noOfNodes; j++) {
				gnNext = lg.getGNs().get(j);
				if (areDeletedNodes(gnNext.getLexemes())) {
					continue;
				}
				if (gn.getArc().to == gnNext.getArc().from) {
					distance.get(i).set(j, 1);
				}
			}
		}
		/* Warshall's algorithm for transitive closure */
		for (i = 0; i < noOfNodes; i++) {
			for (j = 0; j < noOfNodes; j++) {
				ji = distance.get(j).get(i);

				if (ji > 0) {
					for (k = 0; k < noOfNodes; k++) {
						ik = distance.get(i).get(k);
						jk = distance.get(j).get(k);

						if (ik > 0 && (jk == 0 || ji + ik < jk)) {
							distance.get(j).set(k, ji + ik);
						}
					}
				}
			}
		}

		for (i = 0; i < noOfNodes; i++) {
			for (j = 0; j < noOfNodes; j++) {
				ij = ik = distance.get(i).get(j);
				ji = ik = distance.get(j).get(i);

				if (ij == 0 && ji > 0) {
					distance.get(i).set(j, -ji);
				}
			}
		}
	}
	
	/**
	 @brief computes # of paths possible in the graph, given the current state of
	 deletions.

	 This function computes the number of paths possible in @a lg, according to
	 the state of its Vector LexemGraph::isDeletedNode. It calls
	 computeNoOfPathsToEnd() and computeNoOfPathsFromStart() for each lexeme node.
	 The total number of all paths is the sum of all numbers of paths leading to
	 grapheme nodes that are end nodes.

	 set realloc, if the number of nodes might have changed so several arrays have to be reallocated
	 */
	private void computeNoOfPaths(boolean realloc) {
		int noOfNodes = lg.getGNs().size();
		/* maybe (re)allocate the arrays */
		if(realloc || noOfPathsFromStart == null) {
			noOfPathsFromStart = new ArrayList<>(noOfNodes);
		}
		if(realloc || noOfPathsToEnd == null) {
			noOfPathsToEnd= new ArrayList<>(noOfNodes);
		}

		/* recompute ambiguities of each word */
		for (GraphemeNode gn : lg.getGNs()) {
			liveGNs.put(gn, false);
			for (LexemeNode ln : gn.getLexemes()) {
				if (!isDeletedLn(ln)) {
					liveGNs.put(gn, true);
					break;
				}
			}

			/* no paths from start/to end yet */
			noOfPathsFromStart.add(-1L);
			noOfPathsToEnd.add(-1L);
		}

		/* compute # of paths for all nodes */
		noOfPaths = 0;
		for (GraphemeNode gn: lg.getGNs()) {
			if (noOfPathsFromStart.get(gn.getNo()) < 0) {
				computeNoOfPathsFromStart(gn, 0);
			}
			if (noOfPathsToEnd.get(gn.getNo()) < 0) {
				computeNoOfPathsToEnd(gn, 0);
			}

			/* count # of paths in lg */
			if (isLiveGn(gn) && gn.getArc().from == lg.getMin())
				noOfPaths += noOfPathsToEnd.get(gn.getNo());
		}
	}
	
	/**
	 * whether this gn's arc is still part of a possible path through the lattice
	 * respective whether one of this gn's lexemenodes is part of a possible 
	 * path thfrough the lexemegraph.
	 * 
	 * Unless a non-trivial lattice was given (like from an ASR),
	 * this should always return true
	 * as there are no alternatives to a given gn 
	 * 
	 * @param gn
	 * @return
	 */
	private boolean isLiveGn(GraphemeNode gn) {
		return liveGNs.get(gn);
	}
	
	/**
	 @brief computes LexemGraph::noOfPathsFromStart

	 This function computes the number of paths leading to @a gn from the start of
	 @a lg. If @a gn corresponds to a start node, this is simply the number of
	 lexeme nodes sprung from @a gn. Otherwise it is that number multiplied by the
	 sum of the numbers of paths leading from the start to immediately preceding
	 grapheme nodes. If @a gn is deleted, the number is always zero.
	 */
	private long computeNoOfPathsFromStart(GraphemeNode gn, int sofar) {

		long sum;
		GraphemeNode prevNode;
		long pathsFromStart;
		int maximal = lg.getGNs().size();

		pathsFromStart = noOfPathsFromStart.get(gn.getNo());

		if (pathsFromStart >= 0) { // -1 indicates that we have to recompute
			return pathsFromStart;
		}

		if (!isLiveGn(gn)) {
			noOfPathsFromStart.set(gn.getNo(), 0L);
			return 0;
		}

		if (sofar > maximal) {
			logger.error("ERROR: lexem lattice contains cycles\n");
			throw new RuntimeException("ERROR: lexem lattice contains cycles\n");
		}

		if (gn.getArc().from == lg.getMin()) {
			noOfPathsFromStart.set(gn.getNo(), 1L);
			return 1L;
		}
		sum = 0;
		for (int i = 0; i < maximal; i++) {
			prevNode = lg.getGNs().get(i);

			if (!isLiveGn(prevNode) || prevNode.getArc().to != gn.getArc().from)
				continue;

			sum += computeNoOfPathsFromStart(prevNode, sofar + 1);

			if (sum < 0) {
				logger.error("PANIC: integer overflow in computeNoOfPathsFromStart()!\n");
				throw new RuntimeException("PANIC: integer overflow in computeNoOfPathsFromStart()!\n");
			}
		}
		noOfPathsFromStart.set(gn.getNo(), sum);
		return sum;
	}

	
	/**
	 @brief computes LexemGraph::noOfPathsToEnd

	 This function computes the number of paths leading from @a gn to the end of
	 @a lg. If @a gn corresponds to an end node, this is simply the number of
	 lexeme nodes sprung from @a g. Otherwise it is that number multiplied by the
	 sum of the numbers of paths leading to the end from immediately following
	 grapheme nodes. If @a gn is deleted, the number is always zero.
	 */
	private long computeNoOfPathsToEnd(GraphemeNode gn, long sofar) {
		long sum;
		GraphemeNode nextNode;
		long pathsToEnd;
		int maximal = lg.getGNs().size();

		pathsToEnd = noOfPathsToEnd.get(gn.getNo());

		if (pathsToEnd >= 0) // -1 indicates that we have to recompute
			return pathsToEnd;

		if(!isLiveGn(gn)) {
			noOfPathsToEnd.set(gn.getNo(), 0L);
			return 0L;
		}

		if (sofar > maximal) {
			logger.error("ERROR: lexem lattice contains cycles\n");
			throw new RuntimeException("ERROR: lexem lattice contains cycles\n");
		}

		if (gn.getArc().to == lg.getMax()) {
			noOfPathsToEnd.set(gn.getNo(), 1L);
			return 1;
		}

		sum = 0;
		for (int i = 0; i < maximal; i++) {
			nextNode = lg.getGNs().get(i);

			if (!isLiveGn(nextNode) || nextNode.getArc().from != gn.getArc().to)
				continue;

			sum += computeNoOfPathsToEnd(nextNode, sofar + 1);
			if (sum < 0) {
				logger.error("PANIC: integer overflow in computeNoOfPathsToEnd()!\n");
				throw new RuntimeException("PANIC: integer overflow in computeNoOfPathsToEnd()!\n");
			}
		}
		
		noOfPathsToEnd.set(gn.getNo(), sum);
		return sum;
	}


	/**
	 * This checks if the @a lexemes have been deleted TRUE is returned, if not
	 * FALSE
	 */
	private boolean areDeletedNodes(List<LexemeNode> lexemes) {
		for (LexemeNode ln : lexemes) {
			if (!isDeletedLn(ln))
				return false;
		}
		return true;
	}
	
	public boolean isDeletedLv(LevelValue lv) {
		return deletedLVs.contains(lv);
	}
	
	public boolean isDeletedLn(LexemeNode ln) {
		return deletedLNs.contains(ln);
	}
	
	/**
	 * marks the given lv as deleted 
	 * and updates noOfUndeletedLvs and noValid values
	 */
	public void deleteLv(LevelValue lv) {
		if(!isDeletedLv(lv)) {
			deletedLVs.add(lv);
			for(int i= lv.getDependentGN().getArc().from; i < lv.getDependentGN().getArc().to; i++) {
				ConstraintNode cnode = net.getCNode(lv.getIndex());
				noValidValues.put(cnode, noValidValues.get(cnode) -1);
				noOfUndeletedLVs -= 1;
			}
		}
	}

	/**
	 @brief deletes a node from the lexeme graph itself.

	 This function marks a lexeme node as deleted. It does this by setting the
	 cell @a ln.no in the Vector @a lg.isDeletedNode. If this destroys the last
	 possible path through @a lg=, a warning is displayed. This function always
	 re-computes the number of remaining paths in @a lg.
	 */
	public void deleteLn(LexemeNode ln) {
		int oldNo = noOfPaths;

		/* already deleted, ok */
		if (deletedLNs.contains(ln))
			return;

		deletedLNs.add(ln);
		computeNoOfPaths(false);

		if (noOfPaths == 0 && oldNo > 0) {
			logger.warn(String.format("WARNING: lgDeleteNode: deletion of node `%s' destroys last path",
					ln.getLexeme().getDescription()));
		}

		/* have to do this because two nodes may become more distant by the
		 * deletion of the shortest connecting path */
		computeDistances();
	}

	/**
	   optimize constraint net by deleting lexemes and values

	   This function deletes lexeme nodes and LVs from a constraint net that
	   cannot possibly appear in any solution. It uses several rules for
	   deleting structures:

	   -  An LV can be deleted if it binds a set of lexeme nodes that are
	     all deleted.
	   -  An LV can be deleted if is modifier and modifiee are
	     incompatible.
	   -  A lexeme node can be deleted if it it cannot be bound by any LV
	     on one level.
	   -  A lexeme can be deleted if there is no path through the lexeme
	     graph in which it appears, and which is totally undeleted.

	   Since these conditions can trigger each other, the function
	   \b cnOptimizeNode() is called repeatedly on each node in the
	   constraint net until no progress has been made. The function returns
	   the total number of changes returned by the calls to
	   \b cnOptimizeNode(), or -1 if none of them did so.

	   @returns > 0 : we have changed the constraintnet
	   @returns 0   : no changes have been made
	   @returns < 0 : the net is invalid
	 */
	public int optimizeNet() {
		int n, result = 0;
		boolean progress = true;

		/* loop while progress has been made */
		while (progress) {
			progress = false;

			/* loop over all nodes and call cnOptimizeNode */
			for (ConstraintNode cnode : net.getNodes()) {
				
				n = optimizeNode(cnode);
				if (n < 0) {
					return -1;
				}
				else if (n > 0) {
					progress = true;
					result += n;
				}
			}
		}
		return result;
	}

	/**
	   Optimize constraint node by deleting lexemes and values.
	   @returns > 0 : we have changed the constraintnode
	   @returns 0   : no changes have been made
	   @returns < 0 : the node is invalid

	   This function serves to discard structures in a constraint node that
	   cannot appear in any solution. It returns the number of deletion
	   operations that it has performed, or -1 if an inconsistency was found.
	   This function is repeatedly called by optimizeNet().
	 */
	private int optimizeNode(ConstraintNode node) {
		int noChanges = 0;
		
		GraphemeNode gn = node.getGN();
		boolean pruneModifiees = true;
		
		GraphemeNode theModifiee = null;
		List<Boolean> down = new ArrayList<>(net.getNoOfLexemes());
		List<Boolean> up   = new ArrayList<>(net.getNoOfLexemes());
		for(int i =0; i < net.getNoOfLexemes(); i++) {
			down.add(false);
			up.add(false);
		}

		/* condition (a) */
		if (noOfValidValuesForCNode(node) > 0) {

			/* test domain */
			for (LevelValue lv: node.getValues()) {
				if (isDeletedLv(lv)) {
					continue;
				}

				/* mark invalid levelvalues as deleted */
				if (areDeletedNodes(lv.getDependents())) {
					deleteLv(lv);
					noChanges++;
				} else if(lv.getRegentGN().spec() && areDeletedNodes(lv.getRegents()) ||
						lv.getRegentGN().spec() && 
						!areCompatibleNodes(lv.getDependents().get(0), lv.getRegents().get(0))) {
					deleteLv(lv);
					noChanges++;
				}

				if (isDeletedLv(lv)) {
					continue;
				}

				/* detect that a lexeme node can still be bound */
				for (LexemeNode ln : lv.getDependents()) {
					down.set(ln.getNo(), true);
				}

				/* if all of the LVs in this node modify the same grapheme node, then
				 * we can delete all variants of their regents.
				 * Otherwise we can't be sure, so we don't do anything. */
				if (lv.getRegentGN().spec()) {
					if (pruneModifiees) {
						if (theModifiee == null) {
							theModifiee = lv.getRegentGN();
						} else if (theModifiee != lv.getRegentGN()) {
							pruneModifiees = false;
						}
					}
					if (pruneModifiees) {
						for (LexemeNode ln : lv.getRegents()) {
							up.set(ln.getNo(), true);
						}
					}
				} else {
					pruneModifiees = false;
				}
			}
		}

		if (noValidValues.get(node) == 0) {
			if (!isDeletableNode(gn)) {
				logger.warn(String.format(
						"can't find a value for %s/%s(%d,%d), invalid net",
						gn.getArc().word,
						node.getLevel().getId(), gn.getArc().from, gn.getArc().to));
				return -1;		       /* this indicates an invalid net */
			}
		}

		/* condition (b) */
		for (LexemeNode ln : gn.getLexemes()) {
			if (!isDeletedLn(ln) && !down.get(ln.getNo())) {
				deleteLexemeNode(ln);
				noChanges++;
			}
		}
		if (pruneModifiees && theModifiee != null) {
			for (LexemeNode ln : theModifiee.getLexemes()) {
				if (!isDeletedLn(ln) && !up.get(ln.getNo())) {
					deleteLexemeNode(ln);
					noChanges++;
				}
			}
		}

		/* condition (c) */
		if (noOfPathsFromStart.get(gn.getNo()) * noOfPathsToEnd.get(gn.getNo()) == 0 &&
				!areDeletedNodes(gn.getLexemes())) {
			deleteLexemeNodes(gn.getLexemes());
			noChanges++;
		}
		return noChanges;
	}


	/**
	 * checks whether not all paths passing this GraphemNode will be broken by
	 * its deletion
	 */
	private boolean isDeletableNode(GraphemeNode gn) {
		long affectedPaths = noOfPathsFromStart.get(gn.getNo()) * noOfPathsToEnd.get(gn.getNo());

		return affectedPaths < noOfPaths;
	}
	
	/**
	 @brief delete a list of lexeme nodes

	 This function behaves as lgDeleteNode() were called on each element of the @a
	 nodes, but it is more efficient since it only re-computes the number of
	 remaining paths once.
	 */
	public void deleteLexemeNodes(List<LexemeNode> nodes) {
		int oldNo = noOfPaths;

		/* So we inline the relevant code into a loop here... */
		for (LexemeNode ln : nodes) {
			deletedLNs.add(ln);
		}

		computeNoOfPaths(false);

		if (noOfPaths == 0 && oldNo > 0) {
			logger.warn(String.format("WARNING: deleteLexemeNodes: deletion of node `%s' destroys last path",
					nodes.get(0).getLexeme().getDescription()));
		}

		/* have to do this because two nodes may become more distant by the
		 * deletion of the shortest connecting path */
		computeDistances();
	}

	/**
	 @brief deletes a node from the lexeme graph itself.

	 This function marks a lexeme node as deleted. It does this by setting the
	 cell @a ln->no in the Vector @a lg->isDeletedNode. If this destroys the last
	 possible path through @a lg=, a warning is displayed. This function always
	 re-computes the number of remaining paths in @a lg.
	 */
	public void deleteLexemeNode(LexemeNode ln) {
		List<LexemeNode> lns = new ArrayList<>();
		lns.add(ln);
		deleteLexemeNodes(lns);
	}

	/**
	 @brief returns TRUE if lexem nodes a and b exist on one path.

	 This function checks whether, in principle, a complete path can exist
	 through @a lg that includes both @a a and @a b. This is independent of the
	 current state of deletions. In fact, the function merely checks whether the
	 distance between the nodes is not 0 by using lgDistanceOfNodes(). Note that
	 two nodes are not automatically compatible merely because they do not
	 overlap in time.  
	 Also, a lexeme node IS compatible with itself!
	 */
	private boolean areCompatibleNodes(LexemeNode a, LexemeNode b) {
		if ( a == null || b == null) {
			logger.error("ERROR: areCompatibleNodes: argument is NULL\n");
			throw new NullPointerException();
		}
		
		return (a == b || !a.overlaps(b));
	}
	
	
	/**
	 @brief returns a distance measure for two lexem nodes

	 This function computes the logical distance between @a a and @a b, measured
	 in words. Usually this is just the corresponding element of
	 LexemGraph::distance. If either of the nodes is underspecified it is treated
	 as if it followed the latest specified lexeme node directly. Hence, the
	 return value may be greater than value in LexemGraph::distance. Two
	 underspecified lexeme nodes are considered to have distance zero.

	 The flag virtualAsNonspec determines if the position of virtual nodes
	 should be treated as underspecified
	  FALSE for lattice consistency checks like the path criterion
	  TRUE for linguistic checks like the distance function
	 */
	public int distanceOfNodes(GraphemeNode a, GraphemeNode b, boolean virtualAsNonspec) {
		int maxDist, i, maxDim, dist;

		if ( a == null || b == null) {
			logger.error("ERROR: distanceOfNodes: argument is NULL\n");
			throw new NullPointerException();
		}
		if (a.isNonspec() || (virtualAsNonspec && a.isVirtual()) ) {
			if (b.isNonspec() || (virtualAsNonspec && b.isVirtual()) )
				return 0;

			maxDim  = distance.size();
			maxDist = 0;
			for (i = 0; i < maxDim; i++) {
				if(lg.getGNs().get(i).isVirtual()) {
					continue; // virtual nodes don't push back nonspec
				}
				dist = distance.get(b.getNo()).get(i);
				if (maxDist < dist) {
					maxDist = dist;
				}
			}
			return -maxDist - 1;
		}

		if (b.isNonspec() || (virtualAsNonspec && b.isVirtual()) ) {
			maxDim = distance.size();
			maxDist = 0;
			for (i = 0; i < maxDim; i++) {
				if(lg.getGNs().get(i).isVirtual()) {
					continue; // virtual nodes don't push back nonspec
				}
				dist = distance.get(a.getNo()).get(i);
				if (maxDist < dist) {
					maxDist = dist;
				}
			}
			return maxDist + 1;
		}
		return distance.get(a.getNo()).get(b.getNo());
	}
	
	/**
	 * mark the given lv as undeleted 
	 * and update the valid lv counts
	 */
	public void undeleteLv(LevelValue lv) {
		if(isDeletedLv(lv)) {
			deletedLVs.remove(lv);
			
			for(int i= lv.getDependentGN().getArc().from; i < lv.getDependentGN().getArc().to; i++) {
				ConstraintNode cnode = net.getCNode(lv.getIndex());
				noValidValues.put(cnode, noValidValues.get(cnode) +1);
				noOfUndeletedLVs += 1;
			}
		}
	}
	
	public void undeleteLn(LexemeNode ln) {
		deletedLNs.remove(ln);
	}
	
	public double getLvLimit(LevelValue lv) {
		if(lvLimits.containsKey(lv))
			return lvLimits.get(lv);
		else if(lv.getIndexWRTNet() == -1)
			// lookup of unknown lvs can happen with lvs
			// that where deleted when buiulding the net (iwrtnet==-1)
			// such lvs might be needed later on for transitions from one valid 
			// analysis to another and are temporarily rebuild
			// such lvs alwqays have a score, and thus a limit, of zero
			return 0.0;
		else 
			throw new RuntimeException("Limit lookup for unknown levelValue");
	}
	
	public double getLnLimit(LexemeNode ln) {
		return lnLimits.get(ln);
	}
	
	public void setLvLimit(LevelValue lv, double limit) {
		lvLimits.put(lv, limit);
	}
	
	public void setLnLimit(LexemeNode ln, double limit) {
		lnLimits.put(ln, limit);
	}

	public int getNoOfUndeletedLVs() {
		return noOfUndeletedLVs;
	}
	
	public double getAmbiguity() {
		return (double)noOfUndeletedLVs/ net.getNoOfActiveSlots();
	}
	
	public String getAmbiguityString() {
		if (noOfUndeletedLVs == net.getNoOfActiveSlots()) {
			return " (unique)";
		} else {
			return String.format(" ambiguity %.1f", (float)noOfUndeletedLVs / net.getNoOfActiveSlots());
		}
	}
	
	/** 
	 * returns the cached information about how many of a constraintnode's values are
	 * still valid (= not deleted)  
	 */
	public int noOfValidValuesForCNode(ConstraintNode cnode) {
		return noValidValues.get(cnode);
	}
	
	/**
	 * How many undeleted LVs does the domain WHERE contain?
	 */
	public int getDomainSize(int where) {
		// we don't have to count as that is cached in noValidValues
		return noOfValidValuesForCNode( net.getCNode(where) );
	}
	
	/**
	 Count remaining LVs in Problem.

	 This function computes the field P->nrUndeletedLVs. Note that usually
	 this field is maintained by the frobbing routines that may delete LVs.
	 This function must be called after a foreign routine has deleted LVs.
	 */
	public boolean checkAmbiguity() {
		int tmpNoOfUndeletedLVs = 0;
		for (int i = 0; i < net.getNoOfSlots(); i++) {
			int tmpValidValuesForSlot =0;
			ConstraintNode cnode = net.getCNode(i);
			for (LevelValue lv: domains.get(i)) {
				if (lv.getLevel().isUsed() && !deletedLVs.contains(lv)) {
					tmpValidValuesForSlot+= 1;
				}
			}
			
			if(tmpValidValuesForSlot != noValidValues.get(cnode)) {
				logger.warn(String.format("Inconsistency in ambiguity tracking for domain %d: cached:%d, counted: %d",
						i, noValidValues.get(cnode), tmpValidValuesForSlot));
				return false;
			}
			tmpNoOfUndeletedLVs += tmpValidValuesForSlot;
		}
		
		if(noOfUndeletedLVs != tmpNoOfUndeletedLVs) {
			logger.warn(String.format("Inconsistency in ambiguity tracking: cached:%d, counted: %d",
					noOfUndeletedLVs, tmpNoOfUndeletedLVs));
			return false;
		}
		return true;
	}
	
	/**
	 Propagate a limit to an LV.

	 This can only happen for two reasons: either LV binds a set of lexeme
	 nodes (as dependents or as regents) that all have a common limit not
	 higher than F, or there is, for a certain set of domains, no combined
	 assignment including LV with a limit greater than F.

	 Accordingly, the only calls to downgrade() are during the routines
	 updateLimit() and dSearch().
	 */
	public void downgrade(LevelValue lv, double newLimit, double globalMinimum) {
		/* downgrade */
		setLvLimit(lv, newLimit);

		/* downgrading an LV past the global record means deleting */
		if (Badness.significantlyGreater(globalMinimum, newLimit) && 
				!isDeletedLv(lv)) {
			deleteLv(lv);
		}
	}
	
	/**
	 * Recompute the limit of an LV based on the limits of the lexemes it binds.
	 */
	public void updateLimit(LevelValue lv, double threshold) {
		double f;
		
		/* deleted? */
		if (deletedLVs.contains(lv)) {
			downgrade(lv, 0.0, threshold);
		}

		/* dependents limited? */
		f = 0.0;
		for (LexemeNode ln : lv.getDependents()) {
			if (lnLimits.get(ln) > f) {
				f = lnLimits.get(ln);
			}
		}
		if (f < lvLimits.get(lv)) {
			downgrade(lv, f, threshold);
		}

		/* regents limited? */
		if (lv.getRegentGN().spec()) {
			f = 0.0;
			for (LexemeNode ln : lv.getRegents()) {
				if (lnLimits.get(ln) > f) {
					f = lnLimits.get(ln);
				}
			}
			if (f < lvLimits.get(lv)) {
				downgrade(lv, f, threshold);
			}
		}
	}
	
	/**
	 * get a list of all levelvalues for a given slot
	 * all levelvalues in the list will have index i
	 * 
	 * @param i
	 * @return
	 */
	public List<LevelValue> getDomain(int i) {
		return domains.get(i);
	}
	
	/**
	 * Optimize several domains after another.
	 *
	 * This function behaves like separate calls to dCheck() with all
	 * the integers in which as arguments, but is more efficient
	 * because the cleanup code only runs once.
	 * 
	 * @throws InvalidNetStateException 
	 */
	public void checkDomains(List<Integer> which, double threshold) throws InvalidNetStateException {
		
		for (Integer d : which) {
			checkDomain(d, threshold);
		}

		if (optimizeNet() < 0) {
			throw new InvalidNetStateException("Invalid net!");
		}
		
		assert checkAmbiguity();
	}
	
	/**
	 Re-calculate limits for all LVs in a domain.

	 This function propagates limits bidirectionally between LVs and lexeme
	 nodes. It is a generalization of cnOptimizeNode(): the former function
	 can only delete structures if they are absolutely impossible. This
	 function can also detect implausible structures:

	 The limit of an LV cannot exceed the limits of all its dependents (or all
	 of its regents).

	 The limit of a lexeme node cannot exceed the limit of all LVs that bind
	 it as a modifier. (Note that a lexeme node can appear in an Analysis
	 without ever being a modifiee, so this second condition is not
	 symmetrical.)

	 Theoretically, the limit of a lexeme node cannot exceed the limits of
	 all other lexemes that are needed to connect it to the start and end of
	 the word graph. However, because this condition is expensive to compute,
	 and is also approximated by the first two conditions, it is not
	 enforced.

	 !ALERT! This function modifies the domain WHICH. Using it while
	 iterating over the same domain will yield undefined behaviour.
	 */
	private void checkDomain(int which, double threshold) {
		//List l, m;
		//Number *localLimit;
		//LevelValue lv;
		//LexemNode ln;
		//int i, no, from;
		//Number f;

		/* empty domain ? */
		if (getDomain(which).size() == 0)
			return;
		
		int from = which / net.getNoOfLevels();

		/* localLimits start out at 0 */
		List<Double> localLimit = new ArrayList<>(net.getNoOfLexemes());
		for (int i = 0; i < net.getNoOfLexemes(); i++) {
			localLimit.add(0.0);
		}

		/* propagate limit of LVs to lexeme nodes */
		for (LevelValue lv: getDomain(which)) {
			if(isDeletedLv(lv)) {
				setLvLimit(lv, 0.0);
				continue;
			}
			updateLimit(lv, threshold);
			double f = getLvLimit(lv);

			if (Badness.significantlyGreater(threshold, f) && !isDeletedLv(lv)) {
				deleteLv(lv);
				continue;
			}

			for (LexemeNode ln : lv.getDependents()) {
				int no = ln.getNo();
				if (f > localLimit.get(no)) {
					localLimit.set(no, f);
				}
			}
		}

		/* lower limits of lexeme nodes that have too little support */
		for (LexemeNode ln : net.getLexemesAt(from)) {
			if (localLimit.get(ln.getNo()) < getLnLimit(ln)) {
				setLnLimit(ln, localLimit.get(ln.getNo()));

				if (Badness.significantlyGreater(threshold, getLnLimit(ln))) {
					deleteLn(ln);
				}
			}
		}

		/* propagate limits from lexeme nodes to LV*/
		for (LevelValue lv : getDomain(which)) {
			updateLimit(lv, threshold);
		}
		/* and re-sort domain */
		Collections.sort(getDomain(which), this);
	}

	@Override
	public int compare(LevelValue lv1, LevelValue lv2) {
		// high limits first
		double limit1 = lvLimits.get(lv1);
		double limit2 = lvLimits.get(lv2);
		if(limit1 < limit2)
			return  1;
		if(limit1 > limit2)
			return -1;
		return 0;
	}
	
	/**
	 * @return the underlying net
	 */
	public ConstraintNet getNet() {
		return net;
	}

	public Set<LexemeNode> getLnDeletion() {
		return deletedLNs;
	}	
	
	public int getNoOfPaths() {
		return noOfPaths;
	}
	
	/**
	 * search through a domain for (one of) the levelvalue(s),
	 * where the regent is NIL and the label is empty
	 * alternatively a lexemenode can be given
	 * 
	 * if no such levelvlaue exists, null is returned
	 */
	public LevelValue findUnusedLv(int i, LexemeNode ln) {
		
		for(LevelValue lv : domains.get(i)) {
			 if(     lv.getRegentGN().isNil()  && 
				  	 lv.getLabel().equals("") &&
				  	 (ln == null || lv.getDependents().contains(ln)) ) {
				 return lv;
			 }
		}
		return null;
	}
	
	public String printPossibilities() {
		StringBuilder ret = new StringBuilder();
		Map<Integer, Set<String>> labels = new HashMap<>();
		Map<Integer, Set<Integer>> heads = new HashMap<>();
		
		for(int i=0; i < net.getNoOfSlots(); i++) {
			labels.put(i, new HashSet<>());
			heads.put(i, new HashSet<>());
		}
		
		for(LevelValue lv : net.getValues()) {
			if(!isDeletedLv(lv)) {
				labels.get(lv.getIndex()).add(lv.getLabel());
				heads.get(lv.getIndex()).add(lv.getRegentGN().getNo());
			}
		}
		
		for(int i=0; i < net.getNoOfSlots(); i++) {
			List<Integer> hs = new ArrayList<>(heads.get(i));
			Collections.sort(hs);
			List<String > ls = new ArrayList<>(labels.get(i));
			Collections.sort(ls);
			ret.append(String.format("%10s(%4s):%s%s%n", 
					net.getLexemeGraph().getGNs().get(i/net.getNoOfLevels()).getArc().word, 
					net.getGrammar().getLevels().get(i%net.getNoOfLevels()).getId(),
					hs.toString(), ls.toString()));
		}
		return ret.toString();
	}
	
	public String printValidValues() {
		StringBuilder ret = new StringBuilder("Lexemes:\n");
		for(GraphemeNode gn : lg.getGNs()) {
			int total = gn.getLexemes().size();
			int valid = 0;
			for(LexemeNode ln : gn.getLexemes()) {
				if(!isDeletedLn(ln))
					valid ++;
			}
			ret.append(String.format("%2d: %3d/%3d \n", gn.getNo(), valid, total));
		}
		ret.append("LevelValues: \n");
		for(ConstraintNode cnode : net.getNodes()) {
			ret.append(String.format("%2d%4s: %3d/? \n", 
					cnode.getGN().getNo(), cnode.getLevel().getId(), noOfValidValuesForCNode(cnode)));
		}
		
		return ret.toString();
	}
	
	/**
	 * mostly for debugging 
	 */
	public List<LevelValue> findLvInDomainByLabel(int domain, String label) {
		List<LevelValue> ret = new ArrayList<>();
		for(LevelValue lv : domains.get(domain)) {
			if(lv.getLabel().equals(label)) {
				ret.add(lv);
			}
		}
		return ret;
	}
	
	/**
	 * mostly for debugging 
	 */
	public List<LevelValue> findLvInDomainByHead(int domain, int head) {
		List<LevelValue> ret = new ArrayList<>();
		for(LevelValue lv : domains.get(domain)) {
			if(lv.getRegentGN().getNo() == head) {
				ret.add(lv);
			}
		}
		return ret;
	}
	
	/**
	 * mostly for debugging 
	 */
	public List<LevelValue> findLvInDomainByHeadAndLabel(int domain, String label, int head) {
		List<LevelValue> ret = new ArrayList<>();
		for(LevelValue lv : domains.get(domain)) {
			if(lv.getLabel().equals(label) && lv.getRegentGN().getNo() == head) {
				ret.add(lv);
			}
		}
		return ret;
	}
	
	/**
	 * mostly for debugging 
	 */
	public List<Boolean> mapIsDeleted(List<LevelValue> lvs) {
		List<Boolean> ret = new ArrayList<>();
		for(LevelValue lv : lvs) {
			ret.add(isDeletedLv(lv));
		}
		return ret;
	}
	
	/**
	 * mostly for debugging 
	 */
	public List<Double> mapLimit(List<LevelValue> lvs) {
		List<Double> ret = new ArrayList<>();
		for(LevelValue lv : lvs) {
			ret.add(getLvLimit(lv));
		}
		return ret;
	}
	
	/**
	 * Commit to the current value of domain WHERE.
	 * 
	 * All other LVs in the domain are marked deleted.
	 */
	public void freezeDomain(Analysis a, int where) {
		LevelValue theOne = a.slotFiller(where);
		
		for (LevelValue lv : domains.get(where)) {
			if(lv != theOne) {
				// TODO isn't this too harsh? 
				// a definition like this would still allow lexeme flexibility
//			if (!lv.getLabel().equals(theOne.getLabel()) ||
//					lv.getRegentGN().getNo() != theOne.getRegentGN().getNo()) {
				deleteLv(lv);
			}
		}
	}
	
	/**
	 * This function deletes or undeletes all LVs in the net,
	 *  - deletes if the score is below the given threshold or is zero
	 *  - undeletes else
	 */
	public void resetToThreshold(double threshold) {
		
		for (LevelValue lv : net.getValues()) {
			if(lv.getScore() < threshold || lv.getScore() == 0.0) {
				deleteLv(lv);
				setLvLimit(lv, 0.0);
			} else {
				undeleteLv(lv);
				setLvLimit(lv, lv.getScore());
			}
		}

		undeleteLexemes();
	}
	
	/**
	 * marks all lexemes in the lexemgraph as not deleted
	 * and resets the limits
	 */
	public void undeleteLexemes() {
		
		for(LexemeNode ln : net.getLexemeGraph().getNodes()) {
			deletedLNs.remove(ln);
			lnLimits.put(ln, 1.0);
		}
		
		lgBuildFinal();
	}
	
	/**
	 * mark LVs as deleted so that only solutions compatible with the given parse are allowed
	 * compatibility check regards
	 * - label
	 * - regent
	 *
	 * if a level is given, only values of this level are manipulated
	 */
	public void lockToParse(Parse p, Level lvl) {
		int dep, reg; // dependent and regent ids
		String levelId;

		for ( LevelValue lv : net.getValues() ) {
			
			if (lvl != null && lvl != lv.getLevel()) {
				continue;
			}

			dep = lv.getDependentGN().getNo();
			reg = lv.getRegentGN().getNo();
			levelId = lv.getLevel().getId();

			if(reg != p.getHeadOf(dep, levelId) ||
					!p.getLabelOf(dep, levelId).equals(lv.getLabel()) ) {
				// lv is not compatible with the given parse, so:
				deleteLv(lv);
				setLvLimit(lv, 0.0);
			}
		}
	}
	
	/**
	 * Similar to lockToParse
	 * 
	 * mark LVs as deleted so that only solutions compatible with 
	 * the unlexicalized parse for the given analysis are allowed.
	 * The compatibility check regards
	 * - label
	 * - regent
	 *
	 * If a level is given, only values of this level are manipulated
	 */
	public void lockToOnlyLexemeVariants(Analysis a, Level lvl) {
		GraphemeNode reg;

		for ( LevelValue lv : net.getValues() ) {
			LevelValue slotholder = a.getSlots().get(lv.getIndex());
			
			if (lvl != null && lvl != lv.getLevel()) {
				continue;
			}

			reg  = lv.getRegentGN();
			
			if(reg != slotholder.getRegentGN() ||
					!lv.getLabel().equals(slotholder.getLabel()) ) {
				// lv is not compatible with the given parse, so:
				deleteLv(lv);
				setLvLimit(lv, 0.0);
			}
		}
	}
	
	/**
	 * deletes and undeletes lvs so that the attachment to the given regent via the given label 
	 * is the only possible attachment of the given dependent on the given level
	 */
	public void lockToNewAttachment(Level lvl, GraphemeNode dep, String label, GraphemeNode reg) {
		List<LevelValue> domain = getDomain(net.getIndexOf(dep, lvl));
		for (LevelValue lv : domain) {
			if(lv.getRegentGN() == reg && lv.getLabel().equals(label)) {
				setLvLimit(lv, lv.getScore());
				undeleteLv(lv);
			} else {
				deleteLv(lv);
				setLvLimit(lv, 0.0);
			}
		}
	}
	
	/**
	 * undeletes all LVs with virtual nodes as dependent, that are unused in the given parse
	 */
	public void unlockUnusedVNs(Parse p) {
		LexemeGraph lg = net.getLexemeGraph();
		int reg;
		int noOfLevels = net.getNoOfLevels();
		String label;
		
		if(! (lg instanceof VirtualLexemeGraph)) {
			// no VNs, no problem
			return;
		}
		
		VirtualLexemeGraph vlg = (VirtualLexemeGraph) lg;

		for(int i=vlg.getVStartId(); i < vlg.getMax(); i++) {
			reg  = p.getHeadOf( i, net.getMainLevel().getId());
			label= p.getLabelOf(i, net.getMainLevel().getId());
			
			if( reg==-1 && label.equals("") ) {
				// is unused, reactivate all lvs
				for( int k=0; k<noOfLevels; k++) {
					List<LevelValue> lvs =getDomain(i*noOfLevels);
					for(LevelValue lv  : lvs) {
						if(lv.getScore() > 0.0) {
							setLvLimit(lv, lv.getScore());
							undeleteLv(lv);
						}
					}
				}
			}
		}
	}
	
	/**
	 * checks, whether there are valid values left for every constraintnode
	 */
	public boolean isValid() {
		for(ConstraintNode cn : net.getNodes()) {
			if( noValidValues.get(cn) <= 0 ) {
				return false;
			}
		}
		
		return true;
	}
}
