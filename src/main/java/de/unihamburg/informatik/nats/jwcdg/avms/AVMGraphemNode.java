package de.unihamburg.informatik.nats.jwcdg.avms;

import java.util.regex.Matcher;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.GraphemeNode;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;

public class AVMGraphemNode extends AVM {
	private static final long serialVersionUID = 8521659902381250494L;
	
	private GraphemeNode gn;

	public AVMGraphemNode(GraphemeNode gn) {
		this.gn = gn;
	}
	
	public GraphemeNode getGn() {
		return gn;
	}
	
	@Override
	public AVM substituteRefs(Matcher matcher, Configuration config) {
		return this;
	}
	
	@Override
	public String toString() {
		return "[" + gn.toString() + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		
		if(!(obj instanceof AVMGraphemNode))
			return false;
		
		AVMGraphemNode other = (AVMGraphemNode) obj;
		
		return this.gn.equals(other.gn);
	}

	@Override
	public int hashCode() {
		return gn.hashCode();
	}
}
