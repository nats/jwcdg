package de.unihamburg.informatik.nats.jwcdg.input;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Hierarchy implements Serializable{

    private static final long serialVersionUID = 1L;
    public static final String BOTTOM = "bot";
	public static final String TOP    = "top";

	private static Logger logger = LogManager.getLogger(Hierarchy.class);
	
	/** identifikation */
	private String id;

	/** hash table of all defined types */
	private Map<String, Integer> types;    

	/**
	 *  array of int
	 * distance[2, 4] == 3 means there is a path of length 3
	 * from node 2 to node 4
	 * distance[...] == 0 means there is NO such path
	 */
	private List<List<Integer>> distance;

	private SourceInfo source;


	public Hierarchy(String id, Sort sort, SourceInfo source) {
		this.id= id;
		this.source = source;

		types = new HashMap<>();

		/* collects types in hashtable */
		fromSortTree( sort );
	}

	public Hierarchy(String id, List<Subsumption> subsumptions, SourceInfo source) {
		this.id= id;
		this.source = source;

		types = new HashMap<>();

		/* collects types in hashtable */
		subsumptionListToHierarchy(subsumptions);
	}
	
	public String getId() {
		return id;
	}

	public boolean contains(String key) {
		return types.containsKey(key);
	}

	/**
	 *  Does UP subsume DOWN in this Hierarchy?
	 */
	public boolean subsumes(String up, String down) {
		int a, b;

		/* find both sorts */
		if(!types.containsKey(up) || !types.containsKey(down)) 
			// unknown entry, no subsumption
			return false;
		
		a = types.get(up);
		b = types.get(down);

		/* return canned answer */
		return distance.get(a).get(b) >= 0;
	}

	@Override
	public String toString() {
		return "Hierarchy "+ id;
	}

	public SourceInfo getSource() {
		return source;
	}

	/**
	 * inputSortTreeToHierarchy
	 */
	private void fromSortTree( Sort s ) {
		int size;

		fillTypesHashFromSortTree(s);

		int top = assureTop();
		int bot = assureBot();

		size = types.size();

		initDistance(size);

		/* register direct subsumption */
		markDirectSucessors( s );

		for ( int i = 0; i < size; i++ ) {
			Boolean iSubsumesSomething = false;
			Boolean iIsSubsumed = false;

			for ( int j = 0; j < size; j++ ) {
				if ( distance.get(i).get(j) > 0 ) {
					iSubsumesSomething = true;
				}
				if ( distance.get(j).get(i) > 0 ) {
					iIsSubsumed = true;
				}
			}
			if ( ! iSubsumesSomething ) {
				distance.get(i).set(bot, 1);
			}
			if ( ! iIsSubsumed ) {
				distance.get(top).set(i, 1);
			}
			/* reflexivity */
			distance.get(i).set(i, 0);
		}

		transitiveClosure( distance, size );
	}

	private void initDistance(int size) {
		distance = new ArrayList<>(size);
		for(int i =0; i< size; i++) {
			List<Integer> l = new ArrayList<>(size);
			for(int j =0; j< size; j++) {
				l.add(-1);
			}
			distance.add(l);
		}
	}

	private int assureTop() {
		/* if `top' not already there, insert it */
		if ( types.containsKey(TOP) ) {
			return types.get(TOP);
		} else {
			int top = types.size();
			types.put(TOP, top);
			return top;
		} 
	}
	
	private int assureBot() {
		/* if `bot' not already there, insert it */
		if ( types.containsKey(BOTTOM)) {
			return types.get(BOTTOM);
		} else {
			int bot = types.size();
			types.put(BOTTOM, bot);
			return bot;
		}
	}

	/**
	 * markDirectSucessorsInHierarchy
	 */
	private void markDirectSucessors( Sort s ) {
		int x, y;

		if ( types.containsKey(s.getId())) {
			x= types.get(s.getId());
		} else {
			logger.error(String.format(
					"ERROR: error in markDirectSucessorsInHierarchy with `%s'",
					s.getId()));
			return;
		}

		for ( Sort t : s.getSubSorts() ) {
			if ( types.containsKey(t.getId()) ) {
				y= types.get(t.getId());
			} else {
				logger.error(String.format(
						"ERROR: error in markDirectSucessorsInHierarchy with `%s'",
						t.getId()));
				return;
			}
			distance.get(x).set(y, 1);
			markDirectSucessors( t );
		}
	}

	/**
	 * Warshall's algorithm for transitive closure
	 */
	private void transitiveClosure( List<List<Integer>> a, int size ) {
		int i, j, k;

		for ( i = 0; i < size; i++ ) {
			for ( j = 0; j < size; j++ ) {
				int ji = a.get(j).get(i);

				if ( ji > 0 ) {
					for ( k = 0; k < size; k++ ) {
						int ik = a.get(i).get(k);
						int jk = a.get(j).get(k);

						if ( ik > 0 && ( jk == -1 || ji+ik < jk ) ) {
							a.get(j).set(k, ji+ik);
						}
					}
				}
			}
		}
	}

	/**
	 * fillTypesHashFromSortTree
	 */
	private boolean fillTypesHashFromSortTree( Sort s ) {

		if ( types.containsKey(s.getId()) ) {
			logger.warn(String.format(
					"WARNING: multiple introduction of sort `%s' in tree hierarchy `%s', ignored",
					s.getId(), id ));
			return false;
		}

		types.put(s.getId(), types.size());

		/* check whether `bot' subsumes something */
		if ( s.getId().equals(BOTTOM) && !s.getSubSorts().isEmpty() ) {
			logger.warn(String.format(
					"WARNING: special type `bot' must not subsume any type (e.g. `%s') in `%s', ignored",
					s.getSubSorts().get(0).getId(), id ));
			return false;
		}


		for ( Sort t : s.getSubSorts()) {
			if ( t.getId().equals(TOP) ) {
				logger.warn(String.format(
						"WARNING: special type `top' must not be subsumed by type `%s' in `%s', ignored",
						s.getId(), id ));
			} else {
				fillTypesHashFromSortTree(t);
			}
		}
		return true;
	}

	/**
	 * inputSubsumptionListToHierarchy
	 */
	private void subsumptionListToHierarchy( List<Subsumption> l ) {
		int size, x, y;

		for (Subsumption s : l) {
			if ( ! types.containsKey(s.type)) {
				types.put(s.type, types.size());
			}
			for ( String t : s.types) {
				if ( ! types.containsKey(t)) {
					types.put(t, types.size());
				}
			}
		}

		int top = assureTop();
		int bot = assureBot();

		size = types.size();
		initDistance(size);
		
		for ( Subsumption s : l ) {
			if ( s.type.equals(TOP) && !s.subsumesFlag) {
				logger.warn(String.format(
						"WARNING: special type `top' must not be subsumed by any type in `%s', ignored",
						id ));
				continue;
			}
			if ( s.type.equals(BOTTOM) && s.subsumesFlag) {
				logger.warn(String.format(
						"WARNING: special type `bot' must not subsume any other type in `%s', ignored",
						id ));
				continue;
			}
			x = types.get(s.type);

			/* register direct subsumption */
			for (String str : s.types) {
				y = types.get(str);
				if (s.subsumesFlag) {
					if ( y == top ) {
						logger.warn(String.format(
								"WARNING: special type `top' must not be subsumed by type `%s' in `%s', ignored",
								s.type, id ));
						continue;
					}
					distance.get(x).set(y, 1);
				} else {
					if ( y == bot ) {
						logger.warn(String.format(
								"WARNING: special type `bot' must not subsume type `%s' in `%s', ignored",
								s.type, id ));
						continue;
					}
					distance.get(y).set(x, 1);
				}
			}
		}

		for ( int i = 0; i < size; i++ ) {
			Boolean iSubsumesSomething = false;
			Boolean iIsSubsumed = false;

			for ( int j = 0; j < size; j++ ) {
				if ( distance.get(i).get(j) > 0 ) {
					iSubsumesSomething = true;
				}
				if ( distance.get(j).get(i) > 0 ) {
					iIsSubsumed = true;
				}
			}
			if ( !iSubsumesSomething && i != bot ) {
				distance.get(i).set(bot, 1);
			}
			if ( !iIsSubsumed && i != top ) {
				distance.get(top).set(i, 1);
			}
		}

		transitiveClosure( distance, size );

		/* check for cycles */
		for ( String key : types.keySet()) {
			x = types.get(key);
			if ( distance.get(x).get(x) > 0 ) {
				logger.warn(String.format(
						"cycle in hierarchy `%s' for type `%s'",
						id, key ));
			}
			/* reflexivity, can't be done before */
			distance.get(x).set(x, 0);
		}

	}

}
