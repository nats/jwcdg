package de.unihamburg.informatik.nats.jwcdg.constraints.functions;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.GraphemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.Constraint;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.TermPeek;
import de.unihamburg.informatik.nats.jwcdg.evaluation.ConstraintEvaluationResult;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
Return the timepoint of the regent of a given word.

If there is a second argument, the search is repeated as long as the
edges in question bear that label.

If the second argument is the name of a unary constraint, this
constraint is called on each edge and the search continues until it
fails.

*/
public class FunctionParent extends Function {

	public FunctionParent(Grammar g) {
		super("parent", g, 1, 2, true);
	}

	@Override
	public AVM eval(List<Term> args, Context context, LexemeGraph lg,
			LevelValue[] binding) {
		TermPeek t = (TermPeek) args.get(0);
		int levelNo = binding[t.getVarIndex()].getLevel().getNo();
		LexemeNode ln;
		String condition = null;
		
		AVM v = t.eval(lg, context, binding);
		if (v instanceof AVMLexemNode) {
			ln = ((AVMLexemNode) v).getLexemeNode();
		} else {
			return new AVMError("WARNING: type mismatch (1st arg) for `parent'");
		}
		
		if (!ln.spec()) {
			return new AVMError("WARNING: unspecified 1st arg for `parent'");
		}
		int a = ln.getArc().from;

		if (args.size() == 2) {
			AVM val2 = args.get(1).eval(lg, context, binding);
			if (val2 instanceof AVMString) {
				condition = ((AVMString)val2).getString();
			} else {
				return new AVMError("WARNING: type mismatch (2st arg) for `parent'");
			}

		}
		
		int p = funcParentImpl(lg, context, a, levelNo, condition);
		
		return new AVMNumber(p);
	}

	/**
	 * Perform the work of funcParent().
	 */
	private int funcParentImpl(LexemeGraph lg, Context context, int i,
			int levelno, String condition) {
		
		int noOflevels = grammar.getNoOfLevels();
		LevelValue lv = null;
		Constraint c = null;
		int counter = 0;

		/* unary call: look one edge above, return apropriate result*/
		if (condition == null) {
			lv = context.getLV(noOflevels * i + levelno);
			if (lv == null) {
				return 0;
			}
			GraphemeNode gn = lv.getRegentGN();
			if (gn.isNonspec()) {
				return lg.getMax() + 1;
			} else if (gn.isNil()) {
				return 0;
			} else {
				return gn.getArc().to;
			}
		}
		
		/* if the string is the name of a unary constraint,
		 use this constraint instead of label match  */
		c = grammar.findConstraint(condition);
		if (c != null  && c.getVars().size() != 1) {
			c = null;
		}


		/* binary call: ascend edges with the correct label,
		 then return the dependent */
		while (counter < lg.getMax()) {
			counter++;
			
			lv = context.getLV(noOflevels * i + levelno);
			if (lv == null) {
				return 0;
			}
			GraphemeNode gn = lv.getRegentGN();
			
			if (gn.spec()) {
				i = gn.getArc().from;
			} else {
				break;
			}

			/* call constraint */
			if (c != null) { 
				ConstraintEvaluationResult cer = c.eval(lg, context, lv);
				if(cer.isViolated()) {
					break;
				} 
			}

			/* compare label */
			else {
				if (!lv.getLabel().equals(condition))
					break;
			}
		}
		
		if(counter >= lg.getMax()) {
			/* cycle */
			return -1;
		}
		
		return lv.getDependentGN().getArc().to;
	}

}
