package de.unihamburg.informatik.nats.jwcdg.avms;

import java.util.regex.Matcher;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;

public class AVMLexemNode extends AVM {
	private static final long serialVersionUID = -7324531686313658897L;
	
	private LexemeNode ln;
	
	public AVMLexemNode(LexemeNode ln) {
		this.ln = ln;
	}
	
	public LexemeNode getLexemeNode() {
		return ln;
	}
	
	@Override
	public String toString() {
		return "["+ln.toString()+"]";
	}
	
	@Override
	public AVM substituteRefs(Matcher matcher, Configuration config) {
		return this;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ln == null) ? 0 : ln.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AVMLexemNode))
			return false;
		AVMLexemNode other = (AVMLexemNode) obj;
		if (ln == null) {
			if (other.ln != null)
				return false;
		} else if (!ln.equals(other.ln))
			return false;
		return true;
	}
}
