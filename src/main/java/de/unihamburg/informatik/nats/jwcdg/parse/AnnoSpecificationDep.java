package de.unihamburg.informatik.nats.jwcdg.parse;

public class AnnoSpecificationDep extends AnnoSpecification {

	public final int position;		       /* position of modifying word */
	
	public AnnoSpecificationDep(String level, String label, int head) {
		super(level, label);
		this.position = head;
	}
	
	@Override
	public String toString() {
		return kind + "--" + name + "->" + position;
	}
	
}
