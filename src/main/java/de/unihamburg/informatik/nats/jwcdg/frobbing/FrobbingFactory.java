package de.unihamburg.informatik.nats.jwcdg.frobbing;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNet;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNetState;
import de.unihamburg.informatik.nats.jwcdg.evaluation.CachedGrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;
import de.unihamburg.informatik.nats.jwcdg.lattice.Lattice;
import de.unihamburg.informatik.nats.jwcdg.transform.Analysis;
import de.unihamburg.informatik.nats.jwcdg.transform.ProblemSolver;
import de.unihamburg.informatik.nats.jwcdg.transform.SolverFactory;


/**
 * 
 * generates solvers based on the frobbing algorithm
 * they all share a config and a grammar evaluator
 * 
 * @author the CDG-Team
 */
public abstract class FrobbingFactory implements SolverFactory {

	public enum FrobbingMethod {
		Combined,
		Dynamic,
		Threefold
	}

	private static final String INITIAL_PRESSURE = "initialPressure";
	
	private GrammarEvaluator evaluator;
	private Lexicon lex;
	private Configuration config;
	private FrobbingMethod method; 
	
	public FrobbingFactory(Grammar grammar, Lexicon lex, Configuration config, FrobbingMethod method) {
		this.lex       = lex;
		this.evaluator = new CachedGrammarEvaluator(grammar, config); // TODO #EVALUATOR allow other kinds of evaluators
		this.config    = config;
		this.method    = method;
	}
	
	@Override
	public ProblemSolver generateSolver(Lattice lat) {
		ConstraintNet      net   = new ConstraintNet(lat, lex, true, true, evaluator, config); 
		ConstraintNetState state = new ConstraintNetState(net, config);

		return generateSolver(state);
	}
	
	@Override
	public ProblemSolver generateSolver(ConstraintNetState state) {
		double pressure = config.getDouble(INITIAL_PRESSURE);
		
		switch (method) {
		case Combined:
			return new CombinedFrobber (state, config, evaluator, pressure);
		case Dynamic: 
			return new DynamicFrobber  (state, config, evaluator, pressure);
		case Threefold:
			return new ThreefoldFrobber(state, config, evaluator, pressure);
		default:
			throw new RuntimeException("Unsupported frobbing Method!");
		}
	}

	@Override
	public ProblemSolver generateSolver(ConstraintNetState state, Analysis a) {
		double pressure = config.getDouble(INITIAL_PRESSURE);
		
		switch (method) {
		case Combined:
			return new CombinedFrobber (state, a, config, evaluator, pressure);
		case Dynamic:
			return new DynamicFrobber  (state, a, config, evaluator, pressure);
		case Threefold:
			return new ThreefoldFrobber(state, a, config, evaluator, pressure);
		default:
			throw new RuntimeException("Unsupported frobbing Method!");
		}
	}

}
