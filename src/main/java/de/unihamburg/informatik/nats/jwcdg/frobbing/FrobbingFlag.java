package de.unihamburg.informatik.nats.jwcdg.frobbing;

/**
Bit flags for dSearch() and related functions.

FLAGS specifies particular kinds of behaviour. The following flags are defined:

OD_PREDICT: choose the Patch with the best predicted Badness.

OD_LIMIT: return the Patch with the best limit (default).

OD_SINGLE: return only unary Patches even when multiple domains are
specified.

OD_ORIENT: allow Patches that change the direction of edges.

OD_LEXICAL: allow Patches that change lexical items.

OD_LABEL: allow Patches that change labels.

OD_ALLOWALL: a synonym for specifying all of the three previous flags.
This ensures that no Patch is rejected for one of its LVs.

OD_DELETED: allow Patches that contain deleted LVs or lexemes.

Note that OD_DELETED only re-allows LVs that are present, but were
marked as deleted during the preceding parsing process. It can not
magically revive LVs that were rejected when the constraint net was
first built. Therefore, LVs that violate hard unary constraints are
usually not found even with OD_DELETE.

If you specify OD_DELETED to a function in the toolchain, you must also
pass it to all later functions that operate on the resulting Patches, to
avoid spooking the internal consistency checks.

OD_PRUNE: delete LVs that do not appear in any of the possible Patches.

OD_NARROW: ignore Patches with suboptimal limit.

OD_INTERACTIVE: ask the user to select a Patch instead of computing the
result autonomously.

OD_DEBUG: give overwhelming amount of output.

OD_CONTEXT: use context-sensitive constraints for evaluating a Patch.
This will give wrong results unless WHICH actually contains the entire
context.

OD_IMMEDIATE: return immediately if a Patch is found that improves A
beyond a->P->best->b. */
public enum FrobbingFlag {
	 
	//OD_INT,       /* arg 1 is int, not Vector */
	
	/** choose Patch with best Badness */
	OD_PREDICT,
	
	/** choose Patch with best limit  */
	OD_LIMIT,       
	
	/** do not search domains jointly */
	OD_SINGLE,
	
	/** allow structural change */
	OD_ORIENT,      
	
	/** allow lexical change */
	OD_LEXICAL,
	
	/** allow label change */
	OD_LABEL,
	
	/** allow combinations with score 0 */
	OD_DELETED,
	
	/** delete LVs that are proven wrong */
	OD_PRUNE,       
	
	/** ignore Patches with suboptimal limit */
	OD_NARROW,
	
	/** use context-sensitive constraints also */
	OD_CONTEXT,     
	
	/** break if new maximum found */
	OD_IMMEDIATE,   
	
	/** only allow attachments to nil or a word one of the affected LVs already attaches to */
	OD_SWAP,        
	
	/** force pJudge to invent LVs, if necessary */
	OD_NONEWLVS,    
}
