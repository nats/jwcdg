package de.unihamburg.informatik.nats.jwcdg.avms;

import java.util.regex.Matcher;

import de.unihamburg.informatik.nats.jwcdg.input.Configuration;

public class AVMNumber extends AVM {
	private static final long serialVersionUID = 8019352896851184368L;
	
	public static final String NUMBER_FORMAT_LOCALE = "numberFormatLocale";
	
	private double number;
	
	private boolean rounded;
	private int integer;
	
	public AVMNumber(double d) {
		this.number  = d;
		this.rounded= false;
	}
	
	public AVMNumber(int i) {
		this.number  = i;
		this.integer = i;
		this.rounded = true;
	}
	
	public double getNumber() {
		return number;
	}
	
	public int getInteger() {
		if(!rounded) {
			this.integer = (int)Math.round(number);
			rounded = true;
		}
		return integer;
	}
	
	@Override
	public String toString() {
		return String.format("%.5f", number);
	}
	
	@Override
	public AVM substituteRefs(Matcher matcher, Configuration config) {
		return this;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(number);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AVMNumber))
			return false;
		AVMNumber other = (AVMNumber) obj;
		if (Double.doubleToLongBits(number) != Double
				.doubleToLongBits(other.number))
			return false;
		return true;
	}
}
