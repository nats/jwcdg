package de.unihamburg.informatik.nats.jwcdg.constraints.terms;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.AccessContext;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;


/**
 * 
 * TTTopPeek,           	       // lexical info from top node: X^cat 
 * TTBottomPeek,        	       // lexical info from bottom node: X@cat 
 * TTLabel,             	       // label of subordination: X.label 
 * TTLevel,             	       // level of subordination: X.level 
 * TTAdd,               	       // addition 
 * TTSubtract,          	       // subtraction 
 * TTMultiply,          	       // multiplication 
 * TTDivide,            	       // division 
 * TTFunction,          	       // function: e.g. distance(X@id, X^id) 
 * TTString,            	       // string constant: 'foo' 
 * TTNumber,            	       // number constant: 0.025 
 * 
 * @author the CDG-Team
 *
 */
public abstract class Term {
	public boolean isContextSensitive;
	
	protected Term() {
		isContextSensitive = false;
	}
	/** 
	 * propagate feature access information through a Term 
	 */
	public void analyze(AccessContext context, Grammar grammar) {
		// default: do nothing!
	}
	
	public void collectFeatures(List<Path> sofar) {
		// default: do nothing
	}

	/**
	 evaluate a CDG Term.

	 @param lg the current lexemgraph
	 @param context the current parse tree (or NULL)
	 @returns an avm, possibly the ErrorValue AVMError

	 This function evaluates a term on a value of the cdg formula language,
	 computing both its type and its value. Both are returned as the result of
	 evalTerm(). The function performs an exhaustive case distinction over the
	 possible values of @c t->type:

	 - TTTopPeek: The field @c t->data.peek.varinfo->levelvalue->regents is
	 checked. If the modifiee is underspecified, ErrorValue is returned,
	 otherwise peekValue() is applied to the path @c t->data.peek.path in the
	 value of the modifiee.\n
	 \n
	 Exceptions occur if @c t->data.peek.path is @c id, @c word, @c from or @c to.
	 These special attributes are notated like features of a lexeme node, but
	 are actually attributes of the lexeme node, and not the lexeme itself.
	 These attributes are computed as follows:
	 - the attribute id is computed by setting @c val->lexemnode
	 to the modifiee itself. If the modifiee is underspecified,
	 ErrorValue is returned, VTLexemNode otherwise.
	 - the attribute word is computed by setting
	 @c val->data.string to the field word of the lexical entry of the
	 modifier. (Strictly speaking, this *is* an attribute of the lexeme
	 rather than the lexeme node; but as it does not appear in the
	 feature matrix, it cannot be computed by peekValue().) If the
	 modifiee is underspecified, ErrorValue is returned, @c val otherwise.
	 - the attribute from is computed by setting @c val->data.number
	 to the field from of the modifiee. If the modifiee is underspecified,
	 @c lg->max+1 is assigned instead. In any case, @c val is returned.
	 - the attribute to is computed by setting
	 @c val->data.number to the field to of the modifiee. If the
	 modifiee is underspecified, the number @c lg->max+2 is assigned
	 instead. In any case, @c val is returned. \n
	 \n
	 - TTBottomPeek: The same algorithm is applied to the
	 modifier of the LevelValue -- simplified by the fact that dependents cannot
	 be underspecified.
	 - TTLabel: @c val->data.string is set to the field label of the LV and @c val is returned.
	 - TTLevel: @c val->data.string is set to the identifier of the Level of LV,
	 and @c val is returned.
	 - TTAdd, TTSubtract, TTMultiply, TTDivide: The two fields
	 @c t->data.operation.op1 and @c t->data.operation.op1 are evaluated by
	 evalTerm(). ErrorValue is returned unless both results are numbers.
	 Dividing by @c 0 also returns ErrorValue. Otherwise the results are
	 conjoined by the corresponding C operator (+, -, * oder /),
	 @c val->data.number is set to the result of this computation, and VTNumber
	 is returned.
	 applied to t->data.function.args (and lg) and both the resulting type
	 and the computed are passed to the caller of evalTerm().
	 - TTString: @c val->data.string is set to @c t->data.string, and @c val is returned.
	 - TTNumber: @c val->data.number is set to @c t->data.number, and @c val is returned.

	 */
	public abstract AVM eval(LexemeGraph lg, Context context, LevelValue[] binding);
	
	/**
	 * check the arity of contained funtions
	 */
	public boolean check() {
		// default: nothing to check, just return true
		return true;
	}
}
