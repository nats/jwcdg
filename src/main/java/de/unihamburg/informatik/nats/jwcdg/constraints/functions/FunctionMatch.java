package de.unihamburg.informatik.nats.jwcdg.constraints.functions;

import java.util.List;
import java.util.ListIterator;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMList;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Hierarchy;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * Returns the number corresponding to the first sort that subsumes 
 * the additional argument.
 *
 * Function assumes well-formedness of VTList argument 
 * (it's enforced in parser.y):
 *
 *   VTString VTNumber VYString VTNumber ...
 */
public class FunctionMatch extends Function {

	public FunctionMatch(Grammar g) {
		super("match", g, 3);
	}

	@Override
	public AVM eval(List<Term> args, Context context, LexemeGraph lg,
			LevelValue[] binding) {
		
		AVM val1= args.get(0).eval(lg, context, binding);
		AVM val2= args.get(1).eval(lg, context, binding);
		AVM val3= args.get(2).eval(lg, context, binding);
		
		if( !(val1 instanceof AVMString) )
			return new AVMError("WARNING: type mismatch (1st arg) for `match'");
		
		if( !(val2 instanceof AVMString) && !(val2 instanceof AVMList) )
			return new AVMError("WARNING: type mismatch (2nd arg) for `match'");
		
		if( !(val3 instanceof AVMString))
			return new AVMError("WARNING: type mismatch (3rd arg) for `match'");

		
		
		/* get hierarchy */
		Hierarchy h = grammar.findHierarchy(((AVMString)val1).getString());
		if (h == null) {
			return new AVMError("WARNING: hierarchy `%s' not found");
		}
		
		String s2 = ((AVMString) val3).getString();

		/* find second sort */
//		if (0 > (b = (int) hashGet(h->types, vb->data.string) - 1)) {
//			cdgPrintf(
//					CDG_WARNING,
//					"WARNING: type `%s' not found in hierarchy `%s' in constraint `%s'\n",
//					vb->data.string, vh->data.string,
//					evalCurrentConstraint ? evalCurrentConstraint->id : "*UNKNOWN*");
//			return ErrorValue;
//		}

		if (val2 instanceof AVMString) {
			/* This is a MATCH( STRING, STRING, STRING) call. */
			String s = ((AVMString) val2).getString();
			
//			if (0 > (a = (int) hashGet(h->types, va->data.string) - 1)) {
//				cdgPrintf(
//						CDG_WARNING,
//						"WARNING: type `%s' not found in hierarchy `%s' in constraint `%s'\n",
//						va->data.string, vh->data.string,
//						evalCurrentConstraint ? evalCurrentConstraint->id
//								: "*UNKNOWN*");
//				return ErrorValue;
//			}
			if( h.subsumes(s, s2) ) {
				return new AVMNumber(1);
			}
		} else {
			/* This is a MATCH( STRING, LIST, STRING) call. */

			/* Loop through list */
			List<AVM> l = ((AVMList) val2).getList();
			ListIterator<AVM> it = l.listIterator();
	
			while( it.hasNext() ) {
//				/* find first sort */
//				if (0 > (a
//						= (int) hashGet(h->types, ((Value) l->item)->data.string)
//								- 1)) {
//					cdgPrintf(
//							CDG_WARNING,
//							"WARNING: type `%s' not found in hierarchy `%s' in constraint `%s'\n",
//							((Value) l->item)->data.string, vh->data.string,
//							evalCurrentConstraint ? evalCurrentConstraint->id
//									: "*UNKNOWN*");
//					return ErrorValue;
//				}
				AVM vala = it.next();
				AVM valb = it.next(); // AVMlists always have an even number of elements
				
				
				if (vala instanceof AVMString) {
					String s = ((AVMString) vala).getString();
					if(h.subsumes(s, s2))
						return valb;
				}
			}
		}
		
		// no subsumption found
		return new AVMNumber(0);
	}


}
