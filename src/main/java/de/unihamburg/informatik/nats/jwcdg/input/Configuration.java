package de.unihamburg.informatik.nats.jwcdg.input;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNet;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.evaluation.CachedGrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.frobbing.AbstractFrobber;
import de.unihamburg.informatik.nats.jwcdg.incrementality.IncrementalFrobber;
import de.unihamburg.informatik.nats.jwcdg.incrementality.virtualNodes.VirtualNodeSpec;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;
import de.unihamburg.informatik.nats.jwcdg.predictors.Tagger;

public class Configuration implements Serializable{

	private static final long serialVersionUID = 1L;
    private static final String JWCDG_DIR_ENV = "JWCDG_DIR";
	private static final String DEFAULT_CONFIG_FILE  = "defaultConfigFile";
	
	private static Logger logger = LogManager.getLogger(Configuration.class);
	
	/**
	 * Expands relative path by the prefix defined 
	 * in the environment variable JWCDG_DIR,
	 * if defined
	 * 
	 * @param path 
	 * @return expanded path
	 */
	public static String fixPath(String path) {
		// absolute path
		if(path.startsWith("/"))
			return path;
		
		String prefix = System.getenv(JWCDG_DIR_ENV);
		
		// not defined
		if(prefix == null) {
			return path;
		}
		
		// make sure it ends with a slash
		if(!prefix.endsWith("/")) {
			prefix+="/";
		}
		
		// relative path
		return prefix+path;
	}
	
	private Map<String, Boolean> flags;
	private Map<String, String > strings;
	private Map<String, Integer> integers;
	private Map<String, Double > doubles;
	private Map<String, List<String>> lists;
	
	private	boolean suppressWarnings;
	
	public static Configuration fromFile(String filename) throws IOException {
		if(System.getenv(JWCDG_DIR_ENV) == null) {
			logger.warn("The environmental variable 'JWCDG_DIR' is not set, some files might not be found!");
		}
		
		Properties settings = new Properties();
		FileInputStream fis1 = new FileInputStream(fixPath(filename));
		settings.load(fis1);
		fis1.close();
		Properties defaultProps = new Properties();
		String defaultConfigFileName = settings.getProperty(DEFAULT_CONFIG_FILE);
		if (defaultConfigFileName == null)
			throw new RuntimeException("No default Config file set! This error could stem from using default.properties directly.");
		FileInputStream fis2= new FileInputStream(fixPath(defaultConfigFileName));
		defaultProps.load(fis2);
		fis2.close();
		for (String key:settings.stringPropertyNames()) {
			if (key.equals(DEFAULT_CONFIG_FILE))
					// this is obviously not specified in the default
					// configuration file
					continue;
			if (!defaultProps.containsKey(key)) {
				logger.error("unknown property "+key+" in your config, aborting.");
				System.exit(1);
			}
		}
		settings = new Properties(defaultProps);
		fis1 = new FileInputStream(fixPath(filename));
		settings.load(fis1);
		fis1.close();
		
		return new Configuration(settings);
	}
	
	
	/**
	 * An empty configuration that will only result in 
	 * empty strings and lists,
	 * all flags are false,
	 * all ints are 0
	 * and all doubles are 1.0
	 * 
	 * This config will give no warning for returning such dafualt values
	 * 
	 */
	public Configuration() {
		flags   = new HashMap<>();
		strings = new HashMap<>();
		integers= new HashMap<>();
		doubles = new HashMap<>();
		lists   = new HashMap<>();
		suppressWarnings = true;
	}
	
	/**
	 * Generate a config from a properties file.
	 * Since the values given by the properties are all strings,
	 * String will be queryable as every type it is parsable to
	 * 
	 * Whenever a key is asked for that was not given in the properties, 
	 * a default value will be rreturned (see the Constructor "Configuration()"),
	 * but a warning will be given via the logger the first time for each key.
	 * 
	 * @param properties a properties file object
	 */
	public Configuration(Properties properties) {
		this();
		suppressWarnings = false;
		
		// why is it not possible to 'foreach' over the properties? *grml* 
		@SuppressWarnings("rawtypes")
		Enumeration en = properties.propertyNames();
		while(en.hasMoreElements()) {
			String key  = (String) en.nextElement();
			
			String sval = properties.getProperty(key).trim();
			
			// we have no idea about the type of the properties
			// the caller of the get functions knows, 
			// but we don't want to burden him with parsing the string
			// therefore we will put the values in all appropriate maps
			
			// first as string without parsing
			setString(key, sval);
			
			// next as list
			// cannot fail, as one-element lists are perfectly legal 
			setList(key, Arrays.asList(sval.split(",")));
			
			// next as double, if parsable
			try{
				int i = Integer.parseInt(sval);
				setInt(key, i);
			} catch(NumberFormatException e) {
			}
			
			
			// same for ints
			try{
				double d = Double.parseDouble(sval);
				setDouble(key, d);
			} catch(NumberFormatException e) {
			}
			
			// flags by special values
			if(sval.equals("true") || sval.equals("on"))
				setFlag(key, true);
			
			if(sval.equals("false") || sval.equals("off"))
				setFlag(key, false);
		}
	}
	
	/**
	 * copy constructor
	 */
	public Configuration(Configuration orig) {
		this();
		
		for(String key : orig.doubles.keySet()) {
			doubles.put(key, orig.doubles.get(key));
		}
		
		for(String key : orig.flags.keySet()) {
			flags.put(key, orig.flags.get(key));
		}
		
		for(String key : orig.integers.keySet()) {
			integers.put(key, orig.integers.get(key));
		}
		
		for(String key : orig.lists.keySet()) {
			lists.put(key, orig.lists.get(key));
		}
		
		for(String key : orig.strings.keySet()) {
			strings.put(key, orig.strings.get(key));
		}
		
		suppressWarnings = orig.suppressWarnings;
	}
	
	public void setFlag(  String key, boolean value) {
		flags.put(key, value);
	}
	
	public void setString(String key, String value) {
		strings.put(key, value);
	}
	
	public void setInt(   String key, int value) {
		integers.put(key, value);
	}
	
	public void setDouble(String key, double value) {
		doubles.put(key, value);
	}
	
	public void setList(String key, List<String> value) {
		lists.put(key, value);
	}
	
	public boolean getFlag(String key) {
		if(!flags.containsKey(key)) {
			if(!suppressWarnings) {
				if(strings.containsKey(key)) {
					logger.warn(String.format("%s = %s none of the allowed values true, false, on or off! Defaulting to false! ", key, strings.get(key)));
				} else {
					logger.warn(String.format("Flag %s not set in configuration, defaulting to false!", key));
				}
			}
			flags.put(key, false);
			return false;
		}
		return flags.get(key);
	}
	
	public String getString(String key) {
		if(!strings.containsKey(key)) {
			if(!suppressWarnings) {
				logger.warn(String.format("String %s not set in configuration, defaulting to \"\"!", key));
			}
			strings.put(key, "");
			return "";
		}
		return strings.get(key);
	}
	
	public int getInt(String key) {
		if(!integers.containsKey(key)) {
			if(!suppressWarnings) {
				if(strings.containsKey(key)) {
					logger.warn(String.format("%s = %s not an integer, defaulting to 0!", key, strings.get(key)));
				} else {
					logger.warn(String.format("integer %s not set in configuration, defaulting to 0!", key));
				}
			}
			integers.put(key, 0);
			return 0;
		}
		return integers.get(key);
	}
	
	public double getDouble(String key) {
		if(!doubles.containsKey(key)) {
			if(!suppressWarnings) {
				if(strings.containsKey(key)) {
					logger.warn(String.format("%s = %s not a number, defaulting to 1.0!", key, strings.get(key)));
				} else {
					logger.warn(String.format("number %s not set in configuration, defaulting to 1.0!", key));
				}
			}
			doubles.put(key, 1.0);
			return 1.0;
		}
		return doubles.get(key);
	}
	
	public List<String> getList(String key) {
		if(!lists.containsKey(key)) {
			if(!suppressWarnings) {
				logger.warn(String.format("list %s not set in configuration, defaulting to the empty list!", key));
			}
			lists.put(key, new ArrayList<>());
		}
		return lists.get(key);
	}
	
	@Override
	public String toString() {
		return String.format("configuration: %s, %s, %s, %s, %s", flags, strings, integers, doubles, lists);
	}
	
	/**
	 * this methods trys to read all expected values from the config
	 * missing (or not properly parsable) entries will result in 
	 * logger warnings
	 */
	public void checkSettings() {
		getFlag(LexemeGraph.COMPACT_LVS);
		getFlag(IncrementalFrobber.FLUCTUATING_SCORE);
		getFlag(CachedGrammarEvaluator.USE_SCORECACHE);
		getFlag(ConstraintNet.USE_VIRTUAL_NODES);
		getFlag(ConstraintNet.USENONSPEC);
		
		getDouble(AbstractFrobber.IGNORETHRESHOLD);
		getFlag(AbstractFrobber.GREEDY);
		getFlag(AbstractFrobber.alwaysShowAllSolutions);
		getFlag(AbstractFrobber.suppressPartialSolutions);
		getFlag(AbstractFrobber.USE_CROSS_LEVEL_HINTS);
		getFlag(AbstractFrobber.BLOCK_NEW_LVS);
		getInt(AbstractFrobber.AGENDASIZE);
		getInt(AbstractFrobber.BEAMWIDTH);
		getInt(AbstractFrobber.MAXSTEPS);
		getInt(AbstractFrobber.TIMELIMIT);
		
		getFlag(Lexicon.DEDUCE_COMPOUNDS);
		getFlag(Lexicon.IGNORE_PARENS);
		getInt(Lexicon.TEMPLATE_MODE);
		
		getString(Tagger.TAGGER_POS_PATH);
		getFlag(Tagger.TAGGER_IGNORE_IMPOSSIBLE);
		getString(Tagger.TAGGER_COMMAND);
		
		getList(Parse.ANNO_FEATS);
		getList(Lexicon.COMPOUND_CATS);
		getList(Lexicon.SPONTANEOUS_UPPCASE_CATS);
		getList(Grammar.DEACTIVATED_CONSTRAINT_CATEGORIES);
		
		getFlag(VirtualNodeSpec.INIT_VNS_UNUSED);
		getFlag(IncrementalFrobber.FLUCTUATING_SCORE);
		getFlag(IncrementalFrobber.MATCH_VIRTUAL_NODES);
		getFlag(IncrementalFrobber.VIRT_FLEXIBLE_LABEL_MATCHING);
		getDouble(IncrementalFrobber.VIRT_MATCHING_FACTOR);
		getFlag(ConstraintNet.USE_EDGEREACTIVATION);
	}
}
