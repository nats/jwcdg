package de.unihamburg.informatik.nats.jwcdg.constraints;

/**
 * direction of an edge
 * i.e., the position of the head compared to the one of the dependent 
 * 
 * @author the CDG-Team
 *
 */
public enum Direction {
	/** to the left \ */
	Left(  0, "\\"),
	/** to the right / */
	Right( 1, "/"),
	/** nowhere | */
	Nil(   2, "|"),
	/** somewhere ! */
	NonNil(3, "!"),
	/** any of the above : */
	AnyDir(4, ":");
	
	public final int    no;
	public final String form;
	
	Direction(int no, String form) {
		this.no   = no;
		this.form = form;
	}
	
	/**
	 * Does this direction subsume the other direction?
	 */
	public boolean subsumes(Direction other) {
		if (this == other)
			return true;
		else if (this == AnyDir)
			return true;
		else if (this == NonNil && (other == Left || other == Right))
			return true;
		else
			return false;
	}
}
