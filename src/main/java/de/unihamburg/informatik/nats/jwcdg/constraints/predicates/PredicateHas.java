package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.Constraint;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.TermPeek;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Hierarchy;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * returns TRUE if the LV has a child edge with the specified label
 * 
Does the LV #1 have a daughter edge with label #2?

`has' checks whether one of the edges below the specified lexeme node
has particular properties.

If there are two arguments, the first is the lexeme node and the second
is a label that must be present.

If the second argument is the name of a unary constraint, its
formula will be used for matching instead of the edges' labels.

If there is a third argument, the third is a hierarchy which should be
used for label matching. Thus, a constraint can say `has(X@id,
OBJA_OBJC, Labels)', and (assuming a fitting `Labels' hierarchy) either
OBJA or OBJC will satisfy the predicate.

If there is a fourth argument, the search is conducted recursively. The
value of the argument is a node in the previously specified hierarchy.
This node subsumes all the labels which are *allowed* on the path
through the tree. So to express that anything which has a relative
pronoun in its scope is a relative clause, you can say

  X@cat = vfin & has(X@id, find_prel, Labels, AUX)
  ->
  X.label = REL

This constraint forces a verb to be a relative clause if one word in its
VP has a relative pronoun as a subordinate, but not if a verb in a
subordinated clause has a relative pronoun, because then at least one
label other than `AUX' will intervene.

If there is a fifth and sixth argument, they must be numbers which then
constrain the range of time points to search. All words that start
outside that exclusive range are not even considered. A value of -1
indicates `no restriction' for the corresponding side.

 */
public class PredicateHas extends Predicate {

	public PredicateHas(Grammar g) {
		super("has", g, true, 2, 6, false);
	}

	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {
		String label = null;
		String hname = null;
		String labels = null;
		Hierarchy h = null;
		int noOfLevels = grammar.getNoOfLevels();
		Constraint c = null;
		Boolean recursive = false;
		int levelno;
		boolean ret = false;
		int min = lg.getMin(), max = lg.getMax();

		/*
		    The fifth and sixth parameter limit the range of the has() loop.
		    Since they can be specified through terms such as X@from, which
		    vary between invocations of the same has() formula, even when the
		    tree does not change, we cannot cache the result of such
		    invocations.
		 */
		boolean useCache = (context != null && args.size() < 5 && formulaID != -1);

		/* if there is no context at all, we can only assume that
		     the edge might be found if we had it. */
		if(context == null) {
			return true;
		}

		/* read lexemeNode */
		Term t = args.get(0);
		TermPeek peek1;
		if (t instanceof TermPeek) {
			peek1 = (TermPeek) t;
		} else {
			throw new PredicateEvaluationException("1st argument of predicate `has' is not a lexeme node access!");
		}
		levelno = binding[peek1.getVarIndex()].getLevel().getNo();
		AVM val1 = t.eval(lg, context, binding);
		if (! (val1 instanceof AVMLexemNode) ) {
			throw new PredicateEvaluationException("Argument of predicate `has' not a lexeme node");
		}
		LexemeNode ln = ((AVMLexemNode)val1).getLexemeNode(); 
		if(!ln.spec()) {
			throw new PredicateEvaluationException(String.format(
					"lexeme node specified by term %s not specified when applied to edge %s",
					t, binding[peek1.getVarIndex()]));
		}

		/* read cache */
		if(useCache) {
			if(context.hasCachedResultFor(formulaID, ln.getArc())) {
				return context.lookupCachedResult(formulaID, ln.getArc());
			}
		}

		/* read second parameter: label */
		AVM val2 = args.get(1).eval(lg, context, binding);
		if(val2 instanceof AVMString) {
			label = ((AVMString)val2).getString();
		} else { /* complain */
			throw new PredicateEvaluationException("type mismatch (2rd arg) for `has'");
		}
		/* if the string is the name of a unary constraint,
		     use this constraint instead of label match  */
		c = grammar.findConstraint(label);
		if(c != null && c.getVars().size() != 1) {
			c = null;
		}

		/* maybe read a third parameter and find Hierarchy to use */
		if(args.size() >=3) {
			AVM val3 = args.get(2).eval(lg, context, binding);
			if (val3 instanceof AVMString) {
				hname = ((AVMString) val3).getString();
			} else {
				throw new PredicateEvaluationException("type mismatch (3rd arg) for `has'");
			}
			h = grammar.findHierarchy(hname);
			if(h == null) {
				throw new PredicateEvaluationException(String.format("hierarchy `%s' not found", hname));
			}
		}

		/* read fourth parameter */
		if(args.size() >= 4) {
			recursive = true;
			AVM val4 = args.get(3).eval(lg, context, binding);
			if(val4 instanceof AVMString) {
				labels = ((AVMString)val4).getString();
			} else {
				throw new PredicateEvaluationException("type mismatch (4th arg) for `has'");
			}
		}

		/* read range parameters */
		if(args.size() >= 5) {
			AVM val5 = args.get(4).eval(lg, context, binding);
			if(val5 instanceof AVMNumber) {
				AVMNumber numval1 = (AVMNumber) val5;
				if(numval1.getNumber() > 0) {
					min = numval1.getInteger();
				}
			} else {
				throw new PredicateEvaluationException("type mismatch (5th arg) for `has'");
			}
		}

		if(args.size() >= 6) {
			AVM val6 = args.get(5).eval(lg, context, binding);
			if(val6 instanceof AVMNumber) {
				AVMNumber numval2 = (AVMNumber) val6;
				if(numval2.getNumber() != -1 && numval2.getNumber() < lg.getMax()) {
					max = numval2.getInteger();
				}
			} else {
				throw new PredicateEvaluationException("type mismatch (6th arg) for `has'");
			}
		}

		/* look for a suitable child edge */
		ret = predHasImpl(ln, lg, label, c, h, labels, recursive, min, max, context, noOfLevels, levelno);

		/* write cache */
		if(useCache) {
			context.cacheResult(formulaID, ln.getArc(), ret);
		}

		return ret;
	}

	/**
	 * 
	 * the actual algorithm traversing the tree
	 */
	private boolean predHasImpl(LexemeNode ln, LexemeGraph lg, String label, Constraint c,
			Hierarchy h, String labels, boolean recursive, int min, int max,
			Context context, int noOfLevels, int levelno) {

		boolean result = false;
		int i;
		LevelValue lv;

		for(i = min; i <= max; i++) {
			int index = noOfLevels * i + levelno;
			if(index >= context.getNoOfLVs()) {
				break;
			}
			lv = context.getLV(index);

			if(lv==null || !lv.getRegentGN().spec()) 
				continue;

			if((!recursive && lv.getRegentGN().getArc().from == ln.getArc().from) ||
					(recursive && descendant(lv, ln, context, 0, h, labels, max))) {

				if(c != null) {
					/* call constraint */
					if(!c.eval(lg, context, lv).isViolated()) {
						result = true;
						break;
					}
				} else {
					/* compare label */
					if(     (h == null && lv.getLabel().equals(label)) ||
							(h != null && h.subsumes(label, lv.getLabel()))) {
						result = true;
						break;
					}
				}
			}
		}
		return result;
	}



	/**
	 * Is the edge LV a direct or indirect descendant of LN?
	 *
	 * Used by predHas() in recursive mode.
	 */
	private boolean descendant(LevelValue lv, LexemeNode ln,
			Context context, int counter, Hierarchy h, String labels, 
			int max) {

		boolean result = false;
		int noOflevels = grammar.getNoOfLevels();
		LevelValue parent;
		/* break out of loops */
		if(counter > max) {
			return false;
		}

		/* answer FALSE for NIL LVs */
		if(!lv.getRegentGN().spec()) {
			return false;
		}

		/* reject wrong labels */
		if(counter > 0 && labels != null && !h.subsumes(labels, lv.getLabel())) {
			return false;
		}

		/* detect trivial success */
		if(lv.getRegentGN().getArc().from == ln.getArc().from) {
			return true;
		}

		/* maybe call self */
		parent = context.getLV(noOflevels * lv.getRegentGN().getArc().from + lv.getLevel().getNo());
		if(parent != null) {
			result = descendant(parent, ln, context, counter+1, h, labels, max);
		}

		return result;

	}

}
