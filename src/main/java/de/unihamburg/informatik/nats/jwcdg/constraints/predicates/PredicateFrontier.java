package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.incrementality.virtualNodes.VirtualLexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * is #1 one of the last #2th nodes (not counting virtual nodes)?
 * 
 * the idea here is, that virtual nodes need conflicts as hints,
 * the frontier predicate is used for suppressing them at the right frontier,
 */
public class PredicateFrontier extends Predicate {

	public static final String USE_PREDICATE_FRONTIER = "usePredicateFrontier";

	public PredicateFrontier(Grammar g) {
		super("frontier", g, false, 2, false);
	}

	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {
		
		if(!lg.getConfig().getFlag(USE_PREDICATE_FRONTIER)) {
			// this predicate is disabled, default to false
			return false;
		}
		
		if(lg.isComplete()) {
			// in complete input, there is no frontier zone where waiting might be the right choice of action
			return false;
		}

		AVM val1 = args.get(0).eval(lg, context, binding);
		if (! (val1 instanceof AVMLexemNode)) {
			throw new PredicateEvaluationException(
					"1st argument of predicate `frontier' not a lexeme node");
		}

		AVM val2 = args.get(1).eval(lg, context, binding);
		if (! (val2 instanceof AVMNumber)) {
			throw new PredicateEvaluationException(
					"2nd argument of predicate `frontier' not a number");
		}

		LexemeNode ln= ((AVMLexemNode)val1).getLexemeNode();
		int n=  ((AVMNumber)val2).getInteger();

		if(!ln.spec() || ln.isVirtual()) {
			return false;
		}

		if(lg instanceof VirtualLexemeGraph) {
			VirtualLexemeGraph vlg = (VirtualLexemeGraph) lg;
			return ln.getArc().to + n > vlg.getVStartId();  // -1 for last nonvirtual, +1 for to instead of from
		} else {
			return ln.getArc().to + n > lg.getMax();
		}
	}

}
