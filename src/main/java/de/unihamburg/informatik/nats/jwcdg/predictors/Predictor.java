package de.unihamburg.informatik.nats.jwcdg.predictors;

import java.util.ArrayList;


public class Predictor {
	
	/**
	 * aggregates a list of keys to a single key
	 * use this for all putting and lookups in Prediction maps
	 */
	public static String makeKey(ArrayList<String> keys) {
		StringBuilder key = new StringBuilder();
		boolean first = true;
		for(String s : keys) {
			if(!first)
				key.append(";");
			else 
				first = false; 
			
			key.append(s);
		}
		
		return key.toString();
	}
	
//	private String name;
//	private Logger logger = Logger.getRootLogger(); 
//	
//	// int predictCall(Predictor p, String special, int slot, LexemGraph lg) {
//	public int call(LexemeGraph lg) {
//
//		int i;
//		int linecount = 0; //counts lines of outout
//		String call;
//		File file1; 
//		File file2; 
//		String name1; // input  to the predictor
//		String name2; // output of the predictor 
//		FileWriter writer; // TODO what about encoding?
//		FileReader reader;
//		LexiconItem favored = null; 
//
//		/* fail on branching lattices */
//		if (lg.getLattice().branches()) {
//			logger.error("ERROR: can't process branching lattices!");
//			return -1;
//		}
//
//		/* open input and output and construct the invocation */
//		try{
//			name1 = tempFileName();
//			writer = new FileWriter(name1);
//			
//			/* write the lattice name as additional information */
//			writer.write(String.format("#sentence: %s\n", lg.getLattice().getId()));
//
//			/* iterate through words of the lattice
//			 and write them to temporary file */
//			for (GraphemeNode gn : lg.getGNs()) {
//				if (gn.isVirtual()) {
//					continue;
//				}
//				Arc a = gn.getArc();
//				
//				/* Words with embedded spaces confuse most programs.
//				 Temporarily replace ' ' with an unprintable control character. */
//				String b = a.word.replace(' ', '\255');
//
//				/* print current word */
//				writer.write(b);
//
//				/* Try to find the preferred lexeme for the grapheme, even if `cat'
//				 is not explicitly requested.
//
//				 The reason for this is to find the most relevant value to print.
//				 For instance, statistical analyzers probably want to normalize
//				 words to their base forms. A predictor might ask for the value of
//				 the feature `base'. But different homonyms can have different
//				 values for that; for instance, the word `gefällt' may either be a
//				 finite verb (VVFIN) with the base form `gefallen', or a participle
//				 (VVPP) with the base form `fällen'. If POS tagging has made the
//				 right decision, this will also disambiguate the base form. */
//				if (-1 != taggerCategoryIndex && NULL
//						!= (cat = gnHighestPrediction(gn))) {
//					for (LexemeNode ln : gn.getLexemes()) {
//						AVM v = ln.getLexeme().getFeature(taggerCategoryIndex);
//						if ((v instanceof AVMString) && cat.equals(((AVMString)v).getString())) {
//							favored = ln->lexem;
//							break;
//						}
//					}
//				}
//
//				if (special == NULL) {
//					/* Print additional information.
//
//					 For instance, a PP attacher might want to know not just the surface
//					 form but also the base form of each word. The List p->input
//					 specifies the set of things that the predictor wants to know. The
//					 token `cat' is special and refers to `the category that was scored
//					 most highly by the POS tagger'. All other tokens are interpreted as
//					 attribute names in the grammar, and the value of the first lexeme
//					 of this grapheme which has that attribute will be printed.
//					 */
//					for (l = p->input; l != NULL; l = l->next) {
//						String value = NULL;
//						String s = l->item;
//						List path = listPrependElement(NULL, s);
//						int index = inputLookupPath(path, inputCurrentGrammar);
//						path = NULL;
//
//						if (!strcmp("cat", s)) {
//
//							/* print predicted POS tag */
//							value = gnHighestPrediction(gn);
//							/* This assumes that only the tagger was yet called! */
//
//						} else if (-1 != index) {
//							List m;
//
//							if (favored) {
//								Value v = favored->values[index];
//								if (v->type == VTString) {
//									value = v->data.string;
//								}
//							} else {
//								for (m = gn->lexemes; m != NULL; m = m->next) {
//									Value v;
//									LexemNode ln = m->item;
//									v = ln->lexem->values[index];
//
//									if (v->type == VTString) {
//										value = v->data.string;
//										break;
//									}
//								}
//							}
//						}
//
//						if (value) {
//							fprintf(input, "\t%s", value);
//						} else {
//							fprintf(input, "\t-");
//						}
//					}
//				} else {
//					linecount++;
//
//					// print Position in sentence, beginning with one
//					fprintf(input, "\t%i", i + 1);
//					predictPrintLexiconInfo(favored, input);
//
//					List lexemes;
//					for (lexemes = gn->lexemes; lexemes != NULL; lexemes
//							= lexemes->next) {
//						LexemNode ln = (LexemNode) lexemes->item;
//						LexiconItem li = (LexiconItem) ln->lexem;
//
//						//favored lexem is ignored because it was already written before
//						if (li != favored) {
//							/* print current word */
//							fprintf(input, "%s", b);
//							linecount++;
//
//							// print Position in sentence, beginning with one
//							fprintf(input, "\t%i", i + 1);
//
//							predictPrintLexiconInfo(li, input);
//						}
//					}
//				}
//
//				writer.write("\n");
//			}
//			writer.close();
//
//			
//		} catch (IOException e) {
//			logger.error(String.format(
//					"ERROR: Can't write input file `%s'!\nAborting call of %s!",
//					name1, this.name));
//		}
//
//		outdescriptor = mkstemp(outputname);
//		if (NULL == (output = fdopen(outdescriptor, "r"))) {
//			cdgPrintf(CDG_ERROR, "ERROR: Can't read output file!\n");
//			unlink(inputname);
//			close(indescriptor);
//			return -1;
//		}
//
//
//
//		/* call the program */
//		cdgPrintf(CDG_INFO, "INFO: calling predictor `%s'\n", p->name);
//		if (special == NULL) {
//			call = strPrintf("%s < %s > %s", p->command, inputname, outputname);
//		} else {
//			call = strPrintf("%s %s %s", p->command, inputname, outputname);
//		}
//		if (system(call)) {
//			cdgPrintf(CDG_ERROR, "ERROR: Can't call predictor `%s'!\n", p->name);
//			unlink(inputname);
//			unlink(outputname);
//			close(indescriptor);
//			close(outdescriptor);
//			return -1;
//		}
//
//		/* go through the response and record predictions */
//		if (special == NULL || !strcmp(special, "-ppc3")) {
//			//cdgPrintf(CDG_INFO, "CHB  predcit.c  predictSaveAsNormal  \n");
//			//-ppc3: record predictions as if ppc is a standard predictor
//			//this is important to recor slot -to homonym predictions
//			predictSaveAsNormal(output, slot, lg);
//		} else {
//			//cdgPrintf(CDG_INFO, "CHB  predcit.c  predictSaveAsSpecial  \n");
//			//record predictions in the special ppc way
//			predictSaveAsSpecial(output, special, lg, linecount);
//		}
//
//		unlink(inputname);
//		unlink(outputname);
//		close(indescriptor);
//		close(outdescriptor);
//
//		return 0;
//	}

}
