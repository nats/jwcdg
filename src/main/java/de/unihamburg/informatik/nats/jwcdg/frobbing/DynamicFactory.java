package de.unihamburg.informatik.nats.jwcdg.frobbing;

import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;

public class DynamicFactory extends FrobbingFactory {
	public static final String NAME = "dynamic";

	public DynamicFactory(Grammar grammar, Lexicon lex, Configuration config) {
		super(grammar, lex, config, FrobbingMethod.Dynamic);
	}
}
