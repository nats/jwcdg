package de.unihamburg.informatik.nats.jwcdg.constraints.terms;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraints.AccessContext;
import de.unihamburg.informatik.nats.jwcdg.constraints.VarInfo;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;

/**
 * superclass of all terms evaluating to a feature (or pseudofeature)
 * of a lexemenode
 * 
 * @author the CDG-Team
 */
public abstract class TermPeek extends Term {
	protected Path path;
	protected final VarInfo vi;
	protected final int vindex;
	
	public TermPeek(Path path, VarInfo vi) {
		this.path = path;
		this.vi   = vi;
		this.vindex = vi.index;
	}
	
	@Override
	public void analyze(AccessContext context, Grammar grammar) {
		
		if(path.isPseudoFeature()) {
			return;
		}

		/* what edge types can the variable be bound to? */
		ArrayList<Boolean> allowed = context.get(vindex);

		for(int i = 0; i < grammar.etMax(); i++) {
			if(allowed.get(i)) {
				/* distinguish modifier and modifiee accesses */
				Set<Path> features = selectFeatureSet(i, grammar);

				/* set appropriate relevance bit */
				features.add(path);
			}
		}
	}
	
	protected abstract Set<Path> selectFeatureSet(int i, Grammar g);

	public int getVarIndex() {
		return vindex;
	}
	
	public Path getPath() {
		return path;
	}
	
	@Override
	public void collectFeatures(List<Path> sofar) {
		if(!path.isPseudoFeature() && !sofar.contains(path)) 
			sofar.add(path);
	}
	
	@Override
	public String toString() {
		return vi.varname+sign()+path.toString();
	}
	
	protected abstract String sign();
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;

		if(!(obj instanceof TermPeek))
			return false;
		
		if(this.getClass() != obj.getClass())
			return false;
		
		TermPeek other = (TermPeek)obj;
		
		return (this.path.equals(other.path) && vi.equals(other.vi));
	}
}
