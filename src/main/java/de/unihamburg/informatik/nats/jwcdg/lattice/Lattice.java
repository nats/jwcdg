package de.unihamburg.informatik.nats.jwcdg.lattice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.input.SourceInfo;

public class Lattice implements Serializable{
	
    private static final long serialVersionUID = 1L;

    private static int counter = 0; 

	/** identification */
	private String id;
	/** list of arcs */
	private List<Arc> arcs;
	/** list of arcs, only for optional tagger lookahead in incremental parsing */
	//private List<Arc> lookahead;

	private SourceInfo source;


	/**
	 *  from a list of arcs 
	 */ 
	public Lattice(String id, List<Arc> arcs, SourceInfo source) {
		this.id = id;
		this.arcs = arcs;
		this.source = source;
		//lookahead = null;
	}

	/**
	 * from a list of tokens
	 * 
	 * @param tokens
	 * @param source
	 */
	public Lattice(List<String> tokens) {
		this.id = "wordgraph"+ counter++;
		this.arcs = new ArrayList<>(tokens.size());
		this.source = new SourceInfo();
		//lookahead = null;

		int i = 0;
		for(String t : tokens) {
			arcs.add(new Arc(i, i+1, i, t, 0.0));
			i++;
		}
	}

	public String getId() {
		return id;
	}

	public List<Arc> getArcs() {
		return arcs;
	}

	/**
	 * Return the Arc that precedes A in LAT, or NULL.
	 * Finds only the first predecessor if several exist.
	 */
	public Arc prevArc(Arc arc) {
		for(Arc result: arcs) {
			if(result.to == arc.from) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Return the Arc that directly succeeds A in LAT, or NULL.
	 * Finds only the first one if several exist.
	 */
	public Arc nextArc(Arc arc) {
		for(Arc result: arcs) {
			if(result.from == arc.to) {
				return result;
			}
		}
		return null;
	}

	/**
	 * tells whether there are different arcs for the same timepoint or not
	 * 
	 * @return
	 */
	public boolean branches() {
		//TODO #ASR
		return false;
	}


	@Override
	public String toString() {
		return String.format("Lattice (%d arcs): %s", arcs.size(), arcs);
	}

	public SourceInfo getSource() {
		return source;
	}

	/**
	   Set the fields quotes, parens, and phraseQuotes in each Arc.

	   The field parens indicates within how many levels of parentheses a word
	   stands. Thus, in the sentence

	     We have (we believe) ample evidence.

	   the words `we' and `believe' would receive a value of 1, and all other
	   words, 0. This information can be retrieved in a constraint with the
	   predicate parens(). For instance, a constraint might say that it is okay
	   to have multiple sentence roots if they are on different levels of
	   parenthesis nesting, but not otherwise.

	   The field quotes counts the number of quotation marks that a word is
	   in. In the sentence

	     "Thus", said Gandalf, "passes Denethor."

	   the words `said' and `Gandalf' will receive values of 0, and all other
	   words, 1. The information can be retrieved in a constraint with the
	   predicate quotes(). For instance, it might say that a matrix clause must
	   not have a higher value that its object clause.

	   The field phrasequotes is a variant of quotes that only considers
	   quotations that are not also preceded with commas. For instance, in the
	   previous example it would remain 0 for all words. It would assign a
	   value of 1 to the word `shall' in the sentence

	     Mark you his absolute "shall"?

	   This information can be retrieved in a constraint with the predicate
	   phrasequotes(). For instance, it could say that words of other classes
	   may function as nouns, i.e. take determiners, be subjects etc., if they
	   are phrase-quoted.


	   All of this information is limited to what can be detected within one
	   lattice. For instance, if in a long paragraph, several sentences are
	   contained within one pair of parentheses, then all but the first will
	   have no trace of that and carry parens values of 0.

	 */
	public void scan(boolean complete) {
		int p = 0;
		int q = 0;
		int pq = 0;
		boolean apo = false;
		Arc last, next;
		int minparen = 0;
		int minquote = 0;
		int noOfhyphens = 0;
		int noOfApostrophes = 0;
		int noOfQuotes = 0;

		if(this.branches()) {
			return;
		}


		/* First do some global counts,
	     because one ' is very different form two ' in the same sentence.  */
		for(Arc a: arcs) {
			switch (a.word) {
				case "-":
					noOfhyphens++;
					break;
				case "'":
					noOfApostrophes++;
					break;
				case "\"":
					noOfQuotes++;
					break;
			}
		}

		last = null;
		for(Arc a: arcs) {
			next = nextArc(a);

			a.setParens(p);
			a.setQuotes(q);
			a.setPhraseQuotes(pq);

			/* parenthesis are unambiguous */
			if(a.word.equals("(") || a.word.equals("[")) {
				p++;
			}
			if(a.word.equals(")") || a.word.equals("]")) {
				p--;
				if(p < minparen) {
					minparen = p;
				}
			}

			/* But quotation marks are more difficult when direct speech is
		       distributed across sentences, so that the entire lattice has only
		       one quote.

		       At the start or end of a sentence it is clear that they start or end
		       a quotation. But the first or last part may also be accompanied by a
		       speech attribution. In that case we investigate the context on
		       either side.

		       Isolated closing quotes tend to be preceded by sentence-final
		       punctuation:

		          Die Würfel sind gefallen!" sagte er.
		          Wo bi-" begann er.

		       or followed by commas:

		          "Komm her, Junge! Das ist doch keine Gesellschaft für dich",
		          sagte er.

		       while isolated opening quotes tend to be preceded by commas or
		       semicolons:

		         Er hub an, "Ich will Euch eine lange und wunderbare Geschichte
		         erzählen. Als ich im letzten Sommer...

		       (Note that this last criterion is German-specific and fails badly in
		       English, which has the opposite convention.)

			 */
			if(a.word.equals("\"")) {

				/* sentence-initial quote */
				if(last==null) {
					q++;
					if(noOfQuotes > 1 || !complete) {
						pq++;
					}
				}

				/* sentence-final quote */
				else if(next== null) {
					q--;
					pq = 0;
					if(q < minquote) {
						minquote = q;
					}
				}


				/* second quote */
				else if(1 == q) {
					q--;
					pq = 0;
					if(q < minquote) {
						minquote = q;
					}
				}

				/* hard case, look at the context */
				else if(last.word.contains("!") || last.word.contains("!")) {
					q--;
					if(q < minquote) {
						minquote = q;
					}
				}

				else if(next.word.contains(",")) {
					q--;
					if(q < minquote) {
						minquote = q;
					}
				}
				else if(last.word.contains(":") || last.word.contains(",")) {
					q++;
				}
				else {
					q++;
					if(noOfQuotes > 1 || !complete) {
						pq++;
					}
				}
			}

			/* an isolated apostrophe is usually a mathematical symbol or a
			   mis-tokenized morpheme. But if two occur, they should probably be
			   treated as quotation marks. */
			if(noOfApostrophes > 1 && a.word.equals("'")) { // TODO how should we handle single ' in incomplete sentences?
				apo = !apo;
				if(apo) {
					if(last!= null && (last.word.contains(":") || last.word.contains(","))) {
						q++;
					} else {
						q++;
						//if(nrapostrophes > 1 || !complete) { // not needed as it is part in the surrounding if
						pq++;
						//}
					}
				} else {
					q--;
					//if(nrapostrophes > 1 ) {
					pq--;
					//}
				}
			}

			last = a;
		}

		/* If a sentence contains one hyphen, it is merely a more marked version
		     of a comma - like this. But if it contains more than one hyphen - that
		     is, if it looks like this - it is very likely that they are intended
		     to imply bracketing, and we should count them as parentheses.

		     There are, of course, people who use more than one non-bracketing
		     hyphen in a sentence, and their sentences will be mis-scanned -
		     terrible thing, really - something ought to be done about it.  */
		if(noOfhyphens > 1) {
			boolean hyph = false;
			for(Arc a: arcs) {
				if(a.word.equals("-")) {
					hyph = !hyph;
				} else if(hyph) {
					a.setParens(a.getParens()+1);
				}
			}
		}


		/* If we found an isolated closing quote or parenthesis,
		     then some values may be below 0. That is no problem as long as we only
		     compare quotelevels against each other, but if a constraint tests the
		     quote level of a word against a constant the results become
		     inconsistent. Therefore we adjust the level so that no value is
		     negative. */
		for(Arc a: arcs) {
			if(minquote < 0) {
				a.setQuotes(a.getQuotes() - minquote);
			}
			if(minparen < 0) {
				a.setParens(a.getParens() - minparen);
			}
		}
	}

	public void addArc(Arc arc) {
		arcs.add(arc);
	}
}
