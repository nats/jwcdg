package de.unihamburg.informatik.nats.jwcdg.gui;

import de.unihamburg.informatik.nats.jwcdg.constraints.predicates.PredicatePartial;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;
import java.util.HashMap;

/**
 * The UI component of the DepTreeViewer tool. Doesn't include the interactions. The concrete
 * classes are either the DepTreeDisplay or the AnnoDisplay
 *
 * @author Sven Zimmer, Arne Köhn and Christine Köhn
 */

abstract class AbstractDisplay extends JFrame {
	private static final long serialVersionUID = 1L;

	static final String TAGS_BOX = "tagsBox";
	static final String REF_BOX = "refBox";
	static final String ROOT_BOX = "rootBox";
	static final String TRANSPARENT_BOX = "transBox";
	static final String DROPLEAVES_BOX = "dropBox";
	static final String PARSE_BOX = "parseBox";
	static final String Y_CURRENT_FIELD = "yCurrentField";
	static final String Y_NONCURRENT_FIELD = "yNoncurrentField";
	static final String FONT_FIELD = "fontField";
	static final String CLEAR_TEXTFIELD_BUTTON = "clearButton";
	static final String PARSE_BUTTON = "parseButton";
	static final String LEVEL_BUTTON = "levelButton";
	static final String PRINT_BUTTON = "printButton";
	static final String SAVE_PREFS_BUTTON = "savePrefsButton";
	static final String APPLY_PREFS_BUTTON = "applyPrefsButton";
	static final String CANCEL_PREFS_BUTTON = "cancelPrefsButton";
	static final String DONE_BUTTON = "doneButton";
	static final String NOT_DONE_BUTTON = "notDoneButton";
	static final String SAVE_BUTTON = "SaveButton";
	static final String TREE_BUTTON = "TreeButton";
	static final String MENU_ITEM = "MenuItem";
	static final String NODES_PANE = "NodesPane";
	static final String PARTIAL_CHECKBOX = "partialCheckBox";

	protected Configuration config;
	protected JTree _navTree;

	public PreferencesDisplay _prefD;

	protected JSplitPane _horSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
	protected JSplitPane _vertSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
	protected JScrollPane _navTreeScrollPane = new JScrollPane(
			_navTree,
			JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
			JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

	protected JPanel _buttonPanel = new JPanel();
	protected JPanel _buttonPanel2 = new JPanel();
	protected JPanel _depTreePanel = new JPanel();
	protected JPanel _panel2 = new JPanel();
	protected JPanel _vertSplitPanel = new JPanel();
	public JSplitPane _constraintsSplitPane;

	protected HashMap<String, JButton> _buttons = new HashMap<>();
	protected HashMap<String, JCheckBox> _boxes = new HashMap<>();
	protected HashMap<String, JTextField> _fields = new HashMap<>();

	protected JButton _prevButton;
	protected JButton _currentButton;
	protected JButton _nextButton;
	//	protected JButton _prevDoneButton;
//	protected JButton _nextDoneButton;
	protected JButton _saveSVGButton;
	protected JButton _loadCDAButton;
	protected JTextField _parseField;
	protected JButton _saveCDAButton;
	protected JLabel _scoreLabel;
	protected JLabel _scoreValueLabel;

	private JMenu _fileMenu;

	public JScrollPane _constraintsScrollPane;

	private JMenuItem _prefItem;
	private JMenuItem _helpItem;
	private JMenuItem _saveItem;
	private JMenuItem _saveAsItem;
	private JMenuItem _saveAllItem;
	private JMenuItem _openItem;
	private JMenuItem _exportItem;
	private JMenuItem _quitItem;

	protected JPopupMenu _popup = new JPopupMenu();
	protected JTable _constraintsTable = new JTable();

	protected JPanel _constraintsPanel;

	protected JPanel _constraintSearchPanel;
	protected JLabel _constraintSearchLabel;
	protected JTextField _constraintSearchInput;
	protected JButton _constraintSearchButton;

	public JTextArea _constraintsArea = new JTextArea();
	public JTextArea _helpArea = new JTextArea();

	public JScrollPane _constraintsFieldScrollPane;

	public JFrame _HelpTextFrame = new JFrame();

	protected AbstractDisplay(Configuration config) {
		this.config = config;
	}

	protected void initButtons() {
		_prevButton = new JButton("<");
		_currentButton = new JButton("Select current");
		_nextButton = new JButton(">");
//	    _prevDoneButton = new JButton("<<");
//	    _nextDoneButton = new JButton(">>");
		_saveSVGButton = new JButton("Save SVG");
		_saveCDAButton = new JButton("Save CDA");
		_loadCDAButton = new JButton("Load CDA");
		setParseField(new JTextField());
//	    setClearButton(new JButton("Clear textfield"));
//	    setParseButton(new JButton("Parse"));
//	    setLevelButton(new JButton("SYN"));
//	    setPrintButton(new JButton("Print actual parse on console"));
		setButton(CLEAR_TEXTFIELD_BUTTON, new JButton("Clear textfield"));
		setButton(PARSE_BUTTON, new JButton("Parse"));
		setButton(PRINT_BUTTON, new JButton("Print actual parse on console"));
		setButton(LEVEL_BUTTON, new JButton("SYN"));
		_scoreLabel = new JLabel("Score:");
		_scoreValueLabel = new JLabel();

		_constraintSearchButton = new JButton("Search");
		_constraintSearchInput = new JTextField();
		_constraintSearchLabel = new JLabel("Constraint:");


	}

	protected void initElements(boolean hasInteractions) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		if (hasInteractions) {
			JMenuBar menuBar = new JMenuBar();

			setFileMenu(new JMenu("File"));
			setSaveItem(new JMenuItem("Save"));
			setSaveAsItem(new JMenuItem("Save As..."));
			setOpenItem(new JMenuItem("Open ..."));
			setExportItem(new JMenuItem("Export as SVG..."));
			setSaveAllItem(new JMenuItem("Save all displayed"));
			setQuitItem(new JMenuItem("Quit"));

			JMenu editMenu = new JMenu("Edit");
			editMenu.setName("edit menu");
			setPrefItem(new JMenuItem("Preferences"));
			editMenu.add(getPrefItem());

			JMenu helpMenu = new JMenu("Help");
			helpMenu.setName("help menu");
			setHelpItem(new JMenuItem("Help"));
			helpMenu.add(getHelpItem());


			menuBar.add(getFileMenu());
			menuBar.add(editMenu);
			menuBar.add(helpMenu);
			this.setJMenuBar(menuBar);

			DefaultMutableTreeNode top = new DefaultMutableTreeNode("root");
			_navTree = new JTree(top);
			_navTree.setToggleClickCount(1);

			// TODO: move to subclass
			setBox(PARTIAL_CHECKBOX, new JCheckBox("partial",
					config.getFlag(PredicatePartial.USE_PREDICATE_PARTIAL)));

			JScrollPane buttonScrollPane = new JScrollPane(_buttonPanel,
					JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
					JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			buttonScrollPane.setMinimumSize(new Dimension(_buttonPanel.getWidth() + 100, _buttonPanel.getHeight() + 100));
			BorderLayout border = new BorderLayout();
			_buttonPanel2.setLayout(border);
			_buttonPanel2.add(buttonScrollPane, BorderLayout.CENTER);

			_constraintsTable.setName("test name");
			_constraintsTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			_constraintsSplitPane = new JSplitPane();
			_constraintsScrollPane = new JScrollPane(_constraintsTable);
			_constraintsScrollPane.setPreferredSize(new Dimension(100, 400));
			_constraintsSplitPane.setLeftComponent(_constraintsScrollPane);
			_constraintsFieldScrollPane = new JScrollPane(_constraintsArea);
			_constraintsSplitPane.setRightComponent(_constraintsFieldScrollPane);
			_constraintsArea.setEditable(false);
			_constraintsSplitPane.setDividerLocation(1000);

			_constraintSearchPanel = new JPanel();
			_constraintSearchPanel.add(_constraintSearchLabel);
			_constraintSearchPanel.add(_constraintSearchInput);
			_constraintSearchPanel.add(_constraintSearchButton);

			JPanel scorePanel = new JPanel();
			scorePanel.add(_scoreLabel);
			scorePanel.add(_scoreValueLabel);

			_constraintsPanel = new JPanel();
			_constraintsPanel.setLayout(new BorderLayout());
			_constraintsPanel.add(scorePanel, BorderLayout.NORTH);
			_constraintsPanel.add(_constraintsSplitPane, BorderLayout.CENTER);
			_constraintsPanel.add(_constraintSearchPanel, BorderLayout.SOUTH);

			_helpArea.setEditable(false);
			JScrollPane helpScrollPane = new JScrollPane(_helpArea);
			_HelpTextFrame.add(helpScrollPane);
			_helpArea.setLineWrap(true);
			_helpArea.setWrapStyleWord(true);


			//		    _buttonPanel2.add(_constraintSearchPanel,BorderLayout.NORTH);
//
//		    border = new BorderLayout();
//		    getContentPane().setLayout(border);
//		    getContentPane().add(_depTreePanel, BorderLayout.WEST);
//		    getContentPane().add(buttonPanel2, BorderLayout.SOUTH);

			getContentPane().setLayout(new BorderLayout());
//		    _navTreeScrollPane.setPreferredSize(new Dimension(300, 300));
			_horSplitPane.setLeftComponent(_navTreeScrollPane);
			_horSplitPane.setDividerLocation(200);
			_vertSplitPanel.setLayout(new BorderLayout());
			_vertSplitPanel.add(_vertSplitPane, BorderLayout.CENTER);
			_horSplitPane.setRightComponent(_vertSplitPanel);
			_panel2.setLayout(new BorderLayout());
			_panel2.add(_depTreePanel, BorderLayout.CENTER);
			_vertSplitPane.setLeftComponent(_panel2);
			_vertSplitPane.setRightComponent(null);
			_vertSplitPane.setDividerLocation(0.5);
			getContentPane().add(_horSplitPane, BorderLayout.CENTER);
			getContentPane().add(_buttonPanel2, BorderLayout.SOUTH);
			enableSaveItems(false);
		} else {
			getContentPane().add(_depTreePanel);
		}
	}

	public void setNavTree(JTree tree) {
		_navTree = tree;
		_navTreeScrollPane = new JScrollPane(
				_navTree,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		_horSplitPane.setLeftComponent(_navTreeScrollPane);
		Dimension size = new Dimension(100, 100);
		_navTreeScrollPane.setMinimumSize(size);
	}

	abstract class PreferencesDisplay extends JFrame {
		private static final long serialVersionUID = 1L;

		protected JPanel _prefPanel = new JPanel();

		PreferencesDisplay() {

			setButton(SAVE_PREFS_BUTTON, new JButton("Save"));
			setButton(APPLY_PREFS_BUTTON, new JButton("Apply"));
			setButton(CANCEL_PREFS_BUTTON, new JButton("Cancel"));

			setTitle("Preferences");
			Container pane = getContentPane();
			pane.setLayout(new BorderLayout());
			Container panel = new JPanel();
			panel.setLayout(new BorderLayout());
			_prefPanel.setLayout(new GridLayout(0, 1));
			panel.add(_prefPanel, BorderLayout.WEST);
			pane.add(panel, BorderLayout.NORTH);

			makeBox(TAGS_BOX, "Tags (cat)");
			makeBox(REF_BOX, "Referents");
			makeBox(ROOT_BOX, "Draw roots");
			makeBox(TRANSPARENT_BOX, "Draw transparent boxes behind labels");
			makeBox(DROPLEAVES_BOX, "Draw most lowest nodes at bottom");

			addSpecialPrefs();

			makeField(Y_CURRENT_FIELD, "Height lines in tree:", 4);
			makeField(Y_NONCURRENT_FIELD, "Height lines below tree:", 4);
			makeField(FONT_FIELD, "Fontsize:", 3);

			addComp(getButton(SAVE_PREFS_BUTTON), getButton(CANCEL_PREFS_BUTTON), getButton(APPLY_PREFS_BUTTON));
			Dimension newSize = pane.getPreferredSize();
			newSize = new Dimension(newSize.width, newSize.height + 30);
			_prefPanel.setMinimumSize(newSize);
			setSize(newSize);
		}

		/**
		 * Generates a check box with a name and a label without a name and puts them next to each
		 * other into the PreferencesDisplay
		 *
		 * @param name      the name of the box, taken from the static variables in DepTree
		 * @param labelText the text of the label
		 */
		protected void makeBox(String name, String labelText) {
			JCheckBox box = new JCheckBox();
			setBox(name, box);
			JLabel label = new JLabel(labelText);
			addComp(box, label);
		}

		/**
		 * Generates a text field with a name and a label without a name and puts them next to each
		 * other into the PreferencesDisplay
		 *
		 * @param name      the name of the field, taken from the static variables in DepTree
		 * @param labelText the text of the label
		 * @param size      the size of the field in characters
		 */
		private void makeField(String name, String labelText, int size) {
			JTextField field = new JTextField("", size);
			setField(name, field);
			JLabel label = new JLabel(labelText);
			addComp(field, label);
		}

//		protected void makeButton(String name, String text) {
//			JButton box = new JButton(text);
//			setButton(name, box);
//		}

		/**
		 * Generates a panel, adds the components to it so they will be next to each other and adds
		 * it to the preferences panel
		 *
		 * @param components the components to be added next to each other
		 */
		protected void addComp(Component... components) {
			JPanel panel = new JPanel();
			for (Component c : components) {
				panel.add(c);
			}
			JPanel panel2 = new JPanel();
			panel2.setLayout(new BorderLayout());
			panel2.add(panel, BorderLayout.WEST);
			_prefPanel.add(panel2);
		}

		/**
		 * for adding unique preferences to the DepTreeDisplay or the AnnoDisplay
		 */
		abstract void addSpecialPrefs();

//		public void refreshValues(){
//		}

//		protected JButton getApplyButton() {
//			return _applyButton;
//		}
//
//		protected void setApplyButton(JButton applyButton) {
//			_applyButton = applyButton;
//			_applyButton.setName("applypref button");
//		}
//
//		protected JButton getCancelButton() {
//			return _cancelButton;
//		}
//
//		protected void setCancelButton(JButton cancelButton) {
//			_cancelButton = cancelButton;
//			_cancelButton.setName("cancelpref button");
//		}

//		protected JCheckBox getTagsBox() {
//			return _tagsBox;
//		}
//
//		protected void setTagsBox(JCheckBox tagsBox) {
//			_tagsBox = tagsBox;
//			_tagsBox.setName("tags box");
//		}

		/**
		 * retrieves a check box by its unique name from its map Use instead of individual getters
		 *
		 * @param name the name of the box, taken from the static variables in DepTree
		 * @return the box of the preferences menu
		 */
		public JCheckBox getBox(String name) {
			return _boxes.get(name);
		}

		/**
		 * retrieves a text field by its unique name from its map Use instead of individual getters
		 *
		 * @param name the name of the field, taken from the static variables in DepTree
		 * @return the field of the preferences menu
		 */
		public JTextField getField(String name) {
			return _fields.get(name);
		}
	}

	/**
	 * retrieves a check box by its unique name from its map Use instead of individual getters
	 *
	 * @param name the name of the box, taken from the static variables in DepTree
	 * @return the box of the preferences menu
	 */
	public JCheckBox getBox(String name) {
		return _prefD.getBox(name);
	}

	/**
	 * Gives a check box an unique name, then links the name to the box in a map Use instead of
	 * individual setters
	 *
	 * @param name the name of the box, taken from the static variables in DepTree
	 * @param box  the box of the preferences menu
	 */
	public void setBox(String name, JCheckBox box) {
		box.setName(name);
		_boxes.put(name, box);
	}

	/**
	 * retrieves a text field by its unique name from its map Use instead of individual getters
	 *
	 * @param name the name of the field, taken from the static variables in DepTree
	 * @return the field of the preferences menu
	 */
	public JTextField getField(String name) {
		return _prefD.getField(name);
	}

	/**
	 * Gives a text field an unique name, then links the name to the field in a map Use instead of
	 * individual setters
	 *
	 * @param name  the name of the field, taken from the static variables in DepTree
	 * @param field the field of the preferences menu
	 */
	private void setField(String name, JTextField field) {
		field.setName(name);
		_fields.put(name, field);
	}

	/**
	 * retrieves a button by its unique name from its map Use instead of individual getters
	 *
	 * @param name the name of the button, taken from the static variables in DepTree
	 * @return the button of any menu
	 */
	public JButton getButton(String name) {
		return _buttons.get(name);
	}

	/**
	 * Gives a button an unique name, then links the name to the button in a map Use instead of
	 * individual setters
	 *
	 * @param name   the name of the button, taken from the static variables in DepTree
	 * @param button the button of any menu
	 */
	private void setButton(String name, JButton button) {
		button.setName(name);
		_buttons.put(name, button);
	}

	public void showPreferences() {
		_prefD.setVisible(true);
	}

	public void hidePreferences() {
		_prefD.setVisible(false);
	}

	public JTree getNavTree() {
		return _navTree;
	}

	public JTable getConstraintsTable() {
		return _constraintsTable;
	}

	public void setConstraintsTable(JTable constraintsTable) {
		_constraintsTable = constraintsTable;
	}

	public void setPopup(JPopupMenu popup) {
		_popup = popup;
	}

	public JPopupMenu getPopup() {
		return _popup;
	}

	public void setParseField(JTextField parseField) {
		_parseField = parseField;
		_parseField.setName("parse field");
	}

	public JTextField getParseField() {
		return _parseField;
	}

//	public void setSaveSVGButton(JButton saveButton) {
//		_saveSVGButton = saveButton;
//	}
//
//	public JButton getSaveSVGButton() {
//		return _saveSVGButton;
//	}
//	
//	public void setSaveCDAButton(JButton saveCDAButton) {
//		_saveCDAButton = saveCDAButton;
//	}
//
//	public JButton getSaveCDAButton() {
//		return _saveCDAButton;
//	}
//
//	public void setLoadCDAButton(JButton loadCDAButton) {
//		_loadCDAButton = loadCDAButton;
//	}
//
//	public JButton getLoadCDAButton() {
//		return _loadCDAButton;
//	}
//
//	public void setPrintButton(JButton printButton) {
//		_printButton = printButton;
//		_printButton.setName("print button");
//	}

//	public JButton getPrintButton() {
//		return _printButton;
//	}
//
//	public JButton getClearButton() {
//		return _clearButton;
//	}
//	
//	public JButton getParseButton() {
//		return _parseButton;
//	}

//	protected void setParseButton(JButton parseButton) {
//		_parseButton = parseButton;
//		_parseButton.setName("parse button");
//	}

	public JButton getConstraintButton() {
		return _constraintSearchButton;
	}

	public JTextField getConstraintInput() {
		return _constraintSearchInput;
	}

	public JLabel getScoreValueLabel() {
		return _scoreValueLabel;
	}

	public void setScoreValueLabel(JLabel scoreValueLabel) {
		_scoreValueLabel = scoreValueLabel;
	}

	protected JMenu getFileMenu() {
		return _fileMenu;
	}

	protected void setFileMenu(JMenu fileMenu) {
		_fileMenu = fileMenu;
		_fileMenu.setName("file menu");
	}

	protected JMenuItem getSaveAsItem() {
		return _saveAsItem;
	}

	protected void setSaveAsItem(JMenuItem saveAsItem) {
		_saveAsItem = saveAsItem;
		_saveAsItem.setEnabled(false);
		_saveAsItem.setName("saveas item");
	}

	protected JMenuItem getSaveItem() {
		return _saveItem;
	}

	protected void setSaveItem(JMenuItem saveItem) {
		_saveItem = saveItem;
		_saveItem.setEnabled(false);
		_saveItem.setName("save item");
	}

	protected JMenuItem getExportItem() {
		return _exportItem;
	}

	protected void setExportItem(JMenuItem exportItem) {
		_exportItem = exportItem;
		_exportItem.setEnabled(false);
		_exportItem.setName("exportsvg item");
	}

	/**
	 * Enables the items saveAs and export in the files menu if enable is true. To be used when the
	 * first DepTree is created. Disables the items save,  saveAs and export in the files menu if
	 * enable is false. To be used when all DepTrees are removed.
	 */
	protected void enableSaveItems(boolean enable) {
		_saveAsItem.setEnabled(enable);
		_exportItem.setEnabled(enable);
		if (!enable) {
			_saveItem.setEnabled(false);
		}
	}

	protected JMenuItem getOpenItem() {
		return _openItem;
	}

	protected void setOpenItem(JMenuItem openItem) {
		_openItem = openItem;
		_openItem.setName("open item");
	}

	protected JMenuItem getPrefItem() {
		return _prefItem;
	}

	protected void setPrefItem(JMenuItem prefItem) {
		_prefItem = prefItem;
		_prefItem.setName("pref item");
	}

	protected JMenuItem getHelpItem() {
		return _helpItem;
	}

	protected void setHelpItem(JMenuItem helpItem) {
		_helpItem = helpItem;
		_helpItem.setName("help item");
	}

	protected JMenuItem getSaveAllItem() {
		return _saveAllItem;
	}

	protected void setSaveAllItem(JMenuItem saveAllItem) {
		_saveAllItem = saveAllItem;
		_saveAllItem.setName("saveall item");
	}

	public JMenuItem getQuitItem() {
		return _quitItem;
	}

	public void setQuitItem(JMenuItem quitItem) {
		_quitItem = quitItem;
		_quitItem.setName("quit item");
	}

	/**
	 * @param message != null
	 */
	void showErrorMessage(String message) {
		if (message != null) {
			JOptionPane.showMessageDialog(rootPane, message, "An error occured",
					JOptionPane.ERROR_MESSAGE);
		}
	}

}
