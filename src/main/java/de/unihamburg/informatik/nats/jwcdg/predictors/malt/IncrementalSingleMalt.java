package de.unihamburg.informatik.nats.jwcdg.predictors.malt;

import java.lang.reflect.Field;

import org.maltparser.core.exception.MaltChainedException;
import org.maltparser.parser.Algorithm;
import org.maltparser.parser.SingleMalt;


/**
 * Warning: This is part of an ugly hack. You will not be able to
 * call parse or process on these because the original auhors chose
 * to make some crucial fields private :-(
 * However, this doesn't matter for our case.
 * @author Arne Köhn
 *
 */
public class IncrementalSingleMalt extends SingleMalt {

	protected void initParsingAlgorithm() throws MaltChainedException {
		if (mode == LEARN) {
			throw new MaltChainedException("Learning with IncrementalSingleMalt is not supported");
		} else if (mode == PARSE) {
			parsingAlgorithm = new IncrementalMaltParser(this);
			setParserField(parsingAlgorithm);
		}
	}
	
	private void setParserField(Algorithm parser) {
		try {
			Field f = SingleMalt.class.getDeclaredField("parser");
			f.setAccessible(true);
			f.set(this, parser);
		} catch (NoSuchFieldException | IllegalAccessException | IllegalArgumentException | SecurityException e) {
			e.printStackTrace();
		}

    }
}
