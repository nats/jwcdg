package de.unihamburg.informatik.nats.jwcdg.constraints.formulas;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.AccessContext;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;



public class FormulaImpl extends Formula {
	private final Formula premise ,conclusion;

	public FormulaImpl(Formula premise, Formula conclusion) {
		this.premise = premise;
		this.conclusion = conclusion;
		this.isContextSensitive = premise.isContextSensitive 
		                          || conclusion.isContextSensitive;
	}

	@Override
	public boolean evaluate(LevelValue[] binding, Context context, LexemeGraph lg) {
		if (! premise.evaluate(binding, context, lg)) {
			return true;
		} else {
			return conclusion.evaluate(binding, context, lg);
		}
	}
	
	@Override
	public void collectFeatures(List<Path> sofar) {
		premise.   collectFeatures(sofar);
		conclusion.collectFeatures(sofar);
	}
	
	@Override
	public boolean check() {
		if(!premise.check() || !conclusion.check())
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return String.format("%s->%s", premise.toString(), conclusion.toString());
	}

	@Override
	public void analyze(AccessContext context, Grammar g) {
		/* implications are evaluated left-to-right and on the
		   need-to-know principle. This means that any constraints imposed on
		   the constraint variables in the premise are still valid in the
		   conclusion. Therefore we can simply pass the current Context along,
		   allowing it to be modified by the subformula. */
		premise.analyze(context, g);
		conclusion.analyze(context, g);
	}
	
	@Override
	public void collectHasInvocations(List<Formula> akku) {
		premise.   collectHasInvocations(akku);
		conclusion.collectHasInvocations(akku);
	}
}
