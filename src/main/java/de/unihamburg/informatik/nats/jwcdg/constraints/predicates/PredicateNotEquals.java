package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;

public class PredicateNotEquals extends PredicateEquals {

	public PredicateNotEquals(Grammar g) {
		super(g, "!=");
	}
	
	@Override
	protected boolean compare(AVM val1, AVM val2) {
		return ! super.compare(val1, val2);
	}
}
