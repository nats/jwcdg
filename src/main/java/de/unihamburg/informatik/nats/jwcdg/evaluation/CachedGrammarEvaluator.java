package de.unihamburg.informatik.nats.jwcdg.evaluation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.Connection;
import de.unihamburg.informatik.nats.jwcdg.constraints.Constraint;
import de.unihamburg.informatik.nats.jwcdg.constraints.Direction;
import de.unihamburg.informatik.nats.jwcdg.incrementality.IncrementalFrobber;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Badness;
import de.unihamburg.informatik.nats.jwcdg.transform.ConstraintViolation;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

public class CachedGrammarEvaluator implements GrammarEvaluator, Serializable {
	
	private static final long serialVersionUID = 1L;

    public static final String USE_SCORECACHE = "useScorecache";
	
	private Grammar       grammar;
	private Configuration config;
	private ScoreCache    cache;
	//private HasCache      hasCache;
	//private int lockCounter;
	private int cachedLgMax; // used to notice extended nets
	private LexemeGraph cachedLg; // used to notice a different net
	
	
	/**
	 * Evaluator for the given Grammar and config
	 * Usually there is no need to generate several evaluators when evaluating the same net.  
	 * with the same grammar and parameters. 
	 * When evaluating an analyses for an extended constraintNet,
	 * the cache is cleared. 
	 * If the cache for the old net is still needed, you actually need a new evaluator.
	 * Also, you should not use the same evaluator concurrently for different nets for this very reason. 
	 */
	public CachedGrammarEvaluator(Grammar grammar, Configuration config) {
		this.grammar = grammar;
		this.config  = config;
		
		//this.hasCache = null;
		//this.lockCounter = 0;
		
		/* builds the score cache, when it is needed, not yet */
		this.cache = null;
	}

	/**
	 Evaluate all constraints on two LVs.

	 This function performs a conceptually simple task: it computes the
	 combined score of two LVs in a constraint net and returns the computed
	 score. This is done by applying evalConstraint() to all known
	 constraints and tallying the scores of all violated constraints.

	 For efficiency reasons, the actual implementation of this function is
	 much more complicated:

	 - The entire function can be run in fast mode or in thorough mode. In
	 fast mode, the function will stop applying constraints if a constraint
	 with score 0 is violated, and return 0 immediately. In thorough mode,
	 it will keep applying all possible constraints and bAdd() the results
	 to the result. Also, ConstraintViolation structures will be
	 allocated and inserted into the List conflicts. 
	 Thorough mode is requested by the flag of the same name. 
	 In fast mode, the returned badness should only be used like a score
	 and no violations will be reported.
	 
	 - In fast mode, if the LVs lva and lvb have already been
	 evaluated by this function and scUseCache was set during both
	 invocations of evalBinary(), the result is read from net->cache and
	 not computed again.
	 - To make this case more probable, the LVs lva and lvb are always sorted by
	 the number of their respective levels. (That is, only half of the
	 entire cache is ever used.)
	 - Even if no precomputed result exists, not all constraints may be
	 evaluated:

	 Unary constraints are ignored completely.

	 Inactive constraints and constraints from inactive sections are
	 skipped likewise.

	 Of the remaining constraints, only those that may actually fail are
	 evaluated. Hence not all constraints are evaluated, but only those
	 whose signature matches the actual configuration existing between
	 lva and lvb. The List of these constraints is found in the
	 matrix cdgConstraintMatrix[].

	 In fast mode, if a hard constraint is violated, the result 0.0 is
	 returned immediately.

	 If the parameter context is NULL, context-sensitive constraints will
	 not be evaluated.

	 If the parameter use_cs_only is set, ONLY context-sensitive constraints
	 will be evaluated. See evalInContext() for why this is occasionally
	 necessary.

	 - Theoretically, a binary constraint may be violated twice by two
	 LVs. Think of a constraint that forbids the label X to appear more
	 than once in an analysis. If two LVs with label X are evaluated
	 jointly, both (a,b) and (b,a) violate this condition.
	 But since the two violations do not really indicate different errors,
	 this kind of double fault is usually not desired.

	 A double fault can only occur if the LVs match the signature of a
	 constraint in both variants of instantiation. Since every binary
	 constraint signature must explicitly state the two levels it refers
	 to, this is only possible if both LVs belong to the same level and
	 either their configuration is symmetrical, or it is is asymmetrical,
	 but the signature of the constraint subsumes both configurations.

	 To eliminate all double faults, evalBinary() checks for all
	 of these possibilities explicitly.

	 - If a value was computed rather than looked up, and
	 scUseCache is set, it is registered in net->cache.

	 The parameter net can be NULL, so that LVs can be evaluated even in the
	 absence of a constraint net. In this case no caching is possible.

	 */
	@Override
	public GrammarEvaluationResult evalBinary(LevelValue lva, LevelValue lvb,
			LexemeGraph lg, Context context, boolean useCsOnly,
			boolean thorough) {
		
		/* force lv order */
		if (lva.getIndexWRTNet() > lvb.getIndexWRTNet()) {
			LevelValue lv = lvb;

			lvb = lva;
			lva = lv;
		}

		/* lookup scorecache */
		boolean useCache = config.getFlag(USE_SCORECACHE);
		if (useCache && !useCsOnly) {
			if(cache == null) {
				cache = new ScoreCache();
				cachedLgMax = lg.getMax();
				cachedLg = lg;
			} else if(cachedLgMax != lg.getMax() || cachedLg != lg) {
				cache.clear();
				cachedLgMax = lg.getMax();
				cachedLg = lg;
			}
			if (cache.hasEntryFor(lva, lvb)) {
				if(context != null) {
					GrammarEvaluationResult gerCF =	cache.getResultFor(lva, lvb);
					if(!thorough && gerCF.getScore()== 0.0)
						// the cached context free result already has hard conflicts, 
						// no need to evaluate the context sensitive stuff  
						return gerCF;
					
					GrammarEvaluationResult gerCS = evalBinary(lva, lvb, lg, context, true, thorough);
					return gerCS.mergeWith(gerCF);
				} else {
					// we already got all we need
					return cache.getResultFor(lva, lvb);
				}
			}
		}
		
		Badness b   = Badness.bestBadness();
		Badness bCF = Badness.bestBadness();		
		List<ConstraintViolation> conflicts   = new ArrayList<>();
		List<ConstraintViolation> conflictsCF = new ArrayList<>();
		
		Set<Constraint> locallyDeactivated = new HashSet<>();

		Direction dir1 = lva.getDirection();
		Connection connection = lva.getCon(lvb);
		Direction dir2 = lvb.getDirection();

		if(context != null && lg != null && !context.isCacheInitialized()) {
			context.initializeCache(lg.getMax(), grammar.getNoOfHas());
		}

		/* simplest case: lva and lvb belong to the same level,
		 and their configuration is symmetrical. That means
		 that there is only one class of constraints that they can violate. */
		if (lva.getLevel().getNo() == lvb.getLevel().getNo() && 
				connection == connection.inverse() && dir1 == dir2) {

			/* give each constraint in the list two chances to fail */
			for (Constraint c: grammar.getHashedBCs(lva.getLevel(), lvb.getLevel(), dir1, connection, dir2)) {
				/* activated? */
				// if the constraint is deactivated in the grammar, it will not be among the hashed values. 
				// So we only need to check local deactivations:
				if (locallyDeactivated.contains(c) ) {
					continue;
				}

				if (! c.isContextSensitive() && useCsOnly)
					continue;
				
				ConstraintEvaluationResult cerA = c.eval(lg, context, lva, lvb);
				ConstraintEvaluationResult cerB = c.eval(lg, context, lvb, lva);
				if ( cerA.isViolated())  {
					b   = b.  add(cerA.getScore());
					if(thorough) {
						ConstraintViolation cv = cerA.toCV();
						conflicts.add(0, cv);
						if(!c.isContextSensitive() && useCache) {
							bCF = bCF.add(cerA.getScore());
							conflictsCF.add(cv);
						}
					} else if(b.hard >0) {
						break;
					}
				} else if(cerB.isViolated()) {
					// symmetrical binary conflicts only count once, 
					// thus the else 
					b   = b.  add(cerB.getScore());
					if(thorough) {
						ConstraintViolation cv = cerB.toCV();
						conflicts.add(0, cv);
						if(!c.isContextSensitive() && useCache) {
							bCF = bCF.add(cerB.getScore());
							conflictsCF.add(cv);
						}
					} else if(b.hard >0) {
						break;
					}
				}
			}
		}
		
		/* general case: there are two classes of constraints that lva and lvb
		 may violate, e.g. {X\SYN/Y\SEM} and {X\SEM\Y\SYN}. */
		else {

			/* evaluate each constraint from either of two lists,
			 but without double faults, therefore the locallyDeactivated list */

			/* first list */
			for (Constraint c: grammar.getHashedBCs(lva.getLevel(), lvb.getLevel(), dir1, connection, dir2)) {
				/* activated? */
				// if the constraint is deactivated in the grammar, it will not be among the hashed values. 
				// So we only need to check local deactivations:
				if (locallyDeactivated.contains(c) )
					continue;
				
				if (!c.isContextSensitive() && useCsOnly)
					continue;
				
				ConstraintEvaluationResult cer = c.eval(lg, context, lva, lvb); 
				if (cer.isViolated()) {
					locallyDeactivated.add(c);
					b   = b.  add(cer.getScore());
					if(thorough) {
						ConstraintViolation cv = cer.toCV();
						conflicts.add(0, cv);
						if(!c.isContextSensitive() && useCache) {
							bCF = bCF.add(cer.getScore());
							conflictsCF.add(cv);
						}
					} else if(b.hard >0) {
						break;
					}
				}
			}

			/* second list */
			for (Constraint c : grammar.getHashedBCs(lvb.getLevel(), lva.getLevel(), dir2,
					connection.inverse(), dir1)) {
				/* activated? */
				// if the constraint is deactivated in the grammar, it will not be among the hashed values. 
				// So we only need to check local deactivations:
				if (locallyDeactivated.contains(c) )
					continue;
				
				if (!c.isContextSensitive() && useCsOnly)
					continue;
				
				ConstraintEvaluationResult cer = c.eval(lg, context, lvb, lva); 
				if (cer.isViolated()) {
					b   = b.  add(cer.getScore());
					
					if(thorough) {
						ConstraintViolation cv = cer.toCV();
						conflicts.add(0, cv);
						if(!c.isContextSensitive() && useCache) {
							bCF = bCF.add(cer.getScore());
							conflictsCF.add(cv);
						}
					} else if(b.hard >0) {
						break;
					}
				}
			}
		}

		if (useCache && !useCsOnly && thorough) {
			// store computed score in scorecache, but only the context free part is cachable
			cache.setResultFor(lva, lvb, 
					new GrammarEvaluationResult(conflictsCF, bCF.getScore(), bCF, null));
		}
		return new GrammarEvaluationResult(conflicts, b.getScore(), b, null);
	}


	/**
	 Eval unary constraints on LV.

	 This function applies all unary constraints to lv. It uses the Vector
	 cdgConstraintVector in much the same way as evalBinary() uses
	 cdgConstraintMatrix. The same exceptions for deactivated constraints
	 apply as in that function. In addition, evalUnary() stores all violated
	 local constraints in the lv.

	 */
	@Override
	public GrammarEvaluationResult evalUnary(LevelValue lv, LexemeGraph lg,
			Context context, boolean useCsOnly, boolean thorough) {
		
		/* lookup scorecache */
		boolean useCache = config.getFlag(USE_SCORECACHE);
		if (useCache && !useCsOnly) {
			if(cache == null) {
				cache = new ScoreCache();
				cachedLgMax = lg.getMax();
			} else if(cachedLgMax != lg.getMax() || cachedLg != lg) {
				cache.clear();
				cachedLgMax = lg.getMax();
				cachedLg = lg;
			}
			if (cache.hasEntryFor(lv)) {
				if(context != null) {
					GrammarEvaluationResult gerCF =
							cache.getResultFor(lv);
					if(!thorough && gerCF.getScore()== 0.0)
						// the cached context free result already has hard conflicts, 
						// no need to evaluate the context sensitive stuff  
						return gerCF;
					
					GrammarEvaluationResult gerCS = 
							evalUnary(lv, lg, context, true, thorough);
					return gerCS.mergeWith(gerCF);
				} else {
					// we already got all we need
					return cache.getResultFor(lv);
				}
			}
		}
		
		Badness b    = Badness.bestBadness();
		Badness bnfl = Badness.bestBadness();
		List<ConstraintViolation> conflicts = new ArrayList<>();
		
		Badness bCF    = Badness.bestBadness();
		Badness bnflCF = Badness.bestBadness();
		List<ConstraintViolation> conflictsCF = new ArrayList<>();

		if(context != null && lg != null && !context.isCacheInitialized()) {
			context.initializeCache(lg.getMax(), grammar.getNoOfHas());
		}
		
		for (Constraint c : grammar.getHashedUCs(lv.getLevel(), lv.getDirection())) {
			// if the constraint is deactivated in the grammar, it will not be among the hashed values.
			// So we don't have to check that here.
			

			if (useCsOnly && !c.isContextSensitive()) {
				continue;
			}
			
			ConstraintEvaluationResult cer = c.eval(lg, context, lv);
			if (cer.isViolated()) {
				b= b.add(cer.getScore());
				conflicts.add(cer.toCV());
				
				if(!c.isContextSensitive() && useCache && thorough) {
					bCF= bCF.add(cer.getScore());
					conflictsCF.add(cer.toCV());
				}
				
				if ( !config.getFlag(IncrementalFrobber.FLUCTUATING_SCORE) || 
						!c.isFluctuating( lv.getRegentGN().isNonspec() || lv.getRegentGN().isVirtual()) ) {
					bnfl = bnfl.add(cer.getScore());
					if(!c.isContextSensitive() && useCache && thorough) {
						bnflCF = bnflCF.add(cer.getScore());
					}
				}
				
				if (!thorough && b.hard >0 && (!config.getFlag(IncrementalFrobber.FLUCTUATING_SCORE) || !c.isFluctuating(lv.getRegentGN().isNonspec())) ) {
					return new GrammarEvaluationResult(conflicts, b.getScore(), b, bnfl);
				}
			}
		}
		
		if(useCache && !useCsOnly && thorough) {
			// store computed score in scorecache, but only the context free part is cachable
			cache.setResultFor(lv, new GrammarEvaluationResult(conflictsCF, bCF.getScore(), bCF, bnflCF));
		}

		return new GrammarEvaluationResult(conflicts, b.getScore(), b, bnfl);
	}

	/**
	Eval context-sensitive constraints only.
	
	This function evaluates all LVs in the Vector LVs against each other,
	using only context-sensitive constraints, and assuming that the context
	is context.
	
	This is useful for methods that cannot normally provide context, e.g.
	because they operate incrementally. Such methods should use normal
	evalUnary and evalBinary calls with no context while they are building a
	structure, and then this function once the structure is complete.
	
	Note that LVs and context can be different; one frobbing method actually
	needs to eval a subset of a structure, but in the global context.
	*/
	@Override
	public GrammarEvaluationResult evalInContext(List<LevelValue> lvs, Context context, LexemeGraph lg) {
		Badness b = Badness.bestBadness();
		List<ConstraintViolation> cvs = new ArrayList<>();
		GrammarEvaluationResult ger;
	
		/* Don't pay for what you don't use */
		if (!grammar.isContextSensitive()) {
			return new GrammarEvaluationResult(cvs, 1.0, 
					Badness.bestBadness(), Badness.bestBadness());
		}
	
		if(context != null && lg != null && !context.isCacheInitialized()) {
			context.initializeCache(lg.getMax(), grammar.getNoOfHas());
		}
		
		for (LevelValue lva : lvs) {
			if (lva == null)
				continue;
			ger = evalUnary(lva, lg, context, true, true);
			b = b.addBadness(ger.getBadness());
			assert(!ger.getCVs().contains(null));
			cvs.addAll(ger.getCVs());
			
			ListIterator<LevelValue> lit = lvs.listIterator(lvs.indexOf(lva)+1);
			LevelValue lvb; 
			while (lit.hasNext()) {
				lvb = lit.next();
				
				if (lvb == null)
					continue;
				
				ger = evalBinary(lva, lvb, lg, context, true, true);
				b = b.addBadness(ger.getBadness());
				cvs.addAll(ger.getCVs());
			}
		}
		return new GrammarEvaluationResult(cvs, b.getScore(), b, null);
	}
	
//	/**
//	 Declare evaluation lock.
//
//	 When many constraints are evaluated on the same dependency tree, it can
//	 be worthwhile to cache the result of common subformulas. Therefore we
//	 offer the caller the possibility to declare that the dependency tree will
//	 not change during the next evaluations. The module eval is then free to
//	 reuse the results of previous evaluations until the tree is unlocked
//	 again.
//
//	 WIDTH specifies the number of time points in the lexeme graph that will
//	 be used in the evaluations.
//	 */
//	private void lockHasCache(int width) {
//
//		/* do not even try to cache things if there are no has() formulas */
//		if (!grammar.isContextSensitive()) {
//			return;
//		}
//
//		if (lockCounter == 0) {
//			hasCache = new HasCache(width, grammar.getNoOfHas());
//		}
//
//		lockCounter++;
//	}
//	
//	
//	/**
//	 Release evaluation lock.
//
//	 If lock_tree() was called multiple times, evaluation remains locked onto
//	 the current tree until each call been cancelled individually.
//	 */
//	private void unlockHasCache() {
//
//		if (!grammar.isContextSensitive()) {
//			return;
//		}
//
//		if (lockCounter <= 0) {
//			throw new RuntimeException("ERROR: invalid hascache unlock request!\n");
//		}
//		lockCounter--;
//		if (lockCounter == 0) {
//			hasCache = null;
//		}
//	}
//	
//	public HasCache getHasCache() {
//		return hasCache;
//	}

	@Override
	public Grammar getGrammar() {
		return grammar;
	}
	
	public void reset() {
		if(cache!=null)
			cache.clear();
	}
}
