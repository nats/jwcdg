package de.unihamburg.informatik.nats.jwcdg.evaluation;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * 
 * @author the CDG-Team
 *
 */
public interface GrammarEvaluator {

	/**
	 * @param lv
	 * @param net
	 * @param context
	 * @param useCsOnly
	 * @param thorough
	 * @return
	 */
    GrammarEvaluationResult evalUnary(LevelValue lv, LexemeGraph lg,
                                      Context context, boolean useCsOnly, boolean thorough);
	
	/**
	 * Evaluate all constraints on two LVs.

	 * This function performs a conceptually simple task: 
	 * it computes the combined score of two LVs in a constraint net 
	 * and returns the computed score. 
	 * This is done by applying constraint.eval() on all known
	 * constraints and tallying the scores of all violated constraints.
	 */
    GrammarEvaluationResult evalBinary(LevelValue lva, LevelValue lvb, LexemeGraph lg,
                                       Context context, boolean useCsOnly, boolean thorough);
	
	GrammarEvaluationResult evalInContext(List<LevelValue> selection, Context context, LexemeGraph lg);
	
	Grammar getGrammar();
}
