package de.unihamburg.informatik.nats.jwcdg;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.List;

import org.antlr.runtime.RecognitionException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNet;
import de.unihamburg.informatik.nats.jwcdg.incrementality.ChunkIncrement;
import de.unihamburg.informatik.nats.jwcdg.incrementality.IncrementalFrobber;
import de.unihamburg.informatik.nats.jwcdg.incrementality.IncrementalStep;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;

/**
 * Main class
 * 
 * Usage:
 * 1. Locate or create a startup file that contains the filenames of the grammar etc.
 * 2. Call makeParser with a path to the startup file 
 * 3. Call parse(sentence) on the returned parser
 * 4a. View resulting parse via parse.print()
 * 4b. The resulting parse object can be converted to the .cda format via parse.toAnnotation
 * 5. ...
 * 6. PROFIT!!!
 * 
 * @author Niels Beuck and Arne Köhn
 */
public abstract class JWCDG {
	public static final String GRAMMAR_FILES = "grammarFiles";
	private static final String LEXICON_FILE = "lexiconFile";
	public static final String GRAMMAR_ENCODING = "grammarEncoding";
	private static final String LEXICON_ENCODING = "grammarEncoding";
	private static Logger logger = LogManager.getLogger(JWCDG.class);
	
	/**
	 * generate a parser from a given properties file
	 * all further paths to the grammar etc. are read from that file
	 * 
	 * make sure to either
	 * - use absolute paths
	 * - use paths to the current working directory
	 * - or a JWCDG_DIR environment variable is defined
	 * 
	 * @param filename path to the settings
	 * @return a parser object for the given settings
	 * @throws IOException
	 */
	public static Parser makeParser(String filename) throws IOException {
		return makeParser(filename, new DefaultParserFactory());
	}
	
	private static <T extends Parser> T makeParser(String filename, ParserFactory<T> pfac) throws IOException {
		
		logger.info("Reading settings...");
		Configuration config = Configuration.fromFile(filename);

		return makeParser(config, pfac);
	}
	
	public static String convertStreamToString(java.io.InputStream is, String encoding) {
		try {
			return new java.util.Scanner(is, encoding).useDelimiter("\\A").next();
		} catch (java.util.NoSuchElementException e) {
			return "";
		}
	}

	/**
	 * generate a parser from a given properties file
	 * all further paths to the grammar etc. are read from that file
	 * 
	 * make sure to either
	 * - use absolute paths
	 * - use paths to the current working directory
	 * - or a JWCDG_DIR environment variable is defined
	 * 
	 * @param settigns the settings 
	 * @return 
	 * @return a parser object for the given settings
	 * @throws IOException
	 */
	public static <T extends Parser> T makeParser(Configuration config, ParserFactory<T> pfac) throws IOException {
		
		config.checkSettings();
		
		if (pfac instanceof DefaultParserFactory && config.getFlag(ConstraintNet.USE_VIRTUAL_NODES)) {
			logger.warn("Nonincremental parsing using virtual nodes is not supported, turning off VNs");
			config.setFlag(ConstraintNet.USE_VIRTUAL_NODES, false);
		}
		
		logger.info("Reading grammar...");
		try{
			StringBuilder sb = new StringBuilder();
			for (String fname: config.getString(GRAMMAR_FILES).split(",")) {
				String fixedPath = Configuration.fixPath(fname);
				sb.append(convertStreamToString(new FileInputStream(fixedPath), config.getString(GRAMMAR_ENCODING)));
			}
			Grammar g = Grammar.fromFile(new StringReader(sb.toString()), config);
			String mdir =config.getString(de.unihamburg.informatik.nats.jwcdg.predictors.MaltPredictor.MALT_MODEL_DIR);
			String mname = config.getString(de.unihamburg.informatik.nats.jwcdg.predictors.MaltPredictor.MALT_MODEL_NAME);
			if (mdir.equals("") || mname.equals(""))
				g.setSectionActive("malt", false);
			g.cache(config);
			
			logger.info("Reading lexicon...");
			Lexicon lex = Lexicon.fromFile(
					new File(Configuration.fixPath(config.getString(LEXICON_FILE))), 
					config.getString(LEXICON_ENCODING), 
					g.getAllAttributes());
			
			logger.info("Initializing parser...");
			T parser = pfac.makeParser(g, lex, config);
			
			logger.info("Parser ready!");
			return parser;
		}	catch (RecognitionException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	
	private static void parseIncremental(IncrementalFrobber parser, String outtemplate) throws IOException {
		IncrementalStep step = parser.makeInitialStep();
		List<String> tokens = Parser.splitSentence(new BufferedReader(new InputStreamReader(System.in)).readLine());
		int increment = 1; // leave room for the "empty" increment with only virtual nodes
		for (String word : tokens) {
			ChunkIncrement chunk = new ChunkIncrement(word);
			boolean isLast = tokens.size()==increment;
			step = parser.continueParsing(step, chunk, isLast, null);
			File outfile = new File(outtemplate.replace("%1", Integer.toString(increment)));
			FileWriter writer = new FileWriter(outfile);
			writer.write(step.getParse().toAnnotation(false));
			writer.close();
			increment += 1;
		}
	}
	
	private static void parse(Parser parser) throws IOException {
		BufferedReader cin = new BufferedReader(new InputStreamReader(System.in));
		String sentence;
		while ((sentence = cin.readLine()) != null) {
			Parse p = parser.parse(sentence);
			String annotation = p.toAnnotation(false);
			System.out.println(annotation);
		}
	}
	
	/**
	 * @param args args[0]: path to config file
	 */
	public static void main(String[] args) {
		try {
			@SuppressWarnings("static-access")
			Option opt_incremental = OptionBuilder.withArgName("output template")
				.hasArg()
				.withDescription("run in incremental mode, write results to "+
				                 "<output template>, %1 will be substituted "+
				                 "by the increment number")
				.create("incremental");
			Option opt_help = new Option( "help", "print this message" );
			Options options = new Options();
			options.addOption(opt_help);
			options.addOption(opt_incremental);
			CommandLineParser cmdlineparser = new GnuParser();
			CommandLine line = null;
			try {
				line = cmdlineparser.parse( options, args );
			}
			catch( ParseException exp ) {
				System.err.println( "Couldn't parse command line args: " + exp.getMessage() );
				System.exit(1);
			}
			
			if (line.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp( "jwcdg Config.options", options );
				System.exit(0);
			}
			
			// construct grammar and lexicon
			long starttime = System.currentTimeMillis();
			args = line.getArgs();
			
			if (args.length == 0) {
				System.out.println("You have to specify a configuration file as argument!");
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp( "jwcdg Config.options", options );
				System.exit(1);
			}
			
			String confFileName = args[0];
			if (line.hasOption("incremental")) {
				IncrementalFrobber parser = JWCDG.makeParser(confFileName, new IncrementalParserFactory()); 
				parseIncremental(parser, line.getOptionValue("incremental"));
			} else {
				Parser parser = JWCDG.makeParser(confFileName, new DefaultParserFactory());
				parse(parser);
			}
			System.err.print("runtime: ");
			System.err.println((System.currentTimeMillis()-starttime)/1000);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
