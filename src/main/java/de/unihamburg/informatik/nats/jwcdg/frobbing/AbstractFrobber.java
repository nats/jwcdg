package de.unihamburg.informatik.nats.jwcdg.frobbing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.base.Stopwatch;
import com.google.common.base.Supplier;
import com.google.common.collect.MinMaxPriorityQueue;
import com.google.common.util.concurrent.AtomicDouble;

import de.unihamburg.informatik.nats.jwcdg.ParseObserver;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNet;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNetState;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.InvalidNetStateException;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.NoPathPossibleException;
import de.unihamburg.informatik.nats.jwcdg.constraints.Constraint;
import de.unihamburg.informatik.nats.jwcdg.constraints.Level;
import de.unihamburg.informatik.nats.jwcdg.evaluation.ConstraintEvaluationResult;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluationResult;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.exceptions.TimeUpException;
import de.unihamburg.informatik.nats.jwcdg.helpers.DaemonThreadFactory;
import de.unihamburg.informatik.nats.jwcdg.incrementality.virtualNodes.VirtualNodeSpec;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;
import de.unihamburg.informatik.nats.jwcdg.parse.ParseInfo;
import de.unihamburg.informatik.nats.jwcdg.transform.Analysis;
import de.unihamburg.informatik.nats.jwcdg.transform.Badness;
import de.unihamburg.informatik.nats.jwcdg.transform.ConstraintViolation;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;
import de.unihamburg.informatik.nats.jwcdg.transform.Patch;
import de.unihamburg.informatik.nats.jwcdg.transform.ProblemSolver;

public abstract class AbstractFrobber implements ProblemSolver {
	
	/* *********/
	/* Statics */
	/* *********/
	public static final String GREEDY                 = "greedy";
	public static final String BLOCK_NEW_LVS          = "blockNewLVs";
	public static final String USE_CROSS_LEVEL_HINTS  = "useCrossLevelHints";
	public static final String BEAMWIDTH              = "beamwidth";
	public static final String TIMELIMIT              = "timelimit";
	public static final String MAXSTEPS               = "maxSteps";
	public static final String IGNORETHRESHOLD        = "ignoreThreshold";
	public static final String suppressPartialSolutions = "suppressPartialSolutions";
	public static final String alwaysShowAllSolutions = "alwaysShowAllSolutions";
	public static final String HAS_CROSS_LEVEL_HINT   = "crosslevelhint";
	public static final String AGENDASIZE             = "agendaSize";
	public static final String NUM_THREADS            = "numThreads";
	
	public static final EnumSet<FrobbingFlag> OD_ALLOWALL = EnumSet.of(FrobbingFlag.OD_ORIENT, FrobbingFlag.OD_LEXICAL, FrobbingFlag.OD_LABEL);
	private static final boolean performTimingMeasurements = false;
	
	private static Logger logger = LogManager.getLogger(AbstractFrobber.class);
	protected static ExecutorService executorService = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 5L, TimeUnit.SECONDS, new SynchronousQueue<>(), new DaemonThreadFactory());

	/* ********/
	/* fields */
	/* ********/
	private   List<ParseObserver> observers;
	protected FrobStats stats;
	
	protected ConstraintNet net;       /* underlying SLCN */
	protected ConstraintNetState netState;
	protected ConstraintNetState savedState; /* state to return to if pruning deleted too much */
	
	protected Analysis best;       /* best Analysis found so far */
	protected Analysis current;    /* current Analysis */
	protected Analysis annotation; /* Analysis built from annotation */
	
	protected double globalMaximum;    /* upper bound on score for the utterance */
	protected double globalMinimum;    /* lower bound on score for the utterance */
	protected List<ConstraintViolation> removedConflicts;   /* recently removed conflicts */
	protected List<ConstraintViolation> unremovable;        /* conflicts that have been given up on */
	protected List<Integer> history;
	
	protected DecoratedParse solution = null;
	
	protected ArrayList<Double> limitBuffer; 	 /* since the number of levelValues can grow very large,
	                                    we don't want to allocate an array like that too often,
	                                    so we keep one in the problemStruct that can be reused */
	
	protected Boolean killedByTimelimit; // whether the time limit was responsible for terminating the search */
	// end old problem fields 
		
	protected Configuration config;
	
	protected double frobbingGoal = 0.0;
	protected int timeLimit;
	
	//protected int stepcount;
	
	private final int numProcessors;
	protected boolean interactive = false;
	protected volatile boolean interrupted = false;

	protected GrammarEvaluator evaluator;
	private Collection<? extends ConstraintViolation> savedUnremovable; 
	
	/**
	 * An initial analysis will be build from scratch using locally best scored values
	 * 
	 * The parameter PRESSURE can be used to exert temporal pressure
	 * on the processing. It is assigned to the globalMinimum of the
	 * resulting Problem.
	 */
	public AbstractFrobber(ConstraintNetState state, Configuration config, GrammarEvaluator evaluator,
			double pressure) {
		this(state, config, evaluator, pressure, null, null);
	}
	
	/**
	 * An initial anlysis will be build from the given parse
	 * 
	 * The parameter PRESSURE can be used to exert temporal pressure
	 * on the processing. It is assigned to the globalMinimum of the
	 * resulting Problem.
	 */
	public AbstractFrobber(ConstraintNetState state, Configuration config, GrammarEvaluator evaluator,
			double pressure, Parse parse) {
		throw new RuntimeException("Not yet implemented!");
		// this(state, config, evaluator, pressure, null, parse);
	}
	
	/**
	 * The given analysis will be used as initial analysis
	 * 
	 * The parameter PRESSURE can be used to exert temporal pressure
	 * on the processing. It is assigned to the globalMinimum of the
	 * resulting Problem.
	 */	
	public AbstractFrobber(ConstraintNetState state, Analysis a,
			Configuration config, GrammarEvaluator evaluator, double pressure) {
		this(state, config, evaluator, pressure, a, null);
	}
	
	/**
	 * Implementation for the three different public constructors
	 */
	private AbstractFrobber(ConstraintNetState state, Configuration config, GrammarEvaluator evaluator,
			double pressure, Analysis a, Parse parse) {
		this.net        = state.getNet();
		this.netState   = state;
		this.savedState = null;
		this.evaluator  = evaluator;
		
		this.config     = config;
		this.observers  = new ArrayList<>();
		this.stats      = new FrobStats();
		
		this.globalMaximum = 1.0;
		
		this.history          = new ArrayList<>();
		this.removedConflicts = new ArrayList<>();
		this.unremovable      = new ArrayList<>();
		this.savedUnremovable = new ArrayList<>();
		
		this.timeLimit = config.getInt(TIMELIMIT);
		this.numProcessors = config.getInt(NUM_THREADS) == 0
		                        ? Runtime.getRuntime().availableProcessors()
		                        : config.getInt(NUM_THREADS);
		/* translate external time pressure into internal controlling
		 variables. At the moment this is done by using the parameter
		 `pressure' as a non-zero initialization for globalMinimum, but
		 other methods could be invented. */
		this.globalMinimum = pressure;

		/* initial analysis */
		try {
			if(a != null) {
				current = a;
			} else if (parse != null) {
				// TODO #REVERSE current = parse.toAnalysis();
				throw new RuntimeException("Not implemented yet!");
			} else {
				logger.info("Building initial analysis...");
				
				current = new Analysis(net);
				current.buildPath(state);

				/* iterate over all slots */
				for (int i = 0; i < net.getNoOfSlots(); i++) {
					if (!current.slotActive(i)) {
						continue;
					}
					extendAnalysis(current, i);
				}
				logger.info(" done.\n");
			}
		} catch (NoPathPossibleException e) {
			/* parse2analysis can fail on inadequate input */
			throw new RuntimeException("cannot build initial anlysis");
		}

		/* compute conflicts */
		logger.info("Computing conflicts...");
		current.judge(evaluator);
		assert current.check(evaluator, 3, new ArrayList<>());
		logger.info(" done.\n");

		/* we *might* just have constructed a valid Analysis by sheer luck... */
		if (current.isValid() && current.getBadness().soft > globalMinimum) {
			globalMinimum = current.getBadness().soft;
		}
		
		/* save initial fallback state */
		reset();
		rememberState();
	}

	/** ----------------------------------------------------------------------
	 Reset Problem to the remembered state.

	 This function restores the netstate that it had at the time when
	 rememberState() was last called on it.
	 */
	protected void restoreSavedState() {
		logger.info("restoring stored state \n");

		if (savedState == null) {
			return;
		}

		unremovable = new ArrayList<>(savedUnremovable);
		netState = new ConstraintNetState(savedState);
	}

	/**
	 * Record the current state of the Problem for posterity.
	 */
	protected void rememberState() {
		if(!netState.isValid()) {
			// this netstate is no suitable fallback position
			throw new RuntimeException("No valid net for this input sentence and grammar! " +
					"Please contact the grammar author. " +
					"If you are the grammar author, try reducing the number of hard contraints in the grammar.");
		}
		
		savedState = new ConstraintNetState(netState);
		savedUnremovable = new ArrayList<>(unremovable);
	}
	
	@Override
	public abstract boolean solve();
	
	public Boolean call() {
		return solve();
	}
	
	/**
	   Find next conflict to be attacked.

	   This is simply the first conflict in the list that isn't unremovable.
	   Because of the way the list ->conflicts is handled, this means that it
	   is the hardest recent conflict, or simply the hardest conflict if there
	   are no recent conflicts.
	*/
	protected ConstraintViolation selectConflict() {
		current.sortConflicts(best.getConflicts());

		for (ConstraintViolation cv : current.getConflicts()) {
			if (!isUnremovable(cv)) {
				return cv;
			}
		}
		return null;
	}
	
	/**
	 * Checks if the Problem can still reach the given score goal
	 * criteria checked:
	 *
	 * global Maximum
	 *
	 * product of unremovable conflic penalty
	 *
	 */
	protected boolean goalReachable() {
		if(best.getBadness().hard == 0 && best.getBadness().soft > frobbingGoal)
			return true; // obviously

		if(globalMaximum < frobbingGoal)
			return false; // limits say it's not possible

		double score = 1.0;
		for(ConstraintViolation cv : unremovable) {
			score *= cv.getPenalty();
		}

		// unremovable conflicts alone prevent the goal to be reachable
		// otherwise, assume it's possible
		return score >= frobbingGoal;
	}
	

	/**
	   Find a Patch that removes the conflict at position WHERE in the list of
	   current conflicts from A.

	   The result can be NULL.

	   FLAGS determines the behaviour of this function and the helper functions
	   it calls (see frobbing.h).
	   
	 * @throws InvalidNetStateException is thrown, if limit propagation proved that there are no values left for one of the slots
	 * @throws TimeUpException
	*/
	protected Patch attackConflict(Analysis a, ConstraintViolation cv, EnumSet<FrobbingFlag> flags) throws InvalidNetStateException, TimeUpException {
		
		List<Integer> which;
		Patch result;
		int w, x, y, z;
		EnumSet<FrobbingFlag> newFlags= flags.clone();
		newFlags.addAll(OD_ALLOWALL); /* allow all kinds of changes */

  		logger.debug(String.format("Attacking %s ...", cv.getConstraint().getId()));

		  // TODO #VN blockVirtualNodes
//		  if(a.P.lg.ve && blockVirtualNodes) {
//			  // mark certain virtual nodes as blocked from use
//			  veBlockVirtualNodes(a.P.lg.ve, a);
//		  }

		x = cv.getLV1().getIndex();
		if (cv.getLV2() != null) {
			y = cv.getLV2().getIndex();
			if (netState.getDomain(y).size() < netState.getDomain(x).size()) {
				w = x;
				x = y;
				y = w;
			}
		} else {
			y = -1;
		}

		if (cv.getLV3() != null) {
			z = cv.getLV3().getIndex();
			if (netState.getDomain(z).size() < netState.getDomain(y).size()) {
				w = y;
				y = z;
				z = w;
			}
			  
			if (netState.getDomain(y).size() < netState.getDomain(x).size()) {
				w = x;
				x = y;
				y = w;
			}
		} else {
			z = -1;
		}  

		removedConflicts.add(new ConstraintViolation(cv));

		/* With a context-sensitive constraint, we can not guarantee
		   that changing *any* LV will fulfill it. It might actually need
		   several different LVs in the structure.

		   But we assume that most of them just check for some kind of existence,
		   so that it is worthwhile to search all unary patches. */
		if(cv.getConstraint().isContextSensitive()) {
			int levelno = cv.getLV1().getLevel().getNo();
			int i, index;
			which = new ArrayList<>();
			for(i = a.getSubStart(); i < a.getSubStop(); i++) {
				// TODO #VN blockvirtual nodes
//				if(blockVirtualNodes && a.P.lg.ve && bvElement( a.P.lg.ve.blocked, i) ) {
//					// skip blocked domains
//					// we need to do that here because for context sensitive constraints, all domains are considered
//					continue;
//				}

				index = net.getNoOfLevels() * i + levelno;
				which.add(index);
			}
			if(cv.getLV2() != null && cv.getLV2().getLevel().getNo() != levelno) {
				for(i = a.getSubStart(); i < a.getSubStop(); i++) {
					index = net.getNoOfLevels() * i + cv.getLV2().getLevel().getNo();
					which.add(index);
				}
			}
			if(cv.getLV3() != null && cv.getLV3().getLevel().getNo() != cv.getLV2().getLevel().getNo()) {
				for(i = a.getSubStart(); i < a.getSubStop(); i++) {
					index = net.getNoOfLevels() * i + cv.getLV3().getLevel().getNo();
					which.add(index);
				}
			}

			newFlags.add(FrobbingFlag.OD_SINGLE);
			result = optimizeDomains(a, which, newFlags);
			
		} else {
			/* otherwise it's easy: frob those places where the error manifests 
			   if the constraint has a cross level hint, also include level variations */
			which = new ArrayList<>(3);
			which.add(x);
			if (cv.getLV2() != null)
				which.add(y);
			if (cv.getLV3() != null)
				which.add(z);
			
			if(config.getFlag(USE_CROSS_LEVEL_HINTS) && cv.getConstraint().hasFlag(HAS_CROSS_LEVEL_HINT)) {
				which = addCrossLevelDomains(which);
				
				newFlags.add(FrobbingFlag.OD_SWAP);
				newFlags.add(FrobbingFlag.OD_PREDICT);
				newFlags.remove(FrobbingFlag.OD_LIMIT);
				newFlags.remove(FrobbingFlag.OD_IMMEDIATE);
				result = optimizeDomains(a, which, newFlags);
			} else {
				result = optimizeDomains(a, which, newFlags);
			}
		}

		if(logger.isDebugEnabled()) {
			if(result != null) {
				logger.debug("returning: ");
				logger.debug(result);
			} else {
				logger.debug("no solution..");
			}
		}

		return result;
	}
	
	/**
	 * generates a vector of domain IDs containing
	 * all IDs in the given vector
	 * and all domain IDs for different levels but the same slot as one of the given domains
	 * (the given vector is reset at the end)
	 */
	private List<Integer> addCrossLevelDomains(List<Integer> domains) {
		logger.info("CrossLevelHint!");
		int noOfLvls = net.getGrammar().getNoOfLevels();
		List<Integer> ret = new ArrayList<>(domains.size() * noOfLvls);
		int j, pos;
		Level lvl;

		for(Integer i: domains) {
			pos = i/noOfLvls;
			// we count backwards because we assume the first domain for a slot to be the biggest and
			// optimize domains works faster if small domains are given first
			for(j = noOfLvls-1; j >= 0 ; j--) {
				lvl = net.getGrammar().getLevels().get(j);
				if(!lvl.isUsed()) {
					// level not used, don't add a domain ID for it
					continue;
				}
				ret.add(noOfLvls*pos + lvl.getNo());
			}
		}

		return ret;
	}
	
	protected class PatchSupplier implements Supplier<Patch> {
		private AtomicInteger i;
		private int maxValue;
		private List<Patch> source;

		public PatchSupplier(List<Patch> source) {
			this.source = source;
			this.i = new AtomicInteger(0);
			this.maxValue = source.size()-1;
		}
		@Override
		public Patch get() {
			int j = i.getAndIncrement();
			if (j>maxValue)
				return null;
			return source.get(j);
		}
		
	}

	protected class OptimizeDomainsWorker implements Callable<Object> {
		private final AtomicBoolean poison;
		private final AtomicDouble bestLimit;
		private final AtomicReference<Patch> bestPatch;
		private final AtomicReference<Badness> predictedOptimum;
		private final EnumSet<FrobbingFlag> flags;
		private final PatchSupplier patchSupplier;
		private final Analysis a;
		
		public OptimizeDomainsWorker(AtomicBoolean poison,
				AtomicDouble bestLimit, AtomicReference<Patch> bestPatch,
				AtomicReference<Badness> predictedOptimum,
				EnumSet<FrobbingFlag> flags, PatchSupplier patchSupplier,
				Analysis a) {
			this.poison = poison;
			this.bestLimit = bestLimit;
			this.bestPatch = bestPatch;
			this.predictedOptimum = predictedOptimum;
			this.flags = flags;
			this.patchSupplier = patchSupplier;
			this.a = a;
		}

		@Override
		public Object call() throws TimeUpException {
			Patch p;
			while ((p = patchSupplier.get()) != null) {
				boolean breaked = false;
				/* react to user break */
				if (interrupted) {
					logger.info("Interrupt caught, stopping...");
					throw new TimeUpException();
				}

				/* if an LV in the Patch was deleted by dSearch itself,
			 disregard the entire Patch */
				for (LevelValue lv : p.getSelection()) {
					if (lv!= null && netState.isDeletedLv(lv) && !flags.contains(FrobbingFlag.OD_DELETED)) {
						breaked = true;
						break;
					}
				}
				if (breaked) {
					continue;
				}

				/* obey narrowSearch flag */
				if (flags.contains(FrobbingFlag.OD_NARROW)) {
					if( flags.contains(FrobbingFlag.OD_LIMIT) && p.getLimit() < bestLimit.get() && p.getNoOfNewLVs() > 1 ) {
						continue;
					}
					if( flags.contains(FrobbingFlag.OD_PREDICT) && p.getLimit() < predictedOptimum.get().getScore() ) {
						continue;
					}
				}

				/* p.lexicalize() only fails if the lexeme graph has become
			 disconnected. In this case all further calls to pLexicalize() will
			 also fail, so we can immediately give up. */
				if (!p.lexicalize(netState, flags)) {
					break;
				}

				logger.debug("judging Patch ");
				logger.debug(p);

				/* do not consider Patches that would re-introduce conflicts */
				if (detectCircle(a, p, flags)) {
					continue;
				}

				/* update the best limit of patches we have found so far */
				double tempBest = bestLimit.get();
				double patchLimit = p.getLimit();
				while (patchLimit > tempBest) {
					if (bestLimit.compareAndSet(tempBest, patchLimit))
						break;
					tempBest = bestLimit.get();
				}
				p.judge(evaluator, !flags.contains(FrobbingFlag.OD_NONEWLVS));
				//if (flags.contains(FrobbingFlag.OD_INTERACTIVE)) {
					// logger.info(String.format("%d) ", no));
				//	logger.info(p);
				//}

				/* Now for the tricky part: which of the Patches should be
			 returned? It depends on the method. In classical minConflict
			 style, it is the Patch that will result in the best immediate
			 improvement. */
				// first check if 
				if (flags.contains(FrobbingFlag.OD_PREDICT)
				    // check if we are currently the best before entering the synchronized block
				    && predictedOptimum.get().compare(p.getPredicted())
				    ) {
					synchronized (bestPatch) {
						if (predictedOptimum.get().compare(p.getPredicted())) {
							predictedOptimum.set(p.getPredicted());
							bestPatch.set(p);
						}
					}
				} else {
					/* the best patch is the one with the best limit and the best
				 predicted Badness.

				 But locally non-optimal patches are allowed if they yield an
				 immediate global improvement. */

					if ((p.getLimit() == bestLimit.get() && p.getPredicted().compare(predictedOptimum.get())) ||
							p.getPredicted().compare(best.getBadness())) {
						synchronized (bestPatch) {
							// check if this is still the best patch
							if ((p.getLimit() == bestLimit.get() && p.getPredicted().compare(predictedOptimum.get())) ||
									p.getPredicted().compare(best.getBadness())) {
								predictedOptimum.set(p.getPredicted());
								bestPatch.set(p);
							}
						}

					}
				}

				/* if we have found a method to improve the Analysis globally;
			 don't even bother with further previewing */
				if ((flags.contains(FrobbingFlag.OD_IMMEDIATE) && p.getPredicted().compare(best.getBadness()))) {
					poison.set(true);
					break;
				}
				if (poison.get())
					break;
			}
			// We have to return something since this is Callable
			return null;
		}
	}

	/**
	Optimise n domains in the context of A.

	Performs a dSearch() on the specified domains and applies pJudge() to
	some or all of the resulting patches to decide which should be applied.

	The result is the Patch that is most suitable in the context of the
	current Problem. NULL may be returned if no Patch is possible.

	If the returned Patch is not NULL, it has its field `predicted' set, and
	the value is an accurate prediction of the Badness of A if that Patch
	were applied to it. It has also been successfully been lexicalized and
	can immediately be applied to A.

	Various additional restrictions are obeyed. The variable beamWidth
	limits the number of patches that are considered at all. If a Patch
	fails the detectCircle() test, it is discarded. If OD_IMMEDIATE is set,
	the first Patch that improves on P.best.b stops further previewing.

	Other flags works as in dSearch().
	
	 * @throws InvalidNetStateException 
	 * @throws TimeUpException 
	*/
	public Patch optimizeDomains(Analysis a, List<Integer> which, EnumSet<FrobbingFlag> flags) throws InvalidNetStateException, TimeUpException {
		Stopwatch stopwatch;
		if (performTimingMeasurements) {
			stopwatch = Stopwatch.createStarted();
		}

		
		Badness predictedOptimum = Badness.worstBadness();
		
		List<Patch> patches= new ArrayList<>();
		AtomicReference<Patch> bestPatch = new AtomicReference<>(null);

		double bestLimit = 0.0;

		/* create only single-spot Patches */
		if (flags.contains(FrobbingFlag.OD_SINGLE) ) {
			for (int where : which) {
				patches= dSearch(a, toList(where), patches, flags);
			}
		}

		/* create Patches by combining LVs from the appropriate domains */
		else {
			patches = dSearch(a, which, null, flags);
		}
		
		if(patches.isEmpty())
			return null;

		PatchSupplier patchSupplier = new PatchSupplier(patches);
		OptimizeDomainsWorker w = new OptimizeDomainsWorker(new AtomicBoolean(false), new AtomicDouble(bestLimit), bestPatch, new AtomicReference<>(predictedOptimum), flags, patchSupplier, a);
		List<OptimizeDomainsWorker> workers = new ArrayList<>(numProcessors);
		for (int i = 0; i < numProcessors; i++)
			workers.add(w);
		try {
		executorService.invokeAll(workers);
		} catch (Exception e) {
			if (interrupted)
				throw new TimeUpException();
		}
		if (performTimingMeasurements) {
			logger.info(String.format("Timing: optimizeDomains took %d ms", stopwatch.elapsed(TimeUnit.MILLISECONDS)));
			stopwatch.reset();
		}
		if(bestPatch.get() != null)
			return new Patch(bestPatch.get());
		else 
			return null;
	}
	
	/** just a small helper to wrap a single int into a list */ 
	private ArrayList<Integer> toList(Integer i) {
		ArrayList<Integer> ret = new ArrayList<>(1);
		ret.add(i);
		return ret;
	}
	
	
	/**
	 * If p were applied to A, would A then repeat a conflict from removedConflicts?
	 */
	protected boolean detectCircle(Analysis a, Patch p, EnumSet<FrobbingFlag> flags) {
		boolean result = false;
		Analysis newA;
		int arity;
		ConstraintViolation offender = null;

		/* these LVs are actually checked */
		LevelValue lv1, lv2;
		
		/* We may have to construct an Analysis with a different path
		 selected than a to see whether conflicts would be re-introduced
		 or not. But that is only necessary if p introduces any new
		 lexemes. */
		newA = new Analysis(a);

		/* preview what the Analysis would look like
		 after swapping in the new lexemes */
		newA.selectPath(p.getNewLexemes(), true);

		/* add the new LVs */
		for (LevelValue lv : p.getSelection()) {
			if (lv != null) {
				newA.selectLV(lv);
			}
		}

		/* check all recently removed conflicts */
		for (ConstraintViolation cv : removedConflicts) {
			arity = cv.getConstraint().getVars().size();

			/* find the corresponding LV. */
			lv1 = newA.slotFiller(cv.getNodeBindingIndex1());

			/* check it */
			if (arity == 1 && cv.getConstraint().getSignature().match(lv1)) {
				ConstraintEvaluationResult cer = cv.getConstraint().eval(
						net.getLexemeGraph(), 
						new Context(newA.getSlots()), 
						lv1);
				if(cer.isViolated() && cer.getScore() == cv.getPenalty()) {
					result = true;
					offender= cv;
					break;	
				}
			}

			/* binary conflict */
			else {
				/* Let lv1 and lv2 be the LVs that may or may not re-introduce cv. */
				lv2 = newA.slotFiller(cv.getNodeBindingIndex2());

				if (lv2!= null && newA.lvsCauseConflict(lv1, lv2, cv)) {
					result = true;
					offender= cv;
					break;
				}
			}
		}

		if (result) {
			logger.debug(p);
			logger.debug(" would re-violate ");
			logger.debug(offender);
			logger.debug("\n");
		}
		return result;
	}
	
	/**
	 Search specified domains for good combined assignments.

	 The domains searched are the ones whose number is contained in WHICH.

	 This executes a local search among all LVs of the domains in question.
	 Care is taken not to form tuples that are physically incompatible, nor
	 is the same LV ever added twice to the same tuple, even if WHICH
	 contains duplicates.

	 The result is a List of all possible Patches, ordered by p.compareTo().
	 Only the fields .local and .limit are computed, not the field
	 .predicted (see optimizeDomains() for a way to do that).

	 FLAGS specifies particular kinds of behaviour. 
	 See FrobbingFlag.java for a documentation of the flags

	 Note that these flags are used by various other functions that ultimately
	 call or are called by dSearch(), such as optimizeDomains() and pNext(),
	 and not all flags are used by all of them. For instance, OD_IMMEDIATE
	 is read only by optimizeDomains() since it implies Patch previewing.

	 The set of possible transformations is generated recursively, using an
	 Agenda with size localSize. Therefore a simultaneous search in many
	 domains will usually be incomplete.

	 If the search was not truncated (and OD_PRUNE is set), the result of the
	 search is used to propagate limits to LVs from all domains searched.
	 Each LV is assigned a limit that is not higher than the best limit of a
	 Patch it appears in.
	 
	 * @throws InvalidNetStateException 
	 * @throws TimeUpException

	 */
	private List<Patch> dSearch(Analysis a, List<Integer> which, 
			List<Patch> sofar, EnumSet<FrobbingFlag> flags) throws InvalidNetStateException, TimeUpException {
		List<Patch> result;
		if(sofar== null) {
			result = new ArrayList<>();
		} else {
			result = sofar;
		}
		
		MinMaxPriorityQueue<Patch> ag;
		List<Double> localLimits = getLimitBuffer();
		double bestLimit = 0.0;
		int i, width, reg;

		/* Do not trust the caller; several other invocation options
		 would render pruning invalid, so test them explicitly. */
		boolean doPruning = 
				flags.contains(FrobbingFlag.OD_PRUNE) && 
				!flags.contains(FrobbingFlag.OD_SWAP) && 
				flags.containsAll(OD_ALLOWALL);
		LevelValue currentLv;
		boolean debug = logger.isDebugEnabled();

		List<Integer> allowedRegents = null;

		Context context;
		
		width = which.size();
		
		if (debug) {
			logger.debug("\ndSearch(");
			for (Integer j: which) {
				logger.debug(String.format("@%d ", j));
			}
			logger.debug(")\n");
		}

		if (doPruning)
			netState.checkDomains(which, globalMinimum);

		// for swapping, determine the allowed regents
		if(flags.contains(FrobbingFlag.OD_SWAP)) {
			allowedRegents = new ArrayList<>(width);
			for(Integer index : which) {
				LevelValue lv = a.getSlots().get(index);
				reg = lv.getRegentGN().getNo();
				if(!allowedRegents.contains(reg)) {
					//cdgPrintf(CDG_INFO, "allowed regent: %s \n", lvRegent(lv, FALSE));
					allowedRegents.add(reg);
				}
			}

//			allowedLabels = vectorNew( vectorSize(which));
//			for(i = 0; i<vectorSize(which); i++) {
//				index = (int) vectorElement(which, i);
//				lv = vectorElement(a.slots, index);
//				if(!vectorContains(allowedLabels, strRegister(lv.label))) {
//					cdgPrintf(CDG_INFO, "allowed label: %s \n", lv.label);
//					vectorAddElement(allowedLabels, strRegister(lv.label));
//				}
//			}
		}

		/* conduct local search */
		int agendaMaximumSize = config.getInt(AGENDASIZE);
		if (agendaMaximumSize>0) {
			ag = MinMaxPriorityQueue.maximumSize(agendaMaximumSize).create();
		} else {
			ag = MinMaxPriorityQueue.create();
		}
		ag.add(new Patch(a, width, flags, netState.getLnDeletion()));

		boolean agIsTruncated = false;
		while (!ag.isEmpty()) {

			/* read */
			Patch p = ag.poll();

			if (debug) {
				logger.debug("DEBUG: read ");
				logger.debug(p);
			}

			/* time limit reached? */
			/* interrupt? */
			if (interrupted) {
				throw new TimeUpException();
			}

			/* Should we delete Patches with suboptimal limits or not?

			 This is one of the truly important parts of the module.
			 Basically, we only want to investigate Patches with the optimal
			 limit, because that is the idea behind having limits. That is
			 the first line of the condition.

			 Also, a Patch with a suboptimal limit may still be useful for
			 assessing the limits of the LVs contained in it. So we only
			 want to suppress Patches if we already know that that stage
			 will be skipped later on -- because the agenda overflowed, or
			 because the user has told us not to. That is the second line of
			 the condition. */
			if (flags.contains(FrobbingFlag.OD_LIMIT) && p.getLimit() < bestLimit &&
					(agIsTruncated || !doPruning)) {
				if (debug)
					logger.debug(p);
				continue;
			}

			/* Since in general word graphs, one lexeme can cover multiple
			 intervals, one LV can bind multiple domains. It is therefore
			 possible that we have already bound the next domain ordered. */

			/* find the next domain to bind */
			while (p.getIndex() < width) {
				/* next domain ordered */
				i = which.get(p.getIndex());
				
				/* bound already? */
				if (!p.getIsBound(i)) {
					break;
				} else {
					/* skip it */
					p.select(null);
				}
			}


			/* retain */
			if (p.getIndex() == width) {

				if (debug) {
					logger.debug("DEBUG: retained ");
					logger.debug(p);
				}

				/* apply context-sensitive constraints */
				if (flags.contains(FrobbingFlag.OD_CONTEXT)) {
					ArrayList<LevelValue> slots = new ArrayList<>(net.getNoOfSlots());
					for (LevelValue lv : p.getSelection()) {
						if (lv != null) {
							slots.set(lv.getIndex(), lv);
						}
					}
					context = new Context(slots);

					GrammarEvaluationResult ger =evaluator.evalInContext(p.getSelection(), context, net.getLexemeGraph());
					p.setLocal(ger.getBadness());

					/* maybe update limit */
					if (p.getLocal().soft < p.getLimit()) {
						p.setLimit(p.getLocal().soft);
					}
					if (p.getLocal().hard >0) {
						p.setLimit(0.0);
					}
				}

				/* insert into a sorted list */
				ListIterator<Patch> li = result.listIterator();
				while(li.hasNext() && li.next().compare(p)) { /* nothing, next iterates already*/ }
				li.add(p);
				
				if (p.getLimit() > bestLimit) {
					bestLimit = p.getLimit();
				}
				continue;
			}

			/* extend */
			i = which.get(p.getIndex());
			
			if (debug) {
				logger.debug(String.format("considering @%d (length %d)", i, netState.getDomain(i).size()));
			}

			currentLv = a.slotFiller(i);
			for (LevelValue lv : netState.getDomain(i)) {
				// skip attachments to blocked virtual nodes
				// TODO #VN blockVirtualNodes
//				if(blockVirtualNodes && a.P.lg.ve && lvGetRegentGN(lv) &&
//						bvElement(a.P.lg.ve.blocked,lvGetRegentGN(lv).no) ) {
//					continue;
//				}

				if (debug) {
					logger.debug(String.format("Trying #%d... ", lv.getIndexWRTNet()));
				}

				/* reject deleted LV */
				if (!flags.contains(FrobbingFlag.OD_DELETED) && netState.isDeletedLv(lv)) {
					if (debug) 
						logger.debug(String.format("%d is deleted", lv.getIndexWRTNet()));
					continue;
				}
				// for swapping reject not allowed regents and labels
				if(flags.contains(FrobbingFlag.OD_SWAP)) {
					reg = lv.getRegentGN().getNo();
					if(!allowedRegents.contains(reg)) {
						continue;
					}

//					if(!vectorContains(allowedLabels, strRegister(lv.label))) {
//						continue;
//					}
				}

				Patch newP = p.next(lv, netState, evaluator, globalMinimum, flags);
				if (newP != null) {
					/* is it an actual change, or merely a substitution X --> X? */
					if (lv != currentLv)
						newP.incNrNewLVs();
					
					/* write */
					if (!agIsTruncated && ag.size() >= agendaMaximumSize)
						agIsTruncated = true;
					ag.add(newP);

					if (debug) {
						logger.debug("DEBUG: wrote ");
						logger.debug(newP);
					}
				}
			} // end for (l = a.P.domains[i]; l; l = l.next)
		} // end while (!agIsEmpty(ag))

		/* Now, propagate limits to all LVs in the searched domains. */
		if (doPruning && !agIsTruncated) {
			/* update global limit */
			if (globalMaximum > bestLimit) {
				globalMaximum = bestLimit;
				logger.info(String.format("it won't get any better than %1.5f",
						globalMaximum));
			}

			/* if an LV appears in a Patch with limit f,
			 then its limit may be as high as that */
			for (Patch p : result) {
				if (p.getIndex() < width)
					continue;
				for (LevelValue lv: p.getSelection()) {
					if (lv == null)
						continue;
					int index = lv.getIndexWRTNet();

					if (localLimits.get(index) < p.getLimit()) {
							if (debug) 
								logger.debug(String.format("#%d can attain %4.3e",
										index, p.getLimit()));
						localLimits.set(index, p.getLimit());
					}
				}
			}

			/* degrade LVs that do not cooperate */
			for (i = 0; i < width; i++) {
				int index = which.get(i);
				
				for (LevelValue lv : netState.getDomain(index)) {
					
					if (netState.isDeletedLv(lv))
						continue;

					int j = lv.getIndexWRTNet();

					if (localLimits.get(j) < netState.getLvLimit(lv)) {
						if (debug) {
							logger.debug(String.format("limit(#%d) = %4.3e",
									lv.getIndexWRTNet(), localLimits.get(j)));
						}
						netState.downgrade(lv, localLimits.get(j), globalMinimum);
					}
				}
			}
		}
		return result;
	}

	/**
	 * returns a number array the size of the number of levelValues in the problem
	 * such an array is cached in the problem, to minimize reallocation
	 * it is only reallocated if the number of LVs changed since last allocation
	 *
	 * The given array cannot be shared by different function calls, so two function that use this
	 * buffer may never call each other, be aware!!!
	 */
	private List<Double> getLimitBuffer() {
		int i;

		if(limitBuffer == null || net.getNoOfLVs() != limitBuffer.size()) {
			limitBuffer = new ArrayList<>(net.getNoOfLVs());
		} else {
			limitBuffer.clear();
		}

		/* table of locally determined limits */
		for (i = 0; i < net.getNoOfLVs(); i++) {
			limitBuffer.add(0.0);
		}

		return limitBuffer;
	}	
	
	/**
	 Revert to the best known location in the search space;
	 that is, make current a clone of best.
	 */
	public void traceBack() {
		if (current != null)
			current = null;
		if (best != null)
			current = new Analysis(best);
		reset();
	}

	/**
	 Reset the transformation history info for a Problem.
	 */	
	public void reset() {
		removedConflicts.clear();
		history.clear();
		current.sortConflicts((best != null)? best.getConflicts() : new ArrayList<>() );
	}
	
	/**
	 * returns true, if a timelimit is set (i.e. > 0) 
	 * and the elapsed time (time since the starttime was set for the stats) is above the limit
	 * all times in milli seconds
	 */
	public boolean timerExpired() {
		if(timeLimit <= 0 )
			return false;
		long elapsed = System.currentTimeMillis() - stats.getStartTime();
		return elapsed > timeLimit;
	}
	
	/**
	 Register a new globally best Analysis.

	 This function registers a new globally optimal score found for an
	 Analysis. It deallocates P.best and replaces it with a clone of
	 a. If the Analysis carries a positive score, this is assigned
	 to globalMinimum. If a conflict found in unremovable does not also
	 appear in a.conflicts, it is removed from unremovable
	 (since it was obviously not unremovable).
	 
	 * @throws InvalidNetStateException 
	 */
	protected void newMaximum(Analysis a) throws InvalidNetStateException {
		//logger.info("new maximum found");
		double f = 1.0;

		stats.solFound( 
				a.isValid() && 
				a.getSubStart() == net.getMinTimepoint() && 
				a.getSubStop( ) == net.getMaxTimepoint() );

		reset();
		
		/* register new record for Badness */
		best = new Analysis(a);

		/* note new record for score */
		if (best.isValid()) {
			if(best.getBadness().soft > globalMinimum) {
				logger.info(String.format("increased score: %1.5f -> %1.5f", globalMinimum, best.getBadness().soft));
				globalMinimum = best.getBadness().soft;
			}

			LinkedList<Integer> l = new LinkedList<>();
			for (int i = 0; i < net.getNoOfSlots(); i++) {
				
				if (!a.slotActive(i))
					continue;
				
				l.offerFirst(i);
				
				// just checking for a simple inconsistency regarding the limits of participyting LVs
				LevelValue lv = a.getSlots().get(i);
				if (Badness.significantlyGreater(a.getBadness().getScore(), lv.getScore())) {
					logger.warn(String.format("Score of %s is smaller than Analysis it occurs in (%.5f < %.5f)", 
							lv.toString(),
							lv.getScore(), 
							a.getBadness().getScore()));
					StringBuilder s = new StringBuilder("Constraints it violates:");
					for (Constraint c : lv.getConstraints()) {
						s.append("\n");
						s.append(c.toString());
					}
					logger.warn(s.toString());
				}

			}
			netState.checkDomains(l, globalMinimum);
		}

		/* groom the Analysis a bit */
		best.calibrate();

		/* If an `unremovable' conflict was removed anyway,
		 it was obviously not unremovable.
		 So we retract it from the List.  */
		LinkedList<ConstraintViolation> m = new LinkedList<>();
		for (ConstraintViolation cv : unremovable) {
			if (!best.getConflicts().contains(cv)) {
				logger.debug(String.format("Hey, `%s' was removed after all...",
						cv.getConstraint().getId()));
				f = Math.min(f, cv.getPenalty());
			}

			/* By the same token, retract all softer conflicts from this list as
			 well.
			 If we were wrong about not being able to repair a hard conflict, we
			 could also have been wrong about not being able to repair a soft
			 conflict. */
			/* Removing the following constraints in the same loop works
			 * because P.unremovable is always sorted in descending order
			 * so it's guaranteed that all less-bad conflicts than the
			 * ones removed above get removed. This ordering stems from
			 * the way frobbing works: it attack the conflicts in
			 * descending order.
			 */
			else if (cv.getPenalty() > f) {
				logger.debug(String.format("I'll take another shot at `%s'.",
						cv.getConstraint().getId()));
			}

			/* yup, still obstinate */
			else {
				m.offerFirst(cv);
			}
		}
		unremovable = m;

		/* hand the Parse to observers for displaying */
		if (!config.getFlag(suppressPartialSolutions) || config.getFlag(alwaysShowAllSolutions)) { //TODO this should be done by the observers
			DecoratedParse p = best.toParse(config, evaluator);
			ParseInfo info = p.getInfo();
			info.setEvaluationTime(System.currentTimeMillis() - stats.getStartTime());
			info.setSearchStrategy("Frobbing");
			info.setEntry("Frobbing Method", this.methodName());
			registerParse(p);
		}
	}
	
	public abstract String methodName();

	/**
	 Detect conflicts introduced by repair.

	 A conflict CV is `recent' if there is a `best' Analysis
	 and CV is not in it.
	 */
	public boolean isRecent(ConstraintViolation cv) {
		if (best == null)
			return false;
		
		return !best.getConflicts().contains(cv);
	}

	/**
	 * Detect unremovable conflicts.
	 * This simply returns TRUE if CV is in unremovable.
	 */
	public boolean isUnremovable(ConstraintViolation cv) {
		return unremovable.contains(cv);
	}
	
	public void registerObserver(ParseObserver obs) {
		observers.add(obs);
	}
	
	public void registerObservers(List<ParseObserver> obs) {
		observers.addAll(obs);
	}

	protected List<ParseObserver> getObservers() {
		return observers;
	}

	protected void registerParse(DecoratedParse p) {
		for(ParseObserver obs : observers) {
			obs.register(p);
		}
	}
	
	/**
	 * Add LVs to an Analysis so that it binds slots 0 through slot.
	 */
	public void extendAnalysis(Analysis a, int slot) {
		
		/* bound already? */
		if (a.slotFiller(slot) != null) {
			return;
		}
		
//		for (LevelValue lv : netState.getDomain(slot)) {
//			System.out.println(String.format("%1.3f %1.3f %s",
//					netState.getLvLimit(lv),
//					lv.getScore(),
//					netState.isDeletedLv(lv)?"DEL":""));
//		}

		/* select the first undeleted LV */
		for (LevelValue lv : netState.getDomain(slot)) {
			if (!netState.isDeletedLv(lv) && 
					!(lv.getDependentGN().isVirtual() && !lv.getLabel().equals("") && config.getFlag(VirtualNodeSpec.INIT_VNS_UNUSED))) {
				a.insertLv(lv, netState, null);
				return;
			}
		}

		/* if there is no undeleted LV, take the first one */
		a.insertLv(netState.getDomain(slot).get(0), netState, null);		
	}
	
	/**
	 * When the pressure specified by the user turns out too great for the
	 * problem, halve it and reset the net, because all the deletions done
	 * so far cannot be trusted any more.
	 * 
	 * However, all is not lost; the partial solution found during the
	 * high-pressure search is likely to be useful for further search. 
	 */
	protected boolean reducePressure() {

		reset();
		globalMaximum = 1.0;

		if (globalMinimum < 1e-8) {
			logger.warn("Cannot reduce pressure further...");
			logger.warn("Perhaps there *is* no solution?");
			return false;
		}

		globalMinimum /= 10.0;
		logger.info(String.format("Pressure too great, resetting to %5f", globalMinimum));
		restoreSavedState();

		/* By restoring the entire Problem, we have repaired the lexeme
		 graph and reinstated its distance information. Since this information
		 is used by the predicate distance(), each and every constraint formula
		 has to be re-evaluated now, even in unary constraints. */
		current.judge(evaluator);
		best.judge(evaluator);

		return true;
	}
	
	@Override
	public Analysis getBest() {
		return best;
	}
	
	@Override
	public DecoratedParse getSolution() {
		return solution;
	}
	
	@Override
	public void interrupt() {
		interrupted = true;
	}
	
	public void setTimeLimit(int timeLimit) {
		this.timeLimit = timeLimit;
	}
}
