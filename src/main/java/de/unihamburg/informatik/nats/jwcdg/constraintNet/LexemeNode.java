package de.unihamburg.informatik.nats.jwcdg.constraintNet;

import java.io.Serializable;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.avms.Path.PseudoFeature;
import de.unihamburg.informatik.nats.jwcdg.input.LexiconItem;
import de.unihamburg.informatik.nats.jwcdg.lattice.Arc;


/**
A LexemNode represents the hypothesis of a specific lexical variant
for a specific time interval.

The field \a no is the index of the lexeme node in the field
\a nodes of the enclosing lexeme graph.

The field \a lexemgraph point to the enclosing lexeme graph.

The field \a arc points to the underlying Arc.

The field \a lexem points to the lexical entry postulated.

The field \a grapheme points to the grapheme node used to build the
lexeme node.

The field \a limit corresponds to the field \a limit in an LV: a
lexeme node with limit \a x can only appear in solutions not better than
\a x (see Frobbing)
*/
public class LexemeNode implements Serializable{
	
	
    private static final long serialVersionUID = 1L;
    private static AVMError unimplementedPSeudoFeatureError =
			new AVMError("Unsupported feature!");
	private static AVMError chunkingNotImplementedFeatureError =
			new AVMError("Chunking is not supported yet!");
	
	/** index in LexemGraph::nodes */
	private int no;
	
	/** lexicon entry */
	private LexiconItem lexeme;
	
	/** pointer back to the original grapheme */
	private GraphemeNode grapheme;
	
	/** is this a lexeme of a virtual node */
	private boolean isVirtual;
	
	/** cached AVMs so it does not need to be */
	private AVMLexemNode selfVal;
	private AVMString    wordVal;
	
	public LexemeNode(int no, GraphemeNode gn, LexiconItem li) {
		this.no        = no;
		this.grapheme  = gn;
		this.lexeme    = li;
		this.isVirtual = gn.isVirtual();
		
		this.selfVal   = new AVMLexemNode(this);
		this.wordVal   = new AVMString( (li!=null) ? lexeme.getWord() : "");
	}

	public Arc getArc() {
		return grapheme.getArc();
	}

	public int getNo() {
		return no;
	}
	
	/**
	 @brief Do these lexeme nodes overlap?

	 Returns TRUE if the two lexeme nodes have at least one time point in common.

	 This is subtly different from the more common question, "Can the two nodes
	 coexist on one path?": two nodes can be compatible although they overlap if
	 they are identical. Conversely, a and b may be incompatible even if they do
	 not overlap if there is no path between them.
	 */
	public boolean overlaps(LexemeNode lnb) {
		Arc arc = grapheme.getArc();
		Arc arc2 = lnb.getArc();
		return ((arc.to > arc2.from && arc.from <= arc2.from)
				|| (arc2.to > arc.from && arc2.from <= arc.from));
	}

	public LexiconItem getLexeme() {
		return lexeme;
	}

	public boolean isVirtual() {
		return isVirtual;
	}
	
	@Override
	public String toString() {
		return String.format("(%d-%d)%s", getArc().from, getArc().to, lexeme.getDescription());
	}
	
	/**
	 * extracts the given lexical or pseudofeature
	 * if no such feature exists, an AVMError is returned
	 * 
	 * @param path
	 * @return
	 */
	public AVM getFeature(Path path) {
		if(path != null && path.isPseudoFeature()) {
			PseudoFeature pf = path.toPseudoFeature();
			switch (pf) {
			case id:
				return selfVal;
			case word:
				return wordVal;
			case from:
				return getArc().getFromVal();
			case to:
				return getArc().getToVal();
			case grapheme:
				return getArc().getWordVal();
			case info:
				return getArc().getInfo();
			case chunk_start:
			case chunk_end:
			case chunk_type:
				return chunkingNotImplementedFeatureError;
			default:
				return unimplementedPSeudoFeatureError;
			}
		} else {
			return lexeme.getFeature(path);
		}
	}

	public boolean spec() {
		return !isNil() && !isNonspec();
	}

	public boolean isNil() {
		// nil subclass will handle this
		return false;
	}
	
	public boolean isNonspec() {
		// nonspec subclass will handle this
		return false;
	}
	

	public GraphemeNode getGN() {
		return grapheme;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return no;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof LexemeNode))
			return false;
		LexemeNode other = (LexemeNode) obj;
		if (no != other.no)
			return false;
		return true;
	}
}
