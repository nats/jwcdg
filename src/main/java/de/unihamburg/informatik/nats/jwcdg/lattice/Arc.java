package de.unihamburg.informatik.nats.jwcdg.lattice;

import java.io.Serializable;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;



/**
 * A arc in a word lattice
 * corresponding to a word hypothesis
 * 
 * @author the CDG-Team
 *
 */
public class Arc implements Serializable{
	
    private static final long serialVersionUID = 1L;
    /** start node */
	public final int from;
	/** end node */
	public final int to;
	/** unique identifier */
	public final int no;
	/** lexem */
	public final String word;
	/** penalty factor */
	private double penalty;
	/** infostring */
	private AVM info;
	
	/** caches for AMS wrappers */
	private AVMNumber fromVal;
	private AVMNumber toVal;
	private AVMString wordVal;

	/* See inputScanLattice() for an explanation of these three values. */
	/** nesting level of quotation */
	private int quotes;
	/** nesting level of phrase quotation */
	private int phraseQuotes;
	/** nesting level of parentheses */
	private int parens;
	
	
	public Arc(int from, int to, int no, String word, double penalty) {
		this.from    = from;
		this.to      = to;
		this.no      = no;
		this.word    = word;
		this.penalty = penalty;
		this.quotes       = 0;
		this.phraseQuotes = 0;
		this.parens       = 0;
		
		fromVal = new AVMNumber(from);
		toVal   = new AVMNumber(to);
		wordVal = new AVMString(word); 
		
		this.info = new AVMError("No info available");
	}

	public double getPenalty() {
		return penalty;
	}

	public int getQuotes() {
		return quotes;
	}

	public int getPhraseQuotes() {
		return phraseQuotes;
	}

	public int getParens() {
		return parens;
	}

	public void setQuotes(int quotes) {
		this.quotes = quotes;
	}

	public void setPhraseQuotes(int phraseQuotes) {
		this.phraseQuotes = phraseQuotes;
	}

	public void setParens(int parens) {
		this.parens = parens;
	}

	/**
	   Does this Arc carry a word that might indicate a sentence boundary?

	   This is only true of some punctuation marks.
	*/
	public boolean isSentenceBoundary() {
		return (	word.equals("\"") ||
					word.equals("``") ||
		    		word.equals("`") ||
		    		word.equals("'") ||
		    		word.equals("(") ||
		    		word.equals(")") ||
		     		word.equals("[") ||
		    		word.equals("]") ||
		    		word.equals("-") ||
		    		word.equals(",") ||
		    		word.equals("?") ||
		    		word.equals("/") ||
		    		word.equals(":") ||
		    		word.endsWith(")") ||
		    		word.endsWith(".") ||
		    		word.matches("\\*+") 
		    	);
	}

	public boolean inputALLUPPER() {
		return word.matches("[A-Z]+");
	}

	/**
	 * whether this arc shares a timespan with another
	 * 
	 * @param other
	 * @return
	 */
	public boolean overlaps(Arc other) {
		return (this.to > other.from) && (this.from < other.to); 
	}
	
	public AVMNumber getFromVal() {
		return fromVal;
	}
	
	public AVMNumber getToVal() {
		return toVal;
	}
	
	public AVMString getWordVal() {
		return wordVal;
	}
	
	@Override
	public String toString() {
		return word+ "(" + from + "-" +to + ")";
	}

	public AVM getInfo() {
		return info;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		
		if (obj instanceof Arc) {
			Arc other = (Arc) obj;
			
			if ( no != other.no || from != other.from || to != other.to || !word.equals(other.word)) {
				return false;
			}
			
			return true;
		}
		
		return false;
	}

	@Override
	public int hashCode() {
		return no;
	}
}