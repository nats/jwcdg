package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.lattice.Arc;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;


/**
 * test if one of the specified punctuation marks separates the two words
 * 
 * Does a punctuation mark contained in S occur between two words?
The first and second parameters are lexeme nodes, and the third parameter
is a string. TRUE is returned if one of the words between the two lexeme
nodes is one character long and consists of one of the characters in the
string.

The first and second parameters may also be numbers, which stand for the
corresponding time points in the lexeme graph. The sentinel value -1
means lg->min and lg->max, respectively.

Either of the lexeme nodes may be replaced by a Number, which is then
used as an index into the lexeme graph instead.
 */
public class PredicateBetween extends Predicate {

	public PredicateBetween(Grammar g) {
		super("between", g, false, 3, false); 
	}

	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {

		int a, b;
		LexemeNode ln;
		String s;

		AVM val = args.get(0).eval(lg, context, binding);		
		if (val instanceof AVMLexemNode) {
			ln = ((AVMLexemNode)val).getLexemeNode();
			if (ln == null) {
				return false;
			}
			if (ln.isNonspec()) {
				a = lg.getMax() + 1;
			} else {
				a = ln.getArc().from;
			}
		} else if (val instanceof AVMNumber) {
			a = ((AVMNumber)val).getInteger();
			if (a == -1) {
				a = lg.getMin();
			}
		} else {
			throw new PredicateEvaluationException("type mismatch (1st arg) for `between'");
		}

		AVM val2 = args.get(1).eval(lg, context, binding);
		if (val2 instanceof AVMLexemNode) {
			ln = ((AVMLexemNode)val2).getLexemeNode();
			if (ln == null) {
				return false;
			}
			if (ln.isNonspec()) {
				b = lg.getMax() + 1;
			} else {
				b = ln.getArc().from;
			}
		} else if (val2 instanceof AVMNumber) {
			b = ((AVMNumber)val2).getInteger();
			if (b == -1) {
				b = lg.getMin();
			}
		} else {
			throw new PredicateEvaluationException("type mismatch (2nd arg) for `between'");
		}

		if (a > b) {
			int c = a;
			a = b;
			b = c;
		}

		AVM val3 = args.get(2).eval(lg, context, binding);
		if (!(val3 instanceof AVMString)) {
			throw new PredicateEvaluationException("type mismatch (3rd arg) for `between'");
		}

		s = ((AVMString) val3).getString();

		return predBetweenImpl(a, b, s, lg, context);
	}

	private boolean predBetweenImpl(int a, int b, String s, LexemeGraph lg,
			Context context) {
		for (Arc arc : lg.getLattice().getArcs()) {
			if (arc.from < a || arc.from > b)
				continue;

			if (!s.contains(arc.word))
				continue;

			return true;
		}
		return false;
	}

}
