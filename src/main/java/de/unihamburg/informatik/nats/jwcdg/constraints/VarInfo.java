package de.unihamburg.informatik.nats.jwcdg.constraints;


// import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;

public class VarInfo {
	public final String     varname;   /* print name of the variable ("X" in {X:syn})*/
	public final Level      level; /* only valid for this level ("syn" in {X:syn} ) */
	public final int        index;  /*the index of this var in the sigpature of the constraint*/
	public final Direction  dir;       /* declared direction of the edge */
	
	/**
	 * @param name
	 * @param level
	 * @param dir
	 */
	public VarInfo(String name, Direction dir, Level level, int index) {
		this.varname = name;
		this.level = level;
		this.dir = dir;
		this.index = index;
	}
	
	@Override
	public String toString() {
		return String.format("{%s%s%s}", varname, dir.form, level.getId());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dir == null) ? 0 : dir.hashCode());
		result = prime * result + ((level == null) ? 0 : level.hashCode());
		result = prime * result + ((varname == null) ? 0 : varname.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof VarInfo))
			return false;
		VarInfo other = (VarInfo) obj;
		if (dir != other.dir)
			return false;
		if (level == null) {
			if (other.level != null)
				return false;
		} else if (!level.equals(other.level))
			return false;
		if (varname == null) {
			if (other.varname != null)
				return false;
		} else if (!varname.equals(other.varname))
			return false;
		return true;
	}
}
