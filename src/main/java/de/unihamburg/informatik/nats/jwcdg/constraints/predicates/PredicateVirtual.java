package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * "returns TRUE if argument is virtual, second argument is the reason for this check, so specific checks can be disabled"
 * 
 * @author Niels Beuck
 *
 */
public class PredicateVirtual extends Predicate {
	
	public static final String VIRT_EXCEPTIONS = "virtExceptions";

	public PredicateVirtual(Grammar g) {
		super("virtual", g, false, 2, false);
	}

	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {
		
		AVM val1 = args.get(0).eval(lg, context, binding);
		AVM val2 = args.get(1).eval(lg, context, binding);
		
		AVMLexemNode lnval;
		AVMString    sval;
		
		if (val1 instanceof AVMLexemNode) {
			lnval = (AVMLexemNode) val1;
		} else {
			throw new PredicateEvaluationException("1st argument of predicate `virtual' not a lexeme node");
		}
		
		if (val2 instanceof AVMString) {
			sval = (AVMString) val2;
		} else {
			throw new PredicateEvaluationException("2nd argument of predicate `virtual' not a string");
		}
		
		LexemeNode ln = lnval.getLexemeNode();
		
		String s = sval.getString();
		for(String s2 : lg.getConfig().getList(VIRT_EXCEPTIONS)) {
			if( s.equals(s2) && !s2.equals("")) {
				return false;
			}
		}

		return ln.isVirtual();
	}
}
