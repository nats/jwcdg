package de.unihamburg.informatik.nats.jwcdg.parse;

import com.google.common.collect.Lists;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarLexer;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarParser;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.*;
import de.unihamburg.informatik.nats.jwcdg.constraints.Level;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluationResult;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.frobbing.CombinedFactory;
import de.unihamburg.informatik.nats.jwcdg.incrementality.virtualNodes.VirtualLexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.incrementality.virtualNodes.VirtualNodeSpec;
import de.unihamburg.informatik.nats.jwcdg.input.*;
import de.unihamburg.informatik.nats.jwcdg.lattice.Arc;
import de.unihamburg.informatik.nats.jwcdg.lattice.Lattice;
import de.unihamburg.informatik.nats.jwcdg.transform.Analysis;
import de.unihamburg.informatik.nats.jwcdg.transform.Badness;
import de.unihamburg.informatik.nats.jwcdg.transform.ConstraintViolation;
import de.unihamburg.informatik.nats.jwcdg.transform.ProblemSolver;
import io.gitlab.nats.deptreeviz.ParseInterface;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Search algorithm independent representation of a dependency structure 
 * 
 * @author the CDG-Team
 *
 */
public class Parse implements ParseInterface<Word>, Serializable{
	
    private static final long serialVersionUID = 1L;
	/* ***********
	 *  Statics  *
	 *********** */
	public static final String ANNO_FEATS = "annoFeats";

	//private static final String DECORATE_PARSE_ALL_LEXEME_VARIANTS = "decorateParseAllLexemeVariants";
	
	private static int idCounter = 0; 
	
	private static String makeId() {
		return "parse" + idCounter++;
	}
	
	static Pattern pronPat = Pattern.compile("^P(?:POS|I|D|REL|W)S$");
	static Pattern prepPat = Pattern.compile("^APP(?:R|RART|O|Z)$");
	
	private static Logger logger = LogManager.getLogger(Parse.class);
	
	
	/* ***********
	 *  Fields  *
	 *********** */
	/** id of the parse in the list of global parses */
	protected String  id;                
	
	/** Vector of all word nodes        */
	protected List<Word  > words;
	/** List of the used levels (Strings) */
	protected List<String> levels;
	
	/** holds, as an int, the elder node for each node i in level j */
	protected Map<String, List<Integer>> verticesStructure;
	/** holds the label for every vertex (i, j) */
	protected Map<String, List<String>>  verticesLabels;
	
	protected ParseInfo info;
	
	/**
	 * Create parse object from conll string. Creates SYN edges as well as empty REF edges.
	 */
	public static Parse fromConll(String conll) {
		List<WordAnno> annos = new ArrayList<>();
		int num = 0;
		for (String line : conll.split("\n")) {
			String[] args = line.split("\t");
			List<AnnoSpecification> specs = new ArrayList<>();
			specs.add(new AnnoSpecificationTag("cat", args[4]));
			specs.add(new AnnoSpecificationDep("SYN", args[7], Integer.parseInt(args[6])));
			specs.add(new AnnoSpecificationDep("REF", "", 0));
			WordAnno wa = new WordAnno(num, num+1, args[1], specs);
			annos.add(wa);
			num += 1;
		}
		Parse p = new Parse("conll", annos);
		System.out.println("Before removeUnused:");
		System.out.println(p.toAnnotation(false));
		p.removeUnusedVirtualNodes();
		System.out.println("After removeUnused:");
		System.out.println(p.toAnnotation(false));
		return p;
	}

	/**
	 * Create parse obeject from cda string.
	 */
	public static Parse fromCda(String cda) throws RecognitionException {
		jwcdggrammarParser gParser =
				new jwcdggrammarParser(new CommonTokenStream(new jwcdggrammarLexer(new ANTLRStringStream(cda))));

		return gParser.annoEntry();
	}

	public void removeUnusedVirtualNodes() {
		String lastWord = this.words.get(this.words.size()-1).word;
		if (! lastWord.equals("[unused]"))
			return;
		int unkindex = this.words.size()-1;
		System.out.println("Unk index: " + unkindex);
		System.out.println(verticesStructure.get("SYN"));
		verticesLabels.get("SYN").remove(unkindex);
		System.out.println("removed: " + unkindex);
		verticesStructure.get("SYN").remove(unkindex);
		verticesLabels.get("REF").remove(unkindex);
		verticesStructure.get("REF").remove(unkindex);
		words.remove(unkindex);
		ArrayList<Integer> toRemove = new ArrayList<>();
		for (int i = unkindex-1; i > 0; i--) {
			if (verticesStructure.get("SYN").get(i) == unkindex) {
				System.out.println("To Remove: "+i);
				toRemove.add(i);
			}
		}
		System.out.println("2");
		for (int i = unkindex-1; i>=0; i--) {
			if (verticesStructure.get("SYN").get(i) == unkindex) {
				System.out.println("Removing "+ i);
				verticesLabels.get("SYN").remove(i);
				verticesStructure.get("SYN").remove(i);
				verticesLabels.get("REF").remove(i);
				verticesStructure.get("REF").remove(i);
				words.remove(i);
			} else {
				int newHead = adjustWordIndex(verticesStructure.get("SYN").get(i), toRemove);
				verticesStructure.get("SYN").set(i, newHead);
			}
		}
		System.out.println(verticesStructure.get("SYN"));
		System.out.println(verticesLabels.get("SYN"));
		System.out.println(verticesStructure.get("REF"));
		System.out.println(verticesLabels.get("REF"));
		System.out.println(words);
	}
	/**
	 * Return a new Parse structure with nothing set. Take the argument as the
	 * id of the new parse, or create an old-style parse0, parse1, ... id.
	 */
	public Parse(String id) {
		if (id.equals("")) {
			this.id = makeId(); 
		} else {
			this.id = id;
		}
		
		this.words  = new ArrayList<>();
		this.levels = new ArrayList<>();
		this.verticesStructure = new HashMap<>();
		this.verticesLabels    = new HashMap<>();
		this.info = new ParseInfo();
	}
	
	/**
	 * 
	 * @param id
	 * @param net
	 */
	public Parse(String id, ConstraintNet net) {
		this(id);
		for(Level lvl : net.getGrammar().getLevels()) {
			if(lvl.isUsed()) {
				levels.add(lvl.getId());
			}
		}
	}
	
	/**
	 * 
	 * @param id
	 * @param wordAnnos
	 */
	public Parse(String id, List<WordAnno> wordAnnos) {
		this(id);
		
		for(WordAnno wa : wordAnnos) {
			Map<String, String> features = new HashMap<>();
			for(AnnoSpecification as : wa.specs) {
				if(as instanceof AnnoSpecificationTag) {
					features.put(as.kind, as.name);
				} else if(as instanceof AnnoSpecificationDep) {
					AnnoSpecificationDep dep = (AnnoSpecificationDep) as;
					if(!levels.contains(dep.kind))
						levels.add(dep.kind);
					
					if(!verticesLabels.containsKey(dep.kind))
						verticesLabels.put(dep.kind, new ArrayList<>());
					
					if(!verticesStructure.containsKey(dep.kind))
						verticesStructure.put(dep.kind, new ArrayList<>());
					
					verticesLabels   .get(dep.kind).add(dep.name);
					verticesStructure.get(dep.kind).add(dep.position-1);
				} 
			}
			words.add(new Word(wa.from, wa.to, wa.word, wa.word, features));
		}
	}
	
	/**
	 * Copy-constructor
	 * @param orig
	 */
	public Parse(Parse orig) {
		this(orig.id);
		levels.addAll(orig.levels);
		verticesLabels = new HashMap<>();
		verticesStructure = new HashMap<>();
		for(String lvl : orig.levels) {
			verticesLabels   .put(lvl, new ArrayList<>(orig.verticesLabels.get(lvl)));
			verticesStructure.put(lvl, new ArrayList<>(orig.verticesStructure.get(lvl)));
		}
		
		words.addAll(orig.words);
	}
	
	/**
	 * Calculate the structure of a Parse, i.e. the fields words, levels
	 * etc., from the Vector of LVs.
	 * 
	 * lvs must not be empty
	 * 
	 * annoFeats says, which lexical features should be annotated in the parse 
	 * in exceptions a list of to be ignored GraphemeNodes can be given 
	 */
	protected void computeStructure(List<LevelValue> lvs, List<LexemeNode> lexemes, 
			List<String> annoFeats, List<Integer> exceptions) {
		
		Map<Integer, Integer> map;
		int firstLevel = -1;

		/* build words and levels */

		/* we're going to find all used words by selecting the correct modifier
		 * from all LVs of the first level found. If the lexical path is
		 * ambiguous, the user has already been warned. */

		/* search first value and level */
		for (LevelValue lv : lvs) {
			if (lv != null) {
				firstLevel = lv.getLevel().getNo();
				break;
			}
		}

		verticesStructure = new HashMap<>();
		verticesLabels    = new HashMap<>();
		
		for (LevelValue lv: lvs) {
			if(lv == null)
				continue;
			
			if(exceptions.contains(lv.getDependentGN().getNo()))
				continue;

			/* register level */
			String levelName = lv.getLevel().getId();
			
			if( !levels.contains(levelName) )
				levels.add( levelName );

			if(!verticesStructure.containsKey(levelName))
				verticesStructure.put(levelName, new ArrayList<>());
			
			if(!verticesLabels.containsKey(levelName))
				verticesLabels.put(levelName, new ArrayList<>());
			
			/* check for ambiguous LVs */
//			if(lexemes == null) {
//				LexemeNode found = null;
//				for (LexemeNode ln : lv.getDependents()) {
//
//					if ((isDeleted != null && isDeleted.get(ln)) ) {
//						continue;
//					}
//					
//					if(found != null) {
//						logger.warn(String.format("WARNING: %s chosen, but %s is undeleted as well!", found, ln));
//						continue;
//					} else {
//						found = ln;
//					}
//				}
//			}

			/* register word */
			if (lv.getLevel().getNo() != firstLevel)
				continue;
			LexemeNode selectedLN = null;
			for (LexemeNode ln : lv.getDependents()) {
				selectedLN = ln;
				if(lexemes != null) {
					if (lexemes.contains(ln)) {
						break;
					} 
				}
			}
			
			int offset=0;
			if(!exceptions.isEmpty()) {
				offset= adjustWordIndex(selectedLN.getArc().from, exceptions) - selectedLN.getArc().from;
			}
			
			Word w = new Word(selectedLN, annoFeats, offset);
			words.add(w);
		}

		Collections.sort(words);

		/* compute mapping from time index to word index */
		map = new HashMap<>();
		int j = 0;
		for (Word w : words) {
			map.put(w.from, j);
			j++;
		}

		/* compute structure */
		int regentPosition;
		for (LevelValue lv: lvs) {
			if (lv == null)
				continue;
			
			if(exceptions.contains(lv.getDependentGN().getNo()))
				continue;

			if (lv.getRegentGN().isNil() ) {
				regentPosition = -1;
			} else if ( lv.getRegentGN().isNonspec() ) {
				regentPosition = -2;
			} else {
				regentPosition = map.get(adjustWordIndex(
						lv.getRegentGN().getArc().from, exceptions));
			}
			verticesStructure.get(lv.getLevel().getId()).add(regentPosition);
			verticesLabels   .get(lv.getLevel().getId()).add(lv.getLabel());
		}
	}
	
	/**
	 * decrease the given index by one for each index in exceptions that comes before it
	 */
	private int adjustWordIndex(int i, List<Integer> exceptions) {
		assert !exceptions.contains(i);
		
		int j = i;
		
		for(Integer exc : exceptions) {
			if(exc < i)
				j--;
		}
		
		return j;
	}
	
	public String getId() {
		return id;
	}
	
	/**
	 * returns the annotation string representation of this parse
	 * (as seen in .cda files)
	 * 
	 * if all is false, features with an unspecified value ('bot') will not be written
	 */
	public String toAnnotation(boolean all) {
		/* title */  
		StringBuilder ret = new StringBuilder(String.format("'%s' : '%s' <->\n",
		          id, id/*latticeId*/));
		
		/* info */
		if(info != null) {
			ret.append("/*\n");
			ret.append(info.toString());
			ret.append("*/\n");
		}

		/* iterate over words */
		for(int i = 0; i < words.size(); i++) {
			Word word = words.get(i);
			
		    /* write word */
		    ret.append(String.format("  %d %d %s%n",
		            word.from,
		            word.to,
		            formatIdentifier(word.word)));

		    for(String level : levels ) {    	
		    	String label = verticesLabels.get(level).get(i);
		    	int head = verticesStructure.get(level).get(i)+1;
		    	
		    	ret.append(String.format("%4s -> %5s -> %d",
		                formatIdentifier(level),
		                formatIdentifier(label),
		                head));
		        if(head > 0) {
		        	ret.append(String.format(" // %s",
		                  words.get(head-1).word));
		        } else if (head == -1 ) {
		        	ret.append(" // NONSPEC");
		        }
		        ret.append("\n");
		    }
		    
		    for(String feat : word.getDefinedFeatureNames()) {
		    	String val = word.getFeature(feat);
		    	/* do not record the underspecified values unless the caller wants it */
			    if(all || !val.equals("bot")) {
		        	ret.append(String.format("%14s / %s\n",
		                  formatIdentifier(feat), 
		                  formatIdentifier(val)));
		        }
		    }
		    
		    if(i+1< words.size())
		    	ret.append(",\n");
		}
		ret.append(";\n");
		return ret.toString();
	}
	
	@Override
	public String print() {
		return print(false);
	}
	
	public String print(boolean skipUninteresting) {
		String label;
		Word word;
		int noOfLevels = levels.size();
		
		StringBuilder ret = new StringBuilder("\n");

		for (int i = 0; i < words.size(); i++) {

			for(String levelId : levels) {

				label = verticesLabels.get(levelId).get(i);
				if (skipUninteresting && label.equals("") && 
						verticesStructure.get(levelId).get(i) == -1)
					continue;
	
				word = words.get(i);
	
				ret.append(String.format("   %03d %10s: %20s(%2d-%2d) --%4s--> ", 
						word.from * noOfLevels + levels.indexOf(levelId), levelId,
						word.description, word.from, word.to, label));
	
				int reg = verticesStructure.get(levelId).get(i);
				if (reg == -1) {
					ret.append("NIL\n");
				} else if (reg == -2) {
					ret.append("NONSPEC\n");
				} else {
					word = words.get(reg);
					ret.append(String.format("%s(%2d-%2d)\n", word.description,
							word.from, word.to));
				}
			}
		}

		ret.append("\n");
		return ret.toString();
	}

	@Override
	public List<Word> getWords() {
		return words;
	}
	
	public int getNoOfLevels() {
		return levels.size();
	}
	
	public int getHeadOf(int wordNo, String levelId) {
		if(! levels.contains(levelId))
			throw new RuntimeException("Illegal level number " + levelId);
		if(wordNo < 0 || wordNo >= words.size())
			throw new RuntimeException("Illegal word number " + wordNo);
		
		return verticesStructure.get(levelId).get(wordNo);
	}
	
	public String getLabelOf(int wordNo, String levelId) {
		if(!levels.contains(levelId))
			throw new RuntimeException("Illegal level number " + levelId);
		if(wordNo < 0 || wordNo >= words.size())
			throw new RuntimeException("Illegal word number " + wordNo);
		
		return verticesLabels.get(levelId).get(wordNo);
	}
	
	@Override
	public List<String> getLevels() {
		return levels;
	}
	 
	private String formatIdentifier(String s) {
		// TODO escaping etc. überprüfen
		Pattern m = java.util.regex.Pattern.compile("[a-zA-Z]+");
		if (m.matcher(s).matches()) {
			return s;
		}
		return "'"+s.replace("'", "\\'").replace("\"", "\\\"")+"'";
	}
	
//	/**
//	 * calculates a score estimation by multiplying the best score for every slot
//	 */
//	public double getScoreEstimation(ConstraintNet net, boolean virtOffset) {
//		double score = 1.0;
//		int sid_i, wid_i;
//		int wid_r;
//		int noOfLevels = net.getNoOfLevels();
//		String label;
//		int lvlno;
//		LevelValue lv;
//
//		for(sid_i=0; sid_i< verticesStructure.size(); sid_i++) {
//			wid_i = sid_i / noOfLevels;
//			wid_r = verticesStructure.get(sid_i);
//			label = verticesLabels.get(sid_i);
//			lvlno = sid_i % noOfLevels;
//
//			if(virtOffset && (net.getLexemeGraph() instanceof VirtualLexemeGraph) && 
//					wid_i >= ((VirtualLexemeGraph)net.getLexemeGraph()).getVStartId()-1)
//				wid_i +=1;
//
//			if(virtOffset && (net.getLexemeGraph() instanceof VirtualLexemeGraph) && 
//					wid_r >= ((VirtualLexemeGraph)net.getLexemeGraph()).getVStartId()-1)
//				wid_r +=1;
//
//			List<LevelValue> lvs = net.getAccordingLVs(lvlno, wid_i, label, wid_r);
//			lv  = null;
//			for(LevelValue lv2 : lvs) {
//				if( lv2.getScore() > (lv==null?0.0:lv.getScore()) )
//					lv=lv2;
//			}
//
//			if(lv != null) {
//				score*= lv.getScore();
//			} else {
//				return 0.0;
//			}
//		}
//
//		return score;
//	}

	@Override
	public Map<String, List<Integer>> getVerticesStructure() {
		return verticesStructure;
	}
	
	@Override
	public Map<String, List<String>> getVerticesLabels() {
		return verticesLabels;
	}

	@Override
	public void redirectEdge(int pos, String level, int to) {
		// TODO does this need to be implemented?
	}

	/**
	 * A comparison for undecorated parses,
	 * just compares vertices
	 */
	public boolean sameVertices(Parse b) {
		// compare structure
		if (! this.verticesStructure.equals(b.verticesStructure))
			return false;
		// compare labels
		if (! this.verticesLabels.equals(b.verticesLabels))
			return false;
		return true;
	}
	
	public void setInfo(ParseInfo parseInfo) {
		this.info = parseInfo;
	}
	
	public ParseInfo getInfo() {
		return info;
	}
	
	/**
	 * Returns a decorated version of this parse by:
	 * 
	 * - looking up the words in the lexicon, 
	 *    if no lexikon is given (lex == null) the lexikon-items are generated from the featurelist,
	 *    use this latter variant only when sure the words of the parse already contain all 
	 *    features queried in the constraints of the grammar 
	 * - generating an accroding lexeme graph,
	 * - selecting lexemenodes fitting the annotations of the words in the parse,
	 * - and fabricating levelvalues regarding the vertices and the selected lns.
	 * Then we evaluate it via the given grammar evaluator.
	 */
	public DecoratedParse decorate(GrammarEvaluator evaluator, Lexicon lex, Configuration config) {
		return decorate(evaluator, lex, config, true);
	}
	public DecoratedParse decorate(GrammarEvaluator evaluator, Lexicon lex, Configuration config, boolean bestLexemes) {
		List<VirtualNodeSpec> virtualNodesNeeded = new ArrayList<>();

		if(lex== null) {
			List<LexiconItem> lis = new ArrayList<>();
			for(Word w : words) {
				if(w.getDefinedFeatureNames().contains("virtual") &&
						w.getFeature("virtual").equals("true"))
					continue;
				LexiconItem li = w.toLexiconItem(config);
				if(!lis.contains(li))
					lis.add(li);
			}
			lex= new Lexicon(lis, evaluator.getGrammar().getAllAttributes());
		}
		
		Lattice lat = wordsToLattice(virtualNodesNeeded);
		
		LexemeGraph lg = new LexemeGraph(lat, lex, config);
		
		if(!virtualNodesNeeded.isEmpty()) {
			VirtualLexemeGraph vlg = new VirtualLexemeGraph(lg);
			for(VirtualNodeSpec vns : virtualNodesNeeded) {
				vlg.addVirtualNode(vns, 1);
			}
			lg = vlg;
		}
		
		List<List<LevelValue>> candidatesBySlot = new ArrayList<>();
		for(String levelId : levels) {
			for(int i = 0; i < words.size(); i ++) {
				int head = verticesStructure.get(levelId).get(i);
				String label = verticesLabels.get(levelId).get(i);
				Word w = words.get(i);
				candidatesBySlot.add(
						findLVs(levelId, head, label, w, evaluator, lg, config));
			}
		}
		if (! bestLexemes) {
			List<LevelValue> selectedValues = localOptimalVariants(candidatesBySlot);
			return new DecoratedParse(lg, selectedValues, lg.lexicalize(selectedValues), 
									  config.getList(ANNO_FEATS), evaluator, new ArrayList<>());
		}
		
		// spell out variants
		List<List<LevelValue>> variants =
				spellOutVariants(new ArrayList<>(), candidatesBySlot, new ArrayList<>(lg.getNodes()));
		
		Collections.sort(variants, (l1, l2) -> scoreProduct(l2).compareTo(scoreProduct(l1)));
		
		Badness bestB = Badness.worstBadness();
		DecoratedParse best = null;
		for(List<LevelValue> selection : variants) {
			double preview = scoreProduct(selection);
			
			if(preview < bestB.getScore()) {
				break;
			}
			DecoratedParse dp = new DecoratedParse(
					lg, selection, lg.lexicalize(selection), 
					config.getList(ANNO_FEATS), evaluator, new ArrayList<>());
			if(dp.getBadness().compare(bestB)) {
				best  = dp;
				bestB = best.getBadness();
			}
		}
		best.getInfo().mergeInfo(info);
		return best;
	}

	/**
	 * @param virtualNodesNeeded output parameter, lattice doesn't contain virtual nodes
	 * @return lattice containing all non-virtual words
	 */
	private Lattice wordsToLattice(List<VirtualNodeSpec> virtualNodesNeeded) {
		List<Arc> arcs = new ArrayList<>();
		for(Word w : words) {
			if( w.getFeature("virtual") == null || !w.getFeature("virtual").equals("true")) {
				arcs.add(new Arc(w.from, w.to, w.from, w.word, 1.0));
			} else {
				virtualNodesNeeded.add(w.toVirtualNodeSpec());
			}
		}
		
		Lattice lat = new Lattice(id, arcs, new SourceInfo());
		lat.scan(false);
		return lat;
	}
	
	private Double scoreProduct(List<LevelValue> l) {
		double d= 1.0;
		for(LevelValue lv: l) {
			d*= lv.getScore();
		}
		return d;
	}

	private List<LevelValue> localOptimalVariants(List<List<LevelValue>> lvslist) {
		List<LexemeNode> taboo = new ArrayList<>();
		List<LevelValue> result = new ArrayList<>();
		Comparator<LevelValue> cmp = (l1, l2) -> Double.compare(l1.getScore(), l2.getScore());
		for (List<LevelValue> lvs: lvslist) {
			lvs.removeIf(lv -> !lv.isLexemeCompatible(taboo));
			LevelValue selected = Collections.max(lvs, cmp);
			result.add(selected);
			Set<LexemeNode> removedLexemes = new HashSet<>();
			for (LevelValue lv: lvs) {
				removedLexemes.addAll(lv.getDependents());
				removedLexemes.addAll(lv.getRegents());
			}
			removedLexemes.removeAll(selected.getDependents());
			removedLexemes.removeAll(selected.getRegents());
			taboo.addAll(removedLexemes);
		}
		return result;
	}
	
	/**
	 * given a list of lists of alternative LVs,
	 * this function spells the combinations out recursively
	 */
	private List<List<LevelValue>> spellOutVariants(
			List<LevelValue> soFar, List<List<LevelValue>> alternatives, List<LexemeNode> validLNs) {
		List<List<LevelValue>> variants = new ArrayList<>();

		if(alternatives.isEmpty()) {
			// base case, just return a new list containing only the vector soFar
			variants.add(soFar);
			return variants;
		}

		List<LevelValue> lvs = alternatives.get(0);
		List<List<LevelValue>> remaining = alternatives.subList(1, alternatives.size());
		List<LevelValue> continuation;
		boolean compatibleD, compatibleR, contains;
		List<LexemeNode> validLNsCopy;


		for (LevelValue lv : lvs) {
//			// first cut out variants where on different levels different lexemes are selected for the same word,
//			// otherwise we get too many variants
//			if(vectorSize(soFar) > 0 ) {
//				lvlast= vectorElement(soFar, vectorSize(soFar)-1);
//			} else {
//				lvlast = NULL;
//			}
//			if(lvlast && lvGetDependentGN(lv) == lvGetDependentGN(lvlast)
//					&& !haveCommonLN(lv->dependents, lvlast->dependents)) {
//				continue;
//			}

			validLNsCopy = new ArrayList<>(validLNs);

			compatibleD = false;
			for(LexemeNode ln : lv.getDependentGN().getLexemes()) {
				contains = lv.getDependents().contains(ln);
				if(validLNsCopy.contains(ln)) {
					if(contains) {
						compatibleD = true;
					} else {		
						validLNsCopy.remove(ln);
					}
				}
			}

			GraphemeNode reg = lv.getRegentGN();
			if(reg.spec()) {
				compatibleR = false;
				for(LexemeNode ln : reg.getLexemes()) {
					contains = lv.getRegents().contains(ln);
					if(validLNsCopy.contains(ln)) {
						if(contains) {
							compatibleR = true;
						} else {		
							validLNsCopy.remove(ln);
						}
					}
				}
			} else {
				compatibleR = true;
			}

			if(compatibleD && compatibleR) {
				continuation = new ArrayList<>(soFar);
				continuation.add(lv);

				variants.addAll( spellOutVariants(continuation, remaining, validLNsCopy) );
			}
		}
		return variants;
	}
	
	/**
	 * Generate and return an LV that corresponds to the binding at position
	 * where in the Parse.
	 *
	 * There could be more than one fitting LV, if there is more than one fitting lexeme for the word at where
	 */
	private List<LevelValue> findLVs(String levelName, int head, String label, Word w, 
			GrammarEvaluator evaluator, LexemeGraph lg, Configuration config) {
		List<LevelValue> lvs        = new ArrayList<>();
		List<LexemeNode> dependents;
		List<LexemeNode> regents;

		Grammar g = evaluator.getGrammar();
		
		Level level = g.findLevel(levelName);
		List<LexemeNode> modifierCandidates;
		List<LexemeNode> modifieeCandidates;
		int noOfLevels= g.getNoOfLevels();
		
		
		modifierCandidates = word2ln(w, lg, g, config);

		if (head == -1) {
			modifieeCandidates = new ArrayList<>(lg.getNil().getLexemes());
		} else if (head == -2) {
			modifieeCandidates = new ArrayList<>(lg.getNonspec().getLexemes());
		} else {
			Word modifieeWord  = words.get(head);
			modifieeCandidates = word2ln(modifieeWord, lg, g, config);
		}
		// modifiee = modifieeCandidates? modifieeCandidates->item : NULL;

		if (level == null) {
			logger.error(String.format("Can't find level `%s'.", levelName));
			logger.error(              "can't decorate this parse.");
			return null;
		}

		/* check whether the level has this label */
		if(!level.getLabels().contains(label)) {
			String s = level.getLabels().get(0);
			logger.warn(String.format("Level `%s' has no label `%s', using `%s' instead.",
					levelName, label, s));
			label = s;
		}

		//if(config.getFlag(DECORATE_PARSE_ALL_LEXEME_VARIANTS)) {
		/* Fabricate LVs */
		for(LexemeNode dep : modifierCandidates) {
			for(LexemeNode reg : modifieeCandidates) {
				dependents = new ArrayList<>();
				dependents.add(dep);
				regents = new ArrayList<>();
				regents.add(reg);
				LevelValue lv = new LevelValue(level, noOfLevels, 
						dependents, dep.getGN(), 
						label,
						regents, reg.getGN());
				GrammarEvaluationResult ger = 
						evaluator.evalUnary(lv, lg, null, false, false);
				lv.setScore(ger.getScore());
				lvs.add(lv);
			}
		}
//		} else {
//			double record = -1.0;
//			LevelValue bestLV = null;
//			/* Fabricate LVs */
//			for(LexemeNode dep : modifierCandidates) {
//				for(LexemeNode reg : modifieeCandidates) {
//					dependents = new ArrayList<LexemeNode>();
//					dependents.add(dep);
//					regents = new ArrayList<LexemeNode>();
//					regents.add(reg);
//					LevelValue lv = new LevelValue(level, noOfLevels, 
//							dependents, dep.getGN(), 
//							label,
//							regents, reg.getGN());
//					GrammarEvaluationResult ger = 
//							evaluator.evalUnary(lv, lg, null, false, false);
//					if(ger.getScore()> record) {
//						bestLV = lv;
//						record = ger.getScore(); 
//					}
//				}
//			}
//			lvs.add(bestLV);
//		}

		return lvs;
	}
		
	/** ----------------------------------------------------------------------
	 Translate a word to a LexemeNode.

	 Among the nodes of LG, the one that matches w best is selected and
	 returned.

	 An arbitrary number of feature mismatches are allowed if necessary, but
	 if no node even has the same word form, an empty list may be returned.
	 */
	private List<LexemeNode> word2ln(Word w, LexemeGraph lg, Grammar g, Configuration config) {
		List<LexemeNode> result = new ArrayList<>();
		
		int record = Integer.MAX_VALUE;
		
		for (LexemeNode ln : lg.getNodes()) {
			if (ln.getArc().from != w.from || ln.getArc().to != w.to) {
				continue;
			}

			Hierarchy h = g.findHierarchy(config.getString(Grammar.FEATURE_HIERARCHY)); 
			int j = w.differencesToLI(ln.getLexeme(), h);
			
			if (j < record) {
				record = j;
				result.clear();
				result.add(ln);
			} else if (j == record) {
				result.add(ln);
			}
		}

		/* If a result is found but that satisfies the annotation,
		 use the one that matches best,
		 but warn the user about the differences */
		if (record > 0 && !result.isEmpty()) {
			// (void) anCompare(an, result->lexem, TRUE);
			logger.warn(String.format("%s: `%s' differs in %d feature%s.",
					lg.getLattice().getId(), result.get(0).getLexeme().getDescription(),
					record, record == 1 ? "" : "s"));
		}

		return result;
	}
	
	/**
	 * returns the dependency structure for the given level in the conll format
	 * each string in the retuned list represents one line
	 * 
	 * Example:
	 *  1	Veruntreute	Veruntreute	V	VVFIN	3|s|i|t	0	ROOT
	 *  2	die	d	ART	ART	n|s|f	3	DET
	 *  
	 *  position
	 *  word
	 *  lemma
	 *  part-of-speech
	 *  part-of-speech (coarse, but we simply mirror the previous field here )
	 *  morph (ignore here)
	 *  head
	 *  dependency type
	 */
	public ArrayList<String> toCONLL(String level) {
		ArrayList<String> lines = new ArrayList<>();
		
		for(Word w : words) {
			String line = String.format(
					"%d	%s	%s	%s	%s	%s	%d	%s",
					w.to,
					w.word,
					w.getFeature("base")!=null?w.getFeature("base"):w.word,
					getCPOSTAG(w.getFeature("cat")),
					w.getFeature("cat"),
					"-", //w.description,
					getHeadOf(w.from, level)+1,
					getLabelOf(w.from, level).equals("") ? "-PUNCT-" : getLabelOf(w.from, level)
					);
			
			lines.add(line);
		}
		
		return lines;
	}

	@Override
	public String toString() {
		return print();
	}
	
	private String getCPOSTAG(String POSTAG) {
		if (POSTAG.endsWith("AT") || "ART".equals(POSTAG)) {
			return "ART";
		} else if (pronPat.matcher(POSTAG).matches() || "PPER".equals(POSTAG)
				|| "PRF".equals(POSTAG)) {
			return "PRO";
		} else if ("ADV".equals(POSTAG) || "ADJD".equals(POSTAG)) {
			return "ADV";
		} else if (prepPat.matcher(POSTAG).matches() || "PROAV".equals(POSTAG)) {
			return "PREP";
		} else if ('N' == POSTAG.charAt(0) || 'V' == POSTAG.charAt(0)) {
			return POSTAG.charAt(0)+"";
		} else {
			return POSTAG;
		}
	}
	
	/**
	 * Generate an analysis representing this parse
	 * 
	 * the analysis ca nthen be evaluated, modified or frobbed
	 * 
	 * The resulting analysis doesn't guarantee to repect lexical featuresannotated to the parse!
	 * TODO respect lexical features annotated to the parse
	 */
	public Analysis toAnalysis(GrammarEvaluator evaluator, Lexicon lex, Configuration config, boolean isComplete) {
		List<VirtualNodeSpec> vnspecs = new ArrayList<>();
		Lattice lat = wordsToLattice(vnspecs);
		ConstraintNet net = new ConstraintNet(lat, lex, true, isComplete, evaluator, config);
		
		if(!vnspecs.isEmpty()) {
			net.initVirtualNodes(vnspecs);
		}
		
		Analysis a = new Analysis(net);
		LexemeGraph lg = net.getLexemeGraph();
		
		for(String levelId : levels) {
			for(int i = 0; i < words.size(); i ++) {
				Word w = words.get(i);
				GraphemeNode dep = lg.getGNs().get(w.from);
				int head = verticesStructure.get(levelId).get(i);
				GraphemeNode reg = lg.getGNs().get(head);
				String label = verticesLabels.get(levelId).get(i);
				
				Level level = net.getGrammar().findLevel(levelId);
				List<LevelValue> lvs = net.findExistingLVs(level, dep, label, reg);
				LevelValue lv;
				
				if(lvs.isEmpty()) {
					lv = new LevelValue(
							level,
							net.getNoOfLevels(),
							Lists.newArrayList(dep.getLexemes().get(0)),
							dep, 
							label, 
							Lists.newArrayList(reg.getLexemes().get(0)),
							reg);
				} else {
					lv = lvs.get(0);
				}
				
				a.selectLV(lv);
			}
		}
		
		ConstraintNetState state = new ConstraintNetState(net, config);
		state.lockToOnlyLexemeVariants(a, null);
		ProblemSolver solver = new CombinedFactory(net.getGrammar(), lex, config).generateSolver(state, a);
		solver.solve();
				
		return solver.getBest();
	}
	
	/**
	 * Converts the nodeBindingIndex1 of a ConstraintViolation into the index of the word
	 * 
	 * @param parse
	 * @return
	 */
	public int calcIndex1(ConstraintViolation cv){
		int noOfLevels = getLevels().size();
		int wordIndex1 = cv.getNodeBindingIndex1() / noOfLevels;
		return wordIndex1; 
	}
	
	/**
	 * Converts the nodeBindingIndex2 of a ConstraintViolation into the index of the word
	 * 
	 * @param parse
	 * @return
	 */
	public int calcIndex2(ConstraintViolation cv){
		int noOfLevels = getLevels().size();
		int wordIndex2 = cv.getNodeBindingIndex2();
		if (wordIndex2 >= 0){
			wordIndex2 = wordIndex2 / noOfLevels;
		}
		return wordIndex2; 
	}
	
	/**
	 * Gives the name of the level of a nodeBindingIndex of a ConstraintViolation
	 * 
	 * @param nodeBindingIndex
	 * @return
	 */
	public String getCvLevelName(int nodeBindingIndex){
		return getLevels().get(nodeBindingIndex % getLevels().size());
	}
}
