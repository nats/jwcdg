package de.unihamburg.informatik.nats.jwcdg.avms;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


/**
 * A sequence of keys,
 * identifying a feature in an attribute value matrix.
 * 
 * In essence a linked list
 * 
 * @author the CDG-Team
 */
public class Path implements java.io.Serializable {
	private static final long serialVersionUID = 6532122663710788745L;

	public enum PseudoFeature {
		id,
		grapheme,
		word,
		from,
		to,
		info,
		chunk_start,
		chunk_end,
		chunk_type
	}
	
	
	private final String head;
	private final Path   rest;
	
	private final PseudoFeature pf;
	
	public Path(List<String> tokens) {
		this(tokens, 0); 
	}
	
	/**
	 * splits the given string at ' ', '/' or '-' characters
	 * and builds a path from the resulting tokens
	 * 
	 * @param sequence
	 */
	public static Path fromSequence(String seq) {
		StringTokenizer t = new StringTokenizer(seq, "/ -");
		List<String> tokens = new ArrayList<>();
		while(t.hasMoreTokens()) {
			tokens.add(t.nextToken());
		}
		
		return new Path(tokens, 0); 
	}
	
	private Path(List<String> tokens, int i) {
		head = tokens.get(i);
		if(i < tokens.size() - 1) {
			rest = new Path(tokens, i+1);
		} else {
			rest = null;
		}
		
		if(rest != null)
			pf= null;
		else {
			PseudoFeature pftmp = null;
			for(PseudoFeature pf2 : PseudoFeature.values()) {
				if(pf2.toString().equals(head)) { 
					pftmp = pf2;
					break;
				}
			}
			this.pf=pftmp;
		}
	}
	
	public Path(String attribute, Path p) {
		head = attribute;
		rest = p;
		
		if(rest != null)
			pf= null;
		else {
			PseudoFeature pftmp = null;
			for(PseudoFeature pf2 : PseudoFeature.values()) {
				if(pf2.toString().equals(head)) { 
					pftmp = pf2;
					break;
				}
			}
			this.pf=pftmp;
		}
	}

	public String getHead() {
		return head;
	}
	
	public Path getRest() {
		return rest;
	}
	              
	@Override
	public String toString() {
		if(rest == null) {
			return head;
		} else {
			return head + "->" + rest.toString();
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null) {
			return false;
		}
		
		if (obj instanceof Path) {
			Path p = (Path) obj;
			if(!this.head.equals(p.head))
				return false;
			if(this.rest == null) 
				return p.rest == null;
			return this.rest.equals(p.rest);
			
		} else {
			return false;
		}
	}
	
	/**
	 * Detect slots that are written like lexical features but aren't:
	 * `word', `id', etc.
	 */
	public boolean isPseudoFeature() {
		return pf != null;
	}
	
	/**
	 * @return null if this path is no pseudofeature
	 */
	public PseudoFeature toPseudoFeature() {
		return pf;
	}
	
	@Override
	public int hashCode() {
		if(rest == null) {
			return head.hashCode();
		} else {
			return head.hashCode() ^ rest.hashCode();
		}
	}

}
