package de.unihamburg.informatik.nats.jwcdg.gui;

import java.util.ArrayList;

/**
 * The subcomponent of the AnnoDisplayInteractor that handles the interactions
 * with the navigation tree on the left side of the Display
 * 
 * @author Sven Zimmer and Christine Köhn and Arne Köhn
 * 
 */
public class AnnoNavigator extends AbstractNavigator {
	// the last selected node with a parse in it
	// new nodes must be inserted into the model to ensure their visibility

	public class AnnoNavNode extends NavNode {
		private static final long serialVersionUID = 1L;
		public ArrayList<NavNodeContent> _contentList =
                new ArrayList<>();

		public AnnoNavNode(
					ArrayList<NavNodeContent> contentList,
					String navDirectory) {
			super(navDirectory + " : " + getSentence(contentList.get(0)._parse),
						false);
			_contentList = contentList;
		}

	}

	public void add(ArrayList<NavNodeContent> contentList, String navDirectory) {
		AnnoNavNode node = new AnnoNavNode(contentList, navDirectory);
		addNode(node, _root);
	}

	public void addNode(AnnoNavNode node, NavNode dirNode) {
		insertNode(node, dirNode);
	}

	public AnnoNavNode getCurrentNode() {
		return (AnnoNavNode) _currentNode;
	}

}
