package de.unihamburg.informatik.nats.jwcdg.constraints;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import de.unihamburg.informatik.nats.jwcdg.input.SourceInfo;

/**
 * A level in a dependency structure
 * A word has one regent per level (can be the same regent on different levels)
 * 
 * 
 * @author the CDG-Team
 *
 */
public class Level implements Serializable{

	private static final long serialVersionUID = 1L;
    private String  id;           /* level name */
	private Map<String, String> properties;  /* list of Flags for extra info */
	private List<String> labels;  /* list of labels */
	private int     no;           /* index from zero on */
	private boolean showflag;     /* show it or not */
	private boolean useflag;	  /* use it or ignore it */

	private SourceInfo source;
	
	//private static int counter=0;
	
	public Level(String id, Map<String, String> properties, List<String> labels,
			SourceInfo source) {
		this.id = id;
		this.properties = properties;
		this.labels = labels;
		this.no = -1; // will be set when added to a grammar
		//this.no = no;
		//this.no = counter++;
		this.showflag = true;
		this.useflag = true;
		this.source = source;
	}

	/**
	 * the name of the level,
	 * used when printing information
	 * 
	 * @return
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * at what postion this level appears in arrays
	 * 
	 * @return
	 */
	public int getNo() {
		return no;
	}

	/**
	 * should this level be used, e.g.,
	 * should anyl levelvalues be built for it
	 * and should an analysisi reserve slots for it?  
	 * 
	 * @return
	 */
	public boolean isUsed() {
		return useflag;
	}
	
	/**
	 * should this level be regarded when printing out any information?
	 * @return
	 */
	public boolean isShown() {
		return showflag; 
	}

	
	/**
	 * the lsit of all dependency labels that can occur on this level,
	 * if the empty label is possible, it will be an explicit part of this list
	 * 
	 * @return
	 */
	public List<String> getLabels() {
		return labels;
	}

	/**
	 * who told you about this?
	 * @return
	 */
	public SourceInfo getSource() {
		return source;
	}
	
	public String getProperty(String key) {
		return properties.get(key);
	}
	
	@Override
	public String toString() {
		return String.format("(%d-%s)", no, id);
	}

	public void setNo(int no) {
		this.no = no;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Level))
			return false;
		Level other = (Level) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
