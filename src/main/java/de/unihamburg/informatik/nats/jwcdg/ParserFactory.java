package de.unihamburg.informatik.nats.jwcdg;

import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;
import de.unihamburg.informatik.nats.jwcdg.transform.SolverFactory;

public interface ParserFactory <T extends Parser> {

	T makeParser(Grammar grammar, Lexicon lexicon,
                 SolverFactory factory, Configuration config);
	
	T makeParser(Grammar grammar, Lexicon lexicon,
                 Configuration config);
}
