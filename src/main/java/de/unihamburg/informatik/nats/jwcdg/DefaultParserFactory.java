package de.unihamburg.informatik.nats.jwcdg;

import de.unihamburg.informatik.nats.jwcdg.frobbing.CombinedFactory;
import de.unihamburg.informatik.nats.jwcdg.frobbing.DynamicFactory;
import de.unihamburg.informatik.nats.jwcdg.frobbing.ThreefoldFactory;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;
import de.unihamburg.informatik.nats.jwcdg.transform.SolverFactory;

public class DefaultParserFactory implements ParserFactory <Parser> {

	public static final String FROBBING_METHOD = "frobbingMethod";

	@Override
	public Parser makeParser(Grammar grammar, Lexicon lexicon,
			SolverFactory factory, Configuration config) {
		return new Parser(grammar, lexicon, config, factory);
	}

	@Override
	public Parser makeParser(Grammar grammar, Lexicon lexicon,
			Configuration config) {
		String method = config.getString(FROBBING_METHOD);
		SolverFactory factory;

		switch (method) {
			case CombinedFactory.NAME:
				factory = new CombinedFactory(grammar, lexicon, config);
				break;
			case DynamicFactory.NAME:
				factory = new DynamicFactory(grammar, lexicon, config);
				break;
			case ThreefoldFactory.NAME:
				factory = new ThreefoldFactory(grammar, lexicon, config);
				break;
			default:
				throw new RuntimeException("Unknown frobbing method " + method);
		}
		
		return new Parser(grammar, lexicon, config, factory);
	}

}
