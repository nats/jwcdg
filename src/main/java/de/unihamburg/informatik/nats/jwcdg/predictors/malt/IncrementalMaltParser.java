/**
 * 
 */
package de.unihamburg.informatik.nats.jwcdg.predictors.malt;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import org.maltparser.core.exception.MaltChainedException;
import org.maltparser.core.syntaxgraph.DependencyStructure;
import org.maltparser.core.syntaxgraph.node.DependencyNode;
import org.maltparser.parser.DependencyParserConfig;
import org.maltparser.parser.DeterministicParser;
import org.maltparser.parser.TransitionSystem;
import org.maltparser.parser.algorithm.twoplanar.TwoPlanarConfig;
import org.maltparser.parser.history.action.GuideDecision;
import org.maltparser.parser.history.action.GuideUserAction;

/**
 * IncrementalMaltParser exposes an incremental interface making it possible
 * to parse a sentence step by step and use the intermediate results.
 * @author Arne Köhn
 */
public class IncrementalMaltParser extends DeterministicParser {

	/**
	 * @param manager
	 * @throws MaltChainedException
	 */
	public IncrementalMaltParser(DependencyParserConfig manager)
			throws MaltChainedException {
		super(manager);
	}
	
	public void initialize(DependencyStructure pds) throws MaltChainedException {
		parserState.clear();
		parserState.initialize(pds);
		currentParserConfiguration = parserState.getConfiguration();
	}
	
	public DependencyStructure doStep() throws MaltChainedException {
		TransitionSystem ts = parserState.getTransitionSystem();
		GuideUserAction action = null;
		while (action == null || ! ts.getActionString(action).contains("SH")) {
			action = ts.getDeterministicAction(parserState.getHistory(), currentParserConfiguration);
			if (action == null) {
				action = predict();
			}
			parserState.apply(action);
		}
		return currentParserConfiguration.getDependencyGraph();
	}
	
	/**
	 * Tells the parser that the sentence is complete. Will finish parsing and
	 * link all remaining nodes to root
	 * @return the final dependency structure
	 * @throws MaltChainedException
	 */
	public DependencyStructure finalizeParse() throws MaltChainedException {
		TransitionSystem ts = parserState.getTransitionSystem();
		GuideUserAction action = null;
		while (!parserState.isTerminalState()) {
			action = ts.getDeterministicAction(parserState.getHistory(), currentParserConfiguration);
			if (action == null) {
				action = predict();
			}
			parserState.apply(action);
		}
		DependencyStructure parseDependencyGraph = currentParserConfiguration.getDependencyGraph();
		parseDependencyGraph.linkAllTreesToRoot();
		return currentParserConfiguration.getDependencyGraph();
	}
	
	/**
	 * Adds a word to the input that should be parsed. Does not parse, you have
	 * to call doStep() for this.
	 * @param word The word
	 * @param tag The Part-of-Speech tag this word has
	 * @throws MaltChainedException
	 */
	public void addWord(String word, String tag) throws MaltChainedException {
		DependencyStructure parseDependencyGraph = currentParserConfiguration.getDependencyGraph();
		DependencyNode node = parseDependencyGraph.addDependencyNode();
		parseDependencyGraph.addLabel(node, "FORM", word);
		parseDependencyGraph.addLabel(node, "POSTAG", tag);
		parseDependencyGraph.addLabel(node, "CPOSTAG", getCoarseTag(tag));
		
		Stack<DependencyNode> input = ((TwoPlanarConfig)getParserState().getConfiguration()).getInput();
		input.add(0, node);
	}
	
	protected static final List<String> nClass = Arrays.asList("NE","NN");
	protected static final List<String> vClass = Arrays.asList("VVFIN","VVINF","VVPP","VVIZU","VVIMP","VAFIN","VAINF","VAPP","VAIMP","VMFIN","VMINF", "VMPP");
	protected static final List<String> prepClass = Arrays.asList("APPR","APPRART","APPO");
	protected static final List<String> artClass = Arrays.asList("PPOSAT","ART","PWAT","PRELAT","PPOSAT","PIAT","PIDAT","PDAT");
	protected static final List<String> proClass = Arrays.asList("PWS","PRF","PRELS","PPOSS","PPER","PIS","PDS");
	protected static final List<String> advClass = Arrays.asList("ADV","ADJD");
	
	/**
	 * converts stts tag to stts coarse tags FIXME this should be configurable
	 * @param tag
	 * @return coarse tag
	 */
	public static String getCoarseTag(String tag) {
		if (nClass.contains(tag))
			return "N";
		if (vClass.contains(tag))
			return "V";
		if (prepClass.contains(tag))
			return "PREP";
		if (artClass.contains(tag))
			return "ART";
		if (proClass.contains(tag))
			return "PRO";
		if (advClass.contains(tag))
			return "ADV";
		return tag;
	}
	
	/**
	 * copy of DeterministicParser.predict since that method is private
	 * TODO ask Maltparser devs to make it protected instead.
	 * @return
	 * @throws MaltChainedException
	 */
	private GuideUserAction predict() throws MaltChainedException {
		GuideUserAction currentAction = parserState.getHistory().getEmptyGuideUserAction();
		try {
			classifierGuide.predict((GuideDecision)currentAction);
			while (!parserState.permissible(currentAction)) {
				if (!classifierGuide.predictFromKBestList((GuideDecision) currentAction)) {
					currentAction = getParserState().getTransitionSystem().defaultAction(parserState.getHistory(), currentParserConfiguration);
					break;
				}
			}
		} catch (NullPointerException e) {
			throw new MaltChainedException("The guide cannot be found. ", e);
		}
		return currentAction;
	}
}
