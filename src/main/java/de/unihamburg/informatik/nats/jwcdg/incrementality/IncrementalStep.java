package de.unihamburg.informatik.nats.jwcdg.incrementality;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNet;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.frobbing.FrobStats;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;
import de.unihamburg.informatik.nats.jwcdg.parse.ParseInfo;
import de.unihamburg.informatik.nats.jwcdg.transform.Analysis;

public class IncrementalStep implements IncrementalParsingState {
	
	public static final String INIT_VN_TYPES  = "initVNTypes";
	public static final String INIT_VN_LABELS = "initVNLabels";
	public static final String INIT_VN_HEADS  = "initVNHeads";
	
	private static Logger logger = LogManager.getLogger(IncrementalStep.class);

	public static IncrementalStep makeInitialStep(
			GrammarEvaluator evaluator, Lexicon lexicon, Configuration config) {
		long startTime = System.currentTimeMillis();
		
		ConstraintNet net = new ConstraintNet(evaluator, lexicon, config);
		
		Analysis a = new Analysis(net);
		a.setToMaxFragmentation();
		if(config.getFlag(ConstraintNet.USE_VIRTUAL_NODES)) {
			List<String> names  = config.getList(INIT_VN_TYPES);
			List<String> labels = config.getList(INIT_VN_LABELS);
			List<Integer> heads = new ArrayList<>(config.getList(INIT_VN_HEADS).size());
			for(String s: config.getList(INIT_VN_HEADS)) {
				heads.add(Integer.parseInt(s));
			}
			boolean success = a.selectVNStructure(names, labels, heads, net.getMainLevel());
			if(!success)
				logger.warn("Could not establish initial VN configuration, check the following settings: "
						+", "+INIT_VN_HEADS + ", "+ INIT_VN_LABELS + " and the VirtualNodeSpecifications in your grammar.");
		}
		a.judge(evaluator);
		assert a.check(evaluator, 3, new ArrayList<>());

		long elapsedTime = System.currentTimeMillis() - startTime; 
		
		Parse p= a.toParse(config, evaluator);
		ParseInfo info = p.getInfo();
		info.setEvaluationTime(elapsedTime);
		info.setSearchStrategy("Initial Incremental State");
		info.setEntry("Overall Time", Long.toString(elapsedTime));
		IncrementalStep is = new IncrementalStep(net, ">");
		is.registerSolution(a, p, elapsedTime);
		
		return is;
	}
	
	// ID
	private String word; // the token added this step
	private String mode; // which mode was used e.g full incremental or prefix parsing
	private int iteration; // num of the iteration

	// core
	private ConstraintNet net;
	private Parse parse; // the resulting parse
	private Analysis analysis; // the resulting analysis  
	private IncrementalStep previousStep; // the step before this one, NULL if this one is the first
	private double score;
	private boolean virtualNodeMatch; // if the new node matched on a virtual node

	// infos
	private long timeTotal;
	private FrobStats stats;
	
	public IncrementalStep(ConstraintNet net, String increment) {
		this.net     = net;
		word         = increment;
		previousStep = null;
		iteration    = 0;
		timeTotal    = 0;
	}
	
	public IncrementalStep(IncrementalStep step, String increment) {
		previousStep = step;
		iteration    = step.iteration+1;
		net          = step.net;
		word         = increment;
		timeTotal    = 0;
	}
	

	public void registerSolution(Analysis a, Parse p, long timeTotal) {
		this.parse    = p;
		this.analysis = a;
		this.score    = a.getBadness().getScore();
		this.timeTotal= timeTotal;
	}
	
	public Analysis getAnalysis() {
		return analysis;
	}
	
	public Parse getParse() {
		return parse;
	}
	
	public ConstraintNet getNet() {
		return net;
	}

	public int getIteration() {
		return iteration;
	}

	public IncrementalStep getPreviousStep() {
		return previousStep;
	}
	
	public double getScore() {
		return score;
	}
	
	public void setVirtualNodeMatch(boolean virtualNodeMatch) {
		this.virtualNodeMatch = virtualNodeMatch;
	}
	
	public boolean isVirtualNodeMatch() {
		return virtualNodeMatch;
	}
	
	public String getWord() {
		return word;
	}
	
	public String getMode() {
		return mode;
	}
	
	public FrobStats getStats() {
		return stats;
	}
	
	public long getTimeTotal() {
		return timeTotal;
	}
	
	@Override
	public String toString() {
		return String.format("Incremental step for %d '%s', mode: %s", iteration, word, mode);
	}
}
