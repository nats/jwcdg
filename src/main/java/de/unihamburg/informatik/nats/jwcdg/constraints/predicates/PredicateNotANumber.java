package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;


/**
 * Checks if the given thing is not a number
 * the predict() predicates returns the AVMNumber 1 by default
 * and this predicate can be used to check if that default is returned.
 * 
 * @author Arne Köhn
 *
 */
public class PredicateNotANumber extends Predicate {

	public PredicateNotANumber(Grammar g) {
		super("sameType", g, false, 1, false);
	}
	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {
		AVM val1 = args.get(0).eval(lg, context, binding);
		return ! (val1 instanceof AVMNumber);
	}

}
