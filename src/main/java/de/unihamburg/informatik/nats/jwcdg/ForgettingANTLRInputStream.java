package de.unihamburg.informatik.nats.jwcdg;

import java.io.IOException;
import java.io.Reader;

import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CharStream;

public class ForgettingANTLRInputStream extends ANTLRInputStream {

	/**
	 * this class caches just the needed parts and throws everything away once there is no marker.
	 * p is still the position
	 * 
	 */
	protected boolean atEOL;
	protected int numAlreadyForgotChars; // p - nafc == offset for our array
	protected Reader stream;
	protected final static int BUF_SIZE = 1024;
	
	public void forget() {
		if (p-numAlreadyForgotChars > BUF_SIZE) {
			char[] newdata = java.util.Arrays.copyOfRange(this.data, p-numAlreadyForgotChars, this.data.length);
			// this.data[p-numAlreadyForgotChars]; //TODO
			//char[] newdata = new char[1];
			//newdata[0] = nowToken;
			this.data = newdata;
			// this.data[0] = nowToken;
			
			numAlreadyForgotChars = p;
		}
	}
	
	public ForgettingANTLRInputStream(Reader stream) {
		this.stream = stream;
		this.data = new char[0];
		readChunk();

	}
	
	public String substring(int start, int stop) {
		try {
		return new String(data,start-numAlreadyForgotChars,stop-start+1);
		}
		catch(Exception e) {
			return new String(data,start-numAlreadyForgotChars,stop-start+1);
		}
	}
	
	protected void readChunk() {
		this.data = java.util.Arrays.copyOf(data, data.length+BUF_SIZE);
		try {
			int numCharRead = this.stream.read(data, data.length-BUF_SIZE, BUF_SIZE);
			n += numCharRead;
			if (numCharRead <0)
				n -= numCharRead; // reset because we just read nothing, not a negative number of chars
			if (numCharRead < BUF_SIZE) 
				atEOL = true;
		} catch (IOException e) {
			// TODO Somehow throwing exceptions is not wanted by antlr or something
			e.printStackTrace();
		}
	}

	@Override
	public void consume() {
		//System.out.println("prev p="+p+", c="+(char)data[p]);
		if (p == n && !atEOL) {
			// try to read some more
			readChunk();
		}
		if ( p < n ) {
			charPositionInLine++;
			if ( data[p-numAlreadyForgotChars]=='\n' ) {
				/*
				System.out.println("newline char found on line: "+line+
								   "@ pos="+charPositionInLine);
				*/
				line++;
				charPositionInLine=0;
			}
			p++;
			//System.out.println("p moves to "+p+" (c='"+(char)data[p]+"')");
		}
	}

	@Override
	public int LA(int i) {
		if ( i==0 ) {
			return 0; // undefined
		}
		if ( i<0 ) {
			i++; // e.g., translate LA(-1) to use offset i=0; then data[p+0-1]
			if ( (p-numAlreadyForgotChars+i-1) < 0 ) {
				return CharStream.EOF; // invalid; we don't know this part of the input anymore
			}
		}
		if ( (p+i-1) >=n && ! atEOL) {
			readChunk();
		}
		if ( (p+i-1) >= n ) {
			//System.out.println("char LA("+i+")=EOF; p="+p);
			return CharStream.EOF;
		}
		//System.out.println("char LA("+i+")="+(char)data[p+i-1]+"; p="+p);
		//System.out.println("LA("+i+"); p="+p+" n="+n+" data.length="+data.length);
		try {
			return data[p-numAlreadyForgotChars+i-1];
		} catch (Exception e) {
			return data[p-numAlreadyForgotChars+i-1];
		}
	}

}
