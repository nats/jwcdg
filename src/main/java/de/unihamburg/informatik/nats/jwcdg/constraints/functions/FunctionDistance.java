package de.unihamburg.informatik.nats.jwcdg.constraints.functions;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * returns distance between lexem nodes
 */

public class FunctionDistance extends Function {

	public FunctionDistance(Grammar g) {
		super("distance", g, 2);
	}

	@Override
	public AVM eval(List<Term> args, Context context, LexemeGraph lg,
			LevelValue[] binding) {
	
		AVM val1 = args.get(0).eval(lg, context, binding);
		AVM val2 = args.get(1).eval(lg, context, binding);
		
		if ( !(val1 instanceof AVMLexemNode) || !(val2 instanceof AVMLexemNode) ) {
			return new AVMError("WARNING: arguments of function `distance' not all ids");
		}
		
		LexemeNode ln1 = ((AVMLexemNode) val1).getLexemeNode();
		LexemeNode ln2 = ((AVMLexemNode) val2).getLexemeNode();
		
		if(ln1.isNil() || ln2.isNil())
			return new AVMNumber(0);
		
		return new AVMNumber(lg.distanceOfNodes(ln1.getGN(), ln2.getGN(), true));
		
	}

}
