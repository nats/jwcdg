package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.TermPeek;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * returns TRUE if feature exists in lexical entry
 * 
 * @author the CDG-Team
 *
 */
public class PredicateExists extends Predicate {

	public PredicateExists(Grammar g) {
		super("exists", g, false, 1, false);
	}

	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {
		Term t = args.get(0);
		
		if(t instanceof TermPeek ) {
			AVM val = t.eval(lg, context, binding);
			return !( val instanceof AVMError);
		} else {
			throw new PredicateEvaluationException("predicate `exists' needs a lexical access");
		}
	}

}
