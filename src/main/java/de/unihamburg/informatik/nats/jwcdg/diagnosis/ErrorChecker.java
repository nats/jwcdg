package de.unihamburg.informatik.nats.jwcdg.diagnosis;

import java.util.ArrayList;
import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;
import de.unihamburg.informatik.nats.jwcdg.transform.ConstraintViolation;

/**
 * Searches the constraints in a decorated parse for errors. 
 * Gives back all related error messages and a list of the word involved.
 * 
 * @author 3zimmer
 *
 */
public class ErrorChecker {
	
	public static ArrayList<ErrorDiagnosis> makeDiagnoses (DecoratedParse parse){
		ArrayList<ErrorDiagnosis> result = new ArrayList<>();
		for (ConstraintViolation cv : parse.getViolations()){
			makeDiagnosis(result, parse, cv, "Subjekt-Numerus", "Fehlermeldung: Subjekt-Numerus");
			makeDiagnosis(result, parse, cv, "Subjekt-Person", "Fehlermeldung: Subjekt-Person");
			makeDiagnosis(result, parse, cv, "KON-Person", "Fehlermeldung: KON-Person");
		}
		return result;
	}
	
	/**
	 * Checks if a ConstraintViolation is an error represented by its errorName
	 *  and returns an ErrorDiagnosis if true, otherwise null.
	 * 
	 * @param result
	 * @param parse
	 * @param cv
	 * @param errorName
	 * @param errorMessage
	 */
	private static void makeDiagnosis(ArrayList<ErrorDiagnosis> result, DecoratedParse parse, ConstraintViolation cv, String errorName, String errorMessage){
		ErrorDiagnosis diagnosis;
		if (errorName.equals(cv.getConstraint().getId())){
			List<String> levels = parse.getLevels();
			int wordIndex1 = parse.calcIndex1(cv);
			int wordIndex2 = parse.calcIndex2(cv);
			diagnosis = new ErrorDiagnosis(errorName, errorMessage);
			diagnosis.addWordIndex(wordIndex1);
			for (String level : levels){
				int headIndex = parse.getHeadOf(wordIndex1, level);
				if (headIndex != -1){
					diagnosis.addWordIndex(headIndex);
				}
			}
			if (cv.getLV2() != null){
				diagnosis.addWordIndex(wordIndex2);
				for (String level : levels){
                    // FIXME das sieht nach einem Fehler aus!
					int headIndex = parse.getHeadOf(wordIndex1, level);
					headIndex = parse.getHeadOf(wordIndex2, level);
					if (headIndex != -1){
						diagnosis.addWordIndex(headIndex);
					}
				}
			}
			result.add(diagnosis);
		}
	}
	
}
