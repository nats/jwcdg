package de.unihamburg.informatik.nats.jwcdg.diagnosis;

import java.util.HashSet;

public class ErrorDiagnosis {
	
	public String _constraintName;
	public String _errorMessage;
	public HashSet<Integer> _wordIndexes = new HashSet<>();

	public ErrorDiagnosis(String constraintName, String errorMessage){
		_constraintName = constraintName;
		_errorMessage = errorMessage;
	}
	
	public void addWordIndex(int wordIndex){
		if (! _wordIndexes.contains(wordIndex)){
			_wordIndexes.add(wordIndex);
		}
	}

	@Override
	public String toString() {
		return "ErrorDiagnosis [_constraintName=" + _constraintName
					+ ", _errorMessage=" + _errorMessage + ", wordIndexes=" + _wordIndexes
					+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result =
					prime
								* result
								+ ((_constraintName == null) ? 0
											: _constraintName.hashCode());
		result =
					prime
								* result
								+ ((_errorMessage == null) ? 0
											: _errorMessage.hashCode());
		result =
					prime
								* result
								+ ((_wordIndexes == null) ? 0
											: _wordIndexes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ErrorDiagnosis other = (ErrorDiagnosis) obj;
		if (_constraintName == null) {
			if (other._constraintName != null)
				return false;
		}
		else if (! _constraintName.equals(other._constraintName))
			return false;
		if (_errorMessage == null) {
			if (other._errorMessage != null)
				return false;
		}
		else if (! _errorMessage.equals(other._errorMessage))
			return false;
		if (_wordIndexes == null) {
			if (other._wordIndexes != null)
				return false;
		}
		else if (! _wordIndexes.equals(other._wordIndexes))
			return false;
		return true;
	}
}
