package de.unihamburg.informatik.nats.jwcdg.misc.koehnexperiments;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.antlr.runtime.RecognitionException;

import de.unihamburg.informatik.nats.jwcdg.DefaultParserFactory;
import de.unihamburg.informatik.nats.jwcdg.JWCDG;
import de.unihamburg.informatik.nats.jwcdg.Parser;
import de.unihamburg.informatik.nats.jwcdg.antlr.Helpers;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;
import de.unihamburg.informatik.nats.jwcdg.parse.Word;

/**
 * @author Arne Köhn
 * 
 */
public class ParseNegra {

	/**
	 * @param args [Configuration file] [folder containing negra data] [startnumber] [endnumber] [outtemplate]
	 * @throws IOException 
	 * @throws RecognitionException 
	 */
	public static void main(String[] args) throws IOException, RecognitionException {
		Configuration myConfig = Configuration.fromFile(args[0]);
		if (args.length != 5) {
			System.err.println("usage: ParseNegraIncremental [Configuration file] [folder containing negra data] [startnumber] [endnumber] [outtemplate]");
			System.exit(1);
		}
		File folder = new File(args[1]);
		int start = Integer.parseInt(args[2]);
		int end = Integer.parseInt(args[3]);
		if ( ! folder.isDirectory()) {
			System.err.println("The second argument needs to be the directory containing the negra sentences!");
			System.exit(1);
		}
		parse(myConfig, folder, start, end, args[4]);
	}

	protected static void parse(Configuration config, File folder, int start, int end, String outtmpl) throws IOException, RecognitionException {
		Parser parser = JWCDG.makeParser(config, new DefaultParserFactory());
		for (int sentenceNr = start; sentenceNr<=end; sentenceNr++) {
			File f = new File(folder, String.format("negra-s%d.cda", sentenceNr));
			List<String> sentence = new ArrayList<>();
			for (Word word: Helpers.parserFromIS(new FileReader(f)).annoEntry().getWords()) {
				sentence.add(word.word);
			}
			Parse result = parser.parse(sentence);
			FileWriter writer = new FileWriter(String.format(outtmpl, sentenceNr));
			writer.write(result.toAnnotation(false));
			writer.close();
		}
	}
}
