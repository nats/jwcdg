package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

public class PredicateEvaluationException extends RuntimeException {
	private static final long serialVersionUID = 3553454041365067653L;
	
	public PredicateEvaluationException(String message) {
		super(message);
	}
	
	@Override
	public String toString() {
		return super.getMessage();
	}
}
