package de.unihamburg.informatik.nats.jwcdg.input;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMDisj;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMFlat;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMList;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.avms.Path;

public class LexiconItem implements Comparable<LexiconItem>, java.io.Serializable {
	/** Version for serialization. Please increment when changing fields.*/
	private static final long serialVersionUID = 1L;
	/** identifier */
	private String description;
	/** lexem */
	private String word;
	/** tree of lexical information */
	private AVM    values;
	/** features that distinguish this item from its homonyms */
	private ArrayList<Path> distinctiveFeatures;
	/** regular expression that must be matched */
	private Pattern re;

	private SourceInfo source;
	
	public LexiconItem(String word, AVM value, SourceInfo s) {
		this.word = word;
		this.description = word; // distinctive features will be added to the description later on 
		this.values = value;
		this.source = s;
		this.distinctiveFeatures = new ArrayList<>();
		this.re = null;
	}
	
	public LexiconItem(Pattern re, AVM value, SourceInfo s) {
		this.re = re;
		this.description = re.toString(); // distinctive features will be added to the description later on 
		this.values = value;
		this.source = s;
		this.word = ""; // all regexp-LexiconItems are stored as "" in the lexicon
		this.distinctiveFeatures = new ArrayList<>();
	}
	@Override
	public int compareTo(LexiconItem other) {
		return this.description.compareTo(other.description);
	}

	public String getDescription() {
		return description;
	}

	public String getWord() {
		return word;
	}

	/**
	 * Check whether one of FEATURES distinguishes A from B.
	 */
	public boolean differByFeatures(LexiconItem li2, Set<Path> features) {
		for(Path p: features) {
			if (differByFeature(li2, p, false)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Does FEATURE distinguish this from another lexicon item?
	 * 
	 * if unify is set, missing features do not prevent equality
	 */
	public boolean differByFeature(LexiconItem li2, Path feat, boolean unify)
	{
		AVM va = this.getFeature(feat);
		AVM vb =  li2.getFeature(feat);
		
		/* if neither item has the feature, it doesn't. */
		if (va instanceof AVMError && vb instanceof AVMError) {
			return false;
		}
	
		/* What if one item has the feature and the other doesn't? Well, for
		 * disambiguating lexical entries, an absent feature should not be
		 * different from a specified feature because there is no way of
		 * unambiguously specifying the form with the feature absent. Such a
		 * situation is really an error in the lexicon and should be marked as a
		 * duplicate lexical entry. On the other hand, when building LVs it makes
		 * a big difference -- imagine a verb which demands a certain case for
		 * its object, and one which demands none because it doesn't even take an
		 * object. Clearly the two should not be merged into one LV. */
		if (va instanceof AVMError || vb instanceof AVMError) {
			if (unify) {
				return false;
			} else {
				return true;
			}
		}
	
		/* different types are a difference. */
		if (va.getClass() != vb.getClass()) {
			return true;
		}
	
		/* different strings are a difference. */
		if (va instanceof AVMString) {
			if (((AVMString) va).getString().equals(((AVMString) vb).getString())) {
				return false;
			} else {
				return true;
			}
		}
	
		/* different numbers are a difference. */
		if (va instanceof AVMNumber) {
			if (((AVMNumber) va).getNumber() ==  ((AVMNumber) vb).getNumber()) {
				return false;
			} else {
				return true;
			}
		}
	
		/* different lists are a difference. */
		if (va instanceof AVMList) {
			List<AVM> la = ((AVMList) va).getList();
			List<AVM> lb = ((AVMList) vb).getList();
			if (la.size() != lb.size()) {
				return true;
			}
			for (int i = 0; i < la.size(); i+=2) {
				AVM va1 = la.get(i);
				AVM vb1 = lb.get(i);
				// TODO #EXCEPTION_HANDLING proper error handing in case of casting errors
				String sa = ((AVMString) va1).getString();
				String sb = ((AVMString) vb1).getString();
				if (! sa.equals(sb)) return true;
				
				AVM va2 = la.get(i+1);
				AVM vb2 = lb.get(i+1);
				double na = ((AVMNumber) va2).getNumber();
				double nb = ((AVMNumber) vb2).getNumber();
				if (na != nb) return true;
			}
		}
		
		return false;
	}

	/**
	 * return a feature identified by the given path
	 * if no such feature exists, AVMError is returned
	 * 
	 * @param path
	 * @return
	 */
	public AVM getFeature(Path path) {
		return values.followPath(path);
	}
	
	@Override
	public String toString() {
		return String.format("LexiconItem: %s (%s)", word, description);
	}

	/**
	 * transforms the hierachical avm into a flat (and less expensive to read) version
	 * 
	 * @param allAttributes
	 */
	public void cacheValues(List<Path> allAttributes) {
		values = new AVMFlat(values, allAttributes);
	}

	public AVM getValues() {
		return values;
	}

	/**
	 * This function expands all disjunctions encountered among the features of this LexiconItem.
	 * Each disjunction multiplies the size of the result set according to the number of disjuncts
	 * and every copy will contain one of the disjuncts instead of the disjunction.
	 * Coordinated disjunctions are treated as one, they fork the result set only once.
	 * 
	 * No disjunctions are left in any of the results.
	 * If no disjunctions are encountered, the list containing only this element is returned.
	 * 
	 * The result's feature hierarchy is always a flattened.
	 * 
	 * @param the attributes that should be considered
	 * @return
	 */
	public List<LexiconItem> expandDisjunctionsAndFlatten(List<Path> allAttributes) {
		List<LexiconItem> result    = new ArrayList<>();
		List<LexiconItem> workQueue = new ArrayList<>();
		
		/* first make sure our avm is flat*/
		this.cacheValues(allAttributes);
		
		workQueue.add(this);
		for (Path path : allAttributes) {
			for (LexiconItem li: workQueue) {
				AVM val = li.values.followPath(path);
				if (! (val instanceof AVMDisj)) {
					// if li doesn't have a disj here, no copy in the queue has,
					// so nothing changes.
					result = workQueue;
					break;
				}
				AVMDisj vald = (AVMDisj) val;
				// check if the disjunction is coordinated
				int coordIndex = vald.getCoordinationIndex(); 
				List<AVM> disjunction = vald.getList();
				if (coordIndex == 0) { // no coordination
					// create new LIs with the disjunction replaced by the disjuncts
					for (AVM disjunct : disjunction) {
						AVM newV = ((AVMFlat)li.values).copyWithChangedValue(path, disjunct);
						if (li.word.equals("")) { // template
							result.add(new LexiconItem(li.re,   newV, source));
						} else { // ordinary entry
							result.add(new LexiconItem(li.word, newV, source));
						}
					}
				} else { // we need to coordinate
					// iterate over all disjuncts and create a new LI for each
					for (int i = 0; i < disjunction.size(); i++) {
						HashMap<Path, AVM> resultMap = ((AVMFlat)li.values).getMapCopy();
						// iterate over all values an replace those that are disjs coordinated with the current one
						// other uncoordinated disjunctions are not dealt with here
						for (Path p2 : allAttributes) {
							AVM oldAtt = resultMap.get(p2);
							if (oldAtt instanceof AVMDisj) {
								// we found a disjunktion, let's see if it is coordinated
								if (((AVMDisj)oldAtt).getCoordinationIndex() == coordIndex) {
									// it is so we have to select the coordianted value, i.e. the ith one
									resultMap.put(p2, ((AVMDisj) oldAtt).getList().get(i));
								}
							}
						}
						AVM newV = new AVMFlat(resultMap);
						if (li.word.equals("")) { // template
							result.add(new LexiconItem(li.re,   newV, li.source));
						} else { // ordinary entry
							result.add(new LexiconItem(li.word, newV, li.source));
						}
					}
				}
			}
			workQueue = result;
			result = new ArrayList<>();
		}
		
		return workQueue;
	}

	/**
	 * Note PATH as a distinguishing feature.
	 * Also, append the value for path to the description.
	 */
	public void noteDistinctiveFeature(Path p) {
		distinctiveFeatures.add(p);
		
		AVM v = values.followPath(p);
		if (v instanceof AVMString) {
			AVMString sval = (AVMString) v;
			description = description+"_"+sval.getString();
		}
	}
	
	public Pattern getPattern() {
		return re;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public SourceInfo getSource() {
		return source;
	}
	
	
	/**
	 * return a new Lexicon item with the give nword as desciption but 
	 * the features of this one 
	 * 
	 * placeholders like \\1 in the feratures are substituted by matched subexpressions 
	 * of the pattern
	 * 
	 * @param word
	 * @return
	 */
	public LexiconItem instantiatePattern(String word, Configuration config) {
		if(re == null) {
			throw new RuntimeException("ERROR: Called instantiate on a non-template LexiconItem!");
		}
		
		/* A lexical template may capture substrings which are then
		   accessible as backreferences in the newly created Value.
		   turn the string '\1' into whatever was matched. */
		Matcher matcher = re.matcher(word);
		if(!matcher.matches()) // we need to match once
			return null; // why did you even call me?
		AVM v = values.substituteRefs(matcher, config);
		
		return new LexiconItem(word, v, source);
	}
	
	public List<Path> getDistinctiveFeatures() {
		return distinctiveFeatures;
	}
	
	@Override
	public int hashCode() {
		return description.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		
		if(!(obj instanceof LexiconItem))
			return false;
		
		LexiconItem li2= (LexiconItem) obj;
		
		if(!this.word.equals(li2.word))
				return false;
		
		if(!description.equals(li2.description))
			return false;
		
		if(this.distinctiveFeatures.size() != li2.distinctiveFeatures.size())
			return false;
		
		for(Path p : distinctiveFeatures) {
			if(!li2.distinctiveFeatures.contains(p))
				return false;
		}
		
		if(this.re == null || li2.re == null) {
			if(this.re != li2.re)
				return false;
		} else {
			if(!this.re.equals(li2.re))
				return false;
		}
		
		if(!this.values.equals(li2.values))
			return false;	
		
		return true;
	}

}
