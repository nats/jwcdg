package de.unihamburg.informatik.nats.jwcdg.constraintNet;

import java.util.Comparator;

public class LvComparatorByScore implements Comparator<LevelValue> {

	@Override
	public int compare(LevelValue lv1, LevelValue lv2) {
		if(lv1.getScore() > lv2.getScore())
			return 1;
		else if(lv1.getScore() < lv2.getScore())
			return -1;
		else {
			return Integer.valueOf(lv1.getIndexWRTNet()).compareTo(lv2.getIndexWRTNet());
		}
	}

}
