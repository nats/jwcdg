package de.unihamburg.informatik.nats.jwcdg.constraints;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.formulas.Formula;
import de.unihamburg.informatik.nats.jwcdg.constraints.predicates.PredicateEvaluationException;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.TermNumber;
import de.unihamburg.informatik.nats.jwcdg.evaluation.ConstraintEvaluationResult;
import de.unihamburg.informatik.nats.jwcdg.input.SourceInfo;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

public class Constraint implements Serializable {
	
    private static final long serialVersionUID = 1L;
    public static final String ATTR_CONTEXT_SENSITIVE   = "contextSensitive"; 
	public static final String ATTR_TEMP_PENALIZED      = "temporarilyPenalized";
	public static final String ATTR_TEMP_ALLOWED        = "temporarilyAllowed";
	public static final String ATTR_NONSPEC_FLUCTUATION = "fluctuatingNonspec";
	public static final String ATTR_FLUCTUATION         = "fluctuating";
	
	private static Logger logger = LogManager.getLogger(Constraint.class);
	
	private int no;                    /* for unique identification */
	private String id;			       /* constraint identification */
	
	private List<VarInfo> vars;	       /* info about variables */
	private Signature sig;		       /* what configurations matter? */
	private String section;		       /* section name */
	
	private Term penaltyTerm;		   /* penalty term */
	private Set<String> attributes;    /* optional attributes, List of Strings */
	public Formula formula;		       /* formula */
	
	private boolean isContextSensitive;   /* does it use c.s. functions/preds? */
	// private boolean crossLevelHint;         /* whether other level domains for the affected words are considered too in domain optimization */
	
	private SourceInfo source;
	// private boolean nonspecStatus;          /* was a NONSPEC TopPeek attempted in the last evaluation?  */
	
	public Constraint(String id, String section, Signature sig, Formula formula, Term penaltyTerm, List<VarInfo> vars, List<String> attributes, SourceInfo source ) {
		this.id = id;
		this.formula = formula;
		this.sig = sig;
		
		this.section = section;
		
		// assigning id to constraints happens when it is added to a grammar
		this.no = 0;
		
		this.vars = vars;
		this.penaltyTerm = penaltyTerm;
		this.source = source;
		
		this.attributes = new HashSet<>(attributes);
		
		// determine contextsensitivity by formula 
		// or override context sensitivity by the grammar designers wish
		if(attributes.contains(ATTR_CONTEXT_SENSITIVE) || formula.isContextSensitive) {
			isContextSensitive = true;
		} else {
			isContextSensitive = false;
		}
	}

	public int getNo() {
		return no;
	}
	
	public String getId() {
		return id;
	}
	
	public SourceInfo getSource() {
		return source;
	}
	
	public boolean hasFlag(String name) {
		return attributes.contains(name);
	}

	/**
	 * will evaluate the penalty term of this constraint for the given environment consisting of
	 * 
	 * @param lg the lexemegraph
	 * @param context all levelvalues in the analysis 
	 * @param binding the variable binding
	 * @return the returned value converted to double
	 * 
	 * will throw runtimeexceptions if the result of the evaluation is of another type than AVMNumber 
	 */
	public double getPenalty(LexemeGraph lg, Context context, LevelValue[] binding) {
		AVM avm = penaltyTerm.eval(lg, context, binding);
		if (avm instanceof AVMNumber) {
			AVMNumber num = (AVMNumber) avm;
			return num.getNumber();
		} else if (avm instanceof AVMError) {
			AVMError err = (AVMError) avm;
			throw new RuntimeException(String.format(
					"ERROR: evaluation of the penalty term of constraint %s did result in an error: '%s'",
					id, err.getMessage()));
		} else {
			throw new RuntimeException(String.format(
					"ERROR: penalty term of constraint %s did not evaluate to a number but to: '%s'",
					id, avm.toString()));
		}
	}
	
	/**
	 * will return the penalty as long as it is not a complex term
	 * for complex penalty terms NaN will be returned
	 */
	public double getPenalty() {
		if (penaltyTerm instanceof TermNumber) {
			TermNumber num = (TermNumber) penaltyTerm;
			return num.getNumber();
		} else {
			return Double.NaN;
		}
	}

	public boolean isContextSensitive() {
		return isContextSensitive;
	}

	public List<VarInfo> getVars() {
		return vars;
	}

	public Signature getSignature() {
		return sig;
	}

	/**
	 * Evaluate a unary constraint on LV.
	 *
	 * This function evaluates a unary constraint. 
	 *
	 * does NOT check signature matching like whether the level of the lv conforms to the constraint!
	 */
	public ConstraintEvaluationResult eval(LexemeGraph lg, Context context, LevelValue lv) {
		return eval(lg, context, lv, null);
	}

	/**
	 Evaluate one {unary,binary} constraint on {one,two} LVs.
	 lvb == null means unary mode

	 Evaluate a constraint w.r.t. the specified LVs.

	 Here's the detailed algorithm:

	 - check the number of arguments and returns TRUE (with a warning) if
	 it does not match the arity of c
	 - instantiate the variables in c->vars with the LVs passed
	 as arguments
	 - do *not* check whether c is applicable to these LVs. Applying
	 constraints to arbitrary LVs can produce spurious constraint
	 violations without warning! Hence evalConstraint() should only be
	 called on constraints whose applicability was previously checked by
	 match{Unary,Binary}Signature().
	 - set evalCurrentConstraint and evaluates c->formula using evalFormula()
	 - reset evalCurrentConstraint
	 - if the result was FALSE and c has a variable penalty
	 (if c->penaltyTerm is non-NULL), set c->penalty to evalTerm(c->penaltyTerm)
	 - execute the hook HOOK_EVAL
	 - de-instantiate c->vars
	 - increase either evalUnary or evalBinary
	 - return the result of evalFormula()

	 The context may be NULL. If it is not NULL it should be a Vector of LVs,
	 each of which must be located at its index as calculated by lvIndex().
	 This is then passed on to predicates and functions that need to examine
	 the context of an LV to operate.

	 If the constraint c uses such a function or predicate (that is, if it has
	 its is_context_sensitive flag set) and there is no context, then
	 evalConstraint() returns TRUE. This is obviously not always correct, so
	 if you do not provide context to your evaluations and still use
	 context-sensitive constraints, you can miss some conflicts.
	 */
	public ConstraintEvaluationResult eval(LexemeGraph lg, Context context, LevelValue lva, LevelValue lvb) {
		assert ((sig.arity == 2) != (lvb==null));
		
		boolean violated;
		double score;
		AVM val;

		/* if we have no context, don't even try to find a conflict. */
		if (isContextSensitive && context == null) {
			return new ConstraintEvaluationResult(this, lva, lvb, false, 1.0);
		}
		
		/* instantiate constraint variable */
		LevelValue[] binding = {lva, lvb};
		try {
			violated = !formula.evaluate(binding, context, lg);
		} catch (PredicateEvaluationException e) {
			boolean reactivatedLVfound= false;
			if(lva.getIndexWRTNet() ==-1 || (lvb != null && lvb.getIndexWRTNet() ==-1)) {
				reactivatedLVfound = true;
			}
			if(!reactivatedLVfound) {
				logger.warn(String.format(
						"%s%nAn error occured in the evaluation of '%s', it will be treated as violated!",
						e.getMessage(), this.id));
			}
			violated = true;
		}
		
		if (violated) {
			/* determine actual penalty */
			try {
				val= penaltyTerm.eval(lg, context, binding);
				if (val instanceof AVMNumber) {
					AVMNumber num = (AVMNumber) val;
					score = num.getNumber();
				} else {
					logger.warn(String.format(
							"Penalty term of `%s' doesn't evaluate to a number", id));
					score = 0.0;
				}
			} catch (Exception e) {
				logger.warn(String.format(
						"An error occured in the evaluation of the penalty term of '%s', defaulting to 1.0!",
						this.id));
				score = 1.0;
			}
		} else {
			score = 1.0;
		}

		// TODO #STATISTICS we can't collect them here...
		// net->evalUnary++;
		// net->evalBinary++;
		
		if(lg.isComplete()) {
			if(attributes.contains(ATTR_TEMP_PENALIZED)) {
				// this constraint does not apply in complete sentences, 
				// comsider the score as 1.0 
				score = 1.0;
			}
			if(attributes.contains(ATTR_TEMP_ALLOWED)) {
				// this kind of constraints are considered hard in complete sentences, 
				// thus ignore the usual score and simply asign 0.0 
				score = 0.0;		
			}
		}
		
		return new ConstraintEvaluationResult(this, lva, lvb, violated, score);
	}

	public String getSection() {
		return section;
	}

	public Term getPenaltyTerm() {
		return penaltyTerm;
	}

	public void setNo(int i) {
		no = i;
	}

	public Set<String> getAttributes() {
		return attributes;
	}

	public Formula getFormula() {
		return formula;
	}
	
	/**
	 * returns true if
	 * - the section of the constraint is contained in the list of fluctuating
	 *   sections
	 * - the regent would be nonspec and constraint or its section is
	 *   contained in the list of constraints fluctuating fluctuating regarding
	 *   nonspec
	 *
	 * otherwise it return false
	 */
	public boolean isFluctuating(boolean isNonspec) {
		if(attributes.contains(ATTR_FLUCTUATION)) {
			return true;
		}
		if(isNonspec && attributes.contains(ATTR_NONSPEC_FLUCTUATION)) {
			return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return String.format("%s (%s)", id, penaltyTerm.toString());
	}

	/** 
	 * Creates a constraint representation in the original grammar format.
	 * All extras (newline, spaces, comments etc.) are lost.
	 */
	public String toConstraintString() {
		String result = this.sig.toString();
		result += " : " + this.getPenaltyTerm().toString();
		result += " : " + this.formula.toString() + ";";
		return result;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Constraint))
			return false;
		Constraint other = (Constraint) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
