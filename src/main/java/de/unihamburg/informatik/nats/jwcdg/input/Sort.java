package de.unihamburg.informatik.nats.jwcdg.input;

import java.util.List;

/**
 * A tree structure of sorts,
 * used for Hierarchies in the grammar
 * 
 * @author the CDG-Team
 *
 */
public class Sort {

	private String id;                 /* sort identifikation */
	private List<Sort> subSorts;       /* list of subsorts */
	private int depth;                 /* # of levels below root */
	
	/**
	 * 
	 * 
	 * @param id
	 * @param l  
	 */
	public Sort(String id, List<Sort> l) {
		this.id = id;
		subSorts = l;
		int max = 0;
		for(Sort s : subSorts) {
			if(s.depth > max)
				max = s.depth;
		}
		depth = max+1;
	}
	
	
	public String getId() {
		return id;
	}
	
	public List<Sort> getSubSorts() {
		return subSorts;
	}
	
	public int getDepth() {
		return depth;
	}
	
	@Override
	public String toString() {
		return String.format("%s->(%s)(%d)", id, subSorts.toString(), depth );
	}
}
