package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.ArrayList;
import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.Level;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * Does the level contain a cycle?
 */
public class PredicateCycle extends Predicate {
	
	public PredicateCycle(Grammar g) {
		super("cycle", g, true, 1, false);
	}

	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {

		ArrayList<Boolean> cleared;
		ArrayList<Boolean> seen;
		Level level;

		/* if there is no context at all, we cannot check this condition, so we
		 * must assume that it is satisfied. */
		if (context == null) {
			return false;
		}

		AVM val = args.get(0).eval(lg, context, binding);
		String levelName;
		if (val instanceof AVMString) {
			levelName = ((AVMString)val).getString();
		}else {
			throw new PredicateEvaluationException("type mismatch (1st arg) for `cycle'");
		} 

		/* find level */
		level = grammar.findLevel(levelName);
		if (level == null) {
			throw new PredicateEvaluationException(String.format(
					"no level `%s'", levelName));
		}

		/* check all edges */
		seen    = new ArrayList<>(lg.getMax());
		cleared = new ArrayList<>(lg.getMax());
		for(int i = 0; i < lg.getMax(); i++) {
			seen   .set(i, false);
			cleared.set(i, false);
		}

		for (int i = 0; i < context.getNoOfLVs(); i++) {
			LevelValue lv = context.getLV(i);

			if (lv== null) {
				continue;
			}

			/* skip LVs of other levels */
			if (i % grammar.getNoOfLevels() != level.getNo()) {
				continue;
			}

			/* search connection to NIL */
			while (lv != null) {
				int index;

				/* detect loop */
				if (seen.get(lv.getDependentGN().getArc().from)) {
					return true;
				}
				seen.set(lv.getDependentGN().getArc().from, true);

				/* find already cleared LV */
				if (    !lv.getRegentGN().spec() || 
						cleared.get(lv.getDependentGN().getArc().from)) {

					/* set seen LVs to cleared also */
					int j;

					for (j = 0; j < seen.size(); j++) {
						if (seen.get(j)) {
							cleared.set(j, true);
						}
					}
					for(int k = 0 ; k < lg.getMax(); k++) {
						seen.set(k, false);
					}
					break;
				}

				/* step upward */
				index = grammar.getNoOfLevels() * lv.getDependentGN().getArc().from + lv.getLevel().getNo();
				lv = context.getLV(index);
			}
		}
		
		return false;
	}
}
