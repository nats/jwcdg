package de.unihamburg.informatik.nats.jwcdg.constraints.functions;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.Level;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.TermPeek;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;


/* ----------------------------------------------------------------------
 Return the maximal length of dependency chains under a given word.
 */
public class FunctionHeight extends Function {

	public FunctionHeight(Grammar g) {
		super("height", g, 1, 1, true);
	}

	@Override
	public AVM eval(List<Term> args, Context context, LexemeGraph lg,
			LevelValue[] binding) {

		TermPeek t = (TermPeek) args.get(0); 
		AVM val = t.eval(lg, context, binding);
		LexemeNode ln;
		if (val instanceof AVMLexemNode) {
			ln = ((AVMLexemNode) val).getLexemeNode();
		} else {
			return new AVMError("WARNING: type mismatch (1st arg) for `height'");
		}
		
		if (!ln.spec()) {
			return new AVMError("WARNING: unspecified 1st arg for `height'");
		}

		Level level = binding[t.getVarIndex()].getLevel();

		int h = funcHeightImpl(ln, level, lg, context);
		return new AVMNumber(h);
	}

	/**
	 * Perform the work of funcHeight().
	 */
	private int funcHeightImpl(LexemeNode ln, Level level, LexemeGraph lg, Context context) {
		LevelValue lv;
		int noOflevels = grammar.getNoOfLevels();
		int result = 0;

		for (int i = lg.getMin(); i <= lg.getMax(); i++) {
			int index = noOflevels * i + level.getNo();
			int height = 0;
			if (index >= context.getNoOfLVs())
				break;

			while (height < lg.getMax()) {
				height++;
				
				lv = context.getLV(index);
				if (lv == null || !lv.getRegentGN().spec())
					break;
				if ((lv.getRegents().get(0)) == ln) {
					if (height > result) {
						result = height;
					}
					break;
				}
				index = noOflevels * lv.getRegentGN().getArc().from + level.getNo();
			}
		}
		
		return result;
	}
}
