package de.unihamburg.informatik.nats.jwcdg.frobbing;

import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;

public class CombinedFactory extends FrobbingFactory {

	public static final String NAME = "combined";

	public CombinedFactory(Grammar grammar, Lexicon lex, Configuration config) {
		super(grammar, lex, config, FrobbingMethod.Combined);
	}
}
