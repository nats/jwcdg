package de.unihamburg.informatik.nats.jwcdg.constraintNet;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.base.Stopwatch;

import de.unihamburg.informatik.nats.jwcdg.constraints.Constraint;
import de.unihamburg.informatik.nats.jwcdg.constraints.Level;
import de.unihamburg.informatik.nats.jwcdg.evaluation.ConstraintEvaluationResult;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluationResult;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.frobbing.AbstractFrobber;
import de.unihamburg.informatik.nats.jwcdg.helpers.DaemonThreadFactory;
import de.unihamburg.informatik.nats.jwcdg.incrementality.virtualNodes.VirtualLexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.incrementality.virtualNodes.VirtualNodeSpec;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;
import de.unihamburg.informatik.nats.jwcdg.lattice.Lattice;
import de.unihamburg.informatik.nats.jwcdg.predictors.MaltPredictor;
import de.unihamburg.informatik.nats.jwcdg.predictors.Tagger;

/**
 * Representation of the actual constraint optimization problem
 * 
 * @author the CDG-Team
 *
 */
public class ConstraintNet {
	/* statics */
	public static final String USENONSPEC           = "useNonspec";
	public static final String PR_LABELEXCEPTIONS   = "labelExceptions";
	public static final String USE_VIRTUAL_NODES    = "useVirtualNodes";
	public static final String USE_EDGEREACTIVATION = "useEdgeReactivation";
	
	private static int cnCounter = 0;
	private static Logger logger = LogManager.getLogger(ConstraintNet.class);
	private static final boolean performTimingMeasurements = false;

	/* fields */
	
	/** A unique identifier for the net. Each net created by the system is labeled as 
    net <i>n</i>, where <i>n</i> is the current value of cnCounter.  */
	private String id; 
	
	/** the enriched word graph used  in constructing the net. */
	private LexemeGraph      lexemeGraph;
	/** the constraint grammar  used  in constructing the net. */
	private Grammar          grammar;
    /** the constraint grammar  used  in constructing the net. */
	private Lexicon          lexicon;
	/** the evaluator to score all levelvalues */
	private GrammarEvaluator evaluator;
	
	private ArrayList<LevelValue>     values;  /* Vector of LevelValues */
	private List<ConstraintNode> nodes;   /* Vector of ConstraintNode */

	//private Agenda searchagenda;     /* Agenda for searching, it is used
    //  by netsearch() */

	/**< Counting unary evaluations */
	private long evalUnary;           
	
    /**< Counting unary statistics */
	private int statUnary;
	
	/**< Counting binary evaluations */
	private long evalBinary;
	
	/**< Counting all LVs being built */
	private AtomicInteger lvCountTotal;
	
	/**< Counting LVs being built and remaining in the net */
	private int lvCountBuild;
	
	/**< Counting LVs being immediately deleted*/
	private AtomicInteger lvCountDeleted;
	
	/**< Counting LVs being postponed when built*/
	private AtomicInteger lvCountPostponed;
	
	/**< holds currently not used LVs that might get reactivetaed later on */
	private ConcurrentLinkedQueue<LevelValue> postponedLVs; 
	
	/* caching */
	/* *********/
	/** for looking up lexemes quickly by tp*/
	private Map<Integer,List<LexemeNode>> lexemes;          
             
	/** cache level use flags for fast access */
	private Map<Integer, Boolean> levelActive;
    /** Number of levels in the problem. */
	private int                   noOfLevels;
	/** # of levels with their uselevel flag set */
	private int                   noOfActiveLevels;      
	
	private boolean valid = true;
	
	//private boolean interrupted = false; // TODO #INTERRUPTION who will set this?
	private Configuration config;
	
	private static ExecutorService executorService;
	/* ****************
	 * Constructors   *
	 ******************/
	/**
	   This function returns a new ConstraintNet with a unique name,
	   but with all fields set to meaningless default values. Building a
	   complete constraint net is a multi-stage process that involves several
	   of the other functions of this module.
	 */
	private ConstraintNet() {
		this.id = String.format("net%d", cnCounter++);
		this.values = null; 
		this.nodes = null;
		//this.searchagenda = null;
		this.evalUnary = 0;
		this.evalBinary = 0;
//		this.evalTernary = 0;
		this.statUnary = 0;
//		this.cache = NULL;
//		this.cacheTernary = NULL;
//		this.evalTernary = 0;
		this.postponedLVs = new ConcurrentLinkedQueue<>();
		this.lvCountTotal= new AtomicInteger(0);
		this.lvCountBuild=0;
		this.lvCountDeleted=new AtomicInteger(0);
		this.lvCountPostponed=new AtomicInteger(0);
	}
	
	/**
	   Build a constraint net from LAT.

	   This function returns a new constraint net for a word graph. This
	   function uses \b cnBuildInit(), \b cnBuildNodes(), and
	   \b cnBuildFinal(). The resulting net contains no edges. The function
	   performs these initializations:
	   -  \b id is set to \b net<cnCounter>
	   -  the \b lexemgraph is built
	   -  \b state is set to \b NSCreated

	   If BUILDLVS is set, the nodes in the constraint net will be filled with
	   LVs, otherwise the net will contain only constraint nodes, no LVs.
	 */
	public ConstraintNet(Lattice lat, Lexicon lexicon, 
			boolean buildLVs, boolean complete, GrammarEvaluator evaluator,
			Configuration config) {
		this();
		
		long starttime = System.currentTimeMillis();
		
		this.id = String.format("net%d", cnCounter++);
		
		this.grammar   = evaluator.getGrammar();
		this.evaluator = evaluator;
		this.lexicon   = lexicon;
		
		if (executorService == null) {
			int numProcs = config.getInt(AbstractFrobber.NUM_THREADS);
			executorService = Executors.newFixedThreadPool(
					numProcs == 0? Runtime.getRuntime().availableProcessors() : numProcs,
					new DaemonThreadFactory());
		}
		
		this.config = config;

		if(!tag(lat)) {
			throw new RuntimeException("ERROR: tagging failed");
		}

		lexemeGraph.setComplete(complete);

		// gather levels
		noOfLevels     = grammar.getNoOfLevels();
		levelActive  = new HashMap<>();
		noOfActiveLevels = 0;
		for (Level level : grammar.getLevels()) {
			if (level.isUsed()) {
				noOfActiveLevels++;
				levelActive.put(level.getNo(), true);
			} else {
				levelActive.put(level.getNo(), false);
			}
		}
		
		/* build nodes */
		if (!buildNodes(buildLVs, evaluator)) {
			logger.warn("WARNING: invalid net, net contains no nodes\n");
			valid = false;
			return;
		}

//		if (interrupted) {
//			// TODO #INTERRUPTION 
//			logger.warn("WARNING: interrupt while building a constraintnet\n");
//			this.valid = false;
//			return;
//		}

		/* sort LVs in each node according to their score */
		sortLVs(new LvComparatorByScore());
		
		/* build table of lexeme nodes */
		logger.info("Building table of lexemes...");
		lexemes = new HashMap<>(lexemeGraph.getMax());
		for(int i= 0; i< lexemeGraph.getMax(); i++) {
			lexemes.put(i, new ArrayList<>());
		}
		for (LexemeNode ln : lexemeGraph.getNodes()) {
			for (int j = ln.getArc().from; j < ln.getArc().to; j++) {
				lexemes.get(j).add(ln);
			}
		}
		logger.info(" done.\n");
		
		logger.info(String.format("Initializing the constraintNet took %.3f sec", (System.currentTimeMillis()- starttime)/1000.0));
	}

	/**
	 * builds an empty constraintnet,
	 * only populated with virtual nodes,
	 * only useful when virtual nodes are used
	 * only useful for incremental parsing
	 */
	public ConstraintNet(GrammarEvaluator evaluator, Lexicon lexicon, Configuration config) {
		this();
		
		if (executorService == null) {
			int numProcs = config.getInt(AbstractFrobber.NUM_THREADS);
			executorService = Executors.newFixedThreadPool(
					numProcs == 0? Runtime.getRuntime().availableProcessors() : numProcs,
					new DaemonThreadFactory());
		}
		
		this.id = "initial";
		
		this.grammar   = evaluator.getGrammar();
		this.evaluator = evaluator;
		this.lexicon   = lexicon;
		
		this.config = config;

		lexemeGraph = new LexemeGraph(lexicon, config);

		// gather levels
		noOfLevels     = grammar.getNoOfLevels();
		levelActive  = new HashMap<>();
		noOfActiveLevels = 0;
		for (Level level : grammar.getLevels()) {
			if (level.isUsed()) {
				noOfActiveLevels++;
				levelActive.put(level.getNo(), true);
			} else {
				levelActive.put(level.getNo(), false);
			}
		}
		
		buildNodes(true, evaluator);
		
		/* build table of lexeme nodes */
		lexemes = new HashMap<>(lexemeGraph.getMax());
		
		if(config.getFlag(USE_VIRTUAL_NODES)) 
			initVirtualNodes(grammar.getVNSpecs());
	}
	
	/**
	   re-sorts the LVs in each constraint node by limit rather than by score

	   This function sorts the Vectors  \b values=of all constraint nodes
	   of \b net using \b lvCompare().
	 */
	private void sortLVs(Comparator<LevelValue> comparator)
	{
		for (ConstraintNode cnode : nodes) {
			cnode.sortLVs(comparator);
		}
	}

	/**
	   Builds the constraint nodes of NET. If BUILDLVS is set, the nodes are
	   immediately filled with all possible LVs, otherwise they remain empty.

	   Returns FALSE if the constraintnet is invalid.

	   Basically it performs the following steps:

	   -  allocates the Vectors \b nodes and \b values
	   -  partitions the set of lexeme nodes created from each word
	      hypothesis according to each level by using \b lgPartitions()
	   -  allocates a \b ConstraintNode for each of the partitions <i>k</i>
	      and inserts it into the Vector \b nodes
	   -  checks whether the subordination <i>(k, l, m)</i> is possible for each
	      triples of modifier set, label and modifiee set, and constructs the
	      corresponding LV
	   -  checks whether the subordination <i>(k, l, \b NIL)</i> is possible for each
	      pair of modifier set and label, and constructs the corresponding LV
	   -  inserts all new LVs into the respective constraint nodes and the
	      Vector \b values
	   -  sorts the LVs in each constraint node by their limit.

	   However, some complications apply:

	   -  If a level has its \b useflag reset, it is ignored completely.
	   -  Some of the LVs created may be destroyed again by
	      \b cnUnaryPruning() if the variable
	      \b cnUnaryPruningFraction is smaller than~1. Such LVs are not
	      stored in the constraint node nor in the constraint net.
	   -  Each iteration executes the hook \b HOOK_CNBUILDNODES.
	   -  If \b cnSortNodes is set, \b cnSortNodes() is called
	      after building all nodes.
	   -  If \b scUseCache is set, a new cache is allocated for the
	      constraint net.
	   -  This function is interruptible by \b C-c in much the same
	      way as \b cnBuildEdges().
	 */
	private boolean buildNodes(boolean buildLVs, GrammarEvaluator evaluator) {
		
		int lgs = lexemeGraph.getNodes().size();
		int lvls = grammar.getNoOfLevels();

		nodes   = new ArrayList<>(lgs * lvls);
		values  = new ArrayList<>(lgs * lvls);

		for (GraphemeNode gn: lexemeGraph.getGNs()) {
			if (gn.getLexemes().isEmpty())
				continue;

			if (!buildIter(gn, buildLVs, evaluator))
				break;
		}
		
		return true;
	}
	
	/**
	 * Runs node.buildLevelValues. Callable since we want to use invokeAll()
	 * @author Arne Köhn
	 *
	 */
	private class BuildLevelCallable implements Callable<Object> {
		private Level lvl;
		private GraphemeNode gn;
		private GraphemeNode modifiee;
		private GrammarEvaluator evaluator;
		private ConstraintNode node;
		public BuildLevelCallable(ConstraintNode node, Level lvl, GraphemeNode gn, GraphemeNode modifiee, GrammarEvaluator evaluator) {
			this.node = node;
			this.lvl = lvl;
			this.gn = gn;
			this.modifiee = modifiee;
			this.evaluator = evaluator;
		}
		@Override
		public Object call() {
			node.buildLevelValues(lvl, gn, modifiee, evaluator);
			return null;
		}
		
	}
	
	/* ***********
	 * Methods   *
	 *************/

	/**
	  Builds the constraint node corresponding to GN in NET.

	  This function really performs most of the work that was documented
	  under cnBuildNodes() for simplicity.
	*/
	public boolean buildIter(GraphemeNode gn, boolean buildLVs, GrammarEvaluator evaluator) {
		return buildIter(gn, buildLVs, null, null, evaluator);
	}
	
	/**
	 * Like buildIter above, but lvs can be prevented from being build 
	 * for certain graphemenode and level combinations.
	 * To make such exceptions, set the flag in the bytevector according to the position in the nodes vector
	 * if freezecount is given, frozen LVs are built but frozen and counted (simulation mode)
	 * if freezecount == NULL, LVs are not built
	 */
	public boolean buildIter(GraphemeNode gn, boolean buildLVs, 
			List<Boolean> exceptions, List<Integer> freezeCount,
			GrammarEvaluator evaluator) {
		
		GraphemeNode modifiee;
		ConstraintNode node;
		
		int ggs = lexemeGraph.getGNs().size();
		//int lgs = lexemeGraph.getNodes().size();
		//int lvls = grammar.getNoOfLevels();

		if (gn.getLexemes().isEmpty())
			return true;

		
		List<BuildLevelCallable> jobs = new ArrayList<>();
		/* loop through all levels */
		for (Level lvl : grammar.getLevels()) {
			if (!lvl.isUsed())
				continue;

			/* if an entire grapheme is deleted from the graph already,
	           do not even build the constraint node. */
			// we have no state here!
			// and in a lg without parallel gns this can not happen anyway
//			if (lgAreDeletedNodes(lexemgraph, gn.lexemes)) {
//				continue;
//			}

			/* allocate new constraint node */
			node = new ConstraintNode(this, lvl, gn);
			
			/* add it to the net */
			insertConstraintNode(node);
			
			/* fill it */
			if(buildLVs) {
				for (int k = 0; k <= ggs; k++) {
					if(k == ggs) {
						modifiee = lexemeGraph.getNil();
					} else {
						modifiee = lexemeGraph.getGNs().get(k);
					}
					// one way...
					jobs.add(new BuildLevelCallable(node, lvl, gn, modifiee, evaluator));
					//... and the other
					jobs.add(new BuildLevelCallable(node, lvl, modifiee, gn, evaluator));
				}
				if (config.getFlag(USENONSPEC)) {
					jobs.add(new BuildLevelCallable(node, lvl, gn, lexemeGraph.getNonspec(), evaluator));
				}
			}
		}
		try {
			executorService.invokeAll(jobs);
		} catch (InterruptedException e) {
			throw new RuntimeException("Extending the net has been interrupted");
		}
		return true;
	}


	/**
	 * adds a constraintnode at the right position to the given constraintnet
	 * the right position is
	 * - in the end, if the node is virtual or no virtual nodes are used
	 * - before the virtual nodes otherwise
	 */
	private void insertConstraintNode(ConstraintNode node) {
		if(!config.getFlag(USE_VIRTUAL_NODES) || node.getGN().isVirtual()) {
			nodes.add(node);
		} else { // we need to add the node before the virtual ones
			int idx= lexemeGraph.getLastGN().getNo()* getNoOfLevels() + node.getLevel().getNo();
			nodes.add(idx, node);
		}
	}

	/**
	 * Build lexeme graph from the given lattice and add it to the net.
	 */
	private boolean tag(Lattice lat) {

		lexemeGraph = new LexemeGraph(lat, lexicon, config);

        logger.info(String.format("Grapheme graph: #nodes %d, min %d, max %d",
				lexemeGraph.getGNs().size(),
				lexemeGraph.getMin(), lexemeGraph.getMax()));
		logger.info(String.format(
				"Lexeme Graph: #nodes %d, min %d, max %d",
				lexemeGraph.getNodes().size(),
				lexemeGraph.getMin(), 
				lexemeGraph.getMax()));

		return true;
	}

	public String getId() {
		return id;
	}
	public LexemeGraph getLexemeGraph() {
		return lexemeGraph;
	}
	public List<LevelValue> getValues() {
		return values;
	}
	public List<ConstraintNode> getNodes() {
		return nodes;
	}

	public int getTotalNumberOfValues() {
		return values.size();
	}
	public long getEvalUnary() {
		return evalUnary;
	}
	public int getStatUnary() {
		return statUnary;
	}
	public long getEvalBinary() {
		return evalBinary;
	}
	public int getLvCountTotal() {
		return lvCountTotal.get();
	}
	public int getLvCountBuild() {
		return lvCountBuild;
	}
	public int getLvCountDeleted() {
		return lvCountDeleted.get();
	}
	public int getLvCountPostponed() {
		return lvCountPostponed.get();
	}
	public List<LevelValue> getPostponedLVs() {
		return new ArrayList<>(postponedLVs);
	}
	public int getNoOfLVs() {
		return values.size();
	}
	
	/** # slots not disabled by useflags */
	public int getNoOfActiveSlots() {
		return noOfActiveLevels * getNoOfTimepoints(); 
	}
	public int getNoOfSlots() {
		return getMaxTimepoint() * noOfLevels;
	}
	
	public int getMinTimepoint() {
		return lexemeGraph.getMin();
	}
	
	public int getMaxTimepoint() {
		return lexemeGraph.getMax();
	}
	public int getNoOfLexemes() {
		return lexemeGraph.getNodes().size();
	}
	public int getNoOfLevels() {
		return noOfLevels;
	}
	public boolean getLevelActive(int lvlNo) {
		return levelActive.get(lvlNo);
	}
	
	public Grammar getGrammar() {
		return grammar;
	}
	
	/** maximal ambiguity per timepoint */
	public int getLgWidth() {
		return lexemeGraph.getWidth();
	}
	
	public List<LexemeNode> getLexemesAt(int tp) {
		return lexemes.get(tp);
	}
	
	/**
	 * Return TRUE if LV would bind slot WHICH if it were selected.
	 */
	public boolean lvBindsSlot(LevelValue lv, int where) {
		/* must bind same level */
		if (lv.getLevel().getNo() != where % noOfLevels) {
			return false;
		}

		/* must cover timespan */
		if ( lv.getDependentGN().getArc().from > where / noOfLevels) {
			return false;
		}

		if (lv.getDependentGN().getArc().to <= where / noOfLevels) {
			return false;
		}
		
		return true;
	}

	/**
	 * Add an LV to the value list, 
	 * to the regarding constraint node  
	 * and give it an indexWRTNet 
	 * 
	 * @param newLv must not belong to a net yet
	 */
	public synchronized void addLv(LevelValue newLv) {
		assert newLv.getIndexWRTNet() == -1;
		
		newLv.addToNet(this, values.size());
		nodes.get(newLv.getIndex()).getValues().add(newLv);
		values.add(newLv);
		
		lvCountTotal.incrementAndGet();
		lvCountBuild++;	
	}
	
	/**
	 * call this whenever building a lv but not adding it to the net
	 * purely for statistics
	 */
	public void noteLVDeletedUponbuilding() {
		lvCountTotal.incrementAndGet();
		lvCountDeleted.incrementAndGet();
	}
	
	/**
	 * if a net is not valid, something went wrong with building or modifying it
	 * some callers might still want to do saomething with it, 
	 * so we throw no exception and instead set this flag to false  
	 * 
	 * @return
	 */
	public boolean isValid() {
		return valid;
	}
	
	/** # of time slots to be bound */
	public int getNoOfTimepoints() {
		return getMaxTimepoint()-getMinTimepoint();
	}
	
	public int getNoOfActiveLevels() {
		return noOfActiveLevels;
	}
	
	/** 
	 * the index of the constraint node belonging to 
	 * this gn-level combination 
	 * 
	 * @param gn
	 * @param lvl
	 * @return
	 */
	public int slotIndexOf(GraphemeNode gn, Level lvl) {
		return gn.getArc().from * noOfLevels + lvl.getNo();
	}
	
	/**
	 * find the levelvalue matching the given specifications, 
	 * or null, if no such LV exists in the net 
	 */
	public LevelValue findExistingLV(Level level, 
			GraphemeNode dep, LexemeNode lexDep,
			String label,
			GraphemeNode reg, LexemeNode lexReg) {
		
		// don't even bother with constradictory input
		if ( reg.spec() && !lexemeGraph.mayModify(dep, reg)) {
			return null;
		}

		/* otherwise, inspect each LV in the domain closely */
		for (LevelValue model : nodes.get(slotIndexOf(dep, level)).getValues()) {
			if ( ! model.getLabel().equals(label) ) {
				continue;
			}
			if (model.getDependentGN().getArc().from
					!= dep.getArc().from
					|| model.getDependentGN().getArc().to
					!= dep.getArc().to) {
				continue;
			}
			if (model.getRegentGN().spec() && reg.spec()) {
				if (    model.getRegentGN().getArc().from != reg.getArc().from || 
						model.getRegentGN().getArc().to   != reg.getArc().to) {
					continue;
				}
			} else {
				if (reg != model.getRegentGN()) {
					continue;
				}
			}
			if (    ! model.getDependents().contains(lexDep) ||
					! model.getRegents().contains(lexReg)) {
				continue;
			}
			return model;
		}
		return null;
	}
	
	/**
	 * find all levelvalues matching the given specifications 
	 */
	public List<LevelValue> findExistingLVs(Level level, 
			GraphemeNode dep, String label, GraphemeNode reg) {
		List<LevelValue> lvs = new ArrayList<>();
		
		// don't even bother with constradictory input
		if ( reg.spec() && !lexemeGraph.mayModify(dep, reg)) {
			return lvs;
		}

		/* otherwise, inspect each LV in the domain closely */
		for (LevelValue model : nodes.get(slotIndexOf(dep, level)).getValues()) {
			if ( ! model.getLabel().equals(label) ) {
				continue;
			}
			if (model.getDependentGN().getArc().from
					!= dep.getArc().from
					|| model.getDependentGN().getArc().to
					!= dep.getArc().to) {
				continue;
			}
			if (model.getRegentGN().spec() && reg.spec()) {
				if (    model.getRegentGN().getArc().from != reg.getArc().from || 
						model.getRegentGN().getArc().to   != reg.getArc().to) {
					continue;
				}
			} else {
				if (reg != model.getRegentGN()) {
					continue;
				}
			}
			lvs.add(model);
		}
		return lvs;
	}
	
	/**
	 * returns the levelvalue of the given index, if exists,
	 * if not, null is returned
	 */
	public LevelValue getLvById(int indexWRTNet) {
		if(indexWRTNet < 0 || indexWRTNet >= values.size())
			return null;
		return values.get(indexWRTNet);
	}

	/**
	 * The constraintnet is extended by the given arcs
	 *	- extends the lexemgraph
	 *	- builds new levelvalues for the new domain
	 *	- build  new levelvalues for the old domains leading to the new timeslot
	 */
	public void extend(String word, boolean isFinalStep) {
		logger.info(String.format("extending constraintnet  %s", id));
		
		int oldlgmax = lexemeGraph.getMax(); 

		// Timer timer = timerNew();

		lexemeGraph.extend(word, isFinalStep);
		this.id = String.format("net%d", lexemeGraph.getLastGN().getArc().to);

		if(! (lexemeGraph.getMax() > oldlgmax) ) {
			throw new RuntimeException("In extendNet, lexemgraph could not be extended!");
		}
		
		// call the predictors. Note that the order is important.
		Tagger.getTagger(config).predict(lexemeGraph);
		// Maltparser depends on the results from the tagger predictor
		MaltPredictor.getMaltPredictor(config).predict(lexemeGraph);
		

		GraphemeNode gn = lexemeGraph.getLastGN();
		Stopwatch stopwatch;
		if (performTimingMeasurements) {
			stopwatch = Stopwatch.createStarted();
		}

		buildIter(gn, true, evaluator);

		if (performTimingMeasurements) {
			logger.info(String.format("Timing: buildIter(+reverse) took %d ms", stopwatch.elapsed(TimeUnit.MILLISECONDS)));
			stopwatch.reset();
		}
//		if(step) {
//			incrBuildLvsTowardsNewNode(net,gn,step.domainFrozen,step.frozenLvCount);
//		} else {
//			incrBuildLvsTowardsNewNode(net,gn,NULL,NULL);
//		}

		//step.profiling = proMeasureTime(timer,step.profiling,"  other LVs");
		
		reevalFluctuation();
		
		sortLVs(new LvComparatorByScore());
		// push back virtual lexemes, if present
		for(int i= lexemes.keySet().size(); i> gn.getArc().from; i--) {
			lexemes.put(i, lexemes.get(i-1));
		}
		
		lexemes.put(gn.getArc().from, new ArrayList<>());
		for (LexemeNode ln : gn.getLexemes()) {
			for (int j = ln.getArc().from; j < ln.getArc().to; j++) {
				lexemes.get(j).add(ln);
			}
		}
	}

	/**
	 * Cleanup for after extending the net before frobbing it
	 *
	 * - check postponed LVs
	 * - check fluctuating scores of LVs
	 */
	private void reevalFluctuation() {
		// old LevelValues need to have some of their constraints rechecked:
		for(LevelValue lv : values) {
			reEvalFluctuatingConstraints(lv);
		}
		
		// look at all postponed LVs for if they should be reactivated
		if(config.getFlag(USE_EDGEREACTIVATION)) {
			Iterator<LevelValue> it = postponedLVs.iterator();
			while(it.hasNext()) {
				LevelValue lv = it.next();
				GrammarEvaluationResult ger = evaluator.evalUnary(lv, lexemeGraph, null, false, false);
				if(ger.getScore() > 0.0) {
					addLv(lv);
					it.remove();
				}
					
			}
			//proMeasureTime(timer, prof, "  reeval outside");		
		}
	}
	
	/**
	 * calculates the index without room for deactivated levels,
	 * nodes is indexed like this
	 */
	private int gaplessIndex(Level lvl, int pos) {
		int lvlNo= lvl.getNo();

		// count number of active levels
		int activeLevels= 0;
		
		for(Level lvl2 : grammar.getLevels()) {
			if(lvl2.isUsed()) {
				activeLevels++;
			}
		}

		// decrease ther number of the level for each inactive level before  the specified one
		int correctedLvlNo= lvlNo;
		int i;
		for(i=0; i<lvlNo; i++) {
			if(!(grammar.getLevels().get(i).isUsed())) {
				correctedLvlNo--;
			}
		}

		return pos * activeLevels + correctedLvlNo;
	}
	
	/**
	 * returns the constraint node for the given level and timpoint
	 */
	public ConstraintNode getCNode(Level lvl, int pos) {
		return nodes.get(gaplessIndex(lvl, pos));
	}
	
	/**
	 * returns the constraint node for the given slot
	 */
	public ConstraintNode getCNode(int slot) {
		return nodes.get(slot);
	}
	
	/**
	 * reevaluate certain constraints for the lv
	 */
	private void reEvalFluctuatingConstraints(LevelValue lv) {
		// first remove fluctuating penalty
		lv.removeFluctuatingconstraints();
		
		// then reevaluate all according constraints
		for(Constraint c : grammar.getHashedFlUCs(lv.getLevel(), lv.getDirection())) {
			ConstraintEvaluationResult cer = c.eval(lexemeGraph, null, lv);
			if(cer.isViolated()) {
				lv.addViolatedConstraint(c);
				lv.setScore(lv.getScore() * cer.getScore());
			}
		}
		
		// for predictive regents there might be some more constraints to reevaluate
		if(lv.getRegentGN().isNonspec() || lv.getRegentGN().isVirtual()) {
			for(Constraint c : grammar.getHashedFlUCsNonspec(lv.getLevel(), lv.getDirection())) {
				ConstraintEvaluationResult cer = c.eval(lexemeGraph, null, lv);
				if(cer.isViolated()) {
					lv.addViolatedConstraint(c);
					lv.setScore(lv.getScore() * cer.getScore());
				}
			}
		}
		//if(significantlyGreater(oldScore,lv.score) || significantlyGreater(lv.score, oldScore))
		//	cdgPrintf(CDG_INFO,"INFO: a lv's score changed due to fluctuating penalties");
	}

	public void postponeLV(LevelValue lv) {
		postponedLVs.add(lv);
		lvCountPostponed.incrementAndGet();
	}
	
	public GrammarEvaluator getEvaluator() {
		return evaluator;
	}
	
	/**
	 * adds all virtual node specifications as constraint nodes into the net
	 *
	 * exceptions says whether lvs should be added for each domain
	 * freezecount counts lvs not being build because of exceptions
	 */
	public void initVirtualNodes(List<VirtualNodeSpec> specs) {

		if(! (lexemeGraph instanceof VirtualLexemeGraph) ) {
			lexemeGraph = new VirtualLexemeGraph(lexemeGraph);
		}
		
		for(VirtualNodeSpec vns : specs) {
			for(int i=0; i< vns.getNoOfCopies(); i++) {
				addVirtualNode(vns, i);
			}
		}
		
		for(GraphemeNode gn : lexemeGraph.getGNs()) {
			if(gn.isVirtual())
				lexemes.put(gn.getArc().from, new ArrayList<>(gn.getLexemes()));
		}
	}
	
	public int getIndexOf(GraphemeNode gn, Level lvl) {
		return gn.getNo() * noOfLevels + lvl.getNo();
	}
	
	/**
	 * adds a constraint node based on a virtual grapheme node based on the given spec to the constraintnet
	 */
	private void addVirtualNode(VirtualNodeSpec spec, int n) {
		if (lexemeGraph instanceof VirtualLexemeGraph) {
			VirtualLexemeGraph virtLG = (VirtualLexemeGraph) lexemeGraph;
			
			GraphemeNode vgn = virtLG.addVirtualNode(spec, n);
			
			buildIter(vgn, true, evaluator);
			
		} else {
			throw new RuntimeException("Can't add a virtual node to a non virtual lexemegraph");
		}
	}

	/**
	 * returns list with all levelvalues compatible with the given specs
	 */
	public List<LevelValue> getAccordingLVs(int lvlNo, int dep, String label, int reg) {
		List<LevelValue> ret = new ArrayList<>();

		for (LevelValue lv : values) {
			if(
					!lv.getLabel().equals(label)   
					|| lvlNo != lv.getLevel().getNo() 
					|| reg != lv.getRegentGN().getNo() 
					|| dep != lv.getDependentGN().getNo() )
			{
				// not one of the lvs we search for
				continue;
			}
			ret.add(lv);
		}
		return ret;
	}

	/**
	 * returns the first level
	 * 
	 * TODO allow other levels to be marked as main level
	 * @return
	 */
	public Level getMainLevel() {
		return grammar.getLevels().get(0);
	}
}
