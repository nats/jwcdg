package de.unihamburg.informatik.nats.jwcdg.gui;

import de.unihamburg.informatik.nats.jwcdg.incrementality.IncrementalFrobber;
import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;
import de.unihamburg.informatik.nats.jwcdg.parse.Word;
import io.gitlab.nats.deptreeviz.DepTree;
import io.gitlab.nats.deptreeviz.DepTreeBaseInteractor;
import org.apache.batik.dom.events.DOMMouseEvent;

/**
 * The subcomponent of the DepTreeDisplayInteractor that handles interactions
 * with the DepTree
 * 
 * @author Sven Zimmer, Arne Köhn
 * 
 */
public class DepTreeInteractor extends DepTreeBaseInteractor<DecoratedParse, Word> {

	private IncrementalFrobber _parser;
	public DepTreeInteractor() {
	}

	// public class DTRefreshEvent extends java.util.EventObject{
	// private static final long serialVersionUID = 1L;
	// public DTRefreshEvent(Object source) {
	// super(source);
	// }
	// }

	// TODO make independent of SYN and REF

	public DecoratedParse getDecParse() {
		return _dt.getDecParse();
	}

	public void setDecParse(DecoratedParse decParse) {
		_dt.setDecParse(decParse);
	}
	/**
	 * Used during the initalisation
	 *
	 * @param dt
	 * @param parser
	 * @param dParse
	 */
	public void setFields(DepTree<DecoratedParse, Word> dt,
						  IncrementalFrobber parser,
						  DecoratedParse dParse) {
		_dt = dt;
		_dt.setDecParse(dParse);
		_parser = parser;
	}

	protected class CDGChangeTextListener extends ChangeTextListener {
		public void act(DOMMouseEvent devt) {
			super.act(devt);
			// right mouse click on label sets it to its optimal choice
			if (devt.getButton() == 2 && _level.equals("SYN")) {
				_dt.getDecParse().optimizeLabel(_node.getIndex(), _level);
				redraw(_node, _level, true);
			}
		}
	}

	@Override
	protected DTMouseEventListener getChangeTextListener() {
		return new CDGChangeTextListener();
	}

	public IncrementalFrobber getParser() {return _parser;}
	public void setParser(IncrementalFrobber parser) {_parser = parser;}
}
