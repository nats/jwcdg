package de.unihamburg.informatik.nats.jwcdg.constraints.functions;

import java.util.ArrayList;
import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.GraphemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.TermString;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * Returns predictions attached by Graphemenode.setPredictionFor().
 */
public class FunctionPredict extends Function {

	public FunctionPredict(Grammar g) {
		super("predict", g, 3, -1, false);
	}

	@Override
	public AVM eval(List<Term> args, Context context, LexemeGraph lg,
			LevelValue[] binding) {
		
		GraphemeNode gn;
		AVM first = args.get(0).eval(lg, context, binding);
		
		if (first instanceof AVMLexemNode) {
			gn= ((AVMLexemNode) first).getLexemeNode().getGN();
			// description = ln.getLexeme().getDescription(); // TODO needed for PPC predictor?
		} else if(first instanceof AVMNumber){
			int gi = ((AVMNumber) first).getInteger() - 1;
			gn = lg.getGNs().get(gi);
		} else {
			return new AVMError("WARNING: argument 1 of function `predict' neither an id nor a number");
		}
		
		if (! (args.get(1) instanceof TermString)) {
			return new AVMError("WARNING: argument 2 of function `predict' not a string");
		}

		String predictorname = ((TermString)args.get(1)).getString(); 
		
		ArrayList<String> keys = new ArrayList<>();

		for(Term t: args.subList(2, args.size())) {
			AVM v = t.eval(lg, context, binding);
			
			if (v instanceof AVMError) 
				return new AVMError("WARNING: error in evaluation of argument of function `predict': " + v.toString());
			
			keys.add(v.toString());
		}

		AVM a;
		
		a = gn.readPrediction(predictorname, keys);

		if(a == null) {
			return new AVMNumber(1);
		} else {
			return a;
		}
	}

}
