
package de.unihamburg.informatik.nats.jwcdg.constraints;

import java.util.ArrayList;

import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Hierarchy;

public class AccessContext {

	private ArrayList<ArrayList<Boolean>> context;
	private Grammar grammar;
	
	public AccessContext(Grammar grammar) {
		this.context = new ArrayList<>();
		this.grammar = grammar; 
	}
	
	/** copy a Context */
	public AccessContext (AccessContext orig) {
		this(orig.grammar);
		for(ArrayList<Boolean> temp : orig.context) {
			context.add(new ArrayList<>(temp));
		}
	}

	/**
	 * Adds a variable to the context. Note that this has to be called in the
	 * correct order (first variable of the constraint first etc.)
	 * @param init
	 */
	public void addVariable(ArrayList<Boolean> init) {
		context.add(init);
	}
	
	/** invert all bools in all lists */
	public void invert() {
		for(ArrayList<Boolean> l : context) {
			for(int i = 0; i < l.size(); i++) {
				l.set(i, !l.get(i));
			}
		}
	}

	/** apply && to all bools in all lists of this and B and write it to this */
	public void and(AccessContext b) {
		for(int key=0; key<context.size(); key++) {
			ArrayList<Boolean> la =   context.get(key);
			ArrayList<Boolean> lb = b.context.get(key);
			for(int i = 0; i< la.size(); i++) {
				la.set(i, la.get(i) && lb.get(i));
			}
		}
	}

	/** apply || to all bools in all lists of this and B and write it to this */
	public void or(AccessContext b) {
		for(int key=0; key<context.size(); key++) {
			ArrayList<Boolean> la =   context.get(key);
			ArrayList<Boolean> lb = b.context.get(key);
			for(int i = 0; i< la.size(); i++) {
				la.set(i, la.get(i) || lb.get(i));
			}
		}
	}

	/** unmark all edgetypes that do not have one of these labels */
	public void restrictLabels(int var, Hierarchy h, Level level, String node) {
		for(String label : level.getLabels()) {
			if(!h.subsumes(node, label)) {
				context.get(var).set(grammar.etEncode(level, label, Direction.Left) , false);
				context.get(var).set(grammar.etEncode(level, label, Direction.Right), false);
				context.get(var).set(grammar.etEncode(level, label, Direction.Nil)  , false);
			}
		}
	}

	/** unmark all edgetypes that do not have this level */
	public void restrictLevel(int var, Level level) {
		for(int i = 0; i < grammar.etMax(); i++) {
			if(grammar.etDecodeLevel(i) != level) {
				context.get(var).set(i, false); 
			}
		}
	}

	/** unmark all edgetypes that have this level */
	public void restrictNotLevel(int var, Level level) {
		for(int i = 0; i < grammar.etMax(); i++) {
			if(grammar.etDecodeLevel(i) == level) {
				context.get(var).set(i, false);
			}
		}
	}

	/** unmark all edgetypes that do not have this label */
	public void restrictLabel(int var, String label) {
		int i;
		for(i = 0; i < grammar.etMax(); i++) {
			if(!label.equals(grammar.etDecodeLabel(i))) {
				context.get(var).set(i, false);
			}
		}
	}

	/** unmark all edgetypes that have this label */
	public void restrictNotLabel(int var, String label) {
		int i;
		for(i = 0; i < grammar.etMax(); i++) {
			if(label.equals(grammar.etDecodeLabel(i))) {
				context.get(var).set(i, false);
			}
		}
	}

	/** unmark all edgetypes that do not have this direction */
	public void restrictDir(int var, Direction d) {
		if(d == Direction.AnyDir)
			return;
		
		for(int i = 0; i < grammar.etMax(); i++) {
			if(!d.subsumes(grammar.etDecodeDirection(i))) {
				context.get(var).set(i, false);
			}
		}
	}

	public ArrayList<Boolean> get(int var) {
		return context.get(var);
	}
}
