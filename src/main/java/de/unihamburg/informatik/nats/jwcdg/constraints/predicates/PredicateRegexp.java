/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.TermString;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * Matches against a given Regexp
 * @author the CDG-Team
 */
public class PredicateRegexp extends Predicate {

	public PredicateRegexp(Grammar g) {
		super("=~", g, false, 2, true);
	}

	@Override
	public boolean eval(List<Term> args, LevelValue[] binding, LexemeGraph lg, 
			Context context, int formulaID) {
		assert(args.size() == 2);
		assert(args.get(1) instanceof TermString);
		assert(args.get(0).eval(lg, context, binding) instanceof AVMString);
		String regex = args.get(1).toString();
		String input = args.get(0).eval(lg, context, binding).toString();
		return java.util.regex.Pattern.matches(regex, input);
	}
	
}
