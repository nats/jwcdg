package de.unihamburg.informatik.nats.jwcdg.avms;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import de.unihamburg.informatik.nats.jwcdg.input.Configuration;

/**
 * a flat version of an avm
 * only possible for a known finite set of paths like the one in grammar 
 * 
 * @author the CDG-Team
 *
 */
public class AVMFlat extends AVM {
	private static final long serialVersionUID = 6246998791536136442L;
	
	private Map<Path, AVM> map;
	
	/**
	 * generates a cached copy of the given avm where every possible feature lookup
	 * (as defined by the list of paths given as allAttributes)
	 * is stored in a map instead of the recursive AVM structure 
	 * 
	 * @param orig
	 * @param allAttributes
	 */
	public AVMFlat( AVM orig, List<Path> allAttributes ) {
		map= new HashMap<>();
		
		for(Path path : allAttributes) {
			AVM value = orig.followPath(path) ;
			map.put(path, value);
		}
	}
	
	/**
	 * @param map the map is NOT copied
	 */
	public AVMFlat(Map<Path, AVM> map) {
		this.map = map;
	}
	
	private AVMFlat() {
		map = new HashMap<>();
	}
	
	/**
	 * Create a new AVMFlat that differs in p by value
	 * This is needed so we can expand Disjunctions
	 * @return the modified copy
	 */
	public AVMFlat copyWithChangedValue(Path p, AVM value) {
		@SuppressWarnings("unchecked")
		Map<Path, AVM> newMap = (Map<Path, AVM>) ((HashMap<Path, AVM>)map).clone();
		newMap.put(p, value);
		return new AVMFlat(newMap);
	}
	
	@SuppressWarnings("unchecked")
	public HashMap<Path, AVM> getMapCopy() {
		return (HashMap<Path, AVM>) ((HashMap<Path, AVM>)map).clone();
	}
	
	@Override
	public AVM followPathImpl(Path path) {
		if(!map.containsKey(path)) {
			return new AVMError("ERROR: unknown feature path [" + path + "]!");
		}
		
		return map.get(path);
	}
	
	@Override
	public String toString() {
		String ret= "[";
		boolean first = true;
		for(Path k : map.keySet()) {
			AVM val = map.get(k);
			if (val instanceof AVMError) {
				continue;
			}
			if(first)
				first = false;
			else
				ret+= ", ";
			ret+= k.toString()+":"+val.toString();
		}
		return ret+"]";
	}
	
	@Override
	public AVM substituteRefs(Matcher matcher, Configuration config) {
		AVMFlat ret = new AVMFlat();
		for(Path key : map.keySet()) {
			ret.map.put(key, map.get(key).substituteRefs(matcher, config));
		}
		return ret;
	}
	
	// with this definition an avmflat can only be equal to other flatened avms, 
	// not to the avm it resultet from

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((map == null) ? 0 : map.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AVMFlat))
			return false;
		AVMFlat other = (AVMFlat) obj;
		if (map == null) {
			if (other.map != null)
				return false;
		} else if (!map.equals(other.map))
			return false;
		return true;
	}
}
