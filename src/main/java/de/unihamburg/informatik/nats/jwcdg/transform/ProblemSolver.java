package de.unihamburg.informatik.nats.jwcdg.transform;

import de.unihamburg.informatik.nats.jwcdg.ParseObserver;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;


public interface ProblemSolver {

	boolean solve();

	Analysis getBest();

	Parse getSolution();
	
	void interrupt();
	
	void registerObserver(ParseObserver obs);
	
	void setTimeLimit(int timeLimit);
}
