package de.unihamburg.informatik.nats.jwcdg.constraints.terms;


import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * A term evaluating to a fixed number
 * 
 * @author the CDG-Team
 */
public class TermNumber extends Term {
	private AVMNumber number;
	
	public TermNumber(double number) {
		this.number = new AVMNumber(number);
	}

	@Override
	public AVM eval(LexemeGraph lg, Context context,
			LevelValue[] binding) {
		return number;
	}

	@Override
	public String toString() {
		return number.toString();
	}
	
	public double getNumber() {
		return number.getNumber();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof TermNumber))
			return false;
		TermNumber other = (TermNumber) obj;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		return true;
	}
}
