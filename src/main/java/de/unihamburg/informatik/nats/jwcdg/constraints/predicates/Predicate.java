package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

public abstract class Predicate {
	
	protected final Grammar grammar;
//	String functor;
	public final boolean isContextSensitive;
	
	public final String name;
	public final int minArity;
	public final int maxArity;
	
	private boolean infix; // just for printing
	
	public Predicate(String name, Grammar g, boolean cs, int arity, boolean infix) {
		this(name, g, cs, arity, arity, infix);
	}
	
	/**
	 * @param cs
	 * @param minArity
	 * @param maxArity max arity of -1 means there is no limit
	 * @param doc
	 */
	public Predicate(String name, Grammar g, boolean cs, int minArity, int maxArity, boolean infix) {
		this.name     = name;
		this.isContextSensitive = cs;
		this.minArity = minArity;
		this.maxArity = maxArity;
		this.grammar  = g;
		this.infix    = infix;
	}
	
	public abstract boolean eval(List<Term> args, LevelValue[] binding, 
			LexemeGraph lg, Context context, int formulaID);

	public boolean isInfix() {
		return infix;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
