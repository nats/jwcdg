package de.unihamburg.informatik.nats.jwcdg.input;

import de.unihamburg.informatik.nats.jwcdg.ForgettingANTLRInputStream;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarLexer;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarParser;
import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMConj;
import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.UnbufferedTokenStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.*;

/**
 * Compares two lexicon files, outputs differences between files in terms of entries only found
 * in one of the files.
 *
 * Note: Only checks whether each lexicon entry (word/token/pattern and its features) is in the
 * other file and vice versa. Does not detect multiple entries or differences in formatting or
 * order
 *
 * @author Christine Köhn
 */
public class LexiconComparator {
	private static Logger logger = LogManager.getLogger();


	/**
	 * Compares lexicons
	 * @param args LEXICON_PATH1 LEXICON_PATH2 [ENCODING1] [ENCODING2] (ENCODING defaults to
	 *                iso-8859-15
	 */
	public static void main (String args[]) throws IOException, RecognitionException {
		if (args.length < 2) {
			logger.fatal("Provide 2 lexicon files as arguments and optionally encoding " +
					"(defaults to iso-8859-15): LEXICON_PATH1 LEXICON_PATH2 [ENCODING1] " +
					"[ENCODING2]");
			System.exit(1);
		}
		String defaultEncoding = "iso-8859-15";
		String encoding1 = args.length >= 3 ? args[2] : defaultEncoding;
		String encoding2 = args.length >= 4 ? args[3] : defaultEncoding;

		logger.info("Opening lexicon 1");
		Map<String,List<LexiconItem>> lexiconMap1 = readLexicon(args[0], encoding1);
		logger.info("Opening lexicon 2");
		Map<String,List<LexiconItem>> lexiconMap2 = readLexicon(args[1], encoding2);
		List<LexiconItem> only1 = getItemsNotInOther(lexiconMap1, lexiconMap2);
		List<LexiconItem> only2 = getItemsNotInOther(lexiconMap2, lexiconMap1);
		if (only1.size() > 0 || only2.size() > 0) {
			logger.info(String.format("%d items only in lexicon 1 (%s):", only1.size(), args[0]));
			logger.info(only1.toString());
			logger.info(String.format("%d items only in lexicon 2 (%s):", only2.size(), args[1]));
			logger.info(only2.toString());
		}
		else {
			logger.info("Lexicons do not differ (with respect to entries and their features)");
		}
	}

	private static Map<String, List<LexiconItem>> readLexicon (String path, String encoding)
			throws FileNotFoundException, UnsupportedEncodingException, RecognitionException {
		File lexFile = new File(path);
		CharStream cs = new ForgettingANTLRInputStream(new InputStreamReader(new FileInputStream
				(path), encoding));
		jwcdggrammarLexer lexer = new jwcdggrammarLexer(cs);
		UnbufferedTokenStream ts = new UnbufferedTokenStream(lexer);
		jwcdggrammarParser p = new jwcdggrammarParser(ts);
		logger.info(String.format("Attempting to parse %s", lexFile.getName()));
		Map<String, List<LexiconItem>> lexiconMap = new HashMap<>();
		p.lexicon(lexiconMap);
		return lexiconMap;
	}

	/**
	 * Tries to find an identical (with respect to its features) entry in lexiconMap2 for every
	 * entry in lexiconMap1. Every LexiconItem without a match will be returned.
	 * @param lexiconMap1
	 * @param lexiconMap2
	 * @return all items from lexiconMap1 for which no correspondig item can be found in lexiconMap2
	 */
	private static List<LexiconItem> getItemsNotInOther(Map<String, List<LexiconItem>> lexiconMap1,
														Map<String, List<LexiconItem>> lexiconMap2) {
		List<LexiconItem> only1 = new ArrayList<>();
		for (String entry : lexiconMap1.keySet()) {
			List<LexiconItem> matching1 = lexiconMap1.get(entry);
			List<LexiconItem> matching2 = lexiconMap2.get(entry);
			if (matching2 == null) {
				only1.addAll(matching1);
				continue;
			}
			for (LexiconItem li1 : matching1) {
				boolean foundExactMatch = false;
				Set<String> allFeatures1 = new HashSet<>();
				AVMConj avmConj1 = (AVMConj) li1.getValues();
				List<Path> paths1 = avmConj1.possiblePaths();
				for (LexiconItem li2 : matching2) {
					AVMConj avmConj2 = (AVMConj) li1.getValues();
					List<Path> paths2 = avmConj2.possiblePaths();
					boolean identicalSoFar = true;
					//Everything from entry li1 in entry li2?
					for (Path p : paths1) {
						AVM value1 = avmConj1.followPath(p);
						AVM value2 = avmConj2.followPath(p);
						if (!value1.equals(value2)) {
							identicalSoFar = false;
							break;
						}
					}
					if (!identicalSoFar) {
						continue;
					}
					//Everything from entry li2 in entry li1?
					for (Path p : paths2) {
						AVM value1 = avmConj1.followPath(p);
						AVM value2 = avmConj2.followPath(p);
						if (!value2.equals(value1)) {
							identicalSoFar = false;
							break;
						}
					}
					if (identicalSoFar) {
						foundExactMatch = true;
					}
				}
				if (!foundExactMatch) {
					only1.add(li1);
				}
			}
		}
		return only1;
	}

	public LexiconComparator() {
	}
}
