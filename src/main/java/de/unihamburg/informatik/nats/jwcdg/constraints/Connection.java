package de.unihamburg.informatik.nats.jwcdg.constraints;

/** How are two edges connected? */
public enum Connection {
	/** edges X and Y share Top and Bottom (==)*/
	Identical(0, "=="), 
	/** edge X is the inversion of edge Y (~=)*/
	Inverse  (1, "~="), 
	/** edge X is vertically under edge Y (\\)*/
	Under    (2, "\\"), 
	/** edge X is vertically over edge Y (/)*/
	Over     (3, "/"), 
	/** at the top (/\\)*/
	Top      (4, "/\\"), 
	/** at the bottom (\\/)*/
	Bottom   (5, "\\/"), 
	/** not at all (||)*/
	None     (6, "||"), 
	/** any of the above (,)*/
	Any      (7, ","); 
	
	public final int no;
	public final String form;
	
	Connection(int i, String form) {
		this.no   = i;
		this.form = form;
	}

	/**
	 * return the inverse of a Connection
	 */
	public Connection inverse()
	{
		if (this == Over)
			return (Under);
		else if (this == Under)
			return (Over);
		else
			return this;
	}
	
	/**
	 * Does this Connection subsume Connection b?
	 */
	public boolean subsumes(Connection other)
	{
		if (this == other)
			return true;
		else if (this == Any)
			return true;
		else if (other == Identical && (this == Top || this == Bottom))
			return true;
		else if (other == Inverse && (this == Over || this == Under))
			return true;
		else
			return false;

	}
	
}
