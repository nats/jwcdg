package de.unihamburg.informatik.nats.jwcdg.transform;

import java.io.Serializable;

/** -------------------------------------------------------------------------
 * Generalized score for comparing any two structures.
 *
 * This structure counts both the total number of constraint violations
 * (called `conflicts' henceforth) and the number of hard conflicts (those
 * with score 0.0). The field soft is the product of the penalties of all
 * soft (non-zero) conflicts. Note that this structure allows different
 * criteria for comparing two analyses; the function bCompare() is provided
 * as one method, but others could be devised.
 */
public class Badness implements Comparable<Badness>, Serializable {
	
    private static final long serialVersionUID = 1L;
    /** number of conflicts */
	public final int no;
	/** number of hard conflicts */
	public final int hard;
	/** combined effect of soft conflicts */
	public final double soft;
	
	public Badness(int no, int hard, double soft) {
		this.no = no;
		this.hard = hard;
		this.soft = soft;
	}
	
	
	/** ----------------------------------------------------------------------
	 Return new Badness better than any other.

	 The result is owned by the caller.
	 */
	public static Badness bestBadness() {
		return new Badness(0, 0, 1.0);
	}

	/** ----------------------------------------------------------------------
	 Return new Badness worse than any other.

	 Strictly speaking, this is not the worst Badness that any structure
	 could ever have, but I guess it will do.
	 */
	public static Badness worstBadness() {
		return new Badness(Integer.MAX_VALUE, Integer.MAX_VALUE, 0.0);
	}
	
	
	/** ----------------------------------------------------------------------
	 Compare Numbers with some margin for rounding error.

	 Upon removing conflicts and introducing new ones, the score of a
	 structure must frequently be multiplied and divided with constants.
	 Repeated use of this practice may introduce rounding errors.
	 Therefore we use a special function for comparison with some margin
	 for error. The problem of distinguishing rounding errors from very
	 subtle penalties remains unsolved.
	 */
	public static boolean significantlyGreater(double d1, double d2) {
		if (d2 == 0.0) {
			return d1 > 0.0;
		}
		
		return (d1 / d2 > 1.00001);
	}

	/** ----------------------------------------------------------------------
	 Add score to a Badness.

	 This function adds a classical score to a generalized score, i.e. it
	 either increments b.hard or multiplies b.soft by score.

	 */
	public Badness add(double score) {
		if (score == 0.0) {
			return new Badness(no+1, hard+1, soft);
		} else {
			return new Badness(no+1, hard, soft*score);
		}
	}

	/** ----------------------------------------------------------------------
	 Subtract score from a Badness.
	 */
	public Badness subtract(double  score) {
		if (score == 0.0) {
			return new Badness(no-1, hard-1, soft);
		} else {
			return new Badness(no-1, hard, soft/score);
		}
	}

	/** ----------------------------------------------------------------------
	 Add Badness b to a.
	 */
	public Badness addBadness(Badness b) {
		return new Badness(no+b.no, hard+b.hard, soft*b.soft);
	}

	/** ----------------------------------------------------------------------
	 Subtract Badness b from a.
	 */
	public Badness subtractBadness(Badness b) {
		return new Badness(no-b.no, hard-b.hard, soft/b.soft);
	}

	/** ----------------------------------------------------------------------
	 Print a badness in canonical form.
	 */
	public String toString() {
		if (no == 0) {
			return "*** no conflicts! ***";
		}

		return String.format("(%d/%1.5f)", hard, soft);
	}

	/**
	 * returns the score of this badness,
	 * needed, as soft is not always zero if hard >0
	 */
	public double getScore() {
		return hard==0 ? soft : 0.0;
	}

	/** ----------------------------------------------------------------------
	 Compare two Badnesses.

	 Returns TRUE if this is properly better than other, FALSE if they are equal.
	 */
	public boolean compare( Badness other ) {
		return compareTo(other) < 0 ;
	}

	@Override
	public int compareTo(Badness other) {
		
		/* fewer hard conflicts? */
		if (this.hard < other.hard)
			return -1;

		/* more hard conflicts? */
		if (this.hard > other.hard)
			return 1;

		/* better score? */
		if (significantlyGreater(this.soft, other.soft))
			return -1;

		/* worse score? */
		if (significantlyGreater(other.soft, this.soft))
			return 1;

		/* fewer conflicts? */
		if (this.no < other.no) {
			return -1;
		}
		
		/* more conflicts? */
		if (this.no > other.no) {
			return 1;
		}

		return 0;
	}
}
