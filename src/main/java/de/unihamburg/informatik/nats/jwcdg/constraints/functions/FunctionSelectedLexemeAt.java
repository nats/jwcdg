package de.unihamburg.informatik.nats.jwcdg.constraints.functions;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

public class FunctionSelectedLexemeAt extends Function {

	public FunctionSelectedLexemeAt(Grammar g) {
		super("selectedLexemeAt", g, 1, 1, true);
	}

	@Override
	public AVM eval(List<Term> args, Context context, LexemeGraph lg,
			LevelValue[] binding) {
		Term t = args.get(0);
		int pos;
		LexemeNode ln;
		
		AVM v = t.eval(lg, context, binding);
		if (v instanceof AVMNumber) {
			pos= ((AVMNumber) v).getInteger() -1;
		} else {
			return new AVMError("WARNING: type mismatch (1st arg) for `selectedLexemeAt'");
		}
		
		if(pos==-1) {
			ln= lg.getNil(    ).getLexemes().get(0); 
		} else if(pos==-2 || pos == lg.getMax()) {
			// TODO the position of nonspec is defined in two different ways, this should probably be handled 
			// differently, but this should work for now
			ln= lg.getNonspec().getLexemes().get(0);
		} else {
			int index= pos*grammar.getNoOfLevels()+grammar.getMainLevel().getNo();
			ln = context.getLV(index).getDependents().get(0);
		}
		
		return new AVMLexemNode(ln);
	}
}
