package de.unihamburg.informatik.nats.jwcdg.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;

public class NavNodeContent {
	public String _fileName;
	public String _directory;
	public DecoratedParse _parse;
	public boolean _isChanged = false;
	public Map<Integer, ArrayList<String>> _markedNodes =
            new HashMap<>();

	public NavNodeContent() {
	}

	public NavNodeContent(
				DecoratedParse parse,
				String directory,
				String fileName,
				boolean isChanged) {
		_fileName = fileName;
		_parse = parse;
		_isChanged = isChanged;
		_directory = directory;
	}

	public NavNodeContent(DecoratedParse parse, boolean isChanged) {
		this(parse, null, "", isChanged);
	}

	public NavNodeContent(
				DecoratedParse parse,
				String directory,
				String fileName,
				boolean isChanged,
				Map<Integer, ArrayList<String>> markedNodes) {
		this(parse, directory, fileName, isChanged);
		_markedNodes = markedNodes;
	}

	public NavNodeContent(
				DecoratedParse parse,
				boolean isChanged,
				Map<Integer, ArrayList<String>> markedNodes) {
		this(parse, null, "", isChanged, markedNodes);
	}
}
