package de.unihamburg.informatik.nats.jwcdg.incrementality;

import java.util.ArrayList;
import java.util.List;

/**
 * a generalized input increment, 
 * may contain several words 
 * 
 * @author Niels Beuck
 */
public class ChunkIncrement {
	/** the untokenized chunk */
	private String string;
	
	/** list of tokens in string*/
	private List<String> words;

	/**
	 * chunk of length 1
	 * @param token
	 */
	public ChunkIncrement(String token) {
		this.string = token;
		this.words = new ArrayList<>(1);
		words.add(token);
	}
	
	public ChunkIncrement(String chunk, List<String> tokens) {
		this.string = chunk;
		this.words  = tokens;
	}
	
	public String getString() {
		return string;
	}

	public List<String> getWords() {
		return words;
	}
	
	public int length() {
		return words.size();
	}

}
