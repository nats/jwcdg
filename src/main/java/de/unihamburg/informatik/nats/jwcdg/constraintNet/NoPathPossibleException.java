package de.unihamburg.informatik.nats.jwcdg.constraintNet;

public class NoPathPossibleException extends Exception {

	private static final long serialVersionUID = 6611248603531816444L;
	
	public NoPathPossibleException(String message) {
		super(message);
	}

}
