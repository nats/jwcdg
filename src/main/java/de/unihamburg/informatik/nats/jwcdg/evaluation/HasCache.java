package de.unihamburg.informatik.nats.jwcdg.evaluation;

/**
 * Caching of evaluation result of the context sensitive has predicate
 * 
 * At the moment, only the results of identical has() applications are
 * cached because those are expected to be particularly expensive. In
 * principle, this method could be extended to arbitrary identical
 * subformulas.
 *
 * The result of the application of has() number X at time point T
 * is stored in cell X * lock_width + T. 2 means TRUE, 1 means FALSE, and 0
 * `unknown'.
 * @author the CDG-Team
 *
 */
public class HasCache {
	public static final int 
		UNKNOWN = 0,
		FALSE   = 1,
		TRUE    = 2;
	
	private int[] cache;
	private int width;
	
	public HasCache(int width, int noHas) {
		this.width = width;
		int length = width * noHas;
		cache = new int[length];
		for(int i =0; i < length; i++) {
			cache[i] = UNKNOWN;
		}
	}
	
	/**
	 * @param formulaIndex
	 * @param t a timepoint in the lg
	 * @return 0 for unknown, 1 for false, 2 for true
	 */
	public int lookup(int formulaIndex, int t) {
		return cache[formulaIndex * width + t];
	}

	public void put(int formulaIndex, int t, boolean result) {
		cache[formulaIndex* width +t] = result?TRUE:FALSE;
	}
	
}
