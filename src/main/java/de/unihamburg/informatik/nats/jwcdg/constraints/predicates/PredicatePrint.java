package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Prints string or number argument and returns TRUE
 * 
 * @author the CDG-Team
 */
public class PredicatePrint extends Predicate {

	private static Logger logger = LogManager.getLogger(PredicatePrint.class);
	
	public PredicatePrint(Grammar g) {
		super("print", g, false, 1, -1, false);
	}

	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {
		StringBuilder sb = new StringBuilder();
		for (Term term : args) {
			sb.append(term.eval(lg, context, binding).toString());
		}

		logger.info("print(): " + sb.toString());

		return true;
	}
}
