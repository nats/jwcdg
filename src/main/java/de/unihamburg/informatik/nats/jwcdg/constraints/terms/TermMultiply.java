package de.unihamburg.informatik.nats.jwcdg.constraints.terms;

public class TermMultiply extends TermArithmethic {

	public TermMultiply(Term a, Term b) {
		super(a, b);
	}
	
	@Override
	protected double calculate(double d1, double d2) {
		return d1 * d2;
	}
	
	@Override
	protected String sign() {
		return "*";
	}

}
