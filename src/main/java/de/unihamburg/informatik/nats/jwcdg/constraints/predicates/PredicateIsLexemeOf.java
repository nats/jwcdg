package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMGraphemNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * Checks whether a lexeme is the lexeme of a given grapheme
 * @author Arne Köhn
 *
 */
public class PredicateIsLexemeOf extends Predicate {

	public PredicateIsLexemeOf(Grammar g) {
		super("isLexemeOf", g, false, 2, false);
	}

	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {
		AVM val1 = args.get(0).eval(lg, context, binding);
		AVM val2 = args.get(1).eval(lg, context, binding);
		if (! (val1 instanceof AVMLexemNode))
			throw new PredicateEvaluationException("type mismatch (1st arg) for `isLexemeOf'");
		if (! (val2 instanceof AVMGraphemNode))
			throw new PredicateEvaluationException("type mismatch (2nd arg) for `isLexemeOf'");
		AVMLexemNode ln = (AVMLexemNode)val1;
		AVMGraphemNode gn = (AVMGraphemNode)val2;
		return gn.getGn().getLexemes().contains(ln.getLexemeNode());
	}

}
