/**
 * 
 */
package de.unihamburg.informatik.nats.jwcdg.predictors;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMGraphemNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.GraphemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.predictors.malt.IncrementalMaltParser;
import de.unihamburg.informatik.nats.jwcdg.predictors.malt.IncrementalSingleMalt;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.maltparser.core.config.ConfigurationDir;
import org.maltparser.core.exception.MaltChainedException;
import org.maltparser.core.io.dataformat.DataFormatInstance;
import org.maltparser.core.options.OptionManager;
import org.maltparser.core.symbol.SymbolTable;
import org.maltparser.core.symbol.SymbolTableHandler;
import org.maltparser.core.symbol.trie.TrieSymbolTableHandler;
import org.maltparser.core.syntaxgraph.DependencyGraph;
import org.maltparser.core.syntaxgraph.DependencyStructure;
import org.maltparser.core.syntaxgraph.edge.Edge;
import org.maltparser.core.syntaxgraph.node.Node;
import org.maltparser.parser.SingleMalt;

import com.google.common.base.Stopwatch;

/**
 * MaltPredictor uses the Maltparser as an external predictor. It takes the tags
 * from the Tagger predictor and therefore needs to be called after Tagger.
 * Since the tagger doesn't need to be monotonic, the MaltPredictor will always reparse
 * the whole increment and is stateless.
 * 
 * @author Arne Köhn
 */
public class MaltPredictor extends Predictor {
	
	protected static Logger logger = LogManager.getLogger(MaltPredictor.class);
	protected IncrementalMaltParser parser;
	protected DependencyStructure emptyds;
	protected boolean isInitialized;
	protected boolean doReanalysis;
	private final int maltLookahead;
	private static final boolean performTimingMeasurements = false;
	public static final String MALT_MODEL_DIR = "maltModelDir";
	public static final String MALT_MODEL_NAME ="maltModelName";
	public static final String MALT_MODEL_LOOKAHEAD = "maltModelLookahead";
	public static final String MALT_DO_REANALYSIS = "maltDoReanalysis";
	
	protected static Map<String,MaltPredictor> maltMap = new HashMap<>();
	
	/**
	 * Returns a MaltPredictor built using the given Configuration.
	 * Either one that already exists or one that is built just-in-time.
	 * @param conf
	 * @return
	 */
	public static MaltPredictor getMaltPredictor(Configuration conf) {
		String mdir =conf.getString(MALT_MODEL_DIR);
		String mname = conf.getString(MALT_MODEL_NAME);
		int lookahead = conf.getInt(MALT_MODEL_LOOKAHEAD);
		boolean doReanalysis = conf.getFlag(MALT_DO_REANALYSIS);
		String mkey = mdir+mname;
		if ( (mdir.equals("") || mname.equals("")) && !mkey.equals("") )
			logger.info("Maltparser turned off since not all settings are set");
		if (!maltMap.containsKey(mkey)) {
			MaltPredictor mp = new MaltPredictor(mdir, mname, lookahead, doReanalysis);
			maltMap.put(mkey, mp);
		}
		return maltMap.get(mkey);
	}
	
	protected MaltPredictor(String modeldir, String modelname, int lookahead, boolean doReanalysis) {
		this.maltLookahead = lookahead;
		if (modeldir.equals("") || modelname.equals(""))
			return;
		try {
			this.doReanalysis = doReanalysis;
			URL optionFile;
			optionFile = MaltPredictor.class.getResource("/malt_options.xml");
			OptionManager opts = OptionManager.instance();
			opts.loadOptionDescriptionFile(optionFile);
			opts.generateMaps();
			
			String mcoFile = modeldir+"/"+modelname+".mco";
			
			opts.overloadOptionValue(0, "system", "workingdir", modeldir);
			ConfigurationDir configDir = new ConfigurationDir(new URL("file://"+mcoFile));
			// TMP_STORAGE is used while parsing (see ConfigurationDir)
			SymbolTableHandler symbolTableHandler = new TrieSymbolTableHandler(TrieSymbolTableHandler.ADD_NEW_TO_TMP_STORAGE);

			URL symboltablesURL = new URL("jar:file:" + mcoFile + "!/" + modelname + "/symboltables.sym");
			symbolTableHandler.load(new InputStreamReader(symboltablesURL.openStream()));
			SingleMalt singleMalt = new IncrementalSingleMalt();
			opts.loadOptions(0, configDir.getInputStreamReaderFromConfigFile("savedoptions.sop"));
			opts.generateMaps();
			configDir.initDataFormat();
			DataFormatInstance dataFormatInstance = configDir.getDataFormatManager().getInputDataFormatSpec().createDataFormatInstance(
					symbolTableHandler,
					OptionManager.instance().getOptionValueString(0, "singlemalt", "null_value")); 

			emptyds = new DependencyGraph(symbolTableHandler);

			singleMalt.initialize(0, dataFormatInstance, configDir, SingleMalt.PARSE);		

			parser = (IncrementalMaltParser) singleMalt.getAlgorithm();
			parser.initialize(emptyds);
			isInitialized = true;
		} catch (MalformedURLException e) {
			logger.warn("Can't initialize becausse of a MalformedURLException");
		} catch (MaltChainedException e) {
			logger.warn("Can't initialize becausse of a MaltChainedException");
		} catch (IOException e) {
			logger.warn("Can't initialize becausse of a IOException");
		}
	}
	
	
	public void predict(LexemeGraph lg) {
		Stopwatch stopwatch; 
		if (performTimingMeasurements) {
			stopwatch = Stopwatch.createStarted();
		}
		if (!isInitialized)
			return;
		try {
			int numAlreadyParsedTokens;
			Object storedParser = lg.getPredictorData("Maltparser");
			if (storedParser == null || doReanalysis) {
				parser.initialize(emptyds);
				numAlreadyParsedTokens = 0;
				if (doReanalysis) {
					lg.setPredictorData("Maltparser", parser);
				}
			} else {
				parser = (IncrementalMaltParser)storedParser;
				// parser.initialize((DependencyStructure)storedds);
				numAlreadyParsedTokens = parser.getParserState().getConfiguration().getDependencyGraph().getHighestTokenIndex();
				//numAlreadyParsedTokens = ((DependencyStructure)storedds).getHighestTokenIndex() + 1;
			}
			
			SymbolTable deprelTable = parser.getCurrentParserConfiguration().getDependencyGraph().getSymbolTables().getSymbolTable("DEPREL");
			
			int numWords = 0; // we need the number of "normal" words for later...
			for (GraphemeNode gn : lg.getGNs()) {
				if ( !gn.isSpecified() || gn.isVirtual() ) // ignore nonspec and virtual nodes
					continue;
				numWords += 1;
				if (numWords <= numAlreadyParsedTokens)
					continue;
				// get best tag TODO hardcoded POS
				Map<String, AVM> tags = gn.getPredictionFor("POS");
				if (tags == null) {
					// TODO this should probably be catched earlier (while constructing the predictor)
					throw new RuntimeException("The Malt predictor needs the Tagger predictor, however you seem to not have configured one!");
				}
				String bestTag = "NN";
				double bestScore = 0.0;
				for (Entry<String, AVM> e : tags.entrySet()) {
					double score = ((AVMNumber)e.getValue()).getNumber();
					if (score > bestScore) {
						bestScore = score;
						bestTag = e.getKey();
					}
				}
				parser.addWord(gn.getArc().word, bestTag);
			}
			
			
			// do one step for every word without tripping into the lookahead.
			for (int i = 0; i < numWords - maltLookahead - numAlreadyParsedTokens; i++) {
				parser.doStep();
			}
			
			if (lg.isComplete()) // Tell Maltparser that it can finish the parse
				parser.finalizeParse();
			for (Edge e: parser.getCurrentParserConfiguration().getDependencyGraph().getEdges()) {
				GraphemeNode regent = maltNodeToGraphemeNode(e.getSource(), lg);
				GraphemeNode dependant = maltNodeToGraphemeNode(e.getTarget(), lg);
				Map <String, AVM> predMap = new HashMap<>(4);
				predMap.put("regent", new AVMGraphemNode(regent));
				
				/* For some NIL edges, the label "ROOT" - which is not used by
				 * the german grammar - is hardcoded in Maltparser.
				 * To avoid spurious constraint violations, we don't predict them.
				 */
				try {
					String label = e.getLabelSymbol(deprelTable);
					if (!"ROOT".equals(label))
						predMap.put("mlabel", new AVMString(label));
				} catch (NullPointerException exception) {
					logger.warn(exception); // TODO ask someone who knows why this sometimes happens
				}
				dependant.setPredictionFor("malt", predMap);
			}
		} catch (MaltChainedException e1) {
			logger.warn("couldn't call Maltparser!");
			return;
		}
		if (performTimingMeasurements) {
			logger.info(String.format("Timing: Maltparser took %d ms", stopwatch.elapsed(TimeUnit.MILLISECONDS)));
			stopwatch.reset();
		}
	}
	
	/**
	 * Looks up to which GraphemeNode the Node n maps and wraps it in an AVM
	 * @param n the malt Node
	 * @param lg the (jwcdg) lexemegraph
	 * @return AVMGRaphemeNode gn
	 */
	protected GraphemeNode maltNodeToGraphemeNode(Node n, LexemeGraph lg) {
		if ( n.getIndex() == 0 )
			return lg.getNil();
		return lg.getGNs().get(n.getIndex()-1);
	}
}
