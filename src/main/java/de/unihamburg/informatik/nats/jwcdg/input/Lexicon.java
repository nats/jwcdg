package de.unihamburg.informatik.nats.jwcdg.input;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import org.antlr.runtime.CharStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.UnbufferedTokenStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.collections.StoredMap;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.DatabaseNotFoundException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;

import de.unihamburg.informatik.nats.jwcdg.ForgettingANTLRInputStream;
import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMFlat;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.predictors.Tagger;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarLexer;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarParser;

public class Lexicon implements Serializable{
	
    private static final long serialVersionUID = 1L;
    public static final String DEDUCE_COMPOUNDS         = "lexiconDeduceCompounds";
	public static final String IGNORE_PARENS            = "lexiconIgnoreParens";
	public static final String COMPOUND_CATS            = "compoundCats";
	public static final String SPONTANEOUS_UPPCASE_CATS = "spontaneousUppcaseCats";
	public static final String TEMPLATE_MODE            = "lexiconTemplateMode";
	
	private Map<String, List<LexiconItem>> entries;                       /* hashtable of lists of lexical entries */
	private static Logger logger = LogManager.getLogger(Lexicon.class);
	private List<Path> allAttributes;
	
	/**
	 * this constructor build the map of lexicon entries itself
	 * from the given list, using lexiconItem.getWord() as key
	 * 
	 * @param items
	 */
	public Lexicon(List<LexiconItem> items, List<Path> allAttributes) {
		entries = new HashMap<>();
		for(LexiconItem li : items) {
			String word = li.getWord();
			if(!entries.containsKey(word)) {
				entries.put(word, new ArrayList<>());
			}
			entries.get(word).addAll(li.expandDisjunctionsAndFlatten(allAttributes)); // TODO ungetestet
		}
		this.allAttributes = allAttributes;
	}
	
	/**
	 * uses the given map directly 
	 * 
	 * @param items
	 */
	public Lexicon(Map<String, List<LexiconItem>> entries, List<Path> allAttributes) {
		this.entries = entries;
		this.allAttributes = allAttributes;
	}
	
	
	/** 
	 * parses a lexicon from a file into a berkeleydb and returns a lexicon
	 * which uses that database as data source.
	 * If an up-to-date database is present, skips the parsing.
	 * 
	 * Use this function if your lexicon is large i.e. not just a dummy since
	 * it will save a lot of time and only parts of the lexicon need to be
	 * in memory.
	 * 
	 * @param filename
	 * @param encoding
	 * @return
	 * @throws IOException 
	 * @throws UnsupportedEncodingException 
	 * @throws RecognitionException 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Lexicon fromFile(File file,
	                               String encoding, List<Path> allAttributes)
	                               throws IOException,
	                                      RecognitionException {
		/*
		 * 1. open database in read-only-mode
		 * 2. check if the db is up-to-date
		 *     if so -> return lexicon using that db
		 * 3. re-open db in write mode and re-parse the file
		 * 4. close db and recursively call fromFile
		 * 
		 * This way we open the db read-only whenever possible which allows for
		 * multiple instances of our parser
		 * 
		 * note that execution still fails if several processes try to
		 * re-read the lexicon
		 */
		
		logger.info("Lexicon: Opening database environment");
		// use a directory that is in the directory which contains
		// the file to be read as db directory
		String cDir = file.getAbsoluteFile().getParentFile().getCanonicalPath()
				+File.separator+"lexicon_cache";
		File envHome = new File(cDir);
		if (! envHome.exists()) {
			if (! envHome.mkdir()) {
				logger.error("can't create folder "+envHome.toString());
				System.exit(1);
			}
		}
		if (!envHome.isDirectory()) {
			logger.error(envHome.toString()+" is a regular file, please delete it.");
			System.exit(1);
		}
		
		// initialize read-only and writable configurations
		EnvironmentConfig envConfig = new EnvironmentConfig();
		envConfig.setTransactional(true);
		envConfig.setReadOnly(true);
		envConfig.setAllowCreate(true);
		envConfig.setCacheSize(1024*1024*50); //TODO db cachesize
		
		DatabaseConfig dbConfig = new DatabaseConfig();
		dbConfig.setTransactional(false);
		dbConfig.setKeyPrefixing(true);
		dbConfig.setReadOnly(true);
		
		EnvironmentConfig envCWriteable = envConfig.clone();
		envCWriteable.setReadOnly(false);
		
		DatabaseConfig dbConfigWritable = dbConfig.clone();
		dbConfigWritable.setReadOnly(false);
		dbConfigWritable.setAllowCreate(true);
		dbConfigWritable.setDeferredWrite(true);
		
		Environment env = new Environment(envHome, envConfig);
		
		String lexiconDBName = "DB_" + file.getName();
		
		// see when the file has changed
		long lastModified = file.lastModified();
		byte[] checksum = ByteBuffer.allocate(8).putLong(lastModified).array();
		
		// check whether the db needs to be updated
		boolean db_uptodate = false;
		byte[] oldchecksum = null;
		
		Database metaDb = null;
		DatabaseEntry theKey = new DatabaseEntry(lexiconDBName.getBytes("UTF-8"));
		DatabaseEntry theData = new DatabaseEntry();
		
		try {
			metaDb = env.openDatabase(null, "META_DB", dbConfig);
			metaDb.get(null, theKey, theData, null);
			oldchecksum = theData.getData();
			metaDb.close();
			if (java.util.Arrays.equals(checksum, oldchecksum)) {
				logger.info("lexicon db is up to date");
				db_uptodate = true;
			}
		} catch (DatabaseNotFoundException e) {
			/* 
			 * No db -> db_uptodate stays false
			 * remember: writable configs create the db automatically
			 */
		}
		if (!db_uptodate) {
			// Try to reopen the database in write mode to update it
			env.close();
			env = new Environment(envHome, envCWriteable);
			dbConfig = dbConfigWritable;
			metaDb = env.openDatabase(null, "META_DB", dbConfigWritable);
			// delete the old version if there was one 
			if (oldchecksum != null) {
				// the fastest way to delete all entries
				try {
					env.removeDatabase(null, lexiconDBName);
				} catch (DatabaseNotFoundException e) {
					// No db -> nothing to do
				}
				env.sync();
			}
		}
		
		Database lexiconDb = env.openDatabase(null, lexiconDBName, dbConfig);
		StoredClassCatalog foocatalog = new StoredClassCatalog(lexiconDb);
		EntryBinding<String> lexiconKeyBinding = new SerialBinding<>(foocatalog, String.class);
		EntryBinding<List<LexiconItem>> lexiconValueBinding = 
				new SerialBinding(foocatalog, ArrayList.class); // TODO how to say ArrayList<LexiconItem>.class 
		StoredMap<String, List<LexiconItem>> lexiconMap =
				new StoredMap<>(lexiconDb, lexiconKeyBinding, lexiconValueBinding, true);
		
		if (!db_uptodate){
			logger.info("(re)building lexicon db...");
			CharStream cs = new ForgettingANTLRInputStream(new InputStreamReader(new FileInputStream(file), encoding));
			jwcdggrammarLexer lexer = new jwcdggrammarLexer(cs);
			UnbufferedTokenStream ts = new UnbufferedTokenStream(lexer);
			jwcdggrammarParser p = new jwcdggrammarParser(ts);
			logger.info("attempting to read "+ file.getName());
			p.lexicon(lexiconMap);
			
			theData.setData(checksum);
			metaDb.put(null, theKey, theData);
			metaDb.sync();
			// close the environment again and try again
			metaDb.close();
			lexiconDb.sync();
			lexiconDb.close();
			env.close();
			logger.info("done!");
			return fromFile(file, encoding, allAttributes);
		}
		
		return new Lexicon(lexiconMap, allAttributes);
	}
	/**
	 * returns whether there is at least one entry for the given word
	 * does not consider whether an entry could be generated via templates etc.
	 * 
	 * @param word
	 * @return
	 */
	public boolean query(String word) {
		return entries.containsKey(word);
	}
	
	/**
	   Search current lexicon for matching words.

	   This function returns a List of LexiconItem structures that match WORD.
	   The simplest kind of match is if a known lexicon item has the exact
	   reading WORD.

	   Near-matches of various kinds are tried if no exact match is found:

	   If inputDeduceCompounds is set, a lexicon item may match if it is a
	   compound of WORD marked with a hyphen: a query for `Halb-Elf' will
	   succeed by finding `Elf'.

	   If inputUnmarkedCompounds is set, a lexicon item may match if it is any
	   compound of word: `Halbelf' will match on `Elf'. The word that is
	   actually in the lexicon must have the same casing as WORD, and it must
	   belong to the categories allowed by inputUnmarkedCompounds.

	   If inputIgnoreParens is set, a lexicon item may also match if it is
	   identical to WORD with all parentheses (where ()[] count as parentheses)
	   removed.

	   These three lookup methods are tried in order, so if there is an exact
	   match in the normal lexicon, no compounding will be tried.

	   If lexicon templates are loaded, any of them whose regex matches WORD
	   may be returned. This behaviour is further controlled by the setting of
	   templateMode.

	*/
	public List<LexiconItem> get(String word, Configuration config) {
		
		List<LexiconItem> result;

		int templateMode = config.getInt(TEMPLATE_MODE);

		/* look for exact match */
		result = getExactly(word);
		
		/* Maybe this word is in spontaneous upper case.
	     Look up lower-case variants to check that. */
		if(!config.getList(SPONTANEOUS_UPPCASE_CATS).isEmpty()) {
			result.addAll(getBySpontaniousUppercase(word, config));
		}

		/* if still no entry was found, try to find entries for suffixes (marked or unmarked) */
		if(result.isEmpty() && config.getFlag(DEDUCE_COMPOUNDS)) {
			result= deduceCompound(word, config);
		}
		
		/* try to find `abbiegen' for `ab(biegen)' */
		if(result.isEmpty() && config.getFlag(IGNORE_PARENS) && 
				(word.contains("(") || word.contains(")") || word.contains("[") || word.contains("]")) ) {
			StringBuilder baseBuilder = new StringBuilder();
			int i = 0;
			int max = word.length();
			for(i = 0; i < max; i++) {
				switch(word.charAt(i)) {
				case '(':
				case ')':
				case '[':
				case ']':
					break;
				default:
					baseBuilder.append(word.charAt(i));
				}
			}
			String base = baseBuilder.toString();
			result = getExactly(base);
			logger.info(String.format("`%s'might be a parenthesized version of `%s'.", word, base));
		}

		/* templateMode determines when to look for matching templates. */
		/* never */
		if(templateMode == 0) {
			/* no issues with newly created items, we can return immediately */
		}

		/* by category */
		else if(templateMode == 1) {
			List<String> cats = new ArrayList<>();
			for(LexiconItem li : result) {
				AVM v = li.getFeature(taggerCat(config));
				if (v instanceof AVMString) {
					String cat = ((AVMString)v).getString();
					if(! cats.contains(cat))
						cats.add(cat);
				}
			}
			result.addAll(getByTemplate(word, cats, config));
		}

		/* if needed */
		else if(templateMode == 2) {
			if(result.isEmpty()) {
				result = getByTemplate(word, null, config);
			}
		}

		/* always */
		else if(templateMode == 3) {
			result.addAll(getByTemplate(word, null, config));
		}

		/* expand all disjunctions */
		List<LexiconItem> resultExpanded = new ArrayList<>();
		for (LexiconItem lexiconItem : result) {
			resultExpanded.addAll(lexiconItem.expandDisjunctionsAndFlatten(allAttributes));
		}
		result = resultExpanded;
		
		/* Items created from templates are brand new,
	     and have not received distinctive feature lists yet,
	     so we have to install them now. */
		for(LexiconItem li : result) {	
			li.setDescription(li.getWord());
		}
		
		resolvePartition(result, allAttributes);
		return result;
	}

	/**
	 * Search existing templates for items matching WORD.
	 *
	 * Only templates are considered, not normal lexicon items. All templates
	 * that match WORD are used to produce new LexiconItems which are then
	 * owned by the caller.
	 *
	 * Templates that have a category (as defined by taggerCategoryPath)
	 * listed in CATEGORIES are not used.
	 */
	private List<LexiconItem> getByTemplate(String word, List<String> forbiddenCats, Configuration config) {
		List<LexiconItem> result = new ArrayList<>();
		Path taggerCat = taggerCat(config);

		/* templates are stored in the lexicon under the key "" */
		for(LexiconItem template : entries.get("")) {
			
			/* check regex match */
			Matcher matcher = template.getPattern().matcher(word); 
			if(matcher.matches()) {
				LexiconItem instance = template.instantiatePattern(word, config);
				
				/* maybe throw it away again, if it does not have the right category */
				if(forbiddenCats != null && taggerCat != null) {
					AVM v = instance.getFeature(taggerCat);
					if ( (v instanceof AVMString) && forbiddenCats.contains(v.toString()) ) {
						continue;
					}
				}
				result.add(instance);
			}
		}

		return result;
	}
	
	/**
	 * looks up a word by splitting it at several positions and 
	 * and calling get() for the suffix
	 * 
	 * first try splitting at '-' and '/' characters
	 * then try unmarked suffixes
	 * 
	 * does not resolve partitions of the entries found
	 * 
	 * @param word
	 * @param config
	 * @return
	 */
	private List<LexiconItem> deduceCompound(String word, Configuration config) {
		List<LexiconItem> result = new ArrayList<>(1);
		
		/* try to find an entry for a marked suffix first */
		String suffix = "", prefix = "";
		int i= -1;
		if(       word.contains("-") && word.indexOf("-") < word.length()-1) {
			i = word.indexOf('-');
		} else if(word.contains("/") && word.indexOf("/") < word.length()-1) {
			i = word.indexOf('/');
		}
		if(i > 0) { 
			suffix = word.substring(i+1);
			prefix = word.substring(0,i);

			List<LexiconItem> meanresult = get(suffix, config);
			for(LexiconItem li : meanresult) {
				LexiconItem li2= new LexiconItem(word, li.getValues(), new SourceInfo("*compound deduction*", -1));
				result.add(li2);
			}
			
			if(!result.isEmpty() ) {
				logger.info(String.format(
						"Assuming `%s' is a compound of `%s'",
						word, suffix));
			}
			
			for(LexiconItem li : result) {
				li.setDescription(prefix + li.getDescription());
			}
		}
		
		/* try to find an entry for an unmarked suffix
		   if still no entry was found */
		if(result.isEmpty() && word.length() > 4) {
			boolean upper = false;
			if(Character.isUpperCase(word.charAt(0))) {
				upper = true;
			}
			for(i = 2; i < word.length()-3; i++) {
				suffix = word.substring(i);
				prefix = word.substring(0,i);
				List<LexiconItem> meanresult;
				if(upper) {
					suffix = toUpper(suffix);
				}

				meanresult = getExactly(suffix);
				if( ! meanresult.isEmpty() ) {
					for(LexiconItem li : meanresult) {

						/* check allowed categories */
						if(taggerCat(config) != null) {
							AVM v = li.getFeature(taggerCat(config));
							if ( (v instanceof AVMString) &&
									! config.getList(COMPOUND_CATS).contains(((AVMString)v).getString())) {
								continue;
							}
							
							LexiconItem li2= new LexiconItem(word, li.getValues(), new SourceInfo("*compound deduction*", -1));
							result.add(li2);
						}
					}
					
					if(!result.isEmpty() ) {
						logger.info(String.format(
								"Assuming `%s' is a compound of `%s'",
								word, suffix));
						break;
					}
				} 
			}
		}
		
		return result;
	}

	/**
	   Search the lexicon for an exact match with WORD.

	   This function checks if the exact form WORD is present in the lexicon;
	   if so, it returns all lexicon items for it. It does *not* consult
	   templates (see getByTemplate), conflate spurious upper/lower case
	   (see lexemegraph.lgNewIter), or do compound analysis (see deduceCompund),
	   or spontaneous upper case (see getBySpontaniousUppercase)
	   
	   does not resolves partitions
	*/
	public List<LexiconItem> getExactly(String word) {
		/* look up the form in the lexicon. */
		List<LexiconItem> result = entries.get(word);
		List<LexiconItem> expandedResults = new ArrayList<>();
		if (result == null) return new ArrayList<>();
		for (LexiconItem lexiconItem : result)
			expandedResults.addAll(lexiconItem.expandDisjunctionsAndFlatten(allAttributes));
		return expandedResults;
	}
	
	/**
	   If a category is listed in `capitalizable-cats', 
	   then the lower case form is allowed even
	   if WORD is in upper case. This allows processing of `die Deutsche Bahn'
	   without duplicating all entries for `deutsch'.

	   Do not confuse spontaneous upper case (caused by being part of a proper
	   name) with spurious upper case (caused by being the first word of a
	   sentence). The latter must be dealt with in context, which is why it's
	   done in lgNewIter(); but the former can happen to all words of the
	   appropriate category, so we can do it in the lexicon without context.
	   
	   does not resolves partitions
	 */
	private List<LexiconItem> getBySpontaniousUppercase(String word, Configuration config) {
		List<String> cats = config.getList(SPONTANEOUS_UPPCASE_CATS);
		String low = word.toLowerCase();
		List<LexiconItem> result = new ArrayList<>();
		
		if(taggerCat(config) != null && Character.isUpperCase(word.charAt(0))) {
			if(query(low)) {
				for(LexiconItem li : entries.get(low)) {
					AVM v = li.getFeature(taggerCat(config));
					if ( (v instanceof AVMString) && cats.contains(((AVMString)v).getString())) {
						/* If so, generate the uppercase version on the fly */
						String newWord =        Character.toUpperCase(li.getWord().charAt(0)) +
								li.getWord().substring(1);
						String newDescription = Character.toUpperCase(li.getDescription().charAt(0)) + 
								li.getDescription().substring(1);
						LexiconItem li2 = new LexiconItem(newWord, li.getValues(), new SourceInfo());
						li2.setDescription(newDescription);
						result.add(li2);
					}
				}
			}
		}
		
		return result;
	}
	
	/**
	 *  Pre-compute all possible feature accesses and update the
	 *  field value of each item in the lexicon.
	 */
	public void cacheLexicon() {
		/* pre-access all paths in each lexical item */
		for(List<LexiconItem> lis: entries.values()) {
			for(LexiconItem li : lis) {
				cacheLexiconItem(li);
			}
		}
	}
	
	/**
	 * flatten the AVM for one lexical item.
	 */
	private void cacheLexiconItem(LexiconItem li) {

		/* if no new attribute was introduced since we last computed the feature
	     table, we don't have to do anything. */
		if(li.getValues() instanceof AVMFlat && !allAttributes.isEmpty()) {
			return;
		}

		/* Otherwise we have to compute a new one. */
		li.cacheValues(allAttributes);
	}
	
	/**
	 * returns the path object for the configured TAGGER_CAT string
	 * 
	 * @param config
	 * @return
	 */
	private Path taggerCat(Configuration config) {
		String seq = config.getString(Tagger.TAGGER_POS_PATH);
		return  Path.fromSequence(seq);
	}
	
	/**
	 * Add all paths occuring in a lexicon item to the given list,
	 * if not already contained
	 * 
	 * 
	 * @param soFar
	 */
	public void collectFeatures(List<Path> soFar) {
		for(List<LexiconItem> lis : entries.values()) {
			for(LexiconItem li : lis) {
				List<Path> ps = li.getValues().possiblePaths();
				for(Path p: ps) {
					if(!soFar.contains(p))
						soFar.add(p);
				}
			}
		}
	}
	
	/**
	   Annotate lexical items with distinctive features.

	   ITEMS is a list of lexical items, presumably with the same lexical
	   reading, otherwise they wouldn't need disambiguation.

	   FEATURES is a list of paths (i.e., lists of strings) that may be used to
	   find differences between the lexical items. We do not use a BitString
	   here because the user can specify which features to use preferentially,
	   i.e., "name lexicon entries by their case rather than by their gender",
	   therefore we need to be able to represent an ordering between the
	   features.

	   When a difference is found, each item is renamed so that its name
	   reflects the value it has for that path.

	   If no difference at all can be found between two items, a warning is
	   given and inputRemoveDuplicates() is called to remove one of them.

	*/
	public void resolvePartition(List<LexiconItem> items, List<Path> features)
	{
		List<List<LexiconItem>> partitions, subPartitions;
		List<List<LexiconItem>> previousPartitions = new ArrayList<>();
		previousPartitions.add(items);
		
		for(Path p : features) {
			
			partitions = new ArrayList<>();
			
			for(List<LexiconItem> partition: previousPartitions) {
				
				/* disambiguation complete */
				if (partition.size() <= 1) {
					partitions.add(partition);
					continue;
				}
				
				subPartitions = new ArrayList<>();
				
				/* classify the items into partitions
			     according to their value for the first feature */
				for (LexiconItem li: partition) {
					boolean found = false;
					
					for (List<LexiconItem> eqClass: subPartitions) {
						LexiconItem li2 = eqClass.get(0); // it is guaranteed to have at least one element
		
						/* insert into correct class */
						if (! li.differByFeature(li2, p, false)) {
							eqClass.add(li);
							found = true;
							break;
						}
					}
		
					/* open new equivalence class */
					if (!found) {
						List<LexiconItem> eqClass = new ArrayList<>();
						eqClass.add(li);
						subPartitions.add(eqClass);
					}
				}
		
				/* if at least some items have different values,
			     note this attribute as a distinction */
				if(subPartitions.size() > 1) {
					for(LexiconItem li: partition) {
						li.noteDistinctiveFeature(p);
					}
				}
				
				partitions.addAll(subPartitions);
			}
			
			previousPartitions = partitions;
		}
	}
	
	@Override
	public String toString() {
		return String.format("Lexicon, words: %d, entries: %d", entries.keySet().size(), entries.values().size());
	}
	
	
	public void setAllAttributes(List<Path> allAttributes) {
		this.allAttributes = allAttributes;
	}
	
	private String toUpper(String s) {
		return Character.toUpperCase(s.charAt(0)) + s.substring(1);
	}
}
