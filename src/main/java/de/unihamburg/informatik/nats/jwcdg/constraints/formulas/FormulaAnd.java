package de.unihamburg.informatik.nats.jwcdg.constraints.formulas;

import java.util.ArrayList;
import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.AccessContext;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;


public class FormulaAnd extends Formula {
	private List<Formula> subformulas;

	public FormulaAnd() {
		this.subformulas = new ArrayList<>();
	}
	
	@Override
	public boolean evaluate(LevelValue[] binding,
			Context context, LexemeGraph lg) {
		for (Formula f : this.subformulas) {
			if (!f.evaluate(binding, context, lg)) {
				return false;
			}
		}
		return true;
	}

	public void addSubFormula(Formula f) {
		subformulas.add(f);
		this.isContextSensitive = this.isContextSensitive || f.isContextSensitive;
	}
	
	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		boolean first = true;
		for(Formula f: subformulas) {
			if(!first)
				ret.append("&");
			first=false; 
			ret.append(f.toString());
		}
		return ret.toString();
	}

	@Override
	public void analyze(AccessContext context, Grammar g) {
		/* conjunctions are evaluated left-to-right and on the
	   need-to-know principle. This means that any constraints imposed on
	   the constraint variables in the premise are still valid in the
	   conclusion. Therefore we can simply pass the current Context along,
	   allowing it to be modified by the subformula. */
		for (Formula f : subformulas) {
			f.analyze(context, g);
		}
	}

	@Override
	public void collectFeatures(List<Path> sofar) {
		for(Formula f: subformulas) {
			f.collectFeatures(sofar);
		}
	}
	
	@Override
	public boolean check() {
		for(Formula f : subformulas) {
			if(!f.check())
				return false;
		}
		return true;
	}
	
	@Override
	public void collectHasInvocations(List<Formula> akku) {
		for(Formula f : subformulas) {
			f.collectHasInvocations(akku);
		}
	}
}
