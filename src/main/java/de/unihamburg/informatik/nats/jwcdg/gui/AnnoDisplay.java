package de.unihamburg.informatik.nats.jwcdg.gui;

import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;
import de.unihamburg.informatik.nats.jwcdg.parse.Word;
import io.gitlab.nats.deptreeviz.DepTree;
import org.apache.batik.swing.JSVGCanvas;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The UI component of the AnnoViewer tool. Doesn't include the interactions.
 * 
 * @author Sven Zimmer and Christine Köhn and Arne Köhn
 * 
 */

public class AnnoDisplay extends AbstractDisplay {
	private static final long serialVersionUID = 1L;
	private ArrayList<JScrollPane> _nodesScrollPanes =
            new ArrayList<>();
	private List<JButton> _selectTreeButtons = new ArrayList<>();
	private List<JButton> _doneButtons = new ArrayList<>();
	private List<JButton> _notDoneButtons = new ArrayList<>();
	private List<JButton> _saveButtons = new ArrayList<>();
	private List<JSlider> _zoomSliders = new ArrayList<>();
	// public JPanel _constraintsPanel = new JPanel();
	private JPanel _donePanel = new JPanel();
	private JPanel _zoomPanel = new JPanel();
	// 0 means that all 3 _nodesScrollPanes are visible
	public int _visiblePane = 0;
	private int _dividerLocation = 0;

	public AnnoDisplay(List<DepTree<DecoratedParse, Word>> dts, Configuration config) {
		super(config);

		_prefD = new AnnoPreferencesDisplay();

		initButtons();
		// _saveCDAButton = new JButton("Save above CDAs");
		initElements(true);

		getFileMenu().add(getSaveAllItem());
		getFileMenu().insertSeparator(1);
		getFileMenu().add(getQuitItem());

		_panel2.add(_donePanel, BorderLayout.EAST);
		_panel2.add(_zoomPanel, BorderLayout.WEST);

		// _buttonPanel.add(_prevDoneButton);
		// _buttonPanel.add(_prevButton);
		// _buttonPanel.add(_currentButton);
		// _buttonPanel.add(_nextButton);
		// _buttonPanel.add(_nextDoneButton);
		// _buttonPanel.add(_saveCDAButton);
		_buttonPanel.add(getButton(LEVEL_BUTTON));

		initSelectTreeButtons(dts.size());
		disableSelectTreeButton();

		// int fontSize = dts.get(0).getFont().getSize();

		_depTreePanel.setLayout(new GridLayout(0, 1));
		_donePanel.setLayout(new GridLayout(0, 1));
		_zoomPanel.setLayout(new GridLayout(0, 1));

		// TODO fix bug: only scroll bars on resize, perhaps a problem with
		// batik
		//
		// scrollCanvas.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		// frame.firePropertyChange("WIDTH",frame.WIDTH,frame.WIDTH);
		// scrollCanvas.getVerticalScrollBar().setValueIsAdjusting(true);
		// System.out.println(scrollCanvas.getVerticalScrollBar().getValue());
		// canvas.setEnableZoomInteractor(true);
		// DOMTreeManager tm = g.getDOMTreeManager();

		int i = 0;
		for (@SuppressWarnings("unused")
		DepTree dt : dts) {
			_nodesScrollPanes.add(new JScrollPane());
			ImageIcon doneIcon = new ImageIcon("resources/done v04.png");
			JButton doneButton = new JButton(doneIcon);
			doneButton.setName(DONE_BUTTON + i);
			ImageIcon notDoneIcon = new ImageIcon("resources/not done v04.png");
			JButton notDoneButton = new JButton(notDoneIcon);
			notDoneButton.setName(NOT_DONE_BUTTON + i);
			ImageIcon saveIcon = new ImageIcon("resources/save v04.png");
			JButton saveButton = new JButton(saveIcon);
			saveButton.setName(SAVE_BUTTON + i);
			_doneButtons.add(doneButton);
			_notDoneButtons.add(notDoneButton);
			_saveButtons.add(saveButton);
			JSlider zoomSlider = new JSlider(JSlider.VERTICAL, - 10, 10, 0);
			_zoomSliders.add(zoomSlider);
			i++;
		}
		setSize(dts.get(0).getWindowSize());
	}

	private void initSelectTreeButtons(int buttonCount) {
		for (int i = 0; i <= buttonCount; i++) {
			String label = "Select all trees";
			if (i > 0) {
				label = "Select tree " + i;
			}
			JButton button = new JButton(label);
			button.setName(TREE_BUTTON + i);
			_buttonPanel.add(button);
			_selectTreeButtons.add(button);
		}
	}

	public void disableSelectTreeButton() {
		for (JButton button : _selectTreeButtons) {
			int i = _selectTreeButtons.indexOf(button);
			boolean isEnabled = true;
			if (_visiblePane == i) {
				isEnabled = false;
			}
			button.setEnabled(isEnabled);
		}
	}

	// public void setSaveCDAButton(JButton saveCDAButton) {
	// _saveCDAButton = saveCDAButton;
	// }
	//
	// public JButton getSaveCDAButton() {
	// return _saveCDAButton;
	// }

	// public void setLevelButton(JButton levelButton) {
	// _levelButton = levelButton;
	// }
	//
	// public JButton getLevelButton() {
	// return _levelButton;
	// }

	public void setNodesScrollPane(int index,
				List<DepTree<DecoratedParse, Word>> dts,
				ArrayList<Boolean> isDoneList,
				ArrayList<Boolean> isChangedList) {
		JSVGCanvas canvas = dts.get(index)
					.getNodesCanvas();
		JScrollPane scrollPane = new JScrollPane(canvas , 
					JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
					JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		canvas.setName("" + index);
//		System.out.println("-" + scrollPane.getName() + "-");
		_nodesScrollPanes.set(index, scrollPane);
		scrollPane.setName(NODES_PANE + index);
		showNodesScrollPanes(_visiblePane, dts, isDoneList, isChangedList);
	}

	public void showNodesScrollPanes(int visiblePane,
				List<DepTree<DecoratedParse, Word>> dts,
				ArrayList<Boolean> isDoneList,
				ArrayList<Boolean> isChangedList) {
		_visiblePane = visiblePane;
		_depTreePanel.removeAll();
		_donePanel.removeAll();
		_zoomPanel.removeAll();
		_vertSplitPanel.removeAll();
		_vertSplitPanel.add(_vertSplitPane, BorderLayout.CENTER);
		for (JScrollPane nodesScrollPane : _nodesScrollPanes) {
			int index = _nodesScrollPanes.indexOf(nodesScrollPane);
			DepTree dt = dts.get(index);
			int fontSize = dt.getFont().getSize();
			nodesScrollPane.getHorizontalScrollBar()
						.setUnitIncrement(fontSize * 3);
			nodesScrollPane.getVerticalScrollBar()
						.setUnitIncrement(fontSize * 3);
			nodesScrollPane.setSize(dt.getCanvasSize());
			getSaveAllItem().setEnabled(isChangedList.contains(true));
			int paneIndex = _nodesScrollPanes.indexOf(nodesScrollPane);
			if (_visiblePane == 0 || _visiblePane == paneIndex + 1) {
				_depTreePanel.add(nodesScrollPane);
				JButton doneButton = _doneButtons.get(paneIndex);
				_donePanel.add(doneButton);
				doneButton.setEnabled(! isDoneList.get(paneIndex));
				JButton notDoneButton = _notDoneButtons.get(paneIndex);
				_donePanel.add(notDoneButton);
				notDoneButton.setEnabled(isDoneList.get(paneIndex));
				JButton saveButton = _saveButtons.get(paneIndex);
				_donePanel.add(saveButton);
				saveButton.setEnabled(isChangedList.get(paneIndex));
				if (_visiblePane == 0) {
					_vertSplitPane.setRightComponent(null);
					_zoomPanel.add(_zoomSliders.get(paneIndex));
				}
				else {
					_vertSplitPane.setRightComponent(_constraintsPanel);
					_vertSplitPane.setDividerLocation(dt.getCanvasSizeY() + 20);
					_vertSplitPanel.add(_zoomSliders.get(paneIndex),
								BorderLayout.WEST);
				}
			}
		}
		validate();
		repaint();
	}

	protected class AnnoPreferencesDisplay extends PreferencesDisplay {
		private static final long serialVersionUID = 1L;

		@Override
		protected void addSpecialPrefs() {
		}
	}

	public void rememberDividerLocation() {
		_dividerLocation = _vertSplitPane.getDividerLocation();
	}

	public void resetDividerLocation() {
		_vertSplitPane.setDividerLocation(_dividerLocation);
	}

	@Override
	public JPopupMenu getPopup() {
		return _popup;
	}

	@Override
	public void setPopup(JPopupMenu popup) {
		_popup = popup;
	}

	public List<JButton> getSelectTreeButtons() {
		return _selectTreeButtons;
	}

	public void setSelectTreeButtons(List<JButton> selectTreeButtons) {
		_selectTreeButtons = selectTreeButtons;
	}

	// public JButton getPrevButton() {
	// return _prevButton;
	// }
	//
	// public void setPrevButton(JButton prevButton) {
	// _prevButton = prevButton;
	// }
	//
	// public JButton getCurrentButton() {
	// return _currentButton;
	// }
	//
	// public void setCurrentButton(JButton currentButton) {
	// _currentButton = currentButton;
	// }
	//
	// public JButton getNextButton() {
	// return _nextButton;
	// }
	//
	// public void setNextButton(JButton nextButton) {
	// _nextButton = nextButton;
	// }

	public List<JButton> getNotDoneButtons() {
		return _notDoneButtons;
	}

	public void setNotDoneButtons(List<JButton> notDoneButtons) {
		_notDoneButtons = notDoneButtons;
	}

	public List<JButton> getDoneButtons() {
		return _doneButtons;
	}

	public void setDoneButtons(List<JButton> doneButtons) {
		_doneButtons = doneButtons;
	}

	public List<JButton> getSaveButtons() {
		return _saveButtons;
	}

	public void setSaveButtons(List<JButton> saveButtons) {
		_saveButtons = saveButtons;
	}

	@Override
	public JTable getConstraintsTable() {
		return _constraintsTable;
	}

	@Override
	public void setConstraintsTable(JTable constraintsTable) {
		_constraintsTable = constraintsTable;
	}

	public List<JSlider> getZoomSliders() {
		return _zoomSliders;
	}

	public void setZoomSliders(List<JSlider> zoomSliders) {
		_zoomSliders = zoomSliders;
	}

}
