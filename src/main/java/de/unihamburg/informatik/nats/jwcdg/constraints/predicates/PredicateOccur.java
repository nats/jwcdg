package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 *  Does word #1 occur somewhere in the sentence?
 */
public class PredicateOccur extends Predicate {

	public PredicateOccur(Grammar g) {
		super("occurs", g, false, 1, false);
	}


	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {

		AVM val = args.get(0).eval(lg, context, binding);
		if (val instanceof AVMString) {
			AVMString sval = (AVMString) val;
			return lg.contains(sval.getString());
		} else {
			throw new PredicateEvaluationException("type mismatch (1st arg) for `occur'");
		}
	}

}
