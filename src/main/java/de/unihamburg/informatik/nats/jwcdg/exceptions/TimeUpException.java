/**
 * 
 */
package de.unihamburg.informatik.nats.jwcdg.exceptions;

/**
 * This Exception signals that code ran out of time.
 * It is thrown by code that notices the lack of time to return the control
 * flow to a method higher up in the stack.
 * @author Arne Köhn
 *
 */
public class TimeUpException extends Exception {
	private static final long serialVersionUID = -1437299408215180863L;

}
