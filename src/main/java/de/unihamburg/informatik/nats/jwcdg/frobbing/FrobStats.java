package de.unihamburg.informatik.nats.jwcdg.frobbing;

import java.util.ArrayList;
import java.util.List;

public class FrobStats {
	
	/* Statistics */
	private int judgeCounter;        /* # of calls to pJudge() so far */
	private int patchAttempts;       // iterations of the repair loop
	private int successfulPatches;   // iterations of the repair loop resulting in a new maximum
	private int steps;
	private List<RepairAttempt> repairAttempts; 	   // a list of all repair attempts for later analysis
	
	private long startTime;        /* time when frobbing started */
	private long firstSoftFound;   /* time when 1st sol. >0 was found */
	private long lastSolFound;     /* time when final result was found */
	
	
	public FrobStats() {
		judgeCounter      = 0;
		patchAttempts     = 0;
		successfulPatches = 0;
		steps             = 0;
		
		repairAttempts = new ArrayList<>();
		
		startTime      = System.currentTimeMillis(); 
		firstSoftFound = 0;
		lastSolFound   = 0;
	}
	
	public int getJudgeCounter() {
		return judgeCounter;
	}
	public int getPatchAttempts() {
		return patchAttempts;
	}
	public int getSuccessfulPatches() {
		return successfulPatches;
	}
	public int getSteps() {
		return steps;
	}
	public List<RepairAttempt> getRepairAttempts() {
		return repairAttempts;
	}
	public long getStartTime() {
		return startTime;
	}
	public long getFirstSoftFound() {
		return firstSoftFound;
	}
	public long getLastSolFound() {
		return lastSolFound;
	}
	
	public void incPatchAttempts() {
		patchAttempts++;
	}


	public void registerRepairAttempt(RepairAttempt currentAttempt, Boolean success) {
    	currentAttempt.setSuccessful(success);
    	currentAttempt.noteTime();
    	repairAttempts.add(currentAttempt);
    	
    	if(success) {
    		successfulPatches ++;
    	}
	}
	public void setStepcount(int n) {
		steps = n;
	}
	
	/**
	 * Will note the time since start
	 * and write ist to lastSolFound
	 * 
	 * if soft is set and no soft solution was registered before, 
	 * firstsoftfound will also be set
	 */
	public void solFound(boolean soft) {
		lastSolFound = startTime - System.currentTimeMillis();
		
		if (soft && firstSoftFound ==0) {
			firstSoftFound = lastSolFound;
		}
	}
	
	
	
	
}
