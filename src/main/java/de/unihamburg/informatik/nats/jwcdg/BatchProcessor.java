package de.unihamburg.informatik.nats.jwcdg;

import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.lang.reflect.Array;
import java.nio.file.FileAlreadyExistsException;
import java.text.Collator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;

/**
 * Parses a batch of sentences, outputs cda files to a folder and parser errors to log file
 *
 * @author Christine Köhn
 */
public class BatchProcessor {

	private File fileToAnalyze;
	private File analysesFolder;
	private Parser parser;
	private int mode;
	private static Logger logger = LogManager.getLogger();

	/**
	 *
	 * @param fileName input file path
	 * @param confFileName config file path (properties file)
	 * @param mode mode (see {@link BatchProcessor#main(String[])}  main})
	 * @throws IOException {@link FileAlreadyExistsException} if output folder exists or
	 * {@link IOException} if output folder cannot be created
	 */
	public BatchProcessor(String fileName, String confFileName, int mode) throws IOException {
		this.mode = mode;
		fileToAnalyze = new File(fileName);
		String date = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new java.util.Date());
		analysesFolder = new File(String.format("%s.analyses.%s", fileToAnalyze.getAbsolutePath(), date));
		if (analysesFolder.exists()) {
			throw new FileAlreadyExistsException(analysesFolder.getAbsolutePath(), null, "File " +
					"already exists. For every batch, the folder will be created anew.");
		}
		else {
			boolean success = analysesFolder.mkdir();
			if (!success) {
				throw new IOException(analysesFolder.getAbsoluteFile() + " could not be created.");
			}
		}
		parser = JWCDG.makeParser(confFileName);
	}

	/**
	 * @param args myconfig.properties INPUTFILE -mode MODE
	 *
	 * usage: Usage
	 *                -help         Usage: myconfig.properties INPUTFILE -mode MODE
	 *                -mode <arg>   select input mode:
	 *                1 means one token per line, multiple sentences per file,
	 *                automatic output numbering, output directory is fixed.
	 *                2 means one line per sentence. output directory is fixed.
	 *                3 means folder with one sentence per file, one token per
	 *                line.
	 */
	public static void main(String[] args) {
		Options options = new Options();
		Option optMode = Option.builder("mode")
				.hasArg()
				.desc("select input mode:\n" +
						"1 means one token per line, multiple sentences per file. Empty line => " +
						"new sentence. Tokens may not contain \\t: Everything after a \\t will be" +
						" ignored.\n"
						+ "2 means one line per sentence. Expected line format: ID\\tSentence. ID" +
						" will be used for file naming.\n"
						+ "3 means folder with one sentence per file, one token per line.\n" +
						"The output will be written in a folder INPUTFILE.analyses" +
						".yyyy-MM-dd_HH-mm-ss. One file per sentence.\n" +
						"File names depend on mode\n" +
						"1: Automatic numbering starting at 00.cda\n" +
						"2: ID.cda\n" +
						"3: original_filename.cda")
				.build();
		Option optHelp = Option.builder("help")
				.desc("Usage: myconfig.properties INPUTFILE -mode MODE\n" +
						"Output format is cda. If you need conll: Use convert-cda2conll.py " +
						"from https://gitlab.com/nats/toolbox/ for converting cda to conll " +
						"(CoNLL-X)\n")
				.build();
		options.addOption(optMode);
		options.addOption(optHelp);

		CommandLineParser argParser = new DefaultParser();
		CommandLine line = null;
		try {
			line = argParser.parse( options, args );
		}
		catch( ParseException e ) {
			logger.fatal("Could not parse args: %s", e);
			System.exit(1);
		}

		if (line.hasOption("help")) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "Usage", options );
			System.exit(0);
		}

		if (args.length < 3) {
			logger.fatal("Missing arguments. You have to specify a configuration file, an " +
					"input file and a mode.");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "Usage", options );
			System.exit(1);
		}

		if (!line.hasOption("mode")) {
			logger.fatal("You have to specify a mode.");
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "Usage", options );
			System.exit(1);
		}

		String modeStr = line.getOptionValue("mode");
		String modes = "123";
		if (!modes.contains(modeStr)) {
			logger.fatal(String.format("Invalid mode: %s", modeStr));
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "Usage", options );
			System.exit(1);
		}

		int mode = Integer.parseInt(modeStr);

		String confFileName = args[0];
		String fileName = args[1];
		BatchProcessor processor = null;
		try {
			processor = new BatchProcessor(fileName, confFileName, mode);
		} catch (IOException e) {
			logger.fatal(e);
			System.exit(1);
		}
		if (mode == 3) {
			processor.analyzeFolder();
		} else {
			try {
				processor.analyzeFile();
			} catch (IOException e) {
				logger.fatal(e);
			}
		}
	}

	private void analyzeFolder() {
		File[] files = fileToAnalyze.listFiles();
		Arrays.sort(files);
		if (files == null) {
			logger.warn(String.format("Directory '%s' is empty. No files to process!",
					fileToAnalyze.getAbsolutePath()));
			return;
		}

//		List<File> fileList = Arrays.asList(files);
//		Stream<String> s = fileList.stream().map(e -> e.toString()).sorted(String::compareToIgnoreCase);
		
		String line;
		List<String> tokens;
		for (File file : files) {
			try {
				FileReader fileReader = new FileReader(file);
				BufferedReader bufferedReader  = new BufferedReader(fileReader);
				tokens = new ArrayList<>();
				while ((line = bufferedReader.readLine())!=null) {
					String[] parts = line.split("\t", 2);
					tokens.add(parts[0]);
				}
				logger.info(String.format("parsing %s", file.getAbsolutePath()));
			} catch (IOException e) {
				logger.error("Encountered IOException but will continue", e);
				continue;
			}
			DecoratedParse parse;
			try {
				parse = (DecoratedParse) parser.parse(tokens);
			} catch (RuntimeException e) {
				logger.error("Encountered RuntimeException but will continue", e);
				continue;
			}
			try {
				writeCdaFile(file.getName(), parse);
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}

	private void analyzeFile() throws IOException {
		FileReader fileReader = new FileReader(fileToAnalyze);
		BufferedReader bufferedReader  = new BufferedReader(fileReader);
		String line;
		if (mode == 1) {
			int counter = 0;
			List<String> tokens = new ArrayList<>();
			StringBuilder sentence = new StringBuilder();
			while ((line = bufferedReader.readLine())!=null || tokens.size() > 0) {
				if (line == null || (line.length() == 0 && tokens.size() > 0)) {
					DecoratedParse parse;
					logger.info(String.format("parsing '%s'", sentence.toString()));
					try {
						parse = (DecoratedParse) parser.parse(tokens);
					} catch (RuntimeException e) {
						logger.error("Encountered RuntimeException but will continue", e);
						continue;
					}
					writeCdaFile(String.format("%02d", counter), parse);
					counter++;
					tokens = new ArrayList<>();
					sentence = new StringBuilder();
					continue;
				}
				String[] parts = line.split("\t", 2);
				tokens.add(parts[0]);
				sentence.append(parts[0]);
			}
		}
		if (mode == 2) {
			while ((line = bufferedReader.readLine())!=null) {
				String[] parts = line.split("\t");
				if (parts.length != 2) {
					String msg = String.format("Line in file %s has the wrong format (Expected " +
									"'sentence\\tsomething') but got: '%s'",fileToAnalyze
							.getAbsoluteFile(), line);
					logger.error(msg);
					continue;
				}
				DecoratedParse parse;
				logger.info("parsing " + parts[0]);
				try {
					parse = (DecoratedParse) parser.parse(parts[1]);
				} catch (RuntimeException e) {
					logger.error("Encountered RuntimeException but will continue", e);
					continue;
				}
				writeCdaFile(parts[0], parse);
			}
		}
		bufferedReader.close();
	}

	private void writeCdaFile(String s, DecoratedParse parse) throws IOException {
		File cdaFile = new File(analysesFolder, String.format("%s.cda", s));
		if (cdaFile.exists()) {
			throw new FileAlreadyExistsException(String.format("%s already exists.", cdaFile
					.getAbsolutePath()));
		}
		else {
			FileWriter fileWriter = new FileWriter(cdaFile);
			fileWriter.write(parse.toAnnotation(false));
			fileWriter.flush();
			fileWriter.close();
		}
	}
}
