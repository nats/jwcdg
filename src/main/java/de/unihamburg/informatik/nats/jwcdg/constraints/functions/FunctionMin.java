package de.unihamburg.informatik.nats.jwcdg.constraints.functions;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * returns the minimum of all give nnumbers
 */
public class FunctionMin extends Function {

	public FunctionMin(Grammar g) {
		super("min", g, 1, -1, false);
	}

	@Override
	public AVM eval(List<Term> args, Context context, LexemeGraph lg,
			LevelValue[] binding) {
		double min = Double.MAX_VALUE;
		
		for(Term t: args) {
			AVM val = t.eval(lg, context, binding);
			
			if ( !(val instanceof AVMNumber) ) {
				return new AVMError("WARNING: argument of function `min' not a number");
			}
			
			double d = ((AVMNumber) val).getNumber();
			if(d<min)
				min = d;
		}

		return new AVMNumber(min); 
	}
	
}
