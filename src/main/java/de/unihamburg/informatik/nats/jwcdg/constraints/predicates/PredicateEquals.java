package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;

public class PredicateEquals extends PredicateComparison {

	public PredicateEquals(Grammar g) {
		super("==");
	}

	/**
	 * needed because we implement notEquals as a subclass of this
	 */
	protected PredicateEquals(Grammar g, String s) {
		super(s);
	}

	@Override
	protected boolean compare(AVM val1, AVM val2) {
		
		if ( (val1 instanceof AVMError) || (val2 instanceof AVMError) )
			return (false);

		if ((val1 instanceof AVMNumber) && (val2 instanceof AVMNumber)) {
			return ((AVMNumber)val1).getNumber() == ((AVMNumber)val2).getNumber();
			
		} else if ((val1 instanceof AVMString) && (val2 instanceof AVMString)) {
			return ((AVMString)val1).getString().equals(((AVMString)val2).getString());
			
		} else if ((val1 instanceof AVMLexemNode) && (val2 instanceof AVMLexemNode)) {
			LexemeNode ln1 = ((AVMLexemNode)val1).getLexemeNode(); 
			LexemeNode ln2 = ((AVMLexemNode)val2).getLexemeNode();
			
			if (ln1.isNonspec() || ln2.isNonspec()) {
				// nonspec is never equal to nonspec
				return false;
			} else if (ln1.isNil() && ln2.isNil()) {
				// root is root
				return true;
			} else if(ln1.getArc().equals(ln2.getArc())) {
				return (true);
			} else {
				return (false);
			}
		}

		throw new PredicateEvaluationException("WARNING: type mismatch in formula " + this.toString());
	}

	
}
