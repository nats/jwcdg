package de.unihamburg.informatik.nats.jwcdg.input;

import java.util.List;

public class Subsumption {
	public final String type;                   /* type identifikation */
	public final boolean subsumesFlag;          /* TRUE if type subsumes types */
	/* FALSE if types subsume type */
	public final List<String> types;            /* list of types */


	public Subsumption(String type,
			boolean subsumesFlag,
			List<String> types) {
		super();
		this.type = type;
		this.subsumesFlag = subsumesFlag;
		this.types = types;
	}

}
