package de.unihamburg.informatik.nats.jwcdg.frobbing;

import java.util.LinkedList;

import de.unihamburg.informatik.nats.jwcdg.constraints.Constraint;
import de.unihamburg.informatik.nats.jwcdg.transform.ConstraintViolation;

public class RepairAttempt {
	private long starttime;
	private long time;
	private ConstraintViolation primaryConflict;
	private LinkedList<Constraint> cs;
	private boolean successful;

	public RepairAttempt(ConstraintViolation primaryConflict) {
		starttime = System.currentTimeMillis();
		time = 0;
		this.primaryConflict = primaryConflict;
		cs = new LinkedList<>();
		successful= false;
	}

	public void addConstraint(Constraint constraint) {
		cs.add(constraint);
	}

	public void setSuccessful(boolean b) {
		successful = b;
	}

	public void noteTime() {
		time = System.currentTimeMillis() - starttime;
	}

	public long getTime() {
		return time;
	}

	public ConstraintViolation getPrimaryConflict() {
		return primaryConflict;
	}

	public LinkedList<Constraint> getCs() {
		return cs;
	}

	public boolean isSuccessful() {
		return successful;
	}
	
	@Override
	public String toString() {
		if(time==0) // wip
			return String.format(
					"Repairing of %s in Prorgress(%d steps so far)",
					primaryConflict.toString(), cs.size()+1);
		else
			return String.format(
					"Repairing %s(%d steps)-->%s", 
					primaryConflict.toString(), cs.size()+1, 
					successful?"success":"failed");
	}

	
}
