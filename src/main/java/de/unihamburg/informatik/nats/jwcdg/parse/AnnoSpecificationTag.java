package de.unihamburg.informatik.nats.jwcdg.parse;

public class AnnoSpecificationTag extends AnnoSpecification {

	public AnnoSpecificationTag(String tag, String val) {
		super(tag, val);
	}
	
	@Override
	public String toString() {
		return kind + ":" + name;
	}
}
