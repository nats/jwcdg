package de.unihamburg.informatik.nats.jwcdg.constraints.formulas;

import java.io.Serializable;
import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.AccessContext;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

public abstract class Formula implements Serializable {

	private static final long serialVersionUID = 1L;

    public boolean isContextSensitive;
	
	/** used for cache lookups */
	protected int id = -1; 
	
	public Formula(){
		this.isContextSensitive = false;
	}
	
	public abstract boolean evaluate(LevelValue[] binding, Context context, LexemeGraph lg);
	
	/** 
	 * Propagate feature access information through a Formula.
	 * 
	 * Restrictions introduced by the formula itself are propagate up in context 
     */
	public abstract void analyze(AccessContext context, Grammar g);
	
	public abstract void collectFeatures(List<Path> sofar);
	
	/**
	 * checks whether all arities of predicates and functions are correct
	 * 
	 * @return
	 */
	public abstract boolean check();
	
	/**
	   Are the two formulas different?

	   This function does much less than the name suggests. It returns FALSE
	   only if f1 and f2 are two `has' invocations with the same second
	   argument. At the moment we don't need any more from it.
	   Therefore we don't dare to call this equals()
	*/
	public boolean areSameHasInvocation(Formula other) {
		return false;
	}
	
	public abstract void collectHasInvocations(List<Formula> akku);

	public int getIndex() {
		return id;
	}

	public void setIndex(int index) {
		id= index;
	}
}
