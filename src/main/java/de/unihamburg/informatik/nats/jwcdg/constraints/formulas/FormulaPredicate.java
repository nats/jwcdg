package de.unihamburg.informatik.nats.jwcdg.constraints.formulas;

import java.util.ArrayList;
import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.AccessContext;
import de.unihamburg.informatik.nats.jwcdg.constraints.predicates.Predicate;
import de.unihamburg.informatik.nats.jwcdg.constraints.predicates.PredicateEquals;
import de.unihamburg.informatik.nats.jwcdg.constraints.predicates.PredicateHas;
import de.unihamburg.informatik.nats.jwcdg.constraints.predicates.PredicateNotEquals;
import de.unihamburg.informatik.nats.jwcdg.constraints.predicates.PredicateSubsumes;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.TermLabel;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.TermString;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Hierarchy;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

public class FormulaPredicate extends Formula {
	private final Predicate pred;
	private final List<Term> args;
	
	public FormulaPredicate(Predicate p) {
		this.pred = p;
		this.args = new ArrayList<>(0);
		this.isContextSensitive = p.isContextSensitive;
	}

	public FormulaPredicate(Predicate p, List<Term> args) {
		this.pred = p;
		this.args = args;
		this.isContextSensitive = p.isContextSensitive;
		for (Term term : args) {
			if(term.isContextSensitive)
				this.isContextSensitive = true;
		}
	}
	
	public FormulaPredicate(Predicate p, Term arg) {
		this.pred = p;
		this.args = new ArrayList<>(1);
		this.args.add(arg);
		this.isContextSensitive = p.isContextSensitive;
		if(arg.isContextSensitive)
			this.isContextSensitive = true;
	}
	
	public FormulaPredicate(Predicate p, Term arg1, Term arg2) {
		this.pred = p;
		this.args = new ArrayList<>(2);
		this.args.add(arg1);
		this.args.add(arg2);
		this.isContextSensitive = p.isContextSensitive;
		if(arg1.isContextSensitive || arg2.isContextSensitive)
			this.isContextSensitive = true;
	}
	
	public FormulaPredicate(Predicate p, Term arg1, Term arg2, Term arg3) {
		this.pred = p;
		this.args = new ArrayList<>(3);
		this.args.add(arg1);
		this.args.add(arg2);
		this.args.add(arg3);
		this.isContextSensitive = p.isContextSensitive;
		if(arg1.isContextSensitive || arg2.isContextSensitive || arg3.isContextSensitive)
			this.isContextSensitive = true;
	}

	@Override
	public boolean evaluate(LevelValue[] binding,
			Context context, LexemeGraph lg) {
		return pred.eval(args, binding, lg, context, id);
	}
	
	@Override
	public void collectFeatures(List<Path> sofar) {
		for(Term t: args) {
			t.collectFeatures(sofar);
		}
	}
	
	@Override
	public boolean check() {
		int n= args.size();
		if(n < pred.minArity || pred.maxArity != -1 && n > pred.maxArity)
			return false;
		for (Term t : args) {
			if( !t.check() )
				return false;
		}
		return true;
		
	}
	
	@Override
	public String toString() {
		if(pred.isInfix() && args.size() ==2) {
			return String.format("(%s %s %s)",
					args.get(0).toString(), 
					pred.toString(),
					args.get(1).toString());
		} else {
			StringBuilder ret= new StringBuilder(pred.toString());
			ret.append("(");
			boolean first = true;
			for(Term t : args) {
				if(!first)
					ret.append(", ");
				first = false;
				ret.append(t);
			}
			ret.append(")");
			return ret.toString();
		}
	}

	@Override
	public void analyze(AccessContext context, Grammar g) {
		if (pred instanceof PredicateSubsumes) {
			TermString a1 = (TermString)args.get(0);
			Term a2 = args.get(1);
			Term a3 = args.get(2);
			if ((a2 instanceof TermString)
			     && (a3 instanceof TermLabel)) {
				Hierarchy h = g.findHierarchy(a1.getString());
				if (h != null) {
					String label = ((TermString)a2).getString();
					int vi = ((TermLabel)a3).getVi().index;
					context.restrictLabel(vi, label);
				}
			}
		}
		if (pred instanceof PredicateEquals) {
			Term t1 = args.get(0);
			Term t2 = args.get(1);
			if ((t1 instanceof TermLabel) && (t2 instanceof TermString)) {
				context.restrictLabel(((TermLabel)t1).getVi().index, ((TermString)t2).getString());
			} else if ((t2 instanceof TermLabel) && (t1 instanceof TermString)) {
				context.restrictLabel(((TermLabel)t2).getVi().index, ((TermString)t1).getString());
			}
		}
		if (pred instanceof PredicateNotEquals) {
			Term t1 = args.get(0);
			Term t2 = args.get(1);
			if ((t1 instanceof TermLabel) && (t2 instanceof TermString)) {
				context.restrictNotLabel(((TermLabel)t1).getVi().index, ((TermString)t2).getString());
			} else if ((t2 instanceof TermLabel) && (t1 instanceof TermString)) {
				context.restrictNotLabel(((TermLabel)t2).getVi().index, ((TermString)t1).getString());
			}
		}
		/* predicates mark the end of the control flow, so the Context
		does not change anymore, and we can pass it by reference without
		fear. But they may contain feature accesses, so we have to scan them
		as well. */
		for (Term t : args) {
			t.analyze(context, g);
		}
	}
	
	/**
	 * Are the two formulas has predicates with only two arguments 
	 * and share the 2nd argument?
	 */
	public boolean areSameHasInvocation(Formula other) {
		if(!(other instanceof FormulaPredicate)) {
			return false;
		}
		FormulaPredicate f2 = (FormulaPredicate) other;
		
		if( !(this.pred instanceof PredicateHas) || !(f2.pred instanceof PredicateHas)) {
			return false;
		}
		
		if( this.args.size() != 2 || f2.args.size() != 2) {
			return false;
		}
		/* We compare only the second argument, not the first. This means that
	     `has(X@id, OBJA)' and `has(Y^id, OBJA)' are considered identical.
	     PredicateHas is aware of this aggressive definition. */
		return this.args.get(1).equals(f2.args.get(1));
	}
	
	@Override
	public void collectHasInvocations(List<Formula> akku) {
		if( (pred instanceof PredicateHas) && args.size()==2 ) {
			akku.add(this);
		}
	}
}
