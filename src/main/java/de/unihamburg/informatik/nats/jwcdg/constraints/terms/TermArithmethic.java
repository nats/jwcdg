package de.unihamburg.informatik.nats.jwcdg.constraints.terms;


import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.AccessContext;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * superclass of all terms calculating some arithmetical function for two argument terms
 * 
 * @author the CDG-Team
 *
 */
public abstract class TermArithmethic extends Term {
	protected Term a, b;
	
	public TermArithmethic(Term a, Term b) {
		this.a = a;
		this.b = b;
		this.isContextSensitive = a.isContextSensitive || b.isContextSensitive;
	}
	
	@Override
	public void analyze(AccessContext context, Grammar g) {
		a.analyze(context, g);
		b.analyze(context, g);
	}
	
	@Override
	public AVM eval(LexemeGraph lg, Context context,
			LevelValue[] binding) {
		AVM va = a.eval(lg, context, binding);
		AVM vb = b.eval(lg, context, binding);
		
		if( (va instanceof AVMNumber) && (vb instanceof AVMNumber)) {
			double res = calculate(((AVMNumber)va).getNumber(), ((AVMNumber)vb).getNumber());
			return new AVMNumber(res);
		} else {
			if (va instanceof AVMError) {
				return va;
			}
			if (vb instanceof AVMError) {
				return vb;
			}
			return new AVMError("Cannot calculate with non-numerical values");
		}
	}
	
	protected abstract double calculate(double d1, double d2); 
	
	@Override
	public String toString() {
		return a.toString() + sign() + b.toString(); 
	}
	
	protected abstract String sign(); 
	
	@Override
	public boolean check() {
		if(!a.check() || !b.check())
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return a.hashCode()*31+b.hashCode();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		
		if(!(obj instanceof TermArithmethic))
			return false;
		
		if(this.getClass() != obj.getClass())
			return false;
		
		TermArithmethic other = (TermArithmethic)obj;
		
		return (this.a.equals(other.a) && this.b.equals(other.b));
	}
}
