package de.unihamburg.informatik.nats.jwcdg.antlr;
import java.io.InputStreamReader;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.CommonTokenStream;

import de.unihamburg.informatik.nats.jwcdg.ForgettingANTLRInputStream;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarLexer;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarParser;

/**
 *
 * @author Arne Köhn
 */
public class Helpers {
	public static jwcdggrammarParser parserFromString(String s) {
		CharStream cs = new ANTLRStringStream(s);
		jwcdggrammarLexer lexer = new jwcdggrammarLexer(cs);
		CommonTokenStream ts = new CommonTokenStream(lexer);
		return new jwcdggrammarParser(ts);
	}
	public static jwcdggrammarParser parserFromIS(InputStreamReader is) {
		CharStream cs = new ForgettingANTLRInputStream(is);
		jwcdggrammarLexer lexer = new jwcdggrammarLexer(cs);
		CommonTokenStream ts = new CommonTokenStream(lexer);
		return new jwcdggrammarParser(ts);
	}
}
