package de.unihamburg.informatik.nats.jwcdg.constraintNet;

import static de.unihamburg.informatik.nats.jwcdg.constraints.Connection.Bottom;
import static de.unihamburg.informatik.nats.jwcdg.constraints.Connection.Identical;
import static de.unihamburg.informatik.nats.jwcdg.constraints.Connection.Inverse;
import static de.unihamburg.informatik.nats.jwcdg.constraints.Connection.None;
import static de.unihamburg.informatik.nats.jwcdg.constraints.Connection.Over;
import static de.unihamburg.informatik.nats.jwcdg.constraints.Connection.Top;
import static de.unihamburg.informatik.nats.jwcdg.constraints.Connection.Under;
import static de.unihamburg.informatik.nats.jwcdg.constraints.Direction.Left;
import static de.unihamburg.informatik.nats.jwcdg.constraints.Direction.Nil;
import static de.unihamburg.informatik.nats.jwcdg.constraints.Direction.Right;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.constraints.Connection;
import de.unihamburg.informatik.nats.jwcdg.constraints.Constraint;
import de.unihamburg.informatik.nats.jwcdg.constraints.Direction;
import de.unihamburg.informatik.nats.jwcdg.constraints.Level;

/**
 * An edge in a dependency graph,
 * corresponds to a possible value for a constraint variable on a given level in the constraint net, thus the name 
 * 
 * @author the CDG-Team
 *
 */
public class LevelValue implements Serializable {
	
    private static final long serialVersionUID = 1L;
    private static Logger logger = LogManager.getLogger(LevelValue.class);
	private List<LexemeNode> dependents; /* modifying lexemenodes */
	private GraphemeNode dependent;      /* modifying graphemnode */
	private Level level;                 /* level of modification relation */
	private String label;                /* label of modification relation */
	private List<LexemeNode> regents;    /* modified nodes */
	private GraphemeNode regent;         /* modified graphemnode */
	
	private int noOfLevels;              /* cached from net so we can compute our index */

	private double score;                  /* score */
	private double nonFluctuatingScore;	   /* part of the score resulting from fluctuating constraints, used in incremental environment */
	private List<Constraint> constraints;  /* list of violated constraints */
	
	private int indexWRTNet;               /* index with respect to net */
	private AVMString labelVal;
	
	
	public LevelValue(Level level, int noOfLevels,
			List<LexemeNode> dependents, GraphemeNode dep, 
			String label, 
			List<LexemeNode> regents, GraphemeNode reg) {
		this.dependents = new ArrayList<>(dependents);
		this.regents    = new ArrayList<>(regents);
		this.level      = level;
		this.label      = label;
		this.dependent  = dep;
		this.regent     = reg;
		this.noOfLevels  = noOfLevels;
		
		this.labelVal = new AVMString(label);
		
		// will be added to a net later;
		//net = null;
		indexWRTNet = -1;
		
		// will be set by eval unary 
		score = 0.0;
		nonFluctuatingScore = 0.0;
		
		constraints = new ArrayList<>();
	}
	
	public LevelValue(LevelValue orig) {
		this(orig.level, 0, orig.dependents, orig.dependent, orig.label, orig.regents, orig.regent);
		// we don't know the number of levels, (the 0 above), so we copy it explicitely: 
		noOfLevels = orig.noOfLevels;
	}

	/**
	 * This function computes the `logical' number of an LV in the set of LVs
	 * that constitute a dependency analysis: it is the position it will have if
	 * the LVs are sorted by time index first, and by level second. Some
	 * indices may not be assigned in two cases:
	 *
	 * (a) if there are lexeme nodes that span more than one time interval,
	 * lvIndex() will leave out those that correspond to positions within the
	 * long word.
	 *
	 * (b) if individual levels have their useFlag reset, the indices
	 * corresponding to LVs of these levels will also be skipped.
	 *	
	 * This allows the function to determine exactly what position an LV holds,
	 * without any other context, but can be wasteful on space. The structure
	 * Analysis from the module frobbing uses this indexing method; the
	 * Parse uses a compact method that always gives consecutive index numbers. 
	 */
	public int getIndex() { 
		return noOfLevels * dependent.getArc().from + level.getNo();
	}

	public int getIndexWRTNet() {
		return indexWRTNet;
	}

	public Level getLevel() {
		return level; 
	}

	public GraphemeNode getRegentGN() {
		return regent;
	}

	public GraphemeNode getDependentGN() {
		return dependent;
	}

	public void clearConstraints() {
		constraints.clear();
	}

	/**
	 * return the Direction of an LV
	 */
	public Direction getDirection() {
		if (regent.isNil())
			return Nil;
		else if (regent.isNonspec())
			return Right;
		else if (regent.getArc().from < dependent.getArc().from)
			return Left;
		else
			return Right;
	}

	public void addViolatedConstraint(Constraint c) {
		constraints.add(c);
	}

	public void setScore(double score) {
		this.score = score;
	}

	/**
	 * return the Connexion of two LVs
	 */
	public Connection getCon(LevelValue lvb) {
		GraphemeNode atop = regent,    btop = lvb.regent;
		GraphemeNode abot = dependent, bbot = lvb.dependent;

		if (atop == btop && abot == bbot && atop.spec())
			return Identical;

		  if (atop == bbot && abot == btop)
		    return Inverse;

		  if (atop == bbot)
		    return Under;

		  if (abot == btop)
		    return Over;

		  if (atop == btop && atop.spec())
		    return Top;

		  if (abot == bbot)
		    return Bottom;

		  return None;
	}

	public List<LexemeNode> getRegents() {
		return regents;
	}

	/**
	 * make a lexeme variant of this lv,
	 * useful to produce combinations not present in the net
	 * 
	 * the copy has a score of zero, probably ok, 
	 * as it is absent from the net for a reason..
	 * 
	 * the variant lv has no id (-1) as it is not part of a net
	 * and it can be restricted to a given regent or dependent LN
	 * 
	 * @param dep
	 * @param reg
	 * @return
	 */
	public LevelValue makeVariant(LexemeNode dep, LexemeNode reg) {
		LevelValue clone = new LevelValue(this);
		clone.indexWRTNet = -1;
		
		if(dep != null ){
			clone.dependents = new ArrayList<>();
			clone.dependents.add(dep);
		}
		if(reg != null ){
			clone.regents = new ArrayList<>();
			clone.regents.add(reg);
		}
		return clone;
	}

	public List<LexemeNode> getDependents() {
		return dependents;
	}

	/**
	 Fix self-modifying LVs.

	 When swapping in a lexeme that is so long that it overlaps both the
	 modifier and the modifiee of an LV, an LV may end up with a lexeme
	 node that modifies itself.

	 Since this is unlikely to be helpful, we point those LVs to NIL
	 immediately. Perhaps it would be smarter to point them wherever is
	 best at the moment, but that is a problem for another day.

	 Note that since this LV is invalid, it cannot be the member of any
	 domain. So we don't have to check its index to avoid changing the
	 original LVs.
	 */
	public void breakCircle(GraphemeNode replacement) {
		if (regent.spec() && dependents.get(0).overlaps(regents.get(0))) {
			logger.warn(String.format(
					"#%d modifies self at slot @%d",
					indexWRTNet, getIndex()));
			regent  = replacement;
			regents = new ArrayList<>(replacement.getLexemes());
		}
	}

	public double getScore() {
		return score;
	}
	
	public List<Constraint> getConstraints() {
		return constraints;
	}

	/**
	 * by this index we should be able to find this lv in the values list of its constraintnet
	 * @param index
	 */
	public void addToNet(ConstraintNet net, int index) {
		//this.net = net;
		this.indexWRTNet = index;
	}

	public String getLabel() {
		return label;
	}
	
	/**
	 *  Is the LV physically compatible with the lexemes allowed in the
	 *  Vector?
	 *
	 *  This uses a Vector of Boolean as produced by lgRequireLexeme().
	 */
	public boolean isLexemeCompatible(List<LexemeNode> taboo) {
		Boolean found = false;
		
		/* one modifiee must be allowed */
		if (regent.spec()) {
			for (LexemeNode ln: regents) {
				if (!taboo.contains(ln)) {
					found = true;
					break;
				}	
			}
			if (!found) {
				return false;
			}
		}

		/* one modifier must be allowed */
		for (LexemeNode ln : dependents) {
			if (!taboo.contains(ln)) {
				return true;
			}
		}
		
		return false;
	}
	 
	@Override
	public boolean equals(Object obj) {
		if(this==obj)
			return true;
		
		if (obj instanceof LevelValue) {
			LevelValue other = (LevelValue) obj;
			if(this.indexWRTNet != -1 || other.indexWRTNet !=-1)
				return this.indexWRTNet == other.indexWRTNet;
			
			if(!this.level.equals(other.level))
				return false;
			
			if(!this.label.equals(other.label))
				return false;

			if(!this.dependent.equals(other.dependent))
				return false;
			
			if(!this.regent.equals(other.regent))
				return false;
			
			if(this.dependents.size() != other.dependents.size() || this.regents.size() != other.regents.size())
				return false;
			
			for(int i=0; i < this.dependents.size(); i++) {
				if(!this.dependents.get(i).equals(other.dependents.get(i)))
					return false;
			}
			
			for(int i=0; i < this.regents.size(); i++) {
				if(!this.regents.get(i).equals(other.regents.get(i)))
					return false;
			}
			
			return true;
		}
		return false;
	}

	public double getNonflucScore() {
		return nonFluctuatingScore;  
	}
	
	public void setNonFluctuatingScore(double nonFluctuatingScore) {
		this.nonFluctuatingScore = nonFluctuatingScore;
	}
	
	/**
	 * resets the score to the nonfluctuating score and
	 * removes all fluctuating constraitns from the constraint list
	 */
	public void removeFluctuatingconstraints() {
		score = nonFluctuatingScore;
		Iterator<Constraint> it = constraints.iterator();
		while(it.hasNext()) {
			Constraint c= it.next();
			if( c.getAttributes().contains(Constraint.ATTR_FLUCTUATION) ||
				c.getAttributes().contains(Constraint.ATTR_NONSPEC_FLUCTUATION)) {
				it.remove();
			}
		}
	}
	
	@Override
	public String toString() {
		return String.format("%s:%s--%s->%s", 
				level.getId(), dependents.get(0), label, regents.get(0));
	}
	
	@Override
	public int hashCode() {
		return indexWRTNet;
	}

	public AVM getLabelVal() {
		return labelVal;
	}
}
