package de.unihamburg.informatik.nats.jwcdg.avms;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import de.unihamburg.informatik.nats.jwcdg.input.Configuration;

public class AVMList extends AVM {
	private static final long serialVersionUID = 4409221765221121011L;

	private List<AVM> list;

	public AVMList(List<AVM> list) {
		this.list = list;
	}
	public List<AVM> getList() {
		return list;
	}
	
	@Override
	public AVM substituteRefs(Matcher matcher, Configuration config) {
		List<AVM> l2 = new ArrayList<>(list.size());
		for(AVM val : list) {
			l2.add(val.substituteRefs(matcher, config));
		}
		
		return new AVMList(l2);
	}	
	
	@Override
	public String toString() {
		return list.toString();
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((list == null) ? 0 : list.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AVMList))
			return false;
		AVMList other = (AVMList) obj;
		if (list == null) {
			if (other.list != null)
				return false;
		} else if (!list.equals(other.list))
			return false;
		return true;
	}
}
