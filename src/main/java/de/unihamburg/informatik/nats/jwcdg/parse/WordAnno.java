package de.unihamburg.informatik.nats.jwcdg.parse;

import java.util.List;

public class WordAnno {
	public final int from;                 /* start node */
	public final int to;                   /* end node */
	public String word;                    /* lexem */
	public List<AnnoSpecification> specs;  /* list of specifications */
	
	public WordAnno(int from, int to, String word, List<AnnoSpecification> specs) {
		super();
		this.from = from;
		this.to = to;
		this.word = word;
		this.specs = specs;
	}
	
	@Override
	public String toString() {
		return word + ":" + specs.toString();
	}
}
