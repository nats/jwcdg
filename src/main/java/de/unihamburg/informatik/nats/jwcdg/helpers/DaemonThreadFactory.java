package de.unihamburg.informatik.nats.jwcdg.helpers;

import java.util.concurrent.ThreadFactory;

/**
 * A Thread factory that returns daemon threads.
 * copied from http://stackoverflow.com/questions/1516172/executor-and-daemon-in-java
 * but this is *really* straightforward.
 *
 */

public class DaemonThreadFactory implements ThreadFactory {
	@Override
	public Thread newThread(Runnable r) {
		Thread thread = new Thread(r);
		thread.setDaemon(true);
		return thread;
	}
}
