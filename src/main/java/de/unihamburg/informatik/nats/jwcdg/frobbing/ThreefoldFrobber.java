package de.unihamburg.informatik.nats.jwcdg.frobbing;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNetState;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;
import de.unihamburg.informatik.nats.jwcdg.parse.ParseInfo;
import de.unihamburg.informatik.nats.jwcdg.transform.Analysis;

/**
 * The most precise frobbing method
 * 
 * First applies the dynamic method that solves the subclauses, 
 * but is bound to the decisions made in these subfrobs.
 * 
 * So we reset the netState and do an overall frobb again in the end
 * with the combined method.
 * 
 * @author The CDG-Team
 *
 */
public class ThreefoldFrobber extends AbstractFrobber {

	private static Logger logger = LogManager.getLogger(ThreefoldFrobber.class);
	
	public ThreefoldFrobber(ConstraintNetState state, Configuration config,
			GrammarEvaluator evaluator, double pressure) {
		super(state, config, evaluator, pressure);
	}

	public ThreefoldFrobber(ConstraintNetState state, Analysis a,
			Configuration config, GrammarEvaluator evaluator, double pressure) {
		super(state, a, config, evaluator, pressure);
	}

	public ThreefoldFrobber(ConstraintNetState state, Configuration config,
			GrammarEvaluator evaluator, double pressure, Parse parse) {
		super(state, config, evaluator, pressure, parse);
	}

	@Override
	public boolean solve() {
		logger.info("Starting Threefold Frobbing");
		long starttime= System.currentTimeMillis();
		
		// step one: stop via dynamic
		logger.info("=============================================");
		logger.info("Step 1: dynamic");		
		DynamicFrobber dynamic = new DynamicFrobber(
				netState, current, config, evaluator, globalMinimum);
		dynamic.registerObservers(getObservers());
		// don't spend more than half of the time in step 1
		dynamic.setTimeLimit(Math.max(timeLimit/2, 1));
		dynamic.solve();
		this.current = dynamic.getBest();
		
		// step two: reset state
		logger.info("=============================================");
		logger.info("Step 2: restore netState");
		// state was remembered in the constructor
		restoreSavedState();
		
		// step three, frob the whole problem again via combined
		logger.info("=============================================");
		logger.info("Step 3: combined");
		CombinedFrobber combined = new CombinedFrobber(netState, current, config, evaluator, globalMinimum);
		dynamic.registerObservers(getObservers());
		// we gave half of the time to step one, but probably it didn't use it all,
		// so can set the timelimit for this step to what is actually left
		int timeLeft = Math.max(timeLimit -(int)(System.currentTimeMillis()-starttime),1);
		logger.info(String.format("Time left: %.3f sec", timeLeft/1000.0));
		combined.setTimeLimit(timeLeft);
		boolean ret = combined.solve();
		this.current = combined.getBest();
		
		// finished:
		this.best = new Analysis(current);
		this.solution = combined.getSolution();
		ParseInfo info = solution.getInfo();
		info.setEvaluationTime(System.currentTimeMillis() - stats.getStartTime());
		info.setSearchStrategy("Frobbing");
		info.setEntry("Frobbing Method", this.methodName());
		
		return ret;
	}

	@Override
	public String methodName() {
		return ThreefoldFactory.NAME;
	}
}
