package de.unihamburg.informatik.nats.jwcdg.constraintNet;

import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;
import de.unihamburg.informatik.nats.jwcdg.input.LexiconItem;
import de.unihamburg.informatik.nats.jwcdg.lattice.Arc;
import de.unihamburg.informatik.nats.jwcdg.lattice.Lattice;
import de.unihamburg.informatik.nats.jwcdg.predictors.MaltPredictor;
import de.unihamburg.informatik.nats.jwcdg.predictors.Tagger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.util.*;

/**
A LexemeGraph is a word graph enriched with lexical information. In
particular, it contains several lexemes for each arc of the Lattice
that is lexically ambiguous. Each of these pairs of lexical entry and time
span is called a lexeme node.  The field \a lattice points to the
underlying word graph.

The Vectors \a graphemenodes and \a nodes contain all grapheme nodes
and lexeme nodes. Grapheme nodes are an intermediary data structure between
word arcs and lexeme nodes that is not strictly necessary.

The fields \a min and \a max correspond to the fields with the
same names in the Lattice.

The field \a distance holds an Array of the distance between any two
lexeme nodes. The distance is measured in words, hence any two adjacent
lexeme nodes have distance~1. The distance array is also used to check
whether two lexeme nodes are compatible with each other, i.e.  whether there
is a path through the lexeme graph that includes them both.

The fields \a noOfPathsFromStart and \a noOfPathsToEnd hold arrays
that map each grapheme node to the number of complete paths from the start
or to the end of the entire graph that go through it. This is used to
determine whether a lexeme node can be deleted or not.

The field \a noOfPaths holds the total number of paths from start to end
possible in the word graph. This should reflect the state of the Vector
\a isDeletedNode.

The Vector \a isDeletedNode marks those lexeme nodes that have been
deleted and should be ignored. For instance, when an LV is added to a
partial solution, all lexeme nodes that are not lgCompatibleNodes()
to its lexeme nodes should be marked as deleted.
 */
public class LexemeGraph implements Serializable {
	
    private static final long serialVersionUID = 1L;

    public static final String COMPACT_LVS = "compactLVs";

	protected Lattice lattice;                    /**< underlying word graph    */
	protected List<GraphemeNode> graphemeNodes;   /**< vector of grapheme nodes */
	protected List<LexemeNode>   nodes;           /**< vector of lexeme nodes   */
	protected Lexicon lexicon;
	
	private GraphemeNodeNil     nil;
	private GraphemeNodeNonspec nonspec;
	
	/** matrix of distances, does not regard deletion of GNs, 
	 *  the dynamic version is in ConstraintNetState*/
	protected List<List<Integer>> distance;  

	protected int min;                           /**< minimum start position */
	protected int max;                           /**< maximum end position */

	// TODO #PREDICTORS_CHUNKING
	//private List<Chunk> chunks;                   // set of all chunks of the lattice

	private Map<String, Object> predictorData;

	private boolean complete;                     // whether this lexemgraph represents a complete sentence, in contrast to a partial sentence like a prefix 

	protected Configuration config; 
	protected static Logger logger = LogManager.getLogger(LexemeGraph.class);

	/**
	 * constructor for cloning
	 */
	protected LexemeGraph(LexemeGraph other) {
		this.complete = other.complete;
		this.config   = other.config;
		this.lattice  = other.lattice;
		this.lexicon  = other.lexicon;
		this.max      = other.max;
		this.min      = other.min;
		this.nil      = other.nil;
		this.nonspec  = other.nonspec;
		this.predictorData = other.predictorData;
		
		// shallow copy of all lists 
		this.distance = new ArrayList<>(other.distance.size());
		for(int i=0; i< other.distance.size(); i++) {
			this.distance.add(new ArrayList<>(other.distance.get(i)));
		}
		this.graphemeNodes = new ArrayList<>(other.graphemeNodes);
		this.nodes         = new ArrayList<>(other.nodes);
	}
	
	/**
	 This function creates a lexeme graph from a Lattice @a lat and a cdg lexicon.
	 For each arcs of the lattice a grapheme node is allocated and annotated with
	 all possible lexical entries. (If there is no lexical entry for an arc, a
	 warning is given, but processing continues.)

	 For each grapheme node, as many lexeme nodes are created as there are
	 lexical alternatives in the lexicon.

	 Furthermore:
	 - LexemGraph::isDeletedNode is initialized to FALSE
	 - LexemGraph::noOfPathsFromStart and LexemGraph::noOfPathsFromStart=are
	 computed using computeNoOfPaths().
	 - LexemGraph::distance is computed using lgComputeDistances().
	 */
	public LexemeGraph(Lattice lat, Lexicon lexicon, Configuration config) {
		this.config   = config;
		graphemeNodes = new ArrayList<>();
		nodes         = new ArrayList<>();
		max = 0;
		min = Integer.MAX_VALUE;
		this.lexicon = lexicon;
		this.predictorData = new HashMap<>();
		//distance = NULL;
		//isDeletedNode = bvNew(20);
		//isFrozenNode = bvNew(20);
		//noOfPathsFromStart = NULL;
		//noOfPathsToEnd = NULL;

		//chunks = NULL;
		//ve = NULL; //veNew(lg);
		complete  = false; // default assumption, overwrite later on if it should be FALSE
		lattice   = lat;
		
		nil     = new GraphemeNodeNil();
		nonspec = new GraphemeNodeNonspec(this);

		for (Arc arc : lat.getArcs()) {
			lgNewIter(arc);
		}
		
		computeDistances();

		Tagger.getTagger(config).predict(this);
		MaltPredictor.getMaltPredictor(config).predict(this);
		// TODO #PREDICTORS call other predictos
		//		lgPredict();
	}
	
	/**
	 * generate an empty initial lexemegraph
	 * used for incremental parsing
	 */
	public LexemeGraph(Lexicon lexicon, Configuration config) {
		this.config   = config;
		graphemeNodes = new ArrayList<>();
		nodes         = new ArrayList<>();
		max = 0;
		min = 0;
		this.lexicon = lexicon;
		this.predictorData = new HashMap<>();

		complete  = false;
		lattice   = new Lattice(new ArrayList<>());
		
		nil     = new GraphemeNodeNil();
		nonspec = new GraphemeNodeNonspec(this);
		
		computeDistances();
	}

	/** ----------------------------------------------------------------------------
	 @brief Insert lexeme nodes into the LexemGraph that correspond the Arc.

	 This function builds all possible lexeme nodes for the specific
	 @a arc and adds them to @a lg. It fails if there is no
	 matching entry in the lexicon.
	 */
	protected void lgNewIter(Arc arc) {
		GraphemeNode gn;
		List<LexiconItem> entries = new ArrayList<>();

		/* Maybe undo capitalisation introduced by orthographic convention.

		 If the written word is uppercase, but that uppercase-ness is suspect
		 because it is at the start of a phrase and might be mere orthographic
		 convention, we have to decide which version we use for lexicon lookup.

		 If our lexicon contains items for the lower-case version but none for
		 the upper-case versions, we use only those; if it contains only
		 items for the upper-case version, we use those; and if it contains
		 neither, we allow both and hope that there is a lexical template which
		 will catch this word.

		 We do not use the obvious solution - look up both versions whenever a
		 word is spurious - because it has the following defect: If a sentence
		 starts with `Der', some naive lexical template could introduce a noun
		 reading, and if POS tagging allows, it might actually survive even
		 though it is exceedingly unlikely. Since we do not want this, we
		 effectively force the reading to be `der'.

		 Moral: If you really need to have open-class items in your lexicon
		 that are near-homonymous with closed-class items, you can bloody well
		 write proper lexicon items for them and not templates. */

		if (spuriousUppercase(arc)) {
			boolean have_upper, have_lower;

			/* skip initial ( or " */
			int index = 0;
			while ( arc.word.charAt(index) == '"' ||
					arc.word.charAt(index) == '(') {
				index++;
			}

			char[] cs = arc.word.toCharArray();
			cs[index] = Character.toLowerCase(cs[index]);
			String low = new String(cs);

			have_upper = lexicon.query(arc.word);
			have_lower = lexicon.query(low);

			if (have_upper && !have_lower) {
				entries.addAll(lexicon.get(arc.word, config));
			} else if (have_lower && !have_upper) {
				entries.addAll(lexicon.get(low, config));
			} else {
				entries.addAll(lexicon.get(low, config));
				entries.addAll(lexicon.get(arc.word, config));
			}
		}

		/* Much the same goes for words in ALL UPPER CAPS, except that those can
		 occur anywhere in a sentence, not only at the start, and we have to
		 check three different spellings instead of two. */
		else if (arc.inputALLUPPER()) {
			String low        = arc.word.toLowerCase(); 
			String startUpper = arc.word.charAt(0)+arc.word.substring(1).toLowerCase();
			boolean have_upper, have_lower, have_startUpper;

			have_lower      = lexicon.query(low);
			have_startUpper = lexicon.query(startUpper);
			have_upper      = lexicon.query(arc.word);

			/* In one-letter words, the intermediate version is
			 indistinguishable from the third one, so we suppress it.  */
			if (arc.word.length() == 1) {
				have_startUpper = false;
			}

			if (have_lower) {
				entries.addAll(lexicon.get(low, config));
			}
			if (have_startUpper) {
				entries.addAll(lexicon.get(startUpper, config));
			}
			if (have_upper) {
				entries.addAll(lexicon.get(arc.word, config));
			}
			if (!have_lower && !have_startUpper && !have_upper) {
				entries.addAll(lexicon.get(low, config));
				entries.addAll(lexicon.get(startUpper, config));
				entries.addAll(lexicon.get(arc.word, config));
			}

		} else {
			entries.addAll(lexicon.get(arc.word, config));
		}

		if (entries.isEmpty()) {
			logger.error(String.format("No lexical entry for `%s'!", arc.word));
			throw new RuntimeException(String.format("No lexical entry for `%s'!", arc.word));
		}

		min = Math.min(min, arc.from);
		max = Math.max(max, arc.to);

		gn = new GraphemeNode(arc);
		/* force consistent order among homonyms */
		Collections.sort(entries);
		for (LexiconItem le: entries) {
			addLexemeNode(gn, le);
		}
		
		addGraphemeNode(gn);
	}

	/**
	 * generate a lexemnode for a given lexicon item and graphemnode and add it to the lg
	 */
	protected void addLexemeNode(GraphemeNode gn, LexiconItem li) {
		LexemeNode ln = new LexemeNode(nodes.size(), gn, li);
		gn.addLexeme(ln);
		nodes.add(ln);
	}

	/**
	 Might this be a lowercase word that is spelled in upper case
	 because of orthographic convention?
	 */
	private boolean spuriousUppercase(Arc arc) {
		int index = 0;

		/* skip over leading punctuation in a word */
		while( index < arc.word.length() && 
				(arc.word.charAt(index) == '"' || 
				arc.word.charAt(index) == '(')) {
			index++;
		}
		// check if we have only punctuation
		if (index == arc.word.length())
			return false;
		
		/** Spurious uppercase must be an upper case letter... */
		if (!Character.isUpperCase(arc.word.charAt(index))) {
			return false;
		}

		/** ... followed by a lower case letter. */
		if (arc.word.length() > index + 1 &&
				!Character.isLowerCase(arc.word.charAt(index+1))) {
			return false;
		}

		/** This is another instance of the "wordgraphs start at 0" assumption.

		 Ordinarily, this would be wrong, since the lexeme graph might start at
		 some other time point. However, at this time lg.min may not be
		 initialized, so we can't check it. Since spurious upper case only
		 occurs in written text, and weird time points occur mainly in
		 recognizer output for spoken text, I'm letting it pass here.  */
		if (arc.from == 0) {
			return true;
		}

		return lattice.prevArc(arc).isSentenceBoundary();
	}

	/**
	 * add one graphemnode to a Lexemgraph
	 * if the node is not virtual, it needs to be inserted before virtual ones and push them back
	 */
	public void addGraphemeNode(GraphemeNode gn) {
		gn.setNo(graphemeNodes.size());
		graphemeNodes.add(gn);
	}

	public List<LexemeNode> getNodes() {
		return nodes;
	}

	public List<GraphemeNode> getGNs() {
		return graphemeNodes;
	}

	public int getMax() {
		return max;
	}

	public int getMin() {
		return min;
	}

	public Object getPredictorData(String key) {
		return predictorData.get(key);
	}

	public void setPredictorData(String key, Object value) {
		predictorData.put(key, value);
	}

	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		ret.append(String.format("%n%s : %n", lattice.getId()));
		for (LexemeNode ln: nodes) {
			ret.append(ln.toString());
			ret.append("\n");
		}

		// TODO #PREDICTORS_CHUNKING chunks?
		//		if (!chunks.isEmpty()) {
		//			ret+= "chunks:\n";
		//			chunkerPrintChunks(mode, chunks);
		//		}
		return ret.toString();
	}


	/** 
	 * for incremental parsing: notify a lexemegraph that no more words are coming
	 */
	public void setComplete(boolean complete) {
		this.complete = complete;
	}

	/**
	 @brief partitions a set of lexeme nodes into equivalence classes

	 This function partitions the set of lexeme nodes of @a gn into equivalence
	 classes. The equivalence relation used is the function inputCompareLeByAtts()
	 with the argument @a features. The function returns a new List of new lists
	 of lexemes. (The latter are re-used in ConstraintNode structures, the former
	 are deallocated by cnBuildNodes().)

	 the algorithm in pseudocode:
 	 @code
	 result = [];

	 FOR each lexeme l:
	 IF l fits into one of the known classes,
	 insert l there;
	 ELSE
	 create new class [lexem];
	 insert the new class into result;
	 FI
	 ROF

	 return result.
	 @endcode
	 */
	public List<List<LexemeNode>> partitions(GraphemeNode gn, Set<Path> features) {
		List<List<LexemeNode>> result = new ArrayList<>();
		List<LexemeNode> ec; // equivalence class

		/* partitioning special gns 
		 yields the set with one class containijng a special ln: 
		 { {special ln} } */
		if (!gn.spec() ) {
			ec = new ArrayList<>();
			ec.add(gn.getLexemes().get(0));
			result.add(ec);
			return result;
		}

		/* partitioning a normal set yields sets of equivalent lexemes:
		 { { das_ART_nom,das_ART_acc }, { das_PREL_nom,das_PREL_acc} } */
		for (LexemeNode ln : gn.getLexemes()) {
			LexiconItem lexeme1 = ln.getLexeme();
			boolean foundClass = false;

			/* compare to the first item in each class */
			for (List<LexemeNode> ec2 : result) {
				LexiconItem lexeme2 = ec2.get(0).getLexeme();

				/* insert into correct class */
				if (config.getFlag(COMPACT_LVS) && lexeme1.getWord().equals(lexeme2.getWord()) &&
						!lexeme1.differByFeatures(lexeme2, features)) {
					ec2.add(ln);
					foundClass = true;
					break;
				}
			}

			/* if no fitting class was found, open a new equivalence class */
			if (!foundClass) {
				ec = new ArrayList<>();
				ec.add(ln);
				result.add(ec); 
			}
		}
		return result;
	}

	/**
	 @returns maximal ambiguity per time point

	 This function computes the maximal number of overlapping lexeme nodes for any
	 time point in @a lg. Thus it gives an upper bound (not an estimate) of the
	 average acoustical and lexical ambiguity of the graph.
	 */
	public int getWidth() {
		int result = 0;
		ArrayList<Integer> ambiguity = new ArrayList<>(max);

		for (int i = 0; i < max; i++) {
			ambiguity.add(0);
		}

		for (LexemeNode ln : nodes) {
			for (int j = ln.getArc().from; j < ln.getArc().to; j++) {
				ambiguity.set(j, ambiguity.get(j)+1);
				if (ambiguity.get(j) > result) {
					result = ambiguity.get(j);
				}
			}
		}
		return result;
	}

	/**
	 @brief may these words modify each other?

	 This function checks whether an LevelValue can exist with the dependent down
	 and a modifiee up. 
	 This is the case iff both can coexist on one path and do not overlap.
	 */
	public boolean mayModify(GraphemeNode down, GraphemeNode up) { 
		if (!up.spec()) {
			return true;
		}

		return !down.getArc().overlaps(up.getArc());
	}

	/**
	 * is the underlying sentence complete or just a sentence prefix?
	 *
	 * @return
	 */
	public boolean isComplete() {
		return complete;
	}

	public Lattice getLattice() {
		return lattice;
	}

	/**
	 @brief Does a lexemgraph contain at least one instance of a given form?

	 This function checks whether @a lg contains at least one instance of the
	 form @a form. Capitalized versions of @a form are permissible if they are
	 spurious (cf. lgSpuriousUppercase()).
	 */
	public boolean contains(String form) {
		for (GraphemeNode gn : graphemeNodes) {
			if (form.equals(gn.getArc().word)) {
				return true;
			}
			if (spuriousUppercase(gn.getArc())
					&& form.equalsIgnoreCase(gn.getArc().word)) {
				return true;
			}
		}
		return false;
	}

	/**
		 @brief returns a distance measure for two lexeme nodes

		 This function computes the logical distance between @a a and @a b, measured
		 in words. Usually this is just the corresponding element of
		 LexemGraph::distance. If either of the nodes is underspecified it is treated
		 as if it followed the latest specified lexeme node directly. Hence, the
		 return value may be greater than value in LexemGraph::distance. Two
		 underspecified lexeme nodes are considered to have distance zero.

		 The flag virtualAsNonspec determines if the position of virtual nodes
		 should be treated as underspecified
		  FALSE for lattice consistency checks like the path criterion
		  TRUE for linguistic checks like the distance function
	 */
	public int distanceOfNodes(GraphemeNode a, GraphemeNode b, boolean virtualAsNonspec) {
		int maxDist, i, maxDim, dist;

		if ( a == null || b == null) {
			logger.error("ERROR: distanceOfNodes: argument is NULL\n");
			throw new NullPointerException();
		}
		if (a.isNonspec() || (virtualAsNonspec && a.isVirtual()) ) {
			if (b.isNonspec() || (virtualAsNonspec && b.isVirtual()) )
				return 0;

			maxDim  = distance.size();
			maxDist = 0;
			for (i = 0; i < maxDim; i++) {
				if(graphemeNodes.get(i).isVirtual()) {
					continue; // virtual nodes don't push back nonspec
				}
				dist = distance.get(b.getNo()).get(i);
				if (maxDist < dist) {
					maxDist = dist;
				}
			}
			return -maxDist - 1;
		}

		if (b.isNonspec() || (virtualAsNonspec && b.isVirtual()) ) {
			maxDim = distance.size();
			maxDist = 0;
			for (i = 0; i < maxDim; i++) {
				if(graphemeNodes.get(i).isVirtual()) {
					continue; // virtual nodes don't push back nonspec
				}
				dist = distance.get(a.getNo()).get(i);
				if (maxDist < dist) {
					maxDist = dist;
				}
			}
			return maxDist + 1;
		}
		return distance.get(a.getNo()).get(b.getNo());
	}

	/**
	 @brief (re-)computes the distance matrix LexemGraph::distance

	 This function computes the distance between any two lexeme nodes in @a lg and
	 stores the result in @a lg.distance.
	 */
	protected void computeDistances() {
		
		int noOfNodes = graphemeNodes.size();
		int i, j, k, ji, ik, jk, ij;

		/* arrayNew initializes the cells with NULL */
		if (distance == null || distance.size() < noOfNodes) {
			distance = new ArrayList<>(noOfNodes);
			for(i = 0; i< noOfNodes; i++) {
				ArrayList<Integer> l = new ArrayList<>(noOfNodes);
				for(j=0; j<noOfNodes; j++) {
					l.add(0);
				}
				distance.add(l);
			}
		} else {
			for(i = 0; i< noOfNodes; i++) {
				for(j=0; j<noOfNodes; j++) {
					distance.get(i).set(j, 0);
				}
			}
		}

		GraphemeNode gn, gnNext;
		for (i = 0; i < noOfNodes; i++) {
			gn = graphemeNodes.get(i);

			/* If this gn has been deleted from the problem, leave its distances all
			 * at 0. This is necessary because the distance info does double duty as
			 * compatibility info. */
//			if (areDeletedNodes(gn.getLexemes())) {
//				continue;
//			}
			for (j = 0; j < noOfNodes; j++) {
				gnNext = graphemeNodes.get(j);
//				if (areDeletedNodes(gnNext.getLexemes())) {
//					continue;
//				}
				if (gn.getArc().to == gnNext.getArc().from) {
					distance.get(i).set(j, 1);
				}
			}
		}
		
		/* Warshall's algorithm for transitive closure */
		for (i = 0; i < noOfNodes; i++) {
			for (j = 0; j < noOfNodes; j++) {
				ji = distance.get(j).get(i);

				if (ji > 0) {
					for (k = 0; k < noOfNodes; k++) {
						ik = distance.get(i).get(k);
						jk = distance.get(j).get(k);

						if (ik > 0 && (jk == 0 || ji + ik < jk)) {
							distance.get(j).set(k, ji + ik);
						}
					}
				}
			}
		}

		for (i = 0; i < noOfNodes; i++) {
			for (j = 0; j < noOfNodes; j++) {
				ij = ik = distance.get(i).get(j);
				ji = ik = distance.get(j).get(i);

				if (ij == 0 && ji > 0) {
					distance.get(i).set(j, -ji);
				}
			}
		}
	}
	
	/**
	 Takes a set of lexeme nodes, and extends it to a complete path through the
	 graph, composed of undeleted LexemNodes.  Returns NULL if this is impossible,
	 It returns a List of lexeme nodes that
	 -# is a superset of @a nodes
	 -# corresponds to a complete path through the graph and
	 -# contains only undeleted lexeme nodes (only checked if a state is given)

	 If this is not possible, throw an exceptiond.
	 */
	public List<LexemeNode> makePathThrough(
			ConstraintNetState state, 
			List<LexemeNode> soFar) 
			throws NoPathPossibleException {

		/** We do this by simply appending arbitrary non-contradictory nodes
		 until we have bound all time points. Note that for this approach
		 to be correct, there must not be any undeleted dangling nodes in
		 the graph. This condition must have ensured by cnOptimizeNet(). */

		int noOfBoundPoints = 0;
		/* is the list of nodes self-contradictory? */
		for (LexemeNode lna : soFar) {
			noOfBoundPoints += (lna.getArc().to - lna.getArc().from);
			for (int i = soFar.indexOf(lna); i < soFar.size(); i++) {
				LexemeNode lnb = soFar.get(i);
				if (lna.overlaps(lnb)) {
					throw new NoPathPossibleException("Inconsisten path given!");
				}
			}
		}

		/* choose arbitrary nodes until all time points are bound */
		boolean okay;
		ArrayList<LexemeNode> result = new ArrayList<>(soFar);
		for (LexemeNode lna : nodes) {
			okay = true;
			if (state != null && state.isDeletedLn(lna)) {
				continue;
			}
			for (LexemeNode lnb : result) {
				if (lna == lnb || lna.overlaps(lnb)) {
					okay = false;
					break;
				}
			}
			if (okay) {
				result.add(lna);
				noOfBoundPoints += (lna.getArc().to - lna.getArc().from);
			}
		}

		if (noOfBoundPoints != max - min ) {
			throw new NoPathPossibleException("Can't find a path of correct length!");
		}

		return result;
	}

	public GraphemeNode getNil() {
		return nil;
	}
	
	public GraphemeNode getNonspec() {
		return nonspec;
	}

	/** returns the last real, i.e. non virtual gn */
	public GraphemeNode getLastGN() {
		return graphemeNodes.get(graphemeNodes.size()-1);
	}

	public Configuration getConfig() {
		return config;
	}

	/**
	 * Extends the given lexemgraph with the given arc
	 */
	public void extend(String word, boolean complete) {
		setComplete(complete);
		
		Arc arc = new Arc(max, max+1, max, word, 1.0);
		
		// extending lattice
		lattice.addArc(arc);
		lattice.scan(complete);

		// extending the lg, stuff like the max field will be taken care of here, too
		lgNewIter(arc);
		
		computeDistances();
	}
	
	public List<LexemeNode> lexicalize(List<LevelValue> lvs) {
		List<LexemeNode> ret   = new ArrayList<>(max - min);
		List<Boolean> suitable = new ArrayList<>();
		for(int i = 0; i < nodes.size(); i++) {
			suitable.add(true);
		}
		
		for(LevelValue lv : lvs) {
			for(LexemeNode ln : lv.getDependentGN().getLexemes()) {
				if(!lv.getDependents().contains(ln)) {
					suitable.set(ln.getNo(), false);
				}
			}
			
			if(!lv.getRegentGN().spec())
				continue;
			for(LexemeNode ln : lv.getRegentGN().getLexemes()) {
				if(!lv.getRegents().contains(ln)) {
					suitable.set(ln.getNo(), false);
				}
			}	
		}
		
		for(GraphemeNode gn : graphemeNodes) {
			boolean found = false;
			for(LexemeNode ln : gn.getLexemes()) {
				if(suitable.get(ln.getNo())) {
					ret.add(ln);
					found = true;
					break;
				}
			}
			
			// not found
			if(!found) {
				return null;
			}
		}
		
		return ret;
	}

}
