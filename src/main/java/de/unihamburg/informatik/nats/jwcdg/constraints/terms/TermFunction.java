package de.unihamburg.informatik.nats.jwcdg.constraints.terms;

import java.util.ArrayList;
import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.AccessContext;
import de.unihamburg.informatik.nats.jwcdg.constraints.functions.Function;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * A term evaluated by evaluating a function
 * 
 * @author the CDG-Team
 *
 */
public class TermFunction extends Term {

	private Function function;
	private List<Term> args;

	
	public TermFunction(Function f) {
		this.function = f;
		this.isContextSensitive = f.isContextSensitive();
	}
	
	public TermFunction(Function f, List<Term> args) {
		this(f);
		this.args = args;
		for (Term t : args) {
			if(t.isContextSensitive)
				this.isContextSensitive = true;
		}
	}
	
	public TermFunction(Function f, Term arg) {
		this(f);
		this.args = new ArrayList<>(1);
		args.add(arg);
		if(arg.isContextSensitive)
			this.isContextSensitive = true;
	}
	
	public TermFunction(Function f, Term arg1, Term arg2) {
		this(f);
		this.args = new ArrayList<>(2);
		this.args.add(arg1);
		this.args.add(arg2);
		if(arg1.isContextSensitive || arg2.isContextSensitive)
			this.isContextSensitive = true;
	}
	
	@Override
	public void analyze(AccessContext context, Grammar grammar) {
		for(Term t : args) {
			t.analyze(context, grammar);
		}
	}

	@Override
	public AVM eval(LexemeGraph lg, Context context,
			LevelValue[] binding) {
		return function.eval(args, context, lg, binding);
	}
	
	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder(function.toString());
		ret.append("(");
		boolean first = true;
		for(Term t : args) {
			if(!first) {
				ret.append(", ");
			}
			first = false;
			ret.append(t);
		}
		ret.append(")");
		return ret.toString();
	}
	
	@Override
	public boolean check() {
		int n= args.size();
		if(n < function.arity || (function.maxArity != -1 && n > function.maxArity) )
			return false;
		for(Term t: args) {
			if(!t.check()) {
				return false;
			}
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((args == null) ? 0 : args.hashCode());
		result = prime * result
				+ ((function == null) ? 0 : function.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof TermFunction))
			return false;
		TermFunction other = (TermFunction) obj;
		if (args == null) {
			if (other.args != null)
				return false;
		} else if (!args.equals(other.args))
			return false;
		if (function == null) {
			if (other.function != null)
				return false;
		} else if (!function.equals(other.function))
			return false;
		return true;
	}
}
