package de.unihamburg.informatik.nats.jwcdg.parse;

import java.io.Serializable;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Information about how a parse was generated
 * 
 * @author the CDG-Team
 *
 */
public class ParseInfo implements Serializable{

	private static final long serialVersionUID = 1L;
    /** additional information */
	private SortedMap<String,String> moreInfo;
	/** With which search strategy? */
	private String  searchStrategy;    
	/** How long did the evaluation take?   */
	private long      evaluationTime;
	public ParseInfo() {
		this.moreInfo = new TreeMap<>();
	}

	public String getSearchStrategy() {
		return searchStrategy;
	}

	public void setSearchStrategy(String searchStrategy) {
		this.searchStrategy = searchStrategy;
	}

	public long getEvaluationTime() {
		return evaluationTime;
	}

	public void setEvaluationTime(long evaluationTime) {
		this.evaluationTime = evaluationTime;
	}

	/**
	 * Sets a K-V pair so it can be printed later on
	 * @param key
	 * @param value
	 */
	public void setEntry(String key, String value) {
		moreInfo.put(key, value);
	}

	/**
	 * gets the information that is stored for the given key
	 * @param key
	 * @return the corresponding value
	 */
	public String getEntry(String key) {
		return moreInfo.get(key);
	}
	
	/**
	 * copies all Information from other, only information not present in other
	 * is kept.
	 * @param other
	 */
	public void mergeInfo(ParseInfo other) {
		this.searchStrategy = other.searchStrategy;
		this.evaluationTime = other.evaluationTime;
		this.moreInfo.putAll(other.moreInfo);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("Search Strategy : ");
		result.append(this.searchStrategy);
		result.append("\nProcessing Time : ");
		result.append(Long.toString(evaluationTime));
		result.append("\n");
		for (Entry<String, String> e : moreInfo.entrySet()) {
			result.append(e.getKey());
			result.append(" : ");
			result.append(e.getValue());
			result.append("\n");
		}
		return result.toString();
	}
}
