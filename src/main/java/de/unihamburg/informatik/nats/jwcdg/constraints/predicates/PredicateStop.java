package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.incrementality.virtualNodes.VirtualLexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * returns TRUE if lattice ends with word
 * 
 * @author the CDG-Team
 *
 */
public class PredicateStop extends Predicate {

	public PredicateStop(Grammar g) {
		super("stop", g, false, 1, false);
	}

	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {

		AVM val = args.get(0).eval(lg, context, binding);
		AVMLexemNode lnval;
		if (val instanceof AVMLexemNode) {
			lnval = (AVMLexemNode) val;
		} else {
			throw new PredicateEvaluationException("ERROR: argument of predicate `stop' not a lexeme node");
		}
		LexemeNode ln = lnval.getLexemeNode();


		if(!ln.spec() || ln.isVirtual()) {
			return false;
		}

		if(lg instanceof VirtualLexemeGraph) {
			return ln.getArc().to == ((VirtualLexemeGraph)lg).getVStartId(); //-1 for last nonvirtual, +1 for to instead of from
		} else {
			return ln.getArc().to == lg.getMax();
		}
	}

}
