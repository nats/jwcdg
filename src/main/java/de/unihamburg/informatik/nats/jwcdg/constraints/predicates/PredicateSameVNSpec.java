package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.incrementality.virtualNodes.VirtualLexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;


/**
 * are #1 and 2# virtual nodes based on the same virtual node specification?
 */
public class PredicateSameVNSpec extends Predicate {

	public PredicateSameVNSpec(Grammar g) {
		super("samevnspec", g, false, 2, false);
	}

	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {
		
		if(!(lg instanceof VirtualLexemeGraph)) {
			return false;
		}

		AVM val1 = args.get(0).eval(lg, context, binding);
		AVM val2 = args.get(1).eval(lg, context, binding);

		if (! (val1 instanceof AVMLexemNode)) {
			throw new PredicateEvaluationException("1st argument of predicate `sameVnSpec' not a lexeme node");
		}

		if (! (val2 instanceof AVMLexemNode)) {
			throw new PredicateEvaluationException("2nd argument of predicate `sameVnSpec' not a lexeme node");
		}

		LexemeNode ln1 = ((AVMLexemNode)val1).getLexemeNode();
		LexemeNode ln2 = ((AVMLexemNode)val2).getLexemeNode(); 

		if(!ln1.isVirtual() || !ln2.isVirtual()) {
			return false;
		}

		VirtualLexemeGraph vlg = (VirtualLexemeGraph) lg;
		
		return vlg.getSpecOf(ln1).equals(vlg.getSpecOf(ln2));
	}
}
