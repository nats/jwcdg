package de.unihamburg.informatik.nats.jwcdg.constraints.terms;

import java.util.Set;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.VarInfo;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

public class TermBottomPeek extends TermPeek {

	public TermBottomPeek(Path path, VarInfo vi) {
		super(path, vi);
	}

	@Override
	protected Set<Path> selectFeatureSet(int i, Grammar g) {
		return g.getDownFeatures(i);
	}
	
	@Override
	public AVM eval(LexemeGraph lg, Context context, LevelValue[] binding) {
		LexemeNode ln = binding[vindex].getDependents().get(0);
		return ln.getFeature(path);
	}

	@Override
	protected String sign() {
		return "@";
	}
}
