package de.unihamburg.informatik.nats.jwcdg.transform;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNetState;
import de.unihamburg.informatik.nats.jwcdg.lattice.Lattice;

public interface SolverFactory {

	ProblemSolver generateSolver(Lattice lat);
	
	ProblemSolver generateSolver(ConstraintNetState state);

	ProblemSolver generateSolver(ConstraintNetState state, Analysis a);
}
