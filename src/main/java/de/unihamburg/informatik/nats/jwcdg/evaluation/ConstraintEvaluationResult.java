package de.unihamburg.informatik.nats.jwcdg.evaluation;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraints.Constraint;
import de.unihamburg.informatik.nats.jwcdg.transform.ConstraintViolation;

/**
 * Contains all relevant information of evluating a single constraint on a set of levelvalues matching its signature.
 * the information contained is:
 *   - which constraint 
 *   - which levelvalues
 *   - do the lvs violated the constraint?
 *   - what is the resulting penalty?
 *   
 * if violated, this can be transformed into a ConstraintViolation 
 * 
 * @author the CDG-Team
 *
 */
public class ConstraintEvaluationResult {
	private Constraint c;
	private LevelValue lva, lvb, lvc;
	private boolean violated;
	private double penalty;
	
	/**
	 * 
	 * @param c
	 * @param lvs
	 * @param violated
	 * @param penalty
	 */
	public ConstraintEvaluationResult(Constraint c, List<LevelValue> lvs,
			boolean violated, double penalty) {
		this.c = c;
		this.lva = lvs.get(0);
		if(lvs.size() > 1) {
			this.lvb = lvs.get(1);
			if(lvs.size() > 2) {	
				this.lvc = lvs.get(2);
			}
		}
		this.violated = violated;
		this.penalty = penalty;
	}

	public ConstraintEvaluationResult(Constraint c, LevelValue lva,
			boolean violated, double penalty) {
		this.c = c;
		this.lva = lva;
		this.violated = violated;
		this.penalty = penalty;
	}

	public ConstraintEvaluationResult(Constraint c, LevelValue lva, LevelValue lvb,
			boolean violated, double penalty) {
		this.c = c;
		this.lva = lva;
		this.lvb = lvb;
		this.violated = violated;
		this.penalty = penalty;
	}
	
	public boolean isViolated() {
		return violated;
	}
	
	public double getScore() {
		return penalty;
	}		

	public ConstraintViolation toCV() {
		if(!violated) 
			return null;
		return new ConstraintViolation(c, lva, lvb, lvc, penalty);
	}
	
	@Override
	public String toString() {
		if(violated)
			return String.format("%s violated(%1.5f)", c.getId(), penalty);
		else
			return String.format("%s ok", c.getId());	
	}
	
}
