package de.unihamburg.informatik.nats.jwcdg.constraints.terms;


import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

public class TermString extends Term {
	private AVMString s;
	
	public TermString(String s) {
		this.s = new AVMString(s);
	}

	@Override
	public AVM eval(LexemeGraph lg, Context context,
			LevelValue[] binding) {
		return s;
	}
	
	public String getString() {
		return s.getString();
	}
	
	@Override
	public String toString() {
		return s.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((s == null) ? 0 : s.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof TermString))
			return false;
		TermString other = (TermString) obj;
		if (s == null) {
			if (other.s != null)
				return false;
		} else if (!s.equals(other.s))
			return false;
		return true;
	}
}
