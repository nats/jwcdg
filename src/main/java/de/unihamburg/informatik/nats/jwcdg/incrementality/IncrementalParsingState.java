package de.unihamburg.informatik.nats.jwcdg.incrementality;

import de.unihamburg.informatik.nats.jwcdg.parse.Parse;

public interface IncrementalParsingState {
	Parse getParse();
}
