package de.unihamburg.informatik.nats.jwcdg.frobbing;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNetState;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraints.Level;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.lattice.Arc;
import de.unihamburg.informatik.nats.jwcdg.lattice.Lattice;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;
import de.unihamburg.informatik.nats.jwcdg.parse.ParseInfo;
import de.unihamburg.informatik.nats.jwcdg.transform.Analysis;
import de.unihamburg.informatik.nats.jwcdg.transform.Badness;

/**
 * build a combinedFrobber for to subproblems and recombine.
 * 
 * This Class implements the transformation strategy with the same name.
 * It will first call subFrob() on partial problems delimited by
 * punctuation, then combined() on the entire problem. If a time limit is
 * in force, half of the allowed time will be alloted to the search within
 * subproblems and half to the final combined search.
 */
public class DynamicFrobber extends AbstractFrobber {
	
	private static Logger logger = LogManager.getLogger(DynamicFrobber.class);
	
	private static final String DYNAMIC_MIN_SIZE         = "dynamicMinSize";
	private static final String DYNAMIC_MAX_SIZE         = "dynamicMaxSize";
	private static final String DYNAMIC_BOUNDARIES       = "dynamicBoundaries";
	private static final Object PUNC_BOUNDARIES_METHOD   = "punc";
	private static final Object CHUNKS_BOUNDARIES_METHOD = "chunks";
	private static final String EVERY_N_BOUNDARY         = "every";
	private static final String EXACT_N_M_O_BOUNDARIES_METHOD = "exact";
	private static final String STRICT_SUBPROBLEMS       = "strictSubproblems";
	private static final String DYNAMIC_FREEZING_THRESHOLD = "dynamicFreezingThreshold";
	
	
	public DynamicFrobber(ConstraintNetState state, Configuration config,
			GrammarEvaluator evaluator, double pressure) {
		super(state, config, evaluator, pressure);
	}
	
	public DynamicFrobber(ConstraintNetState state, Analysis a,
			Configuration config, GrammarEvaluator evaluator, double pressure) {
		super(state, a, config, evaluator, pressure);
	}
	
	public DynamicFrobber(ConstraintNetState state, Configuration config,
			GrammarEvaluator evaluator, double pressure, Parse parse) {
		super(state, config, evaluator, pressure, parse);
	}
	
	@Override
	public boolean solve() {
		int time;

		/* If the lattice branches, call combined() instead */
		if(net.getLexemeGraph().getLattice().branches()) {
			logger.warn("lattice has branches, using `combined' instead.");
			return combined(timeLimit, this.best);
		}

		/* break sentence into subclauses */
		List<Integer> clauses = findSubproblems();
		if(clauses== null || clauses.isEmpty()) 
			return false;

		/* if there aren't any, just call combined() */
		if(clauses.size() == 2) {
			logger.info("no subclauses present, using `combined' instead.\n");
			return combined(timeLimit, this.best);
		}

		/* turn all LVs that cross boundaries to NIL, so they won't
	     satisfy nonlocal constraints across subproblems. */
		if(config.getFlag(STRICT_SUBPROBLEMS)) {
			current.separateRanges(clauses);
			best = new Analysis(current);
		}

		/* call frobbing on each subproblem in turn */
		int i = clauses.get(0);
		for(int j: clauses.subList(1, clauses.size())) {
			
			time = timeLimit * (j - i) / (net.getMaxTimepoint() - net.getMinTimepoint()) / 2;

			// 0 is a sentinel value that means `no time limit'. Therefore we
	        // absolutely must not allow rounding to convert 0.3 to 0.
			if(0 == time && timeLimit > 0) {
				time = 1;
			}
			
			subFrob(i, j, time);
			restoreSavedState();
			i = j;
		}

		/* now try to join the fragments */
		current.judge(evaluator);
		best.judge(evaluator);
		restoreSavedState();
		freezeSubtrees(current, config.getDouble(DYNAMIC_FREEZING_THRESHOLD));

		/* recalculate score for the entire structure */
		current.judge(evaluator);
		best.judge(evaluator);

//		/* If the entire structure is now valued at >0, we count this as
//	     the first `soft' solution. There might actually have been a previous
//	     solution that had a soft score globally, but since we do not do global
//	     evaluation in phase 1, we could not have detected that. */		
//		try {
//			newMaximum(current);
//		} catch (InvalidNetStateException e) {
//			// This point should not be reachable,
//			// since the pressure would already have been reduced 
//			// when building the initial netstate
//			e.printStackTrace();
//		}

		/* give frobbing another stab at the entire problem */
		time = timeLimit / 2;
		if(time == 0 && timeLimit > 0) { // 0 would mean: unlimited
			time = 1;
		}
		
		return combined(time, this.best);
	}

	/**
	 * build a combined frobber for out netstate and solve it with that method
	 * 
	 * @return
	 */
	private boolean combined(int timeLimit, Analysis a) {
		CombinedFrobber combined = new CombinedFrobber(new ConstraintNetState(netState), 
				a, config, evaluator, globalMinimum);
		combined.registerObservers(getObservers());
		combined.setTimeLimit(timeLimit);
		boolean ret = combined.solve();
		solution = combined.getSolution();
		ParseInfo info = solution.getInfo();
		info.setEvaluationTime(System.currentTimeMillis() - stats.getStartTime());
		info.setSearchStrategy("Frobbing");
		info.setEntry("Frobbing Method", this.methodName());
		
		best     = combined.getBest();
		current  = new Analysis(best);
		
		return ret;
	}

	/**
	 * Return list of time points that constitute subproblems to be searched in
	 * isolation.
	 * 
	 * For instance, if the sentence `Steuermann, laß die Wacht!' carries the
	 * expected time information (time points 0 through 6) and only commas are
	 * considered indicators of subproblems, the list [0,2,6] is returned. Each
	 * element is either the first time point in the sentence, or the end of a
	 * subproblem.
	 *
	 * The behaviour of this function is controlled by the variable
	 * dynamicBoundaries.
	 */
	private List<Integer> findSubproblems() {
		List<Integer> result = new ArrayList<>();
		Lattice lat = net.getLexemeGraph().getLattice();
		result.add(net.getLexemeGraph().getMin());

		int thisTp, lastTp;

		lastTp = net.getLexemeGraph().getMin();

		String method = config.getString(DYNAMIC_BOUNDARIES);

		/* method `punc' (default): break at punctuation */
		if(     method.equals("") || 
				method.equals(PUNC_BOUNDARIES_METHOD)) {
			for(Arc arc : lat.getArcs()) {
				thisTp = arc.to;
				if(thisTp - lastTp < config.getInt(DYNAMIC_MIN_SIZE)) 
					continue;
				if(     thisTp - lastTp >= config.getInt(DYNAMIC_MAX_SIZE) ||
						(arc.word.length() == 1 && ",:;-()".contains(arc.word))) {
					result.add(thisTp);
					lastTp = thisTp;
				}
			}
		}
		/* method `chunks': break at chunk boundaries */
		else if(method.equals(CHUNKS_BOUNDARIES_METHOD)) {
			// TODO #CHUNKS
			throw new RuntimeException("Chunks not implemented yet!"); 
			//			for(l = P.lg.chunks; l != NULL; l = l.next) {
			//				Chunk c = l.item;
			//				this = c.to.arc.to;
			//				if(this < P.maxTimepoint) {
			//					result = listAppendElement(result, (Pointer)this);
			//				}
			//			}
		}

		/* method `everyN': break at regular intervals */
		else if(method.startsWith(EVERY_N_BOUNDARY)) {
			try {
				int i = Integer.parseInt(method.substring(5));
				thisTp = lastTp;
				while(thisTp+i < net.getLexemeGraph().getMax()) {
					thisTp += i;
					result.add(thisTp);
				}
			} catch (NumberFormatException e) {
				logger.error("Error in parsing config entry " + DYNAMIC_BOUNDARIES + "\nexpected a number after "+ EVERY_N_BOUNDARY);
			}
		}

		/* method `exactN,M,O': break at specified places */
		else if(method.startsWith(EXACT_N_M_O_BOUNDARIES_METHOD)) {
			// TODO #CHUNKS
			throw new RuntimeException("Chunks not implemented yet!");

			//			String positions=dynamicBoundaries+5;
			//			while(strlen(positions) &&
			//					isdigit((int)positions[0])) {
			//				sscanf(positions,"%d",&this);
			//				if(this > last &&
			//						this > P.minTimepoint &&
			//						this < P.maxTimepoint) {
			//					result = listAppendElement(result, (Pointer)this);
			//				}
			//				/* skip numbers and commas */
			//				positions += strspn(positions,"0123456789");
			//				positions += strspn(positions,",");
			//			}
		}

		/* unknown method: complain and fail */
		else {
			logger.error(String.format("unknown fragmentation method `%s' give nfor key %s in the properties",
					method, DYNAMIC_BOUNDARIES));
			return null;
		}

		result.add(net.getLexemeGraph().getMax());
		return result;
	}
	
	/**
	 * Maybe delete unselected LVs permanently.
	 *
	 * This function calls freezeDomain() on all domains of P that do not cause
	 * serious conflicts. 
	 * What conflict accounts as 'serious' is defiend by the given threshold
	 */
	public void freezeSubtrees(Analysis a, double threshold) {
		Level mainlevel = net.getGrammar().getMainLevel();
		List<Badness> byDomain = a.analyzeSlots();

		logger.info("Candidates for reassembly:");

		/* Good candidates for attachments are LVS on the mainlevel
	     that point to NIL and have bad scores. */
		for (int i = 0; i < a.getSlots().size(); i++) {
			Badness b = byDomain.get(i);
			LevelValue lv;
			if(!a.slotActive(i) || (lv=a.getSlots().get(i)) == null ||
					lv.getLevel() != mainlevel) {
				continue;
			}
			if(b.getScore() < 0.2) {
				// serious conflict in this slot,
				// no freezing
				logger.info(lv.toString());
//				cdgPrintf(CDG_INFO, "@%d: ", i);
//				lvPrint(CDG_INFO,lv,NULL,0);
//				cdgPrintf(CDG_INFO, "\n");
			} else {
				netState.freezeDomain(a, i);
			}
		}

		assert netState.checkAmbiguity();
		
		rememberState();
	}
	
	/** ----------------------------------------------------------------------
	   Apply combined frobbing to time points FROM through TO.

	   Invokes combined() on the specified subrange of timepoints of P. It sets
	   P.current.subStart and P.current.subStop and temporarily deletes all
	   LVs that cross the borders of this subproblem.

	 */
	private void subFrob(int from, int to, int ms) {

		StringBuilder s = new StringBuilder("subFrob:`");
		for(int i = from; i < to; i++) {
			s.append(" ");
			s.append(net.getLexemeGraph().getGNs().get(i).getArc().word);
		}
		s.append("'");
		logger.info(s.toString());

		if(config.getFlag(STRICT_SUBPROBLEMS)) {
			/* temporarily delete all LVs that leave the specified zone. */
			for(int i = net.getNoOfLevels() * from;
					i < net.getNoOfLevels() * to;
					i++) {
				for(LevelValue lv : netState.getDomain(i)) {
					if(lv!=null && lv.getRegentGN().spec()) {
						if( lv.getRegentGN().getArc().from >= to ||
								lv.getRegentGN().getArc().from < from) {
							netState.deleteLv(lv);
						}
					}
				}
			}
		}

		/* remove deprecated LVs from the Analysis */
		for(int i = net.getNoOfLevels() * from;
				i < net.getNoOfLevels() * to;
				i++) {
			LevelValue lv = current.slotFiller(i);
			if( lv != null && lv.getRegentGN().spec()) {
				if( lv.getRegentGN().getArc().from >= to ||
						lv.getRegentGN().getArc().from < from) {
					current.redirect(i, -1);
				}
			}
		}

		//best = current.clone();

		/* (pretty) normal frobbing */
		current.restrict(from, to);
		/* This has almost certainly changed the score of the structure. */
		current.judge(evaluator);
		//best.judge(evaluator);
		combined(ms, current);
		
		/* now judge the best Analysis so it won't spook
	     the check in combined() with its good score. */
		current.unrestrict();
		best.unrestrict();
		current.judge(evaluator);
	}
	
	@Override
	public String methodName() {
		return DynamicFactory.NAME;
	}
}
