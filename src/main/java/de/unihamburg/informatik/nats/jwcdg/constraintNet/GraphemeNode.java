package de.unihamburg.informatik.nats.jwcdg.constraintNet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.incrementality.virtualNodes.VirtualNodeSpec;
import de.unihamburg.informatik.nats.jwcdg.lattice.Arc;
import de.unihamburg.informatik.nats.jwcdg.predictors.Predictor;

public class GraphemeNode implements Serializable {
	
    private static final long serialVersionUID = 1L;
    private int no;                             /* index in lg->graphemnodes */
	private Arc arc;                            /* arc in word lattice */
	private List<LexemeNode> lexemes;           /* list of disambiguated lexeme nodes */
	private Map<String, Map<String, AVM>> predictions;
	
	// TODO #PREDICTORS_CHUNIKING 
	//private Chunk chunk;                      /* to which chunk do we belong */
	private boolean isVirtual;                  /* if true, the gn does not correspond to a word from the lattice */
	
	/**
	 * don't construct more than one GraphemeNode per Arc,
	 * since GraphemeNodes are compared by Arc equality
	 */
	public GraphemeNode(Arc arc) {
		this.arc = arc;
		this.lexemes = new ArrayList<>();
		this.predictions = new HashMap<>();
	}

	/**
	 * generate a virtual graphemeNode from a virtualNodeSpecification 
	 */
	public GraphemeNode(VirtualNodeSpec spec, int tp, int n) {
		isVirtual=true;
		arc = new Arc(tp, tp+1, tp, spec.getName()+n, 1.0);
		lexemes = new ArrayList<>();

		no = tp;
		predictions = new HashMap<>();
	}
	
	/**
	 *  index in lg.getGNs() 
	 */
	public int getNo() {
		return no;
	}

	public Arc getArc() {
		return arc;
	}
	
	public boolean isVirtual() {
		return isVirtual; 
	}
	
	public boolean isSpecified() {
		return true;
	}
	
	/**
	 * whether this GN is different from nil and nonspec
	 * 
	 * @return
	 */
	public boolean spec() {
		return isSpecified();
	}
	
	public boolean isNonspec() {
		return false;
	}

//	public void setLexemeGraph(LexemeGraph lg) {
//		this.lexemegraph = lg;
//	}

	public void setNo(int no) {
		this.no = no;
	}

	public void addLexeme(LexemeNode ln) {
		lexemes.add(ln);
	}

	public List<LexemeNode> getLexemes() {
		return lexemes;
	}
	
	@Override
	public String toString() {
		return String.format("%d - %s /%d", no, lexemes.get(0).toString(), lexemes.size());
	}

	public boolean isNil() {
		return false;
	}

	/**
	 * returns the predicted score for a given predictor (by name) and a list of keys
	 * if no prediction for the given parameters is available, null is returned
	 */
	public AVM readPrediction(String predictorname, ArrayList<String> keys) {
		String key = Predictor.makeKey(keys);
		if(predictions.containsKey(predictorname)) {
			return predictions.get(predictorname).get(key);
		}
		return null;
	}
	
	public void setPredictionFor(String predictorName, Map<String, AVM> map) {
		predictions.put(predictorName, map);
	}
	
	/**
	 * gets the prediction map for the given predictor
	 * @param predictorName
	 * @return
	 */
	public Map<String, AVM>getPredictionFor(String predictorName) {
		return predictions.get(predictorName);
	}
	
	public void pushArc() {
		if(!isVirtual)
			throw new RuntimeException("Can't push nonvirtual node!");
		
		arc = new Arc(arc.from+1, arc.to+1, arc.no+1, arc.word, arc.getPenalty());
	}

	/**
	 * returns a set of all values for the given path occurring in at least one lexemenode of this gn
	 * AVMError values are ignored
	 */
	public Set<String> collectPathValues(Path p) {
		Set<String> ret = new HashSet<>(lexemes.size());
		for(LexemeNode ln : lexemes) {
			AVM v= ln.getFeature(p);
			if(v instanceof AVMError) {
				continue;
			}
			ret.add(v.toString());
		}
		
		return ret;
	}
	
	/**
	 * TODO #VN not implemented yet ,defaults to true 
	 * not strictly needed but we might want to not even try words with incompatible PoS
	 * 
	 * do lexemes exist for the given graphemnodes that
	 * share certain characteristics?
	 */
	public boolean haveCompatibleLexemes(GraphemeNode gnb) {
	   return true;  
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		
		if (obj instanceof GraphemeNode) {
			GraphemeNode other = (GraphemeNode) obj;
			
			if(this.isNil() != other.isNil())
				return false;
			
			if(this.isNonspec() != other.isNonspec())
				return false;
			
			// we assume there to be only one gn per arc
			return this.arc.equals(other.arc);
			
		} else {
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		return arc.hashCode();
	}
}
