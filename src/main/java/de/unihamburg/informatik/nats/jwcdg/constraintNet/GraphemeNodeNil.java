package de.unihamburg.informatik.nats.jwcdg.constraintNet;

import java.util.ArrayList;
import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.lattice.Arc;

public class GraphemeNodeNil extends GraphemeNode {
	
	List<LexemeNode> nillns;
	
	public GraphemeNodeNil() {
		super(null);
		nillns = new ArrayList<>();
		nillns.add(new LexemeNodeNil(this));
	}
	
	@Override
	public boolean isSpecified() {
		return false;
	}
	
	@Override
	public boolean isNil() {
		return true;
	}
	
	@Override
	public void addLexeme(LexemeNode ln) {
		throw new RuntimeException("I'm nil, you can't add a lexeme node to me!");
	}
	
	@Override
	public Arc getArc() {
		throw new RuntimeException("I'm nil, I have no arc!");
	}
	
	@Override
	public List<LexemeNode> getLexemes() {
		return nillns;
	}
	
	@Override
	public int getNo() {
		return -1;
	}
	
	@Override
	public void setNo(int no) {
		throw new RuntimeException("I'm nil, don't change my number!");
	}
	
	@Override
	public String toString() {
		return "nil";
	}
	
	
}
