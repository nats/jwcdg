/**
 *
 */
package de.unihamburg.informatik.nats.jwcdg;

import java.io.IOException;

import de.unihamburg.informatik.nats.jwcdg.evaluation.CachedGrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;

/**
 * Class for accessing jwcdg's scoring mechanisms
 * @author Christine Köhn
 *
 */
public class Scoring {
	Parser parser;

	/**
	 * @param pathToConfig path to propreties file for jwcdg
	 * @throws IOException
	 */
	public Scoring (String pathToConfig) throws IOException {
		parser = JWCDG.makeParser(pathToConfig);
	}

	/**
	 * Dummy method for testing
	 * @param d
	 * @return d (same as argument)
	 */
	public double scoreTreeDummy(double d){
		return d;
	}


	/**
	 * Returns score for Parse
	 * @param conll Parse represented in CoNLL data format
	 * @return
	 */
	public double scoreTree(String conll) {
		Parse parse = Parse.fromConll(conll);
		System.out.println(parse.print());
		DecoratedParse decoratedParse = parse.decorate(
				new CachedGrammarEvaluator(parser.getGrammar(), parser
						.getConfig()), parser.getLexicon(), parser.getConfig());
		return decoratedParse.getBadness().getScore();
	}
}
