package de.unihamburg.informatik.nats.jwcdg.constraints.formulas;

import java.util.ArrayList;
import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.AccessContext;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

public class FormulaOr extends Formula {
	private List<Formula> subformulas;
	
	public FormulaOr() {
		this.subformulas = new ArrayList<>();
	}
	
	@Override
	public boolean evaluate(LevelValue[] binding, Context context, LexemeGraph lg) {
		for (Formula f : this.subformulas) {
			if (f.evaluate(binding, context, lg)) {
				return true;
			}
		}
		return false;
	}
	

	public void addSubFormula(Formula f) {
		subformulas.add(f);
		this.isContextSensitive = this.isContextSensitive || f.isContextSensitive;
	}

	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		boolean first = true;
		for(Formula f: subformulas) {
			if(!first)
				ret.append("|");
			first = false;
			ret.append(f.toString());
		}
		return ret.toString();
	}

	@Override
	/** the constraints imposed by disjunctions are valid only within
	    the subformula, so we have to separate what we already know about
	    the context and what the subformula contributed itself. We can,
	    however, utilize the combined Context of all subformulas to
	    constrain our own Context further. */
	public void analyze(AccessContext context, Grammar g) {
		AccessContext aggregate = new AccessContext(context);
		for (Formula f : subformulas) {
			AccessContext subcontext = new AccessContext(context);
			f.analyze(subcontext, g);
			aggregate.or(subcontext);
		}
		context.and(aggregate);
	}

	@Override
	public void collectFeatures(List<Path> sofar) {
		for(Formula f: subformulas) {
			f.collectFeatures(sofar);
		}
	}
	
	@Override
	public boolean check() {
		for(Formula f : subformulas) {
			if(!f.check())
				return false;
		}
		return true;
	}
	
	@Override
	public void collectHasInvocations(List<Formula> akku) {
		for(Formula f : subformulas) {
			f.collectHasInvocations(akku);
		}
	}
}
