package de.unihamburg.informatik.nats.jwcdg.constraints.predicates;

import java.util.List;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.Constraint;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.Term;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.TermPeek;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Hierarchy;
import de.unihamburg.informatik.nats.jwcdg.transform.Context;

/**
 * 
 * 
Is the word #1 subordinated with label #2?

`is' checks whether the specified word is subordinated with the
specified label.

The first parameter is a lexeme node in the graph, and
the second parameter is the label that it should carry.

If the second argument is the name of a unary constraint, its
formula will be used for matching instead of the edges' labels.

If there is a third argument, the third is a hierarchy which should be
used for label matching. Thus, a constraint can say `is(X@id,
OBJC_NEB, Labels)', and (assuming a fitting `Labels' hierarchy) either
OBJC or NEB will satisfy the predicate.

If there is a fourth argument, the search is conducted recursively. The
value of the argument is a node in the previously specified hierarchy.
All the labels which are subsumed by this node are skipped during the
search (unless a NIL edge interferes). In other words, specifying four
parameters effectively asks, `Is word #1 *ultimately* labelled #2?',
where the fourth parameter specifies which intermediate dependencies
should be considered irrelevant.
 */
public class PredicateIs extends Predicate { 

	public PredicateIs(Grammar g) {
		super("is", g, true, 2, 4, false);
	}

	@Override
	public boolean eval(List<Term> args, LevelValue[] binding,
			LexemeGraph lg, Context context, int formulaID) {

		Boolean result;
		LevelValue lv;
		String label;
		Constraint c;
		int noOflevels = grammar.getNoOfLevels();
		int i;
		int levelno;
		String hname;
		Hierarchy h = null;
		String labels = null;
		int counter;

		/* if there is no context at all, we can only assume that
	     the edge might be found if we had it. */
		if(context == null) {
			return true;
		}

		/* read lexemeNode */
		Term t = args.get(0);
		TermPeek peek1;
		if (t instanceof TermPeek) {
			peek1 = (TermPeek) t;
		} else {
			throw new PredicateEvaluationException("1st argument of predicate `is' is not a lexeme node access!");
		}
		levelno = binding[peek1.getVarIndex()].getLevel().getNo();
		AVM val1 = t.eval(lg, context, binding);
		if (! (val1 instanceof AVMLexemNode) ) {
			throw new PredicateEvaluationException("Argument of predicate `is' not a lexeme node");
		}
		LexemeNode ln = ((AVMLexemNode)val1).getLexemeNode(); 
		if(!ln.spec()) {
			return false;
		}

		/* read second parameter: label */
		AVM val2 = args.get(1).eval(lg, context, binding);
		if(val2 instanceof AVMString) {
			label = ((AVMString)val2).getString();
		} else { /* complain */
			throw new PredicateEvaluationException("type mismatch (2rd arg) for `is'");
		}
		/* if the string is the name of a unary constraint,
		     use this constraint instead of label match  */
		c = grammar.findConstraint(label);
		if(c != null && c.getVars().size() != 1) {
			c = null;
		}
		
		/* maybe read a third parameter and find Hierarchy to use */
		if(args.size() >= 3) {
			AVM val3 = args.get(2).eval(lg, context, binding);
			if (val3 instanceof AVMString) {
				hname = ((AVMString) val3).getString();
			} else {
				throw new PredicateEvaluationException("type mismatch (3rd arg) for `is'");
			}
			h = grammar.findHierarchy(hname);
			if(h == null) {
				throw new PredicateEvaluationException(String.format("hierarchy `%s' not found", hname));
			}
		}

		/* maybe read second label name */
		if(args.size()>=4) {
			AVM val4 = args.get(3).eval(lg, context, binding);
			if(val4 instanceof AVMString) {
				labels = ((AVMString)val4).getString();
			} else { /* complain */
				throw new PredicateEvaluationException("type mismatch (4rd arg) for `is'");
			}
		}

		/* Find the LV to check. If there is no fourth argument, this is the LV
	     above the specified word. */
		i = ln.getArc().from;
		lv = context.getLV(noOflevels * i + levelno);
		
		if(lv == null) {
			return false;
		}

		/*  Otherwise, we have to ascend across irrelevant labels. */
		counter = 0;
		while(  labels != null &&
				counter <= lg.getMax() &&
				lv.getRegentGN().spec() &&
				h.subsumes(labels, lv.getLabel())) {
			counter++;
			ln = lv.getRegents().get(0);
			i = ln.getArc().from;
			lv = context.getLV(noOflevels * i + levelno);
			if(lv == null) {
				return false;
			}
		}

		if(counter > lg.getMax()) {
			return false;
		}

		if(c != null) {
			/* If a constraint was specified, eval it... */
			result = !c.eval(lg, context, lv).isViolated();
		} else {
			/* ...otherwise compare the label. */
			if( (h == null && lv.getLabel().equals(label))  || 
					(h != null && h.subsumes(label, lv.getLabel())) ) {
				result = true;
			} else {
				result = false;
			}
		}
		
		return result;
	}

}
