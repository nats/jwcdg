package de.unihamburg.informatik.nats.jwcdg.misc.koehnexperiments;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.antlr.runtime.RecognitionException;

import de.unihamburg.informatik.nats.jwcdg.IncrementalParserFactory;
import de.unihamburg.informatik.nats.jwcdg.JWCDG;
import de.unihamburg.informatik.nats.jwcdg.antlr.Helpers;
import de.unihamburg.informatik.nats.jwcdg.incrementality.ChunkIncrement;
import de.unihamburg.informatik.nats.jwcdg.incrementality.IncrementalFrobber;
import de.unihamburg.informatik.nats.jwcdg.incrementality.IncrementalStep;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.parse.Word;

/**
 * @author Arne Köhn
 * 
 */
public class ParseNegraIncremental {

	/**
	 * @param args [Configuration file] [folder containing negra data] [startnumber] [endnumber] [outtemplate]
	 * @throws IOException 
	 * @throws RecognitionException 
	 */
	public static void main(String[] args) throws IOException, RecognitionException {
		Configuration myConfig = Configuration.fromFile(args[0]);
		if (args.length != 5) {
			System.err.println("usage: ParseNegraIncremental [Configuration file] [folder containing negra data] [startnumber] [endnumber] [outtemplate]");
			System.exit(1);
		}
		File folder = new File(args[1]);
		int start = Integer.parseInt(args[2]);
		int end = Integer.parseInt(args[3]);
		if ( ! folder.isDirectory()) {
			System.err.println("The second argument needs to be the directory containing the negra sentences!");
			System.exit(1);
		}
		parseIncremental(myConfig, folder, start, end, args[4]);
	}

	protected static void parseIncremental(Configuration config, File folder, int start, int end, String outtmpl) throws IOException, RecognitionException {
		IncrementalFrobber parser = JWCDG.makeParser(config, new IncrementalParserFactory());
		for (int sentenceNr = start; sentenceNr<=end; sentenceNr++) {
			File f = new File(folder, String.format("negra-s%d.cda", sentenceNr));
			IncrementalStep step = parser.makeInitialStep();
			int increment = 1;
			List<String> sentence = new ArrayList<>();
			for (Word word: Helpers.parserFromIS(new FileReader(f)).annoEntry().getWords()) {
				sentence.add(word.word);
			}
			for (String token : sentence) {
				ChunkIncrement chunk = new ChunkIncrement(token);
				boolean isLast = sentence.size() == increment;
				step = parser.continueParsing(step, chunk, isLast, null);
				FileWriter writer = new FileWriter(String.format(outtmpl, sentenceNr, increment));
				writer.write(step.getParse().toAnnotation(false));
				writer.close();
				increment += 1;
			}
			
		}
	}
}
