package de.unihamburg.informatik.nats.jwcdg.gui;

import java.io.File;
import java.io.IOException;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import de.unihamburg.informatik.nats.jwcdg.JWCDG;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarLexer;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarParser;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;

/**
 *
 * The concrete classes are either the DepTreeViewer or the AnnoViewer.
 *
 * @author Sven Zimmer and Christine Köhn and Arne Köhn
 *
 */

public abstract class AbstractViewer {

	protected String _grammarString;

	protected static Options initOptions() {
		Options options = new Options();
		Option opt_help = new Option("help", "print this message");
		options.addOption(opt_help);
		Option opt_config =
				new Option("c",
						true,
						"name of the file containing the startup properties");
		options.addOption(opt_config);
		return options;
	}

	protected static CommandLine
	initCommandLine(Options options, String[] args) {
		CommandLine line = null;
		CommandLineParser cmdlineparser = new GnuParser();
		try {
			line = cmdlineparser.parse(options, args);
		}
		catch (ParseException exp) {
			System.err.println("Couldn't parse command line args: "
					+ exp.getMessage());
			System.exit(1);
		}
		return line;
	}

	/**
	 * @param line
	 * @param options
	 * @return Configuration or null
	 */
	protected static Configuration readConfig(CommandLine line, Options options) {
		String confFileName = line.hasOption("c") ? line.getOptionValue("c")
				: "startup.properties";
		Configuration config = null;
		try {
			config = Configuration.fromFile(confFileName);
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println(String.format("Could not read config file '%s'",
					confFileName));
		}
		return config;
	}

	protected void initGrammarString(Configuration config){
		StringBuilder sb = new StringBuilder();
		for (String fname : config.getString(JWCDG.GRAMMAR_FILES).split(",")) {
			String fixedPath = Configuration.fixPath(fname);
			sb.append(FileService.loadText(new File(fixedPath),
					config.getString(JWCDG.GRAMMAR_ENCODING)));
		}
		_grammarString = sb.toString();
	}

	protected void createDisplay(AbstractDisplayInteractor dtDI,
			AbstractDisplay gui) {
		dtDI.set_GUI(gui);
		gui._constraintsArea.setText(_grammarString);
		gui.setVisible(true);
	}

	public static Parse
	generateParseObject(String cda) throws RecognitionException {
		jwcdggrammarParser gParser =
				new jwcdggrammarParser(new CommonTokenStream(new jwcdggrammarLexer(new ANTLRStringStream(cda))));
		Parse p = gParser.annoEntry();
		return p;
	}

}
