package de.unihamburg.informatik.nats.jwcdg.parse;

public abstract class AnnoSpecification {
	public final String kind;			       /* level name or category kind */
	public final String name;			       /* tag name or label name */
	
	protected AnnoSpecification(String key, String val) {
		kind = key;
		name = val;
	}
}
