grammar jwcdggrammar;

@lexer::header {package de.unihamburg.informatik.nats.jwcdg.antlr;}
@header {
package de.unihamburg.informatik.nats.jwcdg.antlr;
import de.unihamburg.informatik.nats.jwcdg.constraints.*;
import de.unihamburg.informatik.nats.jwcdg.constraints.formulas.*;
import de.unihamburg.informatik.nats.jwcdg.constraints.functions.*;
import de.unihamburg.informatik.nats.jwcdg.constraints.predicates.*;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.*;
import de.unihamburg.informatik.nats.jwcdg.input.*;
import de.unihamburg.informatik.nats.jwcdg.avms.*;
import de.unihamburg.informatik.nats.jwcdg.parse.*;
import de.unihamburg.informatik.nats.jwcdg.incrementality.virtualNodes.*;
import de.unihamburg.informatik.nats.jwcdg.ForgettingANTLRInputStream;
import java.util.Map;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.lang.StringBuffer;
}
@members {

@Override
protected Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow) throws RecognitionException
{
    throw new MismatchedTokenException(ttype, input);
}

@Override
public Object recoverFromMismatchedSet(IntStream input, RecognitionException e, BitSet follow) throws RecognitionException
{
    throw e;
}

Map<String, Predicate> predmap;
Grammar g;
Map<String, Predicate> relationmap;
Map<String, Function> funcmap;

{
	g = new Grammar();
	predmap = new HashMap();
	relationmap = new HashMap();
	funcmap = new HashMap();
	predmap.put("start(", new PredicateStart(g));
	predmap.put("stop(", new PredicateStop(g));
	predmap.put("frontier(", new PredicateFrontier(g));
	predmap.put("root(", new PredicateRoot(g));
	predmap.put("spec(", new PredicateSpec(g));
	predmap.put("virtual(", new PredicateVirtual(g));
	predmap.put("nonspec(", new PredicateNonspec(g));
	predmap.put("print(", new PredicatePrint(g));
	predmap.put("subsumes(", new PredicateSubsumes(g));
	predmap.put("exists(", new PredicateExists(g));
	predmap.put("compatible(", new PredicateCompatible(g));
	predmap.put("chunk_head(", new PredicateChunkhead(g));
	predmap.put("occur(", new PredicateOccur(g));
	predmap.put("has(", new PredicateHas(g));
	predmap.put("is(", new PredicateIs(g));
	predmap.put("cyclic(", new PredicateCycle(g));
	predmap.put("between(", new PredicateBetween(g));
	predmap.put("under(", new PredicateUnder(g));
	predmap.put("connected(", new PredicateConnected(g));
	predmap.put("partial(", new PredicatePartial(g));
	predmap.put("notanumber(", new PredicateNotANumber(g));
	predmap.put("isLexemeOf(", new PredicateIsLexemeOf(g));
	predmap.put("samevnspec(", new PredicateSameVNSpec(g));

	relationmap.put("=", new PredicateEquals(g));
	relationmap.put(">", new PredicateGreater(g));
	relationmap.put("<", new PredicateLess(g));
	relationmap.put(">=", new PredicateGreaterEqual(g));
	relationmap.put("<=", new PredicateLessEqual(g));
	relationmap.put("!=", new PredicateNotEquals(g));
	relationmap.put("=~", new PredicateRegexp(g));
	
	funcmap.put("abs(", new FunctionAbs(g));
	funcmap.put("acoustics(", new FunctionAcoustics(g));
	funcmap.put("distance(", new FunctionDistance(g));
	funcmap.put("exp(", new FunctionExp(g));
	funcmap.put("height(", new FunctionHeight(g));
	funcmap.put("lookup(", new FunctionLookup(g));
	funcmap.put("match(", new FunctionMatch(g));
	funcmap.put("max(", new FunctionMax(g));
	funcmap.put("min(", new FunctionMin(g));
	funcmap.put("parens(", new FunctionParens(g));
	funcmap.put("parent(", new FunctionParent(g));
	funcmap.put("phrasequotes(", new FunctionPhraseQuotes(g));
	funcmap.put("predict(", new FunctionPredict(g));
	funcmap.put("quotes(", new FunctionQuotes(g));
	funcmap.put("selectedLexemeAt(", new FunctionSelectedLexemeAt(g));
}
}

@rulecatch {
catch(RecognitionException re) {
recover(input, re);
throw re;
}
}

NUMBER:	'-'? ('0'..'9')+ ('.' ('0'..'9')+)?;

WS  :   ( ' '
        | '\t'
        | '\r'
        | '\n'
        ) {skip();}
    ;

COMMENT
    :   '//' ~('\n'|'\r')* '\r'? '\n' {skip();}
    |   '/*' ( options {greedy=false;} : . )* '*/' {skip();}
    ;


/* Define your function and predicate names here ! */
PredName:
   (
   'start'|
   'stop'|
   'frontier'|
   'root'|
   'spec'|
   'virtual'|
   'nonspec'|
   'print'|
   'subsumes'|
   'exists'|
   'compatible'|
   'chunk_head'|
   'occur'|
   'has'|
   'is'|
   'cyclic'|
   'between'|
   'under'|
   'connected'|
   'partial'|
   'notanumber'|
   'isLexemeOf'|
   'samevnspec' ) '('
	;


FuncName:
 (
 'abs'|
 'acoustics'|
 'exp'|
 'distance'|
 'match'|
 'max'|
 'min'|
 'predict'|
 'lookup'|
 'quotes'|
 'parens'|
 'phrasequotes'|
 'parent'|
 'height' |
 'selectedLexemeAt')
 '('
	;

CTRUE	:	'true';
CFALSE	:	'false';
//LABEL	:	'label';
LEVEL	:	'level';
VIRTUALNODE	:	'virtualnode';
TOPDOWNPREDICTOR : 'tdp';

fragment
EXPONENT : ('e'|'E') ('+'|'-')? ('0'..'9')+ ;

fragment
HEX_DIGIT : ('0'..'9'|'a'..'f'|'A'..'F') ;

fragment
ESC_SEQ
    :   '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')
    |   UNICODE_ESC
    |   OCTAL_ESC
    ;

fragment
OCTAL_ESC
    :   '\\' ('0'..'3') ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7')
    ;

fragment
UNICODE_ESC
    :   '\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT
    ;


/* Denotate the start and end of VM Lattice */
BEGIN_LATTICE: 'BEGIN_LATTICE';
END_LATTICE : 'END_LATTICE';

MINUS : '-';
BIIMPL : '<->';
IMPL : '->';
LPMI : '<-';
GTEQ : '>=';
LSEQ : '<=';
NEQ : '!=';
ISDEFINEDBY : ':=';
MATCHES : '=~';
MAP : '=>';

CTOP : '/\\';
CBOTTOM : '\\/';
CNONE : '||';
CINVERSE : '~=';
CIDENT : '==';
SLASH	:	'/';

STR	
	: '\'' ( ( ~('\\'|'\'') | '\\' . ) )* '\''
//	: '\'' (~('\''|'\\') | '\\\''|'\\\'' )* '\''
	{
		String oldText = getText();
		StringBuffer newText = new StringBuffer();
		// remember: the first and last character are ' which we discard
		boolean inEscape = false;
		char current;
		for (int i=1; i< oldText.length()-1; i++) {
			current = oldText.charAt(i);
			if (current == '\\' && !inEscape) {
				inEscape = true;
				continue;
			}
			// if the \ was not meant for escaping, insert it afterwards
			if (inEscape && current != '\\'
			    && current != '\''
			    && current != '"')
				newText.append("\\");
			newText.append(current);
			inEscape = false;
		}
		setText(newText.toString());
	}
  | '"' ( ( ~('\\'|'"') | '\\'.) )* '"'
//  | '"' ( ( ~('\\'|'"') | '\\"' | '\\\\'| '\\\'') )* '"' V2
//	| '"' ( ( ~('\\'|'"'|'\'') | '\\"' | '\\\\' | '\\\'') )* '"'
//'"' ( options {greedy=false;} :( ~('"'|'\\') | '\\') )* '"'
	{
		String oldText = getText();
		StringBuffer newText = new StringBuffer();
		// remember: the first and last character are ' which we discard
		boolean inEscape = false;
		char current;
		for (int i=1; i< oldText.length()-1; i++) {
			current = oldText.charAt(i);
			if (current == '\\' && !inEscape) {
				inEscape = true;
				continue;
			}
			// if the \ was not meant for escaping, insert it afterwards
			if (inEscape && current != '\\'
			    && current != '\''
			    && current != '"')
				newText.append("\\");
			newText.append(current);
			inEscape = false;
		}
		setText(newText.toString());
	}
	| ('a'..'z'|'A'..'Z'|'_'|'0'..'9'|'\u0080'..'\ufffe')+
	;

string	:	STR;
strings	:	STR+;

annoEntry returns [Parse p]
    :	id=STR ':' STR BIIMPL l=annoList
    {p = new Parse($id.text.intern(), l);}
    ;

number returns [double f] :	n=NUMBER
{$f = Double.parseDouble($n.text);}; 
 
annoList returns [List l]:
    { l= new ArrayList(); }
    (a=annotation {l.add(a);}) 
    (',' a2=annotation {l.add(a2);})*
;

annotation returns [WordAnno wa]:
    from=number to=number word=STR l=specSeq
    {wa = new WordAnno((int) Math.floor(from), (int) Math.floor(to), $word.text.intern(), l);}
;

specification returns [AnnoSpecification as]:
    (tag=STR SLASH val=STR {as= new AnnoSpecificationTag($tag.text.intern(), $val.text.intern());}) 
    | (lvl=STR IMPL label=STR IMPL head=number
    {as= new AnnoSpecificationDep($lvl.text.intern(), $label.text.intern(), (int)Math.floor($head.f));})
;

specSeq returns [List l] :
   s=specification {l = new ArrayList(); l.add(s);}
   (s2=specification {l.add(s2);})*
;

arc:
    STR
  | STR value
  | NUMBER NUMBER STR
  | NUMBER NUMBER STR NUMBER
 
;

arcVM:
  NUMBER NUMBER STR NUMBER NUMBER NUMBER (infoString)?
| NUMBER NUMBER '<' STR '>' NUMBER NUMBER NUMBER (infoString)?
//| NUMBER NUMBER STR NUMBER NUMBER NUMBER 
//| NUMBER NUMBER '<' STR '>' NUMBER NUMBER NUMBER 
;

arcList:
    (arc) (',' arc)*
;

arcListVM:
  /* same as by ArcList, but without the "," */
    arcVM+
;

attributeValue returns [AVMNode avm]
:
    k=STR ':' v=value {$avm = new AVMNode($k.text.intern(),$v.avm);}
  | k=NUMBER ':' v=value {$avm = new AVMNode($k.text.intern(),$v.avm);}
;

conjunction returns [AVMConj avm]
:
	{List l = new ArrayList();}
	(v=value {l.add($v.avm);}) (',' v=value {l.add($v.avm);})*
	{$avm = new AVMConj(l);}
;

connexion returns [Connection c]
:
    ',' {$c = Connection.Any;}
  | 
    CTOP {$c = Connection.Top;}
  | 
    CBOTTOM {$c = Connection.Bottom;}
  | 
    '/' {$c = Connection.Over;}
  | 
    '\\' {$c = Connection.Under;}
  | 
    CNONE{$c = Connection.None;}
  | 
    CINVERSE{$c = Connection.Inverse;}
  | 
    CIDENT{$c = Connection.Identical;}
;    
constraint returns [Constraint c, Signature s]
scope {
	Map<String, VarInfo> varMap;
}
:
	{
		Term penaltyTerm = null;
		String section = "default";
		List attributes = null;
	}
	'{' v=varList '}' ':' name=STR ':'
	( sec=STR ':' w=NUMBER ':' ('(' att=stringlist ')' ':' )? f=formula
		{
			penaltyTerm = new TermNumber(Double.parseDouble($w.text));
			section= $sec.text.intern();
			attributes = $att.l;
		}
	| sec=STR ':' '[' t=term ']' ':' ('(' att=stringlist ')' ':' )? f=formula
		{
			penaltyTerm = $t.t;
			section= $sec.text.intern();
			attributes = $att.l;
		}
	| w=NUMBER ':' f=formula
		{
			penaltyTerm = new TermNumber(Double.parseDouble($w.text));
		}
	| '[' t=term ']' ':' f=formula
		{
			penaltyTerm = $t.t;
		}
	)
//  | '{' varList '}' ':' STR ':' formula
	{
		try {
			if (attributes == null)
				attributes = new ArrayList(0);
			$c = new Constraint($name.text, section, $v.sig,  $f.form, penaltyTerm, $v.vars, attributes, null);
		} catch (Exception e)
		{
			throw new RuntimeException("undefined level or something!"); //TODO #EXCEPTIONS fail more specific
		}
	}
;

direction returns [Direction d]
:
    ':' {d= Direction.AnyDir;}
  | 
    OR  {d= Direction.Nil;}
  |
    '!' {d= Direction.NonNil;}
  |
    '\\' {d= Direction.Left;}
  |
    SLASH {d= Direction.Right;}
;

disjunction returns [AVM avm]:
	{List l = new ArrayList();}
	(v=value {l.add($v.avm);}) (OR v=value {l.add($v.avm);})*
	{$avm = new AVMDisj(l);}
;

property returns [String k, String v]:
	key=STR '=' val=STR
	{$k=$key.text.intern(); $v=$val.text.intern();}
;

properties returns [Map p]:
	{Map pr = new HashMap();}
	prop=property
	{
		pr.put($prop.k, $prop.v);
	} 
	(
	',' prop=property
	{
		pr.put($prop.k, $prop.v);
	}
	)*
	{$p = pr;}
;

propertyList returns [Map p]:
	'[' props=properties ']'
	{$p=$props.p;}
;


OR	:	'|';
AND	:	'&';

/* if there is only one subformula: just pass the formula up
*  else (second exists): create a new formula and add the subformulas
*  This way we don't create a lot of unnecessary formula objects.
*/
// TODO maybe return to simple (a|(b|c))-style if it's faster
formula returns [Formula form]:
  f=implformula {$form = $f.form;}
  (BIIMPL f=implformula {$form  = new FormulaBiImpl($form, $f.form);})?
  ;

implformula returns [Formula form]:
  f=orformula {$form = $f.form;}
  (IMPL f=orformula {$form  = new FormulaImpl($form, $f.form);})?;

orformula returns [Formula form]:
  f=andformula {$form = $f.form;}
  (
		OR f=andformula
		{
			FormulaOr newform  = new FormulaOr();
			newform.addSubFormula($form);
			newform.addSubFormula($f.form);
			$form = newform;
		}
		(
			OR f=andformula
			{
				((FormulaOr)$form).addSubFormula($f.form);
			}
		)*
	)?
;

andformula returns [Formula form]:
	f=formatom {$form = $f.form;}
	(
		AND f=formatom
		{
			FormulaAnd newform = new FormulaAnd();
			newform  = new FormulaAnd();
			newform.addSubFormula($form);
			newform.addSubFormula($f.form);
			$form = newform;
		}
		(
			AND f=formatom
			{
				((FormulaAnd)$form).addSubFormula($f.form);
			}
		)*
	)?
;

// keep in mind that PredName also matches the opening bracket
predicate returns [Formula form]:
	p=PredName ')'
	{
		$form = new FormulaPredicate(predmap.get($p.text));
	}
	|	p=PredName t=termList')'
	{
		$form = new FormulaPredicate(predmap.get($p.text), $t.tl);
	}
;

relation returns [Formula form]:
	t1=term r=relationType t2=term
	{
		List templist = new ArrayList();
		templist.add($t1.t);
		templist.add($t2.t);
		$form = new FormulaPredicate(relationmap.get($r.text),templist);
	}
;

formatom returns [Formula form]:	
	  '(' formula ')' {$form = $formula.form;}
	| '~' f=formatom {$form = new FormulaNot($f.form);}
	| CTRUE {$form = new FormulaTrue();}
	| CFALSE {$form = new FormulaFalse();}
	| p=predicate {$form = $p.form;}
	| a=STR c=connexion b=STR
	  {
	    $form = new FormulaConnection($constraint::varMap.get($a.text),
	                                  $c.c,
	                                  $constraint::varMap.get($b.text));
	  }
	| a=STR d=direction
	  {
	    $form = new FormulaDirection($d.d, $constraint::varMap.get($a.text));
	  }
	| r=relation {$form = $r.form;}
	//m4_define(isa, subsumes(Features, $2, $1cat))
	| 'isa(' var=STR '^' ',' cat=STR ')' {
	  $form = new FormulaPredicate(
	    predmap.get("subsumes("),
	    new TermString("Features"),
	    new TermString($cat.text),
	    new TermTopPeek(new Path("cat", null), $constraint::varMap.get($var.text)));
	}
	| 'isa(' var=STR '@' ',' cat=STR ')' {
    $form = new FormulaPredicate(
      predmap.get("subsumes("),
      new TermString("Features"),
      new TermString($cat.text),
      new TermBottomPeek(new Path("cat", null), $constraint::varMap.get($var.text)));
  }
  | 'isa2(' var=STR '^' ',' cat=STR ')' {
    $form = new FormulaPredicate(
      predmap.get("subsumes("),
      new TermString("Features"),
      new TermString($cat.text),
      new TermTopPeek(new Path("cat2", null), $constraint::varMap.get($var.text)));
  }
  | 'isa2(' var=STR '@' ',' cat=STR ')' {
    $form = new FormulaPredicate(
      predmap.get("subsumes("),
      new TermString("Features"),
      new TermString($cat.text),
      new TermBottomPeek(new Path("cat2", null), $constraint::varMap.get($var.text)));
  }
  | 'edge(' var=STR ',' lab=STR ')' {
    //m4_define(edge, subsumes(Label, $2, $1.label))
    $form = new FormulaPredicate(
      predmap.get("subsumes("),
      new TermString("Label"),
      new TermString($lab.text),
      new TermLabel($constraint::varMap.get($var.text)));
  }
	| 'topicalized(' var=STR ')' {
	  // m4_define(topicalized, 
    // (parent($1^id, AUX) > $1@from & parent($1^id, AUX) < $1^from |
    //  parent($1^id, OBJI) > $1@from & parent($1^id, OBJI) < $1^from))
      List tl1= new ArrayList(2);
      List tl2= new ArrayList(2);
      List tl3= new ArrayList(2);
      List tl4= new ArrayList(2);
      tl1.add(new TermFunction(
        funcmap.get("parent("),
        new TermTopPeek(new Path("id", null), $constraint::varMap.get($var.text)),
        new TermString("AUX")));
      tl1.add(new TermBottomPeek(new Path("from", null), $constraint::varMap.get($var.text)));
      
      tl2.add(new TermFunction(
        funcmap.get("parent("),
        new TermTopPeek(new Path("id", null), $constraint::varMap.get($var.text)),
        new TermString("AUX")));
      tl2.add(new TermTopPeek(new Path("from", null), $constraint::varMap.get($var.text)));
      
      tl3.add(new TermFunction(
        funcmap.get("parent("),
        new TermTopPeek(new Path("id", null), $constraint::varMap.get($var.text)),
        new TermString("OBJI")));
      tl3.add(new TermBottomPeek(new Path("from", null), $constraint::varMap.get($var.text)));
      
      tl4.add(new TermFunction(
        funcmap.get("parent("),
        new TermTopPeek(new Path("id", null), $constraint::varMap.get($var.text)),
        new TermString("OBJI")));
      tl4.add(new TermTopPeek(new Path("from", null), $constraint::varMap.get($var.text)));
      
      FormulaAnd fa1= new FormulaAnd();
      fa1.addSubFormula(new FormulaPredicate(relationmap.get(">"), tl1));
      fa1.addSubFormula(new FormulaPredicate(relationmap.get("<"), tl2));
      FormulaAnd fa2= new FormulaAnd();
      fa2.addSubFormula(new FormulaPredicate(relationmap.get(">"), tl3));
      fa2.addSubFormula(new FormulaPredicate(relationmap.get("<"), tl4));
      FormulaOr  fo = new FormulaOr();
      fo.addSubFormula(fa1);
      fo.addSubFormula(fa2);
      $form = fo;
    }
    | 'Partizipialadjektiv(' var=STR '^' ')' {
		//	m4_define(Partizipialadjektiv,
		//	 ((  exists($1 partizipial1) | exists($1 partizipial2) )))
		  FormulaPredicate ex1= new FormulaPredicate(
        predmap.get("exists("),
        new TermTopPeek(new Path("partizipial1", null), $constraint::varMap.get($var.text)));
		  FormulaPredicate ex2= new FormulaPredicate(
        predmap.get("exists("),
        new TermTopPeek(new Path("partizipial2", null), $constraint::varMap.get($var.text)));
      FormulaOr fo = new FormulaOr();
      fo.addSubFormula(ex1);
      fo.addSubFormula(ex2);
      $form = fo;
    }
    | 'Partizipialadjektiv(' var=STR '@' ')' {
      FormulaPredicate ex1= new FormulaPredicate(
        predmap.get("exists("),
        new TermBottomPeek(new Path("partizipial1", null), $constraint::varMap.get($var.text)));
      FormulaPredicate ex2= new FormulaPredicate(
        predmap.get("exists("),
        new TermBottomPeek(new Path("partizipial2", null), $constraint::varMap.get($var.text)));
      FormulaOr fo = new FormulaOr();
      fo.addSubFormula(ex1);
      fo.addSubFormula(ex2);
      $form = fo;
    }
    // m4_define(KON_PP,
    //   (($1 cat = APPR & exists($1 cat2) & $1 cat2 = KON)))
    | 'KON_PP(' var=STR '^' ')' {
      FormulaPredicate eq1= new FormulaPredicate(
        relationmap.get("="),
        new TermTopPeek(new Path("cat", null), 
                        $constraint::varMap.get($var.text)),
        new TermString("APPR"));
      FormulaPredicate ex= new FormulaPredicate(
        predmap.get("exists("),
        new TermTopPeek(new Path("cat2", null), $constraint::varMap.get($var.text)));
      FormulaPredicate eq2= new FormulaPredicate(
        relationmap.get("="),
	      new TermTopPeek(new Path("cat2", null), 
	                      $constraint::varMap.get($var.text)),
	      new TermString("KON"));
      FormulaAnd fa = new FormulaAnd();
      fa.addSubFormula(eq1);
      fa.addSubFormula(ex);
      fa.addSubFormula(eq2);
      $form = fa;
    }
    | 'KON_PP(' var=STR '@' ')' {
      FormulaPredicate eq1= new FormulaPredicate(
        relationmap.get("="),
        new TermBottomPeek(
          new Path("cat", null), 
          $constraint::varMap.get($var.text)),
        new TermString("APPR"));
      FormulaPredicate ex= new FormulaPredicate(
        predmap.get("exists("),
        new TermBottomPeek(new Path("cat2", null), $constraint::varMap.get($var.text)));
      FormulaPredicate eq2= new FormulaPredicate(
	      relationmap.get("="),
	      new TermBottomPeek(new Path("cat2", null), 
	                      $constraint::varMap.get($var.text)),
	      new TermString("KON"));
      FormulaAnd fa = new FormulaAnd();
      fa.addSubFormula(eq1);
      fa.addSubFormula(ex);
      fa.addSubFormula(eq2);
      $form = fa;
    }
    | 'quoted(' var=STR ')' {
	    // m4_define(quoted,
	    //   ((phrasequotes($1 @id) > phrasequotes($1 ^id))))
	    $form = new FormulaPredicate(
	      relationmap.get(">"),
	      new TermFunction(
	        funcmap.get("phrasequotes("),
	        new TermBottomPeek(
	          new Path("id", null), 
            $constraint::varMap.get($var.text))),
	      new TermFunction(
          funcmap.get("phrasequotes("),
          new TermTopPeek(
            new Path("id", null), 
            $constraint::varMap.get($var.text))));
    }
    //m4_define(case_specified,
    //   (($1 case=nom | $1case = gen | $1 case = dat | $1 case = acc)))
    | 'case_specified(' var=STR '^' ')' {
      FormulaPredicate eq1= new FormulaPredicate(
        relationmap.get("="),
        new TermTopPeek(new Path("case", null), 
                        $constraint::varMap.get($var.text)),
        new TermString("nom"));
        FormulaPredicate eq2= new FormulaPredicate(
        relationmap.get("="),
        new TermTopPeek(new Path("case", null), 
                        $constraint::varMap.get($var.text)),
        new TermString("gen"));
        FormulaPredicate eq3= new FormulaPredicate(
        relationmap.get("="),
        new TermTopPeek(new Path("case", null), 
                        $constraint::varMap.get($var.text)),
        new TermString("dat"));
        FormulaPredicate eq4= new FormulaPredicate(
        relationmap.get("="),
        new TermTopPeek(new Path("case", null), 
                        $constraint::varMap.get($var.text)),
        new TermString("acc"));

      FormulaOr fo = new FormulaOr();
      fo.addSubFormula(eq1);
      fo.addSubFormula(eq2);
      fo.addSubFormula(eq3);
      fo.addSubFormula(eq4);
      $form = fo;
    }
    | 'case_specified(' var=STR '@' ')' {
      FormulaPredicate eq1= new FormulaPredicate(
        relationmap.get("="),
        new TermBottomPeek(new Path("case", null), 
                        $constraint::varMap.get($var.text)),
        new TermString("nom"));
      FormulaPredicate eq2= new FormulaPredicate(
        relationmap.get("="),
        new TermBottomPeek(new Path("case", null), 
                        $constraint::varMap.get($var.text)),
        new TermString("gen"));
      FormulaPredicate eq3= new FormulaPredicate(
        relationmap.get("="),
        new TermBottomPeek(new Path("case", null), 
                        $constraint::varMap.get($var.text)),
        new TermString("dat"));
      FormulaPredicate eq4= new FormulaPredicate(
        relationmap.get("="),
        new TermBottomPeek(new Path("case", null), 
                        $constraint::varMap.get($var.text)),
        new TermString("acc"));

      FormulaOr fo = new FormulaOr();
      fo.addSubFormula(eq1);
      fo.addSubFormula(eq2);
      fo.addSubFormula(eq3);
      fo.addSubFormula(eq4);
      $form = fo;
    }
    | 'check_case(' t1=term ',' t2=term ')' { 
    // m4_define(check_case,
    //  (( compatible(Features, $1, $2) |
    //     subsumes(Features, dat, $1) & subsumes(Features, dat, $2) |
    //     subsumes(Features, acc, $1) & subsumes(Features, acc, $2) |
    //     subsumes(Features, nom, $1) & subsumes(Features, nom, $2) |
    //     subsumes(Features, gen, $1) & subsumes(Features, gen, $2))))
      
      FormulaPredicate fc = new FormulaPredicate(
        predmap.get("compatible("),
        new TermString("Features"),$t1.t, $t2.t);
      
      List tl1= new ArrayList(3);
      List tl2= new ArrayList(3);
      List tl3= new ArrayList(3);
      List tl4= new ArrayList(3);
      List tl5= new ArrayList(3);
      List tl6= new ArrayList(3);
      List tl7= new ArrayList(3);
      List tl8= new ArrayList(3);
      tl1.add(new TermString("Features"));
      tl1.add(new TermString("dat"));
      tl1.add($t1.t);
      tl2.add(new TermString("Features"));
      tl2.add(new TermString("dat"));
      tl2.add($t2.t);
      
      tl3.add(new TermString("Features"));
      tl3.add(new TermString("acc"));
      tl3.add($t1.t);
      tl4.add(new TermString("Features"));
      tl4.add(new TermString("acc"));
      tl4.add($t2.t);
      
      tl5.add(new TermString("Features"));
      tl5.add(new TermString("nom"));
      tl5.add($t1.t);
      tl6.add(new TermString("Features"));
      tl6.add(new TermString("nom"));
      tl6.add($t2.t);
      
      tl7.add(new TermString("Features"));
      tl7.add(new TermString("gen"));
      tl7.add($t1.t);
      tl8.add(new TermString("Features"));
      tl8.add(new TermString("gen"));
      tl8.add($t2.t);
      
      FormulaAnd fa1= new FormulaAnd();
      fa1.addSubFormula(new FormulaPredicate(predmap.get("subsumes("), tl1));
      fa1.addSubFormula(new FormulaPredicate(predmap.get("subsumes("), tl2));
      FormulaAnd fa2= new FormulaAnd();
      fa2.addSubFormula(new FormulaPredicate(predmap.get("subsumes("), tl3));
      fa2.addSubFormula(new FormulaPredicate(predmap.get("subsumes("), tl4));
      FormulaAnd fa3= new FormulaAnd();
      fa3.addSubFormula(new FormulaPredicate(predmap.get("subsumes("), tl5));
      fa3.addSubFormula(new FormulaPredicate(predmap.get("subsumes("), tl6));
      FormulaAnd fa4= new FormulaAnd();
      fa4.addSubFormula(new FormulaPredicate(predmap.get("subsumes("), tl7));
      fa4.addSubFormula(new FormulaPredicate(predmap.get("subsumes("), tl8));
      
      FormulaOr  fo = new FormulaOr();
      fo.addSubFormula(fc);
      fo.addSubFormula(fa1);
      fo.addSubFormula(fa2);
      fo.addSubFormula(fa3);
      fo.addSubFormula(fa4);
      $form = fo;
    }
    | 'check_gender(' t1=term ',' t2=term ')' {
//  m4_define(check_gender,
//    (( compatible(Features, $1, $2) |
//       subsumes(Features, $1, masc) & subsumes(Features, $2, masc) |
//       subsumes(Features, $1, fem) & subsumes(Features, $2, fem) |
//       subsumes(Features, $1, neut) & subsumes(Features, $2, neut))))
      FormulaPredicate fc = new FormulaPredicate(
        predmap.get("compatible("),
        new TermString("Features"), $t1.t, $t2.t);
      
      List tl1= new ArrayList(3);
      List tl2= new ArrayList(3);
      List tl3= new ArrayList(3);
      List tl4= new ArrayList(3);
      List tl5= new ArrayList(3);
      List tl6= new ArrayList(3);
      tl1.add(new TermString("Features"));
      tl1.add($t1.t);
      tl1.add(new TermString("masc"));
      tl2.add(new TermString("Features"));
      tl2.add($t2.t);
      tl2.add(new TermString("masc"));
      
      tl3.add(new TermString("Features"));
      tl3.add($t1.t);
      tl3.add(new TermString("fem"));
      tl4.add(new TermString("Features"));
      tl4.add($t2.t);
      tl4.add(new TermString("fem"));
      
      tl5.add(new TermString("Features"));
      tl5.add($t1.t);
      tl5.add(new TermString("neut"));
      tl6.add(new TermString("Features"));
      tl6.add($t2.t);
      tl6.add(new TermString("neut"));
      
      FormulaAnd fa1= new FormulaAnd();
      fa1.addSubFormula(new FormulaPredicate(predmap.get("subsumes("), tl1));
      fa1.addSubFormula(new FormulaPredicate(predmap.get("subsumes("), tl2));
      FormulaAnd fa2= new FormulaAnd();
      fa2.addSubFormula(new FormulaPredicate(predmap.get("subsumes("), tl3));
      fa2.addSubFormula(new FormulaPredicate(predmap.get("subsumes("), tl4));
      FormulaAnd fa3= new FormulaAnd();
      fa3.addSubFormula(new FormulaPredicate(predmap.get("subsumes("), tl5));
      fa3.addSubFormula(new FormulaPredicate(predmap.get("subsumes("), tl6));
     
      FormulaOr  fo = new FormulaOr();
      fo.addSubFormula(fc);
      fo.addSubFormula(fa1);
      fo.addSubFormula(fa2);
      fo.addSubFormula(fa3);
      $form = fo;
    }
	;

hashentry returns[List l]:
	{$l = new ArrayList(2);}
	sl=stringlist 
	{
		String key=""; 
		boolean first=true; 
		for(Object o: sl) {
			if(!first)
				key+=":";
			first = false;
			key+= (String) o;
		} 
		$l.add(key);
	}
	MAP v=value {$l.add(v);}
;

hashentrylist returns [Map m]:
  {$m = new HashMap();}
  he=hashentry { $m.put(he.get(0), he.get(1)); }
  (',' he=hashentry { $m.put(he.get(0), he.get(1)); })*
;

hierarchy returns [Hierarchy h]:
   ( id=STR IMPL s=sort {$h = new Hierarchy($id.text.intern(), s, new SourceInfo());})
  | (id=STR IMPL l=subsumptionList { $h= new Hierarchy($id.text.intern(), l, new SourceInfo()); } )
;

infoString:
  /* Infostring is a Conjunction of module contributions */
   ('(' STR infoModule ')')+
;

infoModule:
 (infoItem)+
;

infoItem:
 '(' STR infoModule ')' 
| STR 
| NUMBER 
;


Pragma	:	'#pragma' (~'\n')*;
pragma	:	Pragma;
propertiesdef:	STR '(' stringListCS ')';

cdggrammar returns [Grammar grammar]
:
{
	Map<String,Level> levels = new HashMap<String,Level>();
}
(
	(
		lev=level {g.addLevel((Level)$lev.l);}
		| constr=constraint {g.addConstraint($constr.c);}
		| lattice
		| annoEntry
		| h=hierarchy {g.addHierarchy(h);}
		| id=STR MAP hel=hashentrylist { g.addMap($id.text.intern(), hel); }
		| propertiesdef
		| vns=vnSpec {g.addVNSpec(vns);}
		| tdp
	)
	';'
)* EOF
{
	$grammar = g;
}
;


lexicon [Map lexiconMap]
:
{
	String INVALID_WORD = "!THISWORDWILLHOPEFULLYNEVERBEINTHELEXICON!!1"; 
	String lastWord= INVALID_WORD;
	List currentWordCache = new ArrayList();
	ForgettingANTLRInputStream is = (ForgettingANTLRInputStream)((Lexer)this.input.getTokenSource()).getCharStream();

}
(
	lex=lexiconItem
	{
		String currentWord = $lex.l.getWord();
		if (currentWord.equals(lastWord)) {
			currentWordCache.add($lex.l);
		} else {
			if (!currentWordCache.isEmpty())
				$lexiconMap.put(lastWord, currentWordCache);
			currentWordCache = (List<LexiconItem>)$lexiconMap.get(currentWord);
			if (currentWordCache == null)
				currentWordCache = new ArrayList();
			lastWord = currentWord;
			currentWordCache.add($lex.l);
		}
	}{is.forget();}
	';'
)* EOF
{
	// put the last word into the map
	if (!lastWord.equals(INVALID_WORD)) $lexiconMap.put(lastWord, currentWordCache);
}
;


labelList returns [List<String> l]:
    s=STR {$l = new ArrayList();$l.add($s.text.intern());} (',' s=STR {$l.add($s.text.intern());})*
;

lattice:
    STR ':' arcList
 |  BEGIN_LATTICE
    arcListVM 
    END_LATTICE
;

level returns [Level l]:
    name=STR plist=propertyList? '#' llist=labelList
{$l = new Level($name.text.intern(), $plist.p, $llist.l, null);}
;

lexiconItem returns [LexiconItem l]
:
// this is deprecated
//    STR ISDEFINEDBY STR ':' value
//    |
	word=STR ISDEFINEDBY '[' ']'
	{
		$l= new LexiconItem($word.text.intern(), new AVMConj(new ArrayList(0)), null);
	}
	|
	word=STR ISDEFINEDBY '[' con=conjunction ']'
	{
		$l= new LexiconItem($word.text.intern(), $con.avm, null);
	}
	|
	word=STR MATCHES '[' con=conjunction ']'
	{
		$l= new LexiconItem(Pattern.compile($word.text), $con.avm, null);
	}
;
/*
parameter:
    '[' varList ']' ':' NUMBER ':' formula
;
*/
path returns [Path p]:
	{List l = new ArrayList();}
	(s=STR  {l.add($s.text.intern());}
	| s=NUMBER {l.add($s.text.intern());}
	) (
	':' ( s=STR {l.add($s.text.intern());}
	    | s=NUMBER {l.add($s.text.intern());}
	    )
	)*
{$p = new Path(l);}
;

relationType:
    '='
  | '>'
  | '<'
  | GTEQ
  | LSEQ
  | NEQ
  | MATCHES
;

sort returns[Sort s]:
    {List l = new ArrayList();}
    id=STR ( '(' l2=sortList {l.addAll(l2);} ')' )?
    {$s = new Sort($id.text.intern(), l);}
;

sortList returns [List l]:
    {l = new ArrayList();}
    (s=sort {l.add(s);}) (',' s=sort {l.add(s);} )*
;


stringlist returns [List l]:
  {$l= new ArrayList();}
  s=STR{$l.add($s.text.intern());} (s=STR{$l.add($s.text.intern());})*
;


stringListCS returns [List l]:
  {$l= new ArrayList();}
  s=STR{l.add($s.text.intern());} (',' s=STR{l.add($s.text.intern());})*
;


subsumption returns [Subsumption s]:
    type=STR IMPL l=typeSeq {$s=new Subsumption($type.text.intern(), true, l); }
  | type=STR LPMI l=typeSeq {$s=new Subsumption($type.text.intern(), false, l); } 
;

subsumptionList returns [List l]:
	{ $l= new ArrayList(); }
    (s=subsumption { $l.add(s); } ) (',' s=subsumption { $l.add(s); })*
;


term returns [Term t]:
	mt=minusterm {$t=$mt.t;}
	('+' secondt=term{$t = new TermAdd($t, $secondt.t);})?;
minusterm returns [Term t]:
	tt=timesterm {$t=$tt.t;}
	(MINUS secondt=minusterm{$t = new TermSubstract($t, $secondt.t);})?;
timesterm returns [Term t]:
	st=slashterm {$t=$st.t;}
	('*' secondt=timesterm{$t = new TermMultiply($t, $secondt.t);})?;
slashterm returns [Term t]:
	ta=termatom {$t=$ta.t;}
	(SLASH secondt=slashterm{$t = new TermDivide($t, $secondt.t);})?;

// keep in mind that FuncName also matches the opening bracket
function returns [Term t]:
  f=FuncName ')'
  {
    $t = new TermFunction(funcmap.get($f.text));
  }
  | f=FuncName tl=termList')'
  {
    $t = new TermFunction(funcmap.get($f.text), $tl.tl);
  }
;


termatom returns [Term t]:
   f=function {$t=$f.t;}
  | s=STR {
		$t = new TermString($s.text.intern());
	}
	| vname=STR (
		'^' p=path
		{
			$t = new TermTopPeek($p.p, $constraint::varMap.get($vname.text));
		}
		| '@' p=path
		{
			$t = new TermBottomPeek($p.p, $constraint::varMap.get($vname.text));
		}
		| '.' 'label'
		{
			$t = new TermLabel($constraint::varMap.get($vname.text));
		}
  )
  | '[' subt=term ']' {$t = $subt.t;}
  | n=number {
		$t = new TermNumber($n.f);
	}
	| 'gradient(' arg=term ')' {
	  //m4_define(gradient, [ $1 / [ $1 + abs( distance(X@id, X^id) ) ] ])
	  List tl2 = new ArrayList(2);
	  tl2.add(new TermBottomPeek(new Path("id", null), $constraint::varMap.get("X")));
	  tl2.add(new TermTopPeek(   new Path("id", null), $constraint::varMap.get("X")));
	  List tl1 = new ArrayList(1);
	  tl1.add(new TermFunction(funcmap.get("distance("), tl2));
	  $t= new TermDivide($arg.t, 
	                     new TermAdd($arg.t,
	                                 new TermFunction(funcmap.get("abs("), 
	                                 tl1)));
	}
;

termList returns [List tl]:
	{
		$tl = new ArrayList();
	}
  t=term {$tl.add($t.t);}(',' t=term {$tl.add($t.t);})*
;

tdp:
    TOPDOWNPREDICTOR ':' STR ':' NUMBER ':' ('@'|'^') ':' '(' stringlist ')' ':' '(' stringlist ')'  
;

typeSeq returns [List l]:
    {$l= new ArrayList();}
    ( t=STR { $l.add($t.text.intern()); } )+
;

valueNumber returns [AVM avm]:
	n=number {$avm = new AVMNumber($n.f);}
;

valueString returns [AVM avm]:
	s=STR {$avm = new AVMString($s.text.intern());}
;

value returns [AVM avm]
:
    n=valueNumber {$avm = $n.avm;}
  | s=valueString {$avm = $s.avm;}
  | av=attributeValue {$avm = $av.avm;}
  | '<' vl=valueList '>' {$avm=$vl.avm;}
  | '[' vc=conjunction ']'  {$avm=$vc.avm;}
  | '(' vd=disjunction ')' {$avm=$vd.avm;}
  | '#' coordn=number '(' vd=disjunction ')'
  {
    $avm=$vd.avm;
    ((AVMDisj)$avm).setCoordinationIndex((int)Math.floor($coordn.f));
  }
;

valueList returns [AVMList avm]
:
	{List l = new ArrayList();}
	(v=valueString {l.add($v.avm);} ',' v=valueNumber {l.add($v.avm);})
	(',' v=valueString {l.add($v.avm);} ',' v=valueNumber {l.add($v.avm);})*
	{$avm = new AVMList(l);}
;

// vindex is the index of this variable in the signature of the constraint
varInfo [int vindex] returns [VarInfo var]:
	name=STR dir=direction lv=STR
	{
		Level l = g.findLevel($lv.text);
		$var = new VarInfo($name.text.intern(),$dir.d,l,$vindex);}
;

varList returns [List<VarInfo> vars, Signature sig]:
	{
		$vars = new ArrayList();
		$constraint::varMap = new HashMap<String, VarInfo>();
		Map vmap = $constraint::varMap;
	}
	(
		v=varInfo[0]
		{
			$vars.add($v.var);
			$sig = new Signature($v.var.level,$v.var.dir);
			vmap.put($v.var.varname, $v.var);
		}
		| v1=varInfo[0] c=connexion v2=varInfo[1]
			{
				$vars.add($v1.var);
				$vars.add($v2.var);
				vmap.put($v1.var.varname, $v1.var);
				vmap.put($v2.var.varname, $v2.var);
				$sig = new Signature($v1.var.level,
				                     $v1.var.dir,
				                     $v2.var.level,
				                     $v2.var.dir,
				                     $c.c);
			}
	)
;


vnSpec returns [VirtualNodeSpec vns] :
VIRTUALNODE ':' n=STR ':' p=NUMBER ':' c=NUMBER ':' '[' v=conjunction ']' 
{
	List vs = new ArrayList();
	vs.add(v);	
}
( ',' '[' v=conjunction ']' 
{
	vs.add(v);
}
)*
{
	$vns = new VirtualNodeSpec($n.text.intern(), vs, Integer.parseInt($c.text), new SourceInfo());
	// TODO #SOURCEINFO
}
;

