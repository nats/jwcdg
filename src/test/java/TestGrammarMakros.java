import junit.framework.Assert;

import org.antlr.runtime.CharStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unihamburg.informatik.nats.jwcdg.ForgettingANTLRInputStream;
import de.unihamburg.informatik.nats.jwcdg.antlr.Helpers;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarLexer;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarParser;
import de.unihamburg.informatik.nats.jwcdg.constraints.Constraint;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;

/**
 * 
 * @author Niels Beuck
 */
public class TestGrammarMakros {
	
	@Test
	public void testISA() { // and gradient, and edge
		String constr1 = 
				"{X!SYN} : 'mod-Distanz' : dist : [gradient(100)] :\n" + 
				"  spec(X^id) & edge(X,Modifikator)\n" + 
				"  ->\n" + 
				"\n" + 
				"  // PP-attachment wird schon von einem Prädiktor geregelt.\n" + 
				"  X.label = PP | X.label = KOM |\n" + 
				"\n" + 
				"  isa(X@,unterordnend) & isa(X^,finit) |\n" + 
				"  X@cat = KON & isa(X^,finit) |\n" + 
				"  isa(X@,finit) |\n" + 
				"  abs( distance( X@id, X^id ) ) <= 1;";
		String constr2 = 
				"{X!SYN} : 'mod-Distanz' : dist : [ 100 / [ 100 + abs( distance(X@id, X^id) ) ] ] :\n" + 
				"  spec(X^id) & subsumes(Label, Modifikator, X.label)\n" + 
				"  ->\n" + 
				"\n" + 
				"  // PP-attachment wird schon von einem Prädiktor geregelt.\n" + 
				"  X.label = PP | X.label = KOM |\n" + 
				"\n" + 
				"  subsumes(Features, unterordnend, X@cat) & subsumes(Features, finit, X^cat) |\n" + 
				"  X@cat = KON & subsumes(Features, finit, X^cat) |\n" + 
				"  subsumes(Features, finit, X@cat) |\n" + 
				"  abs( distance( X@id, X^id ) ) <= 1;";
		String constr3 = 
				"{X!SYN} : 'mod-Distanz' : dist : [gradient(100)] :\n" + 
				"  spec(X^id) & edge(X,Modifikator)\n" + 
				"  ->\n" + 
				"\n" + 
				"  // PP-attachment wird schon von einem Prädiktor geregelt.\n" + 
				"  X.label = PP | X.label = KOM |\n" + 
				"\n" + 
				"  isa(X@,unterordnend) & isa(X^,finit) |\n" + 
				"  X@cat = KON & isa2(X^,finit) |\n" + 
				"  isa(X@,finit) |\n" + 
				"  abs( distance( X@id, X^id ) ) <= 1;";
		try {
			Constraint c1 =Helpers.parserFromString(constr1).constraint().c;
			Constraint c2 =Helpers.parserFromString(constr2).constraint().c;
			String s1 = c1.getFormula().toString();
			String s2 = c2.getFormula().toString();
			String s3 = Helpers.parserFromString(constr3).constraint().c.getFormula().toString();
			
			Assert.assertEquals(c1.toString(), c2.toString());
			Assert.assertEquals(s1, s2);
			Assert.assertFalse(s1.equals(s3));
		} catch (RecognitionException e) {
			Assert.assertFalse(true);
		}
	}

	
	@Test
	public void testCheckGender() {
		String constr1 = 
				"	{X!SYN} : 'Adjektiv-Genus' : agree : 0.11 :\n" + 
				"		  X.label = ATTR & X@cat != ADJD &\n" + 
				"		  exists(X^gender) &\n" + 
				"		  exists(X@gender)\n" + 
				"		  ->\n" + 
				"		 \n" + 
				"		  phrasequotes(X^id) > phrasequotes(X@id) |\n" + 
				"\n" + 
				"		  check_gender(X^gender, X@gender) |\n" + 
				"\n" + 
				"		  // `Eine Million/fem Rechner/masc sind angekündigt.'\n" + 
				"		  X@cat = NN;\n" + 
				"";
		String constr2 = 
				"{X!SYN} : 'Adjektiv-Genus' : agree : 0.11 :\n" + 
				"  X.label = ATTR & X@cat != ADJD &\n" + 
				"  exists(X^gender) &\n" + 
				"  exists(X@gender)\n" + 
				"  ->\n" + 
				" \n" + 
				"  phrasequotes(X^id) > phrasequotes(X@id) |\n" + 
				"\n" + 
				"  (( compatible(Features, X^gender, X@gender) |\n" + 
				"    subsumes(Features, X^gender, masc) & subsumes(Features, X@gender, masc) |\n" + 
				"    subsumes(Features, X^gender, fem) & subsumes(Features, X@gender, fem) |\n" + 
				"    subsumes(Features, X^gender, neut) & subsumes(Features, X@gender, neut))) |\n" + 
				"\n" + 
				"  // `Eine Million/fem Rechner/masc sind angekündigt.'\n" + 
				"  X@cat = NN;\n" + 
				"";
		
		try {
			jwcdggrammarParser parser1 = Helpers.parserFromString(constr1);
			String s1 = parser1.constraint().c.getFormula().toString();
			String s2 = Helpers.parserFromString(constr2).constraint().c.getFormula().toString();
			
			Assert.assertEquals(s1, s2);
		} catch (RecognitionException e) {
			Assert.assertFalse(true);
		}
	}
	
	@Test
	public void testCheckCase() {
		String constr1 = 
				"{X!SYN} : 'Adjektiv-Kasus' : agree : 0.12 :\n" + 
				"  X.label = ATTR & X@cat != ADJD &\n" + 
				"  exists(X^case) &\n" + 
				"  exists(X@case)\n" + 
				"  ->\n" + 
				"\n" + 
				"  phrasequotes(X^id) > phrasequotes(X@id) |\n" + 
				"\n" + 
				"  check_case(X^case, X@case);";
		String constr2 = 
				"{X!SYN} : 'Adjektiv-Kasus' : agree : 0.12 :\n" + 
				"  X.label = ATTR & X@cat != ADJD &\n" + 
				"  exists(X^case) &\n" + 
				"  exists(X@case)\n" + 
				"  ->\n" + 
				"\n" + 
				"  phrasequotes(X^id) > phrasequotes(X@id) |\n" + 
				"\n" + 
				"  (( compatible(Features, X^case, X@case) |\n" + 
				"    subsumes(Features, dat, X^case) & subsumes(Features, dat, X@case) |\n" + 
				"    subsumes(Features, acc, X^case) & subsumes(Features, acc, X@case) |\n" + 
				"    subsumes(Features, nom, X^case) & subsumes(Features, nom, X@case) |\n" + 
				"    subsumes(Features, gen, X^case) & subsumes(Features, gen, X@case)));";
		
		try {
			jwcdggrammarParser parser1 = Helpers.parserFromString(constr1);
			String s1 = parser1.constraint().c.getFormula().toString();
			String s2 = Helpers.parserFromString(constr2).constraint().c.getFormula().toString();
			
			Assert.assertEquals(s1, s2);
		} catch (RecognitionException e) {
			Assert.assertFalse(true);
		}
	}
	
	
	@Test
	public void testTopicalized() {
		String constr1 = 
				"{X!SYN/\\Y!SYN} : 'ethischer Dativ nach Objekt' : order : 0.1 :\n" + 
				"		 X.label = ETH &\n" + 
				"		 (X\\ & Y\\ | X/ & Y/) &\n" + 
				"		 (edge(Y,Nominalobjekt) | Y.label = PRED | Y@word = das) &\n" + 
				"		 X@from > Y@from\n" + 
				"		 ->\n" + 
				"		 Y.label = SUBJ | \n" + 
				"		 Y@cat = PPER | \n" + 
				"		 (( Y@cat = PWS | Y@cat = PRELS | Y@cat = PWAT | Y@cat = PWAV | Y@case = PRELAT |\n" + 
				"		    has(Y@id, find_initial, Label, Skopus))) | \n" + 
				"		 topicalized(Y) |\n" + 
				"		 X@cat = PRF | \n" + 
				"		 Y@cat = PRF;";
		String constr2 = 
				"{X!SYN/\\Y!SYN} : 'ethischer Dativ nach Objekt' : order : 0.1 :\n" + 
				" X.label = ETH &\n" + 
				" (X\\ & Y\\ | X/ & Y/) &\n" + 
				" (subsumes(Label, Nominalobjekt, Y.label) | Y.label = PRED | Y@word = das) &\n" + 
				" X@from > Y@from\n" + 
				" ->\n" + 
				" Y.label = SUBJ | \n" + 
				" Y@cat = PPER | \n" + 
				" (( Y@ cat = PWS | Y@ cat = PRELS | Y@ cat = PWAT | Y@ cat = PWAV | Y@ case = PRELAT |\n" + 
				"    has(Y@ id, find_initial, Label, Skopus))) | (parent(Y^id, AUX) > Y@from & parent(Y^id, AUX) < Y^from |\n" + 
				"   parent(Y^id, OBJI) > Y@from & parent(Y^id, OBJI) < Y^from)\n" + 
				" |\n" + 
				" X@cat = PRF | \n" + 
				" Y@cat = PRF;";
		
		try {
			jwcdggrammarParser parser1 = Helpers.parserFromString(constr1);
			String s1 = parser1.constraint().c.getFormula().toString();
			String s2 = Helpers.parserFromString(constr2).constraint().c.getFormula().toString();
			
			Assert.assertEquals(s1, s2);
		} catch (RecognitionException e) {
			Assert.assertFalse(true);
		}
	}
	
	@Test
	public void testPartizipialadjektiv() {
		String constr1 = 
				"{X:SYN,Y!SYN} : 'irreführende Präposition' : shallow : 0.1 :\n" + 
				"  ~virtual(Y@id,\"misc\") & \n" + 
				"  X@to = Y@from &\n" + 
				"  (X@cat = APPR | X@cat = APPRART) & \n" + 
				"  Y@cat = NN\n" + 
				"  ->\n" + 
				"  X/Y | \n" + 
				"  X.label = CJ | \n" + 
				"  Y.label = GMOD | \n" + 
				"  Y.label = ATTR |\n" + 
				"  Y! & Partizipialadjektiv(Y^);";
		String constr2 = 
				"{X:SYN,Y!SYN} : 'irreführende Präposition' : shallow : 0.1 :\n" + 
				"  ~virtual(Y@id,\"misc\") & \n" + 
				"  X@to = Y@from &\n" + 
				"  (X@cat = APPR | X@cat = APPRART) & \n" + 
				"  Y@cat = NN\n" + 
				"  ->\n" + 
				"  X/Y | \n" + 
				"  X.label = CJ | \n" + 
				"  Y.label = GMOD | \n" + 
				"  Y.label = ATTR |\n" + 
				"  Y! & ((  exists(Y^ partizipial1) | exists(Y^ partizipial2) ))\n" + 
				";";
		
		try {
			jwcdggrammarParser parser1 = Helpers.parserFromString(constr1);
			String s1 = parser1.constraint().c.getFormula().toString();
			String s2 = Helpers.parserFromString(constr2).constraint().c.getFormula().toString();
			
			Assert.assertEquals(s1, s2);
		} catch (RecognitionException e) {
			Assert.assertFalse(true);
		}
	}
	
	@Test
	public void testCaseSpecified() {
		String constr1 = 
				"{X/SYN\\Y!SYN} : 'DET-PN-Kasus' : agree : 0.1 :\n" + 
				" (X.label = DET | X.label = ATTR) &\n" + 
				"  Y.label=PN & exists(Y^case) &\n" + 
				" ~case_specified(X^) &\n" + 
				" exists(X@case) \n" + 
				" ->\n" + 
				"\n" + 
				" //  `Er umschrieb das Ambiente mit \"gediegene Eleganz\".'\n" + 
				" phrasequotes(X@id) > phrasequotes(Y^id) |\n" + 
				"\n" + 
				" check_case(X@case, Y^case) |\n" + 
				"\n" + 
				" // was für ein...\n" + 
				" Y^word = für & (X@definite=no | X@cat = ADJA);";
		String constr2 = 
				"{X/SYN\\Y!SYN} : 'DET-PN-Kasus' : agree : 0.1 :\n" + 
				" (X.label = DET | X.label = ATTR) &\n" + 
				"  Y.label=PN & exists(Y^case) &\n" + 
				" ~((X^ case=nom | X^case = gen | X^ case = dat | X^ case = acc)) &\n" + 
				" exists(X@case) \n" + 
				" ->\n" + 
				"\n" + 
				" //  `Er umschrieb das Ambiente mit \"gediegene Eleganz\".'\n" + 
				" phrasequotes(X@id) > phrasequotes(Y^id) |\n" + 
				"\n" + 
				" (( compatible(Features, X@case, Y^case) |\n" + 
				"    subsumes(Features, dat, X@case) & subsumes(Features, dat, Y^case) |\n" + 
				"    subsumes(Features, acc, X@case) & subsumes(Features, acc, Y^case) |\n" + 
				"    subsumes(Features, nom, X@case) & subsumes(Features, nom, Y^case) |\n" + 
				"    subsumes(Features, gen, X@case) & subsumes(Features, gen, Y^case))) |\n" + 
				"\n" + 
				" // was für ein...\n" + 
				" Y^word = für & (X@definite=no | X@cat = ADJA);";
		try {
			jwcdggrammarParser parser1 = Helpers.parserFromString(constr1);
			String s1 = parser1.constraint().c.getFormula().toString();
			String s2 = Helpers.parserFromString(constr2).constraint().c.getFormula().toString();
			
			Assert.assertEquals(s1, s2);
		} catch (RecognitionException e) {
			Assert.assertFalse(true);
		}
	}
	
	@Test
	public void testKON_PP() {
		String constr1 = 
				"{X\\SYN/Y\\SYN} : 'KOKOM-PP-Kategorie' : init : 0.0 :\n" + 
				" X.label = KOM & Y.label = CJ &\n" + 
				" isa(X^,Adposition)\n" + 
				" ->\n" + 
				" isa(Y@,Adverbial) |\n" + 
				" KON_PP(X^);";
		String constr2 = 
				"{X\\SYN/Y\\SYN} : 'KOKOM-PP-Kategorie' : init : 0.0 :\n" + 
				" X.label = KOM & Y.label = CJ &\n" + 
				" subsumes(Features, Adposition, X^cat)\n" + 
				" ->\n" + 
				" subsumes(Features, Adverbial, Y@cat) |\n" + 
				" ((X^ cat = APPR & exists(X^ cat2) & X^ cat2 = KON));";
		try {
			jwcdggrammarParser parser1 = Helpers.parserFromString(constr1);
			String s1 = parser1.constraint().c.getFormula().toString();
			String s2 = Helpers.parserFromString(constr2).constraint().c.getFormula().toString();
			
			Assert.assertEquals(s1, s2);
		} catch (RecognitionException e) {
			Assert.assertFalse(true);
		}
	}
	
	@Test
	public void testQuoted() {
		String constr1 = 
				"{X\\SYN} : 'Vergleich von Satz mit \"wie\"' : lexical : 0.4 :\n" + 
				"  X.label = CJ & X^word = wie & isa(X@,finit)\n" + 
				"  ->\n" + 
				"\n" + 
				"  // `Dort standen Parolen wie \"Kohl gewinnt die Wahl!\".'\n" + 
				"  quoted(X) |\n" + 
				"\n" + 
				"  // `Es fühlt sich an, wie wenn man in ein Dimensionsloch fällt.'\n" + 
				"  has(X@id, KONJ);";
		String constr2 = 
				"{X\\SYN} : 'Vergleich von Satz mit \"wie\"' : lexical : 0.4 :\n" + 
				"  X.label = CJ & X^word = wie & subsumes(Features, finit, X@cat)\n" + 
				"  ->\n" + 
				"\n" + 
				"  // `Dort standen Parolen wie \"Kohl gewinnt die Wahl!\".'\n" + 
				"  ((phrasequotes(X @id) > phrasequotes(X ^id))) |\n" + 
				"\n" + 
				"  // `Es fühlt sich an, wie wenn man in ein Dimensionsloch fällt.'\n" + 
				"  has(X@id, KONJ);";
		try {
			jwcdggrammarParser parser1 = Helpers.parserFromString(constr1);
			String s1 = parser1.constraint().c.getFormula().toString();
			String s2 = Helpers.parserFromString(constr2).constraint().c.getFormula().toString();
			
			Assert.assertEquals(s1, s2);
		} catch (RecognitionException e) {
			Assert.assertFalse(true);
		}
	}
}
