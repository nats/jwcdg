/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.antlr.runtime.RecognitionException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNet;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNetState;
import de.unihamburg.informatik.nats.jwcdg.constraints.Constraint;
import de.unihamburg.informatik.nats.jwcdg.evaluation.CachedGrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.frobbing.CombinedFrobber;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;
import de.unihamburg.informatik.nats.jwcdg.lattice.Lattice;
import de.unihamburg.informatik.nats.jwcdg.transform.ProblemSolver;
import de.unihamburg.informatik.nats.jwcdg.antlr.Helpers;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarParser;

/**
 *
 * @author Arne Köhn
 */
public class TestOnToyData {
	
	public TestOnToyData() {
	}

	@BeforeClass
	public static void setUpClass() throws Exception {
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
	}
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}
	
	@Test
	public void testParseRealGrammar() {
		//InputStream is = getClass().getResourceAsStream("testgrammar.cdg");
		try {
			FileReader is = new FileReader("src/test/resources/testgrammar.cdg");
			jwcdggrammarParser p = Helpers.parserFromIS(is);
		
			Grammar g = p.cdggrammar();
			assertEquals(g.getLevels().size(), 1);
			assertEquals(g.findLevel("SYN").getNo(),0);
			Constraint c = g.findConstraint("DET-Unterordnung");
			assertEquals(c.getPenalty(), 0.0, 0.001);
		} catch (RecognitionException ex) {
			assertTrue("Exception in parsing the testgrammar",false);
		} catch (FileNotFoundException e) {
			assertTrue("Exception in parsing the testgrammar",false);
		}
	}
	
	@Test
	public void testBuildConstraintNet() {
		try {
			FileReader is = new FileReader("src/test/resources/testgrammar.cdg");
			jwcdggrammarParser pGram = Helpers.parserFromIS(is);
			FileReader is2 = new FileReader("src/test/resources/toylex.cdg");
			jwcdggrammarParser pLex = Helpers.parserFromIS(is2);
		
			@SuppressWarnings("rawtypes")
			HashMap lexiconItems = new HashMap();
			Grammar g = pGram.cdggrammar();
			pLex.lexicon(lexiconItems);
			
			//List ltest = (List)(lexiconItems.get("\""));
			
			Configuration config = new Configuration();
			//config.setDefaultValues();
			
			g.cache(config);
			@SuppressWarnings("unchecked")
			Lexicon l = new Lexicon(lexiconItems, g.getAllAttributes());
			
			ArrayList<String> tokens = new ArrayList<String>();
			tokens.add("Peter");
			tokens.add("kauft");
			tokens.add("ein");
			tokens.add("Buch");
			Lattice lat = new Lattice(tokens);
			
			GrammarEvaluator evaluator = new CachedGrammarEvaluator(g, config);
			ConstraintNet net = new ConstraintNet(lat, l, true, true, evaluator, config);
			ConstraintNetState state = new ConstraintNetState(net, config);
			
			ProblemSolver frobber = new CombinedFrobber(state, config, evaluator, 0.0);
			frobber.solve();
				
		} catch (RecognitionException e) {
			assertTrue("Exception in parsing the testgrammar",false);
		} catch (FileNotFoundException e) {
			assertTrue("Exception in parsing the testgrammar",false);
		}
	}
}
