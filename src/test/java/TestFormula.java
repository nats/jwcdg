/**
 * This is the unit test for formula evaluation
 * 
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.antlr.runtime.RecognitionException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unihamburg.informatik.nats.jwcdg.antlr.Helpers;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarParser;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraints.formulas.Formula;
import de.unihamburg.informatik.nats.jwcdg.constraints.formulas.FormulaAnd;
import de.unihamburg.informatik.nats.jwcdg.constraints.formulas.FormulaFalse;
import de.unihamburg.informatik.nats.jwcdg.constraints.formulas.FormulaImpl;
import de.unihamburg.informatik.nats.jwcdg.constraints.formulas.FormulaTrue;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;
import de.unihamburg.informatik.nats.jwcdg.input.LexiconItem;

/**
 *
 * @author Arne Köhn
 */
public class TestFormula {

	public TestFormula() {
	}

	@BeforeClass
	public static void setUpClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Just test the simple formula constructs
	 * TODO #TEST should every formula be tested?
	 */
	@Test
	public void testSimpleFormula() {
		Formula t = new FormulaTrue();
		Formula f = new FormulaFalse();
		assertTrue("True should evaluate to true", t.evaluate(new LevelValue[1], null, null));
		assertFalse("False should evaluate to false",f.evaluate(new LevelValue[1], null, null));
		FormulaAnd a = new FormulaAnd();
		a.addSubFormula(t);
		assertTrue("and(t) should be true", a.evaluate(new LevelValue[1], null, null));
		a.addSubFormula(f);
		assertFalse("and(t,f) should be false", a.evaluate(new LevelValue[1], null, null));
		Formula i = new FormulaImpl(f,t);
		assertTrue("f->t should be true", i.evaluate(new LevelValue[1], null, null));
	}


	@Test
	public void testFunctionParsing() {
		String input = "(true | false) <-> ~false";
		try {
			Formula f1 = Helpers.parserFromString(input).formula();
			assertTrue(f1.evaluate(new LevelValue[1], null, null));
			Formula f2 = Helpers.parserFromString(
							"~true | false | (false -> true)").formula();
			assertTrue(f2.evaluate(new LevelValue[1], null, null));
		} catch (RecognitionException ex) {
//			LogManager.getLogger(TestFormula.class.getName()).log(Level.SEVERE, null, ex);
			assertTrue(false);
		}
	}

//	@Test
//	public void testParseConstraints() {
//		String s1 = "{X/SYN} : 'Modifikator steht links' : order : [ X@leftpenalty ] : spec(X^id) & X.label = ADV & ~subsumes(Features, Verb, X^cat) & X^cat != ADJA ->   ~exists(X@leftpenalty);";
//		String s2 = "{X|SYN} : acc  : pref : 0.999  : ~(X@case = acc);";
////		try {
//			Map foo = new HashMap();
//			Level level = new Level("SYN", foo, null, null);
//			foo.put("SYN", level);
//			// TODO #TESTING map erstellen und testen
////			Constraint c1 = TestHelpers.parserFromString(s1).constraint(foo).c;
////			Constraint c2 = TestHelpers.parserFromString(s2).constraint(foo).c;
////			assertEquals(c2.getPenalty(),0.999,0.0001);
////		} catch (RecognitionException ex) {
////			assertTrue(false);
////		}
//	}
	
	@Test
	public void testGrammarFile() {
		FileReader is;
		try {
			is = new FileReader(getClass().getResource("/testgrammar.cdg").getFile());
			jwcdggrammarParser p = Helpers.parserFromIS(is);
			Grammar s = p.cdggrammar();
			assertTrue(s != null);
		} catch (RecognitionException ex) {
			assertTrue(false);
		} catch (FileNotFoundException e) {
			assertTrue(false);
		}
	}

	
	@Test
	public void testParseLexiconFile() {
		try {
			FileReader is = new FileReader(getClass().getResource("/toylex.cdg").getFile());
			jwcdggrammarParser p = Helpers.parserFromIS(is);
			@SuppressWarnings("rawtypes")
			HashMap lexiconItems = new HashMap();
			p.lexicon(lexiconItems);
			@SuppressWarnings("unchecked")
			Lexicon l = new Lexicon(lexiconItems, new ArrayList<Path>());
			assertTrue(l != null);
		} catch (RecognitionException ex) {
			assertTrue(false);
		} catch (FileNotFoundException e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testParseLexicon() {
		try {
			String s1 = "an     := [ cat:APZR, prep:an ]";
			String s2 = "cat";
			String s3 = "'abschreck\\'':=[base:abschrecken,cat:VVFIN,pos:SOV,avz:forbidden,person:first,number:sg,tense:present,mood:indicative,perfect:haben,valence:'a+ip?',objp:von,avz_valence:'a+ip?'];";
			jwcdggrammarParser p = Helpers.parserFromString(s1);
			LexiconItem l1 = p.lexiconItem();
			assertEquals(l1.getWord(),"an");

			jwcdggrammarParser p3 = Helpers.parserFromString(s3);
			LexiconItem l3 = p3.lexiconItem();
			assertTrue(l3 != null);
			
			Path path = Helpers.parserFromString(s2).path();
			assertTrue(l1.getFeature(path) instanceof AVMString);
			assertEquals(((AVMString) l1.getFeature(path)).getString(),"APZR");
		} catch (RecognitionException ex) {
//			LogManager.getLogger(TestFormula.class.getName()).log(Level.SEVERE, null, ex);
			assertEquals(true,false);
		}
	}
}
