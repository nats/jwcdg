import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unihamburg.informatik.nats.jwcdg.input.Hierarchy;
import de.unihamburg.informatik.nats.jwcdg.input.Sort;
import de.unihamburg.informatik.nats.jwcdg.input.SourceInfo;



public class TestHierarchies {

	Hierarchy h;
	String a = "A", b="B", c="C";

	
	public TestHierarchies() {
	}

	@BeforeClass
	public static void setUpClass() throws Exception {

	}

	@Before
	public void setUp() {
		Sort sb= new Sort(b, new ArrayList<Sort>());
		Sort sc= new Sort(c, new ArrayList<Sort>());
		List<Sort> l = new ArrayList<Sort>();
		l.add(sb);
		l.add(sc);
		Sort sa = new Sort(a, l);
		h= new Hierarchy("test", sa, new SourceInfo());
	}
		

	@After
	public void tearDown() {
	}

	@Test
	public void testSubsumption() {
		assertTrue(h.contains(Hierarchy.TOP));
		assertTrue(h.contains(Hierarchy.BOTTOM));
		assertTrue(h.subsumes(a, b));
		assertTrue(h.subsumes(a, c));
		assertTrue(h.subsumes(Hierarchy.TOP, b));
		assertTrue(h.subsumes(a, Hierarchy.BOTTOM));
		
		assertFalse(h.subsumes(b, c));
		assertFalse(h.subsumes(c, Hierarchy.TOP));
		assertFalse(h.subsumes(Hierarchy.BOTTOM, a));
	}
}
