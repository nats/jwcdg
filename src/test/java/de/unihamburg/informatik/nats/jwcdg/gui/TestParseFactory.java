package de.unihamburg.informatik.nats.jwcdg.gui;
import java.io.IOException;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;

import de.unihamburg.informatik.nats.jwcdg.DefaultParserFactory;
import de.unihamburg.informatik.nats.jwcdg.JWCDG;
import de.unihamburg.informatik.nats.jwcdg.Parser;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarLexer;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarParser;
import de.unihamburg.informatik.nats.jwcdg.evaluation.CachedGrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;


/**
 * This class provides a static methods for generating 
 * example instances of the parse class.
 * 
 * For this, there are several dependency structures in the form of 
 * string constants.
 * 
 *  Simple give one of these constants as argument to the
 *  generateParseObject method. 
 *
 * @author Niels Beuck
 */
public class TestParseFactory {
	
	public static Parse generateParseObject(String cda) throws RecognitionException {
		jwcdggrammarParser gParser = new jwcdggrammarParser(new CommonTokenStream(new jwcdggrammarLexer(new ANTLRStringStream(cda))));
		
		Parse p = gParser.annoEntry();
		
		return p;
	}
	
	public static DecoratedParse generateDecoratedParseObject(String cda, Configuration config) 
			throws IOException, RecognitionException 
	{
		
		Parser parser = JWCDG.makeParser(config, new DefaultParserFactory());
		jwcdggrammarParser gParser = new jwcdggrammarParser(new CommonTokenStream(new jwcdggrammarLexer(new ANTLRStringStream(cda))));
		
		Parse p = gParser.annoEntry();
		
		DecoratedParse dp = p.decorate(
				new CachedGrammarEvaluator(parser.getGrammar(), parser.getConfig()), 
				parser.getLexicon(), parser.getConfig());
		
		return dp;	
	}
	
	public static final String PARSE_0 = "'parse0' : parse0 <->\n" + 
			"/*\n" + 
			"Search Strategy : Frobbing\n" + 
			"Processing Time : 861\n" + 
			"Frobbing Method : threefold\n" + 
			"score : 0,9980\n" + 
			"*/\n" + 
			"  0 1 dies\n" + 
			" SYN ->  SUBJ -> 2 // ist\n" + 
			" REF ->    '' -> 0\n" + 
			"        gender / neut\n" + 
			"        person / third\n" + 
			"        number / sg\n" + 
			"           cat / PDS\n" + 
			"          case / nom\n" + 
			",\n" + 
			"  1 2 ist\n" + 
			" SYN ->     S -> 0\n" + 
			" REF ->    '' -> 0\n" + 
			"          mood / indicative\n" + 
			"          base / sein\n" + 
			"           cat / VAFIN\n" + 
			"        number / sg\n" + 
			"        person / third\n" + 
			"         tense / present\n" + 
			",\n" + 
			"  2 3 ein\n" + 
			" SYN ->   DET -> 4 // Test\n" + 
			" REF ->    '' -> 0\n" + 
			"        gender / masc\n" + 
			"           cat / ART\n" + 
			"        number / sg\n" + 
			"          case / nom\n" + 
			",\n" + 
			"  3 4 Test\n" + 
			" SYN ->  PRED -> 2 // ist\n" + 
			" REF ->    '' -> 0\n" + 
			"          base / Test\n" + 
			"        gender / masc\n" + 
			"        person / third\n" + 
			"           cat / NN\n" + 
			"        number / sg\n" + 
			"          case / 'nom_dat_acc'\n" + 
			";\n";
	
	public static final String PARSE_1 = "'parse1' : parse1 <->\n" + 
			"/*\n" + 
			"Search Strategy : Frobbing\n" + 
			"Processing Time : 28266\n" + 
			"Frobbing Method : threefold\n" + 
			"score : 0,0988\n" + 
			"*/\n" + 
			"  0 1 Dieser\n" + 
			" SYN ->   DET -> 2 // Satz\n" + 
			" REF ->    '' -> 0\n" + 
			"          base / dies\n" + 
			"        gender / masc\n" + 
			"           cat / PDAT\n" + 
			"        number / sg\n" + 
			"          case / nom\n" + 
			",\n" + 
			"  1 2 Satz\n" + 
			" SYN ->  SUBJ -> 3 // ist\n" + 
			" REF ->    '' -> 0\n" + 
			"          base / Satz\n" + 
			"        gender / masc\n" + 
			"        person / third\n" + 
			"           cat / NN\n" + 
			"        number / sg\n" + 
			"          case / 'nom_dat_acc'\n" + 
			",\n" + 
			"  2 3 ist\n" + 
			" SYN ->     S -> 0\n" + 
			" REF ->    '' -> 0\n" + 
			"          mood / indicative\n" + 
			"          base / sein\n" + 
			"           cat / VAFIN\n" + 
			"        number / sg\n" + 
			"        person / third\n" + 
			"         tense / present\n" + 
			",\n" + 
			"  3 4 etwas\n" + 
			" SYN ->   ADV -> 5 // länger\n" + 
			" REF ->    '' -> 0\n" + 
			"        subcat / grade\n" + 
			"           cat / ADV\n" + 
			",\n" + 
			"  4 5 'länger'\n" + 
			" SYN ->  PRED -> 3 // ist\n" + 
			" REF ->    '' -> 0\n" + 
			"          base / lang\n" + 
			"           cat / ADJA\n" + 
			"        degree / comparative\n" + 
			",\n" + 
			"  5 6 und\n" + 
			" SYN ->   KON -> 5 // länger\n" + 
			" REF ->    '' -> 0\n" + 
			"          base / und\n" + 
			"           cat / KON\n" + 
			",\n" + 
			"  6 7 komplizierter\n" + 
			" SYN ->    CJ -> 6 // und\n" + 
			" REF ->    '' -> 0\n" + 
			"           cat / ADJA\n" + 
			",\n" + 
			"  7 8 '.'\n" + 
			" SYN ->    '' -> 0\n" + 
			" REF ->    '' -> 0\n" + 
			"           cat / '$.'\n" + 
			";";
	
	public static final String PARSE_2 = "'parse2' : parse2 <->\n" + 
			"  0 1 Peter\n" + 
			" SYN ->  SUBJ -> 2 // hat\n" + 
			" REF ->    '' -> 0\n" + 
			"        person / third\n" + 
			"           cat / NE\n" + 
			"        number / sg\n" + 
			"        subcat / Nachname\n" + 
			",\n" + 
			"  1 2 hat\n" + 
			" SYN ->     S -> 0\n" + 
			" REF ->    '' -> 0\n" + 
			"          mood / indicative\n" + 
			"          base / haben\n" + 
			"           cat / VAFIN\n" + 
			"        number / sg\n" + 
			"        person / third\n" + 
			"         tense / present\n" + 
			",\n" + 
			"  2 3 den\n" + 
			" SYN ->   DET -> 4 // Wagen\n" + 
			" REF ->    '' -> 0\n" + 
			"        gender / masc\n" + 
			"        number / sg\n" + 
			"           cat / ART\n" + 
			"          case / acc\n" + 
			",\n" + 
			"  3 4 Wagen\n" + 
			" SYN ->  OBJA -> 5 // gekauft\n" + 
			" REF ->    '' -> 0\n" + 
			"          base / Wagen\n" + 
			"        gender / masc\n" + 
			"        person / third\n" + 
			"           cat / NN\n" + 
			"        number / sg\n" + 
			"          case / 'nom_dat_acc'\n" + 
			",\n" + 
			"  4 5 gekauft\n" + 
			" SYN ->   AUX -> 2 // hat\n" + 
			" REF ->    '' -> 0\n" + 
			"          base / kaufen\n" + 
			"           cat / VVPP\n" +  
			",\n" + 
			"  5 6 ','\n" + 
			" SYN ->    '' -> 0\n" + 
			" REF ->    '' -> 0\n" + 
			"           cat / '$,'\n" + 
			",\n" + 
			"  6 7 den\n" + 
			" SYN ->  OBJA -> 11 // angeschaut\n" + 
			" REF ->    '' -> 4 // Wagen\n" + 
			"        gender / masc\n" + 
			"        person / third\n" + 
			"        number / sg\n" + 
			"           cat / PRELS\n" + 
			"          case / acc\n" + 
			",\n" + 
			"  7 8 er\n" + 
			" SYN ->  SUBJ -> 12 // hatte\n" + 
			" REF ->    '' -> 0\n" + 
			"        gender / masc\n" + 
			"        person / third\n" + 
			"        number / sg\n" + 
			"           cat / PPER\n" + 
			"          case / nom\n" + 
			",\n" + 
			"  8 9 sich\n" + 
			" SYN ->  OBJD -> 11 // angeschaut\n" + 
			" REF ->    '' -> 1 // Peter\n" + 
			"        person / third\n" + 
			"           cat / PRF\n" + 
			"          case / dat\n" + 
			",\n" + 
			"  9 10 gestern\n" + 
			" SYN ->   ADV -> 11 // angeschaut\n" + 
			" REF ->    '' -> 0\n" + 
			"        subcat / temporal\n" + 
			"           cat / ADV\n" + 
			",\n" + 
			"  10 11 angeschaut\n" + 
			" SYN ->   AUX -> 12 // hatte\n" + 
			" REF ->    '' -> 0\n" + 
			"          base / anschauen\n" + 
			"           cat / VVPP\n" +  
			",\n" + 
			"  11 12 hatte\n" + 
			" SYN ->   REL -> 4 // Wagen\n" + 
			" REF ->    '' -> 0\n" + 
			"          mood / indicative\n" + 
			"          base / haben\n" + 
			"           cat / VAFIN\n" + 
			"        number / sg\n" + 
			"        person / third\n" + 
			"         tense / past\n" + 
			",\n" + 
			"  12 13 '.'\n" + 
			" SYN ->    '' -> 0\n" + 
			" REF ->    '' -> 0\n" + 
			"           cat / '$.'\n" +
			";";
	
	public static final String PARSE_CIRCLE = "'parse2' : parse2 <->\n" + 
			"  0 1 und\n" + 
			" SYN ->  S -> 2 // im\n" +
			" REF ->    '' -> 0\n" +
			",\n" + 
			"  1 2 im\n" + 
			" SYN ->     S -> 3 // Kreis\n" +
			" REF ->    '' -> 0\n" +
			",\n" + 
			"  2 3 Kreis\n" + 
			" SYN ->   S -> 1 // und\n" +
			" REF ->    '' -> 0\n" +
			";";
	
	// used to test ErrorChecker. please do not change
	public static final String PARSE_ERRORC_1 = "'parse17' : 'parse17' <->\n" +
				"/*\n" +
				"Search Strategy : null\n" +
				"Processing Time : 0\n" +
				"score : 0,4995\n" +
				"*/\n" +
				"  0 1 Der\n" +
				" SYN ->   DET -> 2 // Hund\n" +
				" REF ->    '' -> 0\n" +
				"        gender / masc\n" +
				"        number / sg\n" +
				"           cat / ART\n" +
				"          case / nom\n" +
				",\n" +
				"  1 2 Hund\n" +
				" SYN ->  SUBJ -> 3 // bellen\n" +
				" REF ->    '' -> 0\n" +
				"        gender / bot\n" +
				"        person / third\n" +
				"           cat / NE\n" +
				"        number / sg\n" +
				"          case / bot\n" +
				"        subcat / Nachname\n" +
				",\n" +
				"  2 3 bellen\n" +
				" SYN ->     S -> 0\n" +
				" REF ->    '' -> 0\n" +
				"          mood / indicative\n" +
				"          base / bellen\n" +
				"        person / third\n" +
				"           cat / VVFIN\n" +
				"        number / pl\n" +
				"         tense / present\n" +
				",\n" +
				"  3 4 '.'\n" +
				" SYN ->    '' -> 0\n" +
				" REF ->    '' -> 0\n" +
				"           cat / '$.'\n" +
				";\n";

	public static final String PARSE_ERRORC_2 = "'parse14' : 'parse14' <->\n" +
				"/*\n" +
				"Search Strategy : null\n" +
				"Processing Time : 0\n" +
				"score : 0,1000\n" +
				"*/\n" +
				"  0 1 Der\n" +
				" SYN ->   DET -> 2 // Hund\n" +
				" REF ->    '' -> 0\n" +
				"        gender / masc\n" +
				"        number / sg\n" +
				"           cat / ART\n" +
				"          case / nom\n" +
				",\n" +
				"  1 2 Hund\n" +
				" SYN ->  SUBJ -> 3 // bellst\n" +
				" REF ->    '' -> 0\n" +
				"        gender / bot\n" +
				"        person / third\n" +
				"           cat / NE\n" +
				"        number / sg\n" +
				"          case / bot\n" +
				"        subcat / Nachname\n" +
				",\n" +
				"  2 3 bellst\n" +
				" SYN ->     S -> 0\n" +
				" REF ->    '' -> 0\n" +
				"          mood / indicative\n" +
				"          base / bellen\n" +
				"        person / second\n" +
				"           cat / VVFIN\n" +
				"        number / sg\n" +
				"         tense / present\n" +
				",\n" +
				"  3 4 '.'\n" +
				" SYN ->    '' -> 0\n" +
				" REF ->    '' -> 0\n" +
				"           cat / '$.'\n" +
				";\n";
	
	public static final String PARSE_ERRORC_3 = "'parse22' : 'parse22' <->\n" +
				"/*\n" +
				"Search Strategy : null\n" +
				"Processing Time : 0\n" +
				"score : 0,0995\n" +
				"*/\n" +
				"  0 1 Der\n" +
				" SYN ->   DET -> 2 // Hund\n" +
				" REF ->    '' -> 0\n" +
				"        gender / masc\n" +
				"        number / sg\n" +
				"           cat / ART\n" +
				"          case / nom\n" +
				",\n" +
				"  1 2 Hund\n" +
				" SYN ->  SUBJ -> 3 // geht\n" +
				" REF ->    '' -> 0\n" +
				"        gender / bot\n" +
				"        person / third\n" +
				"           cat / NE\n" +
				"        number / sg\n" +
				"          case / bot\n" +
				"        subcat / Nachname\n" +
				",\n" +
				"  2 3 geht\n" +
				" SYN ->     S -> 0\n" +
				" REF ->    '' -> 0\n" +
				"           cat / VVFIN\n" +
				"        person / third\n" +
				"         tense / present\n" +
				"       valence / '-'\n" +
				"          mood / indicative\n" +
				"          base / gehen\n" +
				"        stress / none\n" +
				"        number / sg\n" +
				",\n" +
				"  3 4 und\n" +
				" SYN ->   KON -> 3 // geht\n" +
				" REF ->    '' -> 0\n" +
				"          base / und\n" +
				"           cat / KON\n" +
				",\n" +
				"  4 5 bellst\n" +
				" SYN ->    CJ -> 4 // und\n" +
				" REF ->    '' -> 0\n" +
				"          mood / indicative\n" +
				"          base / bellen\n" +
				"        person / second\n" +
				"           cat / VVFIN\n" +
				"        number / sg\n" +
				"         tense / present\n" +
				",\n" +
				"  5 6 '.'\n" +
				" SYN ->    '' -> 0\n" +
				" REF ->    '' -> 0\n" +
				"           cat / '$.'\n" +
				";\n";

	// used to test AnnoViewer. please do not change
	public static final String PARSE_ANNOV_CTOK_1 = "'parse0' : 'parse0' <->\n" +
				"/*\n" +
				"Search Strategy : null\n" +
				"Processing Time : 0\n" +
				"score : 0,0000\n" +
				"*/\n" +
				"  0 1 Dieser\n" +
				" SYN ->   DET -> 2 // Satz\n" +
				" REF ->    '' -> 0\n" +
				"          base / dies\n" +
				"        gender / masc\n" +
				"        number / sg\n" +
				"           cat / PDAT\n" +
				"          case / nom\n" +
				",\n" +
				"  1 2 Satz\n" +
				" SYN ->  SUBJ -> 3 // ist\n" +
				" REF ->    '' -> 0\n" +
				"          base / Satz\n" +
				"        gender / masc\n" +
				"        person / third\n" +
				"           cat / NN\n" +
				"        number / sg\n" +
				"          case / 'nom_dat_acc'\n" +
				",\n" +
				"  2 3 ist\n" +
				" SYN ->     S -> 0\n" +
				" REF ->    '' -> 0\n" +
				"           cat / VAFIN\n" +
				"        person / third\n" +
				"         tense / present\n" +
				"       valence / e\n" +
				"          mood / indicative\n" +
				"          base / sein\n" +
				"        stress / none\n" +
				"        number / sg\n" +
				",\n" +
				"  3 4 ctok\n" +
				" SYN ->   ADV -> 3 // ist\n" +
				" REF ->    '' -> 0\n" +
				"       pattern / ADJD\n" +
				"           cat / ADJD\n" +
				"        degree / positive\n" +
				",\n" +
				"  4 5 Satz\n" +
				" SYN ->  PRED -> 4 // ctok\n" +
				" REF ->    '' -> 0\n" +
				"          base / Satz\n" +
				"        gender / masc\n" +
				"        person / third\n" +
				"           cat / NN\n" +
				"        number / sg\n" +
				"          case / 'nom_dat_acc'\n" +
				",\n" +
				"  5 6 '1'\n" +
				" SYN ->   APP -> 5 // Satz\n" +
				" REF ->    '' -> 0\n" +
				"        gender / bot\n" +
				"        person / third\n" +
				"        number / sg\n" +
				"           cat / CARD\n" +
				"          case / bot\n" +
				"       pattern / arabisch\n" +
				",\n" +
				"  6 7 '.'\n" +
				" SYN ->    '' -> 0\n" +
				" REF ->    '' -> 0\n" +
				"           cat / '$.'\n" +
				";\n";

	public static final String PARSE_ANNOV_ZH1_1 = "'parse2' : 'parse2' <->\n" +
				"/*\n" +
				"Search Strategy : null\n" +
				"Processing Time : 0\n" +
				"score : 0,6865\n" +
				"*/\n" +
				"  0 1 Dieser\n" +
				" SYN ->   DET -> 2 // Satz\n" +
				" REF ->    '' -> 0\n" +
				"          base / dies\n" +
				"        gender / masc\n" +
				"        number / sg\n" +
				"           cat / PDAT\n" +
				"          case / nom\n" +
				",\n" +
				"  1 2 Satz\n" +
				" SYN ->  SUBJ -> 3 // ist\n" +
				" REF ->    '' -> 0\n" +
				"          base / Satz\n" +
				"        gender / masc\n" +
				"        person / third\n" +
				"           cat / NN\n" +
				"        number / sg\n" +
				"          case / 'nom_dat_acc'\n" +
				",\n" +
				"  2 3 ist\n" +
				" SYN ->     S -> 0\n" +
				" REF ->    '' -> 0\n" +
				"           cat / VAFIN\n" +
				"        person / third\n" +
				"         tense / present\n" +
				"       valence / e\n" +
				"          mood / indicative\n" +
				"          base / sein\n" +
				"        stress / none\n" +
				"        number / sg\n" +
				",\n" +
				"  3 4 'zh1'\n" +
				" SYN ->  PRED -> 3 // ist\n" +
				" REF ->    '' -> 0\n" +
				"        gender / bot\n" +
				"        person / third\n" +
				"        number / bot\n" +
				"           cat / NE\n" +
				"          case / bot\n" +
				"       pattern / NE\n" +
				",\n" +
				"  4 5 Satz\n" +
				" SYN ->   APP -> 2 // Satz\n" +
				" REF ->    '' -> 0\n" +
				"          base / Satz\n" +
				"        gender / masc\n" +
				"        person / third\n" +
				"           cat / NN\n" +
				"        number / sg\n" +
				"          case / 'nom_dat_acc'\n" +
				",\n" +
				"  5 6 '1'\n" +
				" SYN ->   APP -> 5 // Satz\n" +
				" REF ->    '' -> 0\n" +
				"        gender / bot\n" +
				"        person / third\n" +
				"        number / sg\n" +
				"           cat / CARD\n" +
				"          case / bot\n" +
				"       pattern / arabisch\n" +
				",\n" +
				"  6 7 '.'\n" +
				" SYN ->    '' -> 0\n" +
				" REF ->    '' -> 0\n" +
				"           cat / '$.'\n" +
				";\n";

	public static final String PARSE_ANNOV_ZH2_1 = "'parse4' : 'parse4' <->\n" +
				"/*\n" +
				"Search Strategy : null\n" +
				"Processing Time : 0\n" +
				"score : 0,6865\n" +
				"*/\n" +
				"  0 1 Dieser\n" +
				" SYN ->   DET -> 2 // Satz\n" +
				" REF ->    '' -> 0\n" +
				"          base / dies\n" +
				"        gender / masc\n" +
				"        number / sg\n" +
				"           cat / PDAT\n" +
				"          case / nom\n" +
				",\n" +
				"  1 2 Satz\n" +
				" SYN ->  SUBJ -> 3 // ist\n" +
				" REF ->    '' -> 0\n" +
				"          base / Satz\n" +
				"        gender / masc\n" +
				"        person / third\n" +
				"           cat / NN\n" +
				"        number / sg\n" +
				"          case / 'nom_dat_acc'\n" +
				",\n" +
				"  2 3 ist\n" +
				" SYN ->     S -> 0\n" +
				" REF ->    '' -> 0\n" +
				"           cat / VAFIN\n" +
				"        person / third\n" +
				"         tense / present\n" +
				"       valence / e\n" +
				"          mood / indicative\n" +
				"          base / sein\n" +
				"        stress / none\n" +
				"        number / sg\n" +
				",\n" +
				"  3 4 'zh2'\n" +
				" SYN ->  PRED -> 3 // ist\n" +
				" REF ->    '' -> 0\n" +
				"        gender / bot\n" +
				"        person / third\n" +
				"        number / bot\n" +
				"           cat / NE\n" +
				"          case / bot\n" +
				"       pattern / NE\n" +
				",\n" +
				"  4 5 Satz\n" +
				" SYN ->   APP -> 2 // Satz\n" +
				" REF ->    '' -> 0\n" +
				"          base / Satz\n" +
				"        gender / masc\n" +
				"        person / third\n" +
				"           cat / NN\n" +
				"        number / sg\n" +
				"          case / 'nom_dat_acc'\n" +
				",\n" +
				"  5 6 '1'\n" +
				" SYN ->   APP -> 5 // Satz\n" +
				" REF ->    '' -> 0\n" +
				"        gender / bot\n" +
				"        person / third\n" +
				"        number / sg\n" +
				"           cat / CARD\n" +
				"          case / bot\n" +
				"       pattern / arabisch\n" +
				",\n" +
				"  6 7 '.'\n" +
				" SYN ->    '' -> 0\n" +
				" REF ->    '' -> 0\n" +
				"           cat / '$.'\n" +
				";\n";
	
	public static final String PARSE_ANNOV_ZH1_2 = "'parse8' : 'parse8' <->\n" +
				"/*\n" +
				"Search Strategy : null\n" +
				"Processing Time : 0\n" +
				"score : 0,6865\n" +
				"*/\n" +
				"  0 1 Dieser\n" +
				" SYN ->   DET -> 2 // Satz\n" +
				" REF ->    '' -> 0\n" +
				"          base / dies\n" +
				"        gender / masc\n" +
				"        number / sg\n" +
				"           cat / PDAT\n" +
				"          case / nom\n" +
				",\n" +
				"  1 2 Satz\n" +
				" SYN ->  SUBJ -> 3 // ist\n" +
				" REF ->    '' -> 0\n" +
				"          base / Satz\n" +
				"        gender / masc\n" +
				"        person / third\n" +
				"           cat / NN\n" +
				"        number / sg\n" +
				"          case / 'nom_dat_acc'\n" +
				",\n" +
				"  2 3 ist\n" +
				" SYN ->     S -> 0\n" +
				" REF ->    '' -> 0\n" +
				"           cat / VAFIN\n" +
				"        person / third\n" +
				"         tense / present\n" +
				"       valence / e\n" +
				"          mood / indicative\n" +
				"          base / sein\n" +
				"        stress / none\n" +
				"        number / sg\n" +
				",\n" +
				"  3 4 'zh1'\n" +
				" SYN ->  PRED -> 3 // ist\n" +
				" REF ->    '' -> 0\n" +
				"        gender / bot\n" +
				"        person / third\n" +
				"        number / bot\n" +
				"           cat / NE\n" +
				"          case / bot\n" +
				"       pattern / NE\n" +
				",\n" +
				"  4 5 Satz\n" +
				" SYN ->   APP -> 2 // Satz\n" +
				" REF ->    '' -> 0\n" +
				"          base / Satz\n" +
				"        gender / masc\n" +
				"        person / third\n" +
				"           cat / NN\n" +
				"        number / sg\n" +
				"          case / 'nom_dat_acc'\n" +
				",\n" +
				"  5 6 '2'\n" +
				" SYN ->   APP -> 5 // Satz\n" +
				" REF ->    '' -> 0\n" +
				"        gender / bot\n" +
				"        person / third\n" +
				"        number / pl\n" +
				"           cat / CARD\n" +
				"          case / bot\n" +
				"       pattern / arabisch\n" +
				",\n" +
				"  6 7 '.'\n" +
				" SYN ->    '' -> 0\n" +
				" REF ->    '' -> 0\n" +
				"           cat / '$.'\n" +
				";\n";
				
	public static final String PARSE_HEISE58 = "\r\n" + 
			"'heiseticker_s58' : 'heiseticker_s58' <->\r\n" + 
			"  0 1 die\r\n" + 
			"case / nom\r\n" + 
			"cat / ART\r\n" + 
			"gender / fem\r\n" + 
			"number / sg\r\n" + 
			"SYN -> DET -> 3 /* ('Tochter') */\r\n" + 
			"REF -> '' -> 0\r\n" + 
			",\r\n" + 
			"  1 2 deutsche\r\n" + 
			"base / deutsch\r\n" + 
			"case / nom_acc\r\n" + 
			"cat / ADJA\r\n" + 
			"degree / positive\r\n" + 
			"gender / fem\r\n" + 
			"number / sg\r\n" + 
			"SYN -> ATTR -> 3 /* ('Tochter') */\r\n" + 
			"REF -> '' -> 0\r\n" + 
			"flexion / weak\r\n" + 
			",\r\n" + 
			"  2 3 Tochter\r\n" + 
			"base / Tochter\r\n" + 
			"cat / NN\r\n" + 
			"gender / fem\r\n" + 
			"number / sg\r\n" + 
			"person / third\r\n" + 
			"SYN -> SUBJ -> 4 /* ('hat') */\r\n" + 
			"REF -> '' -> 0\r\n" + 
			"case / nom\r\n" + 
			",\r\n" + 
			"  3 4 hat\r\n" + 
			"cat / VAFIN\r\n" + 
			"base / haben\r\n" + 
			"mood / indicative\r\n" + 
			"number / sg\r\n" + 
			"person / third\r\n" + 
			"tense / present\r\n" + 
			"SYN -> S -> 0\r\n" + 
			"REF -> '' -> 0\r\n" + 
			",\r\n" + 
			"  4 5 ihre\r\n" + 
			"case / acc\r\n" + 
			"cat / PPOSAT\r\n" + 
			"number / pl\r\n" + 
			"person / third\r\n" + 
			"SYN -> DET -> 6 /* ('Datenschutzrichtlinien') */\r\n" + 
			"REF -> '' -> 0\r\n" + 
			",\r\n" + 
			"  5 6 Datenschutzrichtlinien\r\n" + 
			"base / Linie\r\n" + 
			"cat / NN\r\n" + 
			"gender / fem\r\n" + 
			"number / pl\r\n" + 
			"person / third\r\n" + 
			"SYN -> OBJA -> 11 /* ('geändert') */\r\n" + 
			"REF -> '' -> 0\r\n" + 
			"case / acc\r\n" + 
			",\r\n" + 
			"  6 7 nicht\r\n" + 
			"cat / PTKNEG\r\n" + 
			"SYN -> ADV -> 11 /* ('geändert') */\r\n" + 
			"REF -> '' -> 0\r\n" + 
			",\r\n" + 
			"  7 8 wie\r\n" + 
			"cat / KOKOM\r\n" + 
			"SYN -> KOM -> 11 /* ('geändert') */\r\n" + 
			"REF -> '' -> 0\r\n" + 
			",\r\n" + 
			"  8 9 die\r\n" + 
			"case / acc\r\n" + 
			"cat / ART\r\n" + 
			"gender / fem\r\n" + 
			"number / sg\r\n" + 
			"SYN -> DET -> 10 /* ('US-Gesellschaft') */\r\n" + 
			"REF -> '' -> 0\r\n" + 
			",\r\n" + 
			"  9 10 'US-Gesellschaft'\r\n" + 
			"base / Gesellschaft\r\n" + 
			"cat / NN\r\n" + 
			"gender / fem\r\n" + 
			"number / sg\r\n" + 
			"person / third\r\n" + 
			"SYN -> CJ -> 8 /* ('wie') */\r\n" + 
			"REF -> '' -> 0\r\n" + 
			",\r\n" + 
			"  10 11 geändert\r\n" + 
			"cat / VVPP\r\n" + 
			"base / ändern\r\n" + 
			"SYN -> AUX -> 4 /* ('hat') */\r\n" + 
			"REF -> '' -> 0\r\n" + 
			",\r\n" + 
			"  11 12 ','\r\n" + 
			"cat / '$,'\r\n" + 
			"SYN -> '' -> 0\r\n" + 
			"REF -> '' -> 0\r\n" + 
			",\r\n" + 
			"  12 13 was\r\n" + 
			"base / was\r\n" + 
			"case / nom\r\n" + 
			"cat / PRELS\r\n" + 
			"gender / neut\r\n" + 
			"number / sg\r\n" + 
			"person / third\r\n" + 
			"SYN -> SUBJ -> 22 /* ('wäre') */\r\n" + 
			"REF -> '' -> 4 /* ('hat') */\r\n" + 
			",\r\n" + 
			"  13 14 nach\r\n" + 
			"case / dat\r\n" + 
			"cat / APPR\r\n" + 
			"SYN -> PP -> 22 /* ('wäre') */\r\n" + 
			"REF -> '' -> 0\r\n" + 
			",\r\n" + 
			"  14 15 deutschem\r\n" + 
			"base / deutsch\r\n" + 
			"case / dat\r\n" + 
			"cat / ADJA\r\n" + 
			"degree / positive\r\n" + 
			"flexion / strong\r\n" + 
			"gender / not_fem\r\n" + 
			"number / sg\r\n" + 
			"SYN -> ATTR -> 16 /* ('Recht') */\r\n" + 
			"REF -> '' -> 0\r\n" + 
			",\r\n" + 
			"  15 16 Recht\r\n" + 
			"base / Recht\r\n" + 
			"case / dat\r\n" + 
			"cat / NN\r\n" + 
			"gender / neut\r\n" + 
			"number / sg\r\n" + 
			"person / third\r\n" + 
			"SYN -> PN -> 14 /* ('nach') */\r\n" + 
			"REF -> '' -> 0\r\n" + 
			",\r\n" + 
			"  16 17 auch\r\n" + 
			"cat / ADV\r\n" + 
			"cat2 / KON\r\n" + 
			"subcat / focus\r\n" + 
			"SYN -> ADV -> 21 /* ('möglich') */\r\n" + 
			"REF -> '' -> 0\r\n" + 
			",\r\n" + 
			"  17 18 nicht\r\n" + 
			"cat / PTKNEG\r\n" + 
			"SYN -> ADV -> 21 /* ('möglich') */\r\n" + 
			"REF -> '' -> 0\r\n" + 
			",\r\n" + 
			"  18 19 ohne\r\n" + 
			"case / acc\r\n" + 
			"cat / APPR\r\n" + 
			"SYN -> PP -> 21 /* ('möglich') */\r\n" + 
			"REF -> '' -> 0\r\n" + 
			",\r\n" + 
			"  19 20 Weiteres\r\n" + 
			"base / weit\r\n" + 
			"case / acc\r\n" + 
			"cat / ADJA\r\n" + 
			"degree / comparative\r\n" + 
			"flexion / strong_mixed\r\n" + 
			"gender / neut\r\n" + 
			"number / sg\r\n" + 
			"SYN -> PN -> 19 /* ('ohne') */\r\n" + 
			"REF -> '' -> 0\r\n" + 
			",\r\n" + 
			"  20 21 möglich\r\n" + 
			"cat / ADJD\r\n" + 
			"degree / positive\r\n" + 
			"SYN -> PRED -> 22 /* ('wäre') */\r\n" + 
			"REF -> '' -> 0\r\n" + 
			",\r\n" + 
			"  21 22 wäre\r\n" + 
			"cat / VAFIN\r\n" + 
			"base / sein\r\n" + 
			"mood / subjunctive2\r\n" + 
			"number / sg\r\n" + 
			"person / third\r\n" + 
			"tense / present\r\n" + 
			"SYN -> REL -> 4 /* ('hat') */\r\n" + 
			"REF -> '' -> 0\r\n" + 
			",\r\n" + 
			"  22 23 '.'\r\n" + 
			"cat / '$.'\r\n" + 
			"SYN -> '' -> 0\r\n" + 
			"REF -> '' -> 0\r\n" + 
			";\r\n" + 
			"\r\n";
	
	public static void main(String[] args) {
		try {
			Parse p1 = generateParseObject(PARSE_0);
			Parse p2 = generateParseObject(PARSE_1);
			Parse p3 = generateParseObject(PARSE_2);
			Parse p4 = generateParseObject(PARSE_CIRCLE);
			
			Configuration config = Configuration.fromFile("startup.properties"); 
			DecoratedParse dp1 = generateDecoratedParseObject(PARSE_0, config);
		} catch (RecognitionException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
