package de.unihamburg.informatik.nats.jwcdg.gui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.antlr.runtime.RecognitionException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Node;

import de.unihamburg.informatik.nats.jwcdg.parse.Parse;
import de.unihamburg.informatik.nats.jwcdg.parse.Word;

// public class TestDepTreeGenerator {
// 	protected static String _fileName = "startup.properties";
// 	protected static String _s0 = TestParseFactory.PARSE_0;
// 	protected static String _s1 = TestParseFactory.PARSE_1;
// 	protected static String _s2 = TestParseFactory.PARSE_2;
// 	protected static String _sc = TestParseFactory.PARSE_CIRCLE;
// 	protected static String _sh = TestParseFactory.PARSE_HEISE58;
// 	private DepTreeGenerator _dtGen;
// 	private DepTree _dt;
// 	private DepTree _dtNoMarked;
// 	private Parse _p;
// 	private Map<Integer, ArrayList<String>> _markedNodes;

// 	@BeforeClass
// 	public static void setUpBeforeClass() throws Exception {
// 	}

// 	@AfterClass
// 	public static void tearDownAfterClass() throws Exception {
// 	}

// 	/**
// 	 * generate a Parse from a sentence, then use makeTree to generate a DepTree
// 	 * that contains a resulting graph in the form of a NodesCanvas
// 	 * 
// 	 * @throws Exception
// 	 */
// 	@Before
// 	public void setUp() throws Exception {
// 		try {
// 			final Parse p = TestParseFactory.generateParseObject(_sh);
// 			_p = p;
// 			final Map<Integer, ArrayList<String>> markedNodes;
// 			markedNodes = new HashMap<Integer, ArrayList<String>>();
// 			_markedNodes = markedNodes;
// 			ArrayList<String> markedLevels = new ArrayList<String>();
// 			ArrayList<String> marked2 = new ArrayList<String>();
// 			markedLevels.add("SYN");
// 			marked2.add("SYN");
// 			marked2.add("REF");
// 			markedNodes.put(1, markedLevels);
// 			markedNodes.put(3, markedLevels);
// 			markedNodes.put(6, marked2);
// 			_dtGen = new DepTreeGenerator();
// 			_dt = _dtGen.makeTree(p, markedNodes);
// 			_dtNoMarked = _dtGen.makeTree(p);
// 		}
// 		catch (RecognitionException e) {
// 			e.printStackTrace();
// 		}
// 		finally {
// 		}
// 	}

// 	@After
// 	public void tearDown() throws Exception {
// 	}

// 	@Test
// 	public void testDepTreeGenerator() {
// 		check(_dt);
// 		assertEquals(_dt.getMarkedNodes(), _markedNodes);
// 		check(_dtNoMarked);
// 		DTValues v1 = new DTValues(_dt);
// 		DTValues vn = new DTValues(_dtNoMarked);
// 		assertNotSame("Generating DepTrees with marked nodes should have different results than without.",
// 					v1,
// 					vn);
// 		_dtGen.draw(_dt, _p);
// 		DTValues v2 = new DTValues(_dt);
// 		assertEquals("Draw with the same DepTree shouldn't change any values. \n",
// 					v1,
// 					v2);
// 		_dtGen.redraw(_dt);
// 		DTValues v3 = new DTValues(_dt);
// 		assertEquals("Redraw with the same DepTree shouldn't change any values. \n",
// 					v2,
// 					v3);
// 	}

// 	private void check(DepTree dt) {
// 		assertNotNull(dt);
// 		int wordsCount = _p.getWords().size();
// 		assertEquals("The number of nodes in the DepTree should be the number of words in the sentence. \n",
// 					dt.size(),
// 					wordsCount);
// 		assertNotNull(dt.getNodesCanvas());
// 		assertNotNull(dt.getSVGGen());
// 		// marked nodes in the DepTree equal the ones that were called
// 		for (DepTreeNode node1 : dt) {
// 			for (DepTreeNode node2 : dt) {
// 				if (node1.getIndex() < node2.getIndex()) {
// 					assertTrue("all words of the nodes should be written from left to right:  \n"
// 								+ nodeText(node1, " at " + node1.getX())
// 								+ nodeText(node2, " at " + node2.getX()),
// 								node1.getX() < node2.getX());
// 				}
// 				// (on the SYN level), except the ones in a circle
// 				// (lower means higher y)
// 				if (node1.getLink(dt.getCurrentLevel()) == node2.getIndex()) {
// 					assertTrue("all nodes should be below the ones they are linking to, except when in a circle (higher y means lower position):  \n"
// 								+ nodeText(node1, " at " + node1.getY()
// 											+ " circle:" + node1.isInCircle())
// 								+ nodeText(node2, " at " + node2.getY()),
// 								node1.getY() > node2.getY()
// 											|| node1.isInCircle());
// 				}
// 			}
// 		}
// 	}

// 	private String nodeText(DepTreeNode node, String restText) {
// 		return " " + node.getIndex() + " " + node.getWord() + restText + " \n";
// 	}

// 	@Test
// 	public void testWrite() throws Exception {
// 		DepTreeGenerator dtGen = new DepTreeGenerator();
// 		List<String> words = new ArrayList<String>();
// 		for (Word word : _p.getWords()) {
// 			words.add(word.word);
// 		}
// 		String filename = "demo.svg";
// 		Writer w =
// 					new OutputStreamWriter(new FileOutputStream(filename),
// 								"UTF-8");
// 		PrintWriter writer = new PrintWriter(w);
// 		dtGen.writeTree(_p.getLevels(),
// 					_p.getVerticesLabels(),
// 					_p.getVerticesStructure(),
// 					words,
// 					_markedNodes,
// 					writer);

// 		File file = new File(filename);
// 		// String parserName = XMLResourceDescriptor.getXMLParserClassName();
// 		// SAXSVGDocumentFactory f = new SAXSVGDocumentFactory(parserName);
// 		// String uri = file.toURI().toString();
// 		// Document doc = f.createDocument(uri);

// 		assertTrue("No svg file generated.", file.exists());

// 		file.delete();

// 		w = new OutputStreamWriter(new FileOutputStream(filename), "UTF-8");
// 		writer = new PrintWriter(w);
// 		dtGen.writeTree(_p.getLevels(),
// 					_p.getVerticesLabels(),
// 					_p.getVerticesStructure(),
// 					words,
// 					writer);

// 		assertTrue("No svg file generated.", file.exists());

// 		file.delete();
// 	}

// 	@Test
// 	public void testHighlightNodes() {
// 		// System.out.println(_dt.getDoc().getTextContent());
// 		// traverse (0, new ArrayList<Integer>(), _dt.getDoc());
// 	}

// 	public void traverse(int level, ArrayList<Integer> posArray, Node node) {
// 		int pos = 0;
// 		do {
// 			pos++;
// 			System.out.print(level + " " + posArray + " " + pos + "   "
// 						+ node.getNodeName() + "   ");
// 			int i = 0;
// 			if (node.getAttributes() != null) {
// 				while (i < node.getAttributes().getLength()) {
// 					System.out.print(node.getAttributes().item(i).getNodeName()
// 								+ "="
// 								+ node.getAttributes().item(i).getNodeValue()
// 								+ " ");
// 					i++;
// 				}
// 			}
// 			System.out.println();
// 			Node child = node.getFirstChild();
// 			if (child != null) {
// 				ArrayList<Integer> childArray = new ArrayList<Integer>();
// 				childArray.addAll(posArray);
// 				childArray.add(pos);
// 				traverse(level + 1, childArray, child);
// 			}
// 			node = node.getNextSibling();
// 		}
// 		while (node != null);
// 	}

// 	//
// 	// @Test
// 	// public void testHighlight() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testChangeColor() {
// 	// fail("Not yet implemented");
// 	// }
// 	//

// }
