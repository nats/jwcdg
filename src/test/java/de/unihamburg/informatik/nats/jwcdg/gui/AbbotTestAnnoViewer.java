package de.unihamburg.informatik.nats.jwcdg.gui;

import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JSlider;

import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;
import de.unihamburg.informatik.nats.jwcdg.parse.Word;
import io.gitlab.nats.deptreeviz.DepTree;
import io.gitlab.nats.deptreeviz.DepTreeNode;
import junit.extensions.abbot.TestHelper;

import org.apache.batik.swing.JSVGCanvas;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.svg.SVGDocument;

import io.gitlab.nats.deptreeviz.DepTree.Elem;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;

/**
 * Tests AnnoViewer using Abbot. It's more of a test of Abbot right now. Its
 * best started as java application with the main method.
 * It requires some directories containing .CDA files. Their complete paths are
 * given as arguments.
 *
 * @author 3zimmer
 *
 */
public class AbbotTestAnnoViewer extends AbbotTestAbstractViewer {
	static String[] _args;
	private File _workingDir;
	private ArrayList<String> _dirNames = new ArrayList<String>();
	private ArrayList<String> _cdaNames = new ArrayList<String>();
	protected boolean _refLinkIsCreated = false;
	protected AnnoViewer _av;
	private List<DepTree<DecoratedParse, Word>> _dts;
	protected ArrayList<ArrayList<String>> _cdaNamesList = new ArrayList<ArrayList<String>>();
	protected ArrayList<ArrayList<String>> _expectedWordsList = new ArrayList<ArrayList<String>>();
	protected ArrayList<ArrayList<String>> _expectedWordsInFilesList = new ArrayList<ArrayList<String>>();
	protected ArrayList<ArrayList<String>> _expectedSYNsInFilesList = new ArrayList<ArrayList<String>>();
	protected ArrayList<ArrayList<String>> _expectedSYNsList = new ArrayList<ArrayList<String>>();
	// the expected status of the done buttons (true means done button can not be pressed, but not done button)
	protected ArrayList<ArrayList<Boolean>> _expectedDoneStatusList = new ArrayList<ArrayList<Boolean>>();
	// the expected status of the save buttons on the right side (true means save button can be pressed)
	protected ArrayList<ArrayList<Boolean>> _expectedSaveStatusList = new ArrayList<ArrayList<Boolean>>();
	// which sentence in the NavTree is currently selected?
	private int _expectedNavDirIndex = 0;
	private HashSet<String> _differentCDANames = new HashSet<String>();

	public AbbotTestAnnoViewer(String name) {
		super(name);
	}

	public static void main(String[] args) {
		_args = args;
		TestHelper.runTests(args, AbbotTestAnnoViewer.class);
	}

	@Override
	@Before
	public void setUp() throws Exception {
		makeTempDir("temp");
		_workingDir = FileService.getWorkingDir();
		makeDir("ctok");
		makeDir("zh1");
		makeDir("zh2");
		makeFile(_dirNames.get(0), "1", TestParseFactory.PARSE_ANNOV_CTOK_1);
		//		getExpectedWords().add("Dieser Satz ist ctok Satz 1 .");
		makeFile(_dirNames.get(1), "1", TestParseFactory.PARSE_ANNOV_ZH1_1);
		//		getExpectedWords().add("Dieser Satz ist zh1 Satz 1 .");
		makeFile(_dirNames.get(2), "1", TestParseFactory.PARSE_ANNOV_ZH2_1);
		//		getExpectedWords().add("Dieser Satz ist zh2 Satz 1 .");
		makeFile(_dirNames.get(1), "2", TestParseFactory.PARSE_ANNOV_ZH1_2);
		FileService.saveToFile("1.cda",
				_workingDir.getAbsolutePath()
				+ File.separator
				+ _dirNames.get(1)
				, "done.txt");
		initStatuses();
		getExpectedWords().set(0, "Dieser Satz ist ctok Satz 1 .");
		getExpectedWords().set(1, "Dieser Satz ist zh1 Satz 1 .");
		getExpectedWords().set(2, "Dieser Satz ist zh2 Satz 1 .");
		getCDANames().set(0, _cdaNames.get(0));
		getCDANames().set(1, _cdaNames.get(1));
		getCDANames().set(2, _cdaNames.get(2));
		getExpectedDoneStatus().set(1, true);
		getExpectedSYNsInFiles().set(0, "DET SUBJ S ADV PRED APP");
		getExpectedSYNs().set(0, "DET SUBJ S ADV PRED APP");
		getExpectedSYNsInFiles().set(1, "DET SUBJ S PRED APP APP");
		getExpectedSYNs().set(1, "DET SUBJ S PRED APP APP");
		getExpectedSYNsInFiles().set(2, "DET SUBJ S PRED APP APP");
		getExpectedSYNs().set(2, "DET SUBJ S PRED APP APP");
		_expectedNavDirIndex = 1;
		getExpectedWords().set(1, "Dieser Satz ist zh1 Satz 2 .");
		getCDANames().set(1, _cdaNames.get(3));
		getExpectedSYNsInFiles().set(1, "DET SUBJ S PRED APP APP");
		getExpectedSYNs().set(1, "DET SUBJ S PRED APP APP");
		_expectedNavDirIndex = 0;
	}

	@Override
	@After
	public void tearDown() throws Exception {
		FileService.setWorkingDir(_workingDir);
		for (String cdaName : _cdaNames){
			deleteFile("", cdaName);
		}
		for (String dirName : _dirNames){
			deleteFile(dirName, "done.txt");
			deleteFile("", dirName);
		}
		deleteFile("", "temp");
	}

	@Test
	public void testAnnoViewer() {
		AnnoViewer av;
		try {
			av = new AnnoViewer(Configuration.fromFile("startup.properties"), _dirNames);
			AVMainPage mainWindow = new AVMainPage(av);
			AutoTestAnnoViewer.test(mainWindow);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	private void initStatuses() {
		ArrayList<Boolean> falseList = new ArrayList<Boolean>();
		ArrayList<String> nullList = new ArrayList<String>();
		for (String dirname : _dirNames){
			falseList.add(false);
			nullList.add(null);
		}
		for (String filename :  _differentCDANames){
			_expectedDoneStatusList.add(new ArrayList<Boolean> (falseList));
			_expectedSaveStatusList.add(new ArrayList<Boolean> (falseList));
			_cdaNamesList.add(new ArrayList<String> (nullList));
			_expectedWordsList.add(new ArrayList<String> (nullList));
			_expectedWordsInFilesList.add(new ArrayList<String> (nullList));
			_expectedSYNsList.add(new ArrayList<String> (nullList));
			_expectedSYNsInFilesList.add(new ArrayList<String> (nullList));
		}
	}

	private void makeDir(String dirname) {
		//		dirname ="temp" + File.separator + dirname;
		dirname = "temp" + File.separator + dirname;
		makeTempDir(dirname);
		_dirNames.add(dirname);
	}

	private void makeFile(String dirname, String filename, String content){
		filename = filename + ".cda";
		//		System.out.println("Generated: " +  dirname + File.separator + filename);
		FileService.saveToFile(content,
				_workingDir.getAbsolutePath()
				+ File.separator
				+ dirname
				, filename);
		_cdaNames.add(dirname + File.separator + filename);
		_differentCDANames.add(filename) ;
	}

	@Override
	protected ArrayList<DepTreeNode> getVisibleNodes(int canvasIndex) {
		return doGetVisibleNodes(_dts.get(canvasIndex), canvasIndex);
	}

	/**
	 * Checks if only the buttons can be pressed that are expected to.
	 */
	protected void checkAll() {
		_ct.actionWaitForIdle();
		for (String dir : _dirNames){
			int i = _dirNames.indexOf(dir);
			checkButton(AbstractDisplay.DONE_BUTTON + i, ! getExpectedDoneStatus().get(i));
			checkButton(AbstractDisplay.NOT_DONE_BUTTON + i, getExpectedDoneStatus().get(i));
			checkButton(AbstractDisplay.SAVE_BUTTON + i, getExpectedSaveStatus().get(i));
			checkWordsInFile(FileService.getWorkingDir().getAbsolutePath(), _cdaNamesList.get(_expectedNavDirIndex).get(i), getExpectedWordsInFiles().get(i));
			checkSYNs();
		}
	}

	public void checkSYNs() {
		String expectedSYNs = getExpectedSYNs().get(0);
		if (expectedSYNs != null && _dts.get(0).getCurrentLevel().equals("SYN")){
			checkWordsInTree(_dts.get(0), expectedSYNs);
		}
		String expectedSYNsInFile = getExpectedSYNsInFiles().get(0);
		if (expectedSYNsInFile != null){
			checkWordsInFile(FileService.getWorkingDir().getAbsolutePath(), _cdaNamesList.get(_expectedNavDirIndex).get(0), expectedSYNsInFile);
		}
	}

	protected void checkButton(String name, Boolean shouldBeEnabled) {
		JButton button = (JButton) find(_gui, null, name);
		String not = "";
		if (! shouldBeEnabled){
			not = "not";
		}
		assertTrue ("Button " + name + " should " + not + " be enabled.", button.isEnabled() == shouldBeEnabled);
	}

	public void selectInNavTree(Point point, int navIndex) {
		selectInNavTree(point);
		setExpectedNavDirIndex(navIndex);
		checkAll();
	}

	// TODO implement Page classes and methods
	public class AVMainPage extends AbstractMainPage{

		public AVMainPage(AnnoViewer av) {
			super(av);
			_av = av;
			_gui = _av.getGUI();
			_dts = _av._dts;
			SVGDocument doc;
			_ct.actionWaitForIdle();
			do {
				doc = _dts.get(0).getDoc();
				_ct.actionDelay(1);
			}
			while (doc == null);
			_ct.actionWaitForIdle();
			do {
				doc = _dts.get(0).getDoc();
				_ct.actionDelay(1);
			}
			while (! doc.getTextContent().contains("Satz"));
			_ct.actionDelay(1000);
			// TODO check if all files have been read.
		}

		public AVMainPage() {
			super();
			checkAll();
		}

		public AVMainPage testChangeDTLinks() {
			return new AVMainPage()
			.generateRandomDTREFLink()
			.changeRandomDTLink(0, "click","dot","SYN")
			////					unnecessary because changeRandomSYNLabel
			////					.changeRandomDTLink("click","word","SYN")
			//					.changeRandomDTLink("click","dot","REF")
			//					.changeRandomDTLink("click","word","REF")
			;
		}

		protected AVMainPage changeRandomDTLink(int treeIndex, String action,
				String target,
				String firstLevel) {
			doChangeRandomDTLink(_dts.get(treeIndex), treeIndex, action, target, firstLevel);
			getExpectedSaveStatus().set(treeIndex, true);
			return new AVMainPage();
		}

		protected AVMainPage generateRandomDTREFLink() {
			doGenerateRandomDTREFLink(1, 1);
			getExpectedSaveStatus().set(1, true);
			return new AVMainPage();
		}

		public AVFileMenuPage selectFileMenu() {
			doSelectFileMenu();
			return new AVFileMenuPage();
		}

		/**
		 * Right now this combines several tests. These should be separated.
		 *
		 * @return
		 */
		public AVMainPage selectRandomInNavTree() {
			Point point1 = getNavTreePosition("2 : ", 0);
			Point point2 = getCurrentNavTreeSelection();
			selectInNavTree(point1, 1);
			if (! _refLinkIsCreated){
				changeSYNLabelToNothing(1, 0);
				selectFileMenu().
				selectSaveAllDisplayed();
				generateRandomDTREFLink();
				pressSaveButton(1);
				pressDoneButton(1);
				_refLinkIsCreated = true;
			}
			selectInNavTree(point2, 0);
			return new AVMainPage();
		}

		public AVMainPage testButtons() {
			doGenerateRandomDTREFLink(2, 0);
			getExpectedSaveStatus().set(0, true);
			return new AVMainPage()
			.pressSYNButton(true)
			.pressSYNButton(true)
			.pressSaveButton(0)
			//					.failToPressDoneButton(1)
			.pressNotDoneButton(1)
			.pressSaveButton(1)
			.pressDoneButton(0)
			.selectRandomInNavTree()
			.pressNotDoneButton(0)
			.selectRandomInNavTree()
			;
		}

		protected AVMainPage failToPressDoneButton(int index) {
			// JOptionPane
			//			Component warning = find(_gui, JOptionPane.class, null);
			Component button = findButton(_gui, "OK");
			_ct.actionClick(button);
			return new AVMainPage();
		}

		protected AVMainPage pressSYNButton(boolean hasEffect) {
			doPressSYNButton(_dts.get(0), hasEffect);
			return new AVMainPage();
		}

		protected AVMainPage pressDoneButton(int index) {
			Component button = find(_gui, AbstractDisplay.DONE_BUTTON + index);
			_ct.actionClick(button);
			getExpectedDoneStatus().set(index, true);
			return new AVMainPage();
		}

		private AVMainPage pressNotDoneButton(int index) {
			Component button = find(_gui, AbstractDisplay.NOT_DONE_BUTTON + index);
			_ct.actionClick(button);
			getExpectedDoneStatus().set(index, false);
			return new AVMainPage();
		}

		private AVMainPage pressSaveButton(int index) {
			Component button = find(_gui, AbstractDisplay.SAVE_BUTTON + index);
			_ct.actionClick(button);
			getExpectedSaveStatus().set(index, false);
			setExpectedWordsInFiles(new ArrayList<String>(getExpectedWords()));
			setExpectedSYNsInFiles(new ArrayList<String>(getExpectedSYNs()));
			return new AVMainPage();
		}

		protected void pressTreeButton(int index) {
			Component button = find(_gui, AbstractDisplay.TREE_BUTTON + index);
			_ct.actionClick(button);
		}

		public AVMainPage changeRandomSYNLabelToNothing() {
			changeSYNLabelToNothing(0, 0);
			changeSYNLabelToNothing(2, 0);
			return new AVMainPage();
		}

		public void changeSYNLabelToNothing(int dirIndex, int nodeIndex) {
			Component canvas = find(_gui, JSVGCanvas.class, "" + dirIndex);
			Point point = getPoint(_dts.get(dirIndex), _dts.get(dirIndex).getNode(nodeIndex), Elem.TEXT, "SYN");
			_ct.actionClick(canvas, (int) point.getX(), (int) point.getY());
			//			System.out.println("clicking: " + point);
			delay();
			Component item = find(_gui, null, "" + AbstractDisplay.MENU_ITEM);
			_ct.actionClick(item);
			//			System.out.println("before: " + getExpectedSYNs().get(nodeIndex) + " - " + getExpectedSYNsInFiles().get(nodeIndex));
			getExpectedSYNs().set(dirIndex, getExpectedSYNs().get(dirIndex).replaceFirst("DET", ""));
			//			System.out.println("after: " + getExpectedSYNs().get(nodeIndex) + " - " + getExpectedSYNsInFiles().get(nodeIndex));
			getExpectedSaveStatus().set(dirIndex, true);
		}

		public AVMainPage testZoom() {
			Component navSlider = find(_gui, JSlider.class, "slider 2");
			_st.actionDecrement(navSlider);
			delay();
			_st.actionIncrement(navSlider);
			return new AVMainPage();
		}

		public AVMainPage testConstraints() {
			selectRandomConstraintsItem();
			deselectRandomConstraintsItem();
			return new AVMainPage();
		}

		protected void selectRandomConstraintsItem() {
			pressTreeButton(1);
			doSelectRandomConstraintsItem();
		}

		protected void deselectRandomConstraintsItem() {
			doDeselectRandomConstraintsItem(_dts.get(0));
			pressTreeButton(0);
		}
	}

	public class AVFileMenuPage extends AbstractFileMenuPage{

		public AVMainPage selectSaveAllDisplayed() {
			selectInFile("saveall item");
			// reset _expectedSaveStatus
			getExpectedSaveStatus().clear();
			for (@SuppressWarnings("unused") String i : _dirNames){
				getExpectedSaveStatus().add(false);
			}
			setExpectedWordsInFiles(new ArrayList<String>(getExpectedWords()));
			setExpectedSYNsInFiles(new ArrayList<String>(getExpectedSYNs()));
			return new AVMainPage();
		}

	}
	protected ArrayList<String> getExpectedWords() {
		return _expectedWordsList.get(_expectedNavDirIndex);
	}

	protected void setCDANames(ArrayList<String> expectedWords) {
		_cdaNamesList.set(getExpectedNavDirIndex(), expectedWords);
	}

	protected ArrayList<String> getCDANames() {
		return _cdaNamesList.get(_expectedNavDirIndex);
	}


	protected void setExpectedWords(ArrayList<String> expectedWords) {
		_expectedWordsList.set(getExpectedNavDirIndex(), expectedWords);
	}

	protected ArrayList<String> getExpectedWordsInFiles() {
		return _expectedWordsList.get(_expectedNavDirIndex);
	}

	protected void setExpectedWordsInFiles(ArrayList<String> expectedWordsInFiles) {
		_expectedWordsInFilesList.set(getExpectedNavDirIndex(), expectedWordsInFiles);
	}

	protected ArrayList<String> getExpectedSYNsInFiles() {
		return _expectedSYNsInFilesList.get(_expectedNavDirIndex);
	}

	protected void setExpectedSYNsInFiles(ArrayList<String> expectedSYNsInFiles) {
		_expectedSYNsInFilesList.set(getExpectedNavDirIndex(), expectedSYNsInFiles);
	}

	protected ArrayList<String> getExpectedSYNs() {
		return _expectedSYNsList.get(_expectedNavDirIndex);
	}

	protected void setExpectedSYNs(ArrayList<String> expectedSYNs) {
		_expectedSYNsList.set(getExpectedNavDirIndex(), expectedSYNs);
	}

	protected ArrayList<Boolean> getExpectedDoneStatus() {
		return _expectedDoneStatusList.get(_expectedNavDirIndex);
	}

	protected void setExpectedDoneStatus(ArrayList<Boolean> expectedDoneStatus) {
		_expectedDoneStatusList.set(getExpectedNavDirIndex(), expectedDoneStatus);
	}

	protected ArrayList<Boolean> getExpectedSaveStatus() {
		return _expectedSaveStatusList.get(_expectedNavDirIndex);
	}

	protected void setExpectedSaveStatus(ArrayList<Boolean> expectedSaveStatus) {
		_expectedSaveStatusList.set(getExpectedNavDirIndex(), expectedSaveStatus);
	}

	protected int getExpectedNavDirIndex() {
		return _expectedNavDirIndex;
	}

	protected void setExpectedNavDirIndex(int expectedNavDirIndex) {
		_expectedNavDirIndex = expectedNavDirIndex;
	}

	@Override
	protected Component getCanvas(int canvasIndex) {
		Component canvas = find(_gui, JSVGCanvas.class, "" + canvasIndex);
		return canvas;
	}

	@Override
	boolean isClickableInDT(DepTree dt, Point point) {
		Component nodesScrollPane = find(_gui, JScrollPane.class, AbstractDisplay.NODES_PANE + 0);
		Rectangle bounds = nodesScrollPane.getBounds();
		return (point != null && bounds.contains(point));
	}

	// remove once the code has been cannibalised
	//	public void testAnno() throws Throwable {
	//		boolean skip = false;
	//		ArrayList<String> dirs = new ArrayList<>();
	//		String properties = "startup.properties";
	//		for (String arg : _args) {
	//			if (! skip) {
	//				if (arg.equals("-c")) {
	//					skip = true;
	//				}
	//				else {
	//					dirs.add(arg);
	//				}
	//			}
	//			else {
	//				properties = arg;
	//				skip = false;
	//			}
	//		}
	//		final AnnoViewer av = new AnnoViewer(properties, dirs);
	//		Frame gui = av.getFrame();
	//		Component navTree = getFinder().find(gui, new Matcher() {
	//			public boolean matches(Component c) {
	//				return (c instanceof JTree);
	//			}
	//		});
	//		JTreeTester tt = new JTreeTester();
	//		tt.actionSelectRow(navTree, 1);
	//		assertTrue(av._dts.get(0).getDoc().getTextContent().contains("1"));
	//		tt.actionSelectRow(navTree, 2);
	//		assertTrue(av._dts.get(0).getDoc().getTextContent().contains("2"));
	//		assertFalse(av._dts.get(0)
	//					.getDoc()
	//					.getTextContent()
	//					.contains("Kriminalität"));
	//		tt.actionSelectRow(navTree, 3);
	//		assertTrue(av._dts.get(0)
	//					.getDoc()
	//					.getTextContent()
	//					.contains("Kriminalität"));
	//		Component navSlider = getFinder().find(gui, new Matcher() {
	//			public boolean matches(Component c) {
	//				return (c instanceof JSlider && c.getName().equals("slider 2"));
	//			}
	//		});
	//		System.out.println(navSlider.getName());
	//		JSliderTester st = new JSliderTester();
	//		st.actionDecrement(navSlider);
	//		assertTrue(av._dts.get(0)
	//					.getDoc()
	//					.getTextContent()
	//					.contains("Kriminalität"));
	//		st.actionIncrement(navSlider);
	//		assertTrue(av._dts.get(0)
	//					.getDoc()
	//					.getTextContent()
	//					.contains("Kriminalität"));
	//		st.actionIncrement(navSlider);
	//		assertTrue(av._dts.get(0)
	//					.getDoc()
	//					.getTextContent()
	//					.contains("Kriminalität"));
	//
	//	}

}
