package de.unihamburg.informatik.nats.jwcdg.gui;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Map;

/**
 * Stores values of a DepTree for comparing important values of two DepTrees in
 * an equal method without comparing all of them, especially not the
 * _nodesCanvas. To be used in class tests.
 * 
 * @author 3zimmer
 * 
 */
public class DTValues {
	public double _bty;
	public Dimension _cSize;
	public Map<Integer, ArrayList<String>> _marked;
	public ArrayList<String> _words;
	public ArrayList<String> _labels;
	public ArrayList<Integer> _links;
	public ArrayList<ArrayList<Integer>> _whoLinks;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(_bty);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((_cSize == null) ? 0 : _cSize.hashCode());
		result = prime * result + ((_labels == null) ? 0 : _labels.hashCode());
		result = prime * result + ((_links == null) ? 0 : _links.hashCode());
		result = prime * result + ((_marked == null) ? 0 : _marked.hashCode());
		result =
					prime
								* result
								+ ((_whoLinks == null) ? 0
											: _whoLinks.hashCode());
		result = prime * result + ((_words == null) ? 0 : _words.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DTValues other = (DTValues) obj;
		if (Double.doubleToLongBits(_bty) != Double.doubleToLongBits(other._bty))
			return false;
		if (_cSize == null) {
			if (other._cSize != null)
				return false;
		}
		else if (! _cSize.equals(other._cSize))
			return false;
		if (_labels == null) {
			if (other._labels != null)
				return false;
		}
		else if (! _labels.equals(other._labels))
			return false;
		if (_links == null) {
			if (other._links != null)
				return false;
		}
		else if (! _links.equals(other._links))
			return false;
		if (_marked == null) {
			if (other._marked != null)
				return false;
		}
		else if (! _marked.equals(other._marked))
			return false;
		if (_whoLinks == null) {
			if (other._whoLinks != null)
				return false;
		}
		else if (! _whoLinks.equals(other._whoLinks))
			return false;
		if (_words == null) {
			if (other._words != null)
				return false;
		}
		else if (! _words.equals(other._words))
			return false;
		return true;
	}

	/**
	 * To be printed when an equals comparison fails.
	 */
	public String toString() {
		return System.lineSeparator() + _bty + System.lineSeparator() + "w "
					+ _cSize.getWidth() + " h " + _cSize.getHeight()
					+ System.lineSeparator() + _marked + System.lineSeparator()
					+ _whoLinks + System.lineSeparator() + _words
					+ System.lineSeparator();
	}

}