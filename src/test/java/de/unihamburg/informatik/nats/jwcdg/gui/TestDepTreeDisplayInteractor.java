package de.unihamburg.informatik.nats.jwcdg.gui;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unihamburg.informatik.nats.jwcdg.IncrementalParserFactory;
import de.unihamburg.informatik.nats.jwcdg.JWCDG;
import de.unihamburg.informatik.nats.jwcdg.incrementality.IncrementalFrobber;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;

// public class TestDepTreeDisplayInteractor {
// 	DepTreeDisplayInteractor _dtdi;
// 	private Parse _p;
// 	private DepTreeGenerator _dtGen;
// 	private DepTree _dt;
// 	private String _cda;

// 	@BeforeClass
// 	public static void setUpBeforeClass() throws Exception {
// 	}

// 	@AfterClass
// 	public static void tearDownAfterClass() throws Exception {
// 	}

// 	@Before
// 	public void setUp() throws Exception {
// 		IncrementalFrobber parser = JWCDG.makeParser(
// 				Configuration.fromFile("startup.properties"),
// 				new IncrementalParserFactory());
// 		_dtdi = new DepTreeDisplayInteractor();
// 		_cda = TestParseFactory.PARSE_HEISE58;
// 		final Parse p = TestParseFactory.generateParseObject(_cda);
// 		_p = p;
// 		_dtGen = new DepTreeGenerator();
// 		_dt = _dtGen.makeTree(p);
// 		_dtdi.setFields(_dt, parser, _p, _dtGen);
// 		// _dtdi.writeToFile((DecoratedParse) p, new File(""));
// 	}

// 	@After
// 	public void tearDown() throws Exception {
// 	}

// 	@Test
// 	public void testDepTreeDisplayInteractor() {
// 		assertNotNull(_dtdi.getDTwithOptions());
// 	}

// 	@Test
// 	public void testWriteToFile() {
// 		String filename = "demo.svg";
// 		File file = new File(filename);
// 		file.delete();
// 		_dtdi.writeToFile(_dtdi.getDTwithOptions().getDecParse(), filename);
// 		assertTrue("No cda file generated.", file.exists());

// 		file.delete();

// 		_dtdi.writeToFile(_dtdi.getDTwithOptions().getDecParse(), file);
// 		assertTrue("No cda file generated.", file.exists());

// 		file.delete();
// 	}

// 	// @Test
// 	// public void testDecorateParse() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testDraw() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testHandleFocus() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testSet_GUI() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testSetOptions() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testActivateTurbo() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testUpdateTable() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testAddParseString() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testAddParseStringMapOfIntegerArrayListOfString() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testSaveSVG() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testSaveCDA() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testSaveAsCDA() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testLoadCDA() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testAmendDescription() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testDrawParse() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testDecorateDecoratedParse() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testDecorateParseParser() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testHandleValueChanged() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testMakeDepTreePopup() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testInit_GUI() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testSetOptionsPreferencesDisplayDepTree() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testRefreshPreferences() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testStringLengthInPixels() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testUpdateTableDepTreeInteractorAbstractDisplay() {
// 	// fail("Not yet implemented");
// 	// }
// 	//
// 	// @Test
// 	// public void testAddConstraint() {
// 	// fail("Not yet implemented");
// 	// }

// }
