package de.unihamburg.informatik.nats.jwcdg.gui;

import de.unihamburg.informatik.nats.jwcdg.gui.AbbotTestDepTreeViewer.DTVMainPage;

/**
 * Automatically tests TestDepTreeViewer.
 * This class is independent of the framework used for automated testing of
 * GUIs.
 * Use by starting the actual tester AbbotTestDepTreeViewer
 * 
 * @author 3zimmer
 * 
 */
public class AutoTestDepTreeViewer {

	private static String _cdaName = AbbotTestDepTreeViewer._cdaName;

	public static void test(DTVMainPage mainWindow) {
		/*
		 * Starts DepTreeViewer without a parse, so no dependency tree is shown.
		 * 1 Selects every menu item, each time immediately closing any window
		 * that appears with each button that does that.
		 * 2 Presses every button.
		 * 3 Presses the root of the navigation tree.
		 * 4 Opens preferences.
		 * 5 Selects each option and changes every value here, each time
		 * pressing the apply button.
		 * 6 Selects each option and changes every value here and presses the save button in
		 * preferences.
		 * *** Enters a sentence into the parse field.
		 * * Saves using each available item for that in the file menu.
		 * _cdaName in "temp".
		 * 
		 * The following actions are done in a randomized order:
		 * 
		 * from here
		 * * Selects random items in the navigation tree.
		 * Selects and deselects random items in the constraints menu.
		 * ** Enters a constraint in the constraints search field and presses
		 * the search button.
		 * ** Uses the zoom slider up and down
		 * Moves every visible scrollbar once in each direction
		 * 
		 * In the DepTree:
		 * *** Generates a REF link from one visible word to another.
		 * Clicks on random visible dots, words, categories (below the words),
		 * SYN and REF links, SYN and REF labels.
		 * 7 Changes a SYN link from one visible word to another in all possible
		 * manners:
		 * - click on / ** drag from link, click on / ** drop to dot / word of
		 * target word (4 combinations)
		 * 8 Same as above, except from the word to itself.
		 * 9 * Drags a random link, then drops it nowhere.
		 * * Changes a REF link from one visible word to another by dragging.
		 * 10 Changes a random SYN label to a [], then to a random value
		 * different from the initial one.
		 * 11 Right clicks one the label.
		 * 12 Changes the category of a random visible word.
		 * Inserts a space in word in the parsefield
		 * * Opens the previously saved .CDA file.
		 * Repeats action 1 to 5.
		 * Selects random Options in the preferences then presses "Save" there.
		 * Repeats action 7, except in order to create a circle.
		 * Repeats action 7 to 12, except on the circle, each time recreating it
		 * when necessary.
		 * to here
		 * 
		 * Tries to close the window.
		 * ** Selects "Cancel" on the resulting warning window.
		 * ** Selects "Yes" on the resulting warning window.
		 * ** Starts DepTreeViewer without a parse
		 * ** * Opens the previously saved .CDA file.
		 * ** Tries to close the window.
		 * ** Selects "No" on the resulting warning window.
		 * ** Starts DepTreeViewer without a parse
		 * ** Closes the window.
		 * 
		 * Deletes the previously saved .CDA file.
		 * Deletes the previously saved .SVG file.
		 * 
		 * 
		 * Tests with a * can not be implemented yet because the actual version
		 * of Abbot does not support it.
		 * Tests with a ** can not be implemented completely yet because of
		 * missing functionality in the DepTreeViewer.
		 * Tests with a *** could be enhanced by adding more tests
		 * 
		 * Missing functionality in DepTreeviewer:
		 * Constraints search works probably, but I don't know how.
		 * Zoom sliders for canvas with DepTree don't appear immediately.
		 * Closing a window before saving doesn't generate a warning yet.
		 */

		mainWindow
//		 			implemented
		
					.testWithoutDT()
		
					.enterParseText("Dies ist ein Test . ")
//					.enterParseText("Dies ist ein eher kurzer Test . Das ist gelogen , er ist ganz schön lang . ")
//
					.testFilesMenuBeforeChanges()
					
//					 somehow randomize order of following:

					.selectAndDeselectRandomInNavTree()
//
					.testConstraints()
					
//					not
					
//					.testZoom()
//
//					.moveEachScrollbar()

//					 implemented
					
					.testPrefs()
//
					.testSelectInDT()
					
//					 not
					
//					.selectRandomDTREFLabel()

//					 implemented
					
					.testChangeDTLinks()
					
//					 not
					
//					.changeRandomDTLink("drag","dot")
//					.changeRandomDTLink("drag","word")

//		 			implemented
					
					.selectFileMenu()
					.selectSave()

//					 not
					
//					.dragRandomDTLinkNowhere();

//					 implemented
					
					.changeRandomSYNLabelToNothing()
					
//					 implemented but bug
					
//					.changeRandomSYNLabel()

//					 implemented
					
					.optimizeRandomSYNLabel()
//
					.changeRandomCategory()

					.testChangeDTLinksToRoot()
					
//					 not
					
//					.changeRandomDTLinkToRoot("drag","dot")
//					.changeRandomDTLinkToRoot("drag","word")

//					.insertSpaceInParse()

//					 implemented
					
					.pressClearTextfieldButton(true)
					.enterParseText("neu ")
					.testOpenFile("temp", _cdaName)
					.testButtons()
					
//					 implemented, but bug
					
//					.pressClearTextfieldButton(true)
					
//					 implemented
					
					.testOpenFile("temp", _cdaName)
					.testSelectRandomInDT()
					
//					 not
					
//					.selectRandomDTREFLabel()
					
//					 implemented
					
					.testChangeDTLinksToCircles()
					
//					not
					
//					.changeRandomDTLinkToCircle("drag","dot")
//					.changeRandomDTLinkToCircle("drag","word")
//
//					 see commentary above about step 7 to 12 on circle

//					not	
					
//					.tryToCloseEverything()
//					.cancelWarning()
//					.tryToCloseEverything()
//					.confirmWarning()
//				;
//				mainWindow
//					.tryToCloseEverything()
//					.denyWarning()
//				;
//				implemented but testing doesn't work
//				mainWindow
//					.closeEveryThing()
		;
	}
}
