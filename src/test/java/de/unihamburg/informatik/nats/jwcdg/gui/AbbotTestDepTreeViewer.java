package de.unihamburg.informatik.nats.jwcdg.gui;

import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.InputEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTree;
import javax.swing.text.Position;

import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;
import de.unihamburg.informatik.nats.jwcdg.parse.Word;
import io.gitlab.nats.deptreeviz.DepTree;
import io.gitlab.nats.deptreeviz.DepTreeNode;
import junit.extensions.abbot.TestHelper;

import org.apache.batik.swing.JSVGCanvas;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.svg.SVGDocument;

import abbot.tester.JTextComponentTester;
import io.gitlab.nats.deptreeviz.DepTree.Elem;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;

/**
 * Tests DepTreeViewer using Abbot. It's more of a test of Abbot right now. Its
 * best started as java application with the main method.
 * TODO many less important tests need more assertions
 *
 * @author 3zimmer
 *
 */
public class AbbotTestDepTreeViewer extends AbbotTestAbstractViewer {
	protected DepTreeViewer _dv;
	protected DepTree<DecoratedParse, Word> _dt;
	public String _expectedWords;
	public String _expectedWordsInFile;
	public static String _cdaName = "test.cda";
	public static String _svgName = "test.svg";

	public AbbotTestDepTreeViewer(String name) {
		super(name);
	}

	public static void main(String[] args) {
		TestHelper.runTests(args, AbbotTestDepTreeViewer.class);
	}


	//	@BeforeClass
	//	public static void setUpBeforeClass() throws Exception {
	//	}
	//
	//	@AfterClass
	//	public static void tearDownAfterClass() throws Exception {
	//	}
	//
	@Override
	@Before
	public void setUp() throws Exception {
		makeTempDir("temp");
		_workingDir = FileService.getWorkingDir();
	}

	@Override
	@After
	public void tearDown() throws Exception {
		FileService.setWorkingDir(_workingDir);
		deleteFile("temp", _cdaName);
		deleteFile("temp", _svgName);
	}

	@Test
	public void testDeptreeViewer() {
		DepTreeViewer dv;
		try {
			dv = new DepTreeViewer(Configuration.fromFile("startup.properties"));
			DTVMainPage mainWindow = new DTVMainPage(dv);
			AutoTestDepTreeViewer.test(mainWindow);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void typeWord(DepTree dt, JTextComponentTester tct, String word) {
		tct.actionKeyString(word);
		tct.actionKeyStroke(' ');
		SVGDocument doc;
		do {
			doc = dt.getDoc();
			tct.actionDelay(1);
		}
		while (doc == null);
		// this isn't enough because several trees containing the same sentence
		// are created
		do {
			doc = dt.getDoc();
			tct.actionDelay(1);
		}
		while (! doc.getTextContent().contains(word));
		tct.actionWaitForIdle();
	}

	@Override
	protected ArrayList<DepTreeNode> getVisibleNodes(int canvasIndex) {
		return doGetVisibleNodes(_dt, canvasIndex);
	}

	// TODO implement classes as used in AutoTestDepTreeViewer

	/**
	 * Implements the page object pattern, representing the main window of the
	 * DepTreeViewer
	 *
	 * @author 3zimmer
	 *
	 */
	public class DTVMainPage extends AbstractMainPage{
		/**
		 * Used when generating the first MainPage representing the window at
		 * the
		 * start of the test
		 *
		 * @param dv
		 *            the DepTreeViewer that is being tested
		 */
		public DTVMainPage(DepTreeViewer dv) {
			super(dv);
			_dv = dv;
			_gui = _dv.getGUI();
			_dt = _dv.getDepTree();
		}

		/**
		 * Used as return value when leaving a Page leads to the main window
		 * without popups being open
		 */
		public DTVMainPage() {
			super();
		}

		DTVMainPage testWithoutDT() {
			return new DTVMainPage()
			.testMenusWithoutDT()
			.testButtonsWithoutDT()
			.selectNavTreeRoot()
			.testPrefsWithoutDT()
			;
		}

		public DTVMainPage testMenusWithoutDT() {
			return new DTVMainPage()
			.selectFileMenu()
			.failToSelectSaveAs()
			.selectFileMenu()
			.failToSelectSave()
			.selectFileMenu()
			.failToSelectExportSVG()
			.selectFileMenu()
			.selectOpen()
			.closeFileChooser()
			;
		}

		public DTVMainPage testButtonsWithoutDT() {
			return new DTVMainPage()
			.pressPrintButton(false)
			.pressSYNButton(false)
			.pressParseButton(false)
			.pressClearTextfieldButton(false)
			;
		}


		public DTVMainPage testPrefsWithoutDT() {
			return new DTVMainPage()
			.selectEditMenu()
			.selectPreferences()
			.selectBoxAndApply(AbstractDisplay.TAGS_BOX, "change")
			.cancelPreferences()
			.selectEditMenu()
			.selectPreferences()
			.setPref(AbstractDisplay.ROOT_BOX, "change")
			.savePrefs()
			;
		}

		public DTVMainPage testFilesMenuBeforeChanges() {
			//			System.out.println("save");
			return new DTVMainPage()
			.selectFileMenu()
			.selectSaveAs()
			.selectTargetFile("temp", _cdaName)
			.selectFileMenu()
			.failToSelectSave()
			.selectFileMenu()
			.selectExportSVG()
			.selectTargetFile("", _svgName)
			;
		}

		public DTVMainPage testConstraints() {
			return new DTVMainPage()
			.selectRandomConstraintsItem()
			.deselectRandomConstraintsItem()
			//			 not implemented
			//			.searchConstraint()
			;
		}

		public DTVMainPage testPrefs() {
			return new DTVMainPage()
			.selectEditMenu()
			.selectPreferences()
			.selectBoxAndApply(AbstractDisplay.TAGS_BOX, "change")
			.setPref(AbstractDisplay.ROOT_BOX, "change")
			.cancelPreferences()
			.selectEditMenu()
			.selectPreferences()
			.selectBoxAndApply(AbstractDisplay.ROOT_BOX, "false")
			.setPref(AbstractDisplay.ROOT_BOX, "change")
			.savePrefs()
			;
		}

		public DTVMainPage testZoom() {
			return new DTVMainPage()
			//			not implemented
			//			.incrementZoom()
			//			.decrementZoom()
			;
		}

		public DTVMainPage testSelectInDT() {
			return new DTVMainPage()
			.generateRandomDTREFLink()
			.selectRandomInDT(Elem.LINE, "SYN")
			.selectRandomInDT(Elem.TEXT, "SYN")
			.selectRandomInDT(Elem.DOT, "SYN")
			.selectRandomInDT(Elem.LINE, "REF")
			.selectRandomInDT(Elem.TEXT, "DEPTREEWORD")
			;
		}

		public DTVMainPage testChangeDTLinks() {
			return new DTVMainPage()
			//					.generateRandomDTREFLink()
			.changeRandomDTLink("click","dot","SYN")
			//					unnecessary because changeRandomSYNLabel
			//					.changeRandomDTLink("click","word","SYN")
			.changeRandomDTLink("click","dot","REF")
			.changeRandomDTLink("click","word","REF")
			;
		}

		public DTVMainPage testChangeDTLinksToRoot() {
			return new DTVMainPage()
			.changeRandomDTLinkToRoot("click","word")
			.testOpenFile("temp", _cdaName)
			.changeRandomDTLinkToRoot("click","dot")
			;
		}

		// TODO change _expectedWords
		public DTVMainPage testOpenFile(String dir, String file) {
			//			System.out.println("testOpen: " + dir + " - " + file);
			FileService.setWorkingDir(_workingDir);
			return new DTVMainPage()
			.selectFileMenu()
			.selectOpen()
			.selectSourceFile("temp", _cdaName)
			;
		}

		public DTVMainPage testButtons() {
			return new DTVMainPage()
			.pressSYNButton(true)
			.pressSYNButton(true)
			.pressPrintButton(true)
			.pressParseButton(true)
			;
		}

		public DTVMainPage testSelectRandomInDT() {
			return new DTVMainPage()
			.selectRandomInDT(Elem.LINE, "SYN")
			.selectRandomInDT(Elem.TEXT, "SYN")
			.selectRandomInDT(Elem.DOT, "SYN")
			.selectRandomInDT(Elem.LINE, "REF")
			.selectRandomInDT(Elem.TEXT, "DEPTREEWORD")
			;
		}

		public DTVMainPage testChangeDTLinksToCircles() {
			return new DTVMainPage()
			.testOpenFile("temp", _cdaName)
			.changeRandomDTLinkToCircle("click", "word")
			.testOpenFile("temp", _cdaName)
			.changeRandomDTLinkToCircle("click","dot")
			;
		}

		public EditMenuPage selectEditMenu() {
			Component fileMenu = find(_gui, "edit menu");
			_ct.actionClick(fileMenu);
			return new EditMenuPage();
		}

		/**
		 * Types the input into the parse field
		 *
		 * @param input
		 * @return the page that is expected to be open afterwards
		 */
		public DTVMainPage enterParseText(String input) {
			//			System.out.println("enterParse");
			Component parseButton = find(_gui, AbstractDisplay.PARSE_BUTTON);
			Component parseField = find(parseButton.getParent(), "parse field");
			_tct.actionClick(parseField);
			for (String word : input.split(" ")) {
				typeWord(_dt, _tct, word);
			}
			_expectedWords = input;
			checkWords();
			return new DTVMainPage();
		}

		/**
		 * Selects random visible DepTrees in the navigation tree
		 *
		 * @return the page that is expected to be open afterwards
		 */
		public DTVMainPage selectAndDeselectRandomInNavTree() {
			doSelectAndDeselectRandomInNavTree("4", 4);
			return new DTVMainPage();
		}

		/**
		 * Selects the nth row in the navigation tree, assuming that there is a
		 * DepTree
		 *
		 * @param n
		 * @return the page that is expected to be open afterwards
		 */
		public DTVMainPage selectNavTreeElement(int n) {
			JTree navTree = (JTree) find(_gui, JTree.class, null);
			// System.out.println("found tree");
			_trt.actionClickRow(navTree, n);
			return new DTVMainPage();
		}

		public DTVMainPage selectRandomConstraintsItem() {
			doSelectRandomConstraintsItem();
			return new DTVMainPage();
		}

		public DTVMainPage deselectRandomConstraintsItem() {
			doDeselectRandomConstraintsItem(_dt);
			return new DTVMainPage();
		}

		public DTVMainPage generateRandomDTREFLink() {
			doGenerateRandomDTREFLink(1);
			return new DTVMainPage();
		}

		public DTVMainPage closeFileChooser() {
			Component chooser = find(_gui, JFileChooser.class, null);
			_cht.actionCancel(chooser);
			return new DTVMainPage();
		}

		public DTVMainPage pressPrintButton(boolean hasEffect) {
			Component button = find(_gui, AbstractDisplay.PRINT_BUTTON);
			_ct.actionClick(button);
			return new DTVMainPage();
		}

		public DTVMainPage pressSYNButton(boolean hasEffect) {
			doPressSYNButton(_dt, hasEffect);
			return new DTVMainPage();
		}

		public DTVMainPage pressParseButton(boolean hasEffect) {
			Component button = find(_gui, AbstractDisplay.PARSE_BUTTON);
			_ct.actionClick(button);
			return new DTVMainPage();
		}

		public DTVMainPage pressClearTextfieldButton(boolean hasEffect) {
			Component button = find(_gui, AbstractDisplay.CLEAR_TEXTFIELD_BUTTON);
			_ct.actionClick(button);
			if (hasEffect){
				assertTrue("Parse field should have been cleared.", _gui.getParseField().getText().equals(""));
			}
			return new DTVMainPage();
		}

		public DTVMainPage selectNavTreeRoot() {
			JTree navTree = (JTree) find(_gui, JTree.class, null);
			_trt.actionSelectPath(navTree,
					navTree.getNextMatch("root", 0, Position.Bias.Forward));
			return new DTVMainPage();
		}

		public DTVMainPage selectRandomInDT(Elem type, String level) {
			// System.out.println(_dt.getDoc().getTextContent());
			Component canvas = find(_gui, JSVGCanvas.class, null);
			for (Point p : getClickablePoints(_dt, type, level)) {
				_ct.actionClick(canvas, (int) p.getX(), (int) p.getY());
				// TODO make work for all levels instead of none
				//				for (DepTreeNode node : _dt){
				//					if ((int) p.getX() == (int) node.getX() && (int) p.getY() == (int) node.getY()){
				//						System.out.println("hit "+ node.getWord());
				//						assertTrue(node.getWord() + " should have been selected", ((Element) node.getElement(type, _dt.getCurrentLevel()).getParentNode()).getAttribute("stroke").equals(_dt.getSelectColor()));
				//					}
				//				}
				//				 _ct.actionDelay(1000);
			}
			return new DTVMainPage();
		}

		public DTVMainPage changeRandomDTLink(String action,
				String target,
				String firstLevel) {
			doChangeRandomDTLink(_dt, action, target, firstLevel);
			return new DTVMainPage();
		}

		public DTVFileMenuPage selectFileMenu() {
			doSelectFileMenu();
			return new DTVFileMenuPage();
		}

		public DTVMainPage changeRandomDTLinkToRoot(String action, String target) {
			Elem type = StringToElem(target);
			String level = StringToLevel(_dt, target);
			Component canvas = find(_gui, JSVGCanvas.class, null);
			for (DepTreeNode node : _dt) {
				Point point1 = getPoint(_dt, node, Elem.LINE, _dt.getCurrentLevel());
				Point point2 = getPoint(_dt, node, type, level);
				if (isClickableInDT(_dt, point1) && isClickableInDT(_dt, point2)) {
					_ct.actionClick(canvas, point1.x, point1.y);
					_ct.actionClick(canvas,
							point2.x,
							point2.y,
							InputEvent.BUTTON3_MASK);
				}
			}
			return new DTVMainPage();
		}

		public DTVMainPage changeRandomSYNLabel() {
			Component canvas = find(_gui, JSVGCanvas.class, null);
			for (Point p : getClickablePoints(_dt, Elem.TEXT, "SYN")) {
				_ct.actionClick(canvas, (int) p.getX(), (int) p.getY());
				//				System.out.println("S" + AbstractDisplay.MENU_ITEM);
				Component item = find(_gui, null, "S" + AbstractDisplay.MENU_ITEM);
				_mit.actionClick(item);
			}
			return new DTVMainPage();
		}

		public DTVMainPage changeRandomSYNLabelToNothing() {
			Component canvas = find(_gui, JSVGCanvas.class, null);
			for (Point p : getClickablePoints(_dt, Elem.TEXT, "SYN")) {
				// _ct.actionDelay(1000);
				_ct.actionClick(canvas, (int) p.getX(), (int) p.getY());
				Component item = find(_gui, null, "" + AbstractDisplay.MENU_ITEM);
				_ct.actionClick(item);
			}
			return new DTVMainPage();
		}

		public DTVMainPage optimizeRandomSYNLabel() {
			Component canvas = find(_gui, JSVGCanvas.class, null);
			for (Point p : getClickablePoints(_dt, Elem.TEXT, "SYN")) {
				_ct.actionClick(canvas,
						(int) p.getX(),
						(int) p.getY(),
						InputEvent.BUTTON3_MASK);
			}
			return new DTVMainPage();
		}

		public DTVMainPage changeRandomCategory() {
			Component canvas = find(_gui, JSVGCanvas.class, null);
			for (Point p : getClickablePoints(_dt, Elem.TEXT, "DEPTREEWORD")) {
				_ct.actionClick(canvas, (int) p.getX(), (int) p.getY());
				// Component menu = find(_gui, JPopupMenu.class, null);
				Component item = find(_gui, JMenuItem.class, null);
				_ct.actionClick(item);
			}
			return new DTVMainPage();
		}

		public DTVMainPage
		changeRandomDTLinkToCircle(String action, String target) {
			Elem type = StringToElem(target);
			String level = StringToLevel(_dt, target);
			Component canvas = find(_gui, JSVGCanvas.class, null);
			SVGDocument doc;
			for (DepTreeNode node : _dt) {
				do {
					doc = _dt.getDoc();
					_ct.actionDelay(1);
				}
				while (doc == null);
				if (! node.linkIsRoot("SYN")){
					double y1 = node.getY();
					DepTreeNode linkedNode = _dt.getNode(node.getLink("SYN"));
					double y2 = linkedNode.getY();
					// if linking line is neither backwards nor to root
					if (! linkedNode.linkIsRoot("SYN") && y2 < y1){
						y1 = linkedNode.getY();
						DepTreeNode linkedNode2 = _dt.getNode(linkedNode.getLink("SYN"));
						y2 = linkedNode2.getY();
						// if second linking line is not backwards
						if (y2 < y1){
							Point point1 = getPoint(_dt, node, type,  level);
							Point point2 = getPoint(_dt, linkedNode, Elem.LINE, _dt.getCurrentLevel());
							if (isClickableInDT(_dt, point1) && isClickableInDT(_dt, point2)) {
								//				_ct.actionDelay(3000);
								_ct.actionClick(canvas, point2.x, point2.y);
								_ct.actionClick(canvas,
										point1.x,
										point1.y,
										InputEvent.BUTTON3_MASK);
								//				_ct.actionDelay(3000);
							}
						}
					}
				}
			}
			return new DTVMainPage();
		}

		public void closeEveryThing() {
			selectFileMenu()
			.selectQuit();
		}
	}


	/**
	 * Implements the page object pattern, representing the menu "File" of
	 * the
	 * DepTreeViewer
	 *
	 * @author 3zimmer
	 *
	 */
	public class DTVFileMenuPage extends AbstractFileMenuPage{
		public DTVFileMenuPage() {
			assertTrue ("File Menu should be open.", ((JMenu) find(_gui, "file menu")).isSelected());
			assertFalse ("Preferences Window should not be open.", _gui._prefD.isVisible());
			delay();
		}

		public void selectQuit() {
			selectInFile("quit item");
		}

		public SaveAsPage selectSaveAs() {
			//				System.out.println("selectSaveAs");
			selectInFile("saveas item");
			return new SaveAsPage();
		}

		public DTVMainPage selectSave() {
			//				System.out.println("selectSave");
			selectInFile("save item");
			_expectedWordsInFile = _expectedWords;
			expectWordsInFile(FileService.getWorkingDir().getAbsolutePath(), _cdaName);
			return new DTVMainPage();
		}

		/**
		 * Tries to select the "Save As..." button in the file menu. Expects
		 * it
		 * not to succeed, for example when there is not tree to save yet,
		 * instead
		 * returning to the main screen
		 *
		 * @return the page that is expected to be open afterwards
		 */
		public DTVMainPage failToSelectSaveAs() {
			return failToSelect("saveas item");
		}

		/**
		 * Tries to select the "Save" button in the file menu. Expects it
		 * not to
		 * succeed, for example when there is not tree to save yet, instead
		 * returning to the main screen
		 *
		 * @return the page that is expected to be open afterwards
		 */
		public DTVMainPage failToSelectSave() {
			return failToSelect("save item");
		}

		/**
		 * Tries to select the "Export as SVG..." button in the file menu.
		 * Expects
		 * it not to succeed, for example when there is not tree to save
		 * yet,
		 * instead returning to the main screen
		 *
		 * @return the page that is expected to be open afterwards
		 */
		public DTVMainPage failToSelectExportSVG() {
			return failToSelect("exportsvg item");
		}

		/**
		 * Tries to select the button with the name itemName in the file
		 * menu.
		 * Expects it not to succeed,
		 *
		 * @return the page that is expected to be open afterwards
		 */
		public DTVMainPage failToSelect(String itemName) {
			JMenu fileMenu;
			fileMenu = (JMenu) find(_gui, "file menu");
			JMenuItem item = (JMenuItem) find(fileMenu, itemName);
			assertFalse("Save Item shouldn't be selectable when there is nothing to save", item.isEnabled());
			_ct.actionClick(fileMenu);
			return new DTVMainPage();
		}

		public OpenPage selectOpen() {
			selectInFile("open item");
			return new OpenPage();
		}

		public SaveAsPage selectExportSVG() {
			selectInFile("exportsvg item");
			return new SaveAsPage();
		}

	}

	/**
	 * Implements the page object pattern, representing the file selection
	 * window
	 * of the DepTreeViewer
	 *
	 * @author 3zimmer
	 *
	 */
	public class SaveAsPage {
		public SaveAsPage() {
			assertFalse ("File Menu should not be open.", ((JMenu) find(_gui, "file menu")).isSelected());
			assertFalse ("Preferences Window should not be open.", _gui._prefD.isVisible());
			delay();
		}

		public DTVMainPage selectTargetFile(String dirname, String filename) {
			JFileChooser chooser =
					(JFileChooser) find(_gui, JFileChooser.class, null);
			dirname = FileService.getWorkingDir() + File.separator + dirname;
			_cht.actionSetDirectory(chooser, dirname);
			_cht.actionSetFilename(chooser, filename);
			_cht.actionApprove(chooser);
			_expectedWordsInFile = _expectedWords;
			expectWordsInFile(dirname, filename);
			//				JButton yesButton = findButton(_gui, "Ja");
			//				_cht.actionClick(yesButton);
			return new DTVMainPage();
		}

		public DTVMainPage close() {
			return new DTVMainPage();
		}

	}

	/**
	 * Implements the page object pattern, representing the file selection
	 * window
	 * of the DepTreeViewer
	 *
	 * @author 3zimmer
	 *
	 */
	public class OpenPage {
		public OpenPage() {
			assertFalse ("File Menu should not be open.", ((JMenu) find(_gui, "file menu")).isSelected());
			assertFalse ("Preferences Window should not be open.", _gui._prefD.isVisible());
			delay();
		}

		public DTVMainPage selectSourceFile(String dirname, String filename) {
			JFileChooser chooser =
					(JFileChooser) find(_gui, JFileChooser.class, null);
			_cht.actionSetDirectory(chooser, dirname);
			_cht.actionSetFilename(chooser, filename);
			_cht.actionApprove(chooser);
			_expectedWords = _expectedWordsInFile;
			checkWords();
			return new DTVMainPage();
		}

		public DTVMainPage closeFileChooser() {
			Component chooser = find(_gui, JFileChooser.class, null);
			_cht.actionCancel(chooser);
			return new DTVMainPage();
		}
	}

	/**
	 * Implements the page object pattern, representing the menu "Edit" of
	 * the
	 * DepTreeViewer
	 *
	 * @author 3zimmer
	 *
	 */
	public class EditMenuPage {
		public EditMenuPage() {
			assertTrue ("Edit Menu should be open.", ((JMenu) find(_gui, "edit menu")).isSelected());
			assertFalse ("Preferences Window should not be open.", _gui._prefD.isVisible());
			delay();
		}

		public PreferencesPage selectPreferences() {
			selectInEdit("pref item");
			return new PreferencesPage();
		}

		private void selectInEdit(String itemName) {
			select(itemName, "edit menu");
		}
	}

	/**
	 * Implements the page object pattern, representing the preferences
	 * window
	 * of the DepTreeViewer
	 *
	 * @author 3zimmer
	 *
	 */
	public class PreferencesPage {
		public PreferencesPage() {
			assertTrue ("Preferences Window should be open.", _gui._prefD.isVisible());
			assertFalse ("File Menu should not be open.", ((JMenu) find(_gui, "file menu")).isSelected());
			assertFalse ("Edit Menu should not be open.", ((JMenu) find(_gui, "edit menu")).isSelected());
			delay();
		}

		// private JCheckBox findBox(String field) {
		// return (JCheckBox) find(_gui, null, field);
		// }

		public PreferencesPage selectBoxAndApply(String name, String value) {
			JCheckBox box = (JCheckBox) find(_gui._prefD, null, name);
			Component button =
					find(_gui._prefD, null, AbstractDisplay.APPLY_PREFS_BUTTON);
			if ("change".equals(value)
					|| "true".equals(value) && ! box.isSelected()
					|| "false".equals(value) && box.isSelected()) {
				_ct.actionClick(box);
				_ct.actionClick(button);
			}
			return new PreferencesPage();
		}

		public DTVMainPage cancelPreferences() {
			Component button =
					find(_gui._prefD, null, AbstractDisplay.CANCEL_PREFS_BUTTON);
			_ct.actionClick(button);
			return new DTVMainPage();
		}

		public PreferencesPage setPref(String name, String value) {
			JCheckBox box = (JCheckBox) find(_gui._prefD, null, name);
			if ("change".equals(value)
					|| "true".equals(value) && ! box.isSelected()
					|| "false".equals(value) && box.isSelected()) {
				_ct.actionClick(box);
			}
			return new PreferencesPage();
		}

		public DTVMainPage savePrefs() {
			Component button =
					find(_gui._prefD, null, AbstractDisplay.SAVE_PREFS_BUTTON);
			_ct.actionClick(button);
			return new DTVMainPage();
		}
	}

	public void checkWords() {
		assertTrue("The DepTree should contain the Sentence " + _expectedWords,
				containsWords(_dt.getDoc().getTextContent(), _expectedWords));
	}

	private void expectWordsInFile(String dirname, String filename) {
		//			System.out.println("expecting");
		checkWordsInFile(dirname, filename, _expectedWordsInFile);
	}

	@Override
	protected Component getCanvas(int canvasIndex) {
		Component canvas = find(_gui, JSVGCanvas.class, null);
		return canvas;
	}

	@Override
	boolean isClickableInDT(DepTree dt, Point point) {
		Rectangle bounds =((DepTreeDisplay) _gui).getNodesScrollPane().getBounds();
		return (point != null && bounds.contains(point));
	}
}
