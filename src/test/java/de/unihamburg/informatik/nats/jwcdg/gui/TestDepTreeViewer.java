package de.unihamburg.informatik.nats.jwcdg.gui;

import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;
import de.unihamburg.informatik.nats.jwcdg.parse.Word;
import io.gitlab.nats.deptreeviz.DepTree;
import org.antlr.runtime.RecognitionException;
import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.swing.JSVGCanvas;
import org.apache.batik.util.XMLResourceDescriptor;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.w3c.dom.Document;
import org.w3c.dom.svg.SVGDocument;

import javax.swing.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestDepTreeViewer {

	private static String _fileName = "startup.properties";
	protected static String _s0 = TestParseFactory.PARSE_0;
	protected static String _s1 = TestParseFactory.PARSE_1;
	protected static String _s2 = TestParseFactory.PARSE_2;
	protected static String _sc = TestParseFactory.PARSE_CIRCLE;
	protected static String _sh = TestParseFactory.PARSE_HEISE58;

	/**
	 * @author Sven Zimmer
	 */
	public static void main(String[] args) {

		try {
			final Parse p0 = TestParseFactory.generateParseObject(_sh);
			final Map<Integer, ArrayList<String>> markedNodes;
			markedNodes = new HashMap<Integer, ArrayList<String>>();
			ArrayList<String> markedLevels = new ArrayList<String>();
			ArrayList<String> marked2 = new ArrayList<String>();
			markedLevels.add("SYN");
			marked2.add("SYN");
			marked2.add("REF");
			markedNodes.put(1, markedLevels);
			markedNodes.put(3, markedLevels);
			markedNodes.put(6, marked2);

			JFrame frame = new JFrame();
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			JPanel bPanel = new JPanel();
			final ArrayList<JButton> buttons = new ArrayList<JButton>();
			buttons.add(new JButton("test all"));
			buttons.add(new JButton("test navigator"));
			buttons.add(new JButton("test viewer"));
			buttons.add(new JButton("test generator"));
			buttons.add(new JButton("test writer"));

			for (final JButton button : buttons) {
				bPanel.add(button);
				button.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						try {
							int i = buttons.indexOf(button);
							switch (i) {
							case 0: {
							}
							case 1: {
								testNavigator(p0, markedNodes);
								if (i != 0) {
									break;
								}
							}
							case 2: {
								testViewer(p0, markedNodes);
								if (i != 0) {
									break;
								}
							}
							case 3: {
								testGenerator(p0, markedNodes);
								if (i != 0) {
									break;
								}
							}
							case 4: {
								testWriter(p0, markedNodes);
								break;
							}
							}
						}
						catch (IOException | RecognitionException e) {
							e.printStackTrace();
						}
						catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						finally {
						}
					}
				});
			}

			frame.getContentPane().add(bPanel);
			frame.setSize(800, 600);
			frame.setVisible(true);

		}
		catch (RecognitionException e) {
			e.printStackTrace();
		}
		finally {
		}

		// testViewerNoParams();

	}

	/**
	 * Tests the method writeTree of the class DepTreeGenerator by letting it
	 * generate a svg file in the actual folder.
	 * Then it reads it and puts the resulting picture into a window.
	 *
	 * @param p0
	 *            a parse that can be generated by TestParseFactory
	 * @param markedNodes
	 *            some example nodes that are to be marked
	 * @throws IOException
	 * @throws TransformerException
	 */
	private static void testWriter(Parse p0,
			Map<Integer, ArrayList<String>> markedNodes) throws Exception {
		List<String> words = new ArrayList<String>();
		for (Word word : p0.getWords()) {
			words.add(word.word);
		}
		String filename = "demo.svg";
		Writer w =
				new OutputStreamWriter(new FileOutputStream(filename),
						"UTF-8");
		PrintWriter writer = new PrintWriter(w);
		DepTree.writeTree(p0.getLevels(),
				p0.getVerticesLabels(),
				p0.getVerticesStructure(),
				words,
				markedNodes,
				writer);

		File file = new File(filename);
		String parserName = XMLResourceDescriptor.getXMLParserClassName();
		SAXSVGDocumentFactory f = new SAXSVGDocumentFactory(parserName);
		String uri = file.toURI().toString();
		Document doc = f.createDocument(uri);

		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount",
				"4");
		transformer.transform(new DOMSource(doc),
				new StreamResult(new OutputStreamWriter(System.out, "UTF-8")));

		file.delete();

		JFrame frame = new JFrame();
		JSVGCanvas canvas = new JSVGCanvas();
		canvas.setSVGDocument((SVGDocument) doc);
		JScrollPane sp = new JScrollPane(canvas);
		frame.getContentPane().add(sp);
		frame.setSize(800, 600);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/**
	 * Tests the method genereateTree of the class DepTreeGenerator by letting
	 * it generate a Deptree.
	 * Then it puts the resulting picture that the DepTree contains into a
	 * window.
	 *
	 * @param p0
	 *            a parse that can be generated by TestParseFactory
	 * @param markedNodes
	 *            some example nodes that are to be marked
	 * @throws IOException
	 */
	private static void testGenerator(Parse p0,
			Map<Integer, ArrayList<String>> markedNodes) throws IOException {
		DepTree dt = new DepTree(p0, markedNodes);
		DepTreeDisplay dtd = new DepTreeDisplay(dt, false, null);
		dtd.setVisible(true);
	}

	/**
	 * Tests the class DepTreeViewer with an example parse.
	 *
	 * @param p0
	 *            a parse that can be generated by TestParseFactory
	 * @param markedNodes
	 *            some example nodes that are to be marked
	 * @throws IOException
	 */
	private static DepTreeViewer testViewer(Parse p0,
			Map<Integer, ArrayList<String>> markedNodes) throws IOException {
		DepTreeViewer dtv = new DepTreeViewer(Configuration.fromFile(_fileName));
		dtv.add(p0, "first parse", markedNodes);
		return dtv;
	}

	/**
	 * Tests the class DepTreeViewer with some example parses.
	 *
	 * @param p0
	 *            a parse that can be generated by TestParseFactory
	 * @param markedNodes
	 *            some example nodes that are to be marked
	 * @throws IOException
	 */
	private static void
	testNavigator(Parse p0,
			Map<Integer, ArrayList<String>> markedNodes) throws IOException,
			RecognitionException {
		DepTreeViewer dtv = testViewer(p0, markedNodes);
		p0 = TestParseFactory.generateParseObject(TestParseFactory.PARSE_2);
		dtv.add(p0, "Parse 2");
		p0 =
				TestParseFactory.generateParseObject(TestParseFactory.PARSE_CIRCLE);
		dtv.add(p0, "Parse Circle");
		p0 =
				TestParseFactory.generateParseObject(TestParseFactory.PARSE_HEISE58);
		dtv.add(p0, "Parse Heise 58");
	}

	/**
	 * Tests the class DepTreeViewer
	 *
	 * @return the generated DepTreeViewer, which opens a window
	 */
	//	private static DepTreeViewer testViewerNoParams() {
	//		DepTreeViewer dtv = new DepTreeViewer(_fileName);
	//		return dtv;
	//	}

	//	private static void makeParse() {
	//		try {
	//			final Parse p0 = TestParseFactory.generateParseObject(_sh);
	//			final Map<Integer, ArrayList<String>> markedNodes;
	//			markedNodes = new HashMap<Integer, ArrayList<String>>();
	//			ArrayList<String> markedLevels = new ArrayList<String>();
	//			ArrayList<String> marked2 = new ArrayList<String>();
	//			markedLevels.add("SYN");
	//			marked2.add("SYN");
	//			marked2.add("REF");
	//			markedNodes.put(1, markedLevels);
	//			markedNodes.put(3, markedLevels);
	//			markedNodes.put(6, marked2);
	//		}
	//		catch (RecognitionException e) {
	//			e.printStackTrace();
	//		}
	//	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

}
