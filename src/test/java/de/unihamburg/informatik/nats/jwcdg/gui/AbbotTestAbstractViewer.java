package de.unihamburg.informatik.nats.jwcdg.gui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.text.Position;

import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;
import de.unihamburg.informatik.nats.jwcdg.parse.Word;
import io.gitlab.nats.deptreeviz.DepTree;
import io.gitlab.nats.deptreeviz.DepTreeNode;
import junit.extensions.abbot.ComponentTestFixture;

import org.apache.batik.swing.JSVGCanvas;
import org.w3c.dom.Element;

import abbot.finder.ComponentNotFoundException;
import abbot.finder.Matcher;
import abbot.finder.MultipleComponentsFoundException;
import abbot.tester.ComponentTester;
import abbot.tester.JFileChooserTester;
import abbot.tester.JMenuItemTester;
import abbot.tester.JSliderTester;
import abbot.tester.JTextComponentTester;
import abbot.tester.JTreeTester;
import io.gitlab.nats.deptreeviz.DepTree.Elem;

/**
 * Contains the code shared between AbbotTestDepTreeViewer and 
 * AbbotTestAnnoViewer. 
 * 
 * @author 3zimmer
 *
 */
abstract class AbbotTestAbstractViewer extends ComponentTestFixture{
	protected AbstractDisplay _gui;
	// this is only used so a human can see what is happening, otherwise set to
	// 0
	protected int _delay;
	protected ComponentTester _ct;
	protected JTextComponentTester _tct;
	protected JMenuItemTester _mit;
	protected JTreeTester _trt;
	// private JTableTester _tat;
	protected JFileChooserTester _cht;
	protected JSliderTester _st;
	protected static File _workingDir;

	public AbbotTestAbstractViewer(String name) {
		super(name);
	}

	/**
	 * Generates a temporary directory that the abbot test will save files in.
	 * 
	 * @param dirname the relative name of the directory
	 */
	static void makeTempDir(String dirname) {
		File workingDir = FileService.getWorkingDir();
		/*
		 * If something went wrong and a second directory with the same name
		 * has been set as the working directory, e.g. "./temp/temp" then move
		 * up one directory level and set the working directory there.
		 */
		if (workingDir.getAbsolutePath().endsWith(dirname)){
			workingDir = workingDir.getParentFile();
			FileService.setWorkingDir(workingDir);
		}
		File tempDir = new File(workingDir.getAbsolutePath() + File.separator + dirname);
		assertFalse("Directory " + tempDir.getName() + " shoudn't already exist in :\n" + workingDir.getAbsolutePath(), tempDir.exists());
		tempDir.mkdirs();
	}
	
	/**
	 * Deletes a file in a sub directory in the current working directory. If
	 * the file is a directory then only delete it if i is empty.
	 * If it was the last file, then also delete the sub directory.
	 * 
	 * @param dirname the relative name of the directory that contains the file
	 * @param filename the relative name of the file
	 */
	static void deleteFile(String dirname, String filename) {
		File workingDir = FileService.getWorkingDir();
		File tempDir = new File(workingDir.getAbsolutePath() + File.separator + dirname);
		File file = new File(tempDir.getAbsolutePath() + File.separator + filename);
//		System.out.println("deleting " + file.getAbsolutePath());
		if (file.exists()){
			if (file.isFile() || (file.isDirectory() && file.list().length == 0)){
				file.delete();
			}
		}
		if (tempDir.list().length == 0){
			tempDir.delete();
		}
	}

	/**
	 * Finds a component in a container by its name
	 * 
	 * @param container
	 * @param name
	 *            the name the component was given in AbstractDisplay
	 * @return the component found
	 * @throws ComponentNotFoundException
	 * @throws MultipleComponentsFoundException
	 */
	protected Component find(Container container, String name) {
		Component result = null;
		try {
			result = getFinder().find(container, new DepTreeMatcher(name));
		}
		catch (ComponentNotFoundException | MultipleComponentsFoundException e) {
			e.printStackTrace();
		}
		return result;
	}

	protected Component find(Container container, Class<?> cl, String name) {
		Component result = null;
		try {
			result = getFinder().find(container, new DepTreeMatcher(cl, name));
		}
		catch (ComponentNotFoundException | MultipleComponentsFoundException e) {
			e.printStackTrace();
		}
		return result;
	}

	protected abstract ArrayList<DepTreeNode> getVisibleNodes(int canvasIndex);

	// TODO should use bounds of nodesscrollpane
	protected ArrayList<DepTreeNode> doGetVisibleNodes(DepTree<DecoratedParse, Word> dt, int canvasIndex) {
//		Component canvas = find(_gui, JSVGCanvas.class, "" + treeIndex);
		Component canvas = getCanvas(canvasIndex);
		ArrayList<DepTreeNode> foundNodes = new ArrayList<DepTreeNode>();
		Rectangle bounds = canvas.getBounds();
		for (DepTreeNode node : dt) {
			if (bounds.contains(node.getX(), node.getY())) {
				foundNodes.add(node);
			}
		}
		return foundNodes;
	}

	protected Elem StringToElem(String target) {
		Elem type = null;
		if ("dot".equals(target)) {
			type = Elem.DOT;
		}
		else if ("word".equals(target)) {
			type = Elem.TEXT;
		}
		return type;
	}

	protected String StringToLevel(DepTree dt, String target) {
		String level = null;
		if ("dot".equals(target)) {
			level = dt.getCurrentLevel();
		}
		else if ("word".equals(target)) {
			level = "DEPTREEWORD";
		}
		return level;
	}

	/**
	 * Finds the DepTreeNode that has an graphic element at certain coordinates in the DepTree 
	 * 
	 * @param point the coordinates where to look
	 * @param type the type of the graphical element
	 * @param level the level of the graphical element
	 * @return the node if found, null otherwise
	 */
	private DepTreeNode pointToDTNode(DepTree<DecoratedParse, Word> dt, Point point, Elem type, String level) {
		DepTreeNode result = null;
		for (DepTreeNode node : dt){
			if (point.equals(getPoint(dt, node, type, level))){
				result = node;
			}
		}
		return result;
	}
	
	protected abstract Component getCanvas(int canvasIndex);
	
	protected Point getCurrentNavTreeSelection() {
		JTree navTree = (JTree) find(_gui, JTree.class, null);
		Rectangle box =
					navTree.getPathBounds(navTree.getSelectionPath());
		return new Point((int) box.getCenterX(), (int) box.getCenterY());
	}

	protected Point getNavTreePosition(String navDirname, int navDirPos) {
		JTree navTree = (JTree) find(_gui, JTree.class, null);
		Rectangle box =
					navTree.getPathBounds(navTree.getNextMatch(navDirname,
								navDirPos,
								Position.Bias.Forward));
		return new Point((int) box.getCenterX(), (int) box.getCenterY());
	}
	
	protected void selectInNavTree(Point point){
		JTree navTree = (JTree) find(_gui, JTree.class, null);
		if (isClickableInNav(point)){
			_ct.actionClick(navTree, point.x, point.y);
		}
	}
	
	protected boolean isClickableInNav(Rectangle box) {
		Point point = new Point((int) box.getCenterX(), (int) box.getCenterY());
		return isClickableInNav(point) ;
	}
	
	protected boolean isClickableInNav(Point point) {
		Rectangle bounds =_gui._navTreeScrollPane.getBounds();
		return (point != null && bounds.contains(point));
	}

	public void delay() {
		_ct.actionDelay(_delay);
	}

	/**
	 * Selects the item itemName in the menu menuName. Internal use only
	 * 
	 * @param itemName
	 */
	protected void select(String itemName, String menuName) {
		JMenu fileMenu;
		fileMenu = (JMenu) find(_gui, menuName);
		Component item = find(fileMenu, itemName);
		_mit.actionSelectMenuItem(item);
	}
	
	/**
	 * Returns true if the string contains the words in the correct order, with
	 * any other characters in between 
	 * 
	 * @param string the string containing the words, in the format of an annotation
	 * @param words the words contained in the string
	 * @return
	 */
	public boolean containsWords(String string, String words){
//		System.out.println(".*" + words.replaceAll(" ", ".*") + ".*");
		return string.replaceAll("\n",  "").matches(".*" + words.replaceAll(" ", ".*") + ".*");
	}

	protected void checkWordsInFile(String dirname,
				String filename,
				String expectedWordsInFile) {
		String fullName = dirname + File.separator + filename;
		File file = new File(fullName);
		if (file.exists() || expectedWordsInFile != null){
			assertTrue("File " + filename + " in directory " + dirname + " should exist.", file.exists());
			String foundWords = FileService.loadText(file);
			assertTrue("Expected no words in file " + filename + " in directory " + dirname + " , instead found:\n" + foundWords, expectedWordsInFile != null);
//			System.out.println("checking for:" + expectedWordsInFile + " in: " + filename + "\n containing: " + foundWords);
			assertTrue("File " + filename + " in directory " + dirname 
							+ " should contain the words: \n" 
							+ expectedWordsInFile 
							+ "\n Instead it did contain: \n" 
							+ foundWords
						, containsWords(foundWords, expectedWordsInFile));
		}
	}

	protected void checkWordsInTree(DepTree dt, String expectedSYNs){
		String content = dt.getDoc().getTextContent();
		for (String word : content.split(" ")){
			assertTrue("The DepTree should contain the Label " + word + "\n instead:\n" + content, 
					content.contains(word));
		}
	}

	protected Point getPoint(DepTree dt, DepTreeNode node, Elem type, String level) {
		Point result = null;
		// ensures that only when the coordinates are set, the point can be
		// within
		// the bounds of the canvas
		double x = - 1;
		double y = - 1;
		if (dt.getCurrentLevel().equals(level)) {
			if (Elem.DOT.equals(type)) {
				Element element = node.getElement(Elem.DOT, level);
				x = Double.parseDouble(element.getAttribute("cx"));
				y = Double.parseDouble(element.getAttribute("cy"));
			}
			else {
				Element element = node.getElement(Elem.LINE, "SYN");
				double x1 = Double.parseDouble(element.getAttribute("x1"));
				double y1 = Double.parseDouble(element.getAttribute("y1"));
				double x2 = Double.parseDouble(element.getAttribute("x2"));
				double y2 = Double.parseDouble(element.getAttribute("y2"));
				x = (x1 + x2) / 2;
				y = (y1 + y2) / 2 + dt.getLineThickness();
				if (type.equals(Elem.TEXT)) {
					// AffineTransform t = new AffineTransform();
					double fontHalfSize =
								(Integer.parseInt(((Element) element.getParentNode()).getAttribute("font-size")) / 2);
					if (node.linkIsRoot(level)) {
						x = x + fontHalfSize * 2;
					}
					else {
						y = y - fontHalfSize;
					}
				}
			}
		}
		else if ("DEPTREEWORD".equals(level)) {
			Element element = node.getElement(Elem.TEXT, "DEPTREEWORD");
			double fontHalfSize =
						(Integer.parseInt(((Element) element.getParentNode()).getAttribute("font-size")) / 2);
			x = Double.parseDouble(element.getAttribute("x")) + fontHalfSize;
			y = Double.parseDouble(element.getAttribute("y")) - fontHalfSize;
		}
		else {
			Element element = node.getElement(Elem.LINE, level);
			if (element != null) {
				String[] curve = element.getAttribute("d").split("C");
				curve = curve[1].split(" ");
				double x1 = Double.parseDouble(curve[0]);
				double y1 = Double.parseDouble(curve[1]);
				double x2 = Double.parseDouble(curve[2]);
				double y2 = Double.parseDouble(curve[3]);
				x = (x1 + x2) / 2;
				y = (y1 + y2) / 2 - dt.getLineThickness();
				 double fontHalfSize = (Integer.parseInt(((Element) element
							 .getParentNode()).getAttribute("font-size")) / 2);
				 y = y + fontHalfSize;
			}
		}
		if (x != - 1 && y != - 1) {
			result = new Point((int) x, (int) y);
		}
		return result;
	}

	protected ArrayList<Point> getClickablePoints(DepTree<DecoratedParse, Word> dt, Elem type, String level) {
		ArrayList<Point> foundPoints = new ArrayList<Point>();
		for (DepTreeNode node : dt) {
			Point point = getPoint(dt, node, type, level);
			if (isClickableInDT(dt, point)) {
				foundPoints.add(point);
			}
		}
		return foundPoints;
	}

	abstract boolean isClickableInDT(DepTree dt, Point point);

	protected JButton findButton(Container container, final String label) {
		JButton result = null;
		try {
			result = (JButton) getFinder().find(container, new Matcher() {
				public boolean matches(Component c) {
					return (c instanceof JButton && ((JButton) c).getText()
								.equals(label));
				}
			});
		}
		catch (ComponentNotFoundException | MultipleComponentsFoundException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Used for finding components by their class or their name given in
	 * AbstractDisplay
	 * 
	 * @author 3zimmer
	 * 
	 */
	protected class DepTreeMatcher implements Matcher {
		private Class<?> _class;
		private String _name;

		DepTreeMatcher(String name) {
			_name = name;
		}

		DepTreeMatcher(Class<?> cl, String name) {
			_class = cl;
			_name = name;
		}

		@Override
		public boolean matches(Component component) {
//			if (_class == null && component.getClass()
//						.equals(_class)){
//			if (_class != null && _class.equals(JSVGCanvas.class) && component.getClass().equals(JSVGCanvas.class)){
//				System.out.println("comparing: " + _name + " - " + _class + " --- " + component.getName() + " - " + component.getClass());
//			}
			return ((_name == null || _name.equals(component.getName())) && (_class == null || component.getClass()
						.equals(_class)));
		}
	}
	
	// TODO put methods for pages with shared code here
	abstract class AbstractMainPage {

		public AbstractMainPage(AbstractViewer av) {
			_tct = new JTextComponentTester();
			_ct = new ComponentTester();
			_mit = new JMenuItemTester();
			_trt = new JTreeTester();
			// _tat = new JTableTester();
			_cht = new JFileChooserTester();
			_st = new JSliderTester();
			_delay = 0;
			delay();
		}
		
		public AbstractMainPage() {
			assertFalse ("File Menu should not be open.", ((JMenu) find(_gui, "file menu")).isSelected());
			assertFalse ("Edit Menu should not be open.", ((JMenu) find(_gui, "edit menu")).isSelected());
			assertFalse ("Preferences Window should not be open.", _gui._prefD.isVisible());
			delay();
		}

		/**
		 * Selects the menu "File"
		 */
		public void doSelectFileMenu() {
			Component fileMenu = find(_gui, "file menu");
			_ct.actionClick(fileMenu);
		}
		
		protected void doSelectAndDeselectRandomInNavTree(String navDirname, int navDirPos){
			Point point1 = getNavTreePosition(navDirname, navDirPos);
			Point point2 = getCurrentNavTreeSelection();
			selectInNavTree(point1);
			selectInNavTree(point2);
		}

		protected void doSelectRandomConstraintsItem() {
			JTable navTable = (JTable) find(_gui, JTable.class, null);
			// System.out.print(_tat.valueToString(navTable, 0, 0));
			// System.out.print(navTable.getValueAt(0, 0));
			// _tat.actionSelectCell(navTable, 0, 0);
			Rectangle rec = navTable.getCellRect(0, 0, false);
			_ct.actionClick(navTable,
						(int) rec.getCenterX(),
						(int) rec.getCenterY());
		}

		protected void doDeselectRandomConstraintsItem(DepTree<DecoratedParse, Word> dt) {
			JTable navTable = (JTable) find(_gui, JTable.class, null);
			Rectangle cell =
						navTable.getCellRect(navTable.getSelectedRow(),
									navTable.getSelectedColumn(),
									false);
			_ct.actionKeyPress(KeyEvent.VK_CONTROL);
			_ct.actionClick(navTable,
						(int) cell.getCenterX(),
						(int) cell.getCenterY());
			_ct.actionKeyRelease(KeyEvent.VK_CONTROL);
			for (DepTreeNode node : dt){
				for (String level : dt.getLevels()){
					Element e = node.getElement(Elem.LINE, level);
					if (e != null){
						assertFalse("No lines should be highlighted: \n" + node.getWord() + " " + level, ((Element) e.getParentNode()).getAttribute("stroke").equals(dt.getHighlightColor()));
					}
				}
			}
		}

		public void doGenerateRandomDTREFLink(int targetIndex) {
			doGenerateRandomDTREFLink(targetIndex, 0);
		}

		public void doGenerateRandomDTREFLink(int targetIndex, int canvasIndex) {
			Component canvas = getCanvas(canvasIndex);
			ArrayList<DepTreeNode> nodes = getVisibleNodes(canvasIndex);
			_ct.actionClick(canvas,
						(int) nodes.get(0).getX(),
						(int) nodes.get(0).getY());
			_ct.actionClick(canvas,
						(int) nodes.get(targetIndex).getX(),
						(int) nodes.get(targetIndex).getY(),
						InputEvent.BUTTON3_MASK);
			assertTrue("REF link should habe been created", nodes.get(0).getLink("REF") == targetIndex);
		}
	}
	
	protected void doChangeRandomDTLink(DepTree dt,
				String action,
				String target,
				String firstLevel) {
		doChangeRandomDTLink(dt, 0, action, target, firstLevel);
	}
	
	// only words for the first deptree in annoviewer right now
	protected void doChangeRandomDTLink(DepTree dt,
				int canvasIndex,
				String action,
				String target,
				String firstLevel) {
		ArrayList<Point> firstTargets =
					getClickablePoints(dt, Elem.LINE, firstLevel);
		Elem type = StringToElem(target);
		String secondLevel = StringToLevel(dt, target);
		ArrayList<Point> secondTargets =
					getClickablePoints(dt, type, secondLevel);
		DepTreeNode node = pointToDTNode(dt, firstTargets.get(0), Elem.LINE, firstLevel);
		Component canvas = find(_gui, JSVGCanvas.class, null);
		_ct.actionClick(canvas,
					(int) firstTargets.get(0).getX(),
					(int) firstTargets.get(0).getY());
		_ct.actionClick(canvas,
					(int) secondTargets.get(1).getX(),
					(int) secondTargets.get(1).getY(),
					InputEvent.BUTTON3_MASK);
		if (node != null){
//			System.out.println("found: " + node.getWord() + " " + firstLevel + " "+ firstTargets.get(0) + "\n");
			assertTrue("Link should have been changed. \n Should be: " 
						+ node.getWord()  + " --> " + dt.getNode(1).getWord()
						+ " at "+ firstLevel
						+ "\n Was: "
						+ node.getWord()  + " --> " + dt.getNode(node.getLink(firstLevel)).getWord()
						, node.getLink(firstLevel) == 1);
		}
//		else {
//			System.out.println("not found: "+ firstLevel + " "  + firstTargets.get(0) + "\n");
//		}
		secondTargets = getClickablePoints(dt, type, secondLevel);
		_ct.actionClick(canvas,
					(int) secondTargets.get(2).getX(),
					(int) secondTargets.get(2).getY(),
					InputEvent.BUTTON3_MASK);
		firstTargets = getClickablePoints(dt, Elem.LINE, firstLevel);
		_ct.actionClick(canvas,
					(int) firstTargets.get(0).getX(),
					(int) firstTargets.get(0).getY());
	}

	protected void doPressSYNButton(DepTree dt, boolean hasEffect) {
		String level = dt.getCurrentLevel();
		Component button = find(_gui, AbstractDisplay.LEVEL_BUTTON);
		_ct.actionClick(button);
		if (level.equals("SYN")){
			level = "REF";
		}
		else {
			level = "SYN";
		}
		if (hasEffect){
			assertTrue("Current level should have been " + level, dt.getCurrentLevel().equals(level));
		}
	}

	abstract class AbstractFileMenuPage {

		protected void selectInFile(String itemName) {
			select(itemName, "file menu");
		}
	}
}
