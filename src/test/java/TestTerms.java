import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMConj;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.GraphemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LevelValue;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraints.Direction;
import de.unihamburg.informatik.nats.jwcdg.constraints.Level;
import de.unihamburg.informatik.nats.jwcdg.constraints.VarInfo;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.TermAdd;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.TermBottomPeek;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.TermDivide;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.TermMultiply;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.TermNumber;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.TermSubstract;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.TermTopPeek;
import de.unihamburg.informatik.nats.jwcdg.input.LexiconItem;
import de.unihamburg.informatik.nats.jwcdg.input.SourceInfo;
import de.unihamburg.informatik.nats.jwcdg.lattice.Arc;

public class TestTerms {

	TermNumber tNum1, tNum2, tNum3, tNum4;
	TermAdd tAdd;
	TermSubstract tSub;
	TermDivide tDiv;
	TermMultiply tMul;
	
	LevelValue[] binding;
	
	TermTopPeek tTPeek;
	TermBottomPeek tBPeek;
	
	Path p1, p2;

	public TestTerms() {
	}

	@BeforeClass
	public static void setUpClass() throws Exception {

	}

	@Before
	public void setUp() {
		tNum1 = new TermNumber(1);
		tNum2 = new TermNumber(2);
		tNum3 = new TermNumber(3);
		tNum4 = new TermNumber(4);
		tAdd = new TermAdd(tNum1, tNum2);
		tSub = new TermSubstract(tNum3, tNum2);
		tDiv = new TermDivide(tNum4, tNum2);
		tMul = new TermMultiply(tNum2, tNum3);
		
		p1 = new Path("life", null);
		ArrayList<String> seq = new ArrayList<String>();
		seq.add("deep");
		seq.add("foo");
		p2 = new Path(seq);
		
		AVMNumber num = new AVMNumber(42);
		AVMString str  = new AVMString("foo!");
		AVMNode life = new AVMNode("life", num);
		AVMNode foo  = new AVMNode("foo", str);
		AVMNode deep = new AVMNode("deep", foo);
		
		ArrayList<AVM> cjs = new ArrayList<AVM>();
		cjs.add(life);
		cjs.add(deep);
		AVMConj conj = new AVMConj(cjs);
		
		Arc a = new Arc(0, 1, 0, "Test", 1.0);
		LexiconItem li = new LexiconItem("Test", conj, new SourceInfo());
		GraphemeNode gn = new GraphemeNode(a);
		LexemeNode ln = new LexemeNode(23, gn, li);
		
		ArrayList<String> labels = new ArrayList<String>();
		labels.add(""); labels.add("S");
		Level lvl = new Level("test", new HashMap<String, String>(), labels, new SourceInfo());
		VarInfo x = new VarInfo("X", Direction.AnyDir, lvl, 0);
		
		binding = new LevelValue[2];
		ArrayList<LexemeNode> deps = new ArrayList<LexemeNode>();
		deps.add(ln);
		ArrayList<LexemeNode> regs = new ArrayList<LexemeNode>();
		regs.add(ln);
		LevelValue lv = new LevelValue(lvl, 1, deps, gn, "S", regs, gn);
		binding[0] = lv;
		
		tTPeek = new TermTopPeek(p1, x);
		tBPeek = new TermBottomPeek(p2, x);
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testArithmetics() {
		assertEquals(3, Math.round(((AVMNumber)tAdd.eval(null, null, null)).getNumber()));
		assertEquals(1, Math.round(((AVMNumber)tSub.eval(null, null, null)).getNumber()));
		assertEquals(2, Math.round(((AVMNumber)tDiv.eval(null, null, null)).getNumber()));
		assertEquals(6, Math.round(((AVMNumber)tMul.eval(null, null, null)).getNumber()));
	}
	
	@Test
	public void testPeek() {
		ArrayList<Path> features = new ArrayList<Path>();
		tTPeek.collectFeatures(features); 
		assertEquals(1, features.size());
		assertEquals(p1, features.get(0));
		
		tBPeek.collectFeatures(features);	 
		assertEquals(2, features.size());
		assertEquals(p2, features.get(1));
		
		AVM val1 = tTPeek.eval(null, null, binding);
		assertEquals(Math.round(new AVMNumber(42).getNumber()), Math.round(((AVMNumber)val1).getNumber()));
		
		AVM val2 = tBPeek.eval(null, null, binding);
		assertEquals(new AVMString("foo!").getString(), ((AVMString)val2).getString());
	}
}
