import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMConj;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;
import de.unihamburg.informatik.nats.jwcdg.input.LexiconItem;
import de.unihamburg.informatik.nats.jwcdg.input.SourceInfo;


public class TestLexicon {
	Lexicon lex;
	String s1, s2;
	LexiconItem li1a, li1b, li2a, li2b;
	List<Path> attrs;
	
	@BeforeClass
	public static void setUpClass() throws Exception {

	}

	@Before
	public void setUp() {
		s1= "s1";
		s2= "s2";
		
		List<LexiconItem> entries = new ArrayList<LexiconItem>();
		
		ArrayList<AVM> vals1a = new ArrayList<AVM>();
		AVMNode node1a1 = new AVMNode("foo", new AVMString("a"));
		AVMNode node1a2 = new AVMNode("bar", new AVMString("x"));
		vals1a.add(node1a1);
		vals1a.add(node1a2);
		li1a = new LexiconItem(s1, new AVMConj(vals1a), new SourceInfo());
		entries.add(li1a);
		
		ArrayList<AVM> vals1b = new ArrayList<AVM>();
		AVMNode node1b1 = new AVMNode("foo", new AVMString("b"));
		AVMNode node1b2 = new AVMNode("bar", new AVMString("x"));
		vals1b.add(node1b1);
		vals1b.add(node1b2);
		li1b = new LexiconItem(s1, new AVMConj(vals1b), new SourceInfo());
		entries.add(li1b);
		
		ArrayList<AVM> vals2a = new ArrayList<AVM>();
		AVMNode node2a1 = new AVMNode("foo", new AVMString("a"));
		AVMNode node2a2 = new AVMNode("bar", new AVMString("x"));
		vals2a.add(node2a1);
		vals2a.add(node2a2);
		li2a = new LexiconItem(s2, new AVMConj(vals2a), new SourceInfo());
		entries.add(li2a);
		
		ArrayList<AVM> vals2b = new ArrayList<AVM>();
		AVMNode node2b1 = new AVMNode("foo", new AVMString("b"));
		AVMNode node2b2 = new AVMNode("bar", new AVMString("y"));
		vals2b.add(node2b1);
		vals2b.add(node2b2);
		li2b = new LexiconItem(s2, new AVMConj(vals2b), new SourceInfo());
		entries.add(li2b);
		
		attrs = new ArrayList<Path>();
		attrs.add(new Path("foo", null));
		attrs.add(new Path("bar", null));
		
		lex = new Lexicon(entries, attrs);
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testPaths() {
		
	}
	
	@Test
	public void testExactly() {
		List<LexiconItem> s1s = lex.getExactly(s1);
		List<LexiconItem> s2s = lex.getExactly(s2);
		
		assertTrue(s1s.size() == 2);
		assertTrue(s2s.size() == 2);
	}
	
	@Test
	public void testResolve() {
		lex.resolvePartition( lex.getExactly(s1), attrs);
		lex.resolvePartition( lex.getExactly(s2), attrs);
		
		List<LexiconItem> s1s = lex.getExactly(s1);
		List<LexiconItem> s2s = lex.getExactly(s2);
		
		System.out.println(s1s.get(0).getDistinctiveFeatures());
		System.out.println(s1s.get(1).getDistinctiveFeatures());
		System.out.println(s2s.get(0).getDistinctiveFeatures());
		System.out.println(s2s.get(1).getDistinctiveFeatures());
		
		System.out.println(s1s.get(0).getDescription());
		System.out.println(s1s.get(1).getDescription());
		System.out.println(s2s.get(0).getDescription());
		System.out.println(s2s.get(1).getDescription());
		
		assertTrue(s1s.get(0).getDistinctiveFeatures().size() == 1);
		assertTrue(s1s.get(1).getDistinctiveFeatures().size() == 1);
		
		assertTrue(s1s.get(0).getDescription().contains("_a"));
		assertTrue(s1s.get(1).getDescription().contains("_b"));
		assertTrue(s2s.get(0).getDescription().contains("_a"));
		assertTrue(s2s.get(1).getDescription().contains("_b"));
		
		// sadly not, only a minimal set of distinctive features is found, make sure important features come first via annoFeats
		//assertTrue(s2s.get(0).getDistinctiveFeatures().size() == 2);   
		//assertTrue(s2s.get(1).getDistinctiveFeatures().size() == 2);
		//assertTrue(s1s.get(0).getDescription().contains("_x"));
		//assertTrue(s1s.get(1).getDescription().contains("_x"));
		//assertTrue(s2s.get(0).getDescription().contains("_x"));
		//assertTrue(s2s.get(1).getDescription().contains("_y"));
	}
}
