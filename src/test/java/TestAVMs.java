import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMConj;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMDisj;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMError;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMFlat;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMLexemNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMList;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNumber;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.GraphemeNode;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeNode;
import de.unihamburg.informatik.nats.jwcdg.input.LexiconItem;
import de.unihamburg.informatik.nats.jwcdg.input.SourceInfo;
import de.unihamburg.informatik.nats.jwcdg.lattice.Arc;

public class TestAVMs {

	AVMNumber num;
	AVMLexemNode aln;
	AVMList   al;
	AVMString str, str2;
	AVMConj   conj;
	AVMDisj   disj;
	//AVMGraphemNode agn;
	AVMNode   life, deep, foo;
	AVMFlat   flat;
	AVMError  err;
	
	Path p1, p2;

	public TestAVMs() {
	}

	@BeforeClass
	public static void setUpClass() throws Exception {

	}

	@Before
	public void setUp() {
		p1 = new Path("life", null);
		ArrayList<String> seq = new ArrayList<String>();
		seq.add("deep");
		seq.add("foo");
		p2 = new Path(seq);
		
		num	= new AVMNumber(42);
		str  = new AVMString("foo!");
		str2 = new AVMString("bar");
		life = new AVMNode("life", num);
		foo  = new AVMNode("foo", str);
		deep = new AVMNode("deep", foo);
		
		ArrayList<AVM> cjs = new ArrayList<AVM>();
		cjs.add(life);
		cjs.add(deep);
		conj = new AVMConj(cjs);
		
		
		ArrayList<AVM> djs = new ArrayList<AVM>();
		djs.add(str);
		djs.add(str2);		
		disj = new AVMDisj(djs);
		
		Arc a = new Arc(0, 1, 0, "Test", 1.0);
		LexiconItem li = new LexiconItem("Test", conj, new SourceInfo());
		GraphemeNode gn = new GraphemeNode(a);
		LexemeNode ln = new LexemeNode(23, gn, li);
		
		aln = new AVMLexemNode(ln);
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testPaths() {
		Path p1a = new Path("life", null);
		Path p2a = new Path("deep", new Path("foo", null));
		
		assertTrue(p1.equals(p1a));
		assertTrue(p2.equals(p2a));
		assertFalse(p1.equals(p2));
		assertTrue( p1.hashCode() == p1a.hashCode());
		assertTrue( p2.hashCode() == p2a.hashCode());
		assertFalse(p1.hashCode() == p2.hashCode());
		
		AVM val1 = aln.getLexemeNode().getFeature(p1);
		if(val1 instanceof AVMNumber) {
			assertTrue(((AVMNumber)val1).getNumber() == 42);
		} else {
			assertTrue(false);
		}
		
		AVM val2= aln.getLexemeNode().getFeature(p2);
		if(val2 instanceof AVMString) {
			assertTrue(((AVMString)val2).getString().equals("foo!"));
		} else {
			assertTrue(false);
		}
	}
	
	@Test
	public void testFlatening() {
		LexemeNode ln = aln.getLexemeNode();
		AVM values = ln.getFeature(null);
		List<Path> ps = values.possiblePaths();
		assertTrue(ps.size() == 2);
		
		flat = new AVMFlat(values, ps);
		
		AVM val1 = flat.followPath(p1);
		if(val1 instanceof AVMNumber) {
			assertTrue(((AVMNumber)val1).getNumber() == 42);
		} else {
			assertTrue(false);
		}
		
		AVM val2= flat.followPath(p2);
		if(val2 instanceof AVMString) {
			assertTrue(((AVMString)val2).getString().equals("foo!"));
		} else {
			assertTrue(false);
		}
	}


}
