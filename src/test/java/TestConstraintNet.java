import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.unihamburg.informatik.nats.jwcdg.avms.AVM;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMConj;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMNode;
import de.unihamburg.informatik.nats.jwcdg.avms.AVMString;
import de.unihamburg.informatik.nats.jwcdg.avms.Path;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNet;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.ConstraintNetState;
import de.unihamburg.informatik.nats.jwcdg.constraintNet.LexemeGraph;
import de.unihamburg.informatik.nats.jwcdg.constraints.Constraint;
import de.unihamburg.informatik.nats.jwcdg.constraints.Direction;
import de.unihamburg.informatik.nats.jwcdg.constraints.Level;
import de.unihamburg.informatik.nats.jwcdg.constraints.Signature;
import de.unihamburg.informatik.nats.jwcdg.constraints.VarInfo;
import de.unihamburg.informatik.nats.jwcdg.constraints.formulas.Formula;
import de.unihamburg.informatik.nats.jwcdg.constraints.formulas.FormulaTrue;
import de.unihamburg.informatik.nats.jwcdg.constraints.terms.TermNumber;
import de.unihamburg.informatik.nats.jwcdg.evaluation.CachedGrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.evaluation.GrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.frobbing.CombinedFrobber;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.input.Grammar;
import de.unihamburg.informatik.nats.jwcdg.input.Lexicon;
import de.unihamburg.informatik.nats.jwcdg.input.LexiconItem;
import de.unihamburg.informatik.nats.jwcdg.input.SourceInfo;
import de.unihamburg.informatik.nats.jwcdg.lattice.Lattice;
import de.unihamburg.informatik.nats.jwcdg.transform.ProblemSolver;

/**
 * 
 * @author Niels Beuck
 *
 */
public class TestConstraintNet {
	private List<String> tokens;
	private Configuration config;
	private Lexicon lex;

	public TestConstraintNet() {
	}

	@BeforeClass
	public static void setUpClass() throws Exception {

	}

	@Before
	public void setUp() {

		tokens = new ArrayList<String>();
		StringTokenizer tokenizer = new StringTokenizer("dies ist ein Test");
		while( tokenizer.hasMoreTokens()) {
			String t = tokenizer.nextToken();
			tokens.add(t);
		}

		config = new Configuration();
		config.setFlag("testflag", true);
		config.setInt("testint", 42);
		config.setDouble("testdouble", 1.0);
		config.setString("teststring", "foo");

		List<AVM> l = new ArrayList<AVM>();
		l.add(new AVMNode("foo", new AVMString("bar")));
		AVM val = new AVMConj(l);
		List<LexiconItem> lis = new ArrayList<LexiconItem>();
		lis.add(new LexiconItem("dies", val, new SourceInfo()));
		lis.add(new LexiconItem("ist" , val, new SourceInfo()));
		lis.add(new LexiconItem("ein" , val, new SourceInfo()));
		lis.add(new LexiconItem("Test", val, new SourceInfo()));

		ArrayList<Path> allAttributes = new ArrayList<Path>();
		allAttributes.add(new Path("foo", null));
		lex = new Lexicon(lis, allAttributes);
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testBuildLat() {
		Lattice lat = new Lattice(tokens);
		assertTrue("First arc's from is not 0", lat.getArcs().get(0).from == 0);
		assertTrue(lat.getArcs().get(lat.getArcs().size()-1).to == tokens.size());
	}

	@Test
	public void testBuildConfig() {
		assertTrue("failed to retrieve flag from config", config.getFlag("testflag"));
		assertTrue("failed to retrieve int from config", config.getInt("testint") == 42);
		assertTrue("failed to retrieve double from config", config.getDouble("testdouble") == 1.0);
		assertTrue("failed to retrieve String from config", config.getString("teststring").equals("foo"));
	}

	@Test
	public void testBuildGrammar() {
		Grammar grammar = new Grammar();
		assertTrue(grammar != null);
	}

	@Test
	public void testBuildLg() {
		Lattice lat = new Lattice(tokens);
		LexemeGraph lg = new LexemeGraph(lat, lex, config);
		assertTrue(lg.getMin() == 0);
		assertTrue(lg.getMax() == 4);
	}

	@Test
	public void testverticalSlice() {
		Lattice lat = new Lattice(tokens);
    	Grammar grammar = toyGrammar();
    	GrammarEvaluator evaluator = new CachedGrammarEvaluator(grammar, config);
    	ConstraintNet net = new ConstraintNet(lat, lex, true, true, evaluator, config);
    	ConstraintNetState state = new ConstraintNetState(net, config);	
    	
    	ProblemSolver solver = new CombinedFrobber(state, config, evaluator, 0.0);
    	solver.solve();
	}
	
	private Grammar toyGrammar() {
		Grammar toy = new Grammar();
		
		List<String> labels= new ArrayList<String>();
		labels.add("");
		labels.add("foo");
		labels.add("bar");
		Level lvl = new Level("TEST", new HashMap<String, String>(), labels, new SourceInfo());
		toy.addLevel(lvl);
		
		Formula f1 = new FormulaTrue();
		List<VarInfo> vars = new ArrayList<VarInfo>();
		VarInfo vi = new VarInfo("X", Direction.NonNil, lvl,0);
		vars.add(vi);
		Signature sig = new Signature(lvl, Direction.NonNil); 
		Constraint c = new Constraint("test1", "default", sig, f1, new TermNumber(0), vars, new ArrayList<String>(), new SourceInfo());
		
		toy.addConstraint(c);
		toy.cache(config);
		
		return toy;
	}


	//    @Test
	//    public void testBuildEvaluator() {
	//    	GrammarEvaluator ger = new CachedGrammarEvaluator(grammar, config);
	//    	assertTrue("First arc's from is not 0", lat.getArcs().get(0).from == 0);
	//    	assertTrue(lat.getArcs().get(lat.getArcs().size()-1).to == tokens.size());
	//    }
	//    
	//    @Test
	//    public void testBuildCN() {
	//    	Lattice lat = new Lattice(tokens);
	//    	ConstraintNet cn = new ConstraintNet(lat, grammar, lexicon, true, true, evaluator);
	//    	
	//    }

}
