import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.junit.*;

import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarLexer;
import de.unihamburg.informatik.nats.jwcdg.antlr.jwcdggrammarParser;
import de.unihamburg.informatik.nats.jwcdg.diagnosis.ErrorChecker;
import de.unihamburg.informatik.nats.jwcdg.diagnosis.ErrorDiagnosis;
import de.unihamburg.informatik.nats.jwcdg.evaluation.CachedGrammarEvaluator;
import de.unihamburg.informatik.nats.jwcdg.gui.FileService;
import de.unihamburg.informatik.nats.jwcdg.gui.TestParseFactory;
import de.unihamburg.informatik.nats.jwcdg.input.Configuration;
import de.unihamburg.informatik.nats.jwcdg.parse.DecoratedParse;
import de.unihamburg.informatik.nats.jwcdg.parse.Parse;

/**
 * 
 */

/**
 * @author 3zimmer
 *
 */
public class TestErrorChecker {

	private Configuration _config;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	//TODO: This only works if the German grammar and lexicon is available and at the right place
	@Test
	public void test() {
//		FileService.convert("2.cda");
		try {
			_config = Configuration.fromFile("startup.properties");
			/*
			 *  sentences: 
			 *  "Der Hund bellen" 
			 *  "Der Hund bellst" 
			 *  "Der Hund geht und bellst"
			 */
			int[] expected1 = {1,2};
			testCDA(TestParseFactory.PARSE_ERRORC_1, "Subjekt-Numerus", "Fehlermeldung: Subjekt-Numerus", expected1);
			testCDA(TestParseFactory.PARSE_ERRORC_2, "Subjekt-Person", "Fehlermeldung: Subjekt-Person", expected1);
			int[] expected3 = {2,3,4};
			testCDA(TestParseFactory.PARSE_ERRORC_3, "KON-Person", "Fehlermeldung: KON-Person", expected3);
		}
		catch (RecognitionException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Tests if the parse that the CDA creates contains the expected error 
	 * 
	 * @param cda
	 * @param expectedError
	 * @throws IOException
	 * @throws RecognitionException
	 */
	private void testCDA(String cda, String expectedError, String errorMessage, int[] wordIndexes) throws IOException, RecognitionException {
		DecoratedParse parse = TestParseFactory.generateDecoratedParseObject(cda, _config);
		ErrorDiagnosis expectedDiagnosis = new ErrorDiagnosis(expectedError, errorMessage);
		for (Integer index : wordIndexes){
			expectedDiagnosis.addWordIndex(index);
		}
//		System.out.println(cda + "\n" + ErrorChecker.makeDiagnoses(parse));
		ArrayList<ErrorDiagnosis> diagnoses = ErrorChecker.makeDiagnoses(parse);
		assertTrue("Expected diagnosis: " + expectedDiagnosis + "\n" + "Actual Diagnoses: " + diagnoses, diagnoses.contains(expectedDiagnosis));
	}

}
