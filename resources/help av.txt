﻿Starting options:

Any number of directories above zero can be given as options for the AnnoViewer. It will look in each for files that end with ".cda" and display the parses. Parses from files with the same name are grouped together. It will also look in each directory for a file "done.txt" to read the names of the files that are marked as done.

Interactions with a tree:

- left-click on edge label for manually changing the label
- right-click on edge label for automatically selecting the best (highest-scored) label
- drag and drop an edge to another word or its corresponding dot to reconnect it
- left-click on edge to select it
- left-click on word to select it and to manually change its lexeme
- left-click on navigation tree on the left to select all parses of a sentence

Buttons:

- The button "done" on the right of each tree marks it as done. The button "not done" marks it as not done (default). Both changes are immediately saved in a file "done.txt" in the corresponding directory. A tree that has been marked as done cannot be saved anymore to prevent overwriting the corresponding cda file and, therefore, protecting the annotation that has been manually declared as finished. However, the tree can still be edited but needs to be marked as "not done" to be able to save it. A tree can only be marked as done after after saving it. This makes sure that the displayed tree has been saved when marking it as done.
- The button labeled "SYN" or "REF" (or any other level name specified in the grammar) switches the display of the trees above between the different levels.
- "select all trees" / "select tree #" switches the display above between showing all dependency trees and showing only one.

Menu items:

- "Save all displayed" saves all currently selected trees as ".cda" files in the corresponding directories with the file name shown in the navigation tree on the left, even when only one tree is currently visible.

Interactions with table of constraint violations (only shown when single tree is displayed):

- Selecting a row marks the one or two edges in the tree that violate the constraint displayed in that row
- Right-clicking on table switches between display modes: Either show the rules for the constraint inside a table cell or in a separate text area on the right. In constrast to the table cell, the text area displays the grammar including comments and formatting.

Keys:

- Up and down arrow: Move along navigation tree on the left to select a sentences
