Welcome to CDG.
===============

This is the Java Constraint Dependency Grammar Parser available on
<http://nats-www.informatik.uni-hamburg.de/view/CDG/>.

Please be aware that this port/reimplementation of cdg to java may be
a bit rough around the edges and doesn't have all of the original
functions, such as an interactive command line, yet. However, it comes
with two GUIs in addition to the parser itself: DepTreeViewer and
AnnoViewer.

In [DepTreeViewer](#deptreeviewer), you can type a sentence into the
input field. It is parsed incrementally (one increment for each new
word). If you wish, you can configure the parser to predict upcoming
words. The dependency tree, its score and the constraint violations
are displayed for every increment. Furthermore, parses can be saved as
cda files and cda files can be loaded. The current dependency tree can
be exported as SVG.

![DeptreeViewer parses sentences typed by the user incrementally](img/deptreeviewer.png)

[AnnoViewer](#annoviewer) can open folders with cda files for viewing
and editing. It facilitates annotating sentences, e.g. sentences can
be marked as "done". Additionally, several folders with different
annotations of the same sentences can be opened in parallel, so that
all the dependency trees for one sentence are displayed at the same
time.

![AnnoViewer displays cda files for viewing and editing](img/annoviewer.png)

Please do send us feedback and suggestions or ask for help if you
encounter any problems.

Have fun,  
    Your CDG Team.


Citing
======

If you use jwcdg in your research, please cite Beuck et al. (2013),
which introduces this implementation:

    @InCollection{beuck13:PredIncr,
    author ={Niels Beuck and Arne Köhn and Wolfgang Menzel},
    editor ={Kim Gerdes and Eva Hajičová and Leo Wanner},
    booktitle ={Computational Dependency Theory},
    title ={Predictive Incremental Parsing and its Evaluation},
    publisher ={IOS press},
    year =2013,
    url = {http://dx.doi.org/10.3233/978-1-61499-352-0-186},
    volume =258,
    series ={Frontiers in Artificial Intelligence and Applications},
    pages ={186 - 206}}


Contact
=======

Email:

cdg@informatik.uni-hamburg.de (You will reach the active project
                               members with this e-mail address)

please consider writing an e-mail to cdg@ before contacting an
individual below!

Wolfgang Menzel	<menzel@informatik.uni-hamburg.de> (project leader)  
Niels Beuck <beuck@informatik.uni-hamburg.de>  
Arne Köhn <koehn@informatik.uni-hamburg.de>  
Christine Köhn <ckoehn@informatik.uni-hamburg.de>

See the AUTHORS file for more information on contributors to CDG.


Copyright
=========

Copyright (C) 1997-2015 The CDG Team <cdg@informatik.uni-hamburg.de>

jwcdg is free software: you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 2 of the License, or (at
    your option) any later version.

Please see the file COPYING for details.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.


Installation Requirements
=========================

You need maven to compile jwcdg. It will get all the requirements automatically.

If you want to use the German grammar bundled with jwcdg, you need to
download or create the lexicon and the correspondig hierarchies. You
can download them from

<https://nats-www.informatik.uni-hamburg.de/view/CDG/DownloadPage>

and unpack it into the resources/ directory. If you want to create the
files yourself, you need Perl:

    cd /path/to/jwcdg/
	scripts/make-lexicon.pl
	mv scripts/deutsch-lexikon.cdg resources/
	mv scripts/deutsch-hierarchien.cdg resources/
	
The lexicon and hierarchies files depend on each other. Make sure that
you don't mix download and generated files.

The following programs are recommended for running CDG:

 - A POS tagger with weighted multitagging capabilities:

     hunpos (free software)  
       <https://gitlab.com/akoehn/hunpos>

     or

     TnT (nonfree software but better accuracy for multitagging)  
       <http://heartofgold.dfki.de/pkg/components-tnt.tar.gz>


Compiling From Source
=====================
    # change to jwcdg directory
    cd /path/to/jwcdg/

    # clean old files
    mvn clean

    # run unit tests
    mvn test

    # compile (instead of test if you don't want to run the tests)
    mvn compile

    # create an executable jar file containing all dependencies
    mvn package

    # create your own configuration file
    # We recommend to configure one of the taggers above with
    # "taggerCommand" (see startup.properties)

    cp startup.properties my-startup.properties
    emacs my-startup.properties

Configuring the Parser
======================

If you follow the installation requirements and set up your
`my-startup.properties` as described above, you are ready to run the
parser (jwcdg itself) or one of the GUIs. Have a look at
[default.properties](default.properties) to see all of the options and
their default value.


<a name="prediction"></a>Predict Upcoming Words during Parsing
--------------------------------------------------------------

Set this in your properties file (e.g. `my-startup.properties`)
    
	useVirtualNodes   = true


Running jwcdg
=============

To run jwcdg non-incrementally, use

    java -jar target/jwcdg-1.0.jar my-startup.properties

You can now write sentences and get parses.

The tokens have to be separated by spaces. If you have
non-alphanumeric characters, you should enclose that token in single
quotes.

Example: '"' Viele 'Michael Jackson-Fans' waren traurig '"' , sagte Petra Musterfrau .

to work with an input/output encoding different to your default system encoding: (example for latin-1)

    java -Dfile.encoding=ISO-8859-1 -jar target/jwcdg-1.0.jar my-startup.properties

if you want to use incremental parsing, do this:

    java -jar target/jwcdg-1.0.jar --incremental /path/to/output-%1.cda my-startup.properties

jwcdg will now read a sentence from stdin and write the results to /path/to/output-[Number of Increment].cda

<a name="batches"></a> Processing Batches of Sentences
======================================================

See the BatchProcessor on how to do this. It provides several input formats:

    java -cp target/jwcdg-1.0.jar de.unihamburg.informatik.nats.jwcdg.BatchProcessor -help


<a name="deptreeviewer"></a>Running DepTreeViewer
=================================================

To run DepTreeViewer, use

    java -cp target/jwcdg-1.0.jar de.unihamburg.informatik.nats.jwcdg.gui.DepTreeViewer -c my-startup.properties

By default, sentences in the input field are parsed incrementally,
which can be turned off in the preferences (Edit →
Preferences). Tokens have to be separated by spaces. When parsing
incrementally, a space character triggers the parsing of the current
increment. So, make sure to end your sentence with a space (even after
punctuation marks).

To open a cda file in DepTreeViewer, use File → Open menu or pass it as an argument:

    java -cp target/jwcdg-1.0.jar de.unihamburg.informatik.nats.jwcdg.gui.DepTreeViewer -c my-startup.properties /path/to/file.cda


<a name="annoviewer"></a>Running AnnoViewer
===========================================

You can view and edit dependency annotations for multiple
sentences. The sentences need to be in cda format. cda is the native
output of jwcdg. One way to obtain cda files for your sentences is to
parse them with BatchProcessor (see [Processing batches of
sentences](#batches)). If you need conll, you can [convert cda files
to conll files](#convertToConll) when you have finished annotating.

To run AnnoViewer, use

    java -cp target/jwcdg-1.0.jar de.unihamburg.informatik.nats.jwcdg.gui.AnnoViewer -c my-startup.properties /path/to/folder/with/cda/files

You can specify several folders as arguments if you want to view/edit
the annotations for the same sentences simultaneously. If you do so,
make sure that the cda files have the same names in each folder.

AnnoViewer's performance will decrease with the number of
sentences. We recommend to load up to approximately 100 sentences per
folder. If you want to annotate more sentences, split them into
several folders and either load them subsequently or in different
instances of AnnoViewer.


<a name="convertToConll"></a>Need Your Annotations in conll? Convert cda to conll
=================================================================================

This script converts cda files to
https://gitlab.com/nats/toolbox/blob/master/convert-cda2conll.py


Documentation
=============

jwcdg doesn't have an API documentation right now. If you want to
include jwcdg into your program, have a look at JWCDG.java, where you
can see how one interacts with the different bits of jwcdg (it's
really easy!)

Online documentation of CDG is available at
<http://nats-www.informatik.uni-hamburg.de/view/CDG/CdgManuals>.

Please visit our website to have a look at the publications related to CDG at
<http://nats-www.informatik.uni-hamburg.de/view/CDG/ProjectPublications>.

