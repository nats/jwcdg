#!/usr/bin/env perl

# Copyright (C) 1997-2018 The CDG Team <cdg@nats.informatik.uni-hamburg.de>
#  
# This file is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. For
# more details read COPYING in the root of this distribution.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

use warnings;
use strict;
use File::Basename;
use Cwd 'abs_path';
use File::Spec;
use Getopt::Std;

# see $opt_p in make-verbs.pl
our $opt_p;

getopts("p");

my $dir = dirname(abs_path($0));
my $file = 'deutsch-lexikon.cdg';
my $path = File::Spec->catfile($dir, $file);
print "Writing lexicon to $path\n";

sub read_extra
{
    open LEX,">>",$path or die;
    open EXTRA, "<","$dir/extra.cdg" or die;

    print "Adding extra.cdg\n";
    my $count = 0;
    while(<EXTRA>)
    {
	   # we add everything including comments and empty lines
       print LEX $_;
       $count++;
    }
    close LEX;
    close EXTRA;
    print "$count lines were added to $file.\n"
}

sub read_lexicon
{
    open LEXOUT,">>",$path or die;
    open LEXIN, "<","$dir/Lexikon.cdg" or die;

    print "Adding Lexikon.cdg\n";
    my $count = 0;
    while(<LEXIN>)
    {
		# we add everything including comments and empty lines
        print LEXOUT $_;
        $count++;
    }
    close LEXOUT;
    close LEXIN;
    print "$count lines were added to $file.\n"
}

# delete automatically created lexicon
system("rm -f $path");
# delete automatically created hierarchies and AVZ map
system("rm -f $dir/hierarchien.cdg");
system("rm -f $dir/AVZ.cdg");
#Build the lexicon by running the make* scripts and concatenating the results:
system("$dir/make-names.pl <$dir/Namen.txt >>$path");
system("$dir/make-nouns.pl <$dir/Nomen.txt >>$path");
if ($opt_p) {
	system("$dir/make-verbs.pl -l -p <$dir/Verben.txt >>$path");
}
else {
	system("$dir/make-verbs.pl -l <$dir/Verben.txt >>$path");
}

system("$dir/make-adjectives.pl <$dir/Adjektive.txt >>$path");
system("$dir/make-adjectives.pl <$dir/Adjektiv-Templates.txt >>$path");

read_extra;
read_lexicon;

print "Wrote lexicon to $path\n";

system("cat $dir/AVZ.cdg >> $dir/hierarchien.cdg;mv $dir/hierarchien.cdg $dir/deutsch-hierarchien.cdg");

print "Wrote hierarchies and AVZ map to $dir/deutsch-hierarchien.cdg\n"
