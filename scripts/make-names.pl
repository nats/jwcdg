#!/usr/bin/env perl

# Copyright (C) 1997-2005 The CDG Team <cdg@nats.informatik.uni-hamburg.de>
#  
# This file is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. For
# more details read COPYING in the root of this distribution.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# make-names.pl

use warnings;
use strict;

# how many forms we produced
our $forms = 0;


sub deutsch_lc {
  my $x = shift;
  $x = lc $x;
  $x =~ tr/���/���/;
  return $x;
}

sub deutsch_uc {
  my $x = shift;
  $x = uc $x;
  $x =~ tr/���/���/;
  return $x;
}


sub print_identifier {
  my $x = shift;
  $x =~ s/_/ /g;
  $x =~ s/([\'\"])/\\$1/g;
  if($x =~ /[^a-zA-Z�������]/) {
    $x = "\"$x\"";
  }
  print $x;
}

sub print_name {
  my $type = shift;
  my $name = shift;
  my $case = shift;
  my $number = shift;
  my $gender = shift;
  
  $forms++;

  print_identifier $name;

  my $sib = ($name =~ /(�|s|ch|c)$/) ? ',sibilant:yes' : '';

  print " := [cat:NE,subcat:$type,
  case:$case,person:third,gender:$gender,number:$number,sort:top $sib];\n";

}

our %done;
sub make_name {

  my $apostrophe = 0;
  my $fixed = 0;
  my $plural = 0;
  my $gender = 'bot';

  my $type = shift;
  my $line = shift;

  $line =~ /^([^ ]+)(.*)$/;
  my ($name, $rest) = ($1,$2);
  $name =~ s/_/ /g;
  $apostrophe = 1 if $name =~ /(�|s|ch|c|x|z)$/;

  # check for duplicates
  if ($done{"$type/$name"}++) {
    warn "Duplicate definition for $type `$name'!\n";
    return;
  }

  for(split(/ +/,$rest)) {
    next if /^$/;

    if($_ eq "fix") {
      $fixed = 1;
    } elsif($_ eq "plu") {
      $plural = 1;
    } elsif($_ eq "fem") {
      $gender = 'fem';
    } elsif($_ eq "masc") {
      $gender = 'masc';
    } else {
      die "Bad line: $line\n";
    }
  }

  # singular forms

  # non-inflectable names (ETH)
  if($fixed ||
     ($gender eq 'fem' && $type ne 'Vorname')) {
    print_name($type, $name,'bot','sg', $gender);
  }

  # names ending in sibilants (Ross)
  elsif($apostrophe) {
    print_name($type, $name,'(nom_dat_acc)','sg', $gender);
    print_name($type, "${name}'",'gen','sg', $gender);

    # Names in `ch' can form the genitive both with ' and with s:
    # Karl Bartsch --> Karl Bartsch' Werke
    # Frankreich   --> Frankreichs Regierung
    if($name =~ /ch?$/) {
      print_name($type, "${name}s",'gen','sg', $gender);
    }
  }

  # pluralia tantum (USA)
  elsif($plural) {
    print_name($type, $name,'bot','pl', $gender);
  }

  # normal names (Jennifer)
  else {

    # country and similar names are often uninflected even in the
    # Genitive:
    #
    # Lybiens Diktator -- die Wirtschaft des heutigen Lybien
    #
    # Therefore we must provide for both possibilities.
    if($type =~ /Region|Vorname|Nachname/) {
      print_name($type, $name,'bot','sg', $gender);
    } else {
      print_name($type, $name,'(nom_dat_acc)','sg', $gender);
    }
    print_name($type, "${name}s",'gen','sg', $gender);
    print_name($type, "${name}'s",'gen','sg', $gender);
  }

  # plural forms
  if($type eq 'Produkt') {
    print_name($type, "${name}s",'bot','pl', $gender);
  }

  if($type eq 'Nachname') {
    print_name($type, "${name}s",'bot','pl', $gender);
  }

}

while (<>) {
  chomp;

  # find comments
  next if /^\#/;

  # find name declaration
  if (/^(Firma|Nachname|Produkt|Region|Vorname) +(.*)$/) {
    make_name ($1, $2);
  }

}

print STDERR "Generated $forms names.\n";
