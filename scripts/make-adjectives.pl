#!/usr/bin/env perl

# Copyright (C) 1997-2005 The CDG Team <cdg@nats.informatik.uni-hamburg.de>
#  
# This file is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. For
# more details read COPYING in the root of this distribution.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# make-adjectives.pl

# Generate full-fledged lexical entries for adjectives
# from a shorthand description.
#
# This program employs the traditional morphological classification
# of the german adjectives.
use warnings;
use strict;

use Getopt::Std;
our $opt_v;
getopt('v:');

#Insert escapes into a string so that it is valid CDG read syntax.

sub format_string {
  my $s = shift;
  return "''" if $s eq '';
  if ($s !~ /^[0-9]/ &&
      $s !~ /[ \@\#\$\%^&*()\-=+\\|\`~\"\';:\/!?.>,<\[\]\{\}]/ &&
      $s !~ /^(true|false|label|level)$/) {
    return $s;
  }
  $s =~ s/([\`\'\"])/\\$1/g;
  return "'$s'";
}

# the mapping between endings and forms
our %endings_alist =
  (
   min =>
   {
    er => ['nom_gen_dat:bot:bot:strong_mixed'],
    e  => ['nom_acc:bot:bot:bot'],
    em => ['dat:sg:not_fem:strong'],
    es => ['nom_acc:sg:neut:strong_mixed'],
    en => ['bot:bot:bot:bot']
   },

   med =>
   {
    er =>
    ['nom:sg:masc:strong_mixed', 'gen_dat:sg:fem:strong', 'gen:pl:bot:strong'],
    e =>
    ['nom_acc:sg:fem:bot', 'nom:sg:masc:weak',
     'nom_acc:sg:neut:weak', 'nom_acc:pl:bot:strong'],
    em =>
    ['dat:sg:not_fem:strong'],
    es =>
    [ 'nom_acc:sg:neut:strong_mixed'],
    en =>
    ['gen:sg:not_fem:strong', 'acc:sg:masc:bot', 'dat:pl:bot:strong',
     'gen_dat:sg:bot:weak', 'bot:pl:bot:weak_mixed', 'gen_dat:sg:bot:mixed']
   },

   max =>
   {
    e => [ 'acc:pl:bot:strong',
           'acc:sg:fem:mixed',
           'acc:sg:fem:strong',
           'acc:sg:fem:weak',
           'acc:sg:neut:weak',
           'nom:pl:bot:strong',
           'nom:sg:fem:mixed',
           'nom:sg:fem:strong',
           'nom:sg:fem:weak',
           'nom:sg:masc:weak',
           'nom:sg:neut:weak' ],

    em => [ 'dat:sg:masc:strong',
            'dat:sg:neut:strong' ],

    en => [ 'acc:pl:bot:mixed',
            'acc:pl:bot:weak',
            'acc:sg:masc:mixed',
            'acc:sg:masc:strong',
            'acc:sg:masc:weak',
            'dat:pl:bot:mixed',
            'dat:pl:bot:strong',
            'dat:pl:bot:weak',
            'dat:sg:fem:mixed',
            'dat:sg:fem:weak',
            'dat:sg:masc:mixed',
            'dat:sg:masc:weak',
            'dat:sg:neut:mixed',
            'dat:sg:neut:weak',
            'gen:pl:bot:mixed',
            'gen:pl:bot:weak',
            'gen:sg:fem:mixed',
            'gen:sg:fem:weak',
            'gen:sg:masc:mixed',
            'gen:sg:masc:strong',
            'gen:sg:masc:weak',
            'gen:sg:neut:mixed',
            'gen:sg:neut:strong',
            'gen:sg:neut:weak',
            'nom:pl:bot:mixed',
            'nom:pl:bot:weak' ],

    er => [ 'dat:sg:fem:strong',
            'gen:pl:bot:strong',
            'gen:sg:fem:strong',
            'nom:sg:masc:mixed',
            'nom:sg:masc:strong' ],

    es => [ 'acc:sg:neut:mixed',
            'acc:sg:neut:strong',
            'nom:sg:neut:mixed',
            'nom:sg:neut:strong' ]
   }
  );

# default to medium-level specifiedness
our $endings = $endings_alist{med};

# hash of possible alternations
our %variants;

# how many forms we produced
our $forms = 0;
our %done = ();


sub deutsch_lc {
  my $x = shift;
  $x = lc $x;
  $x =~ tr/���/���/;
  return $x;
}


# accept verbosity level
if($opt_v) {
  if(!exists $endings_alist{$opt_v}) {
    die "-v can only be min, med, or max\n";
  }
  $endings = $endings_alist{$opt_v};
}

# should we print this form?
# FALSE if there is a whitelist of forms and this one isn't on it.
# TRUE otherwise.
sub wanted {
  my $form = shift;
  my $macro = shift;

  # template declarations are always wanted
  return 1 if $macro =~ /T/;
  return 0 if $form eq 'NULL';
  return 1;
}

# perform vowel shift
sub umlaut {
  my $x = shift;
  my %u = ( au => '�u', aa => '�', aua => 'au�',
            a => '�', o => '�', u => '�', A => '�', O => '�', U => '�');
  $x =~ /^(.*?)([aouAOU]+)([^aou]*)$/ or die "Can't umlaut `$x'!\n";
  die "Can't umlaut `$x'" unless exists $u{$2};
  return $1 . $u{$2} . $3;
}

# print a string, quoting if necessary
sub print_identifier {
  my $x = shift;
  $x =~ s/([\'\"])/\\$1/g;
  if ($x =~ /[^a-zA-Z�������]/) {
    $x = "\"$x\"";
  }
  return $x;
}

# Add anchors ^ and $ if the form is a template, and quote if
# necessary.
sub prepare_form {
  my ($f, $macro) = @_;
  if($macro =~ /T/) {
    $f =~ s/\$*$/\$/;
    $f =~ s/^\^*/\^/;
  }
  return $f;
}

# write one lexical entry for predicative adjectives
# $adj - the dictionary entry
# $comparative, $superlative - basic forms
sub write_predicative {
  my($adj,$comparative,$superlative,$macro,@features_pred) = @_;

  if($macro =~ /N/) {
    return;
  }

  # NULL is passed for defective forms, e.g. the positive of
  # `weltgr��te', which doesn't exist.
  return if $adj eq 'NULL';

  my $op = $macro =~ /T/ ? '=~' : ':=';

  if(wanted($adj, $macro) && $macro !~ /S/) {
    $adj = prepare_form $adj, $macro;
    $adj = print_identifier $adj;
    print "$adj $op [cat:ADJD,degree:positive";
    if ($macro !~ /T/) {
      print ",base:". print_identifier $adj;
    }
    print ",$_" for @features_pred;
    print "];\n";
  }

  # reconstruct `vag' from `vage'
  if($adj =~ /^(.+)e$/) {
    my $b = $1;
    if(wanted($b, $macro) && $macro !~ /S/) {
      $adj = prepare_form $b, $macro;
      $adj = print_identifier $b;
      print "$adj $op [cat:ADJD,degree:positive";
      if ($macro !~ /T/) {
        print ",base:". print_identifier $adj;
      }
      print ",$_" for @features_pred;
      print "];\n";
    }
  }
  return if $macro =~ /O/;

  if($comparative ne 'bot') {
    if((wanted $comparative, $macro) && $macro !~ /S/) {
      $comparative = prepare_form $comparative, $macro;
      $comparative = print_identifier $comparative;
      print "$comparative $op [cat:ADJD,degree:comparative";
      if ($macro !~ /T/) {
        print ",base:". print_identifier $adj;
      }
      print ",$_" for @features_pred;
      print "];\n";
    }
  }
  if($superlative ne 'bot') {
    $superlative .= 'en';
    if (wanted "$superlative", $macro) {
      $superlative = prepare_form $superlative, $macro;
      $superlative = print_identifier $superlative;
      print "$superlative $op [cat:ADJD,degree:superlative";
      if ($macro !~ /T/) {
        print ",base:". print_identifier $adj;
      }
      print ",$_" for @features_pred;
      print "];\n";
    }
  }
}

# actually produce output
sub maybe_print {
  my($form,$op,$entry) = @_;
  my $macro = $op eq '=~';

  # Generate irregular variants
  if(exists $variants{$form}) {
    for my $alt(@{$variants{$form}}) {
      if (wanted $alt, $macro) {
        print format_string($alt), "$op $entry";
        $forms++;
      }
    }
  }

  if(wanted $form, $macro) {
    $form = print_identifier $form;
    print "$form $op $entry";
    $forms++;
  }

  # short forms ending in -eren
  # get an automatic variant of -ern
  if($form =~ /^.{3,6}eren$/) {
    (my $f2 = $form) =~ s/eren$/ern/;
    if(wanted $f2, $macro) {
      $f2 = print_identifier $f2;
      print "$f2 $op $entry";
      $forms++;
    }
  }
}

# write one lexical entry for attributive adjectives
sub write_attributive {
  my($form,
     $case,$number,$gender,$flexion,$grade,
     $base,$macro,@features_attr) = @_;
  my $op = $macro =~ /T/ ? '=~' : ':=';

  $form =~ /(e[nmsr]?)$/;
  my $s = $1 || "''";

  $form = prepare_form $form, $macro;
  my $cat = 'ADJA';
  if($macro =~ /N/) {
    $cat = 'NN,cat2:ADJA,person:third';
  }
  my $entry =
    "[cat:$cat,case:$case,number:$number,gender:$gender," .
      "flexion:$flexion,degree:$grade,suffix:$s";
  if($macro !~ /T/) {
    $entry .= ",base:" . print_identifier $base;
  }
  $entry .= ",$_" for @features_attr;
  $entry .= "];\n";

  maybe_print $form, $op, $entry;

}

# the base forms for the positive, comparative and superlative
# degree are produced here
sub make_degrees {
  my($adj,$macro) = @_;
  my @result = ();
  if ($macro !~ /T/ && $adj =~ /,/) {
    @result = split(/,/, $adj);
    $result[0] =~ s/e$//; # -e am Ende l�schen
    $result[2] =~ s/en$//; # -en am Ende l�schen
    if ($result[0] =~ /^hoch$/i) {
      $result[3] =~ s/en$//;
    }
    return @result; # f�r unregelm��ige Adjektive
  }

  elsif ($macro =~ /E/) {
    $result[2] = $adj."st"; # $superlative
    $adj =~ s/e(.)$/$1/;    # -e- l�schen
    $result[0] = $adj; # $positive
    $result[1] = $adj."er"; # $comparative

    return @result;
  }

  elsif ($macro =~ /A/) {
      $adj =~ s/e$//;
      @result = ($adj);
      return @result;
  }

  elsif ($macro =~ /S/) {
    my @splits = split(/,/, $adj);
    if ($macro =~ /T/ && scalar @splits == 3) {
      @result = ('', '', $splits[2]);
    }
    else {
      @result = ('','',$adj);
    }
    return @result;
  }

  elsif ($macro =~ /[UD]/) {
    $adj =~ s/e$//;
    $result[0] = $adj; # $positive
    my $umlaut = umlaut($adj);
    $result[1] = $umlaut."er"; # $comparative

    if ($adj =~ /[szx�dt]$/) {
      $result[2] = $umlaut."est"; # $superlative
    }

    else {
      $result[2] = $umlaut."st"; # $superlative
    }

    if ($macro =~ /D/) {
      $result[3] = $adj."er"; # $comparative

      if ($adj =~ /[szx�dt]$/) {
      $result[4] = $adj."est"; # $superlative
      }
      else {
        $result[4] = $adj."st"; # $superlative
      }
    }
    return @result;
  }

  else {
    $adj =~ s/e$//;
    $result[0] = $adj; # $positive
    $result[1] = $result[0]."er"; # $comparative

    if ($adj =~ /[szx�dt]$/) {
      $result[2] = $result[0]."est"; # $superlative
    } else {
      $result[2] = $result[0]."st"; # $superlative
    }

    return @result;
  }
}

# make all forms for an adjective
sub make_adjective {
  my $line = shift;
  $line =~ s/(^\s+|\s+$)//g;

  # find the adjective and its definitions
  my ($adj,$rest) = $line =~ /^(\S+)((\s+.+)?)$/i
      or die "Bad line: `$line' (line $.)\n";
  $adj =~ s/\s//g;
  $adj =~ s/_/ /g;
  $rest =~ s/(^\s+|\s+$)//g;

  # check for duplicates
  if ($done{"$adj"}++) {
    warn "Duplicate definition for `$1'!\n";
    return;
  }

  my @flags = split(/\s+/,$rest);

  my $macro = ''; # string of the letters AEUDPIT
  my @features_pred = (); # features that are relevant for the predicative form
  my @features_attr = (); # features that are relevant for the attributive form

  for (@flags) {
    if (/^[A-Za-z]$/) {
      $macro .= $_;
      next;
    }
    @features_pred = (@features_pred, $_);
    @features_attr = (@features_attr, $_)
  }

  # forms that are already superlative, and do not have positive versions
  my $adj_orig = "";
  if($macro =~ /S/) {
    $adj_orig = $adj;
    $adj = "NULL,NULL,$adj";
  }

  if ($macro =~ /I/) {
    push @features_attr, 'invariant:yes';
    write_attributive($adj, "bot", "bot", "bot", "bot","positive",
                      $adj,$macro,@features_attr);
    write_predicative($adj,'bot','bot',$macro,@features_pred);
    return;
  }

  my(@basic_forms) = make_degrees($adj,$macro);

  if ($adj =~ /,/){
    $adj = $basic_forms[0];
  }
  if ($macro !~ /A/){
    if ($basic_forms[0] =~ /^hoch$/i){
  	write_predicative($adj,$basic_forms[2],$basic_forms[3],
                          $macro,@features_pred);
    } else {
      write_predicative($adj,$basic_forms[1],$basic_forms[2],
                        $macro,@features_pred);
    }
  }

  if ($macro !~ /P/) {
    for (my $i = 0; !($i > $#basic_forms); $i++) {
      my $ending;
       foreach $ending ("er","e","em","es","en") {
	   my($form);
	   if (($i == 0) && ($basic_forms[0] =~ /^hoch$/i)) {
	   $i++;
	   }

	   $form = $basic_forms[$i].$ending;

	   my $j = 0;

         FORM:
           while (defined($$endings{$ending}[$j])){
	     my($case, $number, $gender,$flexion,$this_grade);
	     my @grade =
               ("positive","comparative","superlative","comparative","superlative");
	     if ($basic_forms[0] =~ /^hoch$/i) {
               $this_grade = $grade[$i-1];
	     } else {
               $this_grade = $grade[$i];
	     }

             ($case, $number, $gender,$flexion) =
               split(/:/, $$endings{$ending}[$j]);
	     $j++;
         my $base = $adj;
         if ( ($base =~ /^NULL$/) && ($adj_orig !~ //)) {
           $base = $adj_orig;
         }
	     next FORM if $macro =~ /S/ && $this_grade ne 'superlative';

             write_attributive($form, $case, $number, $gender,
                               $flexion, $this_grade,
                               $base,$macro,@features_attr);

             # optional `e' in superlative forms
             if($i == 2 && $macro =~ /e/) {
               my $f = $form;
               $f =~ s/st/est/;
               write_attributive($f, $case, $number, $gender,
                                 $flexion, $this_grade,
                                 $base,$macro,@features_attr);
             }
           }
        }
      last if $macro =~ /O/;
    }
  }
} # end of make_adjective


# MAIN

while (<>) {
  chomp;

  # find comments
  next if /^\#/;
  next if /^\s*$/;

  # find adjective declaration
  if (/^adj +(.*)$/) {
    make_adjective $1;
  }

  # find variant declaration
  elsif (/^variant\s+(\S+)\s+(\S+)\s*$/) {
    push @{$variants{$1}}, $2;
  }

  # reject other lines
  else {
    die "Invalid line $.: `$_'\n";
  }

}

print STDERR "Generated $forms adjectives. \n";

