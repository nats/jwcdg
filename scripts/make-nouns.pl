#!/usr/bin/env perl

# Copyright (C) 1997-2005 The CDG Team <cdg@nats.informatik.uni-hamburg.de>
#  
# This file is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version. For
# more details read COPYING in the root of this distribution.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# make-nouns.pl

# Generate full-fledged lexical entries for nouns
# from a shorthand description.
#
# This program employs the morphological classification specified in
# "Automatic Recognition and Morphological Classification of German
# Nouns" (Nakov, Angelova and von Hahn 2001).
use warnings;
use strict;

use Getopt::Std;
our $opt_v;
getopts('v:');

if (!$opt_v) {
  $opt_v = 'med';
}

our %variants;

our %gender = ( m => 'masc', f => 'fem', n => 'neut' );
our %casenum =
  ( 'nom.sg' => 0,
    'gen.sg' => 1,
    'dat.sg' => 2,
    'acc.sg' => 3,
    'nom.pl' => 4,
    'gen.pl' => 5,
    'dat.pl' => 6,
    'acc.pl' => 7 );

# the 38 German noun inflection classes
our %classes =
  (
   m0    =>  [  qw(  0    0     0     0    0    0    0     0)    ], #  m0:   Fonds
   m1    =>  [  qw(  0    [e]s  [e]   0    e    e    en    e)    ], #  m1:   Tag
   m1a   =>  [  qw(  0    ses   [se]  0    se   se   sen   se)   ], #  m1a:  Bus
   m1b   =>  [  qw(  us   us    us    us   i    i    i     i)    ], #  m1b:  Modus
   m2    =>  [  qw(  0    [e]s  [e]   0    "e   "e   "en   "e)   ], #  m2:   Bach
   m3    =>  [  qw(  0    [e]s  [e]   0    "er  "er  "ern  "er)  ], #  m3:   Wald
   m3a   =>  [  qw(  0    [e]s  [e]   0    er   er   ern   er)   ], #  m3a:  Leib
   m4    =>  [  qw(  0    s     0     0    0    0    n     0)    ], #  m4:   Deckel
   m5    =>  [  qw(  0    s     0     0    "    "    "n    ")    ], #  m5:   Vater
   m6    =>  [  qw(  0    s     0     0    s    s    s     s)    ], #  m6:   Gummi
   m6a   =>  [  qw(  0    [s]   0     0    [s]  [s]  [s]   [s])  ], #  m6a:  Pkw
 # m7 is morphologically an adjective class, and should be declared in Adjektive.txt
   m7a   =>  [  qw(  0    ns    n     n    n    n    n     n)    ], #  m7a:  Glaube
   m7b   =>  [  qw(  0    n     n     n    n    n    n     n)    ], #  m7b:  Riese
   m7c   =>  [  qw(  0    [e]n  [e]n  [e]n en   en   en    en)   ], #  m7c:  Herr
   m8    =>  [  qw(  0    [en]  [en]  [en] en   en   en    en)   ], #  m8:   Mensch
   m8a   =>  [  qw(  0    s     0     0    en   en   en    en)   ], #  m8a:  Prozessor
   m9    =>  [  qw(  0    [e]s  [e]   0    en   en   en    en)   ], #  m9:   Staat
   m9a   =>  [  qw(  0    [e]s  [e]   0    ten  ten  ten   ten)  ], #  m9a:  Bau
   m10   =>  [  qw(  0    s     0     0    n    n    n     n)    ], #  m10:  Konsul
   m11   =>  [  qw(  us   us    us    us   en   en   en    en)   ], #  m11:  Organismus
   m11a  =>  [  qw(  o    os    o     o    i    i    i     i )   ], #  m11a: Maestro
   m11b  =>  [  qw(  ex   exes  ex    ex   izes izes izes  izes )], #  m11b: Index
   m11c  =>  [  qw(  os   os    os    os   en   en   en    en)   ], #  m11c:  Mythos
   m11d  =>  [  qw(  0    [s]   0     0    nen  nen  nen   nen)  ], #  m11d:  Embryo
   f12   =>  [  qw(  0    0     0     0    e    e    en    e)    ], #  f12:  Drangsal
   f13   =>  [  qw(  0    0     0     0    se   se   sen   se)   ], #  f13:  Kenntnis
   f14   =>  [  qw(  0    0     0     0    "e   "e   "en   "e)   ], #  f14:  Nacht
   f14a  =>  [  qw(  0    0     0     0    "    "    "n    ")    ], #  f14a: Mutter
   f15   =>  [  qw(  0    0     0     0    s    s    s     s)    ], #  f15:  Kamera
   f15a  =>  [  qw(  a    a     a     a    en   en   en    en)   ], #  f15a: Firma
   f15b  =>  [  qw(  0    0     0     0    0    0    0     0)    ], #  f15b: Spezies
   f15c  =>  [  qw(  is   is    is    is   en   en   en    en)   ], #  f15c: Basis
   f16   =>  [  qw(  0    0     0     0    n    n    n     n)    ], #  f16:  Blume
   f17   =>  [  qw(  0    0     0     0    en   en   en    en)   ], #  f17:  Zahl
   f18   =>  [  qw(  0    0     0     0    nen  nen  nen   nen)  ], #  f18:  Lehrerin
 # f19 is morphologically an adjective class, and should be declared in Adjektive.txt
   n20   =>  [  qw(  0    [e]s  [e]   0    e    e    en    e)    ], #  n20:  Schaf
   n20a  =>  [  qw(  0    es    [e]   0    "e   "e   "en   "e)   ], #  n20a: Flo�
   n21   =>  [  qw(  0    [e]s  [e]   0    er   er   ern   er)   ], #  n21:  Feld
   n22   =>  [  qw(  0    [e]s  [e]   0    "er  "er  "ern  "er)  ], #  n22:  Dorf
   n23   =>  [  qw(  0    s     0     0    0    0    n     0)    ], #  n23:  Fenster
   n23a  =>  [  qw(  0    s     0     0    "    "    "n    ")    ], #  n23a: Kloster
   n24   =>  [  qw(  0    s     0     0    s    s    s     s)    ], #  n24:  Auto
   n24a  =>  [  qw(  0    [e]s  0     0    es   es   es    es)   ], #  n24:  Match
   n25   =>  [  qw(  0    [e]s  [e]   0    en   en   en    en)   ], #  n25:  Bett
   n25a  =>  [  qw(  0    en    [en]  0    en   en   en    en)   ], #  n25a: Herz
 # n26 is morphologically an adjective class, and should be declared in Adjektive.txt
   n27   =>  [  qw(  0    ses   [se]  0    se   se   sen   se)   ], #  n27:  Begr�bnis
   n28   =>  [  qw(  um   ums   um    um   en   en   en    en)   ], #  n28:  Datum
   n28a  =>  [  qw(  a    as    a     a    en   en   en    en)   ], #  n28a: Drama
   n28b  =>  [  qw(  o    os    o     o    en   en   en    en)   ], #  n28b: Risiko
   n28c  =>  [  qw(  os   os    os    os   en   en   en    en)   ], #  n28c: Epos
   n28d  =>  [  qw(  on   ons   on    on   en   en   en    en)   ], #  n28d: Stadion
   n28e  =>  [  qw(  us   us    us    us   en   en   en    en)   ], #  n28e: Virus
   n28f  =>  [  qw(  o    os     o     o    i    i    i     i )   ], #  n28f: Porto
   n29   =>  [  qw(  um   ums   um    um   a    a    a     a)    ], #  n29:  Maximum
   n29a  =>  [  qw(  on   ons   on    on   a    a    a     a)    ], #  n29a: Lexikon
   n30   =>  [  qw(  0    s     0     0    n    n    n     n)    ], #  n30:  Auge
   n31   =>  [  qw(  0    [e]s  0     0    ien  ien  ien   ien)  ], #  n31:  Privileg
   n32   =>  [  qw(  0    0     0     0    0    0    0     0  )  ], #  n32:  Internet
   n33   =>  [  qw(  es   es    em    es   0    0    0     0  )  ] #  n33:  Besonderes
  );


# how many forms we produced
our $forms = 0;
our %done = ();
our %entries = ();

sub deutsch_lc {
  my $x = shift;
  $x = lc $x;
  $x =~ tr/���/���/;
  return $x;
}

sub deutsch_uc {
  my $x = shift;
  $x = uc $x;
  $x =~ tr/���/���/;
  return $x;
}

# decode an entry from a paradigm
# into the list of actual orthographic sequences
#
# Example: [e]s ==> ('es', 's')
sub decode {
  my $x = shift;
  my @result;

  # remove zero marker and umlaut marker
  $x =~ s/[0\"]//g;

  # without optional parts
  (my $e = $x) =~ s/\[.+\]//;
  push @result, $e;

  # with optional parts
  if (my($pre,undef,$post) = ($x =~ /^(.*)\[(.+)\](.*)$/)) {
    for (split(/\|/,$2)) {
      push @result, "$pre$_$post";
    }
  }

  return @result;
}


# perform vowel shift
sub umlaut {
  my $x = shift;
  my %u = ( au => '�u', aa => '�', aua => 'au�', oa => 'o�', ea => 'e�',
            a => '�', o => '�', u => '�', A => '�', O => '�', U => '�');
  $x =~ /^(.*?)([aouAOU]+)([^aou]*)$/ or die "Can't umlaut `$x'!\n";
  die "Can't umlaut `$x'" unless exists $u{$2};
  return $1 . $u{$2} . $3;
}

# since 1999, � and ss are permissible variants in many situations,
# so we simply allow both everywhere
sub substitute_sharp_s {
  my $form = shift;

  if ($form =~ s/�((e|es|en|er)?)$/ss$1/) {
    return $form;
  }

  if ($form =~ s/ss((e|es|en|er)?)$/�$1/) {
    return $form;
  }
  return $form;
}

# print a string, quoting if necessary
sub print_identifier {
  my $x = shift;
  if ($x =~ /[^a-zA-Z�������]/) {
    $x =~ s/([\'\"])/\\$1/g;
    $x = "\"$x\"";
  }
  return $x;
}

# Write one lexical entry.
sub write_form {
  my($form,$b,$c,$n,$g,$flags) = @_;

  my $result;

  $result .= " := [cat:NN,base:";
  $result .= print_identifier $b;
  $result .= ",case:$c,person:third,gender:$g,number:$n";
  if (exists($$flags{obj})) {
    $result .= ",valence:'$$flags{obj}?'";
  }
  if (exists($$flags{Stoffnomen})) {
    $result .= ",Stoffnomen:yes";
  }
  if (exists($$flags{extra})) {
    $result .= ",$$flags{extra}";
  }

  $result .= "];\n";

  if (! exists($entries{$form}{$result})) {
    my $f = print_identifier $form;
    print "$f$result";
    $entries{$form}{$result} = 1;
    $forms++;
  }

  if (exists($variants{$form})) {
    my $v = print_identifier $variants{$form};
    print "$v$result";
    $entries{$v}{$result} = 1;
    $forms++;
  }
}

# return all forms for a particular case+number
  sub make_forms {
    my $stem = shift;
    my $paradigm = shift;
    my $case = shift;
    my $number = shift;
    my %results;
    my $ending = $$paradigm[$casenum{"$case.$number"}];

    # perform vowel shift
    if ($ending =~ /\"/) {
      $stem = umlaut $stem;
    }

    # get all possible endings
    my @e = decode $ending;

    for my $e (@e) {

      # `Spatenn' => `Spaten'
      if ($stem =~ /n$/ && $e eq 'n') {
        $e = '';
      }

      # prevent `Schatzs'
      my $form=$stem . $e;
      unless($stem =~ /(s|�|z)$/ && $e eq 's') {
        $results{$form}++;

        # `Beschlu�' may also be written `Beschluss'
        $form = substitute_sharp_s $form;
        $results{$form}++;
      }
    }

    return keys %results;
  }

# return e.g. `nom', or `nom_acc', or `bot', as the situation demands
sub make_case {
  my $a = shift;
  my %cases;
  for (@$a) {
    /(...)\.../;
    $cases{$1}++;
  }
  my $result = join("_", reverse sort keys %cases);
  return $result eq 'nom_gen_dat_acc' ? 'bot' : $result;
}

# return `sg', `pl' or `bot', as the situation demands
sub make_number {
  my $a = shift;
  my %numbers;
  for (@$a) {
    /...\.(..)/;
    $numbers{$1}++;
  }
  my $result = join("_", sort keys %numbers);
  return $result eq 'pl_sg' ? 'bot' : $result;
}

# return e.g. `masc', `not_fem' or `bot', as the situation demands
sub make_gender {
  my $a = shift;
  my %genders;
  for (@$a) {
    /\.([^.]+)$/;
    $genders{$1}++;
  }
  my $result = join("_", sort keys %genders);
  return 'bot' if $result eq 'fem_masc_neut';
  return 'not_fem' if $result eq 'masc_neut';
  return 'not_neut' if $result eq 'fem_masc';
  return 'not_masc' if $result eq 'fem_neut';
  return $result;
}


# make all forms for a noun
sub make_noun {
  my $line = shift;
  my ($form,$classes,$rest) =
    ($line =~ /^(.*[^ ]) +((?:[fmn]\d+[a-z]?\/?)+)( .*)?$/)
      or die "Bad line: `$line' (line $.)\n";
  $rest = '' unless $rest;
  $rest =~ s/^ +//;

  # check for duplicates
  if ($done{"$form/$classes"}++) {
    warn "Duplicate definition for `$form'!\n";
  }

  # check inflection class specifications
  my %flags = ();
  my @classes = split m|/|, $classes;
  for (@classes) {
    if (!exists($classes{$_})) {
      die "Bad inflection class `$_' (line $.)\n";
    }
  }

  # read additional flags
  for my $flag (split(/ +/, $rest)) {
    if ($flag eq 'sg') {
      $flags{'singulare tantum'} = 1;
    } elsif ($flag eq 'pl') {
      $flags{'plurale tantum'} = 1;
    } elsif ($flag =~ /obj:([cis]+)/) {
      $flags{obj} = $1;
    } elsif ($flag eq 'sto') {
      $flags{Stoffnomen} = 1;
    } else {
      $flags{extra} .= $flag;
    }
  }

  # compute base form
  my $firstclass = $classes[0];
  my $paradigm = $classes{$firstclass};
  (my $nom_sg = $$paradigm[0]) =~ s/[\[\]0\"]//g;
  my $stem = $form;
  $stem =~ s/$nom_sg$//;

  if ($opt_v eq 'min') {

    # compute all possible forms
    # and represent them as compactly as possible
    my %forms;
    for (@classes) {
      my $paradigm = $classes{$_};
      my $g = $gender{substr($_,0,1)};
      for my $c (qw(nom gen dat acc)) {
        for my $n (qw(sg pl)) {
          next if(exists($flags{'singulare tantum'}) && $n eq 'pl');
          next if(exists($flags{'plurale tantum'}) && $n eq 'sg');
          for (make_forms($stem, $paradigm, $c, $n)) {
            push @{$forms{$_}}, "$c.$n.$g";
          }
        }
      }
    }

    while (my($k,$v) = each %forms) {
      my $case = make_case($v);
      my $gender = make_gender($v);
      my $number = make_number($v);
      write_form($k,$stem.$nom_sg,$case, $number, $gender, \%flags);
    }
  } elsif ($opt_v eq 'med') {

    # make as few entries as possible without losing number information

    my %forms;
    for (@classes) {
      my $paradigm = $classes{$_};
      my $g = $gender{substr($_,0,1)};
      for my $c (qw(nom gen dat acc)) {
        for my $n (qw(sg pl)) {
          next if(exists($flags{'singulare tantum'}) && $n eq 'pl');
          next if(exists($flags{'plurale tantum'}) && $n eq 'sg');
          for (make_forms($stem, $paradigm, $c, $n)) {
            push @{$forms{$n}{$_}}, "$c.$n.$g";
          }
        }
      }
    }

    for my $n (qw(sg pl)) {
      while (my($k,$v) = each %{$forms{$n}}) {
        my $case = make_case($v);
        my $gender = make_gender($v);
        write_form($k,$stem.$nom_sg,$case, $n, $gender, \%flags);
      }
    }
  } else {

    # make all entries no matter what; just take care not to produce
    # identical entries, those would get thrown out by cdgp anyway
    for (@classes) {
      my $paradigm = $classes{$_};
      my $g = $gender{substr($_,0,1)};
      for my $c (qw(nom gen dat acc)) {
        for my $n (qw(sg pl)) {
          next if(exists($flags{'singulare tantum'}) && $n eq 'pl');
          next if(exists($flags{'plurale tantum'}) && $n eq 'sg');
          for (make_forms($stem, $paradigm, $c, $n)) {
            write_form($_, $stem.$nom_sg, $c, $n, $g, \%flags);
          }
        }
      }
    }
  }
}

my @orders;
while (<>) {
  chomp;

  # find comments
  next if /^\#/;

  # find name declaration
  if (/^noun +(.*)$/) {
    push @orders, $1
  } elsif (/^variant\s+(\S+)\s+(\S+)$/) {
    $variants{$1} = $2;    
  }
}

for(@orders) {
  make_noun $_;
}


print STDERR "Generated $forms nouns.\n";
